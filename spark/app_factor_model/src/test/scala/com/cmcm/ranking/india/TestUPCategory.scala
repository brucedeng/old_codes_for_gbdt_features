package com.cmcm.ranking.india

import java.io.File

import org.apache.spark.Logging
import org.scalatest.{Matchers, FunSuite}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import com.cmcm.ranking.india.featureUtils.upParser
import scala.util.Try

/**
 * Created by mengchong on 4/9/16.
 */

@RunWith(classOf[JUnitRunner])
class TestUPCategory extends FunSuite with Logging{

  test("Load JSON correctly.") {
    val line = """{"content_length": 4069, "image_md5": "", "videos": [], "word_count": 3723, "thirdads": "0", "is_servable": true, "has_copyright": 0, "images": [], "head_image": "", "editorial_item_level": 0, "cp_id": "", "fetch_time": 1460087357, "copyright": "", "author": "", "entities": [{"L1_weight": 0.1735013491385794, "name": "\u092e\u0927\u0941\u092e\u0947\u0939", "weight": 0.6006304566349607}, {"L1_weight": 0.16702176623836085, "name": "\u0917\u0930\u094d\u092d\u0935\u0924\u0940", "weight": 0.578199306355813}, {"L1_weight": 0.06589714702859507, "name": "\u092c\u091a\u0947\u0902", "weight": 0.2281240676642399}, {"L1_weight": 0.04459680114804734, "name": "\u092d\u093e\u0930\u0924", "weight": 0.15438610230411223}, {"L1_weight": 0.04459680114804734, "name": "\u0905\u092a\u0928\u093e\u090f\u0902", "weight": 0.15438610230411223}, {"L1_weight": 0.042385389520871446, "name": "\u0917\u0930\u094d\u092d\u093e\u0935\u0938\u094d\u0925\u093e", "weight": 0.14673059309894965}, {"L1_weight": 0.03851541917331361, "name": "\u0926\u093f\u0932\u094d\u0932\u0940", "weight": 0.13333345198991509}, {"L1_weight": 0.035935438941608396, "name": "\u0915\u093f\u0921\u0928\u0940", "weight": 0.12440202458389209}, {"L1_weight": 0.03575115463934374, "name": "\u0917\u0930\u094d\u092d\u093e\u0935\u0938\u094d\u0925\u093e_\u0915\u0947_\u0926\u094c\u0930\u093e\u0928", "weight": 0.12376406548346187}, {"L1_weight": 0.03575115463934374, "name": "\u0939\u093e\u0908_\u092c\u094d\u0932\u0921_\u092a\u094d\u0930\u0947\u0936\u0930", "weight": 0.12376406548346187}, {"L1_weight": 0.03317117440763851, "name": "\u092c\u094d\u0932\u0921_\u092a\u094d\u0930\u0947\u0936\u0930", "weight": 0.11483263807743883}, {"L1_weight": 0.032802605803109196, "name": "\u092a\u0940\u0932\u093f\u092f\u093e", "weight": 0.11355671987657841}, {"L1_weight": 0.03188118429178591, "name": "\u092a\u094d\u0930\u094b\u091f\u0940\u0928", "weight": 0.11036692437442733}, {"L1_weight": 0.03188118429178591, "name": "\u092a\u0947\u091f", "weight": 0.11036692437442733}, {"L1_weight": 0.03169689998952124, "name": "\u092a\u0930\u093f\u0935\u093e\u0930", "weight": 0.10972896527399711}, {"L1_weight": 0.03169689998952124, "name": "\u091b\u094b\u0921\u093c\u0947\u0902", "weight": 0.10972896527399711}, {"L1_weight": 0.031144047082727278, "name": "\u092c\u091a\u094d\u091a\u0947", "weight": 0.10781508797270649}, {"L1_weight": 0.030959762780462616, "name": "\u092a\u0947\u0936\u093e\u092c", "weight": 0.10717712887227626}, {"L1_weight": 0.030775478478197962, "name": "\u092e\u0947\u0902_\u0938\u094d\u0924\u094d\u0930\u0940", "weight": 0.10653916977184605}, {"L1_weight": 0.03003834126913933, "name": "\u0917\u0930\u094d\u092d\u092a\u093e\u0924", "weight": 0.1039873333701252}], "adult_score": 0.0, "title_md5": "c1bb7bfc", "channel_names": ["health", "lifestyle"], "content_md5": "ebef0c35", "opencms_id": "0", "type": "article", "title_entities": [{"L1_weight": 0.4611973392461197, "name": "\u092e\u0927\u0941\u092e\u0947\u0939", "weight": 0.7525126646991296}, {"L1_weight": 0.36363636363636365, "name": "\u0917\u0930\u094d\u092d\u0935\u0924\u0940", "weight": 0.5933272933204676}, {"L1_weight": 0.17516629711751663, "name": "\u092c\u091a\u0947\u0902", "weight": 0.28581009861168866}], "location": [], "update_time": 1460087375, "publish_time": 1460087106, "large_image": "", "body_images": [], "channel_ids": [23, 31], "cp_version": 13, "discovery_time": 1460087357, "source_type": 2, "link": "http://dhunt.in/14wE4", "display_type": 0, "item_id": "11056287", "source_feed": ["http://api-news.dailyhunt.in/api/v1/news/articles/latest/group/hindians/Home"], "categories": [{"name": "1000395", "weight": 0.9050111714923259}], "publisher": "IANS Hindi", "feature_scores": {"newsy_score": 0.7782109456201363, "adult_score": 0.0}, "language": "hi", "photos": [], "editorial_importance_level": 0, "region": "in", "title": "\u092e\u0927\u0941\u092e\u0947\u0939 \u092a\u0940\u0921\u093c\u093f\u0924\u093e \u0917\u0930\u094d\u092d\u0935\u0924\u0940 \u091c\u094b\u0916\u093f\u092e \u0938\u0947 \u092c\u091a\u0947\u0902", "last_modified_time": 1460087357, "summary": "", "cp_disable": false, "source": "", "newsy_score": 0, "group_id": "247793", "image_count": 0}"""
    val upobj = new upParser(line)
//    System.out.println(upobj.getCategoryPair)
  }

  test("Load JSON with two categories.") {
    val line = """{"categories": [{"name": "1000780", "weight": 0.4307547185029262},{"name": "1000781", "weight": 0.2307547185029262}],"image_count": 1}"""
    val upobj = new upParser(line)
//    System.out.println(upobj.getCategoryPair)
  }

}

