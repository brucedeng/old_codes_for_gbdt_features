package com.cmcm.ranking.india

import scala.util.parsing.json._
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import com.cmcm.ranking.india.featureUtils.upParser


/**
 * Created by mengchong on 4/9/16.
 */
object StatisticUPCategories {

  def main(args: Array[String]){
    val data = args.head

    println("Loading data %s".format(data))
    val conf = new SparkConf().setAppName("Simple Application")
    val sc = new SparkContext(conf)
    val logData = sc.textFile(data)
    val logFormat = logData.flatMap( s => new upParser(s).getCategoryPair ).reduceByKey((a,b) => a+b )
    logFormat.collect().foreach(println)
  }

//  def main(args: Array[String]){
//    val data = args.head
//    println("Loading data %s".format(data))
//    val conf = new SparkConf().setAppName("Simple Application")
//    val sc = new SparkContext(conf)
//    val logData = sc.textFile(data)
//    val logFormat = logData.map( s => (1,1) )
//    val statCnt = logFormat.reduceByKey((a,b) => a+b)
//    statCnt.collect().foreach(println)
//  }
}
