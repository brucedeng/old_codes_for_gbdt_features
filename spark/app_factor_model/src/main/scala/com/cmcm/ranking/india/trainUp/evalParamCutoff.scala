package com.cmcm.ranking.india.trainUp

import java.io.{File, InputStream}

import com.typesafe.config.ConfigFactory
import org.apache.spark.ml.classification.{LogisticRegression, LogisticRegressionModel}
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by kehan on 5/20/16.
  */
object evalParamCutoff extends Serializable{



  def main(args: Array[String]) {
    val desc = "Eval different cutoff of params for no-bin factor model"

    println(desc)

    val configPath = args(0)

    val config = ConfigFactory.parseFile(new File(configPath))

    val outputPath = config.getString("output")
    val modelDir = config.getString("model_dir")
    val localModelFile = config.getString("local_model_file")
    val cutNum = config.getString("dcat_param_cut_num").toInt



    val hadoopConf = new org.apache.hadoop.conf.Configuration()

    val conf = new SparkConf().setAppName("eval ParamCutoff")
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._


    val paramFile = args(1)

    println(paramFile)

    val paramCutted = modelUtils.getParamsByCutoff(paramFile,cutNum)
    ioUtils.saveCoef2File(localModelFile+"."+cutNum.toString,modelUtils.genCoefList(paramCutted.toArray))

    println("paramaters:")
    modelUtils.printCatCoef(paramCutted.toArray)

    val testingRaw = sc.objectFile[(Double,Vector,Double,String)](outputPath + "/testing_data")

    //val test = testingRaw.toDF("label","features","weight","uid")

    val test = testingRaw.toDF("label","features","weight","uid")
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    val predictionAndLabels = testingRaw.filter(_._2.numNonzeros > 0).map{
      case(label, features, weight, uid) => {
        val prediction = (for ((a, b) <- features.toArray zip paramCutted.toArray) yield a * b).sum
        (prediction,label,uid)
      }
    }

    val predictionCount = predictionAndLabels.map(x => (x._1.signum, 1)).reduceByKey((a,b) => a+b).collect()

    println("Valid prediction number:")
    println(predictionCount.toList.toString())

    //val predictionValid = predictionAndLabels.filter(x => x._1 > 0 )
    val predictionValid = predictionAndLabels

    // calculate test auc
    val metrics = new BinaryClassificationMetrics(predictionValid.map( x => (x._1,x._2)),1000)
    val auROC = metrics.areaUnderROC()
    println("test auc:\t" + auROC)

    //test per user auc
    val perUserAuc = predictionValid.map{ case (prob,label,uid) => (uid,new PredPoint(prob,label))}
      .groupByKey()
      .map{case (uid,probLabel) => modelUtils.evalAuRoc(probLabel.toArray,5)}
      .filter(_ > 0.0)
      .mean()
    println("peruser auc:\t" + perUserAuc)
  }
}
