package com.cmcm.ranking.india.trainUp

import java.io._
import scala.io.Source
/**
 * Created by mengchong on 5/4/16.
 */
object ioUtils {
  def saveCoef2File(filename:String,coef:Array[(Int,String,Double)]) = {
    val outStr = coef.map{case (fid,fname,value) => fid.toString + "\t" + fname + "\t" + value.toString}
      .mkString("\n")
    val file = new File(filename)
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(outStr)
    bw.close()
  }

  def loadCoefFile(filename:String):Array[(Int,String,Double)] = {
    (for ( line <- Source.fromFile(filename).getLines()) yield {
      val fields = line.split('\t')
      (fields(0).toInt,fields(1),fields(2).toDouble)
    }).toArray
  }
}
