package com.cmcm.ranking.india.trainUp

/**
 * Created by mengchong on 5/5/16.
 */
class event(val label:Double,val gmp:Double,val kwRel:Double,val upCats:Seq[(String,Double)],val cpCats:Seq[(String,Double)],val uid:String,val content_id:String) extends Serializable

class Statistic (val mean:Double, val std:Double) extends Serializable

class PredPoint (val score:Double, val label:Double, val weight:Double = 1.0) extends Serializable
