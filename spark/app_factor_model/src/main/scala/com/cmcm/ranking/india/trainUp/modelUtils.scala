package com.cmcm.ranking.india.trainUp

import java.io.InputStream

import com.cmcm.ranking.india.featureUtils.utils
import org.apache.spark.ml.classification.{BinaryLogisticRegressionSummary, LogisticRegressionModel}
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.sql.Row
import java.io._
import java.{util => ju}
import scala.io.Source


import org.apache.log4j.LogManager
import org.apache.spark.SparkException
import org.apache.spark.mllib.linalg


/**
 * Created by mengchong on 4/24/16.
 */
object modelUtils extends Serializable{


  lazy val featureList = getFeatureList
  lazy val featMapName2Id = featureList.map(x => (x._2,x._1)).toMap
  lazy val featMapId2Name = featureList.toMap
  lazy val allCats = featureList.map(_._2).filter(_.startsWith("b_cat-")).map(_.replace("b_cat-",""))
  lazy val appCats = getAppCats
  lazy val appCats2Id = appCats.zipWithIndex.toMap

  @transient lazy val log = org.apache.log4j.LogManager.getLogger("myLogger")


  def getAppCatsBinByQuantile(upAppCats: Seq[(String, Double)], appQuantileData: Map[String, Seq[Double]], numBin: Int) = {

    upAppCats.map{
      case(appCat, weight) => {
        //Get split points from percentiles
        val step = appQuantileData(appCat).length/numBin
        val splits = appQuantileData(appCat).toArray.zipWithIndex.filter(_._2 % step == 0).map(_._1)

        //println(splits.toString)

        //get bin index by binary search
        val binIndex = binarySearchForBuckets(splits,weight)
        //val appCatBin = appCat + '-' + binIndex.toInt.toString
        //(appCatBin,binIndex.toInt,1.0)
        (appCat,binIndex.toInt,1.0)
      }
    }//.filter(_._2 >=0).map( x => (x._1, x._3))
  }

  def getAppCatsBin(upAppCats: Seq[(String, Double)],numBin:Int) :Seq[(String, Double)] = {

    val min = 0.0
    val max = 1.0
    val step = (max - min)/numBin.toDouble
    val splits = (min to max by step).toArray

    upAppCats.map{
      case(appCat, weight) => {
        val binIndex = binarySearchForBuckets(splits,weight)
        val appCatBin = appCat + '-' + binIndex.toString
        (appCatBin,1.0)
      }
    }
  }

  def binarySearchForBuckets(splits: Array[Double], feature: Double): Double = {
    if (feature == splits.last) {
      splits.length - 2
    } else {
      val idx = ju.Arrays.binarySearch(splits, feature)
      if (idx >= 0) {
        idx
      } else {
        val insertPos = -idx - 1
        if (insertPos == 0 || insertPos == splits.length) {
          throw new SparkException(s"Feature value $feature out of Bucketizer bounds" +
            s" [${splits.head}, ${splits.last}].  Check your features, or loosen " +
            s"the lower/upper bound constraints.")
        } else {
          insertPos - 1
        }
      }
    }
  }

  def appCats2Vec(upAppCats: Seq[(String, Double)]) :Vector = {

    val appCatSeq = upAppCats.map{
      case(appCat,weight) => {
        (appCats2Id(appCat),weight)
      }
    }
    Vectors.sparse(appCats.length,appCatSeq)
  }


  def getFeatureList: Array[(Int,String)] = {
    val stream : InputStream = getClass.getResourceAsStream("/featmap.txt")
    val lines = scala.io.Source.fromInputStream( stream ).getLines()
    lines.map(x => {
      val fields = x.split('\t')
      val fid = fields(0).toInt
      val fname = fields(1)
      val ftype = fields(2)
      (fid,fname)
    }).toArray
  }

  def getAppCats: Array[String] = {
    val stream : InputStream = getClass.getResourceAsStream("/app_gp_cats.txt")
    val lines = scala.io.Source.fromInputStream( stream ).getLines()
    lines.map(x => x.stripLineEnd).toArray
  }



  def rollupCats(l : Seq[(String,Double)]): Seq[(String,Double)] = {
    val catMap = utils.getCatTree
    l.map( x => (utils.getL1Cat(x._1,catMap),x._2))
      .filter( _._1 != "null" )
      .groupBy(x => x._1)
      .toList
      .map(x => (x._1, x._2.map(_._2).sum))
  }

  def L1_norm(l : Seq[(String,Double)],factor:Double = 0): Seq[(String,Double)] = {
    val sum = l.filter(x => x._2 > 0).map(x => x._2).sum + factor
    if (sum > 0) l.map(x => (x._1,x._2/sum))
    else Seq[(String,Double)]()
  }

  def parseSeqRows(cats:Seq[Row]): Seq[(String,Double)] =
    cats match {
      case a:Any =>  a.filter (x => x != null).map (row => (row.getString (0), row.getDouble (1) ) )
      //case a:Any =>  a.filter (x => x != null).map (row => (row.getString (0), row.getString(1).toDouble ) )

      case null => Nil
    }

  def parsePercentile(percnetiles:Seq[Row]): Seq[(Double)] ={
      val percnetileSeq = percnetiles.map(row => row.getDouble(0))
      percnetileSeq
    }

  def printModelSummary(model:LogisticRegressionModel, skipCoef:Boolean) : LogisticRegressionModel = {
    val param = model.coefficients.toArray
    println("Model was fit using parameters: " + model.parent.extractParamMap)
    if (! skipCoef) {
      println("Model coef : " + model.coefficients.toArray.toList)
    }

    println("GMP weight:" +  model.coefficients.toArray(featMapName2Id("gmp")).toString)

    println("Model inception: " + model.intercept.toString)

    val trainingSummary = model.summary
    val binarySummary = trainingSummary.asInstanceOf[BinaryLogisticRegressionSummary]
    // Obtain the receiver-operating characteristic as a dataframe and areaUnderROC.
    println("train auc:\t" + binarySummary.areaUnderROC)
    model
  }


  def genFeatures (gmp:Double, uCats:Seq[(String,Double)], dCats:Seq[(String,Double)],kwRel:Double, crossBlackList:Set[String] = Set())
    : Vector = {
    val crossFeatures:Seq[(Int,Double)] = genCross(uCats,dCats,crossBlackList)
    val statFeatures = Seq((featMapName2Id("gmp"),gmp),(featMapName2Id("kw_rel"),kwRel)).filter(_._2 !=0)
    Vectors.sparse(featureList.length,statFeatures ++ crossFeatures)
  }

  def genCross (uCats:Seq[(String,Double)], dCats:Seq[(String,Double)], crossBlackList:Set[String] = Set()) : Seq[(Int,Double)] = {
    (for (u <- uCats; c <- dCats) yield{
      val (ucat,uweight) = u
      val (dcat,dweight) = c
      val catName = ucat + "-" + dcat
      val fname = "c_cats-"+ ucat + "-" + dcat
      if (featMapName2Id.contains(fname) && (! crossBlackList.contains(catName))) (featMapName2Id(fname),uweight*dweight)
      else (-1,0.0)
    }).filter(_._1>=0)
  }


  def genBinFeatures(gmp: Double, upAppCatsBin: Seq[(String, Int, Double)], dCats: Seq[(String, Double)], kwRel: Double, featmap:featMap, crossBlackList: Set[String] = Set())
  : Vector = {
    val crossFeatures:Seq[(Int,Double)] = genBinCross(upAppCatsBin,dCats,featmap,crossBlackList)


    //log.info(featmap.binFeatName2Id.size.toString+ "\t" + featmap.binFeatName2Id.toString)
    //log.info(featmap.binFeatName2Id.size.toString)
    log.info("up&cp: " + upAppCatsBin.toString + '\t' + dCats.toString)

    val statFeatures = Seq((featmap.binFeatName2Id("gmp"),gmp),(featmap.binFeatName2Id("kw_rel"),kwRel)).filter(_._2 !=0)
    //log.info(statFeatures.toString)
    log.info("features: " + crossFeatures.toString)

    Vectors.sparse(featmap.binFeatureList.length,statFeatures ++ crossFeatures)
  }

  def genBinCross (upAppCatsBin:Seq[(String,Int,Double)], dCats:Seq[(String,Double)], featmap:featMap, crossBlackList:Set[String] = Set()) : Seq[(Int,Double)] = {
    (for (u <- upAppCatsBin; c <- dCats) yield{
      val (ucat,binIdx, uweight) = u
      val (dcat,dweight) = c

      val catName = ucat + "-" + dcat
      val fname = List("c_cats",ucat,binIdx.toString,dcat).mkString("-")
      if (featmap.binFeatName2Id.contains(fname) && (! crossBlackList.contains(catName))) (featmap.binFeatName2Id(fname),uweight*dweight)
      else (-1,0.0)
    }).filter(_._1>=0)
  }

  def genBinFeatureWithCatRel(gmp: Double, upAppCatsBin: Seq[(String, Int, Double)], dCats: Seq[(String, Double)], coef: Array[Double], featmap: featMap)
  : Vector = {
    val catRel = (for (u <- upAppCatsBin; c <- dCats) yield{
      val (ucat,binIdx,uweight) = u
      val (dcat,dweight) = c
      val fname = List("c_cats",ucat,binIdx.toString,dcat).mkString("-")

      if (featmap.binFeatName2Id.contains(fname)) uweight*dweight*coef(featmap.binFeatName2Id(fname))
      else 0.0
    }).sum

    val statFeatures = Array((featmap.binFeatName2Id("gmp"),gmp),(featmap.binFeatName2Id("cat_rel"),catRel)).filter(_._2 !=0)
    Vectors.sparse(5,statFeatures)
  }


  def getKwRel (ukw:Seq[(String,Double)],dKw:Seq[(String,Double)]):Double = {
    val kwMap = dKw.toMap.withDefaultValue(0.0)
    ukw.map(x => x._2 * kwMap(x._1)).sum
  }

  def genFeatureWithCatRel (gmp:Double,uAppCats:Seq[(String,Double)], dCats:Seq[(String,Double)],coef:Array[Double])
  : Vector = {
    val catRel = (for (u <- uAppCats; c <- dCats) yield{
      val (ucat,uweight) = u
      val (dcat,dweight) = c
      val fname = "c_cats-" + ucat + "-" + dcat
      if (featMapName2Id contains fname) uweight*dweight*coef(featMapName2Id(fname))
      else 0.0
    }).sum
    val statFeatures = Array((featMapName2Id("gmp"),gmp),(featMapName2Id("cat_rel"),catRel)).filter(_._2 !=0)
    Vectors.sparse(5,statFeatures)
  }


  def predictByParam (uAppCats:Seq[(String,Double)], dCats:Seq[(String,Double)],coef:Array[Double], intercept:Double)
  : Double = {
    val catRel = (for (u <- uAppCats; c <- dCats) yield{
      val (ucat,uweight) = u
      val (dcat,dweight) = c
      val fname = "c_cats-" + ucat + "-" + dcat
      if (featMapName2Id contains fname) uweight*dweight*coef(featMapName2Id(fname))
      else 0.0
    }).sum
    val m = (catRel + intercept)
    1.0 / (1.0 + math.exp(-m))
  }


  def printCatCoef (coefs : Array[Double]) = {
    val nameMap = utils.getCatTree.map(x => (x._1,x._1 + "," + x._2._2))
    val catName = allCats.map(cat => if(nameMap contains cat) nameMap(cat) else cat)

    println("\t" + catName.mkString("\t"))
    for (l <- appCats) yield {
      val line = (for (c<- allCats) yield {
        val fname = "c_cats-" + l + "-" + c
        coefs(featMapName2Id(fname)).toString
      }).mkString("\t")
      println(l + "\t" + line)
      line
    }
  }

  def genCoefList (coefs : Array[Double]) : Array[(Int,String,Double)] = {
    coefs.zipWithIndex.map{case (w,idx) =>
      (idx,featMapId2Name(idx),w)
    }
  }


  //Example: 904 c_cats-game_word-1001091    0.0
  def getParamsByCutoff(paramFile:String, cutNum: Int = -1 ): Vector = {


    val params = Source.fromFile(paramFile).getLines.map(
      x => {
        val fields = x.stripLineEnd.split("\t")
        val fname = fields(1).split("-")
        val weight = fields(2).toDouble
        (fname,weight)
      }
    ).filter( x => x._1.length >= 3)
      .map(x => (x._1(1),x._1(2),x._2)).toList.groupBy(_._2)

    //println(params.toString)

    val paramsFiltered = params.map{
      case(dcat, paramList) => {
        val paramSorted = paramList.filter(_._3 != 0).sortBy( _._3 ).reverse

        val paramCutted = if(cutNum > 0) paramSorted.take(cutNum) else paramSorted
        (dcat, paramCutted)
      }
    }

    //println(paramsFiltered.toString)

    val paramArray = paramsFiltered.flatMap{
      case(dcat, paramCutted) => {
        for(param <- paramCutted) yield {
          val (ucat,dcat,weight) = param
          val fname = "c_cats-" + ucat + "-" + dcat
          if (featMapName2Id contains fname) (featMapName2Id(fname),weight)
          else (-1,0.0)
        }
      }
    }.filter(_._1 >= 0).toArray

    Vectors.sparse(featureList.length, paramArray)
  }

  def getClickViewEntropy(data:(String,Seq[(String,Double,Double)])) = {

    val (appCat,cvSeq) = data

    val (dummy, sumClick, sumPV) = cvSeq.reduce((a,b) => (a._1, a._2+b._2, a._3+b._3))

    val cvProb = cvSeq.map{
      case (dCat, clk, pv) => (if(clk == 0) 1/sumClick else clk/sumClick, if(pv==0) 1/sumPV else pv/sumPV)
    }

    val cvEnt = cvProb.foldLeft[(Double,Double)]((0,0))( (acc,value) => {

      val (clk_ent, pv_ent) = acc
      val (clk_prob, pv_prob) = value

      val new_clk_ent =  if (clk_prob == 0) clk_ent else clk_ent + (-clk_prob)*math.log(clk_prob)
      val new_pv_ent =  if (pv_prob == 0) pv_ent else pv_ent+(-pv_prob)*math.log(pv_prob)
      (new_clk_ent, new_pv_ent)
    })

    (appCat, sumClick, sumPV, cvEnt._1, cvEnt._2, cvEnt._1/cvEnt._2)
  }

  /**
    *
    * @param data array of tuple, tuple schema is prob, label, weight
    * @return auc
    */
  def evalAuRoc (data: Array[PredPoint], cutoff:Double ) = {
    if (data.length < cutoff) -1.0
    else {
      val totalPosCnt = data.filter(x => x.label > 0).map(_.weight).sum
      val totalNegCnt = data.map(x => x.weight).sum - totalPosCnt
      println((totalPosCnt,totalNegCnt))
      if (totalPosCnt > 0 && totalNegCnt > 0) {
        val res = data.groupBy(x => x.score)
          .map { case (prob, aggList) =>
            val curPosCnt = aggList.filter(x => x.label > 0).map(_.weight).sum
            val curNegCnt = aggList.map(_.weight).sum - curPosCnt
            (prob, (curPosCnt,curNegCnt))
          }
          .toSeq
          .sortBy(-_._1)
          .map(x => x._2)
          .foldLeft[(Double, Double, Double)]((0.0,0.0,0.0))((accu, value) => {
          val (pCnt, nCnt, auc) = accu
          val (curPCnt, curNCnt) = value
          val newPCnt = pCnt+curPCnt
          val newNCnt = nCnt+curNCnt
          val newAuc = auc + (newNCnt-nCnt)/totalNegCnt*(newPCnt+pCnt)/2/totalPosCnt
          //          println((newPCnt,newNCnt,newPCnt/totalPosCnt, newNCnt/totalNegCnt, newAuc))
          (newPCnt, newNCnt, newAuc)
        })
        res._3
      }
      else -1.0
    }
  }
}
