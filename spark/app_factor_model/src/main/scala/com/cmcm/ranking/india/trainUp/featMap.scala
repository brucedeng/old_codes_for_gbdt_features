package com.cmcm.ranking.india.trainUp

import java.io.{InputStream, Serializable}

/**
  * Created by kehan on 5/20/16.
  */
class featMap (val numBins:Int) extends Serializable {

  @transient lazy val log = org.apache.log4j.LogManager.getLogger("myLogger")

  def init() ={

    log.info("initialize featmap with numBins: " + numBins.toString)

    val stream : InputStream = getClass.getResourceAsStream("/app_gp_cats.txt")
    val lines = scala.io.Source.fromInputStream( stream ).getLines()
    val appCats = lines.map(x => x.stripLineEnd).toArray

    val stream2 : InputStream = getClass.getResourceAsStream("/doc_cats.txt")
    val lines2 = scala.io.Source.fromInputStream( stream2 ).getLines()
    val dCats = lines2.map(x => x.stripLineEnd).toArray


    val staticFeature = Array("dummy","gmp","kw_rel","cat_rel").zipWithIndex


    val crossFeature = (for (ucat <- appCats; bin <- 0 to (this.numBins-1); dcat <- dCats) yield {
      List("c_cats",ucat,bin.toString,dcat).mkString("-")
    }).zipWithIndex
      .map(x => (x._1,x._2+staticFeature.length))

    val biasFeature = (for (dcat <- dCats) yield {
      List("b_cat-",dcat).mkString("-")
    }).zipWithIndex
      .map(x => (x._1,x._2 + staticFeature.length + crossFeature.length))

    val features = staticFeature ++ crossFeature ++ biasFeature
    //val features = staticFeature
    val binFeatureList = features.map(x => (x._2, x._1))
    val binFeatName2Id = features.toMap
    val binId2FeatNAme = binFeatureList.toMap
    (binFeatureList, binFeatName2Id, binId2FeatNAme)
  }

  val (binFeatureList, binFeatName2Id, binId2FeatNAme) = this.init()

}
