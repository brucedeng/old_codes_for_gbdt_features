package com.cmcm.ranking.india.trainUp

import java.io.File

import com.cmcm.ranking.india.featureUtils.utils
//import com.cmcm.ranking.india.trainUp.ioUtils
import com.typesafe.config.ConfigFactory
import org.apache.spark.ml.classification.{LogisticRegression, LogisticRegressionModel}
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Edit by chenkehan on 5/06/16.
 */
object statAppCatEntroy extends Serializable {

  def crossCats(x: (Seq[(String, Double)], Seq[(String, Double)], Double)) = {

    val (upCats, cpCats, label) = x
    for (a <- upCats; b <- cpCats) yield {
      (a._1, b._1, a._2 * b._2, label)
    }
  }


  def main(args: Array[String]) {
    val desc = "eval entropy of profile"
    println(desc)

    val configPath = args(0)

    val config = ConfigFactory.parseFile(new File(configPath))

    val dataPath = config.getString("data")
    //val outputPath = config.getString("output")
    val appName = config.getString("app_name")


    println("Loading data: %s".format(dataPath))

    // initializing context
    val conf = new SparkConf().setAppName(appName)
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._

    sc.broadcast(modelUtils)
    sc.broadcast(statAppCatEntroy)

    // load input data
    val inputData = sqlContext.read.parquet(dataPath)
      .select("uid", "content_id", "label", "dwelltime", "u_app_intall_cats", "d_cats", "d_keywords", "gmp")

    inputData.printSchema()

    // parse dataframe function
    def parseInputDataFrame(data: DataFrame) = {
      data.select("label", "u_app_intall_cats", "d_cats", "d_keywords", "gmp", "uid", "content_id")
        .map(row => {
          //        case Row(label:Int,u_cats, d_cats,u_kws,d_kws,gmp:Double) =>
          val label = row.getInt(0)
          val gmpRaw = row.getFloat(4).toDouble
          val gmp = if (gmpRaw < 0) 0.0 else gmpRaw
          val upAppCats = modelUtils.parseSeqRows(row.getSeq[Row](1))

          val cpCats = modelUtils.L1_norm(modelUtils.rollupCats(modelUtils.parseSeqRows(row.getSeq[Row](2))))

          val uid = row.getString(5)
          val content_id = row.getString(6)
          (label.toDouble, gmp, upAppCats, cpCats, uid, content_id)
        })
    }


    val trainData = parseInputDataFrame(inputData)


    val crossCatSumRDD = trainData.map { case (label, gmp, upAppCats, cpCats, uid, content_id) => (upAppCats, cpCats, label) }
      .flatMap(crossCats)
      .map {
        //case (appCat, dCat, weight, label) => ((appCat, dCat), (weight * label, weight))
        case (appCat, dCat, weight, label) => ((appCat, dCat), (label, 1.0))
      }
      .reduceByKey { case ((a_clk, a_pv), (b_clk, b_pv)) => (a_clk + b_clk, a_pv + b_pv) }
      .map {
        case ((appCat, dCat), (clk, pv)) => (appCat, (dCat, clk, pv))
      }
      .groupByKey()

    //val path = "hdfs://mycluster/tmp/chenkehan/crossCatSumRDD"
    //crossCatSumRDD.saveAsTextFile(path)
    //println("save crossCatSumRDD to" + path)

    val crossCatSum = crossCatSumRDD
      .map( x => (x._1,x._2.toSeq))
      .collect()

    //asInstanceOf[Seq[(String, Seq[(String, Double, Double)])]]
    val catEntroy = crossCatSum.map(x => modelUtils.getClickViewEntropy(x))
    println(catEntroy.size)

    catEntroy.map {
      case x => {
        val res = x.productIterator.mkString("\t")
        println(res)
        0.0
      }
    }
  }
}
