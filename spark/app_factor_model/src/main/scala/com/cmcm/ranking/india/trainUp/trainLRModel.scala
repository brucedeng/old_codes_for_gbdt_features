package com.cmcm.ranking.india.trainUp

import java.io.File

import com.typesafe.config.ConfigFactory
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.ml.classification.{LogisticRegression, LogisticRegressionModel}
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.storage.StorageLevel

/**
  * Created by kehan on 5/20/16.
  */
object trainLRModel extends Serializable{

  def evalModel (testData:DataFrame, model:LogisticRegressionModel, comment:String) = {

    val predictionAndLabels = model.transform(testData)
      .select("features", "label", "probability", "prediction","uid")
      .map { case Row(features: Vector, label: Double, prob: Vector, prediction: Double, uid: String) =>
        (prob(1), label, uid)
      }
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    // calculate test auc
    val metrics = new BinaryClassificationMetrics(predictionAndLabels.map( x => (x._1,x._2)),1000)
    val auROC = metrics.areaUnderROC()
    println(comment + "\t" + "test auc:\t" + auROC)
    predictionAndLabels.unpersist()

    //test per user auc
    val perUserAuc = predictionAndLabels.map{case (prob,label,uid) => (uid,new PredPoint(prob,label))}
      .groupByKey()
      .map{case (uid,probLabel) => modelUtils.evalAuRoc(probLabel.toArray,5)}
      .filter(_ > 0.0)
      .mean()
    println(comment + "\t" + "peruser auc:\t" + perUserAuc)
  }

  def main(args: Array[String]) {
    val configPath = args(0)

    val config = ConfigFactory.parseFile(new File(configPath))

    val elasticNetParam = config.getDouble("elastic_net_param")
    val regParam = config.getDouble("reg_param")
    val outputPath = config.getString("output")
    val modelDir = config.getString("model_dir")
    val localModelFile = config.getString("local_model_file")

    val hadoopConf = new org.apache.hadoop.conf.Configuration()
    val hdfs = org.apache.hadoop.fs.FileSystem.get(new java.net.URI("hdfs://mycluster"), hadoopConf)
    try {
      hdfs.delete(new org.apache.hadoop.fs.Path(modelDir), true)
    } catch {
      case _: Throwable => {}
    }

    val conf = new SparkConf().setAppName("train Appcat LR")
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._


    val trainingRaw = sc.objectFile[(Double,Vector,Double)](outputPath + "/training_data")
    val testingRaw = sc.objectFile[(Double,Vector,Double,String)](outputPath + "/testing_data")

    val training = trainingRaw
      .toDF("label","features","weight")
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    val test = testingRaw.toDF("label","features","weight","uid")
      .persist(StorageLevel.MEMORY_AND_DISK_SER)


    // Create a LogisticRegression instance.  This instance is an Estimator.
    val lr = new LogisticRegression()
    // Print out the parameters, documentation, and any default values.
    //println("LogisticRegression parameters:\n" + lr.explainParams() + "\n")

    // We may set parameters using setter methods.
    lr.setMaxIter(1000)
      .setRegParam(regParam)
      .setElasticNetParam(elasticNetParam)
      .setFitIntercept(true)
      .setWeightCol("weight")
      .setProbabilityCol("probability")
      .setStandardization(false)


    // Now learn a new model using the paramMapCombined parameters.
    // paramMapCombined overrides all parameters set earlier via lr.set* methods.
    val model = lr.fit(training)

    println("saving model to " + modelDir )
    model.save(modelDir)

    training.unpersist()
    // print coefficients
    val param = model.coefficients.toArray
    println("parameter size is : " + param.length.toString)
    println("non-zero parameter count : " + param.filter(_ > 0).length.toString)
    println("paramaters:")
    //modelUtils.printCatCoef(param)
    println("model summary:")
    modelUtils.printModelSummary(model, true)


    evalModel(test, model, "")
    test.unpersist()

    //ioUtils.saveCoef2File(localModelFile,modelUtils.genCoefList(param))
    //checkModelConsistency(testForCheck,model)

  }
}
