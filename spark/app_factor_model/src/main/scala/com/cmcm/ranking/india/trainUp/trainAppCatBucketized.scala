package com.cmcm.ranking.india.trainUp

import java.io.File

import com.cmcm.ranking.india.featureUtils.utils
//import com.cmcm.ranking.india.trainUp.ioUtils
import com.typesafe.config.ConfigFactory
import org.apache.spark.ml.classification.{LogisticRegression, LogisticRegressionModel}
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.ml.feature.QuantileDiscretizer


/**
  * Edit by chenkehan on 5/18/16.
 */
object trainAppCatBucketized extends Serializable{
  val catMap = utils.getCatTree

  def parseRows (x:(Seq[(String,Double)],Double)) = {
    val (cpCats,label) = x

    cpCats.map{ case(category,weight)  => (category, (weight * label, weight * (if (label > 0) 0.0 else 1.0)))}
  }

  def crossCats (x:(Seq[(String,Double)],Seq[(String,Double)])) = {

    val (upCats,cpCats) = x
    for(a <- upCats; b <- cpCats) yield {
      (a._1 + "-" + b._1,1)
    }
  }

  def evalModel (testData:DataFrame, model:LogisticRegressionModel, comment:String) = {

    val predictionAndLabels = model.transform(testData)
      .select("features", "label", "probability", "prediction","uid")
      .map { case Row(features: Vector, label: Double, prob: Vector, prediction: Double, uid: String) =>
        (prob(1), label, uid)
      }
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    // calculate test auc
    //predictionAndLabels.saveAsTextFile(outputPath + "/cat_test")
    val metrics = new BinaryClassificationMetrics(predictionAndLabels.map( x => (x._1,x._2)),1000)
    val auROC = metrics.areaUnderROC()
    println(comment + "\t" + "test auc:\t" + auROC)
    predictionAndLabels.unpersist()

    //test per user auc
    val perUserAuc = predictionAndLabels.map{case (prob,label,uid) => (uid,new PredPoint(prob,label))}
      .groupByKey()
      .map{case (uid,probLabel) => modelUtils.evalAuRoc(probLabel.toArray,5)}
      .filter(_ > 0.0)
      .mean()
    println(comment + "\t" + "peruser auc:\t" + perUserAuc)
  }


  //  def filterRows (x:(Seq[Row],Int)): Boolean = {
  //    if(x._1.isEmpty) false
  //    else true
  //  }

  def main(args: Array[String]) {
    val desc = "train up with 1. only use category cross, weighted to 1:1 \n" +
      "2. use the weighted cat_rel train phase-1 features\n" +
      "3. filtered only events with cp category not empty\n" +
      "4. empty gmp is -1 in training data. must be set to 0 before use\n" +
      "5. added up filter. remove user profile less than upThreshold clicks\n" +
      "6. filter out cubes with less than 100 samples\n" +
      "7. normalize non zero up cats"

    println(desc)
    val configPath = args(0)

    val config = ConfigFactory.parseFile(new File(configPath))

    val normFactor = config.getDouble("norm_factor")
    val upThreshold = config.getDouble("up_threshold")
    val trainSampleThreshold = config.getDouble("train_sample_threshold")
    val elasticNetParam = config.getDouble("elastic_net_param")
    val regParam = config.getDouble("reg_param")
    val dataPath = config.getString("data")
    val appQuantilePath = config.getString("app_quantile")
    val outputPath = config.getString("output")
    val appName = config.getString("app_name")
    val modelDir = config.getString("model_dir")
    val localModelFile = config.getString("local_model_file")
    val numBins = config.getString("num_app_bins").toInt


    println("Loading data: %s".format(dataPath))

    // initializing context
    val conf = new SparkConf().setAppName(appName)
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._

    /*
      Init featmap with app bins
    */
    println("Num bins: " + numBins.toString)

    val featmap = new featMap(numBins)
    for (x <- featmap.binFeatureList) yield {
      println(x.toString)
    }
    val featmapBC = sc.broadcast(featmap)

    val hadoopConf = new org.apache.hadoop.conf.Configuration()
    val hdfs = org.apache.hadoop.fs.FileSystem.get(new java.net.URI("hdfs://mycluster"), hadoopConf)
    try {
      hdfs.delete(new org.apache.hadoop.fs.Path(outputPath), true)
    } catch {
      case _: Throwable => {}
    }
    try {
      hdfs.delete(new org.apache.hadoop.fs.Path(modelDir), true)
    } catch {
      case _: Throwable => {}
    }
    try {
      hdfs.delete(new org.apache.hadoop.fs.Path("hdfs://mycluster/tmp/chenkehan/checkModelConsistency"), true)
    } catch {
      case _: Throwable => {}
    }


    // load input data
    val inputData = sqlContext.read.parquet(dataPath)
      .select("uid", "content_id", "label", "dwelltime", "u_app_intall_cats", "d_cats", "d_keywords", "gmp")
    //      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    inputData.printSchema()
    //    inputData.limit(10).collect().foreach(println(_))

    // parse dataframe function
    def parseInputDataFrame(data: DataFrame) = {
      data.select("label", "u_app_intall_cats", "d_cats", "d_keywords", "gmp", "uid", "content_id")
        .map(row => {
          //        case Row(label:Int,u_cats, d_cats,u_kws,d_kws,gmp:Double) =>
          val label = row.getInt(0)
          val gmpRaw = row.getFloat(4).toDouble
          val gmp = if (gmpRaw < 0) 0.0 else gmpRaw
          val upAppCats = modelUtils.parseSeqRows(row.getSeq[Row](1))

          //val upAppCatsClk = upAppCatsRaw.map(_._2).sum
          //val upCats:Seq[(String,Double)] = if (upCatsClk < upThreshold) Seq() else modelUtils.L1_norm(upCatsRaw,normFactor)
          val cpCats = modelUtils.L1_norm(modelUtils.rollupCats(modelUtils.parseSeqRows(row.getSeq[Row](2))))
          //val cpCats = modelUtils.rollupCats(modelUtils.parseSeqRows(row.getSeq[Row](2)))

          //val cpKws = modelUtils.L1_norm(modelUtils.parseSeqRows(row.getSeq[Row](4)))

          val uid = row.getString(5)
          val content_id = row.getString(6)
          (label.toDouble, gmp, upAppCats, cpCats, uid, content_id)
        })
    }


    val appQuantileDataRaw = sqlContext.read.parquet(appQuantilePath)
    appQuantileDataRaw.printSchema()

    val appQuantileData = appQuantileDataRaw
      .select("catid", "percentiles").map {
      case Row(catid: String, percentiles:Seq[Float]) =>
      {
        val sortedPercentile = percentiles.map(x => x.toDouble).sorted
        (catid,sortedPercentile)
      }
    }.collect().toMap

    val appQuantileDataSC = sc.broadcast(appQuantileData)

    val trainData = parseInputDataFrame(inputData)

    val trainDataBin = trainData.map{
      case (label, gmp, upAppCats, cpCats, uid, content_id) => {
        val upAppCatsBin = modelUtils.getAppCatsBinByQuantile(upAppCats,appQuantileDataSC.value,numBins)
        //(upAppCats,upAppCatsBin)
        (label, gmp, upAppCats,upAppCatsBin, cpCats, uid, content_id)
      }
    }.filter(_._3.nonEmpty)
    //.toDF("upAppCats","upAppCatsBin")
    //trainDataBin.show(5,false);

    /*
    val crossBlackListRdd = trainData.map{case(label, gmp, upAppCats, cpCats, uid, content_id) => (upAppCats,cpCats)}
      .flatMap(crossCats)
      .reduceByKey((a,b) => a+b)
      .filter{  case (cross_key, pv) => pv < 50 }

    println(crossBlackListRdd.count())

    //crossBlackListRdd.saveAsTextFile(outputPath + "/black_list")
    val crossBlackList = crossBlackListRdd
      .collect()
      .map(_._1)
      .toSet

    println("black list is : " + crossBlackList)
    sc.broadcast(crossBlackList)
    */

    //get label and cp category
    val cCatMapRaw = trainDataBin.map{ case(label, gmp, upAppCats, upAppCatsBin, cpCats, uid, content_id) => (cpCats, label)}
      .filter(_._1.nonEmpty)
      .flatMap(parseRows)
      .reduceByKey{ case ((a_pos,a_neg),(b_pos,b_neg)) => (a_pos + b_pos, a_neg + b_neg) }
      .collect()

    println("sample category raw " + cCatMapRaw.toList)

    val catWeights = cCatMapRaw
      .map{ case (cat, np_counts) => (cat,(np_counts._2/np_counts._1,1.0))}
      .toMap
      .withDefaultValue((0.0,0.0))
    val catWeightsBC = sc.broadcast(catWeights)
    println("weights:")
    println(catWeights)


    val splits = trainDataBin.randomSplit(Array(0.8, 0.2), seed = 11L)

    // category training data
    println(featmapBC.value.binFeatName2Id)

    val trainingRaw = splits(0)
      .map{
        case (label:Double,gmp:Double,upAppCats,upAppCatsBin,cpCats,uid:String, content_id:String) => {
          val weightVecList = cpCats.map{ case(cat, weight) => {
            val w = catWeightsBC.value(cat)
            (w._1 * weight, w._2 * weight)
          }}
          val weightVec = if(weightVecList.isEmpty) (0.0,0.0)
          else weightVecList.reduce((a, b) => (a._1 + b._1, a._2 + b._2))
          val wt = if (label > 0) weightVec._1 else weightVec._2
          val valid_wt = if (wt > 10.0) 10.0 else wt
          (label, modelUtils.genBinFeatures(0.0, upAppCatsBin, cpCats, 0.0, featmapBC.value), valid_wt)
        }
      }.filter(x => x._2.numNonzeros != 0)
    //trainingRaw.saveAsTextFile(outputPath + "/training_data")

    trainingRaw.saveAsObjectFile(outputPath + "/training_data")

    val training = trainingRaw
      .toDF("label","features","weight")
      .persist(StorageLevel.MEMORY_AND_DISK_SER)


    // category testing data
    val testingRaw = splits(1)
      .map{
        case (label:Double,gmp:Double,upAppCats,upAppCatsBin,cpCats,uid:String, content_id:String) =>
          (label,modelUtils.genBinFeatures(0.0,upAppCatsBin,cpCats,0.0,featmapBC.value),1.0,uid)
      }
      .filter(x => x._2.numNonzeros != 0 )

    testingRaw.saveAsObjectFile(outputPath + "/testing_data")

    val test = testingRaw.toDF("label","features","weight","uid")
      .persist(StorageLevel.MEMORY_AND_DISK_SER)


    /*
    val testForCheck = splits(1)
      .map{
        case (label:Double,gmp:Double,upAppCats,upAppCatsBin,cpCats,uid:String, content_id:String) =>
          (label,modelUtils.genBinFeatures(0.0,upAppCatsBin,cpCats,0.0),1.0,uid,upAppCatsBin,cpCats)
      }
      .filter(x => x._2.numNonzeros != 0 )
      .toDF("label","features","weight","uid","uAppCats","dCats")
      .persist(StorageLevel.MEMORY_AND_DISK_SER)
     */

    // Create a LogisticRegression instance.  This instance is an Estimator.
    val lr = new LogisticRegression()
    // Print out the parameters, documentation, and any default values.
    //println("LogisticRegression parameters:\n" + lr.explainParams() + "\n")

    // We may set parameters using setter methods.
    lr.setMaxIter(1000)
      .setRegParam(regParam)
      .setElasticNetParam(elasticNetParam)
      .setFitIntercept(true)
      .setWeightCol("weight")
      .setProbabilityCol("probability")
      .setStandardization(false)


    // Now learn a new model using the paramMapCombined parameters.
    // paramMapCombined overrides all parameters set earlier via lr.set* methods.
    val model = lr.fit(training)

    println("saving model to " + modelDir)
    model.save(modelDir)

    training.unpersist()
    // print coefficients
    val param = model.coefficients.toArray
    println("parameter size is : " + param.length.toString)
    println("non-zero parameter count : " + param.filter( _ > 0).length.toString)
    println("paramaters:")
    //modelUtils.printCatCoef(param)
    println("model summary:")
    modelUtils.printModelSummary(model,true)


    evalModel(test,model,"Cat only model")
    test.unpersist()

    //ioUtils.saveCoef2File(localModelFile,modelUtils.genCoefList(param))

    //checkModelConsistency(testForCheck,model)


    /*
    val overallWtRaw = trainData.map(x => (x._1,if(x._1 > 0) 0.0 else 1.0)).reduce((a,b) => (a._1 + b._1,a._2 + b._2))
    val overallWt = (overallWtRaw._2/overallWtRaw._1,1.0)
    println("overall weight is :\t" + overallWt)


    val gmpTraining = splits(0)
      .map{
        case (label:Double,gmp:Double,upAppCats,upAppCatsBin,cpCats,uid:String, content_id:String) =>
          val wtOverall = if(label > 0) overallWt._1 else overallWt._2

          (label,modelUtils.genBinFeatureWithCatRel(gmp,upAppCatsBin,cpCats,param,featmapBC.value),wtOverall)
      }
      //.filter(x => x._2.numNonzeros != 0 )
      .toDF("label","features","weight")
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    // category testing data
    val gmpTest = splits(1)
      .map{
        case (label:Double,gmp:Double,upAppCats,upAppCatsBin,cpCats,uid:String, content_id:String) =>

          (label,modelUtils.genBinFeatureWithCatRel(gmp,upAppCatsBin,cpCats,param,featmapBC.value),1.0,uid)
      }
      //.filter(x => x._2.numNonzeros != 0 )
      .toDF("label","features","weight","uid")
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    val paramMap = ParamMap(lr.regParam -> 0)
                      .put(lr.standardization -> false)

    val model2 = lr.fit(gmpTraining, paramMap)

    println("saving model to " + outputPath + "/overall_model")
    model2.save(outputPath + "/overall_model")
    modelUtils.printModelSummary(model2,false)

    evalModel(gmpTest,model2,"Cat-GMP model")
    gmpTest.unpersist()
    */
  }
}
