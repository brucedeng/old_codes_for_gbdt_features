#!/bin/bash
set -x
startday=$1
endday=$2
num=$3
if [ "a$num" == "a" ] 
then
    num=100
fi
echo $startday
echo $endday
#inputPath="/projects/news/deeplearning/model/training/gen_training_data_10min/hi_test/training_data"
inputPath="/projects/news/deeplearning/model/training/gen_training_data_day/hi_test/training_data"
outputPath="/projects/news/deeplearning/model/training/merge_features/featureid_$endday"
hadoop fs -rmr $outputPath
mergeInputPath="$inputPath/$startday"

for (( cur_day="`date -d \"+1 days $startday\" +%Y%m%d`"; $cur_day <= $endday; cur_day="`date -d \"+1 days $cur_day\" +%Y%m%d`" ))
do
    mergeInputPath="$mergeInputPath,$inputPath/$cur_day"
done
echo $mergeInputPath


export LD_LIBRARY_PATH=$LDLIBRARY_PATH:/usr/lib/hadoop/lib/native

spark-submit \
--class com.cmcm.cmnews.model.app.GenFeatureMapDataApp \
--master yarn-cluster \
--driver-memory 4g \
--executor-memory 3g \
--executor-cores 4 \
--queue deeplearning \
--num-executors 50 \
--conf spark.app.name=hi_gen_feature_map_dk \
gen_feature_map-1.0-SNAPSHOT.jar \
train_input "$mergeInputPath" \
feature_out "$outputPath" \
parallelism $num
