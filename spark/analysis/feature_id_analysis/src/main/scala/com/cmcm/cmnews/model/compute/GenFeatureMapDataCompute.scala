package com.cmcm.cmnews.model.compute

//import com.cmcm.cmnews.model.feature.{CFBFeature, CFBBinaryFeature, ContentFeature, UserFeature, XFBFeatureKey, XFBBinaryFeatureKey}
//import com.cmcm.cmnews.model.processor.{CFBProcessor, CPProcessor, EventProcessor, UPProcessor}

import com.cmcm.cmnews.model.processor._

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{JsonUtil, Parameters, LoggingUtils}
import com.cmcm.cmnews.model.util.Parameters._
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.joda.time.DateTime

//import scala.tools.nsc.transform.patmat.Logic.PropositionalLogic.True
//import scala.tools.nsc.transform.patmat.Logic.PropositionalLogic.True
import scala.util.Try

/**
  * New imported packages
  */

import org.json4s.JsonAST._
import scala.util.control.Breaks._
import scala.util.{Failure, Success, Try}


/**
  * Created by tangdong on 1/5/16.
  */
trait GenFeatureMapDataCompute extends EventBatchCompute {
  this: SparkBatchContext =>

  import Parameters._

  override def compute: Unit = {
    super.compute
    logInfo("Start GenFeatureMapDataCompute")
    //generate event data from log
    val trainDataRdd = getRDD[RDD[String]](constant_train_rdd,this)
    val featurePvClick: RDD[String] = GenFeatureMapProcessor.processFeature(trainDataRdd, this.getBatchContext)
    //val featurePvClickWithId: RDD[String] = featurePvClick.zipWithUniqueId().map(x => x._2 + fieldDelimiter + x._1)
    val featurePvClickWithId: RDD[String] = featurePvClick.zipWithIndex().map(x => (x._2 + 1) + fieldDelimiter + x._1)
    rddContext += (constant_feature_rdd -> featurePvClickWithId)
  }
}
