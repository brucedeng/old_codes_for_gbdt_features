package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.component.BatchComponent
import com.cmcm.cmnews.model.spark.SparkBatchContext

/**
 * Created by tangdong on 27/4/16.
 */
trait EventBatchCompute extends BatchComponent{
  this:SparkBatchContext =>
  def compute = {
    logInfo("Start EventBatchCompute...")
  }
}
