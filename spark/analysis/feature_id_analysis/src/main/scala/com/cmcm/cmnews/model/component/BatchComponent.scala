package com.cmcm.cmnews.model.component

import com.cmcm.cmnews.model.spark.SparkBatchContext
import org.apache.spark.Logging

/**
 * Created by tangdong on 27/4/16.
 */
trait BatchComponent extends Logging{
  def init(): Unit = {
    logInfo("BatchComponent finished init")
  }

  protected def getRDD[T](name: String, sbc: SparkBatchContext): T = sbc.getRddContext.get(name) match{
    case Some(rdd) => rdd.asInstanceOf[T]
    case _ =>
      logError(s"lack of $name in Components")
      throw new RuntimeException(s"lack of $name in Components")
  }

  protected def setRDD(path:String, name:String, sbc: SparkBatchContext) = {
    sbc.getBatchContext.get(path) match {
      case Some(p) =>
        val rdd = sbc.getSparkContext.textFile(p)
        sbc.setRddContext(name,rdd)
        val rddContextTmp = sbc.getRddContext
        val keys = rddContextTmp.keys.mkString(",")
        logInfo(s"sbc rddcontext info $keys")
        logInfo(s"Finish set $path to $name RDD with path $p")
      case _ =>
        logError(s"lack of $path!")
        throw new RuntimeException(s"lack of $path!")
    }
  }
  protected def getParam[T](name:String, sbc:SparkBatchContext): T = sbc.getBatchContext.get(name) match{
    case Some(value) => value.asInstanceOf[T]
    case _ =>
      logInfo(s"lack of $name in Components")
      throw new RuntimeException(s"lack of $name in Components")
  }
}
