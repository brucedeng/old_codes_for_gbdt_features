package com.cmcm.cmnews.model.input

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters

/**
 * Created by tangdong on 3/5/16.
 */
trait GenFeatureMapDataInput extends EventBatchInput{
  this: SparkBatchContext =>
  import Parameters._

  override def getInput(): Unit ={
    logInfo("start GenFeatureMapDataInput...")
    setRDD(constant_train_input,constant_train_rdd, this)
  }

}
