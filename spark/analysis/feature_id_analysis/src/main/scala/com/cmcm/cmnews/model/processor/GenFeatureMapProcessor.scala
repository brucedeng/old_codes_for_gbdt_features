package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{LoggingUtils, Parameters}
import org.apache.spark.rdd.RDD

/**
 * Created by tangdong on 27/4/16.
 */
object GenFeatureMapProcessor extends Processor {
  this: SparkBatchContext =>

  import Parameters._

  def processFeature(inputRDD: RDD[String], batchContext: collection.mutable.Map[String, String]) = {
    LoggingUtils.loggingInfo(s"processFeature:")
    val afterProcessRDD = inputRDD.mapPartitions { x => {
      var result = scala.collection.mutable.Map[String, (Int, Int)]()
      while (x.hasNext) {
        val line = x.next()
        preprocessFeature(line, batchContext, result)
      }
      result.toList.toIterator
    }
    }.reduceByKey((x, y) => (x._1 + y._1, x._2 + y._2)).map(x => x._1 + fieldDelimiter + x._2._1 + fieldDelimiter + x._2._2)
    afterProcessRDD
  }

  def preprocessFeature(line: String, batchContext: collection.mutable.Map[String, String], rMap: scala.collection.mutable.Map[String, (Int, Int)]) = {
    val items = line.split(fieldDelimiter)
    try {
      val lable = items(2).split(":")(0).toInt
      if (1 == lable || 0 == lable) {
        val features = items(3).split(" ")
        for (feature <- features) {
          if (rMap.contains(feature)) {
            rMap(feature) = (rMap(feature)._1 + 1 - lable, rMap(feature)._2 + lable)
          } else {
            rMap(feature) = (1 - lable, lable)
          }
        }
      } else {
      }
    } catch {
      case e: Exception => LoggingUtils.loggingError(e.toString + line)
    }
  }
}
