package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.GenFeatureMapDataCompute
import com.cmcm.cmnews.model.input.GenFeatureMapDataInput
import com.cmcm.cmnews.model.output.GenFeatureMapDataOutput
import com.cmcm.cmnews.model.spark.GenFeatureMapDataSparkContext

/**
 * Created by tangdong on 3/5/16.
 */
object GenFeatureMapDataApp extends BatchApp
with GenFeatureMapDataSparkContext
with GenFeatureMapDataInput
with GenFeatureMapDataCompute
with GenFeatureMapDataOutput
