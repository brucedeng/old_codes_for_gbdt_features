package com.cmcm.cmnews.model.output

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters
import com.cmcm.cmnews.model.util.Parameters._
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.rdd.RDD


/**
 * Created by tangdong on 1/5/16.
 */
trait GenFeatureMapDataOutput extends EventBatchOutput{
  this:SparkBatchContext =>
  import Parameters._

  override def outPut: Unit ={
    super.outPut
    logInfo("Start XfbTrainDataOutput...")
    val genFeatureOut = rddContext(constant_feature_rdd).asInstanceOf[RDD[String]]
    //genFeatureOut.saveAsTextFile(batchContext(constant_feature_out),classOf[GzipCodec])
    genFeatureOut.saveAsTextFile(batchContext(constant_feature_out))
  }
}
