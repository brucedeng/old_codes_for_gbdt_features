package com.cmcm.cmnews.model.spark

import com.cmcm.cmnews.model.util.Parameters
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.immutable.HashMap
import scala.util.Try

/**
 * Created by tangdong on 1/5/16.
 */
trait GenFeatureMapDataSparkContext extends SparkBatchContext{
  import Parameters._

  override def init(): Unit ={
    super.init()
    logInfo("Start GenFeatureMapDataSparkContext...")
    val sparkConf = new SparkConf()
      .registerKryoClasses(Array(
        classOf[HashMap[String, Int]]))
      .set("spark.serializer","org.apache.spark.serializer.KryoSerializer")
      .set("spark.default.parallelism", Try(batchContext(constant_parallelism)).getOrElse("32"))
      //.setAppName(Try(batchContext(constant_jobName)).getOrElse(this.getClass.getName))
    sbc = new SparkContext(sparkConf)
  }

}
