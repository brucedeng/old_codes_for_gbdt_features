package com.cmcm.cmnews.model.spark

import com.cmcm.cmnews.model.component.StreamingComponent
import org.apache.spark.SparkContext
import org.apache.spark._
import org.apache.spark.streaming._


/**
 * Created by tangdong on 27/4/16.
 */
trait SparkStreamingContext extends StreamingComponent{
  @transient protected var ssc:StreamingContext = _
  @transient protected var sbc:SparkContext = _
  protected val rddContext:scala.collection.mutable.Map[String, Any] = scala.collection.mutable.Map[String, Any]()
  protected val batchContext:scala.collection.mutable.Map[String,String] = scala.collection.mutable.Map[String, String]()

  override def init() = {
    super.init()
    logInfo("SparkStreamingContext finished init ...")
  }

  def getSparkStreamingContext : StreamingContext = ssc

  def setSparkStreamingContext(_ssc:StreamingContext) = {
    ssc = _ssc
  }

  def getSparkContext : SparkContext = sbc

  def setSparkContext(_ssc:SparkContext) = {
    sbc = _ssc
  }

  def getRddContext = rddContext
  def setRddContext(key:String, value:Any) = rddContext += (key -> value)
  def getBatchContext = batchContext
  def setBatchContext(key:String, value:String) = batchContext += (key -> value)
}
