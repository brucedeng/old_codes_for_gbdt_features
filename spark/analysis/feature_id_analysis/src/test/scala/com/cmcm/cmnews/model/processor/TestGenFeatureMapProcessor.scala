package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.util.Parameters._
import org.joda.time.DateTime
import org.scalatest.FunSuite

/**
  * Created by mengchong on 6/24/16.
  */

class TestSourceEventProcessor extends FunSuite{

  test("parse traindata line") {
    val trainDate =
      "8d688d4fe6f81b48\tZR254407dcg_in\t1:1.0\tB22_1001110 B21_पुरुषों B70_मैच_1000604"
    var result = scala.collection.mutable.Map[String,(Int,Int)]()
    val parsed = GenFeatureMapProcessor.preprocessFeature(trainDate, collection.mutable.Map[String, String](constant_event_pid -> "11", constant_event_lan_region -> "hi_,hi_in", constant_wnd -> "600"), result)
    println(result)
    //assert(parsed == ("906d025c672f3193\003YD202f98c2j_in\00342efb16b","906d025c672f3193\003hi_IN\003YD202f98c2j_in\0031470816047\0031470815400\00311\00335_Bhopal\00342efb16b\0031"))
  }
}

