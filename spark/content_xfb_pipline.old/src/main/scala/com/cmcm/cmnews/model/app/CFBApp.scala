package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.CFBAppCompute
import com.cmcm.cmnews.model.input.XFBAppInput
import com.cmcm.cmnews.model.output.XFBAppOutput
import com.cmcm.cmnews.model.spark.XFBAppSparkContext

/**
  * Created by tangdong on 13/4/16.
  */
object CFBApp extends BatchApp
with XFBAppSparkContext
with XFBAppInput
with CFBAppCompute
with XFBAppOutput
