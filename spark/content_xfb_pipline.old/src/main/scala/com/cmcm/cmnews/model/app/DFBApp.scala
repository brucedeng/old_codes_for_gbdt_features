package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.{DFBAppCompute, CFBAppCompute}
import com.cmcm.cmnews.model.input.XFBAppInput
import com.cmcm.cmnews.model.output.XFBAppOutput
import com.cmcm.cmnews.model.spark.XFBAppSparkContext

/**
 * Created by tangdong on 26/4/16.
 */
object DFBApp extends BatchApp
with XFBAppSparkContext
with XFBAppInput
with DFBAppCompute
with XFBAppOutput