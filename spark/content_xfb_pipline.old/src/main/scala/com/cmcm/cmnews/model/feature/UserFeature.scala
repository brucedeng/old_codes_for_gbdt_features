package com.cmcm.cmnews.model.feature

import com.cmcm.cmnews.model.util.{LoggingUtils, Parameters}
import org.apache.spark.Logging
import org.apache.spark.rdd.RDD

/**
 * Created by tangdong on 13/4/16.
 */
object UserFeature extends Logging{
  import Parameters._

  /*
  * @return (cid, uid+eventFeature+upFeaturesStr)
  * */
  def joinWithBatchEvent(userFeatureRdd:RDD[(String, String)], eventRdd: RDD[(String, String)]) = {
    logInfo("Now, join user profile and event")
    eventRdd.filter(line => line._1.split(fieldDelimiter).length >=2)
      .map((line:(String, String)) => {
      val items = line._1.split(fieldDelimiter)
      val uuid = items(0)
      val cid = items(1)
      (uuid, (cid, line._2))
    }).leftOuterJoin(userFeatureRdd).map(result => {
      val cid = result._2._1._1
      val eventFeatures = result._2._1._2
      val upFeatures = result._2._2
      val uid = result._1
      val defaultValue = "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown"
      val upFeaturesStr = upFeatures.getOrElse(defaultValue)
      val afterFields = uid + fieldDelimiter + eventFeatures + fieldDelimiter + upFeaturesStr
      (cid, afterFields)
    })
  }

}
