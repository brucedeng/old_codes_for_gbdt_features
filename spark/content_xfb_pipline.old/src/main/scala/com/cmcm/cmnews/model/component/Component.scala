package com.cmcm.cmnews.model.component

import com.cmcm.cmnews.model.config.Config
import org.apache.spark.Logging


/**
 * Created by tangdong on 13/4/16.
 */
trait Component extends Logging{
  this: Config =>
  def init(): Unit = {
    logInfo("Component finished init")
  }
}
