package com.cmcm.cmnews.model.config

import java.io.File

import com.typesafe.config.ConfigFactory

/**
 * Created by tangdong on 13/4/16.
 */
class Config(val confFile:String) {
  val typesafeConfig = if(confFile.nonEmpty) ConfigFactory.parseFile(new File(confFile))
  else ConfigFactory.load()
}
