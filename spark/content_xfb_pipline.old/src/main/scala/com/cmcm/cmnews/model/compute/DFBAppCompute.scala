package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.config.Config
import com.cmcm.cmnews.model.feature.{CFBFeature, ContentFeature, DFBFeature, UserFeature}
import com.cmcm.cmnews.model.processor.{CPProcessor, EventProcessor, UPProcessor}
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{LoggingUtils, Parameters}
import org.apache.spark.rdd.RDD
import org.joda.time.DateTime

/**
 * Created by tangdong on 26/4/16.
 */
trait DFBAppCompute extends EventBatchCompute{
  this: SparkBatchContext =>
  import Parameters._

  override def compute: Unit = {
    super.compute
    logInfo("Start DFBAppCompute")
    //get rdd from path
    val eventRdd = getRDD[RDD[String]](constant_event_rdd,this)
    val cpRdd = getRDD[RDD[String]](constant_content_rdd, this)
    val upRdd = getRDD[RDD[String]](constant_user_rdd, this)

    //get parsed rdd
    val parsedEventRdd: RDD[(String, String)] = EventProcessor.process(eventRdd, this.getBatchContext)
    val parsedCpRdd: RDD[(String, String)] = CPProcessor.process(cpRdd, this.getBatchContext)
    val parsedUpRdd: RDD[(String, String)] = UPProcessor.process(upRdd, this.getBatchContext)

    //join rdd
    val eventJoinUpRdd = UserFeature.joinWithBatchEvent(parsedUpRdd, parsedEventRdd)
    val eventJoinUpAndCpRdd = ContentFeature.joinWithBatchEvent(parsedCpRdd, eventJoinUpRdd)

    val runDateTime = batchContext(constant_date_time)
    val timeStamp = try { new DateTime(runDateTime).getMillis/1000 } catch {
      case e:Exception => 0
    }
    val typesafeConfig = new Config(batchContext(constant_config_file)).typesafeConfig


    val dfbFeaturesRdd: RDD[(String,(Double, Double, Double))] = DFBFeature.generateFullDFBFeatures(eventJoinUpAndCpRdd, timeStamp, typesafeConfig)
    val dfbFeatureResultReduce = dfbFeaturesRdd.reduceByKey((x,y) => {
      val click = x._1 + y._1
      val view = x._2 + y._2
      val dwell = x._3 + y._3
      (click, view, dwell)
    })

    val dfbFeatureResult = dfbFeatureResultReduce.map(x => {
      val key = x._1.split(fieldDelimiter).mkString("\t")
      val decayClick = x._2._1
      val decayView = x._2._2
      val decayDwell = x._2._3
      s"$key\t$decayClick\t$decayView\t$decayDwell"
    })
    rddContext += (constant_xfb_rdd -> dfbFeatureResult)
  }
}
