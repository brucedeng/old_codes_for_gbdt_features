package com.cmcm.cmnews.model.feature

import com.cmcm.cmnews.model.config.Config
import com.cmcm.cmnews.model.feature.CFBFeature._
import com.cmcm.cmnews.model.util.Parameters
import com.cmcm.cmnews.model.util.Parameters._
import org.apache.spark.Logging
import org.apache.spark.rdd.RDD

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.Try

/**
 * Created by tangdong on 26/4/16.
 */
object DFBFeature extends Logging {
  import Parameters._
  def generateFullDFBFeatures(fullFeature: RDD[(String, String)], timeStamp:Long, typesafeConfig:com.typesafe.config.Config) : RDD[(String, (Double, Double, Double))] = {
    fullFeature.flatMap(feature => generateDFBFeatures(feature, timeStamp, typesafeConfig))
  }

  def getMatchDFBFeatures(dfbFeatures: ListBuffer[(String, (Double, Double, Double))], featureConfig: String, featureMap: mutable.HashMap[String, List[(String, Double)]], tuple: (String,String, Double, Double, Double)) = {
    val ts = tuple._1
    val pid = tuple._2
    val view = tuple._3
    val click = tuple._4
    val dwell = tuple._5
    for (feature <- featureConfig.split(",")) {
      val fields = feature.split("_")
      val preFeature = fields(1) + "_" + fields(3)
      val posFeature = fields(2) + "_" + fields(4)
      for (value1 <- featureMap.get(preFeature).getOrElse(List())) {
        for (value2 <- featureMap.get(posFeature).getOrElse(List())) {
          if (value1._1.equals(value2._1)) {
            dfbFeatures += ((pid + fieldDelimiter + ts + fieldDelimiter + feature + fieldDelimiter + value1._1 + pairDelimiter + value2._1, (click * value1._2 * value2._2, view * value1._2 * value2._2, dwell * value1._2 * value2._2)))
          }
        }
      }
    }
  }

  def getFeatureValueRecursive(featureMap:mutable.HashMap[String, List[(String, Double)]], featureList: List[String]) : List[(String, Double)] = {
    var featureValues = new ListBuffer[(String, Double)]
    if (featureList.size > 1) {
      val preFeatures = featureMap.get(featureList(0)).getOrElse(List())
      val newFeatureList = featureList.drop(1)
      for (preFeature <- preFeatures) {
        for (posFeature <- getFeatureValueRecursive(featureMap, newFeatureList)) {
          featureValues += ((preFeature._1 + pairDelimiter + posFeature._1, preFeature._2 * posFeature._2))
        }
      }
    } else {
      for (preFeature <- featureMap.get(featureList(0)).getOrElse(List())){
        featureValues += (preFeature)
      }
    }
    featureValues.toList
  }

  def getCrossDFBFeatures(dfbFeatures: ListBuffer[(String, (Double, Double, Double))], featureConfig: String, featureMap: mutable.HashMap[String, List[(String, Double)]], tuple: (String,String, Double, Double, Double)) = {
    val ts = tuple._1
    val pid = tuple._2
    val view = tuple._3
    val click = tuple._4
    val dwell = tuple._5
    for(feature <- featureConfig.split(",")){
      var featureList = new ListBuffer[String]
      val fields = feature.split("_")
      var i = 1
      while(i <= (fields.length/2)){
        featureList += (fields(i) + "_" + fields(i+2))
        i = i + 1
      }
      for (value <- getFeatureValueRecursive(featureMap, featureList.toList)) {
        dfbFeatures += ((pid + fieldDelimiter + ts + fieldDelimiter + feature + fieldDelimiter + value._1, (click * value._2, view * value._2, dwell * value._2)))
      }
    }
  }
  def getSingleEdgeDFBFeatures(cfbFeatures: ListBuffer[(String, (Double, Double, Double))], featureConfig: String, featureMap: mutable.HashMap[String, List[(String, Double)]], tuple: (String,String, Double, Double, Double)) = {
    val ts = tuple._1
    val pid = tuple._2
    val view = tuple._3
    val click = tuple._4
    val dwell = tuple._5
    for (feature <- featureConfig.split(",")){
      val values = featureMap.get(feature).getOrElse(List())
      for (value <- values) {
        cfbFeatures += ((pid + fieldDelimiter + ts + fieldDelimiter + feature + fieldDelimiter + value._1, (click * value._2, view * value._2, dwell * value._2)))
      }
    }
  }

  def generateDFBFeatures(feature:(String,String), timeStamp:Long, typesafeConfig:com.typesafe.config.Config) = {
    val featureMap = new mutable.HashMap[String, List[(String, Double)]]
    val dCid = feature._1
    val items = feature._2.split(fieldDelimiter)
    val uid = items(0)
    val ts = items(1)
    val pid = items(2)
    val event_type = items(3)
    val uCity = items(4)
    val score = items(5).toDouble
    val click: Double = if (event_type == "click") score else 0.0
    val view: Double = if (event_type == "view") score else 0.0
    val dwell: Double = if (event_type == "dwelltime") score else 0.0
    val uCat = items(6)
    val uKey = items(7)
    val uGender = items(8)
    val uAge = items(9)
    val dGroupid = items(10)
    val dTier = items(11)
    val dPublisher = items(12)
    val dPublishTime = items(14).toLong
    val dCat = items(15)
    val dKey = items(16)

    if (uid != "" && uid != "unknown" && uid.size > 0) {
      featureMap.put("U_UID", List((uid, 1.0)))
    }
    if (uCity != "" && uCity != "unknown" && uCity.size > 0) {
      featureMap.put("U_CITY", List((uCity, 1.0)))
    }
    if (uGender != "" && uGender != "unknown" && uGender.size > 0) {
      featureMap.put("U_GENDER", List((uGender, 1.0)))
    }
    if (uAge != "" && uAge != "unknown" && uAge.size > 0) {
      featureMap.put("U_AGE", List((uAge, 1.0)))
    }
    if (uCat != "" && uCat != "unknown" && uCat.size > 0) {
      featureMap.put("U_CATEGORY", uCat.split(pairDelimiter).map(record => {
        val nameAndWeight = record.split(keyValueDelimiter)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }
    if (uKey != "" && uKey != "unknown" && uKey.size > 0) {
      featureMap.put("U_KEYWORD", uKey.split(pairDelimiter).map(record => {
        val nameAndWeight = record.split(keyValueDelimiter)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }

    val expiredTime: Long = Try(typesafeConfig.getLong(constant_expired_time)).getOrElse(172800L)
    if (dGroupid != "" && dGroupid != "unknown" && dGroupid.size > 0) {
      featureMap.put("D_GROUPID", List((dGroupid, 1.0)))
    }
    if (dTier != "" && dTier != "unknown" && dTier.size > 0) {
      featureMap.put("D_TIER", List((dTier, 1.0)))
    }
    if (dPublisher != "" && dPublisher != "unknown" && dPublisher.size > 0) {
      featureMap.put("D_PUBLISHER", List((dPublisher, 1.0)))
    }
    if (dCat != "" && dCat != "unknown" && dCat.size > 0) {
      featureMap.put("D_CATEGORY", dCat.split(pairDelimiter).map(record => {
        val nameAndWeight = record.split(keyValueDelimiter)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }
    if (dKey != "" && dKey != "unknown" && dKey.size > 0) {
      featureMap.put("D_KEYWORD", dKey.split(pairDelimiter).map(record => {
        val nameAndWeight = record.split(keyValueDelimiter)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }
    val docAge = timeStamp - dPublishTime
    if (dCid != "" && dCid != "unknown" && dCid.size > 0) {
      if (docAge > expiredTime) {
        featureMap.put("D_CONID", List())
      } else {
        featureMap.put("D_CONID", List((dCid, 1.0)))
      }
    } else {
      featureMap.put("D_CONID", List())
    }

    val dfbFeatures = new ListBuffer[(String, (Double, Double, Double))]

    val userFeatureConfig = typesafeConfig.getString("cfb.uf")
    val docFeatureConfig = typesafeConfig.getString("cfb.df")
    val crossFeatureConfig = typesafeConfig.getString("cfb.cf")
    val matchFeatureConfig = typesafeConfig.getString("cfb.mf")

    if (userFeatureConfig.size > 0) getSingleEdgeDFBFeatures(dfbFeatures, userFeatureConfig, featureMap,(ts, pid, view, click, dwell))
    if (docFeatureConfig.size > 0) getSingleEdgeDFBFeatures(dfbFeatures, docFeatureConfig, featureMap,(ts, pid, view, click, dwell))
    if (crossFeatureConfig.size > 0) getCrossDFBFeatures(dfbFeatures, crossFeatureConfig, featureMap, (ts, pid, view,click, dwell))
    if (matchFeatureConfig.size > 0) getMatchDFBFeatures(dfbFeatures, matchFeatureConfig, featureMap, (ts, pid, view, click, dwell))
    dfbFeatures.toList

  }
}
