package com.cmcm.ranking.fallback.utils

import scalaj.http.Http
import java.net._
/**
  * Created by mengchong on 9/1/16.
  */
class Monitor (val ts:Long = 0, val endpoint:String = "", val counterType:String = "GAUGE", val step:Int = 600, val retry:Int = 2) {
  val host = "http://localhost:1988/v1/push"

  def writeFalconWithTag(data:Seq[(String,Float,String)]) = {
    val timestamp = if(ts == 0) System.currentTimeMillis / 1000 else ts
    val endpointvalid = if(endpoint.isEmpty) InetAddress.getLocalHost.getHostName else endpoint

    val target = JsonUtil.toJson( data.map{case (metric,value,tag) => Map("endpoint" -> endpointvalid,
      "metric" -> metric,
      "timestamp" -> timestamp,
      "step" -> step,
      "value" -> value,
      "counterType" -> counterType,
      "tags" -> tag)} )
    println(target)
    sendPost(host,target)
  }

  def sendPost(host:String,data:String) = {
    var retryCnt = retry
    var success = false

    while(retryCnt > 0 && !success ){
      val response = HttpRequest.sendPost(host,data)
      if(response == 200) success = true
      retryCnt -= 1
    }
    success
  }
}
