package com.cmcm.ranking.fallback.parser

import com.cmcm.ranking.fallback.types.Gmp

/**
  * Created by mengchong on 8/27/16.
  */
object GMPParser {
  def parse(gmpLine:String):Gmp = {
    val gmpfields = gmpLine.split("\t")
    if(gmpfields.length < 10 || gmpfields(7) == "nw") null
    else new Gmp(gmpfields(0),gmpfields(1).toInt,gmpfields(2).toFloat,gmpfields(3).toFloat,gmpfields(4).toFloat,gmpfields(5).toFloat,gmpfields(6).toFloat,gmpfields(7),gmpfields(8), gmpfields(9).toLong)
  }
}
