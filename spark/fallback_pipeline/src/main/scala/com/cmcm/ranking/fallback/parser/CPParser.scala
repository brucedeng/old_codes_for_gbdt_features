package com.cmcm.ranking.fallback.parser

import com.cmcm.ranking.fallback.types.Content
import com.cmcm.ranking.fallback.utils.JsonUtil
import org.json4s.JsonAST._

import scala.util.control.Breaks._
import scala.util.{Failure, Success, Try}

/**
  * Created by mengchong on 8/27/16.
  */
object CPParser {
  def parse(line:String, categoryLimit:Int, kwLimit:Int):Content = {

    Try(JsonUtil.convertToJValue(line)) match{
      case Success(jvalue) =>
        new Content(extractStr(jvalue,"item_id"),extractLong(jvalue,"publish_time"),extractLong(jvalue,"update_time"),
          extractCatKw(jvalue,"categories",categoryLimit),extractCatKw(jvalue,"entities",kwLimit),extractList(jvalue,"channel_ids"),
          extractStr(jvalue,"title_md5"),extractStr(jvalue,"content_md5"),extractStr(jvalue,"image_md5"),extractStr(jvalue,"group_id"),
          extractBoolean(jvalue,"has_copyright",false),extractDouble(jvalue,"newsy_score"),
          extractList(jvalue,"images").length,extractInt(jvalue,"word_count"),if(extractStr(jvalue,"head_image").isEmpty) 1 else 0,
          extractStr(jvalue,"source_type"),extractStr(jvalue,"display_type"),extractStr(jvalue,"type"),
          extractStr(jvalue,"language"),extractStr(jvalue,"region"),
          extractBoolean(jvalue,"is_servable",false),extractBoolean(jvalue,"cp_disable",true),extractLong(jvalue,"index_flag"),
          extractInt(jvalue, "force_distribute"), extractInt(jvalue, "media_level"))

      case Failure(ex) =>
        null
    }
  }

  def extractStr(jvalue: JValue, fieldName: String, default: String = "") = {
    jvalue \ fieldName match {
      case JString(s) => if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString()
      case _ => default
    }
  }

  def extractLong(jvalue: JValue, fieldName: String, default: Long = 0):Long = {
    jvalue \ fieldName match {
      case JString(s) => Try(s.toLong).getOrElse(default)
      case JInt(s) => s.toLong
      case _ => default
    }
  }

  def extractInt(jvalue: JValue, fieldName: String, default: Int = 0):Int = {
    jvalue \ fieldName match {
      case JString(s) => Try(s.toInt).getOrElse(default)
      case JInt(s) => s.toInt
      case _ => default
    }
  }

  def extractDouble(jvalue: JValue, fieldName: String, default: Double = 0):Double = {
    jvalue \ fieldName match {
      case JString(s) => Try(s.toDouble).getOrElse(default)
      case JDouble(s) => s
      case _ => default
    }
  }

  def extractBoolean(jvalue: JValue, fieldName: String, default: Boolean):Boolean = {
    jvalue \ fieldName match {
      case JString(s) => val s1 = s.toLowerCase
        s1 =="true" || s1 == "yes"
      case JInt(s) => s!=0
      case JBool(s) => s
      case _ => default
    }
  }

  def extractCatKw(jvalue: JValue, fieldName: String, limit:Int):List[(String,Double)] = {
    jvalue \ fieldName match {
      case JArray(s) => s.take(limit).map(line => {
        val name = extractStr(line,"name")
        val wt = extractDouble(line,"weight")
        (name,wt)
      })
      case _ => List[(String,Double)]()
    }
  }

  def extractList(jvalue: JValue, fieldName: String):List[String] = {
    jvalue \ fieldName match {
      case JArray(s) => s.map {
        case JInt(v) => v.toString()
        case JString(v) => v
        case line => line.toString
      }
      case _ => List[String]()
    }
  }
}
