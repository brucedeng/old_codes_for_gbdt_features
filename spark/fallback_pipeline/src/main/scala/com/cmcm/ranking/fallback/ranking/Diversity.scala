package com.cmcm.ranking.fallback.ranking

import com.cmcm.ranking.fallback.types.Content
import scala.collection.mutable.{ArrayBuffer,Set}

/**
  * Created by mengchong on 8/28/16.
  */
object Diversity {
  def process(contents: Seq[Content],configs: Map[String,String]) = {
    val sorted = contents.sortBy(x => -x.score)
    val divWnd = configs.getOrElse("diversity_wnd","6").toInt
    val catDivRatio = configs.getOrElse("category_diversity_rate","0").toDouble
    val kwDivRatio = configs.getOrElse("keyword_diversity_rate","0").toDouble
    val result = ArrayBuffer.empty[Content]
    val prevCat = Set.empty[String]
    val prevKw = Set.empty[String]
    result ++= sorted
    for(idx <- 1 until result.length) {
      var curScore = -0.1
      for(prevIdx <- Math.max(0,idx-divWnd) until idx) {
        prevCat ++= result(prevIdx).categories.map(x => x._1)
        prevKw ++= result(prevIdx).keywords.map(x => x._1)
      }
      var remainIdx = idx
      var targetIdx = remainIdx
      while(remainIdx<result.length && result(remainIdx).score>=curScore ) {
        var newScore = result(remainIdx).score
        if(result(remainIdx).categories.map(x=>x._1).exists(x => prevCat.contains(x))) {
          newScore *= catDivRatio
        }
        if(result(remainIdx).keywords.map(x=>x._1).exists(x => prevKw.contains(x))) {
          newScore *= kwDivRatio
        }
        if(newScore > curScore) {
          curScore = newScore
          targetIdx = remainIdx
        }
        remainIdx += 1
      }
      val tmpVal = result(targetIdx)
      for(remainIdx <- targetIdx until idx by -1){
        result(targetIdx) = result(targetIdx-1)
      }
      result(idx) = tmpVal
    }
    result
  }
}
