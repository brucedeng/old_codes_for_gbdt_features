package com.cmcm.ranking.fallback.utils;

import com.sun.org.apache.xpath.internal.operations.Bool;
import redis.clients.jedis.*;
import java.util.Map;

/**
 * Created by mengchong on 8/31/16.
 */
public class RedisWriter {
    private Jedis redis;
    private Pipeline pipeline;
    public RedisWriter(String host, Integer port, Integer db) {
        redis = new Jedis(host,port);
        redis.select(db);
        pipeline = redis.pipelined();
    }

    public Integer resetData( String key, Map<String,Double> data) {
        try {
            pipeline.multi();
            pipeline.del(key);
            pipeline.zadd(key, data);
            pipeline.exec();
            return data.size();
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    public Integer setKey( String key, String value) {
        try {
            redis.set(key, value);
            //pipeline.multi();
            //pipeline.del(key);
            //pipeline.set(key,value);
            //pipeline.exec();
            return 1;
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    public void disconnect(){
        redis.disconnect();
    }
}
