package com.cmcm.ranking.fallback.types

/**
  * Created by mengchong on 8/27/16.
  */
class Content (val item_id:String, val publish_time:Long, val update_time:Long, val categories:List[(String,Double)],
               val keywords:List[(String,Double)] , val channels:List[String],
               val title_md5:String, val content_md5:String, val image_md5:String, val groupid:String,
               val has_copyright:Boolean, val newsy_score:Double,
               val images_cnt:Int, val word_count:Int, val head_image:Int,
               val source_type:String, val display_type:String, val tp:String,
               val language:String, val region:String,
               val is_servable:Boolean, val cp_disable:Boolean, val indexFlag: Long,
               val force_distribute:Int, val media_level:Int,
               var gmp:Double = 0.0, var score:Double = 0.0) extends Serializable{
  override def hashCode() = {
    item_id.hashCode
  }

  override def equals(c:Any) = {
    c match {
      case v:Content => v.item_id == item_id
      case _ => false
    }
  }

  override def toString = s"$item_id\t$tp\t$language\t$region\t$publish_time\t$categories\t$keywords\t$channels\t$gmp\t$score"
}
