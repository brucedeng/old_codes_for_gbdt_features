package com.cmcm.ranking.fallback.types

/**
  * Created by mengchong on 8/27/16.
  */
/*class Gmp (val item_id:String,val batch_id:Int, val raw_pv:Float, val raw_clk:Float,
           val decayed_pv:Float, val decayed_clk:Float, val language:String, val region:String, val ts:Long) extends Serializable{
}*/

class Gmp (val item_id:String, val batch_id:Int, val raw_pv:Float, val raw_clk:Float,
           val decayed_pv:Float, val decayed_clk:Float, val gmp_value:Float, val write_to_redis:String, val extra_fields:String, val ts:Long) extends Serializable{
}
