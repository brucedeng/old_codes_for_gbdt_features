package com.cmcm.ranking.fallback.ranking

import com.cmcm.ranking.fallback.types.Content

/**
  * Created by mengchong on 8/28/16.
  */
object RawScore {
  def getRawScore(cp:Content, conf:Map[String,String]) = {
    val decayWnd = conf.getOrElse("decay_wnd","12h")
    val period = decayWnd.last match {
      case x:Char if x.toString == "h" => 1
      case x:Char if x.toString == "d" => 24
      case _ => 1
    }
    val now: Long = System.currentTimeMillis / 1000
    val docAge = (now - cp.publish_time)/3600
    val docAgeWnd = docAge/period
    val baseScore:Double = if(docAgeWnd<0) 10 else if(docAgeWnd>10) 0 else 10-docAgeWnd
    val imageScore = cp.images_cnt/10.0
    val copyrightScore = if(cp.has_copyright) 1 else 0
    val ageScore = 1.0/(docAge+1)
    val score = baseScore*10 + cp.gmp*10 + copyrightScore + ageScore + imageScore
//    val score = 40 - (now - cp.publish_time)/3600.0
    score
  }
}
