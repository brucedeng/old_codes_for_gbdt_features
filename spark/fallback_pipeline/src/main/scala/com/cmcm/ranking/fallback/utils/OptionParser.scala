package com.cmcm.ranking.fallback.utils

import java.io.File
import java.util.Map.Entry

import com.typesafe.config.{ConfigFactory, ConfigValue}
import scala.collection.JavaConversions._

/**
  * Created by mengchong on 8/28/16.
  */
case class Config(optConf:Map[String,String], staticConf:com.typesafe.config.Config) extends Serializable{

}

object OptionParser {
  def parse(args:Array[String]) = {
    val confFile = args(0)
    val typesafeConf =  if(confFile.isEmpty) ConfigFactory.load() else ConfigFactory.parseFile(new File(confFile))
    val chains = typesafeConf.getConfig("fallback.chain")
    val staticConf = typesafeConf.getConfig("fallback.conf")
    val optConf = (for(opt <- args.tail) yield {
      val fields = opt.split("=",2)
      if (fields.length<2) (opt,"")
      else (fields(0),fields(1))
    }).toMap
    staticConf.entrySet().map(x => (x.getKey,x.getValue.unwrapped().toString)).toMap ++ optConf
  }
}
