package com.cmcm.ranking.fallback

import com.cmcm.ranking.fallback.parser.{CPParser, GMPParser}
import com.cmcm.ranking.fallback.ranking.{Diversity, RawScore}
import com.cmcm.ranking.fallback.types.Content
import com.cmcm.ranking.fallback.utils.{Monitor, OptionParser}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.mllib.rdd.MLPairRDDFunctions.fromPairRDD
import redis.clients.jedis._

import scala.collection.JavaConversions._
//import collection.mutable._
import scala.util.{Failure, Success, Try}

/**
  * Created by mengchong on 8/28/16.
  */
object FallbackApp {

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("fallback pipeline")
    val sc = new SparkContext(conf)
    val localAppConf = OptionParser.parse(args)
    val appConf = sc.broadcast( localAppConf )

    println(localAppConf)

    val inputCpStr = sc.textFile(localAppConf("input_cp"))
    val inputGmpStr = sc.textFile(localAppConf("input_gmp"))
    val outputPath = localAppConf("output")

    val catLimit = localAppConf.getOrElse("category_limit","5").toInt
    val kwLimit = localAppConf.getOrElse("kw_limit","15").toInt
    val gmpThreshold = localAppConf.getOrElse("gmp_threshold","50").toInt
    val listLength = localAppConf.getOrElse("list_length","1000").toInt
    val minListLength = localAppConf.getOrElse("min_list_length","100").toInt
    val docExpireDays = localAppConf.getOrElse("doc_expire_days","3").toInt
    val videoExpireDays = localAppConf.getOrElse("video_expire_days","7").toInt

    val lan_whitelist = sc.broadcast(localAppConf("lan_whitelist").split(",").flatMap(line => {
      val fields = line.split(":")
      val language = fields(0).split("/")
      val region = fields(1).split("/")
      for ( l <- language; r <- region) yield {
        l + "_" + r
      }
    }).toSet)

    println(lan_whitelist)

    val now: Long = ( System.currentTimeMillis / 1000 ) / 600 * 600
    val expireTsVideo = now - videoExpireDays * 86400
    val expireTsDoc = now - docExpireDays * 86400

    val cpRdd = inputCpStr.map(line => {
      val cp = CPParser.parse(line,catLimit,kwLimit)
      (Try(cp.item_id).getOrElse(""),cp)
    })
      .filter(line => line._2 != null && line._1.nonEmpty && ((line._2.tp == "article" && line._2.images_cnt > 0) || line._2.tp == "video" || line._2.tp == "minivideo" || line._2.tp == "picture") && line._2.is_servable && (!line._2.cp_disable) && line._2.publish_time > expireTsVideo && line._2.has_copyright)
      .reduceByKey((a,b) => if(a.update_time > b.update_time) a else b)

    //dedup cp by group id
    //val cpRdd = cpRddFilter.map(line => {(line._2.groupid + line._2.language + line._2.region, line._2)}).reduceByKey((a,b) => if(a.update_time > b.update_time) a else b).map(line => {(Try(line._2.item_id).getOrElse(""),line._2)})

    val gmpRdd = inputGmpStr.map(line => {
      val gmp = GMPParser.parse(line)
      (Try(gmp.item_id).getOrElse(""),gmp)
    })
      .filter(line => line._2 != null && line._1.nonEmpty)

    val cpDataWithGmp = cpRdd.leftOuterJoin(gmpRdd).flatMap(line => {
      val cp = line._2._1
      val gmp = line._2._2
      val newCp = gmp match {
        case Some(gmpData) =>
          cp.gmp = if(gmpData.raw_pv>=gmpThreshold ) gmpData.decayed_clk/gmpData.decayed_pv else 0.0
          cp
        case None => cp
      }
      newCp.score = RawScore.getRawScore(newCp,appConf.value)
      var channelIds = newCp.channels
      if(newCp.tp == "article"){
        channelIds = "29" :: "0" :: channelIds
      }
      else if(newCp.tp == "video"){
        channelIds = "28" :: channelIds
      }
      var pidList = List[String]("all")
      if(((newCp.indexFlag >>> 15 ) & 1L) >0 ) pidList = "15" :: pidList
      else if(((newCp.indexFlag >>> 24 ) & 1L) >0 ) pidList = "24" :: pidList
      val lan_region_whitelist = lan_whitelist.value
      (for(channel <- channelIds.distinct; pid <- pidList.distinct) yield {
        val keyDelim = "_"
        var key = newCp.language + "_" + newCp.region + "_news_class_channel_" + channel
        if(pid == "15" || pid == "24") {
          key = newCp.language + "_" + newCp.region + "_" + pid + "_news_class_channel_" + channel
        }
        var flag = lan_region_whitelist.contains(newCp.language + "_" + newCp.region) && !(newCp.tp == "article" && newCp.publish_time <= expireTsDoc)
        // For you channel of en us should be high quality
        if(channel == "29" && newCp.language == "en" && newCp.region == "us") {
          flag = flag && (newCp.force_distribute == 1 || newCp.media_level == 10)
        }
        (key,newCp,flag)
      }).filter(x => x._3)
        .map(x => (x._1,x._2))
    }).cache()
    //cpDataWithGmp.saveAsTextFile("/user/news_model_offline/news_model_offline/cp_with_gmp")

    object MaxOrder extends Ordering[Content] {
      def compare(x:Content, y:Content) = x.score compare y.score
    }
    //val minHeap = scala.collection.mutable.PriorityQueue.empty(MinOrder)

    val rankLists = cpDataWithGmp.topByKey(listLength)(MaxOrder)
      .filter(lists => lists._2.length >= minListLength )
      .map(lists => {
        val (key,contentList) = lists
        val newContentList = Diversity.process(contentList.toSeq,appConf.value)
        (key,newContentList.toList.zipWithIndex.map( elem => (elem._1, 1.0 - 0.0001 * elem._2)))
      }).cache()


    rankLists.flatMap(line => line._2.map(x => line._1 + "\t" + x._2 + "\t" + x._1) ).
      repartition(4).saveAsTextFile(outputPath)
    val localList = rankLists.map(line => (line._1, line._2.map(x => (x._1.item_id,x._2,x._1.publish_time)))).collect()

    localList.foreach(x => println(x._1 + "\t" + x._2.length))

    val monitorList = localList.filter(line => {
      val key = line._1
      key.endsWith("news_class_channel_0") || key.endsWith("news_class_channel_29")
    })

    val cpMonitor = cpDataWithGmp.filter(line => line._1.endsWith("news_class_channel_0") || line._1.endsWith("news_class_channel_29"))
      .map(line => (line._1,1))
      .reduceByKey((a,b) => a+b)
      .map(line => ("fallback.keys.valid",line._2.toFloat,"key=" + line._1))
      .collect()
    val docageMonitor = monitorList.map(line =>
      ("fallback.keys.doc_age",line._2.map(x => (now - x._3)/3600.0).sum.toFloat/line._2.size,"key=" + line._1))
    val docCountMonitor = localList.map(line =>
      ("fallback.keys.doc_count", line._2.size.toFloat, "key=" + line._1))

    // write redis
    val host = localAppConf("redis_hosts").split(",")
    val port = localAppConf("redis_port").toInt
    val db = localAppConf("redis_db").toInt
    val redisCnt = host.map(h => {
      var cnt = 1
      val jedisClient = new Jedis(h, port)
      jedisClient.select(db)
      val pipline = jedisClient.pipelined()
      try{
      localList.foreach(data => {
        val key = data._1
        val test:java.util.Map[java.lang.String, java.lang.Double] = mapAsJavaMap(data._2.map(x => {
          val str:java.lang.String = x._1
          val score:java.lang.Double = x._2.toDouble
          (str, score)
        }).toMap)
        val key_s = Array[String](key)
        pipline.multi()
        pipline.del(key_s:_*)
        pipline.zadd(key, test)
        pipline.expire(key, 259200)
        if(key.endsWith("news_class_channel_0")){
          val key_ts = key.replace("news_class_channel_0","last_modify_time")
          println(s"update time $key_ts is $now")
          pipline.set(key_ts, now.toString)
        }
        pipline.exec()
      })
      }
      catch {
        case e: Exception =>
          println(s"Fail read ContentFeature from redis $h:$port")
          cnt = 0
      } finally {
        jedisClient.disconnect()
      }
      (h,cnt)
    })

    val redisMonitor = redisCnt.map(x => ("fallback.keys.redis",x._2.toFloat,"host="+x._1))
    val monitor = new Monitor(ts=now, endpoint="launcher-news-model-offline.hadoop.usw2.contents.cmcm.com", step=600)

    monitor.writeFalconWithTag(cpMonitor ++ docageMonitor ++ redisMonitor ++ docCountMonitor)

  }
}
