INPUT='/projects/news/model/item_cf/result_dump/20160919/0920/*'
OUTPUT='/tmp/news/model/item_cf/result_dump/20160919/0930'

hadoop jar \
    /usr/lib/hadoop-mapreduce/hadoop-streaming-2.6.0-cdh5.4.8.jar \
     -Dmapred.reduce.tasks=1 \
     -Dmapred.job.queue.name=experiment \
     -input "$INPUT" \
     -output "$OUTPUT" \
     -mapper "cat" \
     -reducer "cat"  
