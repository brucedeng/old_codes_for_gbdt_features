package com.cmcm.datahero.content.nrt.model

import java.nio.file.Files

import com.cmcm.datahero.content.nrt.component._
import com.cmcm.datahero.content.nrt.config.Configurable
import com.cmcm.datahero.content.nrt.event.EventComponent
import com.cmcm.datahero.content.nrt.feature.FeatureComponent
import com.cmcm.datahero.content.nrt.spark.SparkComponent
import com.google.common.base.Joiner
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}
import org.joda.time.DateTime
import org.scalatest.{BeforeAndAfter, FunSuite, Matchers}

import scala.collection.mutable

/**
  * Created by hanbin on 15/10/12.
  */
class ModelOutputSuite extends FunSuite with Matchers with BeforeAndAfter



  with Configurable with SparkComponent with EventComponent with FeatureComponent
  with GMPModelOutput {

  private val master = "local[2]"
  private val appName = "upTest"
  private val batchDuration = Seconds(1)
  private val checkpointDir = Files.createTempDirectory(appName).toString
  private var sc: SparkContext = _
  private var ssc: StreamingContext = _

  override def output(dStream: DStream[(String, Long, Double, Double)], appendedKey: String = ""): Option[DStream[Int]] = ???

  before {
    val conf = new SparkConf()
      .setMaster(master)
      .setAppName(appName)

    ssc = new StreamingContext(conf, batchDuration)
    ssc.checkpoint(checkpointDir)

    sc = ssc.sparkContext
    val lines = Seq(("123", "user feature"))
    val userFeature = sc.parallelize(lines)

  }


  test(" ModelOutputSuite full update should be correct") {

    /*
    //
    //    var ssc: StreamingContext = streamingContext
    //    var sc: SparkContext = ssc.sparkContext


    val modelUpdate = new GMPModelRedisFullOutput("10.10.31.104", 6379, 0,"b73e852ae4c79a1932f58eb67e79", 5, 1, 0)
    val newsIds = "00000004" :: Nil


    val gmpRecord1 = ("00000004", 1L, 2.0, 1.0) :: Nil
    modelUpdate.fullUpdate(gmpRecord1.iterator)




    val event = mutable.Queue[RDD[(String, Long, Double, Double,String)]]()
    event.enqueue(sc.parallelize(
      Seq(
        ("1101", 111L, 22.00, 33.00,"")
      )))
    val eventDStream = ssc.queueStream(event)



    val cFBModelFileOutput = new CFBModelFileOutput("/tmp/test11", 1)
    cFBModelFileOutput.output(eventDStream, "11111")

    ///////

    val event1 = mutable.Queue[RDD[(String, Long, Double, Double,String)]]()
    val tempString: String = Joiner.on("\001").join("11", "22")
    event1.enqueue(sc.parallelize(
      Seq(
        (tempString, 111L, 22.00, 33.00,"")
      )))
    val eventDStream1 = ssc.queueStream(event1)

    cFBModelFileOutput.output(eventDStream1, "11111")

    /////////

    val event2 = mutable.Queue[RDD[(String, Long, Double, Double)]]()
    val tempString2:String=Joiner.on("\002").join("11", "22")
    val tempString3:String=Joiner.on("\001").join("333", tempString2)
    event2.enqueue(sc.parallelize(
      Seq(
        (tempString3, 111L, 22.00, 33.00)
      )))
    val eventDStream2 = ssc.queueStream(event2)




    ///////


    val cFBModelMixedOutput = new CFBModelMixedOutput(List(cFBModelFileOutput))

    cFBModelMixedOutput.output(eventDStream)




    val event3 = mutable.Queue[RDD[(String, Long, Double, Double)]]()
    event3.enqueue(sc.parallelize(
      Seq(
        ("1101", 111L, 22.00, 33.00)
      )))
    val eventDStream3 = ssc.queueStream(event3)



    var gMPModelFileOutput = new GMPModelFileOutput("/tmp/test", 1, 1)
    gMPModelFileOutput.output(eventDStream3)


    var cFBModelKafkaOutput = new CFBModelKafkaOutput(Map("default"->("11", "11", "22")))
    cFBModelKafkaOutput.output(eventDStream)



    var gMPModelRedisFullOutput = new GMPModelRedisFullOutput("10.10.31.104", 6379, 0,"b73e852ae4c79a1932f58eb67e79", 5, 1, 0)
    gMPModelRedisFullOutput.output(eventDStream3)





    val multiGMPModelRedisFullOutput = new MultiGMPModelRedisFullOutput(List(gMPModelFileOutput))

    multiGMPModelRedisFullOutput.output(eventDStream3)





    println(new DateTime() + "ssc ")

    ssc.start()
    println(new DateTime() + "sleep")
    Thread.sleep(10000)

    println(new DateTime() + "thread is over")

    ssc.stop()
  */
  }



  after {
    if (sc != null) {
      sc.stop()
    }
    if (ssc != null) {
      ssc.stop()
    }
  }


}
