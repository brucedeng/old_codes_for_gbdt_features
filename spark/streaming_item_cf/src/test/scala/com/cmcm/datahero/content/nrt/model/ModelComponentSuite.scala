package com.cmcm.datahero.content.nrt.model

import com.cmcm.datahero.content.nrt.component.{ItemCFModelComponent, ModelComponent}
import com.cmcm.datahero.content.nrt.config.{ConfigUnit, Configurable}
import com.cmcm.datahero.content.nrt.event.EventComponent
import com.cmcm.datahero.content.nrt.feature.FeatureComponent
import com.cmcm.datahero.content.nrt.spark.SparkComponent
import com.typesafe.config.ConfigValueFactory
import org.apache.spark.rdd.RDD
import org.joda.time.DateTime
import org.scalatest.{BeforeAndAfter, FunSuite, Matchers}

import scala.collection.mutable
import org.joda.time.DateTime
import org.apache.spark.streaming.{Seconds, Time}



/**
  * Created by hanbin on 15/8/26.
  */
class ModelComponentSuite extends FunSuite with Matchers with BeforeAndAfter


  with InitTest
  with Configurable with SparkComponent with EventComponent with FeatureComponent
  with ItemCFModelComponent{


  //override def workflowSetup(): Unit = ???

  /*
  test(" modelComponent ") {

    var ssc = streamingContext
    var sc = ssc.sparkContext

    preStart()
    val modelConfigUnitVal = modelConfigUnit

    ModelComponent.loadInitStateRDD(sc, "file:///tmp/nrt_cfb_test/state/test")

    ModelComponent.loadLatestStateRDD(sc, "file:///tmp/nrt_cfb_test/state/test")
  }
  */





  test("modelComponent cfb gmp") {

    println("test ModelComponentSuite")
    // init("src/test/resources")

    // initModelComponent("src/test/resources/model.conf")
    // initEventComponent("src/test/resources/event.conf")

    val ssc = streamingContext
    val sc = ssc.sparkContext

    var componentConfigUnit = componentsConfigs.get("model_component").get
    var componentConfigUnitConfg = componentConfigUnit.getConfig


    var newConfig = componentConfigUnitConfg.withValue("model.cfb.output.dest", ConfigValueFactory.fromAnyRef("kafka"))
    newConfig = newConfig.withValue("model.gmp.recover_mode", ConfigValueFactory.fromAnyRef(true))


    componentConfigUnit = new ConfigUnit("model_component", newConfig)
    componentsConfigs.put("model_component", componentConfigUnit)



    println(new DateTime() + "start event")


    val event = mutable.Queue[RDD[String]]()
    event.enqueue(sc.parallelize(
      Seq(
        """{"act":"1","aid":"7d2d79ef9611536c","pid":"14","country":"US","app_lan":"en",
          |"contentid":"1065871","cpack":"","ext":{"eventtime":"1447287994","requesttime":"1447287900"},
          |"scenario":{"column":"","is_level1_native":"true","level1":"1","level1_type":"1",
          |"level2":"29","source":""},"servertime":"2015-11-12 00:28:20","servertime_sec":"1447288100",
          |"upack":"","uuid":"7d2d79ef9611536c"}"""
      )))
    val eventDStream = ssc.queueStream(event)

    val parsedDStream = parseCFEvent(eventDStream)
    val dedupedDStream = dedupForWorldwideGMP(parsedDStream)
    val itemCoOccurDStream = getItemCoOccur(dedupedDStream)
    dumpItemCFResult(itemCoOccurDStream)
    //itemCoOccurDStream.foreachRDD(rdd => rdd.foreach(x => println(x.toString())))

    val itemStreamRDD = sc.parallelize(Seq(
      (("a","1","rid\0031",2412147L),(1.0,0.0)),
      (("a","2","rid\0032",2412147L),(1.0,0.0)),
      (("a","3","rid\0032",2412147L),(1.0,0.0)),

      (("b","4","dummy",2412147L),(1.0,1.0)),
      (("b","3","dummy",2412147L),(1.0,1.0)),
      (("b","1","dummy",2412147L),(1.0,0.0))
    ))

    val resRDD = computeItemOccur(true,100,true,1.0)(itemStreamRDD)
    resRDD.foreach(x => {println (x.toString())})


    val path = ItemCFModelComponent.getEventPathByTimeWindow(new Time(System.currentTimeMillis), 600, 6,"/tmp/test/path")
    println (path.get)




    /*
    val gmpInput = mutable.Queue[RDD[((String, String, String, Long), (Double, Double))]]()
    gmpInput.enqueue(sc.parallelize(
      Seq(
        (("1101", "11", "22", 3L), (2.0, 2.0))
      )))

    val inputGMPDStream = ssc.queueStream(gmpInput)

    val modelStream2 = mutable.Queue[RDD[(String, Long, Double, Double)]]()
    modelStream2.enqueue(sc.parallelize(
      Seq(
        ("1101", 1L, 2.0, 3.0)
      )))

    val modelStreamStream2 = ssc.queueStream(modelStream2)

    val dumpFormatGmapStream = mutable.Queue[RDD[(String, (Long, Double, Double, Double, Double))]]()
    dumpFormatGmapStream.enqueue(sc.parallelize(
      Seq(
        ("1101",( 1L, 2.0, 3.0,4.0,5.0))
      )))

    val dumpFormatGmapStreamStream = ssc.queueStream(dumpFormatGmapStream)

/////

    gmpMetricsOutput(eventDStream,
      inputGMPDStream,
      inputGMPDStream,
      dumpFormatGmapStreamStream,
      modelStreamStream2,
      None
    )
    */


    println(new DateTime() + "start ssc" + ssc)
    println()
    ssc.start()
    println(new DateTime() + "start sleep")
    Thread.sleep(10000)
    println(new DateTime()+" unit over")


  }


}
