package com.cmcm.datahero.content.nrt.feature

import java.nio.file.Files

import com.cmcm.datahero.content.nrt.config.Configurable
import com.cmcm.datahero.content.nrt.event.EventComponent
import com.cmcm.datahero.content.nrt.feature.Profile.{CategoryProfile, KeywordProfile}
import com.cmcm.datahero.content.nrt.spark.SparkComponent
import com.typesafe.config.Config
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.{StreamingContext, Seconds}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import redis.clients.jedis.Jedis

import scala.collection.mutable

/**
  * Created by wangwei5 on 15/8/13.
  */

class ContentFeatureSuite extends FlatSpec with Matchers with BeforeAndAfter with MockFactory with FeatureComponent with EventComponent with SparkComponent with Configurable {

  val redisHost = "10.10.12.51"
  val redisPort = 6379
  val auth = "b73e852ae4c79a1932f58eb67e79"
  var jedisClient: Jedis = _
  private val master = "local[2]"
  private val appName = "cpTest"
  private val batchDuration = Seconds(1)
  private val checkpointDir = Files.createTempDirectory(appName).toString
  private var sc: SparkContext = _
  private var ssc: StreamingContext = _
  /*
  before {
    jedisClient = new Jedis(redisHost, redisPort)
    jedisClient.auth(auth)

    val conf = new SparkConf()
      .setMaster(master)
      .setAppName(appName)

    ssc = new StreamingContext(conf, batchDuration)
    ssc.checkpoint(checkpointDir)
    sc = ssc.sparkContext
  }

  "ContentFeature" should "get content feature failed" in {
    val result = ContentFeature.getContentFeature("fake", jedisClient, 5, 15, "v4", "v2")
    result should be("")
  }

  "ContentFeature" should "get content feature successfully" in {
    val key = jedisClient.randomKey()
    val result = ContentFeature.getContentFeature(key.substring(2), jedisClient, 5, 15, "v4", "v2")
    result.size should be >= 0
    println(result)
  }

  "ContentFeature" should "get content feature successfully with zero top" in {
    val key = jedisClient.randomKey()
    val result = ContentFeature.getContentFeature(key.substring(2), jedisClient, 0, 0, "v4", "v2")
    result.size should be >= 0
  }

  "ContentFeature" should "get content keywords successfully" in {
    val keywordProfile1 = KeywordProfile.newBuilder().setId(1).setName("kw1").setWeight(0.5).build()
    val keywordProfile2 = KeywordProfile.newBuilder().setId(2).setName("kw2").setWeight(0.5).build()
    val ck = List[KeywordProfile](keywordProfile1, keywordProfile2)

    val result = ContentFeature.getKeywords(ck, 15)
    result.size should be >= 0
  }

  "ContentFeature" should "get content categories successfully" in {
    val category1 = CategoryProfile.newBuilder().setId(1).setName("cat1").setWeight(0.5).build()
    val category2 = CategoryProfile.newBuilder().setId(2).setName("cat2").setWeight(0.5).build()
    val cc = List[CategoryProfile](category1, category2)

    val result = ContentFeature.getCategories(cc, 5)
    result.size should be >= 0
  }

  "ContentFeature" should "get content keywords successfully with top 0" in {
    val keywordProfile1 = KeywordProfile.newBuilder().setId(1).setName("kw1").setWeight(0.5).build()
    val keywordProfile2 = KeywordProfile.newBuilder().setId(2).setName("kw2").setWeight(0.5).build()
    val ck = List[KeywordProfile](keywordProfile1, keywordProfile2)

    val result = ContentFeature.getKeywords(ck, 0)
    result.size should be >= 0
  }

  "ContentFeature" should "get content categories successfully with top 0" in {
    val category1 = CategoryProfile.newBuilder().setId(1).setName("cat1").setWeight(0.5).build()
    val category2 = CategoryProfile.newBuilder().setId(2).setName("cat2").setWeight(0.5).build()
    val cc = List[CategoryProfile](category1, category2)

    val result = ContentFeature.getCategories(cc, 0)
    result.size should be >= 0
  }

  "ContentFeature" should "get from redis successfully" in {
    val event = mutable.Queue[RDD[(String, (String, Long, Double, Double, String))]]()
    event.enqueue(sc.parallelize(Seq(("fake", ("uid", 1440000000L, 1.0, 1.0, "event feature")))))
    val eventQueue = ssc.queueStream(event)
    val cpConfig = mock[Config]
    (cpConfig.getString _).expects("redis_host").returning(redisHost)
    (cpConfig.getInt _).expects("redis_port").returning(redisPort)
    (cpConfig.getInt _).expects("top_categories").returning(5)
    (cpConfig.getInt _).expects("top_keywords").returning(25)
    (cpConfig.getString _).expects("category_version").returning("v4")
    (cpConfig.getString _).expects("keyword_version").returning("v2")
    (cpConfig.getString _).expects("redis_password").returning(auth)

    val eventCount = sc.accumulator(0L, "Event Count Accumulator")
    eventQueue.foreachRDD(rdd => {
      rdd.foreach(record => eventCount += 1)
    })

    val joinResult = ContentFeature.getContentFeatureFromRedis(eventQueue, cpConfig)
    val joinCount = sc.accumulator(0L, "Join Count Accumulator")
    joinResult.foreachRDD(rdd => {
      rdd.foreach(record => joinCount += 1)
    })
    ssc.start()
    Thread.sleep(1500)
    eventCount.value should be(1)
    joinCount.value should be(1)
    ssc.stop()
  }

  "ContentFeatureForIndia" should "get content feature failed" in {
    val result = ContentFeatureForIndia.getContentFeature("fake", jedisClient, 5, 15,"","")
    result should be("")
  }

  "ContentFeatureForIndia" should "get content feature successfully" in {
    val key = jedisClient.randomKey()
    val result = ContentFeatureForIndia.getContentFeature(key.substring(2), jedisClient, 5, 15,"","")
    result.size should be >= 0
  }

  "ContentFeatureForIndia" should "get content feature successfully with zero top" in {
    val key = jedisClient.randomKey()
    val result = ContentFeatureForIndia.getContentFeature(key.substring(2), jedisClient, 0, 0,"","")
    result.size should be >= 0
  }

  "ContentFeatureForIndia" should "get content keywords successfully" in {
    val keywordProfile1 = KeywordProfile.newBuilder().setId(1).setName("kw1").setWeight(0.5).build()
    val keywordProfile2 = KeywordProfile.newBuilder().setId(2).setName("kw2").setWeight(0.5).build()
    val ck = List[KeywordProfile](keywordProfile1, keywordProfile2)

    val result = ContentFeatureForIndia.getKeywords(ck, 15)
    result.size should be >= 0
  }

  "ContentFeatureForIndia" should "get content categories successfully" in {
    val category1 = CategoryProfile.newBuilder().setId(1).setName("cat1").setWeight(0.5).build()
    val category2 = CategoryProfile.newBuilder().setId(2).setName("cat2").setWeight(0.5).build()
    val cc = List[CategoryProfile](category1, category2)

    val result = ContentFeatureForIndia.getCategories(cc, 5)
    result.size should be >= 0
  }

  "ContentFeatureForIndia" should "get content keywords successfully with top 0" in {
    val keywordProfile1 = KeywordProfile.newBuilder().setId(1).setName("kw1").setWeight(0.5).build()
    val keywordProfile2 = KeywordProfile.newBuilder().setId(2).setName("kw2").setWeight(0.5).build()
    val ck = List[KeywordProfile](keywordProfile1, keywordProfile2)

    val result = ContentFeatureForIndia.getKeywords(ck, 0)
    result.size should be >= 0
  }

  "ContentFeatureForIndia" should "get content categories successfully with top 0" in {
    val category1 = CategoryProfile.newBuilder().setId(1).setName("cat1").setWeight(0.5).build()
    val category2 = CategoryProfile.newBuilder().setId(2).setName("cat2").setWeight(0.5).build()
    val cc = List[CategoryProfile](category1, category2)

    val result = ContentFeatureForIndia.getCategories(cc, 0)
    result.size should be >= 0
  }

  "ContentFeatureForIndia" should "get from redis successfully" in {
    val event = mutable.Queue[RDD[(String, (String, Long, String, Double, String))]]()
    event.enqueue(sc.parallelize(Seq(("fake", ("uid", 1440000000L, "PV", 1.0, "event feature")))))
    val eventQueue = ssc.queueStream(event)
    val cpConfig = mock[Config]
    (cpConfig.getString _).expects("redis_host").returning(redisHost)
    (cpConfig.getInt _).expects("redis_port").returning(redisPort)
    (cpConfig.getInt _).expects("top_categories").returning(5)
    (cpConfig.getInt _).expects("top_keywords").returning(25)
    (cpConfig.getString _).expects("category_version").returning("")
    (cpConfig.getString _).expects("keyword_version").returning("")
    (cpConfig.getString _).expects("redis_password").returning(auth)

    val eventCount = sc.accumulator(0L, "Event Count Accumulator")
    eventQueue.foreachRDD(rdd => {
      rdd.foreach(record => eventCount += 1)
    })

    val joinResult = ContentFeatureForIndia.getContentFeatureFromRedis(eventQueue, cpConfig)
    val joinCount = sc.accumulator(0L, "Join Count Accumulator")
    joinResult.foreachRDD(rdd => {
      rdd.foreach(record => joinCount += 1)
    })
    ssc.start()
    Thread.sleep(1500)
    eventCount.value should be(1)
    joinCount.value should be(1)
    ssc.stop()
  }

  after {
    if (jedisClient != null) {
      jedisClient.disconnect()
    }
    if (sc != null) {
      sc.stop()
    }
    if (ssc != null) {
      ssc.stop()
    }
  }
  */

}
