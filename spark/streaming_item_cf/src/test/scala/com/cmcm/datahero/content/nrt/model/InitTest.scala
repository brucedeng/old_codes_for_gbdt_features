package com.cmcm.datahero.content.nrt.model

import java.io.{File, PrintWriter}

import com.cmcm.datahero.content.nrt.config.Configurable
import com.cmcm.datahero.content.nrt.event.EventComponent
import com.cmcm.datahero.content.nrt.feature.FeatureComponent
import com.cmcm.datahero.content.nrt.spark.SparkComponent
import com.google.common.base.Charsets
import com.google.common.io.Files
import com.typesafe.config.ConfigFactory
import org.apache.commons.io.FileUtils
import org.apache.spark.SparkContext
import org.apache.spark.streaming.StreamingContext
import org.joda.time.{DateTime, Days}
import org.scalatest.{BeforeAndAfter, FunSuite, Matchers}

import scala.io.Source


/**
  * Created by lixiaolong on 16/4/14.
  */
trait InitTest extends FunSuite with Matchers with BeforeAndAfter

  with Configurable with SparkComponent with EventComponent with FeatureComponent {

  var oldFile: File = _;
  var backFile: File = _;

  before {

    println("init Test")
    println(new DateTime() + "start before")

    // delete checkpoint
    var checkPointfile = new File("/tmp/content_cfb_checkpoint_test/");
    if (checkPointfile.exists()) {
      FileUtils.deleteDirectory(checkPointfile);
    }

    var stateFile= new File("/tmp/nrt_cfb_test/state/test")
    if(!stateFile.exists()){
      stateFile.mkdirs()
    }
    var successFile=new File("/tmp/nrt_cfb_test/state/_SUCCESS")
    if(!successFile.exists()) {
      successFile.createNewFile()
    }
    var stateFiletemp=new File("/tmp/nrt_cfb_test/state/success.temp")
    if(!stateFiletemp.exists()) {
      stateFiletemp.createNewFile()
      Files.write("1  2 3 4 5 6",stateFiletemp,Charsets.UTF_8)
    }



    val testResourcePath = "src/test/resources"
    val featureConfigFileName = "feature.conf"

    //var classpath=Thread.currentThread().getContextClassLoader.getResource("").toString
    // println("classpath is " + classpath)
    val absolutePath = new File("src/test/resources").getAbsolutePath
    println("absolutePath is " + absolutePath)

    // update feature.conf  input_dir
    val oldFeatureConfigFile = new File(testResourcePath + "/" + featureConfigFileName)
    oldFile = oldFeatureConfigFile
    val backUpFeatureConfigFile = new File(testResourcePath + "/" + featureConfigFileName + ".backup")
    backFile = backUpFeatureConfigFile

    val featureConfig = ConfigFactory.parseFile(oldFeatureConfigFile)

    // get before value
    var oldInputDir = featureConfig.getString("feature.up.input_dir")

    var newInputDir = "file:///" + absolutePath + oldInputDir


    // 计算时间偏移量 ,当前天 减去 20160410 ,这个地方要考虑 时区的一致性 不?
    var dateTimeData: DateTime = new DateTime("2016-04-10");
    var dateTimeNow: DateTime = new DateTime()


    var days = Days.daysBetween(dateTimeNow, dateTimeData).getDays
    println("date minus 2016-04-10 is " + days)







    // 读入旧文件
    val file = Source.fromFile(oldFeatureConfigFile)

    val newFeatureConfigFile = new File(testResourcePath + "/" + featureConfigFileName + ".tmp")

    // 写入 新文件
    val writer = new PrintWriter(newFeatureConfigFile)

    for (line <- file.getLines()) {
      // println("line is " + line)

      var newLine = line;

      if (line.trim.matches("^input_dir.*\"/up\"")) {
        println("line.trim.matches input_idr.*up"+line)

        newLine = "    input_dir=\"" + newInputDir + "\" "

        println("after newLine is "+newLine)

      } else if (line.trim.matches("^load_datetime_offset.*")) {
        newLine = "    load_datetime_offset=" + days
        println("after load_datetime_offset  newLine is "+newLine)
      }

      writer.println(newLine)
    }

    writer.flush()
    writer.close()

    // 重命名

    // 老文件 先备份
    Files.move(oldFeatureConfigFile, backUpFeatureConfigFile)

    Files.move(newFeatureConfigFile, oldFeatureConfigFile);




    println(new DateTime() + "end before")





    //
    //    val newConfig = featureConfig.withValue("feature.up.input_dir",
    //      ConfigValueFactory.fromAnyRef("file:///"+classpath+inputDir))
    //


    // 全局环境 初始化,包括spark 等
    init(testResourcePath)


  }


  after {
    println(new DateTime() + "enter after")
    // 恢复备份的文件

    Files.move(backFile, oldFile)

    // Thread.sleep(100000)
    // 关闭spark

    var ssc: StreamingContext = streamingContext
    var sc: SparkContext = ssc.sparkContext
    if (sc != null) {
      println(new DateTime() + "stop sc")
      sc.stop()
    }
    if (ssc != null) {
      println(new DateTime() + "stop ssc")
      ssc.stop()
    }

    println(new DateTime() + "over")
  }


}
