package com.cmcm.datahero.content.nrt.app

import com.cmcm.datahero.content.nrt.component.ModelComponent
import com.cmcm.datahero.content.nrt.config.Configurable
import com.cmcm.datahero.content.nrt.event.EventComponent
import com.cmcm.datahero.content.nrt.model.{CFWorkflow, StreamingWorkflow}
import com.cmcm.datahero.content.nrt.spark.SparkComponent
import org.apache.spark.streaming.StreamingContext
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}
import org.mockito.Mockito

/**
 * Created by hanbin on 16/4/12.
 */

trait FakeModelComponent extends ModelComponent {
  this: Configurable with SparkComponent with EventComponent with FakeWorkflow=>

  override def init(configDir: String) = {
  }

  override def preStart() = {
  }

  override def workflowSetup() = {

  }
}

trait  FakeSparkComponent extends SparkComponent { this: Configurable =>

  override def init(configDir: String) = {
  }

  override def preStart() = {
  }

  override def isNewContext: Boolean = {
    true
  }

  override def streamingContext: StreamingContext = {
    Mockito.mock(classOf[StreamingContext])
  }

}

trait FakeEventComponent extends EventComponent {
  this: Configurable with SparkComponent =>

}

trait FakeWorkflow extends StreamingWorkflow {
  override def workflowSetup()  = {}
}

object FakeApp extends NRTApp
               with Configurable
               with FakeSparkComponent
               with FakeEventComponent
               with FakeModelComponent
               with FakeWorkflow

class NRTAppSuite extends FlatSpec with Matchers with MockFactory {

  "NRTApp" should "run correctly" in {
    FakeApp.main(List("-c", "/tmp").toArray)
  }

}
