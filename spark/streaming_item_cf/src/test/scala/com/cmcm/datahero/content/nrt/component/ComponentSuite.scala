package com.cmcm.datahero.content.nrt.component

import com.cmcm.datahero.content.nrt.config.{ConfigUnit, Configurable}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FunSuite, Matchers}

/**
  * Created by lixiaolong on 16/4/12.
  */
class ComponentSuite extends FunSuite with Component with Configurable with Matchers with MockFactory {


  test("Component should load eventConfigFile correctly .") {

    init("/tmp/tmp")
    preStart()

    var name: String = "feature.conf"
    var eventConfigFile: String = "src/test/resources/feature.conf"

    val configUnit: ConfigUnit = loadComponentConfig(name, eventConfigFile)
    assert(configUnit.getConfigName.equals(name))

    // this will throw exception IllegalStateException, but until 2016-04-12 the Component need to be modify.
    eventConfigFile = "src/test/resources/not_exist_file"

    //intercept[IllegalStateException] {
    // loadComponentConfig(name, eventConfigFile)
    //}


  }

}
