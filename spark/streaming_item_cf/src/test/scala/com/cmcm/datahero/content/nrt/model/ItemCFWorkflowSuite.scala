package com.cmcm.datahero.content.nrt.model

import com.cmcm.datahero.content.nrt.config.Configurable
import com.cmcm.datahero.content.nrt.event.EventComponent
import com.cmcm.datahero.content.nrt.feature.FeatureComponent
import com.cmcm.datahero.content.nrt.spark.SparkComponent
import com.cmcm.datahero.content.nrt.component.ItemCFModelComponent
import org.joda.time.DateTime
import org.scalatest.{BeforeAndAfter, FunSuite, Matchers}

/**
  * Created by lixiaolong on 16/4/15.
  */
class ItemCFWorkflowSuite
  extends FunSuite with Matchers with BeforeAndAfter
    with InitTest
    with Configurable with SparkComponent with EventComponent with FeatureComponent
    with ItemCFModelComponent
    with CFWorkflow {

  test("work") {

    println("test ItemCFWorkflowSuite")
    println(new DateTime() + "start test function")
    //workflowSetup()

  }


}