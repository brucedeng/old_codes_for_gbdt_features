package com.cmcm.datahero.content.nrt.event

import com.cmcm.datahero.content.nrt.config.{ConfigUnit, Configurable}
import com.cmcm.datahero.content.nrt.spark.SparkComponent
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.dstream.DStream
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfter, Matchers, FlatSpec}


/**
  * Created by hanbin on 15/8/25.
  */
class EventComponentSuite extends FlatSpec with Matchers with MockFactory with BeforeAndAfter with EventComponent with SparkComponent with Configurable {

  val master = "local[2]"
  val appName = "test-spark-input-stream"
  var ssc : StreamingContext = _

  before {
    val conf = new SparkConf()
      .setMaster(master)
      .setAppName(appName)

    ssc = new StreamingContext(conf, Seconds(1))
  }

  "EventComponent" should "able to init event component correctly" in {
    initEventComponent("conf/nr_us/item_cf/conf/event.conf")
    eventConfigUnit shouldBe a[ConfigUnit]
  }


  /*
  "EventComponent" should "able to get input DStream from file" in {
    initEventComponent("src/test/resources/event.conf")
    getInputDstream(ssc)(eventConfigUnit) shouldBe a [DStream [(_)]]
  }
  */


  /*
  "EventComponent" should "able to get input DStream from kafka use receive" in {
    initEventComponent("src/test/resources/event_kafka_re.conf")
    getInputDstream(ssc)(eventConfigUnit) shouldBe a [DStream [(_)]]
  }*/

  after {
    if (ssc != null) {
      ssc.stop()
    }
  }
}



