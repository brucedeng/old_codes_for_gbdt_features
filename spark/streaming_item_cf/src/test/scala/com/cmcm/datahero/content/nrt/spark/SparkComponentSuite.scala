package com.cmcm.datahero.content.nrt.spark

import com.cmcm.datahero.content.nrt.config.{ConfigUnit, Configurable}
import org.apache.spark.SparkContext
import org.apache.spark.streaming.StreamingContext
import org.scalatest.{BeforeAndAfter, Matchers, FlatSpec}

class SparkComponentSuite extends FlatSpec with Matchers with BeforeAndAfter with SparkComponent with Configurable{

  before {

  }

  "SparkComponent" should "contains setting " in {
    SparkComponent.componentName should not be empty
    SparkComponent.configFileName should not be empty
    SparkComponent.defaultAppName should not be empty
    SparkComponent.defaultRecoverMode shouldBe false
    SparkComponent.defaultCheckPoint should not be empty
    SparkComponent.defaultBatchSeconds should be >= 100
  }

  "SparkComponent" should "init correctly" in {
      init("src/test/resources/")
  }

  "SparkComponent" should "ablt to get streamingContext" in {
    val sc = streamingContext
    sc shouldBe a [StreamingContext]
  }

  "SparkComponent" should "ablt to isNewContext" in {
    val isNew = isNewContext
    isNew should be(true)
  }

  "SparkComponent" should "ablt to get sparkContext" in {
    val sc = sparkContext
    sc shouldBe a [SparkContext]
  }

  "SparkComponent" should "ablt to get sparkConfigUnit" in {
    val sc = sparkConfigUnit
    sc shouldBe a [ConfigUnit]
  }

  after {
    if (streamingContext != null) {
      streamingContext.stop()
    }
  }
}
