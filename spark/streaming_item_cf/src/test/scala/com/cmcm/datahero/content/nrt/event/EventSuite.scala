package com.cmcm.datahero.content.nrt.event

import com.cmcm.datahero.content.nrt.config.ConfigUnit
import org.apache.spark.Logging
import org.scalatest.FunSuite

import scala.util.Try

/**
  * Created by hanbin on 15/8/24.
  */
class EventSuite extends FunSuite with Logging {

  val newConfigUnitTry = Try(ConfigUnit("event", "src/test/resources/event.conf"))
  Event.initEventParser(newConfigUnitTry.get)
  val window: Int = 600

  test("get value or list for key") {
    val viewline: String =
      """{"method":"","params":{"pf":"android","lan":"zh","network":"2G","osversion":"4.4.4",
                       "devicemodel":"N5209","ident":"zsngz72co2l5cuqa2ckglpsapv9l","appversion":"3.16.2",
                       "channel":"10000017","source":"detailpage_relative","newsid":0,"cid":"","ccode":"w-101290101",
                       "productid":"3","brand":"OPPO","packet":"","eventtime":["1438876721","1438876721","1438876721"],
                       "requesttime":["1438876721","1438876721","1438876721"],"uuid":"zsngz72co2l5cuqa2ckglpsapv9l",
                       "ip":"27.193.1.26","time":1438876814,"updatetime":"2015-08-07 00:00:14","aid":"b8afe27160c202e1",
                       "viewnum":"3","ids":["10220278","10212727","10218252"],"cids":[],"ranktypes":["1","1","1"],
                       "column":-100,"sequence_all":"","sequence_news":""}}
      """

    val uuid = Event.getValueForKey("uuid", viewline)
    assert(uuid == "zsngz72co2l5cuqa2ckglpsapv9l")
    val packet = Event.getValueForKey("packet", viewline)
    assert(packet == "")
    val column = Event.getValueForKey("column", viewline)
    assert(column == "-100")
    val ids = Event.getListForKey("ids", viewline)
    assert(ids.length == 3)
    val cids = Event.getListForKey("cids", viewline)
    assert(cids.length == 0)
  }


  test("test act filter") {
    val impressionEvent: String =
      """{"act":"1","aid":"7d2d79ef9611536c","pid":"14","country":"US","app_lan":"en",
        |"contentid":"1065871","cpack":{"des":"rid=8a1ff84d|src=8|ord=1|page_n=0|hit_sc=0","md5":"18895751","ishot":1},"ext":{"eventtime":"1447287994","requesttime":"1447287900"},
        |"scenario":{"column":"","is_level1_native":"true","level1":"1","level1_type":"1",
        |"level2":"29","source":""},"servertime":"2015-11-12 00:28:20","servertime_sec":"1447288100",
        |"upack":"","uuid":"7d2d79ef9611536c"}""".stripMargin

    val impressionTuple = Event.parseEventForCF("NR_US",Array("1"),window)(impressionEvent)
    println(impressionTuple.toString())
    //assert(impressionTuple.size == 1)
    //assert(impressionTuple.head._2._1 == 1.0)
    //assert(impressionTuple.head._2._2 == 0.0)

    val impressionEvent_cl: String =
      """{"act":"2","aid":"7d2d79ef9611536c","pid":"14","country":"US","app_lan":"en",
        |"contentid":"1065871","cpack":"","ext":{"eventtime":"1447287994","requesttime":"1447287900"},
        |"scenario":{"column":"","is_level1_native":"true","level1":"1","level1_type":"1",
        |"level2":"29","source":""},"servertime":"2015-11-12 00:28:20","servertime_sec":"1447288100",
        |"upack":"","uuid":"7d2d79ef9611536c"}""".stripMargin

    val impressionTuple_cl = Event.parseEventForCF("NR_US",Array("1"),window)(impressionEvent_cl)
    println(impressionTuple_cl.toString())
    //assert(impressionTuple_cl.size == 1)
    //assert(impressionTuple_cl.head._2._1 == 0.0)
    //assert(impressionTuple_cl.head._2._2 == 1.0)

  }



  test("test wrong event") {
    val impressionEvent: String =
      """{"act":"3","aid":"7d2d79ef9611536c","pid":"14","country":"US","app_lan":"en",
        |"contentid":"1065871","cpack":"","ext":{"eventtime":"1447287994","requesttime":"1447287900"},
        |"scenario":{"column":"","is_level1_native":"true","level1":"1","level1_type":"1",
        |"level2":"29","source":""},"servertime":"2015-11-12 00:28:20","servertime_sec":"1447288100",
        |"upack":"","uuid":"7d2d79ef9611536c"}""".stripMargin

    val impressionTuple = Event.parseEventForCF("NR_US",Array("1"),window)(impressionEvent)
    println(impressionTuple.toString())
    //assert(impressionTuple.size == 0)


    val impressionEvent2: String =
      """{"act":"1","aid":"7d2d79ef9611536c","pid":"14","country":"US","app_lan":"en",
        |"contentid":"1065871","cpack":"","ext":{"eventtime":"1447287994","requesttime":"1447287900"},
        |"scenario":{"column":"","is_level1_native":"true","level1":"1","level1_type":"1", "ctype":"2",
        |"level2":"29","source":""},"servertime":"2015-11-12 00:28:20","servertime_sec":"1447288100",
        |"upack":"","uuid":"7d2d79ef9611536c"}""".stripMargin

    val impressionTuple2 = Event.parseEventForCF("NR_US",Array("1"),window)(impressionEvent2)
    println(impressionTuple2.toString())
    //assert(impressionTuple2.size == 0)

  }
  
}
