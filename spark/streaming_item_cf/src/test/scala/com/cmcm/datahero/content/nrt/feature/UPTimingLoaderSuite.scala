package com.cmcm.datahero.content.nrt.feature

import java.io.File
import java.nio.file.Files

import com.typesafe.config.Config
import org.apache.spark.{SparkConf, SparkContext}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfter, Matchers, FlatSpec}

/**
 * Created by wangwei5 on 16/4/12.
 */

class UPTimingLoaderSuite extends FlatSpec with Matchers with BeforeAndAfter with MockFactory {

  private val master = "local[2]"
  private val appName = "upTimingLoaderTest"
  private val checkpointDir = Files.createTempDirectory(appName).toString
  private var sc: SparkContext = _

  /*
  before {
    val conf = new SparkConf()
      .setMaster(master)
      .setAppName(appName)

    sc = new SparkContext(conf)
  }

  "UPTimingLoader" should "load up failed" in {
    val upConfig = mock[Config]
    val upSource = new File("src/test/resources/up").getAbsolutePath
    (upConfig.getInt _).expects("load_period").returning(86400)
    (upConfig.getInt _).expects("load_datetime_offset").returning(-1)
    (upConfig.getString _).expects("datetime_format").returning("yyyyMMdd")
    UPTimingLoader.initUPTimingLoader(upConfig, sc)
  }

//
//  "UPTimingLoader" should "get up rdd failed because of no record in queue" in {
//    val upRdd = UPTimingLoader.getCurrentUserFeatureRDD
//    upRdd should be(None)
//  }

//
//  "UPTimingLoader" should "get up rdd successfully" in {
//    val lines = Seq(("123", "user feature"))
//    val userFeature = sc.parallelize(lines)
//    UPTimingLoader.upQueue.add(("20160410", userFeature))
//    val upRdd = UPTimingLoader.getCurrentUserFeatureRDD
//    val date = upRdd.get._1
//    date should be("20160410")
//  }

  "UPTimingLoader" should "waitForReady failed" in {
    UPTimingLoader.waitForReady(0)
  }

  after {
    if (sc != null) {
      sc.stop()
    }
  }
  */

}
