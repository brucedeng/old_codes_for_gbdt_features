package com.cmcm.datahero.content.nrt.config

import org.apache.spark.Logging
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import scala.util.Try

/**
  * Created by hanbin on 15/8/20.
  */
@RunWith(classOf[JUnitRunner])
class ConfigUnitSuite extends FunSuite with Logging {

  test("ConfigUnit should not be created correctly if the file is not existed.") {
    assert(Try(ConfigUnit("", "")).isFailure)
    logInfo("***********************************************************************")

  }

  test("ConfigUnit should be created correctly and the config path can be got.") {
    val configUnit = ConfigUnit("config_unit.conf", "src/test/resources/config_unit.conf")
    assert(configUnit.getConfigName.contains("config_unit.conf"))
    assert(configUnit.getConfig.getBoolean("configunit.test"))
    assert(configUnit.getConfig.getString("configunit.name") == "name")
    assert(configUnit.getConfig.getInt("configunit.number") == 10)
    assert(configUnit.getConfig.getIntList("configunit.list").size == 3)
    val doublList = configUnit.getDoubleList("configunit.double_list", List[Double]())
    assert(doublList.size == 3)
    intercept[com.typesafe.config.ConfigException] {
      configUnit.getConfig.getString("configunit.not_exist")
    }

    assert(configUnit.getBoolean("configunit.boolean", false))
    assert(configUnit.getBooleanList("configunit.boolean_list", List(false)).size == 2)
    assert(configUnit.getDouble("configunit.double", 0) != 0)
    assert(configUnit.getInt("configunit.number", 0) != 0)
    assert(configUnit.getIntList("configunit.list", List(0)).size == 3)
    assert(configUnit.getLong("configunit.number", 0) != 0)
    assert(configUnit.getLongList("configunit.list", List(0)).size == 3)
    assert(configUnit.getString("configunit.name", "").equals("name"))
    assert(configUnit.getStringList("configunit.string_list", List("")).size == 2)
  }

}
