package com.cmcm.datahero.content.nrt.feature

import java.io.File
import java.nio.file.Files
import java.util.concurrent.ArrayBlockingQueue

import com.cmcm.datahero.content.nrt.config.Configurable
import com.cmcm.datahero.content.nrt.event.EventComponent
import com.cmcm.datahero.content.nrt.spark.SparkComponent
import com.typesafe.config.Config
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}
import org.scalamock.scalatest.MockFactory

import org.scalatest.{BeforeAndAfter, Matchers, FlatSpec}

import scala.collection.mutable
import scala.util.Try


/**
  * Created by wangwei5 on 15/8/13.
  */

object UserFeatureSuite {

  private val upQueue = new ArrayBlockingQueue[(String, RDD[(String, String)])](10)
  def getUp() = {
    Some(upQueue.element())
  }

}

class UserFeatureSuite extends FlatSpec with Matchers with BeforeAndAfter with MockFactory with FeatureComponent with EventComponent with SparkComponent with Configurable{

  private val master = "local[2]"
  private val appName = "upTest"
  private val batchDuration = Seconds(1)
  private val checkpointDir = Files.createTempDirectory(appName).toString
  private var sc: SparkContext = _
  private var ssc: StreamingContext = _

  before {
    val conf = new SparkConf()
      .setMaster(master)
      .setAppName(appName)

    ssc = new StreamingContext(conf, batchDuration)
    ssc.checkpoint(checkpointDir)

    sc = ssc.sparkContext
    val lines = Seq(("123", "user feature"))
    val userFeature = sc.parallelize(lines)
    UserFeatureSuite.upQueue.add(("20160410", userFeature))
  }

  "UserFeature" should "load user feature failed" in {
    val upConfig = mock[Config]
    val upSource = new File("src/test/resources/up").getAbsolutePath
    (upConfig.getString _).expects("input_dir").returning("file://" + upSource)
    val noUpDate = "20160409"
    val userFeature = Try(UserFeature.loadJsonFeature(upConfig, noUpDate, sc))
    userFeature.isFailure should be(true)
    // userFeature.failed.get.printStackTrace()
  }

  "UserFeature" should "load user feature successfully" in {
    val upConfig = mock[Config]
    val upSource = new File("src/test/resources/up").getAbsolutePath
    (upConfig.getString _).expects("input_dir").returning("file://" + upSource)
    (upConfig.getString _).expects("category_key").returning("categories")
    (upConfig.getString _).expects("keyword_key").returning("keywords")
    (upConfig.getInt _).expects("top_categories").returning(5)
    (upConfig.getInt _).expects("top_keywords").returning(5)
    (upConfig.getInt _).expects("partitions").returning(100)
    val validUpDate = "20160410"
    val userFeature = Try(UserFeature.loadJsonFeature(upConfig, validUpDate, sc))
    userFeature.isSuccess should be(true)

    val collectUp = userFeature.get.collect()
    collectUp.length should be(1)
    collectUp(0)._1 should be("8e6b182ba3fc")
    println(collectUp(0)._2)
  }

  "UserFeature" should "parse json input correctly" in {
    val line = """{"uid" : "00000000000000000004999010640000", "version" : "0.1", "u_cat_len" : 0,"u_kw_len" : 20, "categories" : [{"name" : "news_entertainment", "weight" : 0.8093},{"name" : "news_astrology", "weight" : 0.4621},{"name" : "fitness", "weight" : 0.2449},{"name" : "science_all", "weight" : 0.1244},{"name" : "news_food", "weight" : 0.1244},{"name" : "news_culture", "weight" : 0.1244},{"name" : "news", "weight" : 0.1244},{"name" : "news_baby", "weight" : 0.1244}], "keywords" : [{"name" : "生肖", "weight" : 0.5546},{"name" : "星座", "weight" : 0.3584},{"name" : "杨幂", "weight" : 0.3584},{"name" : "女明星", "weight" : 0.3584},{"name" : "歌手", "weight" : 0.2449},{"name" : "婚礼", "weight" : 0.2449},{"name" : "生肖蛇", "weight" : 0.2449},{"name" : "生肖虎", "weight" : 0.2449},{"name" : "生肖狗", "weight" : 0.2449},{"name" : "财运", "weight" : 0.2449},{"name" : "赵丽颖", "weight" : 0.2449},{"name" : "电视剧", "weight" : 0.2449},{"name" : "韩国", "weight" : 0.2449},{"name" : "生肖羊", "weight" : 0.2449},{"name" : "婴儿", "weight" : 0.2449},{"name" : "演唱会", "weight" : 0.2449},{"name" : "爱情", "weight" : 0.2449},{"name" : "生肖鼠", "weight" : 0.2449},{"name" : "贾乃亮", "weight" : 0.2449},{"name" : "婚戒", "weight" : 0.1244},{"name" : "中国科学院国家天文台", "weight" : 0.1244},{"name" : "偶像来了", "weight" : 0.1244},{"name" : "颖儿", "weight" : 0.1244},{"name" : "星际迷航", "weight" : 0.1244},{"name" : "12生肖", "weight" : 0.1244},{"name" : "恐怖片", "weight" : 0.1244},{"name" : "行星", "weight" : 0.1244},{"name" : "nicu", "weight" : 0.1244},{"name" : "医院", "weight" : 0.1244},{"name" : "昆凌", "weight" : 0.1244},{"name" : "整容", "weight" : 0.1244},{"name" : "鸡蛋", "weight" : 0.1244},{"name" : "咖啡", "weight" : 0.1244},{"name" : "啤酒", "weight" : 0.1244},{"name" : "陈意涵", "weight" : 0.1244},{"name" : "热搜榜单", "weight" : 0.1244},{"name" : "日本人", "weight" : 0.1244},{"name" : "冰与火的青春", "weight" : 0.1244},{"name" : "小公主", "weight" : 0.1244},{"name" : "食材", "weight" : 0.1244},{"name" : "易时间", "weight" : 0.1244},{"name" : "系外行星", "weight" : 0.1244},{"name" : "煮鸡蛋", "weight" : 0.1244},{"name" : "周杰伦", "weight" : 0.1244},{"name" : "生肖龙", "weight" : 0.1244},{"name" : "中国好声音", "weight" : 0.1244},{"name" : "离婚", "weight" : 0.1244},{"name" : "体重", "weight" : 0.1244},{"name" : "炸鸡", "weight" : 0.1244},{"name" : "最佳女演员奖", "weight" : 0.1244},{"name" : "西班牙", "weight" : 0.1244},{"name" : "bangbang", "weight" : 0.1244},{"name" : "阿凡达", "weight" : 0.1244},{"name" : "王菲", "weight" : 0.1244},{"name" : "巡回演唱会", "weight" : 0.1244},{"name" : "脂溶性维生素", "weight" : 0.1244},{"name" : "张伟欣", "weight" : 0.1244},{"name" : "孙红雷", "weight" : 0.1244},{"name" : "喜剧", "weight" : 0.1244},{"name" : "韩国sbs电视台", "weight" : 0.1244},{"name" : "女演员", "weight" : 0.1244},{"name" : "天文学家", "weight" : 0.1244},{"name" : "宅男女神", "weight" : 0.1244},{"name" : "音乐", "weight" : 0.1244},{"name" : "迪丽", "weight" : 0.1244},{"name" : "克拉恋人", "weight" : 0.1244},{"name" : "演艺圈", "weight" : 0.1244},{"name" : "星际穿越", "weight" : 0.1244},{"name" : "张靓颖", "weight" : 0.1244},{"name" : "牛仔裤", "weight" : 0.1244},{"name" : "流苏裙", "weight" : 0.1244},{"name" : "胆固醇", "weight" : 0.1244},{"name" : "恒星系统", "weight" : 0.1244},{"name" : "当爱来的时候", "weight" : 0.1244},{"name" : "新加坡", "weight" : 0.1244},{"name" : "千山暮雪", "weight" : 0.1244},{"name" : "最后一首歌", "weight" : 0.1244},{"name" : "童星", "weight" : 0.1244},{"name" : "孙燕姿", "weight" : 0.1244},{"name" : "演艺界", "weight" : 0.1244},{"name" : "林青霞", "weight" : 0.1244},{"name" : "蛋白质", "weight" : 0.1244},{"name" : "沙门氏菌", "weight" : 0.1244},{"name" : "公主", "weight" : 0.1244},{"name" : "婴儿床", "weight" : 0.1244},{"name" : "模特", "weight" : 0.1244},{"name" : "演技", "weight" : 0.1244},{"name" : "英国", "weight" : 0.1244},{"name" : "综艺节目", "weight" : 0.1244},{"name" : "柳岩", "weight" : 0.1244},{"name" : "生肖马", "weight" : 0.1244},{"name" : "生肖猴", "weight" : 0.1244},{"name" : "金牛", "weight" : 0.1244},{"name" : "古力娜扎", "weight" : 0.1244},{"name" : "NASA", "weight" : 0.1244},{"name" : "钟乳石", "weight" : 0.1244},{"name" : "日本", "weight" : 0.1244},{"name" : "早产", "weight" : 0.1244},{"name" : "黑原话", "weight" : 0.1244},{"name" : "娱乐圈", "weight" : 0.1244},{"name" : "生肖鸡", "weight" : 0.1244},{"name" : "郑爽", "weight" : 0.1244},{"name" : "周董", "weight" : 0.1244},{"name" : "假发", "weight" : 0.1244},{"name" : "黄家驹", "weight" : 0.1244},{"name" : "卵磷脂", "weight" : 0.1244},{"name" : "甜馨", "weight" : 0.1244},{"name" : "女星", "weight" : 0.1244},{"name" : "太阳系外行星", "weight" : 0.1244},{"name" : "最佳女主角", "weight" : 0.1244},{"name" : "黄晓明", "weight" : 0.1244},{"name" : "蛋白", "weight" : 0.1244},{"name" : "台湾电影金马奖", "weight" : 0.1244},{"name" : "水煮鸡蛋", "weight" : 0.1244},{"name" : "开普勒太空望远镜", "weight" : 0.1244},{"name" : "巴黎", "weight" : 0.1244},{"name" : "婚姻", "weight" : 0.1244},{"name" : "唐嫣", "weight" : 0.1244},{"name" : "当婆婆遇上妈", "weight" : 0.1244},{"name" : "张学友", "weight" : 0.1244},{"name" : "风中奇缘", "weight" : 0.1244},{"name" : "虫洞", "weight" : 0.1244},{"name" : "高跟鞋", "weight" : 0.1244},{"name" : "生肖兔", "weight" : 0.1244},{"name" : "卡罗维发利国际电影节", "weight" : 0.1244},{"name" : "李小璐", "weight" : 0.1244},{"name" : "恋爱", "weight" : 0.1244},{"name" : "张翰", "weight" : 0.1244},{"name" : "十二生肖", "weight" : 0.1244},{"name" : "爱情长跑", "weight" : 0.1244},{"name" : "时间都去哪了", "weight" : 0.1244},{"name" : "硫化亚铁", "weight" : 0.1244},{"name" : "开普勒", "weight" : 0.1244},{"name" : "本命年", "weight" : 0.1244},{"name" : "巨蟹座", "weight" : 0.1244},{"name" : "产科男医生", "weight" : 0.1244},{"name" : "跑步圣经", "weight" : 0.1244},{"name" : "爸爸回来了", "weight" : 0.1244},{"name" : "生肖猪", "weight" : 0.1244},{"name" : "angelababy", "weight" : 0.1244},{"name" : "热巴", "weight" : 0.1244},{"name" : "婴儿房", "weight" : 0.1244},{"name" : "地球文明", "weight" : 0.1244},{"name" : "减肥", "weight" : 0.1244},{"name" : "古剑奇谭", "weight" : 0.1244},{"name" : "氨基酸", "weight" : 0.1244},{"name" : "升降台", "weight" : 0.1244},{"name" : "婚纱", "weight" : 0.1244},{"name" : "王骏迪", "weight" : 0.1244},{"name" : "中枢神经系统", "weight" : 0.1244},{"name" : "电影", "weight" : 0.1244},{"name" : "类地行星", "weight" : 0.1244},{"name" : "王铮亮", "weight" : 0.1244}], "doclist" : [10104262,10117597,10111685,10111412,10087790,10103926,10100135,10099327,10098135,10093627,10091296,10104156,10090254,10091016,10088647,10088768,10089155,10090612,10110864,10001504]}"""
    val (uid, features) = UserFeature.jsonLineParser(line, 3, 3, "categories", "keywords")
    uid should be("00000000000000000004999010640000")
    print("features : " + features.toString())

    val (uid1, features1) = UserFeature.jsonLineParser(line, 1000, 1000,  "categories", "keywords")
    uid1 should be("00000000000000000004999010640000")
    print("features : " + features1.toString())

    val (uid2, features2) = UserFeature.jsonLineParser(line, -1, -1, "categories", "keywords")
    uid2 should be("00000000000000000004999010640000")
    print("features : " + features2.toString())
  }

  "UserFeature" should "get list from json correctly" in {
    val line = """{"uid" : "00000000000000000004999010640000", "version" : "0.1", "u_cat_len" : 0, "u_kw_len" : 20, "categories" : [{"name" : "news_entertainment", "weight" : 0.8093},{"name" : "news_astrology", "weight" : 0.4621},{"name" : "fitness", "weight" : 0.2449},{"name" : "science_all", "weight" : 0.1244},{"name" : "news_food", "weight" : 0.1244},{"name" : "news_culture", "weight" : 0.1244},{"name" : "news", "weight" : 0.1244},{"name" : "news_baby", "weight" : 0.1244}], "keywords" : [{"name" : "生肖", "weight" : 0.5546},{"name" : "星座", "weight" : 0.3584},{"name" : "杨幂", "weight" : 0.3584},{"name" : "女明星", "weight" : 0.3584},{"name" : "歌手", "weight" : 0.2449},{"name" : "婚礼", "weight" : 0.2449},{"name" : "生肖蛇", "weight" : 0.2449},{"name" : "生肖虎", "weight" : 0.2449},{"name" : "生肖狗", "weight" : 0.2449},{"name" : "财运", "weight" : 0.2449},{"name" : "赵丽颖", "weight" : 0.2449},{"name" : "电视剧", "weight" : 0.2449},{"name" : "韩国", "weight" : 0.2449},{"name" : "生肖羊", "weight" : 0.2449},{"name" : "婴儿", "weight" : 0.2449},{"name" : "演唱会", "weight" : 0.2449},{"name" : "爱情", "weight" : 0.2449},{"name" : "生肖鼠", "weight" : 0.2449},{"name" : "贾乃亮", "weight" : 0.2449},{"name" : "婚戒", "weight" : 0.1244},{"name" : "中国科学院国家天文台", "weight" : 0.1244},{"name" : "偶像来了", "weight" : 0.1244},{"name" : "颖儿", "weight" : 0.1244},{"name" : "星际迷航", "weight" : 0.1244},{"name" : "12生肖", "weight" : 0.1244},{"name" : "恐怖片", "weight" : 0.1244},{"name" : "行星", "weight" : 0.1244},{"name" : "nicu", "weight" : 0.1244},{"name" : "医院", "weight" : 0.1244},{"name" : "昆凌", "weight" : 0.1244},{"name" : "整容", "weight" : 0.1244},{"name" : "鸡蛋", "weight" : 0.1244},{"name" : "咖啡", "weight" : 0.1244},{"name" : "啤酒", "weight" : 0.1244},{"name" : "陈意涵", "weight" : 0.1244},{"name" : "热搜榜单", "weight" : 0.1244},{"name" : "日本人", "weight" : 0.1244},{"name" : "冰与火的青春", "weight" : 0.1244},{"name" : "小公主", "weight" : 0.1244},{"name" : "食材", "weight" : 0.1244},{"name" : "易时间", "weight" : 0.1244},{"name" : "系外行星", "weight" : 0.1244},{"name" : "煮鸡蛋", "weight" : 0.1244},{"name" : "周杰伦", "weight" : 0.1244},{"name" : "生肖龙", "weight" : 0.1244},{"name" : "中国好声音", "weight" : 0.1244},{"name" : "离婚", "weight" : 0.1244},{"name" : "体重", "weight" : 0.1244},{"name" : "炸鸡", "weight" : 0.1244},{"name" : "最佳女演员奖", "weight" : 0.1244},{"name" : "西班牙", "weight" : 0.1244},{"name" : "bangbang", "weight" : 0.1244},{"name" : "阿凡达", "weight" : 0.1244},{"name" : "王菲", "weight" : 0.1244},{"name" : "巡回演唱会", "weight" : 0.1244},{"name" : "脂溶性维生素", "weight" : 0.1244},{"name" : "张伟欣", "weight" : 0.1244},{"name" : "孙红雷", "weight" : 0.1244},{"name" : "喜剧", "weight" : 0.1244},{"name" : "韩国sbs电视台", "weight" : 0.1244},{"name" : "女演员", "weight" : 0.1244},{"name" : "天文学家", "weight" : 0.1244},{"name" : "宅男女神", "weight" : 0.1244},{"name" : "音乐", "weight" : 0.1244},{"name" : "迪丽", "weight" : 0.1244},{"name" : "克拉恋人", "weight" : 0.1244},{"name" : "演艺圈", "weight" : 0.1244},{"name" : "星际穿越", "weight" : 0.1244},{"name" : "张靓颖", "weight" : 0.1244},{"name" : "牛仔裤", "weight" : 0.1244},{"name" : "流苏裙", "weight" : 0.1244},{"name" : "胆固醇", "weight" : 0.1244},{"name" : "恒星系统", "weight" : 0.1244},{"name" : "当爱来的时候", "weight" : 0.1244},{"name" : "新加坡", "weight" : 0.1244},{"name" : "千山暮雪", "weight" : 0.1244},{"name" : "最后一首歌", "weight" : 0.1244},{"name" : "童星", "weight" : 0.1244},{"name" : "孙燕姿", "weight" : 0.1244},{"name" : "演艺界", "weight" : 0.1244},{"name" : "林青霞", "weight" : 0.1244},{"name" : "蛋白质", "weight" : 0.1244},{"name" : "沙门氏菌", "weight" : 0.1244},{"name" : "公主", "weight" : 0.1244},{"name" : "婴儿床", "weight" : 0.1244},{"name" : "模特", "weight" : 0.1244},{"name" : "演技", "weight" : 0.1244},{"name" : "英国", "weight" : 0.1244},{"name" : "综艺节目", "weight" : 0.1244},{"name" : "柳岩", "weight" : 0.1244},{"name" : "生肖马", "weight" : 0.1244},{"name" : "生肖猴", "weight" : 0.1244},{"name" : "金牛", "weight" : 0.1244},{"name" : "古力娜扎", "weight" : 0.1244},{"name" : "NASA", "weight" : 0.1244},{"name" : "钟乳石", "weight" : 0.1244},{"name" : "日本", "weight" : 0.1244},{"name" : "早产", "weight" : 0.1244},{"name" : "黑原话", "weight" : 0.1244},{"name" : "娱乐圈", "weight" : 0.1244},{"name" : "生肖鸡", "weight" : 0.1244},{"name" : "郑爽", "weight" : 0.1244},{"name" : "周董", "weight" : 0.1244},{"name" : "假发", "weight" : 0.1244},{"name" : "黄家驹", "weight" : 0.1244},{"name" : "卵磷脂", "weight" : 0.1244},{"name" : "甜馨", "weight" : 0.1244},{"name" : "女星", "weight" : 0.1244},{"name" : "太阳系外行星", "weight" : 0.1244},{"name" : "最佳女主角", "weight" : 0.1244},{"name" : "黄晓明", "weight" : 0.1244},{"name" : "蛋白", "weight" : 0.1244},{"name" : "台湾电影金马奖", "weight" : 0.1244},{"name" : "水煮鸡蛋", "weight" : 0.1244},{"name" : "开普勒太空望远镜", "weight" : 0.1244},{"name" : "巴黎", "weight" : 0.1244},{"name" : "婚姻", "weight" : 0.1244},{"name" : "唐嫣", "weight" : 0.1244},{"name" : "当婆婆遇上妈", "weight" : 0.1244},{"name" : "张学友", "weight" : 0.1244},{"name" : "风中奇缘", "weight" : 0.1244},{"name" : "虫洞", "weight" : 0.1244},{"name" : "高跟鞋", "weight" : 0.1244},{"name" : "生肖兔", "weight" : 0.1244},{"name" : "卡罗维发利国际电影节", "weight" : 0.1244},{"name" : "李小璐", "weight" : 0.1244},{"name" : "恋爱", "weight" : 0.1244},{"name" : "张翰", "weight" : 0.1244},{"name" : "十二生肖", "weight" : 0.1244},{"name" : "爱情长跑", "weight" : 0.1244},{"name" : "时间都去哪了", "weight" : 0.1244},{"name" : "硫化亚铁", "weight" : 0.1244},{"name" : "开普勒", "weight" : 0.1244},{"name" : "本命年", "weight" : 0.1244},{"name" : "巨蟹座", "weight" : 0.1244},{"name" : "产科男医生", "weight" : 0.1244},{"name" : "跑步圣经", "weight" : 0.1244},{"name" : "爸爸回来了", "weight" : 0.1244},{"name" : "生肖猪", "weight" : 0.1244},{"name" : "angelababy", "weight" : 0.1244},{"name" : "热巴", "weight" : 0.1244},{"name" : "婴儿房", "weight" : 0.1244},{"name" : "地球文明", "weight" : 0.1244},{"name" : "减肥", "weight" : 0.1244},{"name" : "古剑奇谭", "weight" : 0.1244},{"name" : "氨基酸", "weight" : 0.1244},{"name" : "升降台", "weight" : 0.1244},{"name" : "婚纱", "weight" : 0.1244},{"name" : "王骏迪", "weight" : 0.1244},{"name" : "中枢神经系统", "weight" : 0.1244},{"name" : "电影", "weight" : 0.1244},{"name" : "类地行星", "weight" : 0.1244},{"name" : "王铮亮", "weight" : 0.1244}], "doclist" : [10104262,10117597,10111685,10111412,10087790,10103926,10100135,10099327,10098135,10093627,10091296,10104156,10090254,10091016,10088647,10088768,10089155,10090612,10110864,10001504]}"""
    val categoriesTop3 = UserFeature.getListFromJson("categories", line, 3)
    categoriesTop3 should be("news_entertainment\u00030.8093\u0002news_astrology\u00030.4621\u0002fitness\u00030.2449")

    val categoriesTop5 = UserFeature.getListFromJson("categories", line, 5)
    categoriesTop5 should be("news_entertainment\u00030.8093\u0002news_astrology\u00030.4621\u0002fitness\u00030.2449\u0002science_all\u00030.1244\u0002news_food\u00030.1244")

    val keywordsTop3 = UserFeature.getListFromJson("keywords", line, 3)
    keywordsTop3 should be("生肖\u00030.5546\u0002星座\u00030.3584\u0002杨幂\u00030.3584")

    val docListTop5 = UserFeature.getListFromJson("docList", line, 5)
    docListTop5 should be("news_entertainment\u00030.8093\u0002news_astrology\u00030.4621\u0002fitness\u00030.2449\u0002science_all\u00030.1244\u0002news_food\u00030.1244")
  }

  "UserFeature" should "get value for key from json correctly" in {
    val line = """{"uid" : "00000000000000000004999010640000", "version" : "0.1", "u_cat_len" : 0, "u_kw_len" : 20, "categories" : [{"name" : "news_entertainment", "weight" : 0.8093},{"name" : "news_astrology", "weight" : 0.4621},{"name" : "fitness", "weight" : 0.2449},{"name" : "science_all", "weight" : 0.1244},{"name" : "news_food", "weight" : 0.1244},{"name" : "news_culture", "weight" : 0.1244},{"name" : "news", "weight" : 0.1244},{"name" : "news_baby", "weight" : 0.1244}], "keywords" : [{"name" : "生肖", "weight" : 0.5546},{"name" : "星座", "weight" : 0.3584},{"name" : "杨幂", "weight" : 0.3584},{"name" : "女明星", "weight" : 0.3584},{"name" : "歌手", "weight" : 0.2449},{"name" : "婚礼", "weight" : 0.2449},{"name" : "生肖蛇", "weight" : 0.2449},{"name" : "生肖虎", "weight" : 0.2449},{"name" : "生肖狗", "weight" : 0.2449},{"name" : "财运", "weight" : 0.2449},{"name" : "赵丽颖", "weight" : 0.2449},{"name" : "电视剧", "weight" : 0.2449},{"name" : "韩国", "weight" : 0.2449},{"name" : "生肖羊", "weight" : 0.2449},{"name" : "婴儿", "weight" : 0.2449},{"name" : "演唱会", "weight" : 0.2449},{"name" : "爱情", "weight" : 0.2449},{"name" : "生肖鼠", "weight" : 0.2449},{"name" : "贾乃亮", "weight" : 0.2449},{"name" : "婚戒", "weight" : 0.1244},{"name" : "中国科学院国家天文台", "weight" : 0.1244},{"name" : "偶像来了", "weight" : 0.1244},{"name" : "颖儿", "weight" : 0.1244},{"name" : "星际迷航", "weight" : 0.1244},{"name" : "12生肖", "weight" : 0.1244},{"name" : "恐怖片", "weight" : 0.1244},{"name" : "行星", "weight" : 0.1244},{"name" : "nicu", "weight" : 0.1244},{"name" : "医院", "weight" : 0.1244},{"name" : "昆凌", "weight" : 0.1244},{"name" : "整容", "weight" : 0.1244},{"name" : "鸡蛋", "weight" : 0.1244},{"name" : "咖啡", "weight" : 0.1244},{"name" : "啤酒", "weight" : 0.1244},{"name" : "陈意涵", "weight" : 0.1244},{"name" : "热搜榜单", "weight" : 0.1244},{"name" : "日本人", "weight" : 0.1244},{"name" : "冰与火的青春", "weight" : 0.1244},{"name" : "小公主", "weight" : 0.1244},{"name" : "食材", "weight" : 0.1244},{"name" : "易时间", "weight" : 0.1244},{"name" : "系外行星", "weight" : 0.1244},{"name" : "煮鸡蛋", "weight" : 0.1244},{"name" : "周杰伦", "weight" : 0.1244},{"name" : "生肖龙", "weight" : 0.1244},{"name" : "中国好声音", "weight" : 0.1244},{"name" : "离婚", "weight" : 0.1244},{"name" : "体重", "weight" : 0.1244},{"name" : "炸鸡", "weight" : 0.1244},{"name" : "最佳女演员奖", "weight" : 0.1244},{"name" : "西班牙", "weight" : 0.1244},{"name" : "bangbang", "weight" : 0.1244},{"name" : "阿凡达", "weight" : 0.1244},{"name" : "王菲", "weight" : 0.1244},{"name" : "巡回演唱会", "weight" : 0.1244},{"name" : "脂溶性维生素", "weight" : 0.1244},{"name" : "张伟欣", "weight" : 0.1244},{"name" : "孙红雷", "weight" : 0.1244},{"name" : "喜剧", "weight" : 0.1244},{"name" : "韩国sbs电视台", "weight" : 0.1244},{"name" : "女演员", "weight" : 0.1244},{"name" : "天文学家", "weight" : 0.1244},{"name" : "宅男女神", "weight" : 0.1244},{"name" : "音乐", "weight" : 0.1244},{"name" : "迪丽", "weight" : 0.1244},{"name" : "克拉恋人", "weight" : 0.1244},{"name" : "演艺圈", "weight" : 0.1244},{"name" : "星际穿越", "weight" : 0.1244},{"name" : "张靓颖", "weight" : 0.1244},{"name" : "牛仔裤", "weight" : 0.1244},{"name" : "流苏裙", "weight" : 0.1244},{"name" : "胆固醇", "weight" : 0.1244},{"name" : "恒星系统", "weight" : 0.1244},{"name" : "当爱来的时候", "weight" : 0.1244},{"name" : "新加坡", "weight" : 0.1244},{"name" : "千山暮雪", "weight" : 0.1244},{"name" : "最后一首歌", "weight" : 0.1244},{"name" : "童星", "weight" : 0.1244},{"name" : "孙燕姿", "weight" : 0.1244},{"name" : "演艺界", "weight" : 0.1244},{"name" : "林青霞", "weight" : 0.1244},{"name" : "蛋白质", "weight" : 0.1244},{"name" : "沙门氏菌", "weight" : 0.1244},{"name" : "公主", "weight" : 0.1244},{"name" : "婴儿床", "weight" : 0.1244},{"name" : "模特", "weight" : 0.1244},{"name" : "演技", "weight" : 0.1244},{"name" : "英国", "weight" : 0.1244},{"name" : "综艺节目", "weight" : 0.1244},{"name" : "柳岩", "weight" : 0.1244},{"name" : "生肖马", "weight" : 0.1244},{"name" : "生肖猴", "weight" : 0.1244},{"name" : "金牛", "weight" : 0.1244},{"name" : "古力娜扎", "weight" : 0.1244},{"name" : "NASA", "weight" : 0.1244},{"name" : "钟乳石", "weight" : 0.1244},{"name" : "日本", "weight" : 0.1244},{"name" : "早产", "weight" : 0.1244},{"name" : "黑原话", "weight" : 0.1244},{"name" : "娱乐圈", "weight" : 0.1244},{"name" : "生肖鸡", "weight" : 0.1244},{"name" : "郑爽", "weight" : 0.1244},{"name" : "周董", "weight" : 0.1244},{"name" : "假发", "weight" : 0.1244},{"name" : "黄家驹", "weight" : 0.1244},{"name" : "卵磷脂", "weight" : 0.1244},{"name" : "甜馨", "weight" : 0.1244},{"name" : "女星", "weight" : 0.1244},{"name" : "太阳系外行星", "weight" : 0.1244},{"name" : "最佳女主角", "weight" : 0.1244},{"name" : "黄晓明", "weight" : 0.1244},{"name" : "蛋白", "weight" : 0.1244},{"name" : "台湾电影金马奖", "weight" : 0.1244},{"name" : "水煮鸡蛋", "weight" : 0.1244},{"name" : "开普勒太空望远镜", "weight" : 0.1244},{"name" : "巴黎", "weight" : 0.1244},{"name" : "婚姻", "weight" : 0.1244},{"name" : "唐嫣", "weight" : 0.1244},{"name" : "当婆婆遇上妈", "weight" : 0.1244},{"name" : "张学友", "weight" : 0.1244},{"name" : "风中奇缘", "weight" : 0.1244},{"name" : "虫洞", "weight" : 0.1244},{"name" : "高跟鞋", "weight" : 0.1244},{"name" : "生肖兔", "weight" : 0.1244},{"name" : "卡罗维发利国际电影节", "weight" : 0.1244},{"name" : "李小璐", "weight" : 0.1244},{"name" : "恋爱", "weight" : 0.1244},{"name" : "张翰", "weight" : 0.1244},{"name" : "十二生肖", "weight" : 0.1244},{"name" : "爱情长跑", "weight" : 0.1244},{"name" : "时间都去哪了", "weight" : 0.1244},{"name" : "硫化亚铁", "weight" : 0.1244},{"name" : "开普勒", "weight" : 0.1244},{"name" : "本命年", "weight" : 0.1244},{"name" : "巨蟹座", "weight" : 0.1244},{"name" : "产科男医生", "weight" : 0.1244},{"name" : "跑步圣经", "weight" : 0.1244},{"name" : "爸爸回来了", "weight" : 0.1244},{"name" : "生肖猪", "weight" : 0.1244},{"name" : "angelababy", "weight" : 0.1244},{"name" : "热巴", "weight" : 0.1244},{"name" : "婴儿房", "weight" : 0.1244},{"name" : "地球文明", "weight" : 0.1244},{"name" : "减肥", "weight" : 0.1244},{"name" : "古剑奇谭", "weight" : 0.1244},{"name" : "氨基酸", "weight" : 0.1244},{"name" : "升降台", "weight" : 0.1244},{"name" : "婚纱", "weight" : 0.1244},{"name" : "王骏迪", "weight" : 0.1244},{"name" : "中枢神经系统", "weight" : 0.1244},{"name" : "电影", "weight" : 0.1244},{"name" : "类地行星", "weight" : 0.1244},{"name" : "王铮亮", "weight" : 0.1244}], "doclist" : [10104262,10117597,10111685,10111412,10087790,10103926,10100135,10099327,10098135,10093627,10091296,10104156,10090254,10091016,10088647,10088768,10089155,10090612,10110864,10001504]}"""
    val uuid = UserFeature.getValueForKey("uid", line)
    uuid should be("00000000000000000004999010640000")

    val version = UserFeature.getValueForKey("version", line)
    version should be("0.1")

    val categories = UserFeature.getValueForKey("categories", line)
    categories should be ("{\"name\" : \"news_entertainment")

    val catLen = UserFeature.getValueForKey("u_cat_len", line)
    catLen should be("0")

    val kwLen = UserFeature.getValueForKey("u_kw_len", line)
    kwLen should be("20")
  }



  "UserFeature" should "join with event correctly" in {
    val event = mutable.Queue[RDD[(String, (String, Long, String, Double, String, String))]]()
    event.enqueue(sc.parallelize(Seq(("123", ("cid", 1440000000L,"PV", 1.0, "event feature", "content feature")))))
    val eventQueue = ssc.queueStream(event)
    val eventCount = sc.accumulator(0L, "Event Count Accumulator")
    eventQueue.foreachRDD(rdd => {
      rdd.foreach(record => eventCount += 1)
    })
    val joinResult = UserFeature.joinWithBatchEvent(UserFeatureSuite.getUp _, eventQueue)
    val joinCount = sc.accumulator(0L, "Join Count Accumulator")
    joinResult.foreachRDD(rdd => {
      rdd.foreach(record => joinCount += 1)
    })

    ssc.start()
    Thread.sleep(1500)
    eventCount.value should be(1)
    joinCount.value should be(1)
    ssc.stop()
  }

  after {
    if (sc != null) {
      sc.stop()
    }
    if (ssc != null) {
      ssc.stop()
    }
  }

}


