package com.cmcm.datahero.content.nrt.feature

import com.cmcm.datahero.content.nrt.config.{ConfigUnit, Configurable}
import com.cmcm.datahero.content.nrt.spark.SparkComponent
import org.scalamock.scalatest.MockFactory
import org.scalatest.{Matchers, FlatSpec, FunSuite}

/**
  * Created by hanbin on 15/8/27.
  */
class FeatureComponentSuite extends FlatSpec with Matchers with MockFactory with Configurable with SparkComponent with FeatureComponent {

  "FeatureComponent" should "contains setting " in {
    FeatureComponent.componentName should not be empty
    FeatureComponent.configFileName should not be empty
    FeatureComponent.defaultUPLoadPeriod should be(86400)
    FeatureComponent.defaultUPDatetimeFormat should not be empty
    FeatureComponent.defaultUPDatetimeOffset should be(-1)
  }

  "FeatureComponent" should "able to init feature component and load config" in {
    initFeatureComponent("conf/nr_us/item_cf/conf/feature.conf")
    featureConfigUnit shouldBe a [ConfigUnit]
  }

  "FeatureComponent" should "preStart successfully" in {
    preStart()
  }

  //"FeatureComponent" should "init successfully" in {
  //  init("src/test/resources/")
  //}

}
