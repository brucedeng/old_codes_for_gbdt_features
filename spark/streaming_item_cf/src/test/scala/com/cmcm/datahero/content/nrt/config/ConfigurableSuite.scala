package com.cmcm.datahero.content.nrt.config

import org.apache.spark.Logging
import org.scalatest.FunSuite

/**
  * Created by hanbin on 15/8/20.
  */
class ConfigurableSuite extends FunSuite with Configurable with Logging {

  var configDir: String = _

  test("Configurable can load the ConfigUnit correctly.") {
    assert(componentsConfigs.isEmpty)

    loadConfigUnit("config_unit.conf", "src/test/resources/config_unit.conf")
    assert(componentsConfigs.size == 1)

    val configUnit = getConfigUnit("config_unit.conf").get
    assert(configUnit.getConfigName.contains("config_unit.conf"))

    val configUnit2 = getConfigUnit("config_unit2.conf")
    assert(configUnit2.isEmpty)

    val configUnit3 = registComponent("config_unit.conf", "src/test/resources/config_unit.conf")
    assert(configUnit3.isDefined)

    val configUnit4 = registComponent("config_unit2.conf", "src/test/resources/config_unit2.conf")
    assert(configUnit4.isEmpty)

  }
}
