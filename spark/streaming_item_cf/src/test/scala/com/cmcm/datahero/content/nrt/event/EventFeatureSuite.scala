package com.cmcm.datahero.content.nrt.event

import org.apache.spark.Logging
import org.scalatest.FunSuite

/**
 * Created by hanbin on 16/1/7.
 */
class EventFeatureSuite extends FunSuite with Logging {

  test("feature can be generated and parsed correctly") {
    assert(EventFeature().genFeatureString() == "")
    assert(EventFeature().addFeature("U_CITY", "beijing")
      .addFeature("U_LAN", "en")
      .genFeatureString() == "U_CITY\003beijing\002U_LAN\003en")

    val eventfeature1 = EventFeature("U_CITY\003beijing\002U_LAN\003en")
    assert(eventfeature1.getFeatureValue("U_CITY").get == "beijing")
    assert(eventfeature1.getFeatureValue("U_LAN").get == "en")
    assert(eventfeature1.getFeatureValue("C_TYPE").isEmpty)
    eventfeature1.addFeature("C_TYPE", "1")
    assert(eventfeature1.getFeatureValue("C_TYPE").get == "1")

  }
}
