package com.cmcm.datahero.content.nrt.util

import org.I0Itec.zkclient.ZkClient
import org.I0Itec.zkclient.exception.{ZkNodeExistsException, ZkBadVersionException, ZkNoNodeException}
import org.scalatest.{FlatSpec, Matchers}
import org.apache.zookeeper.data.Stat
import org.mockito.Mockito._
import org.mockito.Matchers._
/**
 * Created by hanbin on 16/4/11.
 */

class ZKUtilsSuite extends FlatSpec with Matchers {

  "ZKUtil" should "throw exception" in {

    val zkClient = mock(classOf[ZkClient])
    when(zkClient.delete(anyString())).thenThrow(new RuntimeException())
    when(zkClient.getChildren(anyString())).thenThrow(new RuntimeException())
    when(zkClient.readData(anyString(), any[Stat]())).thenThrow(new RuntimeException())
    when(zkClient.deleteRecursive(anyString())).thenThrow(new RuntimeException())
    when(zkClient.writeData(anyString(), anyObject())).thenThrow(new RuntimeException())
    when(zkClient.writeDataReturnStat(anyString(), anyObject(), anyInt())).thenThrow(new ZkNoNodeException())
    when(zkClient.createPersistent(anyString(), any[AnyRef]())).thenThrow(new RuntimeException())
    when(zkClient.createEphemeral(anyString(), anyString())).thenThrow(new RuntimeException())

    intercept[Throwable] {
      ZKUtils.getChildrenParentMayNotExist(zkClient, "/")
    }

    intercept[Throwable] {
      ZKUtils.getChildren(zkClient, "/")
    }

    intercept[Throwable] {
      ZKUtils.readDataMaybeNull(zkClient, "/")
    }

    intercept[Throwable] {
      ZKUtils.readData(zkClient, "/")
    }

    intercept[Throwable] {
      ZKUtils.deletePathRecursive(zkClient, "/")
    }

    intercept[Throwable] {
      ZKUtils.deletePath(zkClient, "/")
    }

    intercept[Throwable] {
      ZKUtils.createPersistentPath(zkClient, "/")
    }

    intercept[Throwable] {
      ZKUtils.updateEphemeralPath(zkClient, "/", "null")
    }

    intercept[Throwable] {
      ZKUtils.conditionalUpdatePersistentPathIfExists(zkClient, "/", "null", 1)
    }

    intercept[Throwable] {
      ZKUtils.updatePersistentPath(zkClient, "/", "null")
    }

    intercept[Throwable] {
      ZKUtils.createEphemeralPathExpectConflictHandleZKBug(zkClient, "/", "null", "null", (String, Any) => true, 1)
    }

    intercept[Throwable] {
      ZKUtils.retry(3){
        throw new Exception()
      }
    }

  }

  "ZKUtil" should "run correctly" in {

    val zkClient = mock(classOf[ZkClient])
    when(zkClient.exists(anyString())).thenReturn(true)
    when(zkClient.getChildren(anyString())).thenReturn(new java.util.ArrayList[String]())
    when(zkClient.readData(anyString(), any[Stat]())).thenReturn("topic1\0020\002100\001topic2\0021\002101")
    when(zkClient.deleteRecursive(anyString())).thenReturn(true)
    when(zkClient.delete(anyString())).thenReturn(true)
    //when(zkClient.writeData(anyString(), anyObject()))
    when(zkClient.writeDataReturnStat(anyString(), anyObject(), anyInt())).thenReturn(new Stat())
    //when(zkClient.createPersistent(anyString(), any[AnyRef]())).thenReturn()
    when(zkClient.createPersistentSequential(anyString(), any[AnyRef]())).thenReturn("")
    //when(zkClient.createEphemeral(anyString(), anyString())).thenReturn()

    ZKUtils.pathExists(zkClient, "/") shouldBe true
    ZKUtils.getChildrenParentMayNotExist(zkClient, "/").size shouldBe 0
    ZKUtils.getChildren(zkClient, "/").size shouldBe 0
    ZKUtils.readDataMaybeNull(zkClient, "/")._1.isDefined shouldBe true
    ZKUtils.readData(zkClient, "/")._1.contains("topic1") shouldBe true
    ZKUtils.maybeDeletePath(zkClient, "/")
    ZKUtils.deletePathRecursive(zkClient, "/")
    ZKUtils.deletePath(zkClient, "/") shouldBe true
    ZKUtils.updateEphemeralPath(zkClient, "/", "")
    ZKUtils.conditionalUpdatePersistentPathIfExists(zkClient, "/", "", 1)._1 shouldBe true
    ZKUtils.conditionalUpdatePersistentPath(zkClient, "/", "", 1)._1 shouldBe true
    ZKUtils.updatePersistentPath(zkClient, "/", "")
    ZKUtils.createSequentialPersistentPath(zkClient, "/", "")
    ZKUtils.createPersistentPath(zkClient, "/", "")
    ZKUtils.createEphemeralPathExpectConflictHandleZKBug(zkClient, "/", "null", "null", (String, Any) => true, 1)
    ZKUtils.makeSurePersistentPathExists(zkClient, "/") shouldBe true
    when(zkClient.exists(anyString())).thenReturn(false)
    ZKUtils.makeSurePersistentPathExists(zkClient, "/") shouldBe false
    ZKUtils.updateAppOffsets(zkClient, "app", "")
    ZKUtils.getTopicsPartitionAndOffsets(zkClient, "app", Set("topic1")).size shouldBe 1

    ZKUtils.retry(3){
      ""
    }

    val b = ZKStringSerializer.serialize("null")
    ZKStringSerializer.deserialize(null)
    ZKStringSerializer.deserialize(b) shouldBe "null"

  }

  "ZKUtil" should "run correctly in unnormal condition" in {

    val zkClient = mock(classOf[ZkClient])
    when(zkClient.delete(anyString())).thenThrow(new ZkNoNodeException())
    when(zkClient.getChildren(anyString())).thenThrow(new ZkNoNodeException())
    when(zkClient.readData(anyString(), any[Stat]())).thenThrow(new ZkNoNodeException())
    when(zkClient.deleteRecursive(anyString())).thenThrow(new ZkNoNodeException())
    when(zkClient.writeData(anyString(), anyObject())).thenThrow(new ZkNoNodeException()).thenCallRealMethod()
    when(zkClient.writeDataReturnStat(anyString(), anyObject(), anyInt())).thenThrow(new RuntimeException()).thenThrow(new ZkBadVersionException())
    when(zkClient.createPersistent(anyString(), any[AnyRef]())).thenThrow(new ZkNoNodeException()).thenCallRealMethod()
    when(zkClient.createEphemeral(anyString(), anyString())).thenCallRealMethod().thenThrow(new ZkNoNodeException()).thenCallRealMethod().thenThrow(new ZkNodeExistsException())


    ZKUtils.getChildrenParentMayNotExist(zkClient, "/").size shouldBe 0
    ZKUtils.readDataMaybeNull(zkClient, "/")._1.isEmpty shouldBe true
    ZKUtils.deletePath(zkClient, "/") shouldBe false
    ZKUtils.updateEphemeralPath(zkClient, "/", "")
    ZKUtils.conditionalUpdatePersistentPathIfExists(zkClient, "/", "", 1)._1 shouldBe false
    ZKUtils.conditionalUpdatePersistentPath(zkClient, "/", "", 1)._1 shouldBe false
    ZKUtils.conditionalUpdatePersistentPath(zkClient, "/", "", 1)._1 shouldBe false
    ZKUtils.conditionalUpdatePersistentPath(zkClient, "/", "", 1, Some((zk: ZkClient, path: String, data: String) => (true,1)))._1 shouldBe true
    ZKUtils.updatePersistentPath(zkClient, "/", "")
    ZKUtils.createPersistentPath(zkClient, "/", "/root/app")
    ZKUtils.createEphemeralPathExpectConflict(zkClient, "/", "")

    TaskLogger.logInfo("finished")

  }

  "ZKUtil.createEphemeralPathExpectConflict" should "run correctly in unnormal condition" in {

    val zkClient = mock(classOf[ZkClient])
    when(zkClient.createEphemeral(anyString(), anyObject())).thenThrow(new ZkNodeExistsException())

    when(zkClient.readData(anyString(), any[Stat]())).thenThrow(new ZkNoNodeException()).thenThrow(new RuntimeException()).thenReturn("")
    intercept[Throwable] {
      ZKUtils.createEphemeralPathExpectConflict(zkClient, "/", "")
    }
    intercept[Throwable] {
      ZKUtils.createEphemeralPathExpectConflict(zkClient, "/", "")
    }
    ZKUtils.createEphemeralPathExpectConflict(zkClient, "/", "")
  }

  "ZKUtil.createEphemeralPathExpectConflictHandleZKBug" should "run correctly in unnormal condition" in {

    val zkClient = mock(classOf[ZkClient])
    when(zkClient.createEphemeral(anyString(), anyObject())).thenThrow(new ZkNodeExistsException())

    when(zkClient.readData(anyString(), any[Stat]())).thenThrow(new ZkNoNodeException()).thenReturn("null").thenThrow(new RuntimeException())

    intercept[Throwable] {
      ZKUtils.createEphemeralPathExpectConflictHandleZKBug(zkClient, "/", "null", "null", (String, Any) => true, 1)
    }
  }

  "ZKUtil.updatePersistentPath and conditionalUpdatePersistentPath" should "run correctly in unnormal condition" in {

    val zkClient = mock(classOf[ZkClient])
    when(zkClient.writeData(anyString(), anyObject())).thenThrow(new ZkNoNodeException()).thenCallRealMethod().thenThrow(new ZkNoNodeException())
    when(zkClient.createPersistent(anyString(), any[AnyRef]())).thenThrow(new ZkNodeExistsException()).thenThrow(new RuntimeException())
    when(zkClient.writeDataReturnStat(anyString(), anyObject(), anyInt())).thenReturn(new Stat()).thenThrow(new RuntimeException("runtime error"))

    ZKUtils.updatePersistentPath(zkClient, "/", "null")
    intercept[Throwable] {
      ZKUtils.updatePersistentPath(zkClient, "/", "null")
    }
    ZKUtils.conditionalUpdatePersistentPath(zkClient, "/", "", 1)._1 shouldBe true
    ZKUtils.conditionalUpdatePersistentPath(zkClient, "/", "", 1)._1 shouldBe false

  }

}




