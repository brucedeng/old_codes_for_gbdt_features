package com.cmcm.datahero.content.nrt.event

import java.util

import org.apache.spark.Logging

import scala.io.Source
import scala.collection.mutable.HashMap

/**
  * Created by hanbin on 15/9/10.
  */
trait Ipv4ToCityMapper {

  def map2City(ipStr: String): Option[String]

  def parseDataFile(dataFile: String)
}

object Ipv4ToCityMapper {

  def apply(region: String): Ipv4ToCityMapper = region match {
    case "CN" => new Ipv4ToCityMapperChina(new HashMap[String, String])
    case "IN" => new Ipv4ToCityMapperIndia(new util.TreeMap[Long, (Long, String)])
  }

  private class Ipv4ToCityMapperChina(ip2CityDataTable: HashMap[String, String]) extends Ipv4ToCityMapper with Logging with Serializable {
    override def map2City(ipStr: String) = {
      if (ipStr == null || ipStr.trim == "") {
        None
      } else {
        val ips: Array[String] = ipStr.trim.split(",")
        val cities: Array[String] = ips.flatMap(ip => {
          val ipFields = ip.split("\\.")
          if (ipFields.size == 4) {
            Some(ipFields(0) + "." + ipFields(1) + "." + ipFields(2) + ".0")
          } else {
            None
          }
        }).flatMap(ip2CityDataTable.get(_))
        if (cities.length > 0) {
          Some(cities(0))
        } else {
          None
        }
      }
    }

    override def parseDataFile(dataFile: String) {
      val source = Source.fromFile(dataFile)
      val lines = source.getLines()
      val kvPairs: Iterator[(String, String)] = lines.flatMap(line => {
        val fields = line.trim.split("\t")
        if (fields.size < 7) {
          None
        } else {
          val ip = fields(0)
          if (ip.endsWith(".0")) {
            Some((ip, fields(4)))
          } else {
            None
          }
        }
      })

      ip2CityDataTable ++= kvPairs
    }
  }

  private class Ipv4ToCityMapperIndia(var ip2CityDataTable: util.TreeMap[Long, (Long, String)]) extends Ipv4ToCityMapper with Logging with Serializable {
    override def map2City(ipStr: String) = {
      if (ipStr == null || ipStr.trim == "") {
        None
      } else {
        val ips: Array[String] = ipStr.trim.split(",")
        val cities = ips.flatMap(ip => {
          val ipFields = ip.split("\\.")
          if (ipFields.length == 4) {
            Some(ip2Long(ip))
          } else {
            None
          }
        }).flatMap(key => {
          if (key < ip2CityDataTable.firstKey() || key > ip2CityDataTable.lastKey()) {
            None
          } else {
            val tuple = ip2CityDataTable.get(ip2CityDataTable.floorKey(key))
            if (key < tuple._1) {
              Some(tuple._2)
            } else {
              None
            }
          }
        })

        if (cities.length > 0) {
          Some(cities(0))
        } else {
          None
        }
      }
    }

    override def parseDataFile(dataFile: String) {
      val source = Source.fromFile(dataFile, "UTF-8")
      val lines = source.getLines()
      val kvPairs: Iterator[(Long, (Long, String))] = lines.flatMap(line => {
        val fields = line.trim.split("\t")
        if (fields.size < 7) {
          None
        } else {
          val ipStart = {
            fields(0).split("\\.").length match {
              case 4 => Some(ip2Long(fields(0)))
              case _ => None
            }
          }
          val ipEnd = {
            fields(1).split("\\.").length match {
              case 4 => Some(ip2Long(fields(1)))
              case _ => None
            }
          }
          if (ipStart.isEmpty || ipEnd.isEmpty) {
            None
          } else {
            Some((ipStart.get, (ipEnd.get, fields(4))))
          }
        }
      })
      kvPairs.foreach(kv => ip2CityDataTable.put(kv._1, kv._2))
    }

    def ip2Long(ip: String): Long = {
      try {
        val atoms: Array[Long] = ip.split("\\.").map(java.lang.Long.parseLong(_))
        val result: Long = (3 to 0 by -1).foldLeft(0L)(
          (result, position) => result | (atoms(3 - position) << position * 8))

        result & 0xFFFFFFFF
      } catch {
        case e: Exception => 0
      }

    }

  }

}
