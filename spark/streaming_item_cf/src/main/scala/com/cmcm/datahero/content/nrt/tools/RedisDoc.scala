package com.cmcm.datahero.content.nrt.tools

import java.nio.charset.StandardCharsets

//import com.cmcm.datahero.content.nrt.component.gmp.GMPRedisProtobuf.GMPRecord
import org.joda.time.{DateTimeZone, DateTime}
import redis.clients.jedis.Jedis

/**
  * Created by shuwen on 16/6/27.
  */
object RedisDoc {
  var redisHost = ""
  var redisPort = 0
  var prefix = ""
  var dbNum = 0
/*
  def main(args: Array[String]): Unit = {

    if(args.length < 2) {
      println("Usage:\n\t1.type click/dwell/video\n\t2.content id list. seperated by comma")
      return
    }
    args(0) match {
      case "click" =>
        settingUSW2()
      case "dwell" =>
        settingDwell()
      case "video" =>
        settingVideo()
    }

    val recordKey = getKeyFromArg(args(1))
    val redisClient = new Jedis(redisHost, redisPort)
    redisClient.select(dbNum)
    recordKey.foreach(ckey => {
      val key = prefix + ckey
      val bytes = redisClient.get(key.getBytes(StandardCharsets.US_ASCII))
      if (bytes.nonEmpty) {
        val gmp = GMPRecord.parseFrom(bytes)
        val ts = gmp.getTimestamp+"000"
        val datetime = new DateTime(ts.toLong)
        println(key, gmp.getTimestamp, gmp.getImpression, gmp.getClick, datetime, datetime.toDateTime(DateTimeZone.forID("Asia/Chongqing")))
      } else {
        println("empty key", key)
      }
    })
  }

  def settingUSW2(): Unit ={
    redisHost = "featureserver.bgkplr.ng.0001.usw2.cache.amazonaws.com"
    redisPort = 6379
    prefix = "ng_"
    dbNum = 0
  }

  def settingVideo(): Unit ={
    redisHost = "featureserver.bgkplr.ng.0001.usw2.cache.amazonaws.com"
    redisPort = 6379
    prefix = "vnge_"
    dbNum = 0
  }

  def settingDwell(): Unit ={
    redisHost = "feature-server-test.mkjqd2.0001.apse1.cache.amazonaws.com"
    redisPort = 6379
    prefix = "ngd_"
    dbNum = 0
  }

  def getKeyFromArg(str:String):List[String] = {
    str.split(",").toList
  }
  */
}
