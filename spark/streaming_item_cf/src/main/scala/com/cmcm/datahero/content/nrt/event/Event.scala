package com.cmcm.datahero.content.nrt.event

import java.text.{SimpleDateFormat, DateFormat}
import java.util.TimeZone
import com.cmcm.datahero.content.nrt.config.ConfigUnit
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkContext, Logging}

import scala.util.Try

object Event extends Logging with Serializable {

  val pairDelimiter = "\002"
  val keyValueDelimiter = "\003"
  val defaultProductIds = "3"
  val defaultSources = "listpage,newscard"
  val defaultApiV = "2,3"

  var productIds: Set[String] = _
  var productIdsBroadcastVar: Broadcast[Set[String]] = _
  var sources: Set[String] = _
  var sourcesBroadcastVar: Broadcast[Set[String]] = _
  var apiv: Set[String] = _
  var apivBroadcastVar: Broadcast[Set[String]] = _
  val actMapping: Map[String, (String, String)] = Map("1" ->("PV", "impression"),
    "2" ->("CL", "click"),
    "3" ->("LD", "listpagedwelltime"),
    "4" ->("DW", "readtime"),
    "5" ->("CP", "completeness"),
    "6" ->("PR", "praise"),
    "7" ->("DL", "dislike"),
    "8" ->("CM", "comment"),
    "9" ->("SH", "share"),
    "10" ->("LI", "listpageimpression"),
    "11" ->("TR", "tread"),
    "12" ->("AF", "adsfill"),
    "13" ->("RT", "rsstimeout"),
    "14" ->("SR", "search"),
    "15" ->("PA", "pushacceptance"),
    "16" ->("MD", "mood"),
    "17" ->("CC", "channelchange"),
    "18" ->("CN", "channelnodata"),
    "101" ->("PS", "push")
  )
  var actMappingBroadcastVar: Broadcast[Map[String, (String, String)]] = _

  def initEventParser(eventConfigUnit: ConfigUnit) = {
    productIds = eventConfigUnit.getString("event.productids", defaultProductIds).trim.split(",").toSet
    sources = eventConfigUnit.getString("event.sources", defaultSources).trim.split(",").toSet
    apiv = eventConfigUnit.getString("event.apiv", defaultApiV).trim.split(",").toSet
  }

  def generateProductIdsBroadcastVar(sparkContext: SparkContext) = {
    logInfo("Init productIds broardcast value.")
    productIdsBroadcastVar = sparkContext.broadcast(productIds)
  }

  def generateSourcesBroadcastVar(sparkContext: SparkContext) = {
    logInfo("Init Sources broardcast value.")
    sourcesBroadcastVar = sparkContext.broadcast(sources)
  }

  def generateApiVBroadcastVar(sparkContext: SparkContext) = {
    logInfo("Init ApiV broardcast value.")
    apivBroadcastVar = sparkContext.broadcast(apiv)
  }

  def generateActMappingBroadcastVar(sparkContext: SparkContext) = {
    logInfo("Init ActMapping value.")
    actMappingBroadcastVar = sparkContext.broadcast(actMapping)
  }

  def parse(sc: SparkContext, impressionFile: String, clickFile: String): RDD[String] = {
    val impressionRDD: RDD[String] = sc.textFile(impressionFile)
    val clickRDD: RDD[String] = sc.textFile(clickFile)
    impressionRDD.union(clickRDD)
  }


  def getBatchId(date: String, window: Int): Long = {
    val sdf: DateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"))
    (sdf.parse(date).getTime() / (1000 * window)) + 1
  }

  def getListForKey(key: String, record: String): Array[String] = {
    val keyIndex = record.indexOf("\"" + key + "\":[")
    if (keyIndex < 0) {
      //      logWarning(s"Error when geting $key from $record")
      Array[String]()
    } else {
      val start = keyIndex + 4 + key.length
      val end = record.indexOf("]", start)
      if (end < 0 || (start > end)) {
        //        logWarning(s"Error when geting $key from $record")
        Array[String]()
      } else {
        val listString = record.substring(start, end)
        listString.split(",").map(field => {
          val fieldSize = field.length
          val startIndex = if (field.startsWith("\"")) 1 else 0
          val endIndex = if (field.endsWith("\"")) fieldSize - 1 else fieldSize
          field.substring(startIndex, endIndex)
        }).filter(_.length > 0)
      }
    }
  }

  def getValueForKey(key: String, record: String) = {
    val keyIndex = record.indexOf("\"" + key + "\":")
    if (keyIndex < 0) {
      //      logWarning(s"Error when geting $key from $record")
      ""
    } else {
      val startIndex = keyIndex + 3 + key.length
      val commaIndex = record.indexOf(",", startIndex)
      val antiBracketIndex = record.indexOf("}", startIndex)
      val endIndex =
        if (commaIndex != -1 && antiBracketIndex != -1)
          Math.min(record.indexOf(",", startIndex), record.indexOf("}", startIndex))
        else if (commaIndex != -1)
          commaIndex
        else
          antiBracketIndex

      if (endIndex < 0) {
        //        logWarning(s"Error when geting $key from $record")
        ""
      } else {
        val mendedStartInedex =
          if (record.charAt(startIndex) == '\"')
            startIndex + 1
          else
            startIndex
        val mendedEndInedex =
          if (record.charAt(endIndex - 1) == '\"')
            endIndex - 1
          else
            endIndex

        if (mendedStartInedex > mendedEndInedex) {
          //          logWarning(s"Error when geting $key from $record")
          ""
        } else
          record.substring(mendedStartInedex, mendedEndInedex)
      }
    }
  }

  /*
  def getEventParserForWorldwide(region: String, actFilter:List[String] = List("1","2"), window: Int = 600) = {
    parseEventForWorldwide(region,actFilter, actMappingBroadcastVar.value, window) _
  }

  def getEventParserForGMP(region: String, useDwell:Boolean = false, window: Int = 600) = {
    parseEventForGMP(region, useDwell, window) _
  }*/

  def getEventParserForCF(region: String, cp: Array[String], window: Int = 600) = {
    parseEventForCF(region, cp, window) _
  }

  /**
    *
    * @param countries country setting
    * @param window default 600
    * @param record event
    * @return (aid, contentid, eventFeature, batchId), (act,value))
    */
  def parseEventForCF(countries: String, cpFilter: Array[String], window: Int)
                     (record: String): List[((String, String, String, Long), (Double, Double))] = {
    if (isEventValid(record, countries, List("1","2"), cpFilter)) {
      val eventFeature = EventFeature()
      val act = getValueForKey("act", record) // pv=1, click=2, dwell=4, dislike=7

      val aid = getValueForKey("aid", record)
      val contentid = getValueForKey("contentid", record)
      val servertime_sec = getValueForKey("servertime_sec", record)
      val batchId = servertime_sec.toLong / window + 1

      setRegularFeature(record, eventFeature)
      act match {
        case "1" =>
          ((aid, contentid, eventFeature.genFeatureString(), batchId), (1.0, 0.0)) :: Nil  //View
        case "2" =>
          ((aid, contentid, eventFeature.genFeatureString(), batchId), (0.0, 1.0)) :: Nil //CLick
      }
    } else {
      Nil
    }
  }


  /**
    *
    * @param countries country setting
    * @param window    default 600
    * @param record    event
    * @return (aid, contentid, eventFeature, batchId), (act,value))
    */
  def parseEventForWorldwide(countries: String,
                             actFilter: List[String],
                             cTypeFilter:Array[String],
                             actMapping: Map[String, (String, String)],
                             window: Int = 600)
                            (record: String): List[((String, String, String, Long), (String, Double))] = {
    if (isEventValid(record, countries, actFilter, cTypeFilter)) {
      val eventFeature = EventFeature()
      val aid = getValueForKey("aid", record)
      val act = if (getValueForKey("level1_type", record).equals("3")) "101" else getValueForKey("act", record)
      val contentid = getValueForKey("contentid", record)
      val servertime_sec = getValueForKey("servertime_sec", record)
      val batchId = servertime_sec.toLong / window + 1

      act match {
        case "4" =>
          val dwellTime = Try(getValueForKey("dwelltime", record).toInt).getOrElse(0)
          val dwell = if (dwellTime > 600) Math.log(601) else Math.log(dwellTime + 1)
          setRegularFeature(record, eventFeature)
          ((aid, contentid, eventFeature.genFeatureString(), batchId), (actMapping.get(act).get._1, dwell)) :: Nil //Dwell
        case "5" =>
          val completeness: Double = 1.0 * Try(getValueForKey("completeness", record).toInt).getOrElse(0) / 100
          setRegularFeature(record, eventFeature)
          ((aid, contentid, eventFeature.genFeatureString(), batchId), (actMapping.get(act).get._1, completeness)) :: Nil //Dwell
        case _ =>
          setRegularFeature(record, eventFeature)
          ((aid, contentid, eventFeature.genFeatureString(), batchId), (actMapping.get(act).get._1, 1.0)) :: Nil //view
      }
    } else {
      Nil
    }
  }

  /**
    *
    * @param countries country setting
    * @param window    default 600
    * @param record    event
    * @return (aid, contentid, eventFeature, batchId), (act,value))
    */
  def parseEventForGMP(countries: String,
                       useDwell: Boolean,
                       cTypeFilter:Array[String],
                       window: Int = 600)
                      (record: String): List[((String, String, String, Long), (Double, Double))] = {
    if (isEventValid(record, countries, if (useDwell) List("1", "4") else List("1", "2"), cTypeFilter)) {
      val eventFeature = EventFeature()
      val act = getValueForKey("act", record) // pv=1, click=2, dwell=4, dislike=7

      val aid = getValueForKey("aid", record)
      val contentid = getValueForKey("contentid", record)
      val servertime_sec = getValueForKey("servertime_sec", record)
      val batchId = servertime_sec.toLong / window + 1

      setRegularFeature(record, eventFeature)
      act match {
        case "1" =>
          ((aid, contentid, eventFeature.genFeatureString(), batchId), (1.0, 0.0)) :: Nil //View
        case "2" =>
          ((aid, contentid, eventFeature.genFeatureString(), batchId), (0.0, 1.0)) :: Nil //CLick
        case "4" =>
          val dwellTime = Try(getValueForKey("dwelltime", record).toInt).getOrElse(0)
          val dwell = if (dwellTime > 600) Math.log(601) else Math.log(dwellTime + 1)
          ((aid, contentid, eventFeature.genFeatureString(), batchId), (0.0, dwell)) :: Nil //Dwell
      }
    } else {
      Nil
    }
  }

  def setRegularFeature(record: String, eventFeature: EventFeature): Unit = {
    val eventtime = getValueForKey("eventtime", record)
    val city = getValueForKey("city", record)
    val country = getValueForKey("country", record)
    val app_lan = getValueForKey("app_lan", record)
    val pid = getValueForKey("pid", record)
    val isNewUser = getValueForKey("new_user", record)
    val des = getValueForKey("des",record)
    val rid = des.split("\\|").filter( x => x.contains("rid"))
                  .headOption.getOrElse("rid=000").split("=").lift(1).getOrElse("000")


    eventFeature.addFeature("EVENT_TIME", eventtime)
    eventFeature.addFeature("U_CITY", city)
    eventFeature.addFeature("U_COUNTRY",
      if ("PY,PA,HN,AR,SV,VE,CU,DO,PE,CO,CL,CR,EC,NI,BO,GT,UY".contains(country)) "LM" else country)
    eventFeature.addFeature("NEW_USER", isNewUser)
    eventFeature.addFeature("U_LAN", app_lan)
    eventFeature.addFeature("PID", pid)
    eventFeature.addFeature("RID", rid)
  }

  def isEventValid(record: String,
                   countries: String,
                   actFilter: List[String],
                   cTypeFilter:Array[String]): Boolean = {
    val pid = getValueForKey("pid", record)
    val position = getValueForKey("level1", record)
    val position_type = getValueForKey("level1_type", record)
    val country = getValueForKey("country", record)
    val app_lan = getValueForKey("app_lan", record)
    val ctypeRaw = getValueForKey("ctype", record)
    val ctype = if (ctypeRaw.length>0) ctypeRaw else "1"

    val des = getValueForKey("des",record)
    val rid = des.split("\\|").filter( x => x.contains("rid"))
      .headOption.getOrElse("rid=000").split("=").lift(1).getOrElse("000")

    //    eventFeature.addFeature("C_TYPE", if (ctype.length > 0) ctype else "1")

    def isCMTab: Boolean = {
      pid.equals("1") && position.equals("1") && position_type.equals("1")
    }
    def isCMTabScreen: Boolean = {
      pid.equals("1") && position.equals("11") && position_type.equals("1")
    }
    def isCMBrowserTab: Boolean = {
      pid.equals("3") && position.equals("1")
    }
    def isCMSPrivacy: Boolean = {
      pid.equals("9") && position.equals("2")
    }
    def isInstaNews: Boolean = {
      pid.equals("11") && position.equals("1") && (position_type.equals("1") || position_type.equals("10"))
    }
    def isInstaNewsV2: Boolean = {
      pid.equals("11") && position.equals("1") && position_type.equals("1")
    }
    def isNewsRepublic: Boolean = {
      pid.equals("14") && position.equals("1") && position_type.equals("1")
    }
    def isNewsRepublicV2: Boolean = {
      pid.equals("14") &&
        (
          (position.equals("13") && position_type.equals("13")) ||
            (position.equals("14") && position_type.equals("14")) ||
            (position.equals("15") && position_type.equals("15"))
          )
    }
    def isNewsRepublicV3: Boolean = {
      pid.equals("14") && (position.equals("19") && position_type.equals("19"))
    }
    def isNewsRepublicPush: Boolean = {
      pid.equals("14") && position.equals("0") && position_type.equals("3")
    }
    def isVideoApp: Boolean = {
      pid.equals("15") && position.equals("1") && position_type.equals("1")
    }
    def isNRUS: Boolean = {
      pid.equals("17") && position.equals("1") && position_type.equals("1")
    }
    def isValidContent: Boolean = {
      val aid = getValueForKey("aid", record)
      val contentid = getValueForKey("contentid", record)
      val act = getValueForKey("act", record)
      val isActValid = actFilter.contains(act)
      cTypeFilter.contains(ctype) && aid.length > 0 && contentid.length > 0 && country.length == 2 && app_lan.length == 2 && isActValid
    }

    isValidContent && (countries match {
    //isValidContent && (countries match {
    case "US" => country.equals("US") && isCMTab // for us_cfb
      case "NR_US" => app_lan.equals("en") && country.equals("US") && (isNewsRepublicV2 || isNewsRepublic || isNRUS)
      case "NR_ES_US" => app_lan.equals("es") && country.equals("US") && (isNewsRepublicV2 || isNewsRepublic || isNRUS)
      case "ALL_US" => country.equals("US") && (isCMTab || isCMTabScreen || isNewsRepublic)
      case "RU" => country.equals("RU") && isCMTab // for ru_cfb
      case "NR_RU" => country.equals("RU") && (isNewsRepublic || isNewsRepublicV2 || isNewsRepublicV3) // for ru_cfb
      case "IN" => country.equals("IN") && isInstaNews
      case "IN_en" => country.equals("IN") && isInstaNews && app_lan.equals("en")
      case "IN_hi" => country.equals("IN") && isInstaNews && app_lan.equals("hi")
      case "IN_hi_V2" => country.equals("IN") && isInstaNewsV2 && app_lan.equals("hi")
      case "US_N_CA" => (country.equals("US") || country.equals("CA")) && isNewsRepublic
      case "OTHER_THAN_IN_US_RU" => !country.equals("IN") && !country.equals("US") && !country.equals("RU") && isCMTab //for multilan_cfb
      case "WORLDWIDE" => (country.equals("IN") && isInstaNews) ||
        (isCMTab && !country.equals("IN")) ||
        (isCMTabScreen && !country.equals("IN")) || //for group_gmp
        (country.equals("US") && (isCMBrowserTab || isCMSPrivacy))
      case "WORLDWIDE_NR" => isNewsRepublic || isNRUS || isNewsRepublicV2
      case "WORLDWIDE_VIDEO" => (country.equals("IN") && isInstaNews) ||
        (!country.equals("IN") && (isVideoApp || isCMTabScreen)) ||
        isNewsRepublic
      case _ => false
    })
  }
}
