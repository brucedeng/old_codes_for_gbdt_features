package com.cmcm.datahero.content.nrt.component

import com.cmcm.datahero.content.nrt.config.{ConfigUnit, Configurable}

/**
 * Created by hanbin on 15/9/2.
 * There will be multiple componets that will cooperate to implement
 * [[com.cmcm.datahero.content.nrt.app.NRTApp]]
 */
trait Component { this: Configurable =>

  def init(configDir: String) = {}
  def preStart() = {}

  def loadComponentConfig(name: String, eventConfigFile: String): ConfigUnit = {
    logInfo(s"Loading $name component config from $eventConfigFile...")
    val componentConfigUnit = registComponent(name, eventConfigFile)
    if (componentConfigUnit.isEmpty) {
      throw new IllegalStateException(s"Can not init $name component from $eventConfigFile")
    }
    return componentConfigUnit.get
  }

}
