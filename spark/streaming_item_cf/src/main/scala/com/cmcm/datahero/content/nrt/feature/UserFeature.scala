package com.cmcm.datahero.content.nrt.feature

import java.net.URI

import com.typesafe.config.Config
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{PathFilter, FileSystem, Path}
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.{Logging, HashPartitioner, SparkContext}

import scala.util.Try
import scala.util.control.Breaks._

/**
*  Created by wangwei5 on 15/8/12.
*/

case class UPNotReadyException(msg: String) extends Throwable

object UserFeature extends Logging {

  val fieldDelimiter = "\001"
  val pairDelimiter = "\002"
  val keyValueDelimiter = "\003"

  def jsonLineParser(line: String, topCategories: Int, topKeywords: Int,
                     categoryKey: String, keywordKey: String): (String, String) = {
    try {
      val uuid = getValueForKey("uid", line)
      val categories = getListFromJson(categoryKey, line, topCategories)
      val keywords = getListFromJson(keywordKey, line, topKeywords)
      val gender = getValueForKey("gender", line)
      val age = getValueForKey("age", line)
      (uuid, categories + fieldDelimiter + keywords + fieldDelimiter + gender + fieldDelimiter + age)
    } catch {
      case e: Exception =>
        ("", "")
    }
  }

  def dirContainsSuccess(hdfsDir: String): Boolean = {
    val hdfs = FileSystem.get(URI.create(hdfsDir), new Configuration())
    val fileNameList = hdfs.listStatus(new Path(hdfsDir)).map(filePath => filePath.getPath.getName).toList
    fileNameList.exists(fileName => fileName.contentEquals("_SUCCESS"))
  }

  def loadJsonFeature(upConfig: Config, date: String, sc: SparkContext): RDD[(String, String)] =  {
    val upPath = upConfig.getString("input_dir") + "/" + date
    if (dirContainsSuccess(upPath) == false) {
      throw UPNotReadyException(upPath + " not ready")
    }
    val categoryKey = Try(upConfig.getString("category_key")).getOrElse("categories")
    val keywordKey = Try(upConfig.getString("keyword_key")).getOrElse("keywords")
    logInfo(s"User feature category key is $categoryKey")
    logInfo(s"User feature keyword key is $keywordKey")

    val hdfs = FileSystem.get(URI.create(upPath),new Configuration())
    val files: Array[String] = hdfs.listStatus(new Path(upPath), new PathFilter() {
      override def accept(path: Path): Boolean = {
        (! path.toString.contains("_SUCCESS")) && (! path.toString.contains("_temporary"))
      }
    }).map(status => status.getPath.toString)
    val userProifleLines: RDD[String] = sc.textFile(files.mkString(","))
    val top_categories = upConfig.getInt("top_categories")
    val top_keywords = upConfig.getInt("top_keywords")
    val userFeature = userProifleLines.map(line => {
      UserFeature.jsonLineParser(line, top_categories, top_keywords, categoryKey, keywordKey)
    }).filter(_._1.length > 0)
    userFeature.partitionBy(new HashPartitioner(upConfig.getInt("partitions")))
  }

  /**
    *
    * @param getUserFeatureRDD Option[(dateString, UserProfile)]
    * @param batchEvent (uid, (contentId, batchId, act, value, eventFeature, contentFeature))
    * @return ((uid, contentId), (batchId, act, value, eventFeature, contentFeature, userFeature))
    */
  def joinWithBatchEvent(getUserFeatureRDD: () =>Option[(String, RDD[(String, String)])],
                         batchEvent: DStream[(String, (String, Long, String, Double, String, String))]): DStream[((String, String), (Long, String, Double, String, String, Option[String]))] = {
    batchEvent.transform(eventRDD => {
      val dateAndUP = getUserFeatureRDD().get
      val dateString = dateAndUP._1
      val up = dateAndUP._2
      logInfo(s"Now, use user profile of $dateString")
      eventRDD.leftOuterJoin(up).map(result =>
        ((result._1, result._2._1._1), (result._2._1._2, result._2._1._3, result._2._1._4, result._2._1._5, result._2._1._6, result._2._2))
      )
    })
  }


  /**
    *
    * @param getUserFeatureRDD Option[(dateString, UserProfile)]
    * @param batchEvent (uid, (contentId, batchId, act, value, eventFeature, contentFeature))
    * @return ((uid, contentId), (batchId, act, value, eventFeature, contentFeature, userFeature))
    */
  def joinWithBatchEventCN(getUserFeatureRDD: () =>Option[(String, RDD[(String, String)])],
                         batchEvent: DStream[(String, (String, Long, Double, Double, String, String))]): DStream[((String, String), (Long, Double, Double, String, String, Option[String]))] = {
    batchEvent.transform(eventRDD => {
      val dateAndUP = getUserFeatureRDD().get
      val dateString = dateAndUP._1
      val up = dateAndUP._2
      logInfo(s"Now, use user profile of $dateString")
      eventRDD.leftOuterJoin(up).map(result =>
        ((result._1, result._2._1._1), (result._2._1._2, result._2._1._3, result._2._1._4, result._2._1._5, result._2._1._6, result._2._2))
      )
    })
  }

  /**
    *
    * @param getUserFeatureRDD DStream with format (uid, userFeature)
    * @param batchEvent DStream with format (uid,(cid,batchId,show,click,eventFeature,contentFeature,pid))
    * @return DStream with format ((uid,cid),(batchId,show,click,eventFeature,contentFeature,userFeature,pid))
    */
  def joinWithBatchEventForCtr(getUserFeatureRDD: () =>Option[(String, RDD[(String, String)])],
                         batchEvent: DStream[(String, (String, Long, Double, Double, String, String,String))]): DStream[((String, String), (Long, Double, Double, String, String, Option[String],String))] = {
    batchEvent.transform(eventRDD => {
      val dateAndUP = getUserFeatureRDD().get
      val dateString = dateAndUP._1
      val up = dateAndUP._2
      logInfo(s"Now, use user profile of $dateString")
      eventRDD.leftOuterJoin(up).map(result =>
        ((result._1, result._2._1._1), (result._2._1._2, result._2._1._3, result._2._1._4, result._2._1._5, result._2._1._6, result._2._2,result._2._1._7))
      )
    })
  }


  def getListFromJson(key: String ,line: String, top: Int) : String = {
    val sb = new StringBuilder
    var newLine = line.substring(line.indexOf("\"" + key + "\" : ") + 5 + key.length)
    newLine = newLine.substring(0, newLine.indexOf("]") + 1)
    var index = 0
    breakable {
      while (index < (if (top < 0) Integer.MAX_VALUE else top)) {
        if (newLine.indexOf("}") == -1) {
          break()
        }
        val name = getValueForKey("name", newLine)
        val weight = getValueForKey("weight", newLine).toDouble
        sb.append(name + keyValueDelimiter + weight + pairDelimiter)
        newLine = newLine.substring(newLine.indexOf("}") + 1)
        index += 1
      }
    }
    if (index > 0)
      sb.dropRight(1).toString()
    else
      sb.toString()
  }

  def getValueForKey(key: String, record: String) = {
    val startIndex = record.indexOf("\"" + key + "\" : " ) + 5 + key.length
    val commaIndex = record.indexOf(",", startIndex)
    val antiBracketIndex = record.indexOf("}", startIndex)
    val endIndex = if (commaIndex != -1 && antiBracketIndex != -1)  Math.min(record.indexOf(",", startIndex),
      record.indexOf("}", startIndex)) else if (commaIndex != -1) commaIndex else antiBracketIndex
    val mendedStartInedex =
      if (record.charAt(startIndex) == '\"' || record.charAt(startIndex) == '[')
        startIndex + 1
      else
        startIndex
    val mendedEndInedex =
      if (record.charAt(endIndex - 1) == '\"' || record.charAt(endIndex - 1) == ']')
        endIndex - 1
      else
        endIndex

    if (mendedStartInedex > mendedEndInedex)
      ""
    else
      record.substring(mendedStartInedex, mendedEndInedex)
  }

}
