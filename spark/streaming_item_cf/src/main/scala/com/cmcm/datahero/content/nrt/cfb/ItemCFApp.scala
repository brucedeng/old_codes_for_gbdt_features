package com.cmcm.datahero.content.nrt.cfb

import com.cmcm.datahero.content.nrt.app.NRTApp
import com.cmcm.datahero.content.nrt.component.ItemCFModelComponent
import com.cmcm.datahero.content.nrt.config.Configurable
import com.cmcm.datahero.content.nrt.event.EventComponent
import com.cmcm.datahero.content.nrt.model.{CFWorkflow}
import com.cmcm.datahero.content.nrt.spark.SparkComponent

object ItemCFApp extends NRTApp
                         with Configurable
                         with SparkComponent
                         with EventComponent
                         with ItemCFModelComponent
                         with CFWorkflow