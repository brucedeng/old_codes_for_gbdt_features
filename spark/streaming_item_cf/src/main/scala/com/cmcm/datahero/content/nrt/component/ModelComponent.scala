package com.cmcm.datahero.content.nrt.component

import com.cmcm.datahero.content.nrt.config.{ConfigUnit, Configurable}
import com.cmcm.datahero.content.nrt.event.{Event, EventComponent, EventFeature}
import com.cmcm.datahero.content.nrt.feature._
import com.cmcm.datahero.content.nrt.spark.SparkComponent
import com.cmcm.datahero.content.nrt.util.TaskLogger
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.Seconds
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.{HashPartitioner, Logging, SparkContext}
import org.joda.time.DateTime

import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.util.Try

/**
  * ModelComponent is used to generate the model data
  * 1, implement the strategy of generating model.
  * 2, provide the interface to generate model from input data
  */
object ModelComponent extends Logging {
  val componentName = "model_component"
  val configFileName = "model.conf"
  val defaultModelOutputDest = "file"
  val defaultModelOutputFilePrefix = "/tmp/news_model/item_cf"
  val defaultPatitionsForFetchingCP = 10
  val defaultDecayWindow = 600
  //var monitor: MonitorProxy = _
  //var monitorBroadcastVar: Broadcast[MonitorProxy] = _

  def loadInitStateRDD(sparkContext: SparkContext, path: String): RDD[(String, (Long, Double, Double, Double, Double))] = {
    sparkContext.textFile(path).flatMap(line => {
      val fields = line.split("\t")
      if (fields.length == 6) {
        Some((fields(0), (fields(1).toLong, fields(2).toDouble, fields(3).toDouble, fields(4).toDouble, fields(5).toDouble)))
      } else {
        None
      }
    })
  }

  /**
    *
    * @param sparkContext
    * @param path
    * @return RDD
    */
  def loadInitStateRDDForMultiLan(sparkContext: SparkContext, path: String): RDD[(String, (Long, Double, Double, Double, Double, String, String))] = {
    sparkContext.textFile(path).flatMap(line => {
      val fields = line.split("\t")
      if (fields.length == 6) {
        Some((fields(0), (fields(1).toLong, fields(2).toDouble, fields(3).toDouble, fields(4).toDouble, fields(5).toDouble, "", "")))
      } else if (fields.length == 7) {
        Some((fields(0), (fields(1).toLong, fields(2).toDouble, fields(3).toDouble, fields(4).toDouble, fields(5).toDouble, fields(6).toString, "")))
      } else if (fields.length == 8) {
        Some((fields(0), (fields(1).toLong, fields(2).toDouble, fields(3).toDouble, fields(4).toDouble, fields(5).toDouble, fields(6).toString, fields(7).toString)))
      } else {
        None
      }
    })
  }

  def dirContainsSuccess(hdfsDir: String): Boolean = {
    val hdfs = FileSystem.get(new Configuration())
    val fileNameList = hdfs.listStatus(new Path(hdfsDir)).map(filePath => filePath.getPath.toString).toList
    fileNameList.exists(fileName => fileName.contains("_SUCCESS"))
  }

  /**
    * find the latest state dir with _SUCCESS
    *
    * @param sparkContext
    * @param path
    * @return
    */
  def loadLatestStateRDD(sparkContext: SparkContext, path: String): RDD[(String, (Long, Double, Double, Double, Double))] = {
    val hdfs = FileSystem.get(new Configuration())
    val pathDir = path.substring(0, path.lastIndexOf("/"))
    val latestStatusDir = hdfs.listStatus(new Path(pathDir)).map(filePath => filePath.getPath.toString).sorted.reverse.find(dirContainsSuccess)
    logInfo("The initial state RDD file name is: " + latestStatusDir.get)
    sparkContext.textFile(latestStatusDir.get).flatMap(line => {
      val fields = line.split("\t")
      if (fields.length == 6) {
        Some(fields(0), (fields(1).toLong, fields(2).toDouble, fields(3).toDouble, fields(4).toDouble, fields(5).toDouble))
      } else {
        None
      }
    })

  }

  // find the latest state dir with _SUCCESS
  def loadLatestStateRDDForMultiLan(sparkContext: SparkContext, path: String): RDD[(String, (Long, Double, Double, Double, Double, String, String))] = {
    val hdfs = FileSystem.get(new Configuration())
    val pathDir = path.substring(0, path.lastIndexOf("/"))
    val latestStatusDir = hdfs.listStatus(new Path(pathDir)).map(filePath => filePath.getPath.toString).sorted.reverse.find(dirContainsSuccess)
    logInfo("The initial state RDD file name is: " + latestStatusDir.get)
    sparkContext.textFile(latestStatusDir.get).flatMap(line => {
      val fields = line.split("\t")
      if (fields.length == 6) {
        Some(fields(0), (fields(1).toLong, fields(2).toDouble, fields(3).toDouble, fields(4).toDouble, fields(5).toDouble, "", ""))
      } else if (fields.length == 7) {
        Some(fields(0), (fields(1).toLong, fields(2).toDouble, fields(3).toDouble, fields(4).toDouble, fields(5).toDouble, fields(6).toString, ""))
      } else if (fields.length == 8) {
        Some(fields(0), (fields(1).toLong, fields(2).toDouble, fields(3).toDouble, fields(4).toDouble, fields(5).toDouble, fields(6).toString, fields(7).toString))
      } else {
        None
      }
    })

  }
}

trait ModelComponent extends Component {
  this: Configurable with SparkComponent with EventComponent =>

  override def init(configDir: String) = {
    super.init(configDir)
    initModelComponent(configDir + "/" + ModelComponent.configFileName)
    //ModelComponent.monitor = new MonitorProxy()
    //ModelComponent.monitor.init(modelConfigUnit)
    //ModelComponent.monitorBroadcastVar = sparkContext.broadcast(ModelComponent.monitor)
  }

  override def preStart() = {
    super.preStart()
  }

  def modelConfigUnit = {
    getConfigUnit(ModelComponent.componentName).get
  }

  def initModelComponent(modelConfigFile: String) = {
    logInfo("Initializing model component...")
    loadComponentConfig(ModelComponent.componentName, modelConfigFile)
  }

  def getEventDStream() = {
    getInputDstream(streamingContext)(eventConfigUnit)
  }

}



