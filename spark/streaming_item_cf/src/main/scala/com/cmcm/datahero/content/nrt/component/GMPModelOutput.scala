package com.cmcm.datahero.content.nrt.component

import java.nio.charset.StandardCharsets
import java.util.Properties

import kafka.producer.{KeyedMessage, Producer, ProducerConfig}
import org.apache.spark.Logging
import org.apache.spark.streaming.dstream.DStream
import org.joda.time._
import redis.clients.jedis.Jedis

import scala.util.Try

/**
  * Created by hanbin on 15/9/6.
  */
trait GMPModelOutput extends Serializable with Logging {
  def output(dStream: DStream[(String, Long, Double, Double)], appendedKey: String = ""): Option[DStream[Int]]
}

class GMPModelFileOutput(val outputFilePrefix: String, val window: Int, val gmpParam: Int) extends GMPModelOutput {

  override def output(dStream: DStream[(String, Long, Double, Double)], appendedKey: String = ""): Option[DStream[Int]] = {
    dStream.map(record => {
      val gmp = record._4 / (record._3 + gmpParam)
      record._1 + "\t" + record._2 * window + "\t" + record._3 + "\t" + record._4 + "\t" + gmp
    }).saveAsTextFiles(outputFilePrefix)
    None
  }
}

/*
class GMPModelRedisFullOutput(val redisHost: String, val redisPort: Int, val redisDB: Int,
                              val redisPassword: String, val expireTime: Int,
                              val window: Int, val gmpParam: Int, val prefix: String = "ng_") extends GMPModelOutput {

  logInfo(s"Model redis full output: $redisHost:$redisPort, prefix: $prefix")

  override def output(dStream: DStream[(String, Long, Double, Double)], appendedKey: String = ""): Option[DStream[Int]] = {
    val outputDStream = dStream.transform(rdd => {
      rdd.mapPartitions(it => {
        Iterator(fullUpdate(it))
      })
    })
    Some(outputDStream)
  }

  def fullUpdate(partition: Iterator[(String, Long, Double, Double)]): Int = {
    val retryMax = 3
    val list = partition.toList
    var failureCount = writeToRedis(list)
    var retryNum = 1
    while (failureCount > 1 && retryNum <= retryMax) {
      retryNum += 1
      failureCount = writeToRedis(list)
    }
    if (failureCount > 0) log.error(s"Fail write to redis $redisHost:$redisPort for $failureCount records after $retryNum retries")
    failureCount
  }


  def writeToRedis(list: List[(String, Long, Double, Double)]): Int = {
    var redisClient: Jedis = null
    var failureCount = 0
    try {
      redisClient = new Jedis(redisHost, redisPort)
      if (redisPassword.size > 0)
        redisClient.auth(redisPassword)

      redisClient.select(redisDB)
      val pipeline = redisClient.pipelined()
      val gmpRecordBuilder: GMPRecord.Builder = GMPRecord.newBuilder()

      list.foreach((record: (String, Long, Double, Double)) => {
        val gmp = record._4 / (record._3 + gmpParam)
        gmpRecordBuilder.setTimestamp(record._2 * window)
        gmpRecordBuilder.setImpression(record._3)
        gmpRecordBuilder.setClick(record._4)
        gmpRecordBuilder.setGmp(gmp)
        val buf: Array[Byte] = gmpRecordBuilder.build().toByteArray
        val key = prefix + record._1
        pipeline.setex(key.getBytes(StandardCharsets.US_ASCII), expireTime, buf)
      })
      failureCount += pipeline.syncAndReturnAll().toArray.count(!_.equals("OK"))
    } catch {
      case e: Exception =>
        failureCount += list.size
        log.error(s"Fail write to redis $redisHost:$redisPort for $failureCount records")
    } finally {
      redisClient.disconnect()
    }
    failureCount
  }
}
*/

/*
class MultiGMPModelRedisFullOutput(val outputs: List[GMPModelOutput]) extends GMPModelOutput {

  override def output(dStream: DStream[(String, Long, Double, Double)], appendedKey: String = ""): Option[DStream[Int]] = {
    val results = outputs.flatMap(output => {
      output.output(dStream)
    })
    val finalResult = Try(results.reduce((l: DStream[Int], r: DStream[Int]) => {
      l.union(r)
    }))
    finalResult.toOption
  }
}
*/


