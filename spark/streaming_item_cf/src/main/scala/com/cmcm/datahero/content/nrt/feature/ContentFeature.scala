package com.cmcm.datahero.content.nrt.feature

import com.cmcm.datahero.content.nrt.feature.Profile.{KeywordProfile, CategoryProfile}
import com.typesafe.config.Config
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.Logging
import redis.clients.jedis.Jedis

import collection.JavaConversions._
/**
 * Created by wangwei5 on 15/8/12.
 */
object ContentFeature extends Logging {

  val fieldDelimiter = "\001"
  val pairDelimiter = "\002"
  val keyValueDelimiter = "\003"

  def getCategories(cc: List[CategoryProfile], topCategories: Int) = {
    val ccBuilder = new StringBuilder()
    var i = 0
    while (i < math.min(cc.size, topCategories)) {
      val name = cc.get(i).getName
      val weight = cc.get(i).getL1Weight  // to be changed to get L1 weight when redis is read
      ccBuilder.append(name + keyValueDelimiter + weight + pairDelimiter)
      i += 1
    }
    if (i > 0)
      ccBuilder.dropRight(1).toString()
    else
      ccBuilder.toString()
  }

  def getKeywords(ck: List[KeywordProfile], topKeywords: Int) = {
    val ckBuilder = new StringBuilder()
    var i = 0
    while (i < math.min(ck.size, topKeywords)) {
      val name = ck.get(i).getName
      val weight = ck.get(i).getL1Weight // to be changed to get L1 weight when redis is ready
      ckBuilder.append(name + keyValueDelimiter + weight + pairDelimiter)
      i += 1
    }
    if (i > 0)
      ckBuilder.dropRight(1).toString()
    else
      ckBuilder.toString()
  }

  def getContentFeature(news_id: String, redisClient: Jedis, topCategories: Int, topKeywords: Int, categoryVersion: String, keywordVersion: String) = {

    val cf = redisClient.get(("n_" + news_id).getBytes())

    if (cf != null) {
      val newsProfile = CmnewsNewsProfile.NewsProfile.parseFrom(cf)
      var cc = List[CategoryProfile]()
      for (category <- newsProfile.getVcategoriesList) {
        if (category.getVersion.contentEquals(categoryVersion)) {
          cc = category.getCategoriesList.toList
        }
      }
      val ccString = getCategories(cc, topCategories)

      var ck = List[KeywordProfile]()
      for (keyword <- newsProfile.getVkeywordsList) {
        if (keyword.getVersion.contentEquals(keywordVersion)) {
          ck = keyword.getKeywordsList.toList
        }
      }
      val ckString = getKeywords(ck, topKeywords)

      val publisher = newsProfile.getPublisher
      val publishTime = newsProfile.getPublishTime
      val groupId = newsProfile.getGroupId
      val tier = newsProfile.getTier
      ccString + fieldDelimiter + ckString + fieldDelimiter + publisher + fieldDelimiter + publishTime + fieldDelimiter + groupId + fieldDelimiter + tier
    } else {
      ""
    }
  }

  /**
    *
    * @param batchEvent (contentId, (uid, batchId, show, click, eventFeature))
    * @param contentFeatureConfig
    * @return (uid, (contentId, batchId, show, click, eventFeature, contentFeature))
    */
  def getContentFeatureFromRedis(batchEvent: DStream[(String, (String, Long, Double, Double, String))], contentFeatureConfig: Config) = {
    val host = contentFeatureConfig.getString("redis_host")
    val port = contentFeatureConfig.getInt("redis_port")
    val topCategories = contentFeatureConfig.getInt("top_categories")
    val topKeywords = contentFeatureConfig.getInt("top_keywords")
    val categoryVersion = contentFeatureConfig.getString("category_version")
    val keywordVersion = contentFeatureConfig.getString("keyword_version")
    val auth = contentFeatureConfig.getString("redis_password")

    batchEvent.mapPartitions(events => {
      val redisClient = new Jedis(host, port)
      if (auth.size > 0) redisClient.auth(auth)
      val result = events.map(event => {
        (event._2._1, (event._1, event._2._2, event._2._3, event._2._4, event._2._5, getContentFeature(event._1, redisClient, topCategories, topKeywords, categoryVersion, keywordVersion)))
      }
      )
      redisClient.disconnect
      result
    })
  }

  /**
    *
    * @param batchEvent DStream with format (cid,(uid,batchId,show,click,eventFeature,pid)
    * @param contentFeatureConfig config
    * @return DStream with format (uid,(cid,batchId,show,click,eventFeature,contentFeature,pid))
    */
  def getContentFeatureFromRedisForCtr(batchEvent: DStream[(String, (String, Long, Double, Double, String,String))], contentFeatureConfig: Config) = {
    val host = contentFeatureConfig.getString("redis_host")
    val port = contentFeatureConfig.getInt("redis_port")
    val topCategories = contentFeatureConfig.getInt("top_categories")
    val topKeywords = contentFeatureConfig.getInt("top_keywords")
    val categoryVersion = contentFeatureConfig.getString("category_version")
    val keywordVersion = contentFeatureConfig.getString("keyword_version")
    val auth = contentFeatureConfig.getString("redis_password")
    batchEvent.mapPartitions(events => {
      val redisClient = new Jedis(contentFeatureConfig.getString("redis_host"), contentFeatureConfig.getInt("redis_port"))
      if (auth.size > 0) redisClient.auth(auth)
      val result = events.map(event => {
        (event._2._1, (event._1, event._2._2, event._2._3, event._2._4, event._2._5, getContentFeature(event._1, redisClient, topCategories, topKeywords, categoryVersion, keywordVersion), event._2._6))
      }
      )
      redisClient.disconnect
      result
    })
  }
}
