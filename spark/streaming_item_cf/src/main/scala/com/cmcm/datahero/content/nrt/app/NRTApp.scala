package com.cmcm.datahero.content.nrt.app

import com.cmcm.datahero.content.nrt.component.ModelComponent
import com.cmcm.datahero.content.nrt.config.Configurable
import com.cmcm.datahero.content.nrt.model.StreamingWorkflow
import com.cmcm.datahero.content.nrt.spark.SparkComponent

import scala.collection.mutable


/**
 * Created by hanbin on 15/9/6.
 */
trait NRTApp {
  this: Configurable with SparkComponent with ModelComponent with StreamingWorkflow =>

  case class Config(cp: String = "")


  def defineParser() = {
    val parser = new scopt.immutable.OptionParser[Config]("NRT@DataHero", "0.1") {
      def options = mutable.Seq(
        opt("c", "config", "config folder") { (v: String, c: Config) => c.copy(cp = v) }
      )
    }
    parser
  }

  def initApp(configDir: String) = {
    init(configDir)
  }

  def start() = {
    if (isNewContext) {
      logInfo("APP will start with a new context.")
      workflowSetup()
    } else {
      logInfo("App will start with a context loaded from checkpoint.")
    }

    logInfo("Prestarting...")
    preStart()
    logInfo("The App start to run.")
    streamingContext.start()
    streamingContext.awaitTermination()
  }

  def main(args: Array[String]) {
    val parser = defineParser()
    val configDir = parser.parse(args, Config()) match {
      case Some(config) => {
        val configDir = config.cp
        logInfo(s"NRT App will load configures from $configDir")
        configDir
      }
    }
    initApp(configDir)
    start()
  }
}
