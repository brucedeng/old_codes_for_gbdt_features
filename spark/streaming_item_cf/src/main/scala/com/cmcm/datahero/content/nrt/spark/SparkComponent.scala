package com.cmcm.datahero.content.nrt.spark

import com.cmcm.datahero.content.nrt.component.Component
import com.cmcm.datahero.content.nrt.config.{ConfigUnit, Configurable}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkContext, SparkConf}

object SparkComponent {
  val componentName = "spark_component"
  val configFileName = "spark.conf"
  val defaultAppName = "Content CFB Spark Streaming"
  val defaultRecoverMode = false
  val defaultCheckPoint = "/tmp/content_cfb_checkpoint/"
  val defaultBatchSeconds = 600

}

/**
 * SparkComponent is used to init and manage spark env for other components.
 * 1, init or recover the spark env
 * 2, provide interface for other components to call the spark service
 */
trait SparkComponent extends Component { this: Configurable =>

  private var _streamingContext: StreamingContext =_
  private var _isNewContext: Boolean =_

  override def init(configDir: String) = {
    super.init(configDir)
    val(streamingContext, isNewContext) = initSparkComponent(configDir + "/" + SparkComponent.configFileName)
    _streamingContext = streamingContext
    _isNewContext = isNewContext
  }

  override def preStart() = {
    super.preStart()
  }

  def streamingContext: StreamingContext = {
    _streamingContext
  }

  def isNewContext: Boolean = {
    _isNewContext
  }

  def sparkContext: SparkContext = {
    _streamingContext.sparkContext
  }

  def sparkConfigUnit = {
    getConfigUnit(SparkComponent.componentName).get
  }

  private def createNewSparkStreamingContext(sparkConfigUnit: ConfigUnit): StreamingContext = {
    logInfo("Initializing new spark streaming context...")
    val sparkConf = new SparkConf().setAppName(sparkConfigUnit.getString("spark.app.name",
      SparkComponent.defaultAppName))
    if(sparkConfigUnit.getBoolean("spark.test",false)){
      sparkConf.setMaster(sparkConfigUnit.getString("spark.master","local[1]"))
    }
    val batchSeconds = sparkConfigUnit.getInt("streaming.batch.seconds",
      SparkComponent.defaultBatchSeconds)
    val sparkContext = new SparkContext(sparkConf)
    val streamingContext = new StreamingContext(sparkContext, Seconds(batchSeconds))

    if (sparkConfigUnit.getBoolean("spark.app.checkpointEnable", false)) {
      streamingContext.checkpoint(sparkConfigUnit.getString("spark.app.checkpoint",
        SparkComponent.defaultCheckPoint))
    }
    return streamingContext
  }

  def initSparkComponent(sparkConfigFile: String): (StreamingContext, Boolean) = {
    logInfo("Initializing spark component...")

    @volatile var isNewContext: Boolean = false
    val sparkConfigUnit = loadComponentConfig(SparkComponent.componentName, sparkConfigFile)
    val checkpoint = sparkConfigUnit.getString("spark.app.checkpoint",
      SparkComponent.defaultCheckPoint)

    /*Create Context from
    val streamingContext = StreamingContext.getOrCreate(checkpoint,
      () => {
        isNewContext = true
        createNewSparkStreamingContext(sparkConfigUnit)
      })
      */
    /*Always create new context*/
    val streamingContext =  createNewSparkStreamingContext(sparkConfigUnit)
    isNewContext = true
    (streamingContext, isNewContext)
  }

}
