package com.cmcm.datahero.content.nrt.feature

import com.cmcm.datahero.content.nrt.feature.Profile.{KeywordProfile, CategoryProfile}
import com.typesafe.config.Config
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.Logging
import redis.clients.jedis.Jedis

import scala.collection.mutable.{ListBuffer, ArrayBuffer}
import collection.JavaConversions._
import scala.util.Try

/**
  * Created by wangwei5 on 15/8/12.
  */
object ContentFeatureForIndia extends Logging {

  val fieldDelimiter = "\001"
  val pairDelimiter = "\002"
  val keyValueDelimiter = "\003"

  def getCategories(cc: List[CategoryProfile], topCategories: Int) = {
    val ccBuilder = new StringBuilder()
    var i = 0
    while (i < math.min(cc.size, topCategories)) {
      val name = cc.get(i).getName
      val weight = cc.get(i).getL1Weight // to be changed to get L1 weight when redis is read
      ccBuilder.append(name + keyValueDelimiter + weight + pairDelimiter)
      i += 1
    }
    if (i > 0)
      ccBuilder.dropRight(1).toString()
    else
      ccBuilder.toString()
  }

  def getKeywords(ck: List[KeywordProfile], topKeywords: Int) = {
    val ckBuilder = new StringBuilder()
    var i = 0
    while (i < math.min(ck.size, topKeywords)) {
      val name = ck.get(i).getName
      val weight = ck.get(i).getL1Weight // to be changed to get L1 weight when redis is ready
      ckBuilder.append(name + keyValueDelimiter + weight + pairDelimiter)
      i += 1
    }
    if (i > 0)
      ckBuilder.dropRight(1).toString()
    else
      ckBuilder.toString()
  }

  def getContentFeature(news_id: String,
                        redisClient: Jedis,
                        topCategories: Int,
                        topKeywords: Int,
                        categoryVersion: String,
                        keywordVersion: String) = {
    val cf = redisClient.get(("n_" + news_id).getBytes())

    if (cf != null) {
      val newsProfile = CmnewsNewsProfile.NewsProfile.parseFrom(cf)
      val cc = if (categoryVersion.length <= 0) {
        newsProfile.getCategoriesList.toList
      } else {
        var ccList = List[CategoryProfile]()
        for (category <- newsProfile.getVcategoriesList) {
          if (category.getVersion.contentEquals(categoryVersion)) {
            ccList = category.getCategoriesList.toList
          }
        }
        ccList
      }
      val ccString = getCategories(cc, topCategories)

      val ck = if (keywordVersion.length <= 0) {
        newsProfile.getKeywordsList.toList
      } else {
        var ckList = List[KeywordProfile]()
        for (keyword <- newsProfile.getVkeywordsList) {
          if (keyword.getVersion.contentEquals(keywordVersion)) {
            ckList = keyword.getKeywordsList.toList
          }
        }
        ckList
      }
      val ckString = getKeywords(ck, topKeywords)

      val publisher = newsProfile.getPublisher
      val publishTime = newsProfile.getPublishTime
      val groupId = newsProfile.getGroupId
      val titleMd5 = newsProfile.getTitleMd5
      ccString + fieldDelimiter + ckString + fieldDelimiter + publisher + fieldDelimiter + publishTime + fieldDelimiter + groupId + fieldDelimiter + titleMd5
    } else {
      ""
    }
  }

  /**
    *
    * @param batchEvent           (contentId, (uid, batchId, act, value, eventFeature))
    * @param contentFeatureConfig cf config
    * @return (uid, (cid, batchId, act, value, eventFeature, contentFeature))
    */
  def getContentFeatureFromRedis(batchEvent: DStream[(String, (String, Long, String, Double, String))], contentFeatureConfig: Config) = {
    val host: String = contentFeatureConfig.getString("redis_host")
    val port = contentFeatureConfig.getInt("redis_port")
    val topCategories = contentFeatureConfig.getInt("top_categories")
    val topKeywords = contentFeatureConfig.getInt("top_keywords")
    val categoryVersion = Try(contentFeatureConfig.getString("category_version")).getOrElse("")
    val keywordVersion = Try(contentFeatureConfig.getString("keyword_version")).getOrElse("")
    val auth = contentFeatureConfig.getString("redis_password")
    batchEvent.transform(rdd => {
      rdd.mapPartitionsWithIndex((partitionNum, events) => {
        val redisHost = if (host.split(",").length > 1) {
          val redisHostList = host.split(",")
          redisHostList(partitionNum % redisHostList.length)
        } else {
          host
        }

        var redisClient: Jedis = null
        var result: Iterator[(String, (String, Long, String, Double, String, String))] = Iterator()
        try {
          redisClient = new Jedis(redisHost, port)
          if (auth.size > 0) redisClient.auth(auth)
          result = events.map(event => {
            (event._2._1, (event._1, event._2._2, event._2._3, event._2._4, event._2._5, getContentFeature(event._1, redisClient, topCategories, topKeywords, categoryVersion, keywordVersion)))
          })
        } catch {
          case e: Exception =>
            log.error(s"Fail read ContentFeature from redis $redisHost:$port")
        } finally {
          redisClient.disconnect()
        }
        result
      })
    })

  }
}
