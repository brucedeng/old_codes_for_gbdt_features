package com.cmcm.datahero.content.nrt.event

import java.util

import org.apache.spark.Logging

import scala.io.Source
import scala.collection.mutable.HashMap

/**
  * Created by hanbin on 15/9/10.
  */
trait MccToCountryMapper {
  def mcc2Country(mcc: String): Option[String]

  def parseDataFile(mccFile: String)
}

object MccToCountryMapper {

  def apply(): MccToCountryMapper = {
    new MccToCountry(new HashMap[String, String])
  }

  private class MccToCountry(mccToCountryTable: HashMap[String, String]) extends MccToCountryMapper with Logging with Serializable {
    override def mcc2Country(mccVal: String) = {
      if (mccVal == null || mccVal.trim == "") {
        None
      } else {
        val mccs: Array[String] = mccVal.trim.split(",")
        val countries: Array[String] = mccs.flatMap(mccToCountryTable.get(_))
        if (countries.length > 0) {
          Some(countries(0))
        } else {
          None
        }
      }
    }

    override def parseDataFile(mccFile: String) {
      val source = Source.fromFile(mccFile,"UTF-8")
      val lines = source.getLines()
      val kvPairs: Iterator[(String, String)] = lines.flatMap(line => {
        val fields = line.trim.split(',')
        if (fields.size != 2) {
          None
        } else {
          val mcc = fields(0)
          val country = fields(1)
          if (mcc.length > 0 && country.length > 0) {
            Some((mcc, country))
          } else {
            None
          }
        }
      })
      mccToCountryTable ++= kvPairs
    }
  }

}
