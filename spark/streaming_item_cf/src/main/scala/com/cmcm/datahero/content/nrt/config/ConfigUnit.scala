package com.cmcm.datahero.content.nrt.config

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}

import scala.collection.JavaConversions._
import scala.util.Try

/**
  * Created by hanbin on 15/8/20.
  */

object ConfigUnit {

  def apply(configName: String, configFilePath: String) = {
    val configFile = new File(configFilePath)
    if (configFile.exists() && configFile.isFile()) {
      val config = ConfigFactory.parseFile(configFile)
      new ConfigUnit(configName, config)
    } else {
      throw new IllegalArgumentException("The config file: " + configFilePath + " is not existed.")
    }
  }

}

/**
  * ConfigUnit is used to config a special component.
  * Be carefull that ConfigUnit is not serializable
  *
  * @param name   : the name of ConfigUnit, which should be identifiable
  * @param config : contains all configurations
  */
class ConfigUnit(val name: String, val config: Config) {

  def getConfigName = name

  def getConfig = config

  def getInt(configPath: String, default: Int): Int = {
    Try(config.getInt(configPath)).getOrElse(default)
  }

  def getIntList(configPath: String, default: List[Integer]): List[Integer] = {
    Try(config.getIntList(configPath)).map(_.toList).getOrElse(default)
  }

  def getString(configPath: String, default: String): String = {
    Try(config.getString(configPath)).getOrElse(default)
  }

  def getStringList(configPath: String, default: List[String]): List[String] = {
    Try(config.getStringList(configPath)).map(_.toList).getOrElse(default)
  }

  def getLong(configPath: String, default: Long): Long = {
    Try(config.getLong(configPath)).getOrElse(default)
  }

  def getLongList(configPath: String, default: List[Long]): List[Long] = {
    Try(config.getLongList(configPath)).map(_.toList.map(Long2long(_))).getOrElse(default)
  }

  def getBoolean(configPath: String, default: Boolean): Boolean = {
    Try(config.getBoolean(configPath)).getOrElse(default)
  }

  def getBooleanList(configPath: String, default: List[Boolean]): List[Boolean] = {
    Try(config.getBooleanList(configPath)).map(_.toList.map(Boolean2boolean(_))).getOrElse(default)
  }

  def getDouble(configPath: String, default: Double): Double = {
    Try(config.getDouble(configPath)).getOrElse(default)
  }

  def getDoubleList(configPath: String, default: List[Double]): List[Double] = {
    Try(config.getDoubleList(configPath)).map(_.toList.map(Double2double(_))).getOrElse(default)
  }

}
