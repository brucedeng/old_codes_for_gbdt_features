package com.cmcm.datahero.content.nrt.event

/**
 * Created by hanbin on 16/1/7.
 */
object EventFeature {

  val pairDelimiter = "\002"
  val keyValueDelimiter = "\003"

  def apply(featureString: String) = {
    val eventFeature = new EventFeature()
    featureString.split(pairDelimiter).foreach(kvString => {
      val pair = kvString.split(keyValueDelimiter)
      if (pair.size == 2)
        eventFeature.addFeature(pair(0), pair(1))
    })
    eventFeature
  }

  def apply() = {
    new EventFeature()
  }

}

class EventFeature() {
  var features: Map[String, String] = Map()

  def addFeature(key: String, value: String) = {
    features = features.+((key, value))
    this
  }

  def genFeatureString() = {
    features.map(pair => {
      pair._1 + EventFeature.keyValueDelimiter + pair._2
    }).mkString(EventFeature.pairDelimiter)
  }

  def getFeatureValue(key: String) = {
    features.get(key)
  }

}
