package com.cmcm.datahero.content.nrt.feature

import java.util.concurrent.ArrayBlockingQueue
import java.util.{TimerTask, Timer}
import com.typesafe.config.Config
import org.apache.spark.{SparkContext, Logging}
import org.apache.spark.rdd.RDD
import org.joda.time.DateTime

import scala.util.Try

/**
 * Created by hanbin on 15/9/7.
 */
case object UPTimingLoader extends Logging {

  val upQueue = new ArrayBlockingQueue[(String, RDD[(String, String)])](10)
  val timer = new Timer(true)
  var lastUPTime: String = ""

  def initUPTimingLoader(upConfigUnit: Config, sparkContext: SparkContext) = {
    println("Init UP timing loader...")
    val period = Try(upConfigUnit.getInt("load_period")).getOrElse(FeatureComponent.defaultUPLoadPeriod)
    val dateTimeFormat = Try(upConfigUnit.getString("datetime_format")).getOrElse(FeatureComponent.defaultUPDatetimeFormat)
    val offset = Try(upConfigUnit.getInt("load_datetime_offset")).getOrElse(FeatureComponent.defaultUPDatetimeOffset)
    val task = new TimerTask {
      override def run(): Unit = {
        val currentDataTime = new DateTime().plusDays(offset)
        val currentDateString = currentDataTime.toString(dateTimeFormat)

        if (lastUPTime == "" || currentDateString > lastUPTime) {
          println(s"Begin to load user profile for $currentDateString")
          val userFeature: Try[RDD[(String, String)]] =
            Try(UserFeature.loadJsonFeature(upConfigUnit, currentDateString, sparkContext))

          if (userFeature.isSuccess) {
            println(s"Success to load user profile for $currentDateString")
            if (upQueue.size() >= 10) {
              println(s"Can not add user profile for $currentDateString to the queue, the queue is full now!")
              sys.exit(-1)
            }
            userFeature.foreach(rdd => upQueue.add((currentDateString, rdd)))

            lastUPTime = currentDateString
          } else {
            val error: Throwable = userFeature.failed.get
            error.printStackTrace()
            println(s"Fail to load user profile for $currentDateString, because: " + error)
          }
        }
      }
    }
    timer.schedule(task, 0L, (period / 24) * 1000)
  }

  def getCurrentUserFeatureRDD(): Option[(String, RDD[(String, String)])] = {
    if (upQueue.isEmpty)
      None
    else {
      while (upQueue.size() > 1) {
        val oldUp = upQueue.remove()
        oldUp._2.unpersist()
        println("User feature: unpersist the old user feature RDD")
      }
      Some(upQueue.element())
    }
  }

  def waitForReady(times: Int) = {
    var tryCount = 0
    while (getCurrentUserFeatureRDD.isEmpty) {
      println("The user feature is not available!")
      Thread.sleep(10000)
      tryCount = tryCount + 1
      if (tryCount > times) {
        logError("Can not get the user feature!")
        sys.exit(-1)
      }
    }
  }
}
