package com.cmcm.datahero.content.nrt.component

import java.awt.Window

import com.cmcm.datahero.content.nrt.config.{ConfigUnit, Configurable}
import com.cmcm.datahero.content.nrt.event.{Event, EventComponent, EventFeature}
import com.cmcm.datahero.content.nrt.spark.SparkComponent
import org.apache.spark.{HashPartitioner, SparkContext}
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.{Seconds, Time}
import org.apache.spark.streaming.dstream.DStream
import org.joda.time.DateTime
import org.apache.hadoop.fs.Path
import org.apache.spark.mllib.rdd.MLPairRDDFunctions.fromPairRDD

import scala.collection.mutable.ListBuffer
import scala.util.Try



object ItemCFModelComponent{
  val pairDelimiter = "\002"
  val keyValueDelimiter = "\003"

  def getEventPathByTimeWindow(time:Time,windowSize:Int,windowCount:Int,basePath:String):Option[Seq[String]] = {

    if (windowCount>0) {
      val windowTimeSlots = for (i <- 1 to windowCount)
        yield new DateTime(time.milliseconds).minusSeconds(windowSize*i).toString("yyyyMMddHHmm")

      val path = for (dt <- windowTimeSlots)
        yield basePath + "/" + dt.substring(0, 8) + "/" + dt.substring(8)
      Some(path)
    }else{
      None
    }
  }

  def loadRDDText(sc:SparkContext, path:String):RDD[String] = {
    val conf = sc.hadoopConfiguration
    val fs = org.apache.hadoop.fs.FileSystem.get(conf)
    val exists = fs.exists(new org.apache.hadoop.fs.Path(path))

    if (exists) {
      println("Load past windows' data: " + path)
      sc.textFile(path)
    }
    else{
      println("Past windows' data no exist: " + path)
      sc.emptyRDD[String]
    }
    /*
    val rdd = sc.textFile(path)
    try {
      rdd.first()
      println("Load past windows' data: " + path)
      rdd
    }
    catch {
      case e:Throwable => {
        println("Past windows' data no exist: " + path)
        sc.emptyRDD[String]
      }
    }
    */
  }

  /*generate Symmetric pair for similarity*/
  def genSymmetricSim(t:((String, String),(Double,Double,Double))) = {
    ((t._1._2, t._1._1),t._2)
  }

  def encodeItemSimArray(sim:Array[(String,Double)]):String = {
    sim
      .map(x => x._1 + keyValueDelimiter + x._2.toString)
      .mkString(pairDelimiter)
  }

  def decodeItemSimArray(str:String):Array[(String,Double)] = {
    str.split(pairDelimiter)
      .map( x=> {
        val f = x.split(keyValueDelimiter)
        (f(0),f(1).toDouble)
      })
  }
}

trait ItemCFModelComponent extends ModelComponent with Serializable{
  this: Configurable with SparkComponent with EventComponent =>

  def getSlidingWindowDStream(inputDStream: DStream[String]) = {
    inputDStream.window(
      Seconds(eventConfigUnit.getInt("event.sliding_window_length", 1200)),
      Seconds(eventConfigUnit.getInt("event.event_window", 600))
    )
  }

  def parseCFEvent(inputDStream: DStream[String]) = {
    val cTypeFilter: Array[String] = eventConfigUnit.getString("event.ctype_filter", "article")
      .split(",")
      .map {
        case "video" => "2"
        case _ => "1"
      }
    val regionFilter = eventConfigUnit.getString("event.region_filter", "")
    val eventWindow = eventConfigUnit.getInt("event.event_window", 600)
    inputDStream.flatMap(
      Event.getEventParserForCF(regionFilter, cTypeFilter, eventWindow))
  }


  /**
    * Deduplication by same key (uid,cid,eventTime,view,click)
    *
    * @param inputDStream ((uid, contentid, eventFeature, batchId), (view, click))
    * @return ((uid, cid, eventFeature, batchId), (view, click))
    */
  def dedupForWorldwideGMP(inputDStream: DStream[((String, String, String, Long), (Double, Double))]) = {
    val dedup = eventConfigUnit.getBoolean("event.dedup", false)
    if (dedup) {
      inputDStream.map(record => {
        val eventFeature = EventFeature(record._1._3)
        val eventTime = eventFeature.getFeatureValue("EVENT_TIME").getOrElse("")
        ((record._1._1, record._1._2, eventTime, record._2._1, record._2._2), (record._1._3, record._1._4))
      }).reduceByKey((r, l) => r)
        .map(record => ((record._1._1, record._1._2, record._2._1, record._2._2), (record._1._4, record._1._5)))
    } else {
      inputDStream
    }
  }

  /**
    * dump parsed event to hdfs, to avoid reading windowed kafka stream
    *
    * @param inputDStream ((uid, contentid, eventFeature, batchId), (view, click))
    */
  def dumpBatchEvent(inputDStream: DStream[((String, String, String, Long), (Double, Double))]) = {
    val eventDumpPath = modelConfigUnit.getString("model.cf.event_dump.path", "/tmp/news_model/cf_parsed_event/")

    inputDStream.foreachRDD((rdd, time) => {
      val datePrefix = new DateTime(time.milliseconds).toString("yyyyMMddHHmm")
      val ts = time.milliseconds
      rdd.map(record => s"${record._1._1}\t${record._1._2}\t${record._1._3}\t${record._1._4}\t${record._2._1}\t${record._2._2}\t${ts}")
        .saveAsTextFile(eventDumpPath + "/" + datePrefix.substring(0, 8) + "/" + datePrefix.substring(8))
    })
  }


  /**
    * Load past few windows' events to merge with current batch,
    * and then compute item co-occurance with the whole window
    *
    * @param inputDStream ((uid, contentid, eventFeature, batchId), (view, click))
    */
  def getItemCoOccur(inputDStream: DStream[((String, String, String, Long), (Double, Double))]) = {
    val regularization = modelConfigUnit.getBoolean("model.cf.regularization",true)
    val regularization_factor = modelConfigUnit.getDouble("model.cf.regularization_factor",1.0)

    val eventDumpPath = modelConfigUnit.getString("model.cf.event_dump.path", "/tmp/news_model/cf_parsed_event/")

    val sliding_window_length = eventConfigUnit.getInt("event.sliding_window_length", 3600)
    val window_size = eventConfigUnit.getInt("event.event_window", 600)
    val maxPV = eventConfigUnit.getInt("event.user_max_pv", 200)
    val ridGroup = eventConfigUnit.getBoolean("event.use_rid_group", true)

    println(s"Item co-occur config => rid group:${ridGroup}, maxPV:${maxPV}, window_size:${window_size}, sliding_window_length:${sliding_window_length}")

    val windowCount = (sliding_window_length/window_size) - 1

    inputDStream.transform(
      (rddCurrentWindow,time) => {
        val eventData = ItemCFModelComponent.getEventPathByTimeWindow(time,window_size,windowCount,eventDumpPath)

        if(eventData.isDefined) {
          val eventRDDs = for (path <- eventData.get) yield
            ItemCFModelComponent.loadRDDText(sparkContext,path)

          val pastWindowEvent = sparkContext.union(eventRDDs).map(x => {
            val f = x.split("\t")
            ((f(0), f(1), f(2), f(3).toLong), (f(4).toDouble, f(5).toDouble))
          })

          val rddSlidingWindow = rddCurrentWindow.union(pastWindowEvent)

          computeItemOccur(ridGroup,maxPV, regularization, regularization_factor)(rddSlidingWindow)

        }else{

          logInfo("Only use current batch to compute co-occurance")
          computeItemOccur(ridGroup,maxPV, regularization, regularization_factor)(rddCurrentWindow)
        }
      })
  }


  /*
   * Compute Item cooccurrence inside Event RDD
   * @param eventRDD ((uid, contentid, eventFeature, batchId), (view, click))
   * @return cooccurrence RDD ((contentid, contentid), (co-view, co-click, co-click/co-view))
   */
  def computeItemOccur(ridGroup: Boolean, userMaxPV:Int, regularization:Boolean, lambda:Double)(event: RDD[((String,String,String,Long),(Double,Double))]): RDD[((String,String),(Double,Double,Double))] = {

    //((uid),(cid, view ,click) )
    val userGrp = event
      /*
      .filter(x => {
        val rid = EventFeature(x._1._3).getFeatureValue("RID").getOrElse("0")
        rid.equals("4c0701bc")
      })
      */
      .map(x => {
      if(ridGroup) {
        val rid = EventFeature(x._1._3).getFeatureValue("RID").getOrElse("0")
        //println (((x._1._1,rid),(x._1._2,x._2._1,x._2._2))).toString
        ((x._1._1,rid),(x._1._2,x._2._1,x._2._2))
      }
      else{
        ((x._1._1),(x._1._2,x._2._1,x._2._2))
      }
    })
      .groupByKey().distinct()


    //Stat user behavior
    /*
    val userEventLen = userGrp.map( x => (x._2.size/50 * 50,1.0))
    val userStat = userEventLen.reduceByKey( (a,b) => a+b )
    val userStatLocal = userStat.collect()
    userStatLocal.foreach( x => println(x.toString()))
    */

    //val userMaxPV =
    val userGrpFilterd = userGrp.filter( x => (x._2.size <= userMaxPV))

    val coOccur = userGrpFilterd.flatMap{
      case (uid, items) => {
        val view_items = items.filter(_._2 > 0).map(_._1)
        val click_items = items.filter(_._3 > 0).map(_._1)

        val co_view = view_items.toSeq.combinations(2)
          .map {case Seq(a,b) => {if (a<b) ((a,b),"view") else ((b,a),"view")}
          }.filter(x => (x._1._1 != x._1._2)).toList

        //all
        val co_click = click_items.toSeq.combinations(2)
          .map {case Seq(a,b) => {if (a<b) ((a,b),"click") else ((b,a),"click")}
          }.filter(x => (x._1._1 != x._1._2)).toList

        val co_occur = co_view ++ co_click
        for (x <- co_occur) yield x
          //.flatten[((String,String),String)]
      }
    }

    val coOccurWithCount = coOccur.aggregateByKey((0.0,0.0))(
      (x,act) => act match {
        case "view" =>
          (x._1 + 1.0, x._2)
        case "click" =>
          (x._1, x._2 + 1.0)
        },

      (a,b) => (a._1 + b._1, a._2 + b._2)
    )

    val filteredRes = coOccurWithCount.filter( x => (x._2._1 > 0 && x._2._2 > 0))
    //val filteredRes = coOccurWithCount

    filteredRes.map {
        case ((conid1, conid2), (co_view, co_click)) => {

          val co_click_rate = if (regularization) (co_click*co_click)/(co_view*(co_click+lambda))
                              else co_click / co_view

          ((conid1, conid2),(co_view, co_click, co_click_rate))
        }
      }
    //( x => (x._1, (x._2._1, x._2._2, x._2._2/(x._2._1))))
  }

  /*
   * Generate similar item candidates for each item
   * @param cooccurrence RDD ((contentid, contentid), (co-view, co-click, co-click/co-view))
   * @return item-similarity RDD (contentid, topk_similar_content_string)
   */
  def saveSimilarItemList(dStream: DStream[((String, String),(Double,Double,Double))]) = {
    val topKsimilarity = modelConfigUnit.getInt("model.cf.similarity.top",3)
    val similarityDumpPath = modelConfigUnit.getString("model.cf.similarity.path", "/tmp/news_model/item_cf/similarity")
    val kafkaOffsetSaver = getKafkaOffsetSaver()
    dStream.foreachRDD((rdd,time) => {
      val datePrefix = new DateTime(time.milliseconds).toString("yyyyMMddHHmm")
      val ts = time.milliseconds

      object MaxOrder extends Ordering[(String,Double)] {
        def compare(x:(String,Double), y:(String,Double)):Int = {
          if (x._2 > y._2)
            1
          else if(x._2 ==  y._2)
            0
          else
            -1
        }
      }

      val symmeticSimilarity = rdd.flatMap(x => Array(x,ItemCFModelComponent.genSymmetricSim(x)))
      val itemSimlarityList = symmeticSimilarity
        .map( x => ((x._1._1),(x._1._2, x._2._3)))
        .topByKey(topKsimilarity)(MaxOrder)
        .map( x => {
        x._1+"\t"+ItemCFModelComponent.encodeItemSimArray(x._2)+"\t"+ts
      })

      val result_path = similarityDumpPath + "/" + datePrefix.substring(0, 8) + "/" + datePrefix.substring(8)
      println("Save similarity to: " + result_path)

      itemSimlarityList
        .saveAsTextFile(result_path)

      kafkaOffsetSaver()
    })
  }





  /**
    * Save to dump file as format (cid \t batchId \t show \t click \t show_decayed \t click_decayed \t lan \t country \t timestamp)
    *
    * @param inputDStream (cid, (batchId, show, click, show_decyaed, click_decyaed, lan, country))
    * @return
    */
  def dumpItemCFResult(inputDStream: DStream[((String, String),(Double, Double,Double))]) = {
    val offlineDumpPath = modelConfigUnit.getString("model.cf.offline_dump.path", "/tmp/news_model/item_cf/result_dump")

    //val kafkaOffsetSaver = getKafkaOffsetSaver()

    inputDStream.foreachRDD((rdd, time) => {
      val datePrefix = new DateTime(time.milliseconds).toString("yyyyMMddHHmm")
      val ts = time.milliseconds

      val result_path = offlineDumpPath + "/" + datePrefix.substring(0, 8) + "/" + datePrefix.substring(8)
      println("Save co-occurance data to: " + result_path)

      rdd.map(record => s"${record._1._1}\t${record._1._2}\t${record._2._1}\t${record._2._2}\t${record._2._3}\t${ts}")
        .saveAsTextFile(result_path)

      //kafkaOffsetSaver()
    })

  }

  def coOccurOutputFilter(inputDStream: DStream[((String, String),(Double, Double,Double))]) = {
    val coViewThreshhold = modelConfigUnit.getInt("model.cf.coview.threshhold", 1)
    val coClickThreshhold = modelConfigUnit.getInt("model.cf.coclick.threshhold", 1)

    inputDStream
      .filter(record => (record._2._1 >= coViewThreshhold && record._2._1 >= coClickThreshhold))
  }


  def getCFModelOutput(modelConfigUnit: ConfigUnit) = {
    val modelOutputDest = modelConfigUnit.getString("model.cf.output.dest", "file")
    logInfo(s"The model out dest is $modelOutputDest")
    val gmpPram = modelConfigUnit.getInt("model.cf.smooth.parameter", 1000)
    val window = modelConfigUnit.getInt("model.gmp.decay.window", 600)
    //val maxWindow = modelConfigUnit.getInt("model.gmp.decay.max_window", GmpDecayAggregator.defaultDecayMaxWindow)
    if (modelOutputDest == "full_redis") {
      //val redisHost = modelConfigUnit.getString("model.gmp.output.redis.host", "unknown")
      //val redisPort = modelConfigUnit.getInt("model.gmp.output.redis.port", 0)
      //val redisPassword = modelConfigUnit.getString("model.gmp.output.redis.password", "")
      //val redisKeyPrefix = modelConfigUnit.getString("model.gmp.output.redis.prefix", "ng_")
      //val redisDB = modelConfigUnit.getInt("model.gmp.output.redis.db", 0)
      //new GMPModelRedisFullOutput(redisHost, redisPort, redisDB, redisPassword, window * maxWindow, window, gmpPram, redisKeyPrefix)
      null
    } else if (modelOutputDest == "multi_redis") {
      //val destNum = modelConfigUnit.getInt("model.gmp.output.dest_num", 1)
      //val outputs = (1 to destNum).map(i => {
      //val redisHost = modelConfigUnit.getString(s"model.gmp.output.multi_redis-$i.host", "unknown")
      //val redisPort = modelConfigUnit.getInt(s"model.gmp.output.multi_redis-$i.port", 0)
      //val redisPassword = modelConfigUnit.getString(s"model.gmp.output.multi_redis-$i.password", "")
      //val redisKeyPrefix = modelConfigUnit.getString(s"model.gmp.output.multi_redis-$i.prefix", "ng_")
      //val redisDB = modelConfigUnit.getInt(s"model.gmp.output.multi_redis-$i.db", 0)
      //new GMPModelRedisFullOutput(redisHost, redisPort, redisDB, redisPassword, window * maxWindow, window, gmpPram, redisKeyPrefix)
      //}).toList
      //new MultiGMPModelRedisFullOutput(outputs)
      null
    } else {
      val modelOutputFilePrefix = modelConfigUnit.getString("model.gmp.output.file.prefix",
        ModelComponent.defaultModelOutputFilePrefix)
      new GMPModelFileOutput(modelOutputFilePrefix, window, gmpPram)
    }
  }

  /*
  def gmpMetricsOutput(eventDStream: DStream[String],
                       parsedDStream: DStream[((String, String, String, Long), (Double, Double))],
                       dedupedDStream: DStream[((String, String, String, Long), (Double, Double))],
                       gmpDecayedDStream: DStream[(String, (Long, Double, Double, Double, Double))],
                       gmpOutputDStream: DStream[(String, Long, Double, Double)],
                       failedOutputToRedisDStream: Option[DStream[Int]]): Unit = {
    if (modelConfigUnit.getBoolean("model.monitor.enabled", false)) {
      eventDStream.foreachRDD(rdd => {
        val eventCount = rdd.count()
        ModelComponent.monitor.recordFromDriver(MonitorMetrics.GMP_INPUT_EVENT_COUNT, eventCount.toString)
      })

      parsedDStream.foreachRDD((rdd, time) => {
        outputParsedMetrics(rdd, time)
      })

      dedupedDStream.foreachRDD(rdd => {
        val dedupedCount = rdd.count()
        ModelComponent.monitor.recordFromDriver(MonitorMetrics.GMP_DEDUPED_COUNT, dedupedCount.toString)
      })

      gmpDecayedDStream.foreachRDD(rdd => {
        ModelComponent.monitor.recordFromDriver(MonitorMetrics.GMP_TOTAL_COUNT, rdd.count().toString)
      })

      gmpOutputDStream.foreachRDD(outputOutputMetrics _)
      if (failedOutputToRedisDStream.isDefined) {
        failedOutputToRedisDStream.get.foreachRDD(rdd => {
          ModelComponent.monitor.recordFromDriver(MonitorMetrics.GMP_FAILED_OUTPUT_TO_REDIS, rdd.sum().toString)
        })
      }
    }
  }

  private def outputOutputMetrics(rdd: RDD[(String, Long, Double, Double)]): Unit = {
    val gmpPram = modelConfigUnit.getInt("model.gmp.smooth.parameter", 1000)
    val gmpOutputCount = rdd.count()
    val gmpReasonableCount = rdd.filter(record => record._4 / (record._3 + gmpPram) <= 0.3).count()
    val gmpSum = rdd.map(record => record._4 / (record._3 + gmpPram)).sum()
    val gmpAvg = gmpSum / gmpOutputCount
    logError(s"gmp sum is $gmpSum, gmp avg is $gmpAvg")
    ModelComponent.monitor.recordFromDriver(MonitorMetrics.GMP_OUTPUT_TOTAL_COUNT, gmpOutputCount.toString)
    ModelComponent.monitor.recordFromDriver(MonitorMetrics.GMP_OUTPUT_REASONABLE_COUNT, gmpReasonableCount.toString)
    ModelComponent.monitor.recordFromDriver(MonitorMetrics.GMP_OUTPUT_AVG_VALUE, gmpAvg.toString)
  }

  private def outputParsedMetrics(rdd: RDD[((String, String, String, Long), (Double, Double))],
                                  time: Time): Unit = {
    val window = modelConfigUnit.getInt("model.gmp.decay.window", GmpDecayAggregator.defaultDecayWindow)
    val impressionCount = rdd.filter(record => record._2._1 > 0).count()
    val clickCount = rdd.filter(record => record._2._2 > 0).count()
    ModelComponent.monitor.recordFromDriver(MonitorMetrics.GMP_IMPRESSION_COUNT, impressionCount.toString)
    ModelComponent.monitor.recordFromDriver(MonitorMetrics.GMP_CLICK_COUNT, clickCount.toString)

    val batchId = System.currentTimeMillis() / 1000 / window
    val partitionedMetrics = rdd.mapPartitions(partition => {
      var min = Long.MaxValue
      var max = Long.MinValue
      var sum = 0L
      var count = 0L
      partition.foreach(record => {
        count += 1L
        val delta = batchId - record._1._4
        min = Math.min(min, delta)
        max = Math.max(max, delta)
        sum += delta
      })
      var array = new ListBuffer[(Long, Long, Long, Long)]()
      array.+=((min, max, sum, count))
      array.toIterator
    })
    val metricsResult = partitionedMetrics.reduce((l, r) => {
      (Math.min(l._1, r._1), Math.max(l._2, r._2), l._3 + r._3, l._4 + r._4)
    })
    val avgLatency = Try(metricsResult._3 * 1.0 / metricsResult._4).getOrElse(-1L)
    ModelComponent.monitor.recordFromDriver(MonitorMetrics.GMP_AGGRE_MIN_LATENCY, metricsResult._1.toString)
    ModelComponent.monitor.recordFromDriver(MonitorMetrics.GMP_AGGRE_MAX_LATENCY, metricsResult._2.toString)
    ModelComponent.monitor.recordFromDriver(MonitorMetrics.GMP_AGGRE_AVG_LATENCY, avgLatency.toString)
  }
  */
}