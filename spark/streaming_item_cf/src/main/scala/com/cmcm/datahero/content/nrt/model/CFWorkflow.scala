package com.cmcm.datahero.content.nrt.model

import com.cmcm.datahero.content.nrt.component.{ItemCFModelComponent, GMPModelOutput}
import com.cmcm.datahero.content.nrt.config.Configurable
import com.cmcm.datahero.content.nrt.event.EventComponent
import com.cmcm.datahero.content.nrt.spark.SparkComponent
import org.apache.spark.streaming.dstream.DStream

trait CFWorkflow extends StreamingWorkflow {
  this: Configurable with SparkComponent with EventComponent with ItemCFModelComponent=>

  override def workflowSetup() = {
    //val gmpModelOutput: GMPModelOutput = getCFModelOutput(modelConfigUnit)

    val eventDStream = getEventDStream()
    //val windowDStream = getSlidingWindowDStream(eventDStream)
    val parsedDStream = parseCFEvent(eventDStream)
    val dedupedDStream = dedupForWorldwideGMP(parsedDStream)

    //dump parsed batch event
    dumpBatchEvent(dedupedDStream)

    val itemCoOccurDStream = getItemCoOccur(dedupedDStream)
    val filteredDStream = coOccurOutputFilter(itemCoOccurDStream)

    dumpItemCFResult(filteredDStream)

    saveSimilarItemList(filteredDStream)

    /*
    val gmpOutputDStream = gmpOutputFilterForMultiLan(gmpDecayedDStream)

    val failedOutputToRedisDStream: Option[DStream[Int]] = gmpModelOutput.output(gmpOutputDStream)

    val gmpDecayedNoTagDStream = gmpDecayedDStream.map(record => {
      (record._1, (record._2._1, record._2._2, record._2._3, record._2._4, record._2._5))
    })
    gmpMetricsOutput(eventDStream, parsedDStream, dedupedDStream,
      gmpDecayedNoTagDStream, gmpOutputDStream, failedOutputToRedisDStream)
    */
  }

}
