

### Start pipeline
Put star.sh and conf/ at the same folder, run start.sh. For restarting pipeline, plz manually delete offset in ZK. Offset path is set in conf

### Stop pipeline
Run stop.sh

### Config

- [event.conf] event_window = 600, update window size, should be same as spark streaming window size
- [event.conf] sliding_window_length = 14400, length of sliding window, in seconds
- [event.conf] user_max_pv = 200, filter abnormal users
- [event.conf] use_rid_group = true, only consider pv in same rid and co-view
- [event.conf] offset = "/news_model/item_cf/offset", offset path in ZK.

- [model.conf] event_dump.path, temporary result
- [model.conf] offline_dump.path, store pairwise similarity 
- [model.conf] similarity.path, store top N similar items for each item 

- [model.conf] regularization of simliarity, regularization = true, regularization_factor = 1.0
- [model.conf] threshold for output, coview.threshhold = 20, coclick.threshhold = 2
- [model.conf] topN similarity, similarity.top = 5
 
