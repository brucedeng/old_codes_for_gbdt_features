#!/usr/bin/env bash

currentDir=$(cd `dirname $0`; pwd)

if [ ! -f "${currentDir}/run/nrt.pid" ]
then
    echo "${currentDir}/run/nrt.pid file is not existed"
else
    gid=`cat ${currentDir}/run/nrt.pid`
    echo "the group id is $gid, whill kill all processes in this group!"
    kill -9 "-$gid"
fi
