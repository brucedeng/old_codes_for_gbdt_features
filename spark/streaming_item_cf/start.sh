#!/usr/bin/env bash
#export SPARK_CONF_DIR=./spark_conf

export LD_LIBRARY_PATH=$LDLIBRARY_PATH:/usr/lib/hadoop/lib/native:/usr/lib/hadoop/lib/native/Linux-amd64-64/
export HADOOP_CONF_DIR=/etc/hadoop/conf
export YARN_CONF_DIR=/usr/lib/spark/conf
#export SPARK_CLASSPATH=/usr/lib/hadoop/lib/*
hadoop fs -rm -r /projects/news/model/item_cf/checkpoint

currentDir=$(cd `dirname $0`; pwd)

echo $$ > ${currentDir}/run/nrt.pid

#--driver-class-path /home/chenkehan/news_model/home/slf4j-log4j12-1.7.18.jar \
spark-submit \
--conf spark.task.maxFailures=100 \
--conf spark.yarn.max.executor.failures=200 \
--driver-java-options "-Dlog4j.configuration=file:${currentDir}/conf/log4j.properties" \
--class com.cmcm.datahero.content.nrt.cfb.ItemCFApp \
--master yarn-client \
--driver-memory 2g \
--executor-memory 2g \
--executor-cores 2 \
--num-executors 25 \
--queue experiment \
${currentDir}/streaming_item_cf-0.1-jar-with-dependencies.jar \
-c ${currentDir}/conf/ #1>${currentDir}/logs/stdout 2>${currentDir}/logs/stderr

