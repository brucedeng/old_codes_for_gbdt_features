# spark program for india model

this file contains scala code and related bash scripts for running spark applications

## environment setup

this session introduces environment setup. including compile environment and deploy environment.

### compile environment

To set up compile environment, you need : 

tool | version
--------- | ---------
java | Java HotSpot(TM) 64-Bit Server VM 1.7.0_79 (1.8 is not compatible)
scala | 2.10.3 or later
maven | apache-maven-3.3.1

Intellij is very useful for developing scala, because it has useful code completation. You can down load it from the [offical site](https://www.jetbrains.com/idea/).

### deploy

If you compile code locally, and mannually upload it to server, you can use the scripts deploy.sh and SimpleHTTPServerWithUpload.py. 

the deploy.sh only useful for sending file to 10.2.2.238, news_model launcher. you can modefy it for other launchers.

1. on server side, cd to your workspace path, and run 
```
python  SimpleHTTPServerWithUpload.py <port>
```

2. on your laptop, set the DEPLOY_PORT environment according to your server port
```
export DEPLOY_PORT=<port>
```

3. on your laptop, run 
```
deploy.sh <file>
```

### run application

the code in this directory uses spark 1.6. You must have spark 1.6 on hadoop grid. 

#### setup spark 1.6 on hadoop 2.6.0-cdh5.4.8

1. download 1.6 spark from [spark download site](http://spark.apache.org/downloads.html).
2. extract the downloaded archive to a directory, and add spark-XXX/bin to PATH
3. copy cdh configs to spark directory
``` cp /usr/lib/spark/conf spark-XXX/conf/
```
4. edit spark-XXX/conf/spark-env.sh , and change SPARK_HOME variable to your own directory.

After setup spark, run spark-shell, to check the spark version.

```
=> spark-shell
...
Welcome to
      ____              __
     / __/__  ___ _____/ /__
    _\ \/ _ \/ _ `/ __/  '_/
   /___/ .__/\_,_/_/ /_/\_\   version 1.6.0
      /_/

```

#### submit jobs to spark

```
nohup spark-submit --num-executors 120 --executor-cores 4 --class com.cmcm.ranking.india.trainUp.trainCatMethod3 --master yarn-client --queue experiment --executor-memory 4g india_ranking_spark-0.1-SNAPSHOT.jar train_up_l20.01.conf > parquet_train_catsl1-0.01.log 2>err.log &
```

notice: 240 executors, 4cores, 10g memory will occupy the whole experiment queue. usually, 150 executors with 4g may bu suitable. this depends your data size. For example, for a 100G size training data, setting it to 180 executors, 4 cores, 5g memory, is sufficient.

#### view parquet files

parquet is a fileformat that spark and pig loads extremely fast. However, it is a binary file format, and hadoop can not show the file content correctly. 

You need parquet-tools to view parquet files. first, download parquet-tools.jar

```
wget 'http://central.maven.org/maven2/com/twitter/parquet-tools/1.6.0/parquet-tools-1.6.0.jar'
```
Then, just run the command:

```
hadoop jar parquet-tools-1.6.0.jar cat hdfs://mycluster/projects/news/model/training/merged_events/hi_features_raw_dwell/20160430 | less
```
or just show the first 10 items

```
hadoop jar parquet-tools-1.6.0.jar head hdfs://mycluster/projects/news/model/training/merged_events/hi_features_raw_dwell/20160430 | less
```

To simplify this command, you can also use the parquet-tools.sh script. put parquet-tools.sh and parquet-tools-1.6.0.jar in the sample directory, and run

```
parquet-tools.sh cat hdfs://mycluster/projects/news/model/training/merged_events/hi_features_raw_dwell/20160430 | less
```