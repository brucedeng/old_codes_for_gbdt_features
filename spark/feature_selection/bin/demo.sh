#!/bin/bash
set -x

jarname=../target/scala-2.10/feature_selection-assembly-1.0.jar
task_name=feature_selection_tongming
init_model="hdfs:///projects/news/model/online/en_us_lr/one_min_logistic_regression_ftrl/20161002/09/59/ftrlOut/smart"
training_data_path="hdfs:/projects/news/model/en_us_lr/gen_training_data_streaming/training_data/20161002/0[7-9]/*/*"
test_data_path="hdfs:/projects/news/model/en_us_lr/gen_training_data_streaming/training_data/20161002/10/*/*"
middle_temp_path="hdfs:/projects/news/model/tongming/cross_selection_abs/temp"
result_path="hdfs:/projects/news/model/tongming/cross_selection_abs/result" 
iter_num=2
lambda1=0.1
lambda2=1.0
alpha=0.003
beta=1
minPv=5
method="abs"  #"rel"


MASTER=yarn-cluster /usr/bin/spark-submit \
        --class com.ijinshan.mllib.ftrl.DeltaTrain \
        --master yarn-cluster \
        --driver-memory 4g \
        --executor-memory 5g \
        --executor-cores 4 \
        --queue experiment \
        --num-executors 80 \
        --verbose \
        --conf spark.akka.timeout=300 \
        --conf spark.network.timeout=360 \
        --conf spark.akka.frameSize=300 \
        --conf spark.driver.maxResultSize=4g \
        --conf spark.storage.memoryFraction=0.7 \
        --conf spark.shuffle.memoryFraction=0.3 \
        --conf spark.broadcast.blockSize=4096 \
        --conf spark.broadcast.factory=org.apache.spark.broadcast.TorrentBroadcastFactory \
        --conf spark.broadcast.compress=true \
        --conf spark.serializer=org.apache.spark.serializer.KryoSerializer \
        --conf spark.kryo.registrator=com.ijinshan.mllib.ftrl.MyRegistrator \
        --conf spark.akka.threads=4 \
        --conf spark.default.parallelism=80 \
        ${jarname} \
        ${task_name} \
        ${init_model} \
        ${training_data_path} \
        ${test_data_path} \
        ${middle_temp_path} \
        ${result_path} \
        ${iter_num} \
        ${alpha} \
        ${beta} \
        ${lambda1} \
        ${lambda2} \
        ${minPv} \
        ${method} > ./log/feature_selection.log 2>&1 
