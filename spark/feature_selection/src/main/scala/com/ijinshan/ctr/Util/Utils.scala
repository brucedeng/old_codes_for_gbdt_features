package com.ijinshan.ctr.Util

import java.io.PrintWriter
import java.io.FileWriter
import java.io.BufferedReader
import java.io.FileReader
import scala.collection.mutable.HashSet
import scala.collection.mutable.HashMap
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import scala.collection.immutable.{Vector, Queue}
import org.apache.spark.rdd.RDD
import scala.io.Source
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap
import it.unimi.dsi.fastutil.longs.Long2IntOpenHashMap
import java.security.MessageDigest

object Utils {

  private val SYMBOL_EQUAL = '='

   def getFs(path: String) : FileSystem= {
   //find root.
   var slashLeft = 2
   var rootEnd = -2
   var index = 0
   path.foreach(c =>{
     if((c == '/' || c == '\\') && (slashLeft > 0)){
       if(rootEnd != index - 1){  //ignore the continue slash like the second '/' in "s3://"
         rootEnd = index
         slashLeft -= 1
       }
     }
     index += 1
   })

   val root = path.substring(0, rootEnd + 1)
//   println("Debug: get the file system root path: " + root)
   val fs = FileSystem.get(new java.net.URI(root), new Configuration())
   fs
 }
  
  def parseArgs(args: Array[String]): HashMap[String, String] = {
    val argsMap: HashMap[String, String] = HashMap()
    args.foreach(v => {
      val index = v.indexOf(SYMBOL_EQUAL)
      require(index > 0, s"Command line argument error: $index, can't find = in augument" + v)
      val key = v.substring(0, index)
      val value = v.substring(index + 1, v.length)

      argsMap.put(key, value)
    })

    argsMap
  }

  def parseSubset(subset: String, pnum: Int): HashMap[Int, HashSet[String]] = {
    val subsetMap = HashMap[Int, HashSet[String]]()
    subset.split(",").foreach(v => {
      val pair = v.split(":")
      require(pair.length == 2, s"wrong format for <subset> param, useage: subset=index:param,...")
      val index = pair(0).toInt - 1
      require(index > -1 && index < pnum, s"wrong format for <subset> param: index " + index + " out of range")
      val param = pair(1)
      if (!subsetMap.contains(index)) {
        subsetMap.put(index, HashSet[String]())
      }

      val inner = subsetMap.get(index).get
      inner.add(param)
    })
    subsetMap
  }

   
   def getMaxId(path:String) : Long = {
     val inputStream = new BufferedReader(new FileReader(path));
     var max = inputStream.readLine().toLong
     if(max == 0)
       max = 1  //0 is resvered for beta
     max
   }
   
   def getMaxIdHadoop(path:String) : Long = {
     val fs = Utils.getFs(path)
     val fsPath = new Path(path)

     val fsin = fs.open(fsPath)
     var max = fsin.readLine().toLong
     
     if(max == 0)
       max = 1
     max
   }
   
  def saveMaxId(max:Long, path:String){
     val out = new FileWriter(path, false)
     out.write(max.toString())
     out.close()    
  }
  
   def saveMaxIdHadoop(max:Long, path:String){
     val fs = Utils.getFs(path)
     val fsPath = new Path(path)
     
     fs.delete(fsPath, true)
     val fso = fs.create(fsPath)
     val str = max + ""
     
     fso.write((str.getBytes()))
     fso.close()
     
   }
   
 def rename(src: String, dst: String){
     val fs = getFs(src)
     fs.rename(new Path(src), new Path(dst))
 }
   
   def rmDir(path : String){
    val fs = getFs(path)
    fs.delete(new Path(path), true) 
  }
      
   def cleanPath(path : String){
    val fs = getFs(path)
    fs.delete(new Path(path), true) 
    fs.mkdirs(new Path(path))
    
  }
   
   def getPrefix(a:String, b:String) : Int = {
    var l = a.length
    if(l > b.length)
      l = b.length
    var prefixLength = 0
    var i = 0
    while(i < l){
      if(a.charAt(i) != b.charAt(i)){
        i = l
      } else {
        i += 1
        prefixLength += 1
      }
    }
    
    prefixLength
  }
   
  def genUrl(files:Queue[String]) : String = {
    var prefix = ""
    if(files.length == 1)
      return files.head
      
    //cal prefix
    files.foreach(path => {
      if(prefix.length == 0){
        prefix = path
      } else {
        val preLength = getPrefix(prefix, path)
        if(preLength == 0)
         throw new RuntimeException("Error: unexpected error, paths don't have common prefix")
        prefix = prefix.substring(0, preLength)
      }
    })
    
    var url = prefix + "{"
    val index = prefix.length()
    files.foreach(path => { 
      val postfix = path.substring(index)
      url += postfix + ","
    })
    url = url.substring(0, url.length - 1) //remove the last ","
    url += "}"
    
    url
  }
  
  def isNum(str : String) : Boolean = {
    if(str.length < 1)
      return false
    val start = if(str.charAt(0) == '-') 1 else 0
    for(i <- start until str.length()){
      val c = str.charAt(i)
      if(c < '0' || c > '9')
        return false
    }
    
    true
  }
  
  
   def genHash(rdd : RDD[String], map : Object2IntOpenHashMap[String]) : Object2IntOpenHashMap[String] = {

      map.clear()
      val mapArr = rdd.map(line=> {
        val slides = line.split("\t")
        (slides(0), slides(1).toInt)
      }).collect()
      
      mapArr.foreach(x => map.put(x._1, x._2))
      map

  }

  def genHashLongKey(rdd : RDD[String], map : Long2IntOpenHashMap) : Long2IntOpenHashMap = {
      map.clear()
      val mapArr = rdd.map(line=> {
        val slides = line.split("\t")
        (hashLong(slides(0)), slides(1).toInt)
      }).collect()
      
      mapArr.foreach(x => map.put(x._1, x._2))
      map

  }
  val hashLong : (String) => Long = {(
                            str: String )  =>
    {
        val md5 = MessageDigest.getInstance("MD5")
        val bKey = md5.digest(str.getBytes())  //128 bit. 
        var res : Long = 0
        for(i <- 0 until 8){
          res = res + ((bKey(i).toLong & 0xff)<< (8 * i))
        }
        res
    }}
     
        
  def genHashDouble(rdd : RDD[String], map : HashMap[String, Double]) : HashMap[String, Double] = {

      map.clear()
      val mapArr = rdd.map(line=> {
        val slides = line.split("\t")
        (slides(0), slides(1).toDouble)
      }).collect()
      
      mapArr.foreach(x => map.put(x._1, x._2))
      map

  }
  
  def genHashDoubleLocal(file : String , map : HashMap[String, Double]) : HashMap[String, Double] = {

      map.clear()
      for(line <- Source.fromFile(file).getLines()){
         //example:       
        // beta    -5.184572641806929
        // chpkg__0_air.com.slotgalaxy     -0.011816394477949178
        // chpkg__0_com.allstargames.allstarheroes.en      -6.952745614589477E-4
        val items = line.split("\t")
        if(items.length >= 2){
          map.put(items(0), items(1).toDouble)  
        } else {
          throw new RuntimeException("unexpected format from test data: " + line)
        }
      }
      map
  }
}
