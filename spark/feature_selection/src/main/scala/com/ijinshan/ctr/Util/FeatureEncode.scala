package com.ijinshan.ctr.Util
import org.apache.spark.SparkContext.rddToPairRDDFunctions 
import org.apache.spark.rdd.RDD
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.SparkContext._ 
import java.io.PrintWriter
import java.io.FileWriter
import java.io.BufferedReader
import java.io.FileReader
import scala.io.Source
import scala.collection.mutable.{HashMap, HashSet}
import scala.collection.mutable.ArrayBuffer
import org.apache.spark.broadcast.Broadcast
import scala.util.Random
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap
import it.unimi.dsi.fastutil.longs.Long2IntOpenHashMap
import java.security.MessageDigest

//ftrl feature encode.
object FeatureEncode {
  //tmp sets, may be updated part by part.

  val setFilter = HashSet[String]()
  var weightMappingFolder = ""
  var nextIdFile = ""
  var filteredWeightFolder = ""
  var filteredWeightIdFolder = ""
  var dataPath = ""
  var sizeLimit = ""
  var setFilterFile = ""
  var outputData = ""
  var minPV = 0
  var infoCols = 0
  var sc : SparkContext = null
  var transferRddRatio = 0.0
  var resetWeight = false
  var filteredSetId = ""
  
  val RDDRATIO = "rddratio"
  val SIZELIMIT = "sizelimit"
  val TRAINDATA = "traindata"
  val SETFILTER = "setfilter"
  val OUTPUT = "outputdata"
  val TMPFOLDER = "tmpfolder"
  val MINPV = "minpv"
  val INFOCOLS = "infocolumns"
  val IDONCE = "idonce"
  val spliter = "[\\s\\t]"
  val WEIGHTROOT = "weightroot" 
  val RESETWEIGHT = "resetweight"
  
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName(s"Feature Encode")
    sc = new SparkContext(conf)   
    
    val argMap = Utils.parseArgs(args)
    sizeLimit = if(argMap.contains(SIZELIMIT)) argMap(SIZELIMIT) else "2G"
    dataPath = argMap(TRAINDATA)
    setFilterFile = argMap(SETFILTER)
    outputData = argMap(OUTPUT)
    minPV = if(argMap.contains(MINPV)) argMap(MINPV).toInt else 0
    infoCols = if(argMap.contains(INFOCOLS)) argMap(INFOCOLS).toInt else 2  //(pv, click) or (pv, click, install)
    transferRddRatio = if(argMap.contains(RDDRATIO)) argMap(RDDRATIO).toDouble else 0.6
    resetWeight = if(argMap.contains(RESETWEIGHT)) argMap(RESETWEIGHT).toBoolean else false
    
    var weightRoot = argMap(WEIGHTROOT)
    weightRoot = if(weightRoot.endsWith("/")) weightRoot else weightRoot + "/" 
    weightMappingFolder = weightRoot + "weightMapping"  //original weight mapping, (feature, id)
    nextIdFile = weightRoot + "nextId.txt"              //original nextId file
    filteredWeightFolder = weightRoot + "filteredWeight"  //new filteredWeight, (feature)
    filteredWeightIdFolder = weightRoot + "filteredWeightId"  //new filteredWIeghtId, (feature, id)
    filteredSetId = weightRoot + "filteredSetId"
    if(resetWeight){
      Utils.cleanPath(weightMappingFolder)
      Utils.saveMaxIdHadoop(1, nextIdFile)
    }
    
    filterWeight()
    val features = sc.textFile(filteredWeightFolder).map(line => (line, true))  //(feature, dummy)
    updateWeightId(features)
    transfer(dataPath, sc, transferRddRatio, weightMappingFolder, sizeLimit, infoCols, outputData)
  }
 
  def filterWeight() {
    initSetMap(setFilterFile, setFilter)
    
    val rawRdd = sc.textFile(dataPath)

    println("Debug: Enter filterWeight, setFilter size is " + setFilter.size + ", minPV is " + minPV +", infoCols is " + infoCols)
    val br = sc.broadcast(setFilter)
    val brCols = sc.broadcast(infoCols)
    val featuresRdd =  rawRdd.flatMap { line  => {
      val slides = line.split(spliter)
      val brSetFilter = br.value
      val info = brCols.value
      if(slides.length < info + 1){
         val buffer = new ArrayBuffer[(String, Int)](0)   
         buffer
      } else {

        var pv = 0
        try{
         pv = slides(slides.length - info).toInt
        }catch  {
          case ex: Exception =>
            new RuntimeException("get exception for substring:\nline: " + line + "\nslides.length-infoCols: " + (slides.length - infoCols) + "\nexception is: " + ex.getMessage)
        }
        val buffer = new ArrayBuffer[(String, Int)](slides.length - info)
        for(i <- 0 until (slides.length - info)){
            var filter = false

            brSetFilter.foreach { setName => {
              if(slides(i).startsWith(setName))
                filter = true
            } }
            if(!filter)
              buffer.append((slides(i), pv))
        }
        buffer
      }
    }}.reduceByKey((x, y) => x + y)

//    Utils.rmDir(filteredSetId)
//    featuresRdd.saveAsTextFile(filteredSetId)
    val oldSize = featuresRdd.count()
    val brPv = sc.broadcast(minPV)
    val filtered = featuresRdd.filter(x => ((x._1.length > 0) && (x._2 >= brPv.value))).map(x => x._1)
    brPv.unpersist()
    val newSize = filtered.count()
    println("Debug: size after setFilter is " + oldSize + " size after pv filter is " + newSize)

    val tmpF = filteredWeightFolder + "tmp"
    filtered.saveAsTextFile(tmpF)
    Utils.rmDir(filteredWeightFolder) 
    Utils.rename(tmpF, filteredWeightFolder)
    
    println("Debug: finish filterWeight")
  }
  
  def getPartition(size : Long, originalPartitions : Int):Int = {
    var partitions = (size  / (30000)).toInt  //each partition save 1M record
//    if(partitions <= (originalPartitions / 2))
//      partitions = originalPartitions 
    if(partitions <= 0)
      partitions = 1
      partitions
  }
  
  def updateWeightId(features : RDD[(String, Boolean)]) = {
    println("Debug: enter updateWeightId")
    val oldMappingRdd = sc.textFile(weightMappingFolder).flatMap(line => {
      val slides = line.split("\t")
      if(slides.length != 2){
        val buffer = new ArrayBuffer[(String, Int)](0)
        buffer
      } else {
        val buffer = new ArrayBuffer[(String, Int)](1)
        buffer.append((slides(0), slides(1).toInt))
        buffer
      }
    })  //(feature, featureId)
    
    val newMappingCount = features.count()
    val joinedCols = features.leftOuterJoin(oldMappingRdd)  //feature, (dummy, featureId)
//    val covered = joinedCols.flatMap{
//     case(feature, (dummy, Some(featureId))) => { 
//        val buffer = new ArrayBuffer[String](1)
//        buffer.append(feature + "\t" + featureId)
//        buffer
//      }
//      case(feature, (dummy, None)) => {  
//        val buffer = new ArrayBuffer[(String)](0)
//        buffer
//      }
//    }
    
    val uncovered = joinedCols.flatMap{           
      case(feature, (dummy, None))=> { //pv and click
        val buffer = new ArrayBuffer[String](1)
        buffer.append(feature)
        buffer
      }
      case(feature, (dummy, Some(featureId))) => {  
        val buffer = new ArrayBuffer[(String)](0)
        buffer
      }
    }
    
    val newIds = uncovered.zipWithIndex()
    val usize = uncovered.count()
    println("Debug: features count from training data is " + newMappingCount + ", uncovered size is " + usize)
    var nextId = Utils.getMaxIdHadoop(nextIdFile)
    if(nextId == 0)
      nextId = 1
    println("Debug: get nextId " + nextId)    
    val newFeatureIds = newIds.map(x => 
      {
        (x._1 + "\t" + (x._2 + nextId))
      })
    
    val fullNewWeight = oldMappingRdd.map(x=>{
      (x._1 + "\t" + x._2)
    }).union(newFeatureIds)
    val fullNewSize = fullNewWeight.count()
    val partitionsNew = getPartition(fullNewSize, fullNewWeight.partitioner.size)
    
    Utils.rmDir(filteredWeightIdFolder) 
    println("Debug: will save filtered weight with id by " + partitionsNew + " partitions")
    fullNewWeight.saveAsTextFile(filteredWeightIdFolder)  //.coalesce(partitionsNew)
    
    val newCover = usize
    if(newCover > 0){
      val fullWeight = oldMappingRdd.map(x=> (x._1 + "\t" + x._2)).union(newFeatureIds)
      val next = fullWeight.count() + 1
      Utils.saveMaxIdHadoop(next, nextIdFile)
      val partitionsFull = getPartition(next, fullWeight.partitions.size)
      val tmpF = weightMappingFolder + "tmp"
      println("Debug: nextId is updated to " + nextId + "\n weight count is "+ next + "\n partitions are" + partitionsFull)
      fullWeight.coalesce(partitionsFull).saveAsTextFile(tmpF)
      Utils.rmDir(weightMappingFolder) 
      Utils.rename(tmpF, weightMappingFolder)
    }
    println("Debug: finish updateWeightId")
  }
  
  def filter(minPV : Int, inPath : String , outPath : String, sc : SparkContext, infoCols : Int) = {
    println("Debug: Enter filter, setFilter size is " + setFilter.size)
    val rawRdd = sc.textFile(inPath)

    
    val br = sc.broadcast(setFilter)
    val filtered = rawRdd.map { line  => {
      val slides = line.split(spliter)
      if(slides.length < infoCols + 1){
       val buffer = new ArrayBuffer[(String, Int)](0)
       buffer
      } else {
        val pv = slides(slides.length - infoCols).toInt
        val buffer = new ArrayBuffer[(String, Int)](slides.length - infoCols)
        for(i <- 0 until (slides.length - infoCols)){
            var filter = false
            val brSetFilter = br.value
            brSetFilter.foreach { setName => {
              if(slides(i).startsWith(setName))
                filter = true
            } }
            if(!filter)
              buffer.append((slides(i), pv))
        }
      }
    }}
    br.unpersist()
    
    filtered.saveAsTextFile(outPath)
  }
  
  //transfer raw data from SetId_feature to SetId:SetId_feature
  //parameter:
  //  raw                    raw training data, format: SetId_feature SetId_feature showPV clickPV
  //  featureIdMapping  [feature, featureId]. 
  //  setFilter         [the set need to be filtered]
  //  filterUnmatch      if true, will filter the records which has no matched id.
  //return:
  //  (String)      （new format)
  /*
   def encode(raw:String, featureHash:Object2IntOpenHashMap[String], infoCols : Int, filterUnmatch : Boolean) : (String) = {
     
     val slides = raw.split(spliter)
     if(slides.length < 3){
//       println("raw training data must has at least 3 columns:\n\t" + raw)
       return ""
     }
     var instance = ""
//     println("ncode Debug: encode, infoCols is " + infoCols)
     for(i <- 0 until slides.length - infoCols){
       //Set_feature
       if(slides(i).contains("_") && featureHash.containsKey(slides(i))){  //it's not a number
           instance += featureHash.getInt(slides(i)) + "\t"
       } else {
         //it's alread a id, or a str.
         if(Utils.isNum(slides(i)) || !filterUnmatch)
             instance += slides(i) + "\t"
       }
     }

     if(instance.length > 0){
       for(i <- (slides.length - infoCols) until slides.length)
         instance += slides(i) + "\t"
       instance = instance.trim()
     }
     instance
    }
   */

   /*
    def encodeLongKey(raw:String, featureHash:Long2IntOpenHashMap, infoCols : Int, filterUnmatch : Boolean) : (String) = {
     
     val slides = raw.split(spliter)
     if(slides.length < 3){
       return ""
     }
     var instance = ""
     for(i <- 0 until slides.length - infoCols){
       //Set_feature
       val hash = Utils.hashLong(slides(i))
       if(slides(i).contains("_") && featureHash.containsKey(hash)){  //it's not a number
           instance += featureHash.get(hash) + "\t"
       } else {
         //it's alread a id, or a str.
         if(Utils.isNum(slides(i)) || !filterUnmatch)
             instance += slides(i) + "\t"
       }
     }

     if(instance.length > 0){
       for(i <- (slides.length - infoCols) until slides.length)
         instance += slides(i) + "\t"
       instance = instance.trim()
     }
     instance
    }
    */
   def encode(raw: String, featureHash: Object2IntOpenHashMap[String], infoCols: Int, filterUnmatch: Boolean): (String) = {
     val slides = raw.split("\\t")
     if(slides.length < 3){
       return ""
     }
     var instance = ""
     for(feature <-  slides.last.split("\\s")){
       //Set_feature
       if(feature.contains("_") && featureHash.containsKey(feature)){  //it's not a number
           instance += featureHash.get(feature) + "\t"
       }
     }

     if (instance.length > 0) instance += slides.dropRight(1).last

     instance

   }
   def encodeLongKey(raw: String, featureHash: Long2IntOpenHashMap, infoCols: Int, filterUnmatch: Boolean): (String) = {
     val slides = raw.split("\\t")
     if(slides.length < 3){
       return ""
     }
     var instance = ""
     for (feature <-  slides.last.split("\\s")){
       //Set_feature
       val hash = Utils.hashLong(feature)
       // if(feature.contains("_") && featureHash.containsKey(hash)){  //it's not a number
       if(featureHash.containsKey(hash)){  //it's not a number
           instance += featureHash.get(hash) + "\t"
       }
     }

     if (instance.length > 0) instance += slides.dropRight(1).last

     instance
   }
     
      
   def initSetMap(path:String, set : HashSet[String]){
      set.clear()
      val inputStream = new BufferedReader(new FileReader(path));
      var line = inputStream.readLine()
      while((line != null)){
        val setName = if(line.endsWith("_")) line else line + "_"
        set.add(setName)
        line = inputStream.readLine()
      }
      inputStream.close()
      println("Debug: Set Filter size is " + setFilter.size)
      
   }


   def transfer(inPath : String, scl : SparkContext, ratio : Double, idMappingFolder : String, rddSizeLimit : String, infoColsL : Int, outputFolder:String) = {
     val mems = scl.getExecutorMemoryStatus
     val cacheMem = mems.head._2._1
     println("Debug: Enter transfer, cached memory is " + (cacheMem / 1024 / 1024).toInt + "MB")


     var rdd : RDD[String] = scl.textFile(inPath)
     val sm = scl.textFile(idMappingFolder + "/*")
     var hashRdd: RDD[String] = sm
     val featureIdMapping = new Object2IntOpenHashMap[String] 

     Utils.genHash(hashRdd, featureIdMapping)
     rdd = encodeByHash(rdd, infoColsL, true, featureIdMapping)

     val rawFolder = outputFolder + "_raw"
     rdd.saveAsTextFile(rawFolder)
     format(rawFolder, outputFolder + "/0", scl)

     println("Debug: Finish transfer")
   }

   def transferLongKey(inPath : String, scl : SparkContext, ratio : Double, idMappingFolder : String, rddSizeLimit : String, infoColsL : Int, outputFolder:String) = {
     val mems = scl.getExecutorMemoryStatus
     val cacheMem = mems.head._2._1
     println("Debug: Enter transfer, cached memory is " + (cacheMem / 1024 / 1024).toInt + "MB")

    
     var rdd : RDD[String] = scl.textFile(inPath)
     val sm = scl.textFile(idMappingFolder + "/*")
     var hashRdd: RDD[String] = sm
     val featureIdMappingLong = new Long2IntOpenHashMap
     
     Utils.genHashLongKey(hashRdd, featureIdMappingLong)
     rdd = encodeByHashLongKey(rdd, infoColsL, true, featureIdMappingLong)
     
     val rawFolder = outputFolder + "_raw"
     rdd.saveAsTextFile(rawFolder)
     format(rawFolder, outputFolder + "/0", scl)
     println("Debug: Finish transfer")
   }

   // format: "label:weight\tfeaturehashValue\tfeaturehashValue\t..."
   def format(rawFolder: String, outputFolder: String, scl: SparkContext): Unit = {
     val rdd = scl.textFile(rawFolder)
     rdd.map(line => {
       val terms = line.split("\\t")
       (terms.last :: terms.dropRight(1).toList).mkString("\t")
     }).mapPartitionsWithIndex((idx, partition) => Random.shuffle(partition))
       .saveAsTextFile(outputFolder)
   }
   
   /*
   def format(rawFolder: String, outputFolder: String, scl: SparkContext) = {
       val rdd = scl.textFile(rawFolder)
       
       rdd.flatMap(line => {
         val cleanLine = line.trim()
         if(cleanLine.length < 2){
           val buffer = new ArrayBuffer[String](0)
           buffer
         } else{           
           val bytes = cleanLine.getBytes
           val length = bytes.length
           var finish = false
           var found = 0
           var curIndex = length - 2
           var pv=0
           var click=0
           var lastTabIndex = 0
           while(!finish && curIndex > 0){
             if(bytes(curIndex) == '\t'){
               found += 1
               if(found == 1){
                 try{
                   click = cleanLine.substring(curIndex + 1).toInt
                 }catch  {
                    case ex: Exception =>
                      println("get exception for substring:\nline: " + cleanLine + "\ncurIndex: " + curIndex + "\nexception is: " + ex.getMessage)
                  }
                 lastTabIndex = curIndex
               }else if(found == 2){
                 try{
                   pv = cleanLine.substring(curIndex + 1, lastTabIndex).toInt
                 }catch  {
                   case ex: Exception =>
                      println("get exception for substring:\nline: " + cleanLine + "\ncurIndex: " + curIndex + "exception is: " + ex.getMessage)
                 }
                 finish = true
               }
             }
             curIndex -= 1
           }
    
           var instance = ""
           try{
              instance = line.substring(0, curIndex + 1)  //take care of curIndex here.
           if(click > pv)
             click = pv
           val unclick = pv - click    
           
           val buffer = new ArrayBuffer[String](pv)
           val posInstance = "1\t" + instance
           val negInstance = "0\t" + instance
           if(instance.length > 0){
             for(i <- 0 until pv ){
               if(i < click)
                 buffer.append(posInstance)
               else
                 buffer.append(negInstance)  
             }
           }
           buffer
           }catch  {
              case ex: Exception =>{
                println("get exception for substring:\nline: " + cleanLine + "\ncurIndex: " + curIndex + "\nline length is " + line.length() + "\nexception is: " + ex.getMessage)
                val buffer = new ArrayBuffer[String](0)
                 buffer
              }
            }
         }
       }).mapPartitionsWithIndex((idx, pIter) => {
         val iter = Random.shuffle(pIter)
         iter
       }).saveAsTextFile(outputFolder )
       println("Debug: save new format to path: " + outputFolder )       


   }
   */
   


    def encodeByHashLongKey(data:RDD[String], infoCols:Int, filterUnmatch : Boolean, map:Long2IntOpenHashMap) : RDD[String] = {
     val sc = data.sparkContext
     val brHash = sc.broadcast(map)
     println("Debug : featureIdMapping for this time is " + map.size)
     val formatted = data.map( line => {
       encodeLongKey(line, brHash.value, infoCols, filterUnmatch)
     }).filter(line => line.length() > 1)
     brHash.unpersist()
     
     formatted
   }

   def encodeByHash(data:RDD[String], infoCols:Int, filterUnmatch : Boolean, map:Object2IntOpenHashMap[String]) : RDD[String] = {
     val sc = data.sparkContext
     val brHash = sc.broadcast(map)
     println("Debug : featureIdMapping for this time is " + map.size)
     val formatted = data.map( line => {
       encode(line, brHash.value, infoCols, filterUnmatch)
     }).filter(line => line.length() > 1)
     brHash.unpersist()
     
     formatted
   }
  

}
