package com.ijinshan.ctr.Util

import scala.collection.immutable.{Vector, Queue}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkContext, SparkConf}

class PartitionManager(url: String, sc:SparkContext, reservedMem:Long){
  var partitions = 0
  var sizeLimit = 0L
  var totalSize = 0L
  var unLoadedFiles = scala.collection.immutable.Queue[(String, Long)]()

  val P_MINE="minexecutors"
  var minExecutors = 1
  
  def getFullSize : Long = {
    if(totalSize == 0)
        throw new RuntimeException("totalSize is 0.")
    totalSize
  }
  def coldStart() = {
    val conf = sc.getConf
    val expectExecutor = conf.getInt("spark.executor.instances", 1) 
    val expectCore = conf.getInt("spark.executor.cores", 1)
    val expectPartition = expectExecutor * expectCore

    if(unLoadedFiles.size > 0){
      val (path, size) = unLoadedFiles.head
      val rdd = sc.textFile(path, expectPartition)
      println("cold start with size " + rdd.count())
    }
  }
  
  def nextRdd(notMerge : Boolean = false) : (Boolean, RDD[String]) = {
    if(partitions == 0){
      init()
    }
    calPartitions()
      
    if((unLoadedFiles.length == 0)){
      println("Debug: there is no left data to load from path \n" + url)
      return (true, null)
    }
 
    if(partitions == 0){
      throw new RuntimeException("can't get resources to load data.")
    }
    
    var curSize = 0L
    var files = Queue[String]()
    while(curSize < sizeLimit && unLoadedFiles.length>0){
      val (file, q) = unLoadedFiles.dequeue
      unLoadedFiles = q
      files = files.enqueue(file._1)
      curSize += file._2
    }
    
    val newUrl = Utils.genUrl(files)
    if((newUrl.length == 0) || (curSize == 0)){
       println("Debug: didn't get sub urls \n")
      return (true, null)
    }
      
    var p = partitions
    if((curSize < (sizeLimit / 2)) && (notMerge == false)){
      println("Debug: curSize < (sizeLimit / 2), partitions = " + partitions + ", sizeLimit = " + sizeLimit + ", curSize = " + curSize)
      p = (p / (sizeLimit / curSize)).toInt + 1  //consider the last few files, the data sizes are much less than limit, the partition count should be reduced
      println("Debug: re-set the partitions to " + p + " to ensure each partition has enough data")
    }
 //   p = 2
    var rdd = sc.textFile(newUrl, p)

    println("Debug: load rdd from below url with " + p + " partitions :\n" + newUrl
          + "\ntotal load size is " + (curSize/1024/1024).toInt + "MB\neach partition load size is about " + (curSize/1024/1024/p).toInt + "MB" +
          "\nleft files are " + unLoadedFiles.length)
    if((rdd.partitions.size > p)  && (notMerge == false)){
      println("before coalesce, partitions is " + rdd.partitions.size)
      rdd = rdd.coalesce(p)
    }
    var lastRdd = false
    if(unLoadedFiles.length == 0){
      println("Debug: all the files has been loaded for path " + url)
      lastRdd = true
    }
    (lastRdd, rdd)
  }
  

  
 
  private def init() = {
    listFiles()
    coldStart()
    val conf = sc.getConf
    try{
      val confExecutors = conf.getInt(P_MINE, 0)
      if(confExecutors > minExecutors)
        minExecutors = confExecutors
    }catch  {
      case ex: Exception =>
        println("get exception for conf.getInt: " + P_MINE + "\nexception is: " + ex.getMessage)
    }
  }
  
  private def listFiles()= {
    val path = url
    val fs = Utils.getFs(path)
    val files = fs.globStatus(new Path(path))
    println("Debug: fetch files from path " + path)
    files.foreach { status => {
      if(!status.isDir() && (status.getLen > 0 )){
        unLoadedFiles = unLoadedFiles.enqueue((status.getPath.toString(), status.getLen))
        totalSize += status.getLen
  //      println("Debug: add in load list:\nFile path: " + status.getPath.toString() + "\nFile Size: " + status.getLen)
      }
    } }
  } 
  
  private def calPartitions(){
    val conf = sc.getConf
    val cores = conf.getInt("spark.executor.cores", 1)

    val mems = sc.getExecutorMemoryStatus
    val executors = if(mems.size > minExecutors) mems.size else minExecutors    //it's the only api I found in SparkContext to get real executor number.
    val cacheMem = mems.head._2._1
    
    partitions = cores * executors
    var maxDataSize = 80 * 1024 * 1024L
    if(cacheMem - reservedMem < maxDataSize) {
      maxDataSize = cacheMem - reservedMem
    }
 //   sizeLimit = executors * (cacheMem  - reservedMem)
    sizeLimit = executors * maxDataSize
    if(sizeLimit <= 0){
      throw new RuntimeException("sizeLimit = " + sizeLimit + ", reservedMem = " + reservedMem)
    }
    println("Debug: executors/cores is " + executors + "/" + cores + "\ncached mem for RDD is " + (cacheMem / 1024 / 1024).toInt + "MB")
    println("Debug: sizeLimit is " + (sizeLimit/ 1024 / 1024).toInt + "MB, Resvered Memory is " + (reservedMem / 1024 / 1024) + "MB, minExecutors is " + minExecutors)
  }
 

}
