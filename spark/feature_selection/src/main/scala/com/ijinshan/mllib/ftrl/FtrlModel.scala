/**
  * Created by tongming on 16/9/13.
  */

package com.ijinshan.mllib.ftrl

import com.ijinshan.ctr.Util.Utils

import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext

import it.unimi.dsi.fastutil.longs.Long2IntOpenHashMap

class FtrlModel(val intercept: Float,
                val shareFeatureWeight: Array[Float],
                val independentFeatureWeights: Array[Array[Float]]) extends Serializable {
  def predictMargin(instance: Instance): Float = {
    val weight = instance.shareIndex.map(shareFeatureWeight(_)).sum + intercept
    weight
  }

  override def toString = {
    shareFeatureWeight.size.toString
  }

}

object FtrlModel extends Serializable {
  def loadModel(path : String, dataFeature: RDD[(Long, Int)], sc : SparkContext, tmpPath:String) : FtrlModel = {
    val featureCode = sc.textFile(path).map { line =>
      val items = line.split("\\t")
      val featureName = items(0)
      val featureWeight = items(1).toFloat
      (Utils.hashLong(featureName), featureWeight)
    }.join(dataFeature).map(x =>(x._1, x._2._1)).zipWithIndex.persist()
    featureCode.map(x => x._1._1.toString + "\t" + x._2.toString).saveAsTextFile(tmpPath)
    val intercept = 0f
    val featureDecodeWeight =featureCode.map(x => (x._1._2, x._2)).collect.sortBy(_._2).map(_._1).toArray
    featureCode.unpersist()
    new FtrlModel(intercept, featureDecodeWeight, null)
  }


}
