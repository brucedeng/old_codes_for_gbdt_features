/**
  * Created by tongming on 16/9/13.
  */

package com.ijinshan.mllib.ftrl

import scala.collection.mutable.ArrayBuffer

object Metric extends Serializable {
  private def trapezoid(points: Seq[(Double, Double)]): Double = {
    require(points.length == 2)
    val x = points.head
    val y = points.last
    (y._1 - x._1) * (y._2 + x._2) / 2.0
  }
  def auc(sortedPredAndScore: Seq[(Double, Short)]): Double = {
    val roc_curve = ArrayBuffer[(Double, Double)]()
    var tp = 0d
    var fp = 0d
    roc_curve.append((tp, fp))
    sortedPredAndScore.foreach { case (pred, score) =>
      if(score == 1)
        tp += 1
      else
        fp += 1
      roc_curve.append((tp, fp))
    }
    roc_curve.toArray.map(tfp => (tfp._1/tp, tfp._2/fp)).sliding(2).aggregate(0.0d)(
      seqop= (auc: Double, points: Array[(Double, Double)]) => auc + trapezoid(points),
      combop = _ + _
    )
  }
}
