package com.ijinshan.mllib.ftrl

import java.text.SimpleDateFormat
import java.util.Calendar
import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.spark.SerializableWritable
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext.rddToPairRDDFunctions
import org.apache.spark.TaskContext
import org.apache.spark.annotation.DeveloperApi
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import com.ijinshan.ctr.Util.Utils
import org.apache.spark.storage.StorageLevel
import org.apache.spark.SerializableWritable

class Wzn(
           var w: Float,
           var z: Double,
           var n: Double,
           var ct: Short,
           var id : Int
         ) extends Serializable {

  def this() = this(0, 0, 0, 0, -1)
  def empty():Boolean = { return (w == 0 && z == 0 && n == 0 && ct == 0 && id == -1)}

  /**
   * only out w, z, n; this will be used to write temporary result, don't modify
   */
  //  override def toString() = "%.6f\t%.6f\t%.6f".format(w, z, n)
  override def toString() = "%s\t%s\t%s".format(w, z, n)

}
