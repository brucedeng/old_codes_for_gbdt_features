package com.ijinshan.mllib.ftrl

import scala.collection.JavaConversions._
import scala.collection.convert.WrapAsJava

import java.text.SimpleDateFormat
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.regex.PatternSyntaxException
import scala.collection.mutable.ArrayBuilder
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import scala.util.Random
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.SerializableWritable
import org.apache.spark.SparkContext
import org.apache.spark.TaskContext
import org.apache.spark.annotation.DeveloperApi
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import breeze.numerics.floor
import org.apache.commons.lang.StringUtils
import com.ijinshan.ctr.Util.{Utils, PartitionManager}
import com.google.common.util.concurrent.Striped
import java.util.concurrent.locks.Lock
import java.io.FileWriter
import com.esotericsoftware.kryo.Kryo
import org.apache.spark.serializer.KryoRegistrator

class WrapInt extends Serializable {
  var v = 0
}

class AddictiveFtrl (val numIterations: Int,
                     val alpha: Float,
                     val beta: Float,
                     val lambda1: Float,
                     val lambda2: Float,
                     val maxId : Int,
                     val modelTempPath: String) extends Serializable{
  @transient var arrayOfLocks : Striped[Lock] = null
  @transient var wznMapGlobal = new Array[(Wzn)](0)

  def updateMargin(data: RDD[Instance], groupFeatureKey: String, sc:SparkContext): RDD[Instance] = {
    val model = sc.textFile(modelTempPath).map{line =>
      val items = line.split("\1")
      val index = items(0).toInt
      val weight = items(1).toFloat
      (index, weight)
    }
    val selectedFeatures = data.flatMap {instance =>
      val keepFeature = instance.independentIndex.filter(_._1 == groupFeatureKey)
      if (keepFeature.size == 1)
        keepFeature(0)._2
      else
        None
    }.map(index => (index, 1)).reduceByKey((x,y) =>x)

    val keepModel = model.join(selectedFeatures).map(x => (x._1, x._2._1)).collectAsMap
    val modelDicBroad = sc.broadcast(keepModel)
    data.map {instance =>
      val keepFeature = instance.independentIndex.filter(_._1 == groupFeatureKey)
      var margin = 0f
      if (keepFeature.size == 1)
        margin = keepFeature(0)._2.map(index => modelDicBroad.value.getOrElse(index, 0f)).sum
      instance.setMargin(margin + instance.margin)
    }
  }

  def train(data : String, sc : SparkContext, middlePath: String, unselectedFeatures: HashSet[String], testData: RDD[Instance], prevTestAuc: Double, splitInfo: Map[String, Double], relMethod: Boolean): (String, (Double, Double, Int), Double, (Double, Double, Int)) = {
    //val pm = new PartitionManager(data , sc, maxId.toLong * 1100+600*1024*1024)  //estimate mem used for a (Int,Wzn) object is 50byte
    val pm = new PartitionManager(data , sc, maxId.toLong * 500)  //estimate mem used for a (Int,Wzn) object is 50byte

    wznMapGlobal = new Array[(Wzn)](maxId)

    for(i <- 0 until maxId){
      if(wznMapGlobal(i) == null)
        wznMapGlobal(i) = ((new Wzn()))
      wznMapGlobal(i).id = i
    }

    var finished = false
    var round = 0
    var rdd : RDD[String] = null
    val lossArray = ArrayBuffer[(String, (Double, Double, Int))]()
    val testLossArray = ArrayBuffer[(String, (Double, Double, Int))]()

    while(!finished){
      println("Debug: read rdd for training data, round " + round)
      val rst = pm.nextRdd()
      rdd = rst._2
      finished = rst._1

      val(trainLoss, testLoss)= train(rdd, unselectedFeatures,  sc, finished, testData)
      lossArray ++= trainLoss
      testLossArray ++= testLoss
      round += 1
    }
    val lossArraySum = lossArray.toArray.groupBy(_._1).map {case(k, v) =>
      val auc = v.map(_._2._1)
      val logloss = v.map(_._2._2)
      val count = v.map(_._2._3)
      (k, (auc.sum/auc.length, logloss.sum/count.sum, count.sum))
    }
    val testLossArraySumMid = testLossArray.toArray.groupBy(_._1).map {case(k, v) =>
      val auc = v.map(_._2._1)
      val logloss = v.map(_._2._2)
      val count = v.map(_._2._3)
      (k, (auc.sum/auc.length, logloss.sum/count.sum, count.sum))
    }
    val testLossArraySum = testLossArraySumMid.toSeq.map {case(k, v) =>
      (k, v, (v._1-prevTestAuc)/splitInfo.getOrElse(k, Double.MaxValue), lossArraySum.getOrElse(k, (0d, 0d, 0)))
    }
    sc.parallelize(testLossArraySum.toSeq, 1).map(x => x._1 + "\t" + x._2._1.toString +"\t" + x._2._2.toString + "\t" + x._2._3.toString + "\t" + x._3.toString+"\t"+x._4._1.toString + "\t" + x._4._2.toString+"\t"+x._4._3.toString).saveAsTextFile(middlePath)
    if(relMethod)
      testLossArraySum.maxBy(_._3)
    else
      testLossArraySum.maxBy(_._2._1)
  }
    
 def train(trainInput: RDD[String],unselectedFeatures: HashSet[String], sc : SparkContext, finished : Boolean, testData: RDD[Instance]): (Array[(String,(Double, Double, Int))], Array[(String, (Double, Double, Int))]) = {
    val data = trainInput.map(line=>
      Instance.loadFromString(line)
    )
    data.cache()
    val trainDataCount = data.count
    var aucCount = 200000.0d
    val sampleRate = if(trainDataCount > aucCount) aucCount/trainDataCount else 1.0d

    for(i <- 0 until numIterations) {
      //broadcast weight.
      println("LogisticRegressionWithFtrl, Debug: Start iteration " + i + " finished flag is " + finished + ", wznMapGlobal size is " + wznMapGlobal.size)
      val activities = new WrapInt()
      val brWeight = data.context.broadcast((wznMapGlobal,activities))

      val currIterRDD = data.mapPartitionsWithIndex{ case (idx, pIter) =>

        //add active flag
        var actCount = brWeight.value._2
        brWeight.value.synchronized{
          actCount.v += 1
          // println("Debug: actCount for partition " + idx + " is " + actCount.v)
        }

        var wznArray = brWeight.value._1

        // println(idx + " Step1: Update sample " + curTime())
        val iter = Random.shuffle(pIter)
        iter.foreach(instance => {
          brWeight.value.synchronized {
            updateSample(wznArray, instance, false)
          }
        })

        //sub active flag
        var retArr = wznArray
        brWeight.value.synchronized{
          actCount.v -= 1
          if(actCount.v != 0){
            //only the last thread return value.
            //others return null.
            retArr = new Array[(Wzn)](0)
          }
        }
        retArr.iterator
      }.map(a=> (a.id, a)).reduceByKey(sum(_,_))
        .map(a=>(a._2))
        .map(v => averageWeights(v))
      currIterRDD.persist()
      brWeight.unpersist()
      //println(s"${currIterRDD.count}, curIterRDDcout")
      wznMapGlobal = currIterRDD.collect().sortWith((a,b) => (a.id < b.id))
      if(finished && (i == (numIterations - 1))){
        val nonZeroRdd = currIterRDD.filter(_.w != 0d).map(v => v.id + "\1" + v.w)
        nonZeroRdd.saveAsTextFile(modelTempPath)
      }
      currIterRDD.unpersist()
    }
   val trainMetric = getEvalMetric(wznMapGlobal, data, unselectedFeatures, sampleRate)
   var testMetric = Array[(String, (Double, Double, Int))]()
   if(finished)
     testMetric = getEvalMetric(wznMapGlobal, testData, unselectedFeatures, 1.0d)
   data.unpersist(true)
   (trainMetric, testMetric)
 }

  private def getEvalMetric(wznA: Array[Wzn], dataRdd: RDD[Instance], unselectedFeatures:HashSet[String], sampleRate: Double): Array[(String, (Double, Double, Int))] = {
    val brWeight = dataRdd.context.broadcast(wznA)
    val brUnselectedFeatures = dataRdd.context.broadcast(unselectedFeatures)
    var rst = dataRdd.flatMap {sample =>
      val wznArray = brWeight.value
      val features = sample.independentIndex
      val label = sample.label
      val group_loss = ArrayBuffer[(String, (Short, Double, Double, Int))]()
      val includeFeatures = features.map(_._1).toSet
      for (groupFeature <- features) {
        val featureName = groupFeature._1
        val indexs = groupFeature._2
        val w = indexs.map(index => wznArray(index).w).sum + sample.margin
        val p = 1.0d / (1.0d + math.exp(-w))
        group_loss.append((featureName, (label, p, if(label ==0) -math.log(1-p) else -math.log(p), 1)))
      }
      val originP = 1.0d / (1.0d + math.exp(-sample.margin))
      val originLoss = if(label==0) -math.log(1-originP) else -math.log(originP)
      for(feature <- brUnselectedFeatures.value) {
        if (!includeFeatures.contains(feature))
          group_loss.append((feature, (label, originP, originLoss, 1)))
      }
      group_loss.toSeq
    }.persist()
    if (sampleRate < 1) {
      rst = rst.sample(false, sampleRate)
    }
    val sortAucArray = rst.map(x=>(x._1, (x._2._2, x._2._1))).groupByKey.mapValues(pas => Metric.auc(pas.toSeq.sortBy(_._1))).collect.sortBy(_._1)
    val logloss = rst.map(x=>(x._1, (x._2._3, x._2._4))).reduceByKey((x,y) => (x._1+y._1, x._2+y._2)).collect().sortBy(_._1)
    val metric = sortAucArray.zip(logloss).map(x => (x._1._1, (x._1._2, x._2._2._1, x._2._2._2)))
    brWeight.unpersist()
    metric
  }

  val updateSample : (Array[(Wzn)],Instance, Boolean) => Boolean = {(
                            wznArray: Array[(Wzn)],
                            sample: Instance,
                            debugInfo : Boolean)  =>
  {
    val features = sample.independentIndex
    val yt = sample.label
    val weight = sample.weight
    for(elem <- features) {
      val featureName = elem._1
      val indexs = elem._2
      try {
        var pt = sample.margin.toDouble
        for (index <- indexs) {
          val value = wznArray(index)
          val wnz = value
          val zi = wnz.z
          var wti = 0f
          if (math.abs(zi) > lambda1) {
            val ni = wnz.n
            // wti = wnz.w
            wti = (-(zi - FtrlUtils.sgn(zi) * lambda1) / ((beta + math.sqrt(ni)) / alpha + lambda2)).toFloat
          }
          if(debugInfo)
            println("tmp Debug, cal wti by zi and ni: w=%f, z=%f,n=%f,lambda1=%f,beta=%f, alpha=%f, lambda2=%f".format(wti, value.z, value.n, lambda1, beta, alpha, lambda2))
          pt += wti
          wnz.w = wti
        }
        if (pt > FtrlUtils.LR_SUM_MAX) {
          pt = FtrlUtils.LR_SUM_MAX
        } else if (pt < FtrlUtils.LR_SUM_MIN) {
          pt = FtrlUtils.LR_SUM_MIN
        }
        pt = 1.0 / (1.0 + math.exp(-pt))
        for (index <- indexs) {

          val wnz = wznArray(index)

          var zi = wnz.z
          val lz = zi
          var ni = wnz.n
          val wti = wnz.w

          val gi = (pt - yt) * weight

          val si = (math.sqrt(ni + gi * gi) - math.sqrt(ni)) / alpha
          zi = (zi + gi - si * wti).toDouble
          ni = (ni + gi * gi).toDouble
          if(debugInfo)
            println("tmp Debug, cal zi&ni: pt=%s, zi=%s, ni=%s, gi=%s,yt=%d, wti=%f, si=%f".format(pt, zi, ni, gi, yt, wti,si))
          wnz.n = ni
          wnz.z = zi
          if (math.abs(zi) > lambda1) {
            wnz.w = (-(zi - FtrlUtils.sgn(zi) * lambda1) / ((beta + math.sqrt(ni)) / alpha + lambda2)).toFloat
          } else{
            wnz.w = 0
          }

          wnz.ct = 1
        }
      } finally{
      }
    }
    true
  }}

  private def averageWeights(v: ( Wzn)): ( Wzn) = {
    var count = v.ct
    if(count == 0)
      count = 1
//    println("tmp Debug, averageWeights: wzn= " + v._2 + "\n count = " + v._2.ct)
    v.w = v.w / count
    v.z = v.z / count
    v.n = v.n / count
    v.ct = 0 //reset count
    v
  }

  private def sum(a: Wzn, b: Wzn): Wzn = {
    
    if((a.ct > 0) && (b.ct > 0)){
      a.w += b.w
      a.z += b.z
      a.n += b.n
      a.ct = (a.ct + b.ct).toShort
      a
    } else if(a.ct > 0){
      a
    } else {
      b
    }
  }
  
    private def sum2(a: Wzn, b: Wzn): Wzn = {
    
    if((a.ct > 0) && (b.ct > 0)){
      a.w = (a.w * a.ct + b.w * b.ct) / (a.ct + b.ct)
      a.z = (a.z * a.ct + b.z * b.ct) / (a.ct + b.ct)
      a.n = (a.n * a.ct + b.n * b.ct) / (a.ct + b.ct)
      a.ct = (a.ct + b.ct).toShort
      a
    } else if(a.ct > 0){
      a
    } else {
      b
    }
  }

}


object FtrlUtils {

  val TAB = "\t"

  val COLON = ":"

  val KEY_0 = 0
  
  val million = 1000000.0f

  val LR_SUM_MAX = 50.0d

  val LR_SUM_MIN = -50.0d



  def loadSamples(sc: SparkContext, path: String): RDD[String] = {
    sc.textFile(path)
      .map(_.trim)
      .filter(validLine)
  }

  def validLine(line: String): Boolean = {
    !(line.isEmpty || line.startsWith("#"))
  }

  def loadHdfsModel(
                     modelDir: String,
                     fs: FileSystem,
                     wznArray: Array[(Wzn)],
                     nextId:Int
                     )= {

    val path = new Path(modelDir)
    val lists = fs.listStatus(path)

    for (ff <- lists) {
      if (!ff.isDir()) {
        val ppp = ff.getPath()
        val fsin = fs.open(ppp)
        val isr = new InputStreamReader(fsin)
        val br = new BufferedReader(isr)
        var line: String = null
        line = br.readLine()
        while (line != null) {
          val arr = line.trim().split(FtrlUtils.TAB)
          val size = arr.length
          if (size == 4 || size == 2) {
            val branchId = arr(0).toInt
            if(branchId < nextId){    
              val w = arr(1).toFloat
              val z = if (size == 4) arr(2).toDouble else 0d
              val n = if (size == 4) arr(3).toDouble else 0d
  
                wznArray(branchId) = (new Wzn(w, z, n, 0, branchId))
              
            } else {
               throw new RuntimeException("loadHdfsModel: the id in history model exceed the feature limit " + branchId)
            }
          }

          line = br.readLine()
        }

        fsin.close()
        isr.close()
        br.close()
      }
    }

    wznArray
  }

  def sgn(a: Double): Int = {
    if (a > 1e-8) 1 else -1
  }

}
