package com.ijinshan.mllib.ftrl

import com.ijinshan.ctr.Util.Utils
import it.unimi.dsi.fastutil.longs.Long2IntOpenHashMap

import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.{SparkContext, SparkConf}
import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import scala.collection.mutable.ArrayBuffer
import scala.math.log
import com.esotericsoftware.kryo.Kryo
import org.apache.spark.serializer.KryoRegistrator

class MyRegistrator extends KryoRegistrator {
  override def registerClasses(kryo: Kryo) {
      kryo.register(classOf[AddictiveFtrl])
      kryo.register(classOf[FtrlModel])
      kryo.register(classOf[Instance])
      kryo.register(classOf[Wzn])
      kryo.register(classOf[Array[(Int, Wzn)]])
      kryo.register(classOf[HashMap[String, Int]])
      kryo.register(classOf[String])
      kryo.register(classOf[Int])
      kryo.register(classOf[Double])
      kryo.register(classOf[Short])
      kryo.register(classOf[Boolean])
      kryo.register(classOf[org.apache.hadoop.io.LongWritable])
      kryo.register(classOf[org.apache.hadoop.io.Text])
    }
}

/**
  * Created by tongming on 16/9/14.
  */
object DeltaTrain {
  def main(args: Array[String]): Unit = {
    val appName = args(0)
    val modelPath = args(1)
    val dataPath = args(2)
    val testDataPath = args(3)
    val tempPath = args(4)
    val resPath = args(5)
    val numIterations = args(6).toInt
    val alpha = args(7).toFloat
    val beta = args(8).toFloat
    val lambda1 = args(9).toFloat
    val lambda2 = args(10).toFloat
    val minPv = args(11).toInt
    val relMethod = if(args(12) == "rel") true else false 
    val conf = new SparkConf().setAppName(appName)
    val sc = new SparkContext(conf)
    val modelFeatureCate = sc.textFile(modelPath).map(_.split("_")(0)).distinct().collect
    var acceptFeatures = HashSet(modelFeatureCate:_*)
    val acceptFeaturesBroad = sc.broadcast(acceptFeatures)
    val trainData = sc.textFile(dataPath).flatMap(
      _.split("\\t").last.split("\\s")).persist
    val newFeatures = trainData.filter(feature =>
        !acceptFeaturesBroad.value.contains(feature.split("_")(0))).map((_, 1)).reduceByKey(_ + _).filter(_._2 >= minPv).persist
    println("featutres number: " + newFeatures.count.toString)
    val oldFeatures = trainData.filter(feature =>
      acceptFeaturesBroad.value.contains(feature.split("_")(0))).map((_, 1)).reduceByKey((x,y) =>x).map(f => (Utils.hashLong(f._1), 1)).persist
    Utils.rmDir(tempPath+"/mapId")
    val model = FtrlModel.loadModel(modelPath, oldFeatures, sc, tempPath+"/mapId")
    val oldFeatureMapping = sc.textFile(tempPath+"/mapId").map {line =>
      val items = line.split("\\t")
      val key = items(0).toLong
      val index = items(1).toInt
      (key, index)
    }.join(oldFeatures).map(f =>(f._1, f._2._1)).collect()
    val shareFeatureDict = new Long2IntOpenHashMap()
    shareFeatureDict.clear
    oldFeatureMapping.foreach(x => shareFeatureDict.put(x._1, x._2))
    val unselectedFeature = newFeatures.map(_._1.split("_")(0)).distinct.collect()
    val unselectedFeatureSet: HashSet[String] = HashSet(unselectedFeature:_*)
    val newFeatureMap = newFeatures.map(f => Utils.hashLong(f._1)).collect().zipWithIndex
    val maxId = newFeatureMap.length
    val independentFeatureDict = new Long2IntOpenHashMap()
    independentFeatureDict.clear
    newFeatureMap.foreach(x => independentFeatureDict.put(x._1, x._2))
    val lossRet = new ArrayBuffer[String]
    lossRet.append("FeatureName\trelative_gain\ttest_auc\ttest_logloss\ttest_sample_number\ttrain_auc\ttrain_logloss\ttrain_sample_number")
    val trainingInstance = TrainingInstance.getTrainingInstanceFromFile(dataPath, acceptFeatures, shareFeatureDict, independentFeatureDict, sc)
    var trainingInstanceWithMargin = TrainingInstance.updateMargin(model, trainingInstance)
    val testInstance = TrainingInstance.getTrainingInstanceFromFile(testDataPath, acceptFeatures, shareFeatureDict, independentFeatureDict, sc)
    var testInstanceWithMargin = TrainingInstance.updateMargin(model, testInstance)
    val testData = sc.textFile(testDataPath).persist()
    val testDataNum = testData.count
    val testDataNumBroad = sc.broadcast(testDataNum)
    val testFeatureCateSplitInfoRDD = testData.flatMap(
        _.split("\\t").last.split("\\s")).filter(feature =>
        !acceptFeaturesBroad.value.contains(feature.split("_")(0))).map((_, 1.0d)).reduceByKey(_ + _).map{ case(featureName, featureCount) =>
          val featureCateName = featureName.split("_")(0)
          val featureFreq = featureCount/testDataNumBroad.value.toDouble
          val logFeatureFreq = log(featureFreq)
          (featureCateName, (featureFreq, -featureFreq*logFeatureFreq))
        }.reduceByKey((v1, v2) => (v1._1+v2._1, v1._2+v2._2)).map(e => (e._1, if(1-e._2._1>0.000001) e._2._2-(1-e._2._1)*log(1-e._2._1) else e._2._2)).persist
    Utils.rmDir(tempPath+"/splitInfo")
    testFeatureCateSplitInfoRDD.coalesce(1).map(e => e._1 + "\t" + e._2.toString).saveAsTextFile(tempPath+"/splitInfo")
    val testFeatureCateSplitInfo = testFeatureCateSplitInfoRDD.collect.toMap
    testFeatureCateSplitInfoRDD.unpersist()
    testData.unpersist()
    val testPredicationAndLabels = testInstanceWithMargin.map(i => (i.margin.toDouble, i.label.toDouble))
    val metrics = new BinaryClassificationMetrics(testPredicationAndLabels)
    var prevTestAuc = metrics.areaUnderROC
    val testFeatureLen = unselectedFeature.size-1
    for (i <- 0 until testFeatureLen) {
      Utils.rmDir(tempPath+"/data" + i.toString)
      Utils.rmDir(tempPath+"/smart" + i.toString)
      Utils.rmDir(tempPath+"/middleres" + i.toString)
      TrainingInstance.saveInstance(trainingInstanceWithMargin, tempPath+"/data" + i.toString)
      val addictiveFtrl = new AddictiveFtrl(numIterations, alpha, beta, lambda1, lambda2, maxId, tempPath+"/smart"+i.toString)
      val (selectedFeatureName, testLoss, relativeAucGain, trainLoss) = addictiveFtrl.train(tempPath+"/data" + i.toString+"/part*", sc, tempPath+"/middleres" + i.toString, unselectedFeatureSet, testInstanceWithMargin, prevTestAuc, testFeatureCateSplitInfo, relMethod)
      prevTestAuc = testLoss._1
      lossRet.append(selectedFeatureName + "\t" + relativeAucGain.toString + "\t" + testLoss._1.toString + "\t" +testLoss._2.toString + "\t"+testLoss._3.toString + "\t" + trainLoss._1.toString+"\t"+trainLoss._2.toString+"\t"+trainLoss._3.toString)
      Utils.rmDir(resPath+"/res"+i.toString)
      sc.parallelize(lossRet.toSeq, 1).saveAsTextFile(resPath+"/res"+i.toString)
      acceptFeatures += selectedFeatureName
      unselectedFeatureSet -= selectedFeatureName
      trainingInstanceWithMargin = addictiveFtrl.updateMargin(trainingInstanceWithMargin, selectedFeatureName, sc)
      trainingInstanceWithMargin = TrainingInstance.excludeFeatures(trainingInstanceWithMargin, selectedFeatureName)
      testInstanceWithMargin = addictiveFtrl.updateMargin(testInstanceWithMargin, selectedFeatureName, sc)
      testInstanceWithMargin = TrainingInstance.excludeFeatures(testInstanceWithMargin, selectedFeatureName)
    }
    Utils.rmDir(resPath+"/full")
    sc.parallelize(lossRet.toSeq, 1).saveAsTextFile(resPath+"/full")

  }

}
