/**
  * Created by tongming on 16/9/13.
  */
package com.ijinshan.mllib.ftrl

import com.ijinshan.ctr.Util.Utils

import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext
import scala.collection.mutable.HashSet

import it.unimi.dsi.fastutil.longs.Long2IntOpenHashMap

class Instance(val label: Short,
               val weight:Float,
               var margin:Float,
               var shareIndex: Array[Int],
               var independentIndex:Array[(String, Array[Int])]) extends Serializable {
  def setMargin(margin: Float): Instance = {
    this.margin = margin
    this
  }

  def removeFeature(excludeFeature: String): Instance = {
    this.independentIndex = independentIndex.filter(_._1 != excludeFeature)
    this
  }

  override def toString: String = {
    val rep = new StringBuilder
    rep.append(label)
    rep.append("\1")
    rep.append(weight.toString)
    rep.append("\1")
    rep.append(margin.toString)
    rep.append("\1")
    rep.append(independentIndex.map(e => e._1 + "\2" + e._2.mkString("\3")).mkString("\4"))
    rep.toString
  }
}

object Instance extends Serializable {
  def loadFromString(line: String): Instance = {
    val items = line.split("\1")
    val label = items(0).toShort
    val weight = items(1).toFloat
    val margin = items(2).toFloat
    val shareIndex = items(3).split("\4").map { e =>
      val es = e.split("\2")
      (es(0), es(1).split("\3").map(_.toInt))
    }
    new Instance(label, weight, margin, null, shareIndex)
  }
}


object TrainingInstance extends Serializable {
  def getTrainingInstanceFromFile(path: String,
                                  acceptFeatures: HashSet[String],
                                  shareFeatureDict: Long2IntOpenHashMap,
                                  independentFeatureDict: Long2IntOpenHashMap,
                                  sc: SparkContext): RDD[Instance] = {
    val featureSetBroad = sc.broadcast(acceptFeatures)
    val featureDictBroad = sc.broadcast(shareFeatureDict)
    val independentFeatureDictBroad = sc.broadcast(independentFeatureDict)
    sc.textFile(path, 200).map{ line =>
      val items = line.split("\\t")
      if (items.size == 4) {
        val aid = items(0)
        val cid = items(1)
        val label = items(2).split(":")(0).toShort
        val weight = items(2).split(":")(1).toFloat
        val features = items(3).split("\\s")
        val shareFeatures = features.filter(feature => featureSetBroad.value.contains(feature.split("_")(0)))
        val shareIndex = shareFeatures.map(Utils.hashLong(_)).filter(featureDictBroad.value.containsKey(_)).map(featureDictBroad.value.get(_))
        val independentFeatures = features.filter(feature => !featureSetBroad.value.contains(feature.split("_")(0)))
        val independentIndex = independentFeatures.map(feature =>
          (feature, Utils.hashLong(feature))).filter(feature =>
          independentFeatureDictBroad.value.containsKey(feature._2)).map(feature =>
          (feature._1.split("_")(0), independentFeatureDictBroad.value.get(feature._2))).groupBy(
          _._1).toArray.map(e => (e._1, e._2.unzip._2.toArray))

        new Instance(label, weight, 0, shareIndex, independentIndex)
      } else {
        throw new RuntimeException("the model columns donesn't match expection\n line is :  " + line)
        null
      }
    }
  }
  def updateMargin(model: FtrlModel, data: RDD[Instance]): RDD[Instance] = {
    val modelBroad = data.context.broadcast(model)
    data.map(i => i.setMargin(modelBroad.value.predictMargin(i)))
  }

  def excludeFeatures(data:RDD[Instance], excludeFeature: String):RDD[Instance] = {
    data.map(_.removeFeature(excludeFeature))
  }

  def saveInstance(data: RDD[Instance], path: String) = {
    data.filter(_.independentIndex.size>0).map(_.toString).saveAsTextFile(path)
  }
}
