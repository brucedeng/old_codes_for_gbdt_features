package com.cmcm.cmnews.model.input

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters

/**
 * Created by tangdong on 3/5/16.
 */
trait XFBBinaryTrainDataInput extends EventBatchInput{
  this: SparkBatchContext =>
  import Parameters._

  override def getInput(): Unit ={
    logInfo("start XFBTrainDataInput...")
    //setRDD(constant_event_input,constant_event_rdd, this)
    setRDD(constant_event_pv_input,constant_event_pv_rdd, this)
    setRDD(constant_event_pv_click_input,constant_event_pv_click_rdd, this)
    setRDD(constant_event_click_input,constant_event_click_rdd, this)
    setRDD(constant_event_readtime_input,constant_event_readtime_rdd, this)
    setRDD(constant_content_input, constant_content_rdd, this)
    setRDD(constant_user_input, constant_user_rdd, this)
    //setRDD(constant_xfb_input,constant_xfb_rdd, this)
  }

}
