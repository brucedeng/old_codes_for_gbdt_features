package com.cmcm.cmnews.model.feature

import com.cmcm.cmnews.model.util.{LoggingUtils, JsonUtil, Parameters}
import org.apache.spark.Logging
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.joda.time.DateTime

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.Random
//import scala.util.control.Breaks._

/**
 * Created by tangdong on 3/5/16.
 */
object CFBFeature extends Logging {

  import Parameters._

  def aggSingleCfb (cfb:String, prevTs:Long, decayWnd:Int, decayFactor:Double) = {
    val cfbArr = cfb.split(recordDelimiter).map(oneRecord => {
      val line = oneRecord.split(fieldDelimiter)
      val ts = line(0).toLong / decayWnd * decayWnd
      val clk = line(1).toDouble
      val pv = line(2).toDouble
      (ts,clk,pv)
    }).sortBy(x => x._1)

    var decayedPv:Double = 0
    var decayedClk:Double = 0
    var decayedTs:Long = 0
    var idx = 0
    while(idx < cfbArr.length && cfbArr(idx)._1 < prevTs) {
      val (ts,clk,pv) = cfbArr(idx)
      if(decayedTs == 0){
        decayedTs = ts
        decayedPv = pv
        decayedClk = clk
      }else {
        val batch = (ts - decayedTs)/decayWnd
        val decayRate = math.pow(decayFactor, batch)
        decayedTs = ts
        decayedClk = clk + decayedClk * decayRate
        decayedPv = pv + decayedPv * decayRate
      }
      idx += 1
    }
    ( (decayedTs + fieldDelimiter + decayedClk + fieldDelimiter + decayedPv) +:
      cfbArr.slice(idx,cfbArr.length).map(line => {
        line._1 + fieldDelimiter + line._2 + fieldDelimiter + line._3
      })
      )
      .mkString(recordDelimiter)
  }

  def preAggCfb (xFBFeatureRdd: RDD[(String, String)], prevTs: Long,  decayWnd:Int, decayFactor:Double ) = {
    xFBFeatureRdd.map(line => (line._1, aggSingleCfb(line._2,prevTs,decayWnd,decayFactor)))
  }

  def joinWithBatchEvent(XFBFeatureRdd: RDD[(String, String)], eventRdd: RDD[(String, String)], batchContext:collection.mutable.Map[String,String]) = {
    logInfo("Now, join xfb feature and event")
    val windSize = batchContext(constant_wnd)
    val prevDateTime = batchContext(constant_prev_date_time)
    val prevTimeStamp = new DateTime(prevDateTime).getMillis/1000 - windSize.toLong
    val posDateTime = batchContext(constant_date_time)
    val posTimeStamp = new DateTime(posDateTime).getMillis/1000 - windSize.toLong
    val decayWind = batchContext(constant_decay_wind)
    val num = decayWind.slice(0, decayWind.size - 1).toInt
    val period = decayWind.last match {
      case x:Char if x.toString == "h" => 6
      case x:Char if x.toString == "d" => 144
      case _ => 0
    }
    val interval = num * period * windSize.toInt - windSize.toInt
    val topNum = batchContext(constant_top_num).toInt
    println(s"topNum is:  $topNum")
//    val randomNum = batchContext(constant_random_num).toInt
    val configMap = Map[String, String](
      constant_date_time -> posTimeStamp.toString,
      constant_prev_date_time -> prevTimeStamp.toString,
      constant_interval -> interval.toString,
      constant_wnd -> windSize,
      constant_decay_factor -> batchContext(constant_decay_factor))
    println(s"configMap is $configMap")
    val parallel = batchContext(constant_parallelism).toInt

    val eventRddPer = eventRdd //.persist(StorageLevel.MEMORY_AND_DISK_SER)
//    eventRddPer.saveAsTextFile("/tmp/news_model/eventRddPer")

    val skewedFeatureList = eventRddPer.sample(false,0.05).map(line=> (line._1,1)).reduceByKey(_+_)
      .sortBy(line => line._2,false,parallel)
      .take(topNum + 1)
      .toList
    println(s"skewedFeatureList is $skewedFeatureList")
    val randomNum = skewedFeatureList.last

    val topNumberTmp=skewedFeatureList.head._2 / randomNum._2.toFloat
    val topNumber = if(topNumberTmp < 2) 2.0 else topNumberTmp
    val skewedFeature = skewedFeatureList.take(topNum).map(pair => (pair._1,(pair._2/topNumber).toInt+1)).toMap
    println(s"skewedFeatureList is $skewedFeature")

    //split eventRdd into two rdds ,one with normal feature keys, the other with skewed feature keys
    val normalEventRdd = eventRddPer.filter(line => {
      if(skewedFeature.contains(line._1)){
        false
      }else{
        true
      }
    })
//    normalEventRdd.saveAsTextFile("/tmp/news_model/normalEventRdd")
    val skewedEventRdd = eventRddPer.filter(line => {
      if(skewedFeature.contains(line._1)){
        true
      }else{
        false
      }
    }).map(line => {
      val randomObject = new Random()
      val key = randomObject.nextInt(skewedFeature(line._1)) + fieldDelimiter + line._1
      (key, line._2)
    })
//    skewedEventRdd.map(line => line._1 + "\t" + line._2).saveAsTextFile("/tmp/news_model/skewedEventRdd")

//    eventRddPer.unpersist()

    val decayedXFBFeatureRdd = preAggCfb(XFBFeatureRdd,prevTimeStamp,windSize.toInt,batchContext(constant_decay_factor).toDouble).persist(StorageLevel.DISK_ONLY)
    //split XFBfeatureRdd into two rdds
    val normalFeatureRdd = decayedXFBFeatureRdd.filter(line => {
      if(skewedFeature.contains(line._1)){
        false
      }else{
        true
      }
    })
//    normalFeatureRdd.saveAsTextFile("/tmp/news_model/normalFeatureRdd")
    val skewedFeatureRdd = decayedXFBFeatureRdd.filter(line => {
      if(skewedFeature.contains(line._1)){
        true
      }else{
        false
      }
    }).flatMap(line=> {
      for(index <- 0 to skewedFeature(line._1)) yield{
        (index + fieldDelimiter + line._1, line._2)
      }
    })
//    decayedXFBFeatureRdd.unpersist()

//    val initialSet = collection.mutable.HashSet.empty[String]
//    val addToSet = (s: List[String], v: String) => v::s
//    val mergePartitionSets = (p1: collection.mutable.HashSet[String], p2: collection.mutable.HashSet[String]) => p1 ++= p2
    val normalJoinFeatureRdd = normalEventRdd //.aggregateByKey(initialSet)(addToSet, mergePartitionSets)
      .groupByKey()
      .leftOuterJoin(normalFeatureRdd,parallel).flatMap(result => {
      flattenEvent(result,configMap,false)
    }) //.aggregateByKey(initialSet)(addToSet,mergePartitionSets)

    val screwJoinFeatureRdd = skewedEventRdd //.aggregateByKey(initialSet)(addToSet, mergePartitionSets)
      .groupByKey()
      .leftOuterJoin(skewedFeatureRdd,parallel).flatMap(result => {
      flattenEvent(result,configMap,true)
    }) //.aggregateByKey(initialSet)(addToSet, mergePartitionSets)

    (normalJoinFeatureRdd.union(screwJoinFeatureRdd))
      .groupByKey(parallel)
      .map((line: (String, Iterable[String])) => {
        val key = line._1
        val records = line._2
        val xfb_out_dict = collection.mutable.Map[String, AnyRef]()
        records.map((record: String) => {
          val items = record.split(fieldDelimiter)
          val featureWeight = items(0)
          val featureType = if (items.size > 1) items(1) else ""
          val featureName = if (items.size > 2) items(2) else ""
          val click = if (items.size > 3) items(3) else ""
          val view = if (items.size > 4) items(4) else ""
          if (featureType.contains("KEYWORD") || featureType.contains("CATEGORY")) {
            if (xfb_out_dict.contains(featureType)) {
              val featureDict = xfb_out_dict(featureType).asInstanceOf[Map[String, List[String]]]
              xfb_out_dict.put(featureType, featureDict + (featureName -> List(click, view, featureWeight)))
            } else {
              xfb_out_dict.put(featureType, Map(featureName -> List(click, view, featureWeight)))
            }
          } else {
            xfb_out_dict.put(featureType, List(click, view, featureWeight))
          }
        })
        (key, JsonUtil.toJson(xfb_out_dict))
      })
  }

  def flattenEvent(record:(String,(Iterable[String],Option[String])), config:Map[String,String], skewed:Boolean = false):List[(String, String)] = {
    val interval = config(constant_interval).toInt
    val decayFactor = config(constant_decay_factor).toDouble
    val beginTimeStamp = config(constant_prev_date_time).toLong
    val windSize = config(constant_wnd).toInt
    val endTimeStamp = config(constant_date_time).toLong
    val keyItems = record._1.split(fieldDelimiter)
    val featureType = if(skewed) keyItems(2) else keyItems(1)
    val featureName = if(skewed) keyItems(3) else keyItems(2)

    val eventArr = record._2._1.toArray.map(line => {
      val items = line.split(fieldDelimiter)
      val ts = items(0).toLong
      val weight = items(1)
      val prefix = items.slice(2, 4).mkString(fieldDelimiter)
      (prefix,ts,weight)
    }).sortBy(line => line._2)

//    val eventSet = record._2._1
//    val defaultFeature = featureType + fieldDelimiter + featureName + fieldDelimiter + "" + fieldDelimiter + ""
//    val eventOut = new ListBuffer[(String, String)]
    record._2._2 match {
      case Some(feature) if !feature.isEmpty =>
        val recordDict = collection.mutable.Map[String, (Double, Double)]()
        val records = feature.split(recordDelimiter)
        val cfbRec = records.map(line => {
          val items = line.split(fieldDelimiter)
          val ts = items(0).toLong/windSize*windSize
          val click = items(1).toDouble
          val view = items(2).toDouble
          (ts,click,view)
//          recordDict += (ts ->(click, view))
        })

        var fbIdx = 0
        var decayedPv:Double = 0
        var decayedClk:Double = 0
        var decayedTs:Long = 0
        eventArr.map(line => {
          val (prefix,ts,weight) = line
          while(ts > decayedTs && fbIdx < cfbRec.length && ts > cfbRec(fbIdx)._1) {
            val (rawTs,rawClk,rawPv) = cfbRec(fbIdx)
//            if(ts <= rawTs ) break
            if(decayedTs == 0){
              decayedTs = rawTs
              decayedClk = rawClk
              decayedPv = rawPv
            }else{
              if((rawTs-decayedTs) > interval){
                decayedTs = rawTs
                decayedClk = rawClk
                decayedPv = rawPv
              }
              else {
                val batch = (rawTs - decayedTs) / windSize
                val decayRate = math.pow(decayFactor, batch)
                decayedTs = rawTs
                decayedClk = rawClk + decayedClk * decayRate
                decayedPv = rawPv + decayedPv * decayRate
              }
            }
            fbIdx += 1
          }
          if(decayedTs > 0 && (ts - decayedTs) <= interval) {
            val batch = (ts - decayedTs) / windSize
            val decayRate = math.pow(decayFactor, batch)
            val curClk = decayedClk * decayRate
            val curPv = decayedPv * decayRate
            val aggXfbFeatures = featureType + fieldDelimiter + featureName + fieldDelimiter + curClk + fieldDelimiter + curPv
            (prefix, weight + fieldDelimiter + aggXfbFeatures)
          }else{
            (prefix, "")
          }
        }).filter(v => !v._2.isEmpty ).toList

      case Some(feature) if feature.isEmpty =>
        List[(String, String)]()
      case None =>
        List[(String, String)]()
    }
//    eventOut.toList
  }

  def joinWithStaticFeature(XFBFeatureRdd: RDD[(String, String)], eventRdd: RDD[(String, String)]) = {
    eventRdd.leftOuterJoin(XFBFeatureRdd).map(result => {
      val staticFeatures = result._2._1 //.split(fieldDelimiter).drop(0).mkString(fieldDelimiter)
      val xfbFeatures = result._2._2.getOrElse("")
      (staticFeatures,xfbFeatures)
    })
  }
}
