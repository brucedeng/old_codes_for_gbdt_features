package com.cmcm.cmnews.model.feature

import com.cmcm.cmnews.model.config.Config
import com.cmcm.cmnews.model.util.{JsonUtil, LoggingUtils, Parameters}
import org.apache.spark.Logging
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.joda.time.{DateTime, DateTimeZone}

import scala.collection.mutable.ListBuffer
import scala.collection.mutable
import scala.util.Try

/**
 * Created by tangdong on 1/5/16.
 */
object XFBFeatureKey extends Logging{
  import Parameters._

  def generateFullXFBFeatures(fullFeature: RDD[(String, String)], config:collection.immutable.Map[String,String]) : (RDD[(String, String)],RDD[(String,String)]) = {
    val eventWithFeatures = fullFeature.map(feature => genStaticFeature(feature,config)) //.persist(StorageLevel.DISK_ONLY)
    //eventWithFeatures.persist(StorageLevel.MEMORY_AND_DISK_SER)
    val parallism = config(constant_parallelism).toInt

    val typesafeConfig = new Config(config(constant_config_file)).typesafeConfig
//    eventWithFeatures.saveAsTextFile("/tmp/news_model/eventWithFeatures")
    val staticFeatures = eventWithFeatures.map(line => {
      val prefix = line._1
      val uniqId = line._2._1
      val pid = line._2._3
      (uniqId + fieldDelimiter + pid, prefix)
    })
    val XFBFeatures = eventWithFeatures.flatMap(line => {
      val uniqId = line._2._1
      val ts = line._2._2
      val pid = line._2._3
      val lanRegion = line._2._5
      val featureMap = line._2._4
      val feature = (uniqId + fieldDelimiter + ts + fieldDelimiter + pid + fieldDelimiter + lanRegion, featureMap) //(r.nextInt(parallism*5),
      generateXFBFeatures(feature,config,typesafeConfig) //.map(line => (r.nextInt(parallism*5),line))
    })
//    eventWithFeatures.unpersist()
    (staticFeatures,XFBFeatures)
  }

  def getMatchXFBFeatures(cfbFeatures: ListBuffer[(String,String)], featureConfig: String, featureMap: mutable.HashMap[String, List[(String,Double)]], tuple: (String,String, String), prefix:String) = {
    val ts = tuple._1
    val pid = tuple._2
    val uniqId = tuple._3
    for (feature <- featureConfig.split(",")) {
      val fields = feature.split("_")
      val preFeature = fields(1) + "_" + fields(3)
      val posFeature = fields(2) + "_" + fields(4)
      for (value1 <- featureMap.getOrElse(preFeature,List())) {
        for (value2 <- featureMap.getOrElse(posFeature,List())) {
          if (value1._1.equals(value2._1)) {
            cfbFeatures += ((pid + fieldDelimiter + prefix + feature + fieldDelimiter + value1._1 + pairDelimiter + value2._1, ts + fieldDelimiter + value1._2 * value2._2 + fieldDelimiter + uniqId + fieldDelimiter + pid))
          }
        }
      }
    }
  }

  def getFeatureValueRecursive(featureMap:mutable.HashMap[String, List[(String, Double)]], featureList: List[String]) : List[(String, Double)] = {
    var featureValues = new ListBuffer[(String, Double)]
    if (featureList.size > 1) {
      val preFeatures = featureMap.getOrElse(featureList.head,List())
      val newFeatureList = featureList.drop(1)
      for (preFeature <- preFeatures) {
        for (posFeature <- getFeatureValueRecursive(featureMap, newFeatureList)) {
          featureValues += ((preFeature._1 + pairDelimiter + posFeature._1, preFeature._2 * posFeature._2))
        }
      }
    } else {
      for (preFeature <- featureMap.getOrElse(featureList.head,List())){
        featureValues += preFeature
      }
    }
    featureValues.toList
  }

  def getCrossXFBFeatures(cfbFeatures: ListBuffer[(String, String)], featureConfig: String, featureMap: mutable.HashMap[String, List[(String,Double)]], tuple: (String,String,String),prefix:String) = {
    val ts = tuple._1
    val pid = tuple._2
    val uniqId = tuple._3
    for(feature <- featureConfig.split(",")){
      var featureList = new ListBuffer[String]
      val fields = feature.split("_")
      var i = 1
      while(i <= (fields.length/2)){
        featureList += (fields(i) + "_" + fields(i+2))
        i = i + 1
      }
      for (value <- getFeatureValueRecursive(featureMap, featureList.toList)) {
        cfbFeatures += ((pid + fieldDelimiter + prefix + feature + fieldDelimiter + value._1, ts + fieldDelimiter + value._2 + fieldDelimiter + uniqId + fieldDelimiter + pid))
      }
    }
  }
  def getSingleEdgeXFBFeatures(cfbFeatures: ListBuffer[(String, String)], featureConfig: String, featureMap: mutable.HashMap[String, List[(String,Double)]], tuple: (String,String,String),prefix:String) = {
    val ts = tuple._1
    val pid = tuple._2
    val uniqId = tuple._3
    for (feature <- featureConfig.split(",")){
      val values = featureMap.getOrElse(feature,List())
      for (value <- values) {
        cfbFeatures += ((pid + fieldDelimiter + prefix + feature + fieldDelimiter + value._1 , ts + fieldDelimiter + value._2 + fieldDelimiter + uniqId + fieldDelimiter + pid))
      }
    }
  }

  def generateXFBFeatures(feature:(String,collection.mutable.HashMap[String, List[(String,Double)]]), config:collection.immutable.Map[String,String],typesafeConfig:com.typesafe.config.Config) = {
    val items = feature._1.split(fieldDelimiter)
    val uniqId = items(0)
    val joinTs = items(1)
    val pid = items(2)
    val lanRegion = items(3)
    val featureMap = feature._2
    val cfbFeatures = new ListBuffer[(String, String)]

    val multiLan = if( config(constant_multi_lan) == "true" ||  config(constant_multi_lan)=="yes" ) true
    else false

//    val userFeatureConfig = typesafeConfig.getString("cfb.uf")
//    val docFeatureConfig = typesafeConfig.getString("cfb.df")
//    val crossFeatureConfig = typesafeConfig.getString("cfb.cf")
//    val matchFeatureConfig = typesafeConfig.getString("cfb.mf")

    val coec_count = typesafeConfig.getInt("cfb.multi_coec")
    for (idx <- 1 to coec_count) {
      val curConf = typesafeConfig.getConfig("cfb.coec-" + idx)
      val userFeatureConfig = curConf.getString("uf")
      val docFeatureConfig = curConf.getString("df")
      val crossFeatureConfig = curConf.getString("cf")
      val matchFeatureConfig = curConf.getString("mf")
      val clickTp = curConf.getString("c")
      val viewTp = curConf.getString("ec")
      val prefix =
        if(clickTp=="CL" && viewTp=="PV") ""
        else if(viewTp=="NAN") s"${clickTp}_"
        else s"${clickTp}_${viewTp}_"
      if (userFeatureConfig.size > 0) getSingleEdgeXFBFeatures(cfbFeatures, userFeatureConfig, featureMap, (joinTs, pid, uniqId),prefix)
      if (docFeatureConfig.size > 0) getSingleEdgeXFBFeatures(cfbFeatures, docFeatureConfig, featureMap, (joinTs, pid, uniqId),prefix)
      if (crossFeatureConfig.size > 0) getCrossXFBFeatures(cfbFeatures, crossFeatureConfig, featureMap, (joinTs, pid, uniqId),prefix)
      if (matchFeatureConfig.size > 0) getMatchXFBFeatures(cfbFeatures, matchFeatureConfig, featureMap, (joinTs, pid, uniqId),prefix)
    }
    if(multiLan) cfbFeatures.map(x => (x._1+pairDelimiter+lanRegion, x._2))
    else cfbFeatures
  }

  def genStaticFeature(feature:(String,String), config:collection.immutable.Map[String,String]) = {
    val featureMap = new mutable.HashMap[String, List[(String,Double)]]
    val dCid = feature._1
    val items = feature._2.split(fieldDelimiter)
    val uid = items(0)
    val uniqueId= items(1)
    val ts = items(2).toLong
    val joinTs = items(3)
    val pid = items(4)
    val uCity = items(5)
    val dwell = items(6)
    val label = items(7)
    val reqid = items(8)
    val lanRegion = items(9)
    val uRelCat = items(8+2)
    val uRelKey = items(9+2)
    val uGender = items(10+2)
    val uAge = items(11+2)
    val uCatLen = items(12+2)
    val uKeyLen = items(13+2)
    val dGroupid = items(14+2)
    val dTier = items(15+2)
    val dPublisher = items(16+2)
    val dUpdateTime = items(17+2).toLong
    val dPublishTime = items(18+2).toLong
    val dRelCat = items(19+2)
    val dRelKey = items(20+2)
    val wordCount = items(21+2)
    val imageCount = items(22+2)
    val newsyScore = items(23+2)
    val dTitlemd5 = items(26)

    val catRel = genRelevance(uRelCat,dRelCat)
    val keyRel = genRelevance(uRelKey,dRelKey)

    val uCat = top(uRelCat,Try(config(constant_u_ncat)).getOrElse("5").toInt)
    val uKey = top(uRelKey,Try(config(constant_u_nkey)).getOrElse("25").toInt)
    val dCat = top(dRelCat,Try(config(constant_d_ncat)).getOrElse("5").toInt)
    val dKey = top(dRelKey,Try(config(constant_d_nkey)).getOrElse("15").toInt)

    val docAge = (ts - dPublishTime)/3600 match {
      case x:Long if x >= 0 => x
      case _ => 0
    }
    val timeTuple = if (ts <= 0) {
      (-1L,-1L)
    } else{
      val dateD = new DateTime(ts * 1000L).withZone(DateTimeZone.UTC)
      (dateD.getHourOfDay, dateD.getDayOfWeek%7)
    }
    val timeOfDay = timeTuple._1
    val dayOfWeek = timeTuple._2

    // TODO: add reqid to prefix
    val staticFeature = Map[String,String](
      "U_CAT_LEN" -> uCatLen,
      "U_KW_LEN" -> uKeyLen,
      "CAT_REL" -> catRel.toString,
      "KW_REL" -> keyRel.toString,
      "WORD_COUNT" -> wordCount,
      "IMAGE_COUNT" -> imageCount,
      "NEWSY_SCORE" -> newsyScore,
      "DOC_AGE" -> docAge.toString,
      "TIME_OF_DAY" -> timeOfDay.toString,
      "DAY_OF_WEEK" -> dayOfWeek.toString
    )
    val req_data = reqid.split(":",2)
    val newReqId = req_data(0)
    val eventsId = if(req_data.length>1) req_data(1) else ""
    //uCatLen,uKeyLen,catRel,keyRel,wordCount,imageCount,newsyScore, docAge,timeOfDay,dayOfWeek
    val prefix = List(uniqueId,uid,dCid,newReqId,joinTs,dwell,pid,lanRegion,label,uCity,eventsId,JsonUtil.toJson(staticFeature)).mkString(fieldDelimiter)
    if (uid != "" && uid != "unknown" && uid.size > 0 ) {
      featureMap.put("U_UID", List((uid,1.0)))
    }
    if (uCity != "" && uCity != "unknown" && uCity.size > 0) {
      featureMap.put("U_CITY", List((uCity,1.0)))
    }
    if (uGender != "" && uGender != "unknown" && uGender.size > 0) {
      featureMap.put("U_GENDER", List((uGender,1.0)))
    }
    if (uAge != "" && uAge != "unknown" && uAge.size > 0) {
      featureMap.put("U_AGE", List((uAge,1.0)))
    }
    if (uCat != "" && uCat != "unknown" && uCat.size > 0) {
      featureMap.put("U_CATEGORY", uCat.split(pairDelimiter).map(record => {
        val nameAndWeight = record.split(keyValueDelimiter)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }
    if (uKey != "" && uKey != "unknown" && uKey.size > 0) {
      featureMap.put("U_KEYWORD", uKey.split(pairDelimiter).map(record => {
        val nameAndWeight = record.split(keyValueDelimiter)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }

    if (dGroupid != "" && dGroupid != "unknown" && dGroupid.size > 0) {
      featureMap.put("D_GROUPID", List((dGroupid,1.0)))
    }
    if (dTier != "" && dTier != "unknown" && dTier.size > 0) {
      featureMap.put("D_TIER", List((dTier,1.0)))
    }
    if (dTitlemd5 != "" && dTitlemd5 != "unknown" && dTitlemd5.size > 0) {
      featureMap.put("D_TITLEMD5", List((dTitlemd5,1.0)))
    }
    if (dPublisher != "" && dPublisher != "unknown" && dPublisher.size > 0) {
      featureMap.put("D_PUBLISHER", List((dPublisher,1.0)))
    }
    if (dCat != "" && dCat != "unknown" && dCat.size > 0) {
      featureMap.put("D_CATEGORY", dCat.split(pairDelimiter).map(record => {
        val nameAndWeight = record.split(keyValueDelimiter)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0),nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }
    if (dKey != "" && dKey != "unknown" && dKey.size > 0) {
      featureMap.put("D_KEYWORD", dKey.split(pairDelimiter).map(record => {
        val nameAndWeight = record.split(keyValueDelimiter)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }
    if (dCid != "" && dCid != "unknown" && dCid.size > 0) {
      featureMap.put("D_CONID", List((dCid,1.0)))
    } else {
      featureMap.put("D_CONID", List())
    }

    featureMap.put("LAN_REGION",List((lanRegion,1.0)))

    (prefix, (uniqueId,joinTs, pid, featureMap, lanRegion))
  }

  def genRelevance(u_input:String,d_input:String):Double = {
    if (u_input == "" || u_input == "unknown" || d_input == "" || d_input == "unknown") {
      0.0
    } else {
      val u_input_dict = u_input.split(pairDelimiter).map((line:String) => {
        val items = line.split(keyValueDelimiter)
        val name = items(0)
        val value = items(1).toDouble
        (name, value)
      }).toMap

      val rel = d_input.split(pairDelimiter).map((line:String) => {
        val items = line.split(keyValueDelimiter)
        val name = items(0)
        val value = items(1).toDouble
        Try(u_input_dict(name)).getOrElse(0.0) * value
      }).sum
      if(rel.isNaN || rel.isInfinity) 0.0
      else rel
    }
  }

  def top(inputStr:String,topNum:Int): String = {
    if (inputStr == "" || inputStr == "unknown") {
      inputStr
    } else {
      inputStr.split(pairDelimiter).slice(0,topNum).mkString(pairDelimiter)
    }
  }
}
