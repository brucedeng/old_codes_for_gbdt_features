package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.feature.{BaseInfo, Instance}
import com.cmcm.cmnews.model.spark.SparkStreamingContext
import com.cmcm.cmnews.model.util.Parameters
import com.cmcm.cmnews.model.util.Util._

import org.json4s._
import org.json4s.jackson.JsonMethods._
import scala.collection.immutable.List
import scala.collection.mutable
import org.apache.spark.streaming.dstream.DStream

/**
  * Created by lilonghua on 16/8/4.
  */
object LRBStreamingProcessor extends ProcessorStreamingAbstract[Instance]{
  this:SparkStreamingContext =>
  import Parameters._

  def disExtract(dataLine: String): List[(String, (Int, Double))] = {
    val json = parse(dataLine)
    json match {
      case JObject(jarray) =>
        jarray.map( { case (str: String, obj: JValue) => (str.replaceAll("\n"," ").replaceAll(" ","_").trim, (1, 1.0)) } )
      case other => {
        //log.warn(s"dataLine parse error, does not match (k, v), $dataLine")
        List.empty[(String, (Int, Double))]
      }
    }
  }



  override def preprocess(line: String, batchContext: mutable.Map[String, String]): Instance = {
    val terms   = line.split("\\t")
    val uid     = terms(1)
    val dCid    = terms(2)
    val dwell   = terms(4)
    val label   = terms(8)
    // val cfbJson = terms.last  // toBe parse
    // val discreF = terms.dropRight(1).last
    val cfbJson = "{}"
    val discreF = terms.last

    val uCatLen    = terms(10)
    val uKeyLen    = terms(11)
    val catRel     = terms(12)
    val keyRel     = terms(13)
    val wordCount  = terms(14)
    val imageCount = terms(15)
    val newsyScore = terms(16)
    val docAge     = terms(17)
    val timeOfDay  = terms(18)
    val dayOfWeek  = terms(19)

    lazy val staticFeature: List[(String, (Int, Double))] = List(
      ("U_CAT_LEN",   (1, unsafe(0.0) { uCatLen.toDouble })),
      ("U_KW_LEN",    (1, unsafe(0.0) { uKeyLen.toDouble })),
      ("CAT_REL",     (1, unsafe(0.0) { catRel.toDouble })),
      ("KW_REL",      (1, unsafe(0.0) { keyRel.toDouble })),
      ("WORD_COUNT",  (1, unsafe(0.0) { wordCount.toDouble })),
      ("IMAGE_COUNT", (1, unsafe(0.0) { imageCount.toDouble })),
      ("NEWSY_SCORE", (1, unsafe(0.0) { newsyScore.toDouble })),
      ("DOC_AGE",     (1, unsafe(0.0) { docAge.toDouble })),
      ("TIME_OF_DAY", (1, unsafe(0.0) { timeOfDay.toDouble })),
      ("DAY_OF_WEEK", (1, unsafe(0.0) { dayOfWeek.toDouble }))
    )
    Instance(BaseInfo(uid, dCid, label, dwell),
      staticFeature, List.empty[(String, (Int, Double))], disExtract(discreF))
  }

  override def process(inputRDD:DStream[String], batchContext: mutable.Map[String, String]) : DStream[Instance] = {
    inputRDD.map((line:String) => preprocess(line, batchContext))
  }
}
