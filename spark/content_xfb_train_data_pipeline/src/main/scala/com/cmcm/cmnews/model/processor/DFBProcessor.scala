package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.util.Parameters
import com.cmcm.cmnews.model.util.Parameters._
import org.apache.spark.rdd.RDD

/**
 * Created by tangdong on 9/5/16.
 */
object DFBProcessor extends Processor{

  import Parameters._

  override def process(inputRDD: RDD[String], batchContext: collection.mutable.Map[String, String]): RDD[(String, String)] = {
//    val pids = batchContext(constant_pid).split(",")
    val filterRDD:RDD[String] = inputRDD //.filter(line => {
//      val items = line.split("\t")
//      val pid = items(0)
//      if(pids.contains(pid) && items.size >= 6){
//        true
//      }else{
//        false
//      }
//    })

    super.process(filterRDD, batchContext).reduceByKey((pre,pos) => pre + recordDelimiter + pos)

    //    val initialSet = collection.mutable.HashSet.empty[String]
    //    val addToSet = (s: collection.mutable.HashSet[String], v: String) => s += v
    //    val mergePartitionSets = (p1: collection.mutable.HashSet[String], p2: collection.mutable.HashSet[String]) => p1 ++= p2
    //
    //    afterProcessRDD.aggregateByKey(initialSet)(addToSet, mergePartitionSets).flatMap(line => {
    //      genAggCfbFeatures(line, configMap)
    //    })
    //    afterProcessRDD.reduceByKey((pre,pos) => pre + recordDelimiter + pos)
  }

  override def preprocess(line: String, batchContext: collection.mutable.Map[String, String]) = {
    val items = line.split("\t")
    val pid = items(0)
    val ts = items(1)
    val featureType = items(2)
    val featureName = items(3)
    val click = items(4)
    val view = items(5)
    val dwell = items(6)

    (pid + fieldDelimiter + featureType + fieldDelimiter + featureName, ts + fieldDelimiter + click + fieldDelimiter + view + fieldDelimiter + dwell)
  }


  //  def genAggCfbFeatures(line: (String, collection.mutable.Set[String]), config: Map[String, String]) = {
  //    val outFeatures = new ListBuffer[(String, String)]
  //    if (line._2.isEmpty) {
  //      outFeatures.toList
  //    } else {
  //      val prefixKey = line._1
  //      val records = line._2
  //
  //      val recordDict = collection.mutable.Map[String, (Double, Double)]()
  //      records.map(line => {
  //        val items = line.split(fieldDelimiter)
  //        val ts = items(0)
  //        val click = items(1).toDouble
  //        val view = items(2).toDouble
  //        recordDict += (ts ->(click, view))
  //      })
  //
  //      val preTimestamp = config(constant_prev_date_time).toLong
  //      val curTimeStamp = config(constant_date_time).toLong
  //      val interval = config(constant_interval).toInt
  //      val decayFactor = config(constant_decay_factor).toDouble
  //      val windSize = config(constant_wnd).toInt
  //
  //      for (curTs <- preTimestamp to curTimeStamp by windSize) {
  //        val origTimestamp = curTs - interval
  //        var prevClick = 0.0
  //        var prevView = 0.0
  //        for (timestamp <- origTimestamp to curTs + 1 by windSize) {
  //          val values = try {
  //            val value = recordDict(timestamp.toString)
  //            val currentClick = value._1
  //            val currentView = value._2
  //            (prevClick * decayFactor + currentClick, prevView * decayFactor + currentView)
  //          } catch {
  //            case e: Exception =>
  //              (prevClick * decayFactor, prevView * decayFactor)
  //          }
  //          prevClick = values._1
  //          prevView = values._2
  //        }
  //        outFeatures += ((curTs + fieldDelimiter + prefixKey, prevClick + fieldDelimiter + prevView))
  //      }
  //
  //      outFeatures.toList
  //    }
  //  }
}
