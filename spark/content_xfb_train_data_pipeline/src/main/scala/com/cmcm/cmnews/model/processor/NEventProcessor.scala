package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters
import com.cmcm.cmnews.model.util.Parameters._
import org.apache.spark.rdd.RDD

/**
 * Created by tangdong on 17/5/16.
 */
object NEventProcessor extends Processor{
  this:SparkBatchContext =>
  import Parameters._

  override def process(inputRDD:RDD[String], batchContext: collection.mutable.Map[String,String]): RDD[(String, String)] = {
//    val pids = batchContext(constant_pid).split(",")
    val afterProcessRDD:RDD[(String,String)] = super.process(inputRDD,batchContext)
    afterProcessRDD //.filter(line => {
//      val pid_in_line = line._2.split(fieldDelimiter)(3)
//      if (pids.contains(pid_in_line)){
//        true
//      }else{
//        false
//      }
//    })
  }

  override def preprocess(line:String,batchContext: collection.mutable.Map[String,String]):(String,String) = {
    val items = line.split("\t")
    try {
      val uniqueId = items(0)
      val uid = items(1)
      val cid = items(3)
      val ts = items(4)
      val joinTs = items(5)
      val pid = items(7)
      val city = items(8)
      val dwell = items(9)
      val label = items(10)
      val ukey = items(11)
      (ukey + fieldDelimiter + uid + fieldDelimiter + cid,  uniqueId + fieldDelimiter + ts + fieldDelimiter  + joinTs + fieldDelimiter + pid + fieldDelimiter + city + fieldDelimiter + dwell + fieldDelimiter + label)
    }catch {
      case e: Exception => ("unknown" + fieldDelimiter + "unknown", "unknown" + fieldDelimiter + "unknown"  +  fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown")
    }
  }
}