package com.cmcm.cmnews.model.util

import java.io.InputStream

/**
  * Created by mengchong on 8/16/16.
  */
object FeatureTypeMapping {

  lazy val featureTypeNameList = {
    val stream : InputStream = getClass.getResourceAsStream("/feature_type_mapper.txt")
    val lines = scala.io.Source.fromInputStream( stream ).getLines()
    lines.map(x => {
      val fields = x.split('\t')
      val fid = fields(0).toInt
      val ftype = fields(1)
      (fid,ftype)
    }).toArray
  }

  lazy val featureType2Id = featureTypeNameList.map(x => (x._2,x._1)).toMap
  lazy val featureId2Type = featureTypeNameList.toMap

}
