package com.cmcm.cmnews.model.input

import com.cmcm.cmnews.model.component.StreamingComponent
import com.cmcm.cmnews.model.spark.SparkStreamingContext

/**
 * Created by tangdong on 27/4/16.
 */
trait EventStreamingInput extends StreamingComponent{
    this:SparkStreamingContext =>
    def getInput()
}
