package com.cmcm.cmnews.model.util

/**
 * Created by tangdong on 27/4/16.
 */
object Parameters {
  //pipeline parameters
  val constant_parallelism = "parallelism"
  val constant_parallelismMax = "parallelismMax"
  val constant_date_time = "date_time"
  val constant_prev_date_time = "prev_data_time"
  val constant_event_input = "event_input"
  val constant_event_pv_input = "event_pv_input"
  val constant_event_pv_click_input = "event_pv_click_input"
  val constant_event_click_input = "event_click_input"
  val constant_event_readtime_input = "event_readtime_input"
  val constant_event_rdd = "event_rdd"
  val constant_event_pv_rdd = "event_pv_rdd"
  val constant_event_pv_click_rdd = "event_pv_click_rdd"
  val constant_event_click_rdd = "event_click_rdd"
  val constant_event_readtime_rdd = "event_readtime_rdd"
  val constant_content_input = "content_input"
  val constant_content_rdd = "content_rdd"
  val constant_user_input = "user_input"
  val constant_user_rdd = "user_rdd"
  val constant_xfb_input = "xfb_input"
  val constant_xfb_rdd = "xfb_rdd"
  val constant_train_out = "train_output"
  val constant_train_rdd = "train_rdd"
  val constant_merge_cfb_input = "merge_cfb_input"
  val constant_merge_cfb_rdd = "merge_cfb_rdd"
  val constant_libsvm_out = "libsvm_output"
  val constant_libsvm_rdd = "libsvm_rdd"
  val constant_wnd = "wnd"
  val constant_u_ncat = "u_ncat"
  val constant_u_nkey = "u_nkey"
  val constant_d_ncat = "d_ncat"
  val constant_d_nkey = "d_nkey"
  val constant_u_rel_ncat = "rel_u_ncat"
  val constant_u_rel_nkey = "rel_u_nkey"
  val constant_d_rel_ncat = "rel_d_ncat"
  val constant_d_rel_nkey = "rel_d_nkey"
  val constant_cat_version = "cat_version"
  val constant_kw_version = "kw_version"
  val constant_decay_factor = "decay_factor"
  val constant_decay_wind = "decay_wind"
  val constant_interval = "decay_interval"
  val constant_pid = "pid"
  val constant_random_num = "random_num"
  val constant_top_num = "top_num"
  val constant_config_file = "config_file"
  val constant_featmap_file = "featmap_file"
  val constant_l1_norm = "l1_norm"
  val constant_multi_lan = "multi_lan"

  //LR params
  val constant_jobName = "jobName"
  val constant_lr_pvBound = "lr_pvBound"
  val constant_lr_startIdx = "lr_startIdx"

  //Event Join
  val constant_event_pid = "event_pid" 
  val constant_event_lan_region = "event_lan_region"

  //process parameters
  val fieldDelimiter = "\003"
  val pairDelimiter = "\001"
  val keyValueDelimiter = "\002"
  val recordDelimiter = "\004"
  val constant_expired_time = "expired_time"
}
