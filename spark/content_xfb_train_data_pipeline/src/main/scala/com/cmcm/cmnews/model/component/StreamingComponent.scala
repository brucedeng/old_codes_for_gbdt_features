package com.cmcm.cmnews.model.component

import com.cmcm.cmnews.model.spark.SparkStreamingContext
import org.apache.spark.Logging

/**
 * Created by tangdong on 27/4/16.
 */
trait StreamingComponent extends Logging{
  def init(): Unit = {
    logInfo("BatchComponent finished init")
  }

  protected def getDStream[T](name: String, ssc: SparkStreamingContext): T = ssc.getRddContext.get(name) match{
    case Some(rdd) => rdd.asInstanceOf[T]
    case _ =>
      logError(s"lack of $name in Components")
      throw new RuntimeException(s"lack of $name in Components")
  }

  protected def setDStream(path:String, name:String, ssc: SparkStreamingContext) = {
    ssc.getBatchContext.get(path) match {
      case Some(p) =>
        val rdd = ssc.getSparkStreamingContext.textFileStream(p)
        ssc.setRddContext(name,rdd)
        val rddContextTmp = ssc.getRddContext
        val keys = rddContextTmp.keys.mkString(",")
        logInfo(s"sbc rddcontext info $keys")
        logInfo(s"Finish set $path to $name RDD with path $p")
      case _ =>
        logError(s"lack of $path!")
        throw new RuntimeException(s"lack of $path!")
    }
  }

  protected def getRDD[T](name: String, sbc: SparkStreamingContext): T = sbc.getRddContext.get(name) match{
    case Some(rdd) => rdd.asInstanceOf[T]
    case _ =>
      logError(s"lack of $name in Components")
      throw new RuntimeException(s"lack of $name in Components")
  }

  protected def setRDD(path:String, name:String, sbc: SparkStreamingContext) = {
    sbc.getBatchContext.get(path) match {
      case Some(p) =>
        val rdd = sbc.getSparkContext.textFile(p)
        sbc.setRddContext(name,rdd)
        val rddContextTmp = sbc.getRddContext
        val keys = rddContextTmp.keys.mkString(",")
        logInfo(s"sbc rddcontext info $keys")
        logInfo(s"Finish set $path to $name RDD with path $p")
      case _ =>
        logError(s"lack of $path!")
        throw new RuntimeException(s"lack of $path!")
    }
  }

  protected def getParam[T](name:String, scc:SparkStreamingContext): T = scc.getBatchContext.get(name) match{
    case Some(value) => value.asInstanceOf[T]
    case _ =>
      logInfo(s"lack of $name in Components")
      throw new RuntimeException(s"lack of $name in Components")
  }
}
