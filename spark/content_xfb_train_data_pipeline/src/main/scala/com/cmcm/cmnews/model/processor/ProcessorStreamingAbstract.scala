package com.cmcm.cmnews.model.processor

import org.apache.spark.Logging
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.DStream

/**
  * Created by lilonghua on 16/8/4.
  */
trait ProcessorStreamingAbstract[T] extends Logging with Serializable{
  def preprocess(line:String, batchContext: collection.mutable.Map[String,String]) : T


  def process(inputRDD:DStream[String],batchContext: collection.mutable.Map[String,String]): DStream[T]
}
