package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.feature.FormatLibsvmFeature
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{Parameters, featmapUtils}
import org.apache.spark.rdd.RDD

/**
  * Created by mengchong on 7/11/16.
  */
trait XFBFormatLibSvmCompute extends EventBatchCompute{
  this: SparkBatchContext =>
  import Parameters._

  override def compute: Unit = {
    super.compute
    logInfo("Start CFBTrainDataCompute")
    //get rdd from path
    val merge_cfb_rdd = getRDD[RDD[String]](constant_merge_cfb_rdd, this)
    val featureList = featmapUtils.getFeatureList(batchContext(constant_featmap_file))
    val formattedFeature = FormatLibsvmFeature.formatLibsvmData(merge_cfb_rdd,featureList)

    rddContext += (constant_libsvm_rdd -> formattedFeature)

  }
}
