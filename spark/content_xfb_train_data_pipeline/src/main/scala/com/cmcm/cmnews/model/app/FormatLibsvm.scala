package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.XFBFormatLibSvmCompute
import com.cmcm.cmnews.model.input.MergedXFBInput
import com.cmcm.cmnews.model.output.LibSvmDataOutput
import com.cmcm.cmnews.model.spark.XFBTrainDataSparkContext

/**
  * Created by mengchong on 7/13/16.
  */
object FormatLibsvm  extends BatchApp
  with XFBTrainDataSparkContext
  with MergedXFBInput
  with XFBFormatLibSvmCompute
  with LibSvmDataOutput