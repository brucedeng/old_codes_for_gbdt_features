package com.cmcm.cmnews.model.output

import com.cmcm.cmnews.model.spark.SparkStreamingContext
import com.cmcm.cmnews.model.util.Parameters
import org.apache.spark.streaming.dstream._
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.rdd.RDD


/**
 * Created by tangdong on 1/5/16.
 */
trait XFBBinaryStreamingTrainDataOutput extends EventStreamingOutput{
  this:SparkStreamingContext =>
  import Parameters._

  override def outPut: Unit ={
    super.outPut
    logInfo("Start XfbTrainDataOutput...")
    val xfbTrainDataOut = rddContext(constant_train_rdd).asInstanceOf[DStream[String]]
    xfbTrainDataOut.saveAsTextFiles(batchContext(constant_train_out),"tsv")
  }
}
