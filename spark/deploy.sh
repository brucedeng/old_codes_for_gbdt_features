#!/bin/bash

input_file="$1"
if [[ $DEPLOY_PORT == "a" ]]; then
	cur_port=8080
	echo "missing port setting. use 8080 as default."
else
	echo "using port $DEPLOY_PORT according to DEPLOY_PORT env variable."
	cur_port=$DEPLOY_PORT
fi

set -x
curl --socks5 52.34.197.81:9090 -F "file=@$input_file"  http://10.2.2.238:$cur_port/
md5	$input_file
