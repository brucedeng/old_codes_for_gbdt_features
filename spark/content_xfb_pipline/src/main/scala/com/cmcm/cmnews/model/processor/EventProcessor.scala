package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters
import org.apache.spark.rdd.RDD

/**
 * Created by tangdong on 13/4/16.
 */
object EventProcessor extends Processor{
  this:SparkBatchContext =>
  import Parameters._

  override def process(inputRDD:RDD[String], batchContext: collection.mutable.Map[String,String]): RDD[(String, String)] = {
//    val pids = batchContext(constant_pid).split(",")
    val afterProcessRDD:RDD[(String,String)] = super.process(inputRDD,batchContext)
    afterProcessRDD //.filter(line => {
//      val pid_in_line = line._2.split(fieldDelimiter)(1)
//      if (pids.contains(pid_in_line)){
//        true
//      }else{
//        false
//      }
//    })
  }
  override def preprocess(line:String,batchContext: collection.mutable.Map[String,String]):(String,String) = {
    val items = line.split("\t")
    try {
      val ts = items(0).toLong/600 * 600
      val noramlTs = ts.toLong
      val cid = items(1)
      val aid = items(2)
      val lanRegion = items(3)
      val event_type = items(4)
      val city = items(5)
      val pid = if (items(6) == "12") "5" else items(6)
      val expression = items(7) match {
        case x: String => items(7)
        case _ => "0.0"
      }
      (aid + fieldDelimiter + cid,  noramlTs + fieldDelimiter + pid + fieldDelimiter + event_type + fieldDelimiter + city + fieldDelimiter + expression + fieldDelimiter + lanRegion)
    }catch {
      case e: Exception => ("unknown" + fieldDelimiter + "unknown", "0" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "")
    }
  }
}
