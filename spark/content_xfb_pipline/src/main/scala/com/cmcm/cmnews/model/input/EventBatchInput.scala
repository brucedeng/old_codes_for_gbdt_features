package com.cmcm.cmnews.model.input

import com.cmcm.cmnews.model.component.BatchComponent
import com.cmcm.cmnews.model.spark.SparkBatchContext

/**
  * Created by tangdong on 13/4/16.
  */
trait EventBatchInput extends BatchComponent{
   this:SparkBatchContext =>
   def getInput()
 }
