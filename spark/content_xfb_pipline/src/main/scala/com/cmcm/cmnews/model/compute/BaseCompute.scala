package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.spark.SparkBatchContext

/**
 * Created by tangdong on 13/4/16.
 */
class BaseCompute extends EventBatchCompute{
  this:SparkBatchContext =>
  override def compute: Unit = {
    super.compute
    logInfo("Base Compute...")
  }
}
