package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.component.BatchComponent
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{JsonUtil, LoggingUtils, Parameters}
import org.apache.spark.rdd.RDD
import org.json4s.JsonAST._

import scala.util.{Failure, Success, Try}
import scala.util.control.Breaks._

/**
 * Created by tangdong on 13/4/16.
 */

object UPProcessor extends Processor {
  this: SparkBatchContext =>
  import Parameters._

  override def process(inputRDD:RDD[String], batchContext: collection.mutable.Map[String,String]): RDD[(String, String)] = {
    val afterProcessRDD:RDD[(String,String)] = super.process(inputRDD,batchContext)
    afterProcessRDD.filter(line => {
      val uuid = line._1
      if (uuid == ""){
        false
      }else{
        true
      }
    })
  }
  override def preprocess(line:String,batchContext: collection.mutable.Map[String,String]) = {
    val topCat = Try(batchContext(constant_u_ncat)).getOrElse("5").toInt
    val topKw = Try(batchContext(constant_u_nkey)).getOrElse("25").toInt
    Try(JsonUtil.convertToJValue(line)) match {
      case Success(jvalue) =>
        val uuid = extractStr(jvalue,"uid", "unknown")
        val categories = jvalue\"categories" match {
          case JArray(cats) => catKwAssist(cats.toList,"",topCat,"unknown")
          case _ => "unknown"
        }
        val keywords = jvalue\"keywords" match {
          case JArray(cats) => catKwAssist(cats.toList,"",topKw,"unknown")
          case _ => "unknown"
        }
        val gender = extractStr(jvalue,"gender", "unknown")
        val age = extractStr(jvalue,"age", "unknown")
        (uuid, categories + fieldDelimiter + keywords + fieldDelimiter + gender + fieldDelimiter + age)
      case Failure(ex) =>
        LoggingUtils.loggingError(LoggingUtils.getException(ex))
        ("","unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown")

    }
  }

  def catKwAssist(jsonItems: List[JValue], version:String, top:Int, default:String): String = {
    def parse_item(item:JValue) = {
      val name = (item \ "name").asInstanceOf[JString].s
      val L1_weight = item \ "weight" match {
        case JDouble(x) =>
          x.toDouble
        case JInt(x) =>
          x.toDouble
        case _ =>
          0.0
      }
      name + keyValueDelimiter + L1_weight
    }
    jsonItems.map(x => parse_item(x)).take(top).mkString(pairDelimiter)
//     sb.append(name + keyValueDelimiter + L1_weight + pairDelimiter)
  }

  def extractStr(jvalue: JValue, fieldName: String, default: String = "") = {
    jvalue \ fieldName match {
      case JString(s) => if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString()
      case _ => default
    }
  }

}

