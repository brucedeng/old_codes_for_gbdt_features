package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.config.Config
import com.cmcm.cmnews.model.feature.{CFBFeature, ContentFeature, UserFeature}
import com.cmcm.cmnews.model.processor.{CPProcessor, EventProcessor, UPProcessor}
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{LoggingUtils, Parameters}
import org.apache.spark.rdd.RDD
import org.joda.time.DateTime
import scala.util.{Failure, Success, Try}

/**
 * Created by tangdong on 13/4/16.
 */
trait CFBAppCompute extends EventBatchCompute{
  this: SparkBatchContext =>
  import Parameters._

  override def compute: Unit = {
    super.compute
    logInfo("Start CFBAppCompute")
    //get rdd from path
    val eventRdd = getRDD[RDD[String]](constant_event_rdd,this)
    val cpRdd = getRDD[RDD[String]](constant_content_rdd, this)
    val upRdd = getRDD[RDD[String]](constant_user_rdd, this)

    //get parsed rdd
    val parsedEventRdd: RDD[(String, String)] = EventProcessor.process(eventRdd, this.getBatchContext)
    val parsedCpRdd: RDD[(String, String)] = CPProcessor.process(cpRdd, this.getBatchContext)
    val parsedUpRdd: RDD[(String, String)] = UPProcessor.process(upRdd, this.getBatchContext)

    //join rdd
    val eventJoinUpRdd = UserFeature.joinWithBatchEvent(parsedUpRdd, parsedEventRdd)
    val eventJoinUpAndCpRdd = ContentFeature.joinWithBatchEvent(parsedCpRdd, eventJoinUpRdd)

    val multiLan = Try(batchContext(constant_multi_lan)).getOrElse("false").toLowerCase()
    val multiLanBool = if(multiLan=="true" || multiLan=="yes") true else false

//    eventJoinUpAndCpRdd.saveAsTextFile("hdfs://mycluster/tmp/news_model/eventJoinUpAndCpRdd")
    val typesafeConfig = new Config(Try(batchContext(constant_config_file)).getOrElse("")).typesafeConfig

    val runDateTime = batchContext(constant_date_time)
    val timeStamp = try { new DateTime(runDateTime).getMillis/1000 } catch {
      case e:Exception => 0
    }

    val cfbFeaturesRdd: RDD[(String,(Double, Double))] = CFBFeature.generateFullCFBFeatures(eventJoinUpAndCpRdd, timeStamp,typesafeConfig,multiLanBool)
    val cfbFeatureResultReduce = cfbFeaturesRdd.reduceByKey((x,y) => {
      val click = x._1 + y._1
      val view = x._2 + y._2
      (click, view)
    })

    val cfbFeatureResult = cfbFeatureResultReduce.map(x => {
      val key = x._1.split(fieldDelimiter).mkString("\t")
      val decayClick = x._2._1
      val decayView = x._2._2
      s"$key\t$decayClick\t$decayView"
    })
    rddContext += (constant_xfb_rdd -> cfbFeatureResult)
//    rddContext += (constant_xfb_rdd -> eventJoinUpAndCpRdd)
  }
}
