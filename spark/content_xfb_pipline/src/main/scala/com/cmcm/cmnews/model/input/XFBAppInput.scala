package com.cmcm.cmnews.model.input

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters

/**
 * Created by tangdong on 13/4/16.
 */
trait XFBAppInput extends EventBatchInput{
  this: SparkBatchContext =>
  import Parameters._

  override def getInput(): Unit ={
    logInfo("start CFBAppInput...")
    setRDD(constant_event_input,constant_event_rdd, this)
    setRDD(constant_content_input, constant_content_rdd, this)
    setRDD(constant_user_input, constant_user_rdd, this)

  }

}
