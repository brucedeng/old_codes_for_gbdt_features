package com.cmcm.cmnews.model.util

/**
 * Created by tangdong on 13/4/16.
 */
object Parameters {
  //pipeline parameters
  val constant_parallelism = "parallelism"
  val constant_date_time = "date_time"
  val constant_event_input = "event_input"
  val constant_event_rdd = "event_rdd"
  val constant_content_input = "content_input"
  val constant_content_rdd = "content_rdd"
  val constant_user_input = "user_input"
  val constant_user_rdd = "user_rdd"
  val constant_xfb_input = "xfb_output"
  val constant_xfb_rdd = "xfb_rdd"
  val constant_wnd = "wnd"
  val constant_u_ncat = "u_ncat"
  val constant_u_nkey = "u_nkey"
  val constant_d_ncat = "d_ncat"
  val constant_d_nkey = "d_nkey"
  val constant_cat_version = "cat_version"
  val constant_kw_version = "kw_version"
  val constant_pid = "pid"
  val constant_config_file = "config_file"
  val constant_l1_norm = "l1_norm"
  val constant_multi_lan = "multi_lan"

  //process parameters
  val fieldDelimiter = "\003"
  val pairDelimiter = "\001"
  val keyValueDelimiter = "\002"
  val constant_expired_time = "expired_time"

  val actMapping:Map[String,(String,String)] = Map("1" -> ("PV","impression"),
    "2" -> ("CL","click"),
    "3" -> ("LD","listpagedwelltime"),
    "4" -> ("DW","readtime"),
    "5" -> ("CP","completeness"),
    "6" -> ("PR","praise"),
    "7" -> ("DL","dislike"),
    "8" -> ("CM","comment"),
    "9" -> ("SH","share"),
    "10" -> ("LI","listpageimpression"),
    "11" -> ("TR","tread"),
    "12" -> ("AF","adsfill"),
    "13" -> ("RT","rsstimeout"),
    "14" -> ("SR","searchs"),
    "15" -> ("PA","pushacceptance"),
    "16" -> ("MD","mood"),
    "17" -> ("CC","channelchange"),
    "18" -> ("CN","channelnodata"),
    "101" -> ("PS","push")
  )
}
