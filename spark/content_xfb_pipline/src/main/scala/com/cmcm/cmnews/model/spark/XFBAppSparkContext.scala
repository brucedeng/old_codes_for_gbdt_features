package com.cmcm.cmnews.model.spark

import com.cmcm.cmnews.model.util.Parameters
import org.apache.spark.{SparkContext, SparkConf}

import scala.util.Try

/**
 * Created by tangdong on 13/4/16.
 */
trait XFBAppSparkContext extends SparkBatchContext{
  import Parameters._

  override def init(): Unit ={
    super.init()
    logInfo("Start XFBAppSparkContext...")
    val sparkConf = new SparkConf()
    .set("spark.default.parallelism", Try(batchContext(constant_parallelism)).getOrElse("32"))
//    .setAppName("cmnews raw xfb pipeline")
    sbc = new SparkContext(sparkConf)
  }

}
