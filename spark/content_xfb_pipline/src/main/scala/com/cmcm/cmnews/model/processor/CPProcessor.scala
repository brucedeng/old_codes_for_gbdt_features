package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{LoggingUtils, JsonUtil, Parameters}
import org.apache.spark.rdd.RDD
import org.json4s.JsonAST._

import scala.util.{Failure, Success, Try}
import scala.util.control.Breaks._

/**
 * Created by tangdong on 13/4/16.
 */
object CPProcessor extends Processor{
  import Parameters._

  override  def process(inputRDD:RDD[String], batchContext: collection.mutable.Map[String,String]): RDD[(String, String)] = {
    val afterProcessRDD:RDD[(String,String)] = super.process(inputRDD,batchContext)
    afterProcessRDD.filter(line => {
      val cid = line._1
      if(cid == ""){
        false
      } else{
        true
      }
    }).reduceByKey((left,right) =>{
      val leftUpdateTime = left.split(fieldDelimiter)(3).toLong
      val rightUpdateTime = right.split(fieldDelimiter)(3).toLong
      if (leftUpdateTime < rightUpdateTime)
        right
      else
        left
    })
  }

  override def preprocess(line:String, batchContext: collection.mutable.Map[String,String]) = {
    val categoryVersion = Try(batchContext(constant_cat_version)).getOrElse("v4")
    val keywordVersion =  Try(batchContext(constant_kw_version)).getOrElse("v2")
    val topCat =  Try(batchContext(constant_d_ncat)).getOrElse("5").toInt
    val topKw = Try(batchContext(constant_d_nkey)).getOrElse("15").toInt
    val l1_norm = Try(batchContext(constant_l1_norm)).getOrElse("true").toLowerCase
    val l1Flag = if(l1_norm=="yes"||l1_norm=="true") true else false

    Try(JsonUtil.convertToJValue(line)) match{
      case Success(jvalue) =>
        val cid = if ( keywordVersion == "oversea" ) extractStr(jvalue,"item_id")
          else extractStr(jvalue,"content_id")
        val groupid = extractStr(jvalue,"group_id") match {
          case x:String if x != "" => x
          case _ => "unknown"
        }
        val titlemd5 = extractStr(jvalue,"title_md5") match {
          case x:String if x != "" => x
          case _ => "unknown"
        }
        val tier = extractStr(jvalue,"tier") match {
          case x:String if x != "" => x
          case _ => "unknown"
        }
        val publisher = extractStr(jvalue,"publisher") match {
          case x:String if x != "" => x
          case _ => "unknown"
        }
        val publishTime = extractStr(jvalue,"publish_time") match {
          case x:String if x != ""  => x
          case _ => "0"
        }
        val updateTime = extractStr(jvalue,"update_time") match {
          case x:String if x != "" => x
          case _ => "0"
        }
        val catStr = if ( categoryVersion == "oversea" ){
          jvalue \ "categories" match {
            case JArray(arr) =>
              overseaCatKwAssist(arr, categoryVersion, topCat, "unknown",l1Flag)
            case _ => "unknown"
          }
        } else if( categoryVersion == "oversea_l2") {
          jvalue \ "l2_categories" match {
            case JArray(arr) =>
              overseaCatKwAssist(arr, categoryVersion, topCat, "unknown",l1Flag)
            case _ => "unknown"
          }
        } else {
          jvalue \ "categories" match {
            case JArray(arr) =>
              catKwAssist(arr, categoryVersion, topCat, "unknown")
            case _ => "unknown"
          }
        }
        val kwStr = if ( keywordVersion == "oversea" ){
          jvalue \ "entities" match {
            case JArray(arr) =>
              overseaCatKwAssist(arr, categoryVersion, topKw, "unknown",l1Flag)
            case _ => "unknown"
          }
        } else {
          jvalue \ "keywords" match {
            case JArray(arr) =>
              catKwAssist(arr, keywordVersion, topKw, "unknown")
            case _ => "unknown"
          }
        }

        (cid, groupid + fieldDelimiter + tier + fieldDelimiter + publisher + fieldDelimiter + updateTime + fieldDelimiter + publishTime + fieldDelimiter + catStr + fieldDelimiter + kwStr + fieldDelimiter + titlemd5)
      case Failure(ex) =>
        LoggingUtils.loggingError(LoggingUtils.getException(ex))
        ("","unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "0" + fieldDelimiter + "0" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown")
    }


  }

  def extractStr(jvalue: JValue, fieldName: String, default: String = "") = {
    jvalue \ fieldName match {
      case JString(s) => if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString()
      case _ => default
    }
  }

  def overseaCatKwAssist(jsonItems: List[JValue], version:String, top:Int, default:String, l1Norm:Boolean): String = {
    def parse_item(item: JValue) = {
      val name = (item \ "name").asInstanceOf[JString].s
      val L1_weight = item \ "weight" match {
        case JDouble(x) =>
          x.toDouble
        case JInt(x) =>
          x.toDouble
        case _ =>
          0.0
      }
      //        name + keyValueDelimiter + L1_weight
      (name, L1_weight)
    }
    val data = jsonItems.map(x => parse_item(x)).filter(x => x._1.nonEmpty)
    //.filter(x=> !x.startsWith(keyValueDelimiter)).take(top).mkString(pairDelimiter)
    if (l1Norm) {
      val sumWt = data.map(x => x._2).sum
      val sumWeight = if (sumWt > 0) sumWt else 1.0
      data.map(x => x._1 + keyValueDelimiter + x._2 / sumWeight).take(top).mkString(pairDelimiter)
    }
    else
      data.map(x => x._1 + keyValueDelimiter + x._2).take(top).mkString(pairDelimiter)
  }

  def catKwAssist(jsonItems: List[JValue], version:String, top:Int, default:String): String = {
      if (jsonItems.isEmpty) default
      else {
        jsonItems.head \ "version" match {
          case JString(v) if v == version => {
            val sb = new StringBuilder
            val items = (jsonItems.head \ "list").asInstanceOf[JArray].arr
            var index = 0
            breakable {
              for (item <- items) {
                if (index >= top)
                  break()
                index += 1
                val name = (item \ "name").asInstanceOf[JString].s
                val L1_weight = (item \ "L1_weight") match {
                  case JDouble(x) =>
                    x.toDouble
                  case JInt(x) =>
                    x.toDouble
                  case _ =>
                    0.0
                }
                sb.append(name + keyValueDelimiter + L1_weight + pairDelimiter)

              }
            }
            sb.toString()
          }
          case _ => catKwAssist(jsonItems.tail, version, top, default)
        }
      }
  }
}
