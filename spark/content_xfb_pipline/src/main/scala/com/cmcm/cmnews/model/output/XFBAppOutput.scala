package com.cmcm.cmnews.model.output

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.rdd.RDD

/**
 * Created by tangdong on 13/4/16.
 */
trait XFBAppOutput extends EventBatchOutput{
  this:SparkBatchContext =>
  import Parameters._

  override def outPut: Unit ={
    super.outPut
    logInfo("Start CFBAppOutput...")
    val cfbAppResult = rddContext(constant_xfb_rdd).asInstanceOf[RDD[String]]
    cfbAppResult.saveAsTextFile(batchContext(constant_xfb_input),classOf[GzipCodec])
  }
}
