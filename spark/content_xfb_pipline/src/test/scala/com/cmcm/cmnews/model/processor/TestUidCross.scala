package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.config.Config
import com.cmcm.cmnews.model.feature.CFBFeature
import com.cmcm.cmnews.model.util.Parameters._
import org.scalatest.FunSuite

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
  * Created by mengchong on 7/12/16.
  */
class TestUidCross extends FunSuite{
  test("parse up data" ) {
    val featureMap = new mutable.HashMap[String, List[(String, Double)]]
    featureMap.put("U_UID" ,List(("aaa", 1.0)))
    featureMap.put("D_CATEGORY" , List(("1000001",1.0)))
    featureMap.put("U_CATEGORY" , List(("1000001",0.3),("1000002",0.7)))
    val crossFeatureConfig = "C_D_U_CATEGORY_UID,C_D_U_CATEGORY_CATEGORY"
    val cfbFeatures = new ListBuffer[(String, (Double, Double))]
    val event = ("1467114000","11",2.0,1.0)
    val parsed = CFBFeature.getCrossCFBFeatures(cfbFeatures,crossFeatureConfig,featureMap,event,"")
      println(parsed)
  }

  test("gen full cfb") {
    val key = "15822774"
    val event = "ed3338820bb69b8d\u00031466921400\u000311\u00031\u0003unknown\u00031.0\u0003\u00031000028\u00020.187525\u00011000844\u00020.175213\u00011000001\u00020.145916\u00011000856\u00020.117713\u00011001110\u00020.079345\u0003भारत\u00020.01616\u0001बॉलीवुड\u00020.014686\u0001अक्षय_कुमार\u00020.01438\u0001महिला\u00020.010237\u0001दिल्ली\u00020.010169\u0001फिल्म\u00020.009656\u0001समय\u00020.009464\u0001शादी\u00020.008891\u0001रोनाल्डो\u00020.008735\u0001मैच\u00020.008653\u0001टीम_इंडिया\u00020.008321\u0001रियो_ओलंपिक\u00020.008136\u0001विराट_कोहली\u00020.008067\u0001पत्नी\u00020.008015\u0001दीपिका_पादुकोण\u00020.007943\u0001दीपिका\u00020.007759\u0001सलमान\u00020.007731\u0001मां\u00020.007684\u0001छात्र\u00020.007433\u0001हैरान\u00020.007122\u0001ओलंपिक\u00020.007114\u0001देश\u00020.006967\u0001कार\u00020.006903\u0001खूबसूरत\u00020.006873\u0001अक्षय\u00020.006872\u0003unknown\u0003unknown\u0003249933\u0003unknown\u0003Jansatta\u00031467275341\u00031466668126\u00031000844\u00020.0\u0003सपना\u00020.24299461127221492\u0001विचार\u00020.06270089613194488\u0001मुक्केबाजी\u00020.05571850959108112\u0001समय\u00020.05478078293633079\u0001रियो\u00020.049527564080961\u0001उम्मीद\u00020.045870655591264936\u0001रियो_ओलंपिक\u00020.042570608426425724\u0001प्रवेश\u00020.04158059427697397\u0001पूरी_तरह\u00020.04059058012752221\u0001अध्यक्ष\u00020.037290532962683004\u0001फिटनेस\u00020.03630051881323124\u0001मेरा_भविष्य\u00020.03531050466377948\u0001प्रयास\u00020.03465049523081164\u0001लिये\u00020.03465049523081164\u0001स्टार\u00020.0339904857978438\u0003c53c7816"
    val parsed = CFBFeature.generateCFBFeatures((key,event),1467114000,new Config("").typesafeConfig,false)
    println(parsed)
//    val typesaveConfig = new Config("").typesafeConfig
//    println(typesaveConfig.entrySet())
  }

  test("parse config multi events") {
    val typesafeConfig = new Config("").typesafeConfig
    val curConf = typesafeConfig.getConfig("cfb.coec-1")
    println(curConf.getString("uf"))
//    println(typesaveConfig)
  }
}
