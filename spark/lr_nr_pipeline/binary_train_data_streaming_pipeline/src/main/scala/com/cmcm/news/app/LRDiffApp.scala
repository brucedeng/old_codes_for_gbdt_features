package com.cmcm.news.app

import com.cmcm.news.analysis.LRFeatureDiff
import com.cmcm.news.config.Configurable
import com.cmcm.news.spark.SparkNormal

/**
  * Created by lilonghua on 16/9/9.
  */
object LRDiffApp extends NormalApp
  with Configurable
  with SparkNormal
  with LRFeatureDiff
