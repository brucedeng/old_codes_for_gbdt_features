package com.cmcm.news.component

import com.cmcm.news.config.Configurable
import com.cmcm.news.parse.UPTimingLoader
import com.cmcm.news.spark.SparkComponent

import scala.util.Try

/**
  * FeatureComponent is used to process all features.
  * 1, provide interfaces to get or process features.
  */
object FeatureComponent {
  val componentName = "feature_component"
  val configFileName = "feature.conf"
  val defaultUPLoadPeriod = 86400
  val defaultUPDatetimeFormat = "yyyyMMdd/HH"
  val defaultUPDatetimeOffset = -1
}

trait FeatureComponent  extends Component { this: SparkComponent with Configurable =>

  override def init(configDir: String) = {
    super.init(configDir)
    initFeatureComponent(configDir + "/" + FeatureComponent.configFileName)
    val upConfig = featureConfigUnit.getConfig.getConfig("feature").getConfig("up")
    val waitTimes = Try(upConfig.getInt("wait_times")).getOrElse(15)

    UPTimingLoader.initUPTimingLoader(upConfig, sparkContext)
    UPTimingLoader.waitForReady(waitTimes)
  }

  override def preStart() = {
    super.preStart()
  }

  def featureConfigUnit = {
    getConfigUnit(FeatureComponent.componentName).get
  }

  def initFeatureComponent(featureConfigFile: String) = {
    logInfo("Initializing feature component...")
    loadComponentConfig(FeatureComponent.componentName, featureConfigFile)
  }
}
