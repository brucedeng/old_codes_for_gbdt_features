package com.cmcm.news.processor

import com.cmcm.news.component.{EventComponent, FeatureComponent, ModelComponent}
import com.cmcm.news.config.{ConfigUnit, Configurable}
import com.cmcm.news.feature._
import com.cmcm.news.monitor.MonitorMetrics
import com.cmcm.news.output._
import com.cmcm.news.parse.UPTimingLoader
import com.cmcm.news.spark.SparkComponent
import com.cmcm.news.util.Utils
import org.apache.spark.streaming.Seconds
import org.apache.spark.streaming.dstream.DStream

import scala.util.Try

/**
  * Created by lilonghua on 16/8/24.
  */

trait LRModelProcessor extends ModelComponent {
  this: Configurable with SparkComponent with EventComponent with FeatureComponent =>

  def getCfbModelOutput(modelConfigUnit: ConfigUnit) = {
    val modelOutputDest = modelConfigUnit.getString("model.lr.output.dest", "file")
    val topic = modelConfigUnit.getString("model.lr.output.kafka.topic", " lr_data_insta_hindi")

    def getKafkas(): Map[String, (String, String, String)] = {
      val broker = modelConfigUnit.getString("model.lr.output.kafka.broker.list", "10.2.2.251:9092,10.2.2.253:9092,10.2.2.5:9092,10.2.2.186:9092,10.2.2.187:9092")
      val producerType = modelConfigUnit.getString("model.lr.output.kafka.producer.type", "async")
      var kafkas = Map("default" ->(broker, producerType, topic))
      kafkas
    }

    modelOutputDest match {
      case "kafka" =>
        //new LRModelKafkaOutput(getKafkas())
        new LRStreamingOutput(ModelComponent.kafkaConf, topic)
      case "kafka_N_file" =>
        val modelOutputFilePrefix = modelConfigUnit.getString("model.lr.output.file.prefix",
          ModelComponent.defaultModelOutputFilePrefix)
        /*val outputs = List(new LRModelKafkaOutput(getKafkas()), new LRModelFileOutput(modelOutputFilePrefix, modelConfigUnit.getInt("model.lr.decay.window", 600)))
        new LRModelMixedOutput(outputs)*/
        new LRStreamingMixedOutput(List(new LRStreamingOutput(ModelComponent.kafkaConf, topic), new LRStreamingFileOutput(modelOutputFilePrefix)))
      case _ =>
        val modelOutputFilePrefix = modelConfigUnit.getString("model.lr.output.file.prefix",
          ModelComponent.defaultModelOutputFilePrefix)
        //new LRModelFileOutput(modelOutputFilePrefix, modelConfigUnit.getInt("model.lr.decay.window", 600))
        new LRStreamingFileOutput(modelOutputFilePrefix)
    }
  }

  /**
    *
    * @param inputDStream (contentId, (uid, batchId, show, click, eventFeature))
    * @return (uid, (contentId, batchId, show, click, eventFeature, contentFeature))
    */
  def joinCNCP(inputDStream: DStream[(String, (String, Long, Double, Double, String))]) = {
    val contentFeatureConfig = featureConfigUnit.getConfig.getConfig("feature").getConfig("cp")
    ContentFeature.getContentFeatureFromRedis(inputDStream, contentFeatureConfig)
  }

  /**
    *
    * @param inputDStream (contentId, (uid, batchId, act, value, eventFeature))
    * @return (uid, (cid, batchId, act, value, eventFeature, contentFeature))
    */
  def joinIndiaCP(inputDStream: DStream[(String, (String, Long, String, Double, String))]) = {
    val contentFeatureConfig = featureConfigUnit.getConfig.getConfig("feature").getConfig("cp")
    val cluster = Try(contentFeatureConfig.getBoolean("cluster")).getOrElse(false)
    if (cluster) {
      ContentFeatureForIndia.getCPFromRedisCluster(inputDStream, contentFeatureConfig)
    } else {
      ContentFeatureForIndia.getContentFeatureFromRedis(inputDStream, contentFeatureConfig)
    }
  }

  /**
    *
    * @param inputDStream (uid, (cid, batchId, act, value, eventFeature, contentFeature))
    * @return ((uid, contentId), (batchId, act, value, eventFeature, contentFeature, userFeature))
    */
  def joinUP(inputDStream: DStream[(String, (String, Long, String, Double, String, String))]) = {
    UserFeature.joinWithBatchEvent(UPTimingLoader.getCurrentUserFeatureRDD _, inputDStream)
  }

  /**
    *
    * @param inputDStream (uid, (cid, batchId, act, value, eventFeature, contentFeature))
    * @return ((uid, contentId), (batchId, act, value, eventFeature, contentFeature, userFeature))
    */
  def joinUPCN(inputDStream: DStream[(String, (String, Long, Double, Double, String, String))]) = {
    UserFeature.joinWithBatchEventCN(UPTimingLoader.getCurrentUserFeatureRDD _, inputDStream)
  }


  /**
    *
    * @param inputDStream ((uid, contentId), (batchId, show, click, eventFeature, contentFeature, userFeature))
    * @return (lrFeature, batchId, show, click, "")
    */
  def generateCNCFBFeatures(inputDStream: DStream[((String, String), (Long, Double, Double, String, String, Option[String]))]) = {
    val cfbFeatureConfig = featureConfigUnit.getConfig.getConfig("feature").getConfig("lr")
    CFBFeature.generateCFBFeatureDstream(inputDStream, cfbFeatureConfig)
  }

  /**
    *
    * @param inputDStream ((uid, contentId), (batchId, act, value, eventFeature, contentFeature, userFeature))
    * @return (feature\001value, (batchId, view, click, country_lan))
    */
  def generateWorldwideLRBFeatures(inputDStream: DStream[((String, String), (Long, String, Double, String, String, Option[String]))]) = {
    val cfbFeatureConfig = featureConfigUnit.getConfig.getConfig("feature").getConfig("lr")
    //CFBFeatureV2.generateCFBFeatureDstream(inputDStream, cfbFeatureConfig)
    LRBFeature.generateLRBFeatureDstream(inputDStream, cfbFeatureConfig)
  }

  /**
    * Reduce by key which is feature-value e.g.'CL_PV_M_D_U_KEYWORD_KEYWORD'
    *
    * @param inputDStream ((batchId,pid), (uid, cid, label, dwell, featureMap))
    * @return (string)
    */
  def cfbAggregation(inputDStream: DStream[((Long,String), (String, String, String, Double, String))]) = {
    val kafkaOffsetSaver = getKafkaOffsetSaver()
    //.map(f => (f._2, f._2._1 + "\t" + f._2._2 + "\t" + f._2._3 + ":" + f._2._4.toString + "\t" + f._2._5))
    inputDStream.transform(rdd => {
      kafkaOffsetSaver()
      rdd
    })
  }


  /**
    *
    * @param inputDStream (feature\001value, (batchId, view, click, country_lan))
    * @return (feature\001value, (batchId, view_aggre, click_aggre, country_lan))
    */
  def cfbAggregationWindowed(inputDStream: DStream[(String, (Long, Double, Double, String))]) = {
    val kafkaOffsetSaver = getKafkaOffsetSaver()
    inputDStream.reduceByKeyAndWindow((left: (Long, Double, Double, String), right: (Long, Double, Double, String)) => {
      (left._1, left._2 + right._2, left._3 + right._3, left._4)
    }, Seconds(600), Seconds(600))
      .map(modelRecord => {
        (modelRecord._1, modelRecord._2._1, modelRecord._2._2, modelRecord._2._3, modelRecord._2._4)
      }).transform(rdd => {
      kafkaOffsetSaver()
      rdd
    })
  }

  def cfbMetricsOutput(eventDStream: DStream[String],
                       parsedDStream: DStream[((String, String, String, Long), (String, Double))],
                       labelDStream: DStream[((String, String, String, Long), (String, Double))],
                       featureStreamWithCP: DStream[(String, (String, Long, String, Double, String, String))],
                       featureStreamWithCPAndUP: DStream[((String, String), (Long, String, Double, String, String, Option[String]))],
                       lrbFeatureStream: DStream[((Long,String), (String, String, String, Double, String))]
                       ): Unit = {
    val monitorEnabled = modelConfigUnit.getBoolean("model.monitor.enabled", false)
    if (monitorEnabled) {
      eventDStream.foreachRDD(rdd => {
        val eventCount = rdd.count()
        ModelComponent.monitor.recordFromDriver(MonitorMetrics.CFB_INPUT_EVENT_COUNT, eventCount.toString)
      })

      val window = modelConfigUnit.getInt("model.lr.decay.window", ModelComponent.defaultDecayWindow)
      parsedDStream.foreachRDD((rdd, time) => {
        Utils.cfbParsedMetrics(rdd, time, window)
      })

      var lableCount = 0L
      labelDStream.foreachRDD(rdd => {
        lableCount = rdd.count()
        ModelComponent.monitor.recordFromDriver(MonitorMetrics.CFB_LABEL_COUNT, lableCount.toString)
      })

      featureStreamWithCP.foreachRDD(Utils.outputFeatureCPMetrics(_,lableCount))
      featureStreamWithCPAndUP.foreachRDD(Utils.outputFeatureCPnUPMetrics(_,lableCount))
      lrbFeatureStream.foreachRDD(Utils.outputModelMetrics _)
    }
  }

  def lrbMetricsOutput(eventDStream: DStream[String],
                       parsedDStream: DStream[((String, String, String, Long), (String, Double))],
                       labelDStream: DStream[((String, String, String, Long), (String, Double))],
                       lrbFeatureStream: DStream[((Long,String), (String, String, String, Double, String))]
                      ): Unit = {
    val monitorEnabled = modelConfigUnit.getBoolean("model.monitor.enabled", false)
    if (monitorEnabled) {
      eventDStream.foreachRDD(rdd => {
        val eventCount = rdd.count()
        ModelComponent.monitor.recordFromDriver(MonitorMetrics.CFB_INPUT_EVENT_COUNT, eventCount.toString)
      })

      val window = modelConfigUnit.getInt("model.lr.decay.window", ModelComponent.defaultDecayWindow)
      parsedDStream.foreachRDD((rdd, time) => {
        Utils.cfbParsedMetrics(rdd, time, window)
      })

      //var lableCount = 0L
      labelDStream.foreachRDD(rdd => {
        val lableCount = rdd.count()
        ModelComponent.monitor.recordFromDriver(MonitorMetrics.CFB_LABEL_COUNT, lableCount.toString)
      })

      /*featureStreamWithCP.foreachRDD(Utils.outputFeatureCPMetrics(_,lableCount))
      featureStreamWithCPAndUP.foreachRDD(Utils.outputFeatureCPnUPMetrics(_,lableCount))*/
      lrbFeatureStream.foreachRDD(Utils.outputModelMetrics _)
    }
  }

}