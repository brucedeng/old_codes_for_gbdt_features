package com.cmcm.news.feature

import com.cmcm.news.feature.Profile.{CategoryProfile, KeywordProfile,TopicProfile}
import com.cmcm.news.util.Utils
import com.typesafe.config.Config
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.Logging
import redis.clients.jedis.{HostAndPort, Jedis, JedisCluster}
import collection.JavaConversions._
import scala.util.Try

/**
  * Created by wangwei5 on 15/8/12.
  */
object ContentFeatureForIndia extends Logging {

  def getCategories(cc: List[CategoryProfile], topCategories: Int) = {
    val ccBuilder = new StringBuilder()
    var i = 0
    while (i < math.min(cc.size, topCategories)) {
      val name = cc.get(i).getName
      val weight = cc.get(i).getL1Weight  // to be changed to get L1 weight when redis is read
      ccBuilder.append(name + Utils.keyGap + weight + Utils.pairGap)
      i += 1
    }
    if (i > 0)
      ccBuilder.dropRight(1).toString()
    else
      ccBuilder.toString()
  }


  def getKeywords(ck: List[KeywordProfile], topKeywords: Int) = {
    val ckBuilder = new StringBuilder()
    var i = 0
    while (i < math.min(ck.size, topKeywords)) {
      val name = ck.get(i).getName
      val weight = ck.get(i).getL1Weight // to be changed to get L1 weight when redis is ready
      ckBuilder.append(name + Utils.keyGap + weight + Utils.pairGap)
      i += 1
    }
    if (i > 0)
      ckBuilder.dropRight(1).toString()
    else
      ckBuilder.toString()
  }


  def getTopics(ct: List[TopicProfile], topTopics: Int) = {
    val ctBuilder = new StringBuilder()
    var i = 0
    while (i < math.min(ct.size, topTopics)) {
      val name = ct.get(i).getName
      val weight = ct.get(i).getL1Weight // to be changed to get L1 weight when redis is ready
      ctBuilder.append(name + Utils.keyGap + weight + Utils.pairGap)
      i += 1
    }
    if (i > 0)
      ctBuilder.dropRight(1).toString()
    else
      ctBuilder.toString()
  }

  def getContentFeature(news_id: String,
                        redisClient: Jedis,
                        topRelCat: Int,
                        topRelKw: Int,
                        topRelTopic: Int,
                        topCategories: Int,
                        topKeywords: Int,
                        topTopics: Int,
                        categoryVersion: String,
                        keywordVersion: String,
                        topicVersion: String
                        ) = {
    val cf = redisClient.get(("n_" + news_id).getBytes())

    if (cf != null) {
      val newsProfile = CmnewsNewsProfile.NewsProfile.parseFrom(cf)

      val cc = if(categoryVersion.length <= 0){
        newsProfile.getCategoriesList.toList
      } else {
        var ccList = List[CategoryProfile]()
        for (category <- newsProfile.getVcategoriesList) {
          if (category.getVersion.equals(categoryVersion)) {
            ccList = category.getCategoriesList.toList
          }
        }
        ccList
      }
      val ccString = getCategories(cc, topCategories)
      val ccRel = getCategories(cc,topRelCat)

      val ck = if(keywordVersion.length <= 0){
        newsProfile.getKeywordsList.toList
      } else {
        var ckList = List[KeywordProfile]()
        for (keyword <- newsProfile.getVkeywordsList) {
          if (keyword.getVersion.equals(keywordVersion)) {
            ckList = keyword.getKeywordsList.toList
          }
        }
        ckList
      }
      val ckString = getKeywords(ck, topKeywords)
      val ckRel = getKeywords(ck, topRelKw)


      val ct = if(topicVersion.length <= 0){
        List[TopicProfile]()
      } else {
        var ctList = List[TopicProfile]()
        for (topic <- newsProfile.getVtopicsList) {
          if (topic.getVersion.equals(topicVersion)) {
            ctList = topic.getTopicsList.toList
          }
        }
        ctList
      }
      val ctString = getTopics(ct, topTopics)
      val ctRel = getTopics(ct, topRelTopic)

      val publisher = newsProfile.getPublisher
      val publishTime = newsProfile.getPublishTime
      val groupId = newsProfile.getGroupId
      val titleMd5 = newsProfile.getTitleMd5
      //val updateTime = newsProfile.getUpdateTime
      val tier  = newsProfile.getTier
      val wordCount = newsProfile.getWordCount
      val imageCount = newsProfile.getImageCount
      val newsyScore = newsProfile.getNewsyScore
      val commentCount = newsProfile.getCommentCount
      val mediallevel = newsProfile.getMediaLevel

      val feature = EventFeature()
      feature.addFeature("D_KEYWORD", ckString)
      feature.addFeature("D_KEYWORD_REL", ckRel)
      feature.addFeature("D_CATEGORY", ccString)
      feature.addFeature("D_CATEGORY_REL", ccRel)
      feature.addFeature("D_TOPIC", ctString)
      feature.addFeature("D_TOPIC_REL", ctRel)


      feature.addFeature("D_PUBLISHER", publisher)
      feature.addFeature("C_PUBLISHTIME", publishTime.toString)
      feature.addFeature("D_GROUPID", groupId)
      feature.addFeature("D_TITLEMD5", titleMd5)
      feature.addFeature("D_TIER", tier)
      feature.addFeature("D_WORDCOUNT", wordCount.toString)
      feature.addFeature("D_IMAGECOUNT", imageCount.toString)
      feature.addFeature("D_NEWSYSCORE", newsyScore.toString)
      feature.addFeature("D_COMMENTCNT", commentCount.toString)
      feature.addFeature("D_MEDIALEVEL", mediallevel.toString)


      feature.genFeatureString()
      //ccString + fieldDelimiter + ckString + fieldDelimiter + publisher + fieldDelimiter + publishTime + fieldDelimiter + groupId + fieldDelimiter + titleMd5
    } else {
      ""
    }
  }

  /**
    *
    * @param batchEvent (contentId, (uid, batchId, act, value, eventFeature))
    * @param contentFeatureConfig cf config
    * @return (uid, (cid, batchId, act, value, eventFeature, contentFeature))
    */
  def getContentFeatureFromRedis(batchEvent: DStream[(String, (String, Long, String, Double, String))], contentFeatureConfig: Config) = {
    val host:String = contentFeatureConfig.getString("redis_host")
    val port = contentFeatureConfig.getInt("redis_port")
    val topRelCat = Try(contentFeatureConfig.getInt("rel_d_ncat")).getOrElse(100)
    val topRelKw = Try(contentFeatureConfig.getInt("rel_d_nkey")).getOrElse(100)
    val topRelTopic = Try(contentFeatureConfig.getInt("rel_d_ntopic")).getOrElse(100)
    val topCategories = Try(contentFeatureConfig.getInt("top_categories")).getOrElse(5)
    val topKeywords = Try(contentFeatureConfig.getInt("top_keywords")).getOrElse(15)
    val topTopics= Try(contentFeatureConfig.getInt("top_topics")).getOrElse(15)
    val categoryVersion = Try(contentFeatureConfig.getString("category_version")).getOrElse("v4")
    val keywordVersion = Try(contentFeatureConfig.getString("keyword_version")).getOrElse("v2")
    val topicVersion = Try(contentFeatureConfig.getString("topic_version")).getOrElse("v2")
    val auth = contentFeatureConfig.getString("redis_password")
    var partitionNum = 1
    batchEvent.mapPartitions(events => {
      val redisHost = if (host.split(",").length > 1) {
        val redisHostList = host.split(",")
        redisHostList(partitionNum % redisHostList.length)
      } else {
        host
      }
      partitionNum += 1
      val redisClient = new Jedis(redisHost, port)
      if (auth.nonEmpty) redisClient.auth(auth)
      val result = events.map(event => {
        (event._2._1, (event._1, event._2._2, event._2._3, event._2._4, event._2._5,
          getContentFeature(event._1, redisClient, topRelCat, topRelKw,topRelTopic, topCategories, topKeywords,topTopics, categoryVersion, keywordVersion,topicVersion)))
      })
      //redisClient.disconnect()
      redisClient.close()
      result
    })

  }

  def getCPFromRedisCluster(batchEvent: DStream[(String, (String, Long, String, Double, String))], contentFeatureConfig: Config) = {
    val host:String = contentFeatureConfig.getString("redis_host")
    val port = contentFeatureConfig.getInt("redis_port")
    val topRelCat = Try(contentFeatureConfig.getInt("rel_d_ncat")).getOrElse(100)
    val topRelKw = Try(contentFeatureConfig.getInt("rel_d_nkey")).getOrElse(100)
    val topRelTopic = Try(contentFeatureConfig.getInt("rel_d_ntopic")).getOrElse(100)
    val topCategories = Try(contentFeatureConfig.getInt("top_categories")).getOrElse(5)
    val topKeywords = Try(contentFeatureConfig.getInt("top_keywords")).getOrElse(15)
    val topTopics = Try(contentFeatureConfig.getInt("top_topics")).getOrElse(15)
    val categoryVersion = Try(contentFeatureConfig.getString("category_version")).getOrElse("v4")
    val keywordVersion = Try(contentFeatureConfig.getString("keyword_version")).getOrElse("v2")
    val topicVersion = Try(contentFeatureConfig.getString("topic_version")).getOrElse("v1")
    //val auth = contentFeatureConfig.getString("redis_password")
    val nodes = host.split(",").distinct.map {h => new HostAndPort(h, port)}.toSet
    val cl = new JedisCluster(nodes)
    val cpFeat = batchEvent.mapPartitions(events => {
      events.map(event => {
        (event._2._1, (event._1, event._2._2, event._2._3, event._2._4, event._2._5,
          contentFeature(event._1, cl, topRelCat, topRelKw,topRelTopic, topCategories, topKeywords, topTopics,categoryVersion, keywordVersion,topicVersion)))
      })
    })
    cl.close()
    cpFeat
  }

  def contentFeature(news_id: String,
                     cl: JedisCluster,
                     topRelCat: Int,
                     topRelKw: Int,
                     topRelTopic: Int,
                     topCategories: Int,
                     topKeywords: Int,
                     topTopics:Int,
                     categoryVersion: String,
                     keywordVersion: String,
                     topicVersion: String) = {
    val cf = cl.get(("n_" + news_id).getBytes())

    if (cf != null) {
      val newsProfile = CmnewsNewsProfile.NewsProfile.parseFrom(cf)
      val cc = if(categoryVersion.length <= 0){
        newsProfile.getCategoriesList.toList
      } else {
        var ccList = List[CategoryProfile]()
        for (category <- newsProfile.getVcategoriesList) {
          if (category.getVersion.equals(categoryVersion)) {
            ccList = category.getCategoriesList.toList
          }
        }
        ccList
      }
      val ccString = getCategories(cc, topCategories)
      val ccRel = getCategories(cc,topRelCat)

      val ck = if(keywordVersion.length <= 0){
        newsProfile.getKeywordsList.toList
      } else {
        var ckList = List[KeywordProfile]()
        for (keyword <- newsProfile.getVkeywordsList) {
          if (keyword.getVersion.equals(keywordVersion)) {
            ckList = keyword.getKeywordsList.toList
          }
        }
        ckList
      }
      val ckString = getKeywords(ck, topKeywords)
      val ckRel = getKeywords(ck, topRelKw)


      val ct = if(topicVersion.length <= 0){
        List[TopicProfile]()
      } else {
        var ctList = List[TopicProfile]()
        for (topic <- newsProfile.getVtopicsList) {
          if (topic.getVersion.equals(topicVersion)) {
            ctList = topic.getTopicsList.toList
          }
        }
        ctList
      }
      val ctString = getTopics(ct, topTopics)
      val ctRel = getTopics(ct, topRelTopic)


      val publisher = newsProfile.getPublisher
      val publishTime = newsProfile.getPublishTime
      val groupId = newsProfile.getGroupId
      val titleMd5 = newsProfile.getTitleMd5
      //val updateTime = newsProfile.getUpdateTime
      val tier  = newsProfile.getTier
      val wordCount = newsProfile.getWordCount
      val imageCount = newsProfile.getImageCount
      val newsyScore = newsProfile.getNewsyScore
      val commentCount = newsProfile.getCommentCount
      val mediallevel = newsProfile.getMediaLevel

      val feature = EventFeature()
      feature.addFeature("D_KEYWORD", ckString)
      feature.addFeature("D_KEYWORD_REL", ckRel)
      feature.addFeature("D_CATEGORY", ccString)
      feature.addFeature("D_CATEGORY_REL", ccRel)
      feature.addFeature("D_TOPIC", ctString)
      feature.addFeature("D_TOPIC_REL", ctRel)
      feature.addFeature("D_PUBLISHER", publisher)
      feature.addFeature("C_PUBLISHTIME", publishTime.toString)
      feature.addFeature("D_GROUPID", groupId)
      feature.addFeature("D_TITLEMD5", titleMd5)
      feature.addFeature("D_TIER", tier)
      feature.addFeature("D_WORDCOUNT", wordCount.toString)
      feature.addFeature("D_IMAGECOUNT", imageCount.toString)
      feature.addFeature("D_NEWSYSCORE", newsyScore.toString)
      feature.addFeature("D_COMMENTCNT", commentCount.toString)
      feature.addFeature("D_MEDIALEVEL", mediallevel.toString)

      feature.genFeatureString()
    } else {
      ""
    }
  }

}
