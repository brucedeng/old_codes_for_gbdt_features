package com.cmcm.news.feature

import com.typesafe.config.Config
import org.apache.spark.streaming.dstream.DStream

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.Try

/**
  * Created by wangwei5 on 15/8/14.
  */
object CFBFeature extends Serializable {

  val fieldDelimiter = "\001"
  val pairDelimiter = "\002"
  val keyValueDelimiter = "\003"

  /**
    *
    * @param fullFeature      ((uid, contentId), (batchId, show, click, eventFeature, contentFeature, userFeature))
    * @param cfbFeatureConfig config
    * @return (cfbFeature, batchId, show, click)
    */
  def generateCFBFeatureDstream(fullFeature: DStream[((String, String), (Long, Double, Double, String, String, Option[String]))], cfbFeatureConfig: Config): DStream[(String, (Long, Double, Double, String))] = {
    fullFeature.flatMap(feature => generateCFBFeatures(feature, cfbFeatureConfig))
  }

  def getMatchCFBFeatures(cfbFeatures: ListBuffer[(String, (Long, Double, Double, String))], matchFeatureConfig: String, featureMap: mutable.HashMap[String, List[(String, Double)]], tuple: (Long, Double, Double)) = {
    for (feature <- matchFeatureConfig.split(",")) {
      val fields = feature.split("_")
      val preFeature = fields(1) + "_" + fields(3)
      val posFeature = fields(2) + "_" + fields(4)
      for (value1 <- featureMap.getOrElse(preFeature,List())) {
        for (value2 <- featureMap.getOrElse(posFeature,List())) {
          if (value1._1.equals(value2._1)) {
            cfbFeatures += ((feature + fieldDelimiter + value1._1, (tuple._1, tuple._2 * value1._2 * value2._2, tuple._3 * value1._2 * value2._2,"")))
          }
        }
      }
    }
  }

  def getFeatureValueRecursive(featureMap: mutable.HashMap[String, List[(String, Double)]], featureList: List[String]): List[(String, Double)] = {
    var featureValues = new ListBuffer[(String, Double)]
    if (featureList.size > 1) {
      val preFeatures = featureMap.getOrElse(featureList(0),List())
      val newFeatureList = featureList.drop(1)
      for (preFeature <- preFeatures) {
        for (posFeature <- getFeatureValueRecursive(featureMap, newFeatureList)) {
          featureValues += ((preFeature._1 + pairDelimiter + posFeature._1, preFeature._2 * posFeature._2))
        }
      }
    } else {
      for (preFeature <- featureMap.getOrElse(featureList(0),List())) {
        featureValues += (preFeature)
      }
    }
    featureValues.toList
  }

  def getCrossCFBFeatures(cfbFeatures: ListBuffer[(String, (Long, Double, Double, String))], crossFeatureConfig: String, featureMap: mutable.HashMap[String, List[(String, Double)]], tuple: (Long, Double, Double)) = {
    for (feature <- crossFeatureConfig.split(",")) {
      val fields = feature.split("_")
      var featureList = new ListBuffer[String]
      var i = 1
      while (i <= (fields.length / 2)) {
        featureList += (fields(i) + "_" + fields(i + 2))
        i = i + 1
      }
      for (value <- getFeatureValueRecursive(featureMap, featureList.toList)) {
        cfbFeatures += ((feature + fieldDelimiter + value._1, (tuple._1, tuple._2 * value._2, tuple._3 * value._2,"")))
      }
    }
  }

  def getNewCrossCFBFeatures(cfbFeatures: ListBuffer[(String, (Long, Double, Double, String))], newCrossFeatureConfig: String, featureMap: mutable.HashMap[String, List[(String, Double)]], tuple: (Long, Double, Double)) = {
    if (featureMap.get("NEW_USER").isDefined && featureMap.get("NEW_USER").get(0)._1.equals("1")) {
      for (feature <- newCrossFeatureConfig.split(",")) {
        val fields = feature.split("_")
        var featureList = new ListBuffer[String]
        var i = 1
        while (i <= (fields.length / 2)) {
          featureList += (fields(i) + "_" + fields(i + 2))
          i = i + 1
        }
        for (value <- getFeatureValueRecursive(featureMap, featureList.toList)) {
          cfbFeatures += ((feature + fieldDelimiter + value._1, (tuple._1, tuple._2 * value._2, tuple._3 * value._2, "")))
        }
      }
    }
  }

  def getSingleEdgeCFBFeatures(cfbFeatures: ListBuffer[(String, (Long, Double, Double, String))], featureConfig: String, featureMap: mutable.HashMap[String, List[(String, Double)]], tuple: (Long, Double, Double)) = {
    for (feature <- featureConfig.split(",")) {
      val values = featureMap.get(feature).getOrElse(List());
      for (value <- values) {
        cfbFeatures += ((feature + fieldDelimiter + value._1, (tuple._1, tuple._2 * value._2, tuple._3 * value._2, "")))
      }
    }
  }

  /**
    *
    * @param feature          ((uid, contentId), (batchId, show, click, eventFeature, contentFeature, userFeature))
    * @param cfbFeatureConfig config
    * @return List[(cfbFeature, batchId, show, click)]
    */
  def generateCFBFeatures(feature: ((String, String), (Long, Double, Double, String, String, Option[String])), cfbFeatureConfig: Config) = {
    var featureMap = new mutable.HashMap[String, List[(String, Double)]]
    val uid = feature._1._1.toString
    featureMap.put("U_UID", List((uid, 1.0)))
    val userFeatures = feature._2._6.getOrElse("")
    if (userFeatures.length > 0) {
      val fields = userFeatures.split(fieldDelimiter)
      if (fields.nonEmpty && fields(0).length > 0) {
        featureMap.put("U_CATEGORY", fields(0).split(pairDelimiter).map(record => {
          val nameAndWeight = record.split(keyValueDelimiter)
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        }
        ).toList)
      }
      if (fields.size > 1 && fields(1).length > 0) {
        featureMap.put("U_KEYWORD", fields(1).split(pairDelimiter).map(record => {
          val nameAndWeight = record.split(keyValueDelimiter)
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        }).toList)
      }
      if (fields.size > 1 && fields(1).length > 0) {
        featureMap.put("U_TOPIC", fields(1).split(pairDelimiter).map(record => {
          val nameAndWeight = record.split(keyValueDelimiter)
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        }).toList)
      }
      if (fields.size > 2 && fields(2).length > 0) {
        featureMap.put("U_GENDER", List((fields(2), 1.0)))
      }
      if (fields.size > 3 && fields(3).length > 0) {
        featureMap.put("U_AGE", List((fields(3), 1.0)))
      }
    }

    val expiredTime = Try(cfbFeatureConfig.getLong("expired_time")).getOrElse(172800L)
    val contentFeatures = feature._2._5
    if (contentFeatures.length > 0) {
      val fields = contentFeatures.split(fieldDelimiter)
      if (fields.nonEmpty && fields(0).length > 0) {
        featureMap.put("D_CATEGORY", fields(0).split(pairDelimiter).map(record => {
          val nameAndWeight = record.split(keyValueDelimiter)
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        }
        ).toList)
      }
      if (fields.size > 1 && fields(1).length > 0) {
        featureMap.put("D_KEYWORD", fields(1).split(pairDelimiter).map(record => {
          val nameAndWeight = record.split(keyValueDelimiter)
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        }
        ).toList)
      }
      if (fields.size > 1 && fields(1).length > 0) {
        featureMap.put("D_TOPIC", fields(1).split(pairDelimiter).map(record => {
          val nameAndWeight = record.split(keyValueDelimiter)
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        }
        ).toList)
      }


      if (fields.size > 2 && fields(2).length > 0) {
        featureMap.put("D_PUBLISHER", List((fields(2), 1.0)))
      }
      if (fields.size > 3 && fields(3).length > 0) {
        val publishTime = fields(3).toLong
        if ((System.currentTimeMillis() / 1000 - publishTime) > expiredTime) {
          featureMap.put("D_CONID", List())
        } else {
          featureMap.put("D_CONID", List((feature._1._2, 1.0)))
        }
      }
      if (fields.size > 4 && fields(4).length > 0) {
        featureMap.put("D_GROUPID", List((fields(4), 1.0)))
      }
      if (fields.size > 5 && fields(5).length > 0) {
        featureMap.put("D_TIER", List((fields(5), 1.0)))
      }
    } else {
      featureMap.put("D_CONID", List())
    }

    val eventFeatures = feature._2._4
    if (eventFeatures.length > 0) {
      eventFeatures.split(pairDelimiter).foreach(pair => {
        val keyValue = pair.split(keyValueDelimiter)
        if (keyValue.length == 2)
          featureMap.put(keyValue(0), List((keyValue(1), 1.0)))
      })
    }

    val batchId: Long = feature._2._1
    val view: Double = feature._2._2
    val click: Double = feature._2._3

    var cfbFeatures = new ListBuffer[(String, (Long, Double, Double, String))]
    val userFeatureConfig = cfbFeatureConfig.getString("uf")
    val docFeatureConfig = cfbFeatureConfig.getString("df")
    val crossFeatureConfig = cfbFeatureConfig.getString("cf")
    val matchFeatureConfig = cfbFeatureConfig.getString("mf")
    val newCrossFeatureConfig = Try(cfbFeatureConfig.getString("ncf")).getOrElse("")

    if (userFeatureConfig.length > 0) getSingleEdgeCFBFeatures(cfbFeatures, userFeatureConfig, featureMap, (batchId, view, click))
    if (docFeatureConfig.length > 0) getSingleEdgeCFBFeatures(cfbFeatures, docFeatureConfig, featureMap, (batchId, view, click))
    if (crossFeatureConfig.length > 0) getCrossCFBFeatures(cfbFeatures, crossFeatureConfig, featureMap, (batchId, view, click))
    if (matchFeatureConfig.length > 0) getMatchCFBFeatures(cfbFeatures, matchFeatureConfig, featureMap, (batchId, view, click))
    if (newCrossFeatureConfig.length > 0) getNewCrossCFBFeatures(cfbFeatures, newCrossFeatureConfig, featureMap, (batchId, view, click))

    cfbFeatures.toList
  }

}
