package com.cmcm.news.app

import com.cmcm.news.component.ModelComponent
import com.cmcm.news.config.Configurable
import com.cmcm.news.spark.SparkComponent

import scala.collection.mutable

/**
  * Created by lilonghua on 16/8/25.
  */
trait DLApp {
  this: Configurable with SparkComponent with ModelComponent =>
  case class Config(cp: String = "")

  def defineParser() = {
    val parser = new scopt.immutable.OptionParser[Config]("DL@DataHero", "0.1") {
      def options = mutable.Seq(
        opt("c", "config", "config folder") { (v: String, c: Config) => c.copy(cp = v) }
      )
    }
    parser
  }

  def initApp(configDir: String) = {
    init(configDir)
  }

  def start() = {
    if (isNewContext) {
      logInfo("APP will start with a new context.")
      workflowSetup()
    } else {
      logInfo("App will start with a context loaded from checkpoint.")
    }

    logInfo("Prestarting...")
    preStart()
    logInfo("The App start to run.")
    streamingContext.start()
    streamingContext.awaitTermination()
  }

  def main(args: Array[String]) {
    val parser = defineParser()
    val configDir = parser.parse(args, Config()) match {
      case Some(config) => {
        val configDir = config.cp
        logInfo(s"DL App will load configures from $configDir")
        configDir
      }
      case None => ""
    }
    initApp(configDir)
    start()
  }
}
