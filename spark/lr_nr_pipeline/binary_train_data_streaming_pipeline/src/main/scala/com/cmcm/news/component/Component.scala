package com.cmcm.news.component

import com.cmcm.news.config.{ConfigUnit, Configurable}

/**
  * Created by hanbin on 15/9/2.
  * There will be multiple componets that will cooperate to implement
  */
trait Component { this: Configurable =>

  def init(configDir: String) = {}
  def preStart() = {}

  def loadComponentConfig(name: String, eventConfigFile: String): ConfigUnit = {
    logInfo(s"Loading $name component config from $eventConfigFile")
    val componentConfigUnit = registComponent(name, eventConfigFile)
    if (componentConfigUnit.isEmpty) {
      logError(s"Can not init $name component from $eventConfigFile")
      throw new IllegalStateException(s"Can not init $name component from $eventConfigFile")
    }
    componentConfigUnit.get
  }

}
