package com.cmcm.news.feature

import java.net.URI

import com.cmcm.news.util.{JsonUtil, Utils}
import com.typesafe.config.Config
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path, PathFilter}
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.{HashPartitioner, Logging, SparkContext}
import org.json4s.JsonAST._

import scala.util.{Failure, Success, Try}
import scala.util.control.Breaks._

/**
  *  Created by wangwei5 on 15/8/12.
  */

case class UPNotReadyException(msg: String) extends Throwable {
  override def getMessage: String = "UP load error : " + msg
}

object UserFeature extends Logging {

  def jsonLineParser(line: String,
                     topRelCat: Int,
                     topRelKw: Int,
                     topRelTopic: Int,
                     topCategories: Int,
                     topKeywords: Int,
                     topTopics: Int,
                     categoryKey: String, keywordKey: String,topicKey:String): (String, String) = {

    Try(JsonUtil.convertToJValue(line)) match {
      case Success(jvalue) =>
        val uuid = extractStr(jvalue, "uid", "")
        val categoriesRel = jvalue \ categoryKey match {
          case JArray(cats) => catKwAssist(cats.toList, "", topRelCat, 0.0)
          case _ => "unknown"
        }
        val categories = topCatOrKw(categoriesRel, Utils.pairGap, topCategories)
        val keywordsRel = jvalue \ keywordKey match {
          case JArray(cats) => catKwAssist(cats.toList, "", topRelKw, 0.0)
          case _ => "unknown"
        }
        val keywords = topCatOrKw(keywordsRel, Utils.pairGap, topKeywords)

        val topicsRel = jvalue \ topicKey match {
          case JArray(cats) => catKwAssist(cats.toList, "", topRelTopic, 0.0)
          case _ => "unknown"
        }
        val topics = topCatOrKw(topicsRel, Utils.pairGap, topTopics)

        val gender = extractStr(jvalue, "gender", "unknown")
        val age = extractStr(jvalue, "age", "unknown")
        val catLen = extractStr(jvalue, "u_cat_len", "0")
        val kwLen = extractStr(jvalue, "u_kw_len", "0")
        val feature = EventFeature()
        feature.addFeature("U_AGE",age)
        feature.addFeature("U_GENDER",gender)
        feature.addFeature("U_CATEGORY",categories)
        feature.addFeature("U_CATEGORY_REL", categoriesRel)
        feature.addFeature("U_KEYWORD",keywords)
        feature.addFeature("U_KEYWORD_REL", keywordsRel)
        feature.addFeature("U_TOPIC",topics)
        feature.addFeature("U_TOPIC_REL", topicsRel)

        feature.addFeature("U_CATLEN",catLen)
        feature.addFeature("U_KWLEN", kwLen)
        (uuid, feature.genFeatureString())
      case Failure(ex) =>
        logInfo(ex.getMessage, ex)
        ("", "")

    }
  }

  def checkDirSuccess(upConfig: Config, date: String): Boolean = {
    Try(dirContainsSuccess(upConfig.getString("input_dir") + "/" + date)).getOrElse(false)
  }

  def dirContainsSuccess(hdfsDir: String): Boolean = {
    val hdfs = FileSystem.get(URI.create(hdfsDir), new Configuration())
    val fileNameList = hdfs.listStatus(new Path(hdfsDir)).map(filePath => filePath.getPath.getName).toList
    //fileNameList.foreach(println(_))
    //fileNameList.exists(fileName => fileName.equals("_SUCCESS"))
    /*def flag = fileNameList.contains("_SUCCESS")
    logInfo(if (flag) "find up file ...." else "not found !!!!")
    flag*/
    fileNameList.contains("_SUCCESS")
  }

  def loadJsonFeature(upConfig: Config, date: String, sc: SparkContext): RDD[(String, String)] =  {
    val upPath = upConfig.getString("input_dir") + "/" + date
    if (!dirContainsSuccess(upPath)) {
      throw UPNotReadyException(upPath + " not ready")
    }
    val categoryKey = Try(upConfig.getString("category_key")).getOrElse("categories")
    val keywordKey = Try(upConfig.getString("keyword_key")).getOrElse("keywords")
    val topicKey = Try(upConfig.getString("topic_key")).getOrElse("topics")

    val hdfs = FileSystem.get(URI.create(upPath),new Configuration())
    val files: Array[String] = hdfs.listStatus(new Path(upPath), new PathFilter() {
      override def accept(path: Path): Boolean = {
        (! path.toString.contains("_SUCCESS")) && (! path.toString.contains("_temporary"))
      }
    }).map(status => status.getPath.toString)

    val userProifleLines: RDD[String] = sc.textFile(files.mkString(","))
    val topRelCat = Try(upConfig.getInt("rel_u_ncat")).getOrElse(100)
    val topRelKw = Try(upConfig.getInt("rel_u_nkey")).getOrElse(100)
    val topRelTopic = Try(upConfig.getInt("rel_u_ntopic")).getOrElse(100)
    val top_categories = upConfig.getInt("top_categories")
    val top_keywords = upConfig.getInt("top_keywords")
    val top_topics = upConfig.getInt("top_topics")

    logInfo(s"User feature category key is $categoryKey\ttopRelCat is $topRelCat\ttop_categories is $top_categories")
    logInfo(s"User feature keyword key is $keywordKey\ttopRelKw is $topRelKw\ttop_keywords is $top_keywords")
    logInfo(s"User feature topic key is $keywordKey\ttopRelTopic is $topRelTopic\ttop_topics is $top_topics")

    val userFeature = userProifleLines.map(line => {
      UserFeature.jsonLineParser(line, topRelCat, topRelKw, topRelTopic,top_categories, top_keywords,top_topics, categoryKey, keywordKey,topicKey)
    }).filter(_._1.length > 0)
    userFeature.partitionBy(new HashPartitioner(upConfig.getInt("partitions")))
  }

  /**
    *
    * @param getUserFeatureRDD Option[(dateString, UserProfile)]
    * @param batchEvent (uid, (contentId, batchId, act, value, eventFeature, contentFeature))
    * @return ((uid, contentId), (batchId, act, value, eventFeature, contentFeature, userFeature))
    */
  def joinWithBatchEvent(getUserFeatureRDD: () =>Option[(String, RDD[(String, String)])],
                         batchEvent: DStream[(String, (String, Long, String, Double, String, String))]): DStream[((String, String), (Long, String, Double, String, String, Option[String]))] = {
    batchEvent.transform(eventRDD => {
      val dateAndUP = getUserFeatureRDD().get
      val dateString = dateAndUP._1
      val up = dateAndUP._2
      logInfo(s"Now, use user profile of $dateString")
      eventRDD.leftOuterJoin(up).map(result =>
        ((result._1, result._2._1._1), (result._2._1._2, result._2._1._3, result._2._1._4, result._2._1._5, result._2._1._6, result._2._2))
      )
    })
  }


  /**
    *
    * @param getUserFeatureRDD Option[(dateString, UserProfile)]
    * @param batchEvent (uid, (contentId, batchId, act, value, eventFeature, contentFeature))
    * @return ((uid, contentId), (batchId, act, value, eventFeature, contentFeature, userFeature))
    */
  def joinWithBatchEventCN(getUserFeatureRDD: () =>Option[(String, RDD[(String, String)])],
                           batchEvent: DStream[(String, (String, Long, Double, Double, String, String))]): DStream[((String, String), (Long, Double, Double, String, String, Option[String]))] = {
    batchEvent.transform(eventRDD => {
      val dateAndUP = getUserFeatureRDD().get
      val dateString = dateAndUP._1
      val up = dateAndUP._2
      logInfo(s"Now, use user profile of $dateString")
      eventRDD.leftOuterJoin(up).map(result =>
        ((result._1, result._2._1._1), (result._2._1._2, result._2._1._3, result._2._1._4, result._2._1._5, result._2._1._6, result._2._2))
      )
    })
  }

  def topCatOrKw(str: String, gap: String, top: Int) = {
    val lh = Try(str.split(gap).length).getOrElse(0)
    Try(str.split(gap).take(if (lh < top) lh else top).mkString(gap)).getOrElse("")
  }

  def extractStr(jvalue: JValue, fieldName: String, default: String = "") = {
    jvalue \ fieldName match {
      case JString(s) => if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString()
      case _ => default
    }
  }

  def catKwAssist(jsonItems: List[JValue], version: String, top: Int, default: Double): String = {
    def parse_item(item: JValue) = {
      val name = (item \ "name").asInstanceOf[JString].s
      val L1_weight = item \ "weight" match {
        case JDouble(x) =>
          x.toDouble
        case JInt(x) =>
          x.toDouble
        case JString(x) =>
          Try(x.toDouble).getOrElse(default)
        case _ =>
          default
      }
      name + Utils.keyGap + L1_weight
    }
    jsonItems.map(x => parse_item(x)).take(top).mkString(Utils.pairGap)
  }


  def getListFromJson(key: String ,line: String, top: Int) : String = {
    val sb = new StringBuilder
    var newLine = line.substring(line.indexOf("\"" + key + "\" : ") + 5 + key.length)
    newLine = newLine.substring(0, newLine.indexOf("]") + 1)
    var index = 0
    breakable {
      while (index < (if (top < 0) Integer.MAX_VALUE else top)) {
        if (newLine.indexOf("}") == -1) {
          break()
        }
        val name = getValueForKey("name", newLine)
        val weight = getValueForKey("weight", newLine).toDouble
        sb.append(name + Utils.keyGap + weight + Utils.pairGap)
        newLine = newLine.substring(newLine.indexOf("}") + 1)
        index += 1
      }
    }
    if (index > 0)
      sb.dropRight(1).toString()
    else
      sb.toString()
  }

  def getValueForKey(key: String, record: String) = {
    val startIndex = record.indexOf("\"" + key + "\" : " ) + 5 + key.length
    val commaIndex = record.indexOf(",", startIndex)
    val antiBracketIndex = record.indexOf("}", startIndex)
    val endIndex = if (commaIndex != -1 && antiBracketIndex != -1)  Math.min(record.indexOf(",", startIndex),
      record.indexOf("}", startIndex)) else if (commaIndex != -1) commaIndex else antiBracketIndex
    val mendedStartInedex =
      if (record.charAt(startIndex) == '\"' || record.charAt(startIndex) == '[')
        startIndex + 1
      else
        startIndex
    val mendedEndInedex =
      if (record.charAt(endIndex - 1) == '\"' || record.charAt(endIndex - 1) == ']')
        endIndex - 1
      else
        endIndex

    if (mendedStartInedex > mendedEndInedex)
      ""
    else
      record.substring(mendedStartInedex, mendedEndInedex)
  }

}
