package com.cmcm.news.feature

import com.cmcm.news.util.{JsonUtil, Utils}
import com.typesafe.config.Config
import kafka.utils.Json
import org.apache.spark.streaming.dstream.DStream
import org.joda.time.{DateTime, DateTimeZone}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.{Success, Try}

/**
  * Created by lilonghua on 16/8/30.
  */
object LRBFeature extends Serializable {

  /**
    *
    * @param fullFeature      ((uid, contentId), (batchId, label, dwell, eventFeature, contentFeature, userFeature))
    * @param cfbFeatureConfig config
    * @return ((batchId,pid), (uid, cid, label, dwell, featureMap))
    */
  def generateLRBFeatureDstream(fullFeature: DStream[((String, String), (Long, String, Double, String, String, Option[String]))],
                                cfbFeatureConfig: Config): DStream[((Long,String), (String, String, String, Double, String))] = {
    /*val feats = */fullFeature.filter(feat => {
      val contentFeature = EventFeature(feat._2._5)
      val wordCount = contentFeature.getFeatureValue("D_WORDCOUNT").getOrElse("0").toDouble
      val trunk = Try(cfbFeatureConfig.getDouble("D_WORDCOUNT")).getOrElse(10000.0d)
      wordCount < trunk
    }).map(feature => genBinaryStaticFeature(feature, cfbFeatureConfig))

    //calculateWeight(feats)
  }

  /**
    *
    * @param matchFeatureConfig list of feature should be generated. "M_D_U_KEYWORD_KEYWORD_B90"
    * @param featureMap         (U_KEYWORD, [(CAT,1.0),(DOG,1.0)])
    * @param binaryFeatures         (batchId, act, value)
    */
  def getMatchBinaryFeatures(matchFeatureConfig: List[String],
                             featureMap: mutable.Map[String, List[(String, Double)]],
                             binaryFeatures: ListBuffer[(String, String)]) = {
    matchFeatureConfig.foreach(feature => {
      val fields = feature.split("_")
      val preFeature = fields(1) + "_" + fields(3)
      val posFeature = fields(2) + "_" + fields(4)
      val featureId = fields(5)

      featureMap.getOrElse(preFeature,List()).foreach(value1 => {
        featureMap.getOrElse(posFeature,List()).foreach(value2 => {
          if (value1._1.equals(value2._1)) {
            binaryFeatures += ((featureId + "_" + value1._1 + "_" + value1._1, "%.3f".format(value1._2 * value2._2)))
          }
        })
      })
    })
  }

  /**
    *
    * @param crossFeatureConfig list of feature should be generated. "C_D_U_CATEGORY_CATEGORY_B50"
    * @param featureMap         (U_KEYWORD, [(CAT,1.0),(DOG,1.0)])
    * @param binaryFeatures         (batchId, act, value)
    */
  def getCrossBinaryFeatures(crossFeatureConfig: List[String],
                             featureMap: mutable.Map[String, List[(String, Double)]],
                             binaryFeatures: ListBuffer[(String, String)]) = {

    crossFeatureConfig.foreach(feature => {
      var featureList = new ListBuffer[String]
      val fields = feature.split("_")
      var i = 1
      val catNameIndex = fields.length/2
      while(i <= ((fields.length-1)/2)){
        featureList += (fields(i) + "_" + fields(i+catNameIndex-1))
        i = i + 1
      }
      val featureId = fields(fields.length-1)
      getBinaryFeatureValueRecursive(featureMap, featureList.toList).foreach(value => {
        binaryFeatures += ((featureId + "_" + value._1, "%.3f".format(value._2)))
      })
    })
  }

  def getBinaryFeatureValueRecursive(featureMap:mutable.Map[String, List[(String, Double)]], featureList: List[String]) : List[(String, Double)] = {
    var featureValues = new ListBuffer[(String, Double)]
    if (featureList.size > 1) {
      val preFeatures = featureMap.getOrElse(featureList.head,List())
      val newFeatureList = featureList.drop(1)
      for (preFeature <- preFeatures) {
        for (posFeature <- getBinaryFeatureValueRecursive(featureMap, newFeatureList)) {
          featureValues += ((preFeature._1 + "_" + posFeature._1, preFeature._2 * posFeature._2))
        }
      }
    } else {
      for (preFeature <- featureMap.getOrElse(featureList.head,List())){
        featureValues += preFeature
      }
    }
    featureValues.toList
  }

  /**
    * @param featureConfig "U_UID_B1"
    * @param featureMap    (U_KEYWORD, [(CAT,1.0),(DOG,1.0)])
    * @param binaryFeatures         (batchId, act, value)
    */
  def getSingleEdgeBinaryFeatures(featureConfig: List[String],
                                  featureMap: mutable.Map[String, List[(String, Double)]],
                                  binaryFeatures: ListBuffer[(String, String)]) = {
    featureConfig.foreach(feature => {
      val fields = feature.split("_")
      val featureName = fields(0) + "_" + fields(1)
      val featureId = fields(2)

      featureMap.getOrElse(featureName,List()).foreach(value => {
        binaryFeatures += ((featureId + "_" + value._1, "%.3f".format(value._2)))
      })
    })
  }

  def getCountryLan(featureMap: mutable.Map[String, List[(String, Double)]]): String = {
    if (featureMap.get("U_COUNTRY").nonEmpty && featureMap.get("U_LAN").nonEmpty)
      featureMap.get("U_COUNTRY").get.head._1 + "_" + featureMap.get("U_LAN").get.head._1
    else ""
  }

  /**
    *
    * @param featureMap          ((batchId,pid), (uid, cid, label, dwell, featureMap))
    * @param cfbFeatureConfig config from feature.conf -> cfb
    * @return ((batchId,pid), (uid, cid, label, dwell, featureMap))
    */
  def generateLRBFeatures(featureMap: ((Long,String), (String, String, String, Double, mutable.HashMap[String, List[(String,Double)]])),
                          cfbFeatureConfig: Config): ((Long,String), (String, String, String, Double, String)) = {

    val binaryFeatures = new ListBuffer[(String, String)]

    val batchId: Long = featureMap._1._1
    val pid = featureMap._1._2
    val uid = featureMap._2._1
    val cid = featureMap._2._2
    val label: String = featureMap._2._3
    val dwell: Double = featureMap._2._4

    setFeatureConfig(cfbFeatureConfig)
      .filter(_._2.nonEmpty)
      .foreach(feature => {
        feature._1 match {
          case "uf" | "df" => getSingleEdgeBinaryFeatures(feature._2.toList, featureMap._2._5, binaryFeatures)
          case "mf" => getMatchBinaryFeatures(feature._2.toList, featureMap._2._5, binaryFeatures)
          case "cf" => getCrossBinaryFeatures(feature._2.toList, featureMap._2._5, binaryFeatures)
          case _ => None
        }
      })

    //((batchId, pid), (uid, cid, label, dwell, Json.encode(binaryFeatures)))
    ((batchId, pid), (uid, cid, label, dwell, binaryFeatures.map(f => f._1.replaceAll("\n"," ").replaceAll(" ","_").trim).mkString(" ")))
  }

  /**
    *
    * @param cfbFeatureConfig config from feature.conf -> cfb
    * @return Sample: CL_PV_U_KEYWORD
    */
  private def setFeatureConfig(cfbFeatureConfig: Config): Map[String, ListBuffer[String]] = {
    val featureConfigMap: Map[String, ListBuffer[String]] = Map(
      "uf" -> ListBuffer[String](),
      "df" -> ListBuffer[String](),
      "cf" -> ListBuffer[String](),
      "mf" -> ListBuffer[String]()
    )

    val coecConf = cfbFeatureConfig.getConfig("coec-feat")
    /*val typeC = coecConf.getString("c")
    val typeEC = coecConf.getString("ec")*/
    featureConfigMap.foreach(feature => {
      feature._2 ++= Try(coecConf.getString(feature._1))
        .getOrElse("")
        .split(",")
        .filter(_.length > 0)
    })

    featureConfigMap
  }

  /**
    *
    * @param feature      ((uid, cid), (batchId, label, dwell, eventFeature, contentFeature, userFeature))
    * @param cfbFeatureConfig config
    * @return ((batchId,pid), (uid, cid, label, dwell, featureMap))
    */
  def genBinaryStaticFeature(feature:((String, String), (Long, String, Double, String, String, Option[String])), cfbFeatureConfig: Config) = {
    val featureMap = new mutable.HashMap[String, List[(String,Double)]]
    val eventFeature = EventFeature(feature._2._4)
    val contentFeature = EventFeature(feature._2._5)
    val userFeature = EventFeature(feature._2._6.getOrElse(""))
    val dCid = feature._1._2
    val uid = feature._1._1
    val ts = eventFeature.getFeatureValue("SERVER_TIME").getOrElse("0").toLong
    val pid = eventFeature.getFeatureValue("PID").getOrElse("11")
    val uCity = eventFeature.getFeatureValue("U_CITY").getOrElse("unknown")
    val uModel = eventFeature.getFeatureValue("U_MODEL").getOrElse("unknown")
    val uBrand = eventFeature.getFeatureValue("U_BRAND").getOrElse("unknown")
    val uPf = eventFeature.getFeatureValue("U_PF").getOrElse("unknown")
    val uNet = eventFeature.getFeatureValue("U_NET").getOrElse("unknown")
    val uIp = eventFeature.getFeatureValue("U_IP").getOrElse("unknown")
    val uOsv = eventFeature.getFeatureValue("U_OSV").getOrElse("unknown")
    val dwell = feature._2._3
    val label = feature._2._2
    val lan = eventFeature.getFeatureValue("U_LAN").getOrElse("unknown")
    val country = eventFeature.getFeatureValue("U_COUNTRY").getOrElse("unknown")
    val lanRegion = lan + "_" + country
    val uRelCat = userFeature.getFeatureValue("U_CATEGORY_REL").getOrElse("unknown")
    val uCat = userFeature.getFeatureValue("U_CATEGORY").getOrElse("unknown")
    val uRelKey = userFeature.getFeatureValue("U_KEYWORD_REL").getOrElse("unknown")
    val uKey = userFeature.getFeatureValue("U_KEYWORD").getOrElse("unknown")
    val uRelTopic = userFeature.getFeatureValue("U_TOPIC_REL").getOrElse("unknown")
    val uTopic = userFeature.getFeatureValue("U_TOPIC").getOrElse("unknown")
    val uGender = userFeature.getFeatureValue("U_GENDER").getOrElse("unknown")
    val uAge = userFeature.getFeatureValue("U_AGE").getOrElse("unknown")
    val uCatLen = userFeature.getFeatureValue("U_CATLEN").getOrElse("unknown")
    val uKeyLen = userFeature.getFeatureValue("U_KWLEN").getOrElse("unknown")
    val dGroupid = contentFeature.getFeatureValue("D_GROUPID").getOrElse("unknown")
    val dTier = contentFeature.getFeatureValue("D_TIER").getOrElse("unknown")
    val dPublisher = contentFeature.getFeatureValue("D_PUBLISHER").getOrElse("unknown")
    val dPublishTime = contentFeature.getFeatureValue("C_PUBLISHTIME").getOrElse("-1").toLong
    val dRelCat = contentFeature.getFeatureValue("D_CATEGORY_REL").getOrElse("unknown")
    val dCat = contentFeature.getFeatureValue("D_CATEGORY").getOrElse("unknown")
    val dRelKey = contentFeature.getFeatureValue("D_KEYWORD_REL").getOrElse("unknown")
    val dKey = contentFeature.getFeatureValue("D_KEYWORD").getOrElse("unknown")
    val dRelTopic = contentFeature.getFeatureValue("D_TOPIC_REL").getOrElse("unknown")
    val dTopic = contentFeature.getFeatureValue("D_TOPIC").getOrElse("unknown")
    val wordCount = contentFeature.getFeatureValue("D_WORDCOUNT").getOrElse("unknown")
    val imageCount = contentFeature.getFeatureValue("D_IMAGECOUNT").getOrElse("unknown")
    val newsyScore = contentFeature.getFeatureValue("D_NEWSYSCORE").getOrElse("unknown")
    val dCommentCnt = contentFeature.getFeatureValue("D_COMMENTCNT").getOrElse("unknown")
    val dmedia_level = contentFeature.getFeatureValue("D_MEDIALEVEL").getOrElse("unknown")
    val dTitlemd5 = contentFeature.getFeatureValue("D_TITLEMD5").getOrElse("unknown")




    val catRel = genRelevance(uRelCat,dRelCat)
    val keyRel = genRelevance(uRelKey,dRelKey)
    val topicRel = genRelevance(uRelTopic,dRelTopic)

    val docAge = Try(math.floor((ts - dPublishTime)/3600.0d).toLong).getOrElse(0l) match {
      case x:Long if x >= 0 => x
      case _ => 0
    }
    val timeTuple = if (ts <= 0) {
      (-1,-1)
    } else{
      val dateD = new DateTime(ts * 1000L).withZone(DateTimeZone.UTC)
      (dateD.getHourOfDay, dateD.getDayOfWeek%7)
    }
    val timeOfDay = timeTuple._1
    val dayOfWeek = timeTuple._2

    if(uCatLen.nonEmpty && uCatLen != "unknown") {
      if (uCatLen == "0") {
        featureMap.put("U_CATLEN", List(("-1",1.0)))
      } else {
        featureMap.put("U_CATLEN", List(("%.0f".format(math.floor(Try(uCatLen.toDouble).getOrElse(-1.0) * 1)),1.0)))
      }
    }else{
      featureMap.put("U_CATLEN", List(("-1",1.0)))
    }
    if(uKeyLen.nonEmpty && uKeyLen != "unknown") {
      if (uKeyLen == "0") {
        featureMap.put("U_KWLEN", List(("0",1.0)))
      } else {
        featureMap.put("U_KWLEN", List(("%.0f".format(math.floor(Try(uKeyLen.toDouble).getOrElse(0.0) * 0.1)),1.0)))
      }
    }else{
      featureMap.put("U_KWLEN", List(("0",1.0)))
    }
    if(wordCount.nonEmpty && wordCount != "unknown") {
      featureMap.put("D_WORDCOUNT", List(("%.0f".format(math.floor(Try(wordCount.toDouble).getOrElse(0.0) * 0.1)),1.0)))
    }else{
      featureMap.put("D_WORDCOUNT", List(("",1.0)))
    }
    if(imageCount.nonEmpty && imageCount != "unknown") {
      featureMap.put("D_IMAGECOUNT", List(("%.0f".format(math.floor(Try(imageCount.toDouble).getOrElse(0.0) * 1)),1.0)))
    }else{
      featureMap.put("D_IMAGECOUNT", List(("",1.0)))
    }
    if(newsyScore.nonEmpty && newsyScore != "unknown") {
      featureMap.put("D_NEWSYSCORE", List(("%.0f".format(math.floor(Try(newsyScore.toDouble).getOrElse(0.0) * 100)),1.0)))
    }else{
      featureMap.put("D_NEWSYSCORE", List(("",1.0)))
    }

    if(dCommentCnt.nonEmpty && dCommentCnt != "unknown") {
      if (dCommentCnt.toDouble >100)  { featureMap.put("D_COMMENTCNT", List(("%.0f".format(5.0),1.0)))}
      else if (dCommentCnt.toDouble >50){ featureMap.put("D_COMMENTCNT", List(("%.0f".format(4.0),1.0)))}
      else if (dCommentCnt.toDouble >10){ featureMap.put("D_COMMENTCNT", List(("%.0f".format(3.0),1.0)))}
      else if (dCommentCnt.toDouble >0){featureMap.put("D_COMMENTCNT", List(("%.0f".format(2.0),1.0)))}
      else {featureMap.put("D_COMMENTCNT", List(("%.0f".format(1.0),1.0)))}
    }else{
      featureMap.put("D_COMMENTCNT", List(("",1.0)))
    }


    if(dmedia_level.nonEmpty && dmedia_level != "unknown") {
      featureMap.put("D_MEDIALEVEL", List(("%.0f".format(dmedia_level.toDouble),1.0)))
    }else{
      featureMap.put("D_MEDIALEVEL", List(("",1.0)))
    }



    if(catRel >= 0) {
      featureMap.put("UD_CATREL", List(("%.0f".format(math.floor(catRel * 1000)),1.0)))
    }else{
      featureMap.put("UD_CATREL", List(("",1.0)))
    }
    if(keyRel >= 0) {
      featureMap.put("UD_KWREL", List(("%.0f".format(math.floor(keyRel * 1000)),1.0)))
    }else{
      featureMap.put("UD_KWREL", List(("",1.0)))
    }
    if(topicRel >= 0) {
      featureMap.put("UD_TOPICREL", List(("%.0f".format(math.floor(topicRel * 1000)),1.0)))
    }else{
      featureMap.put("UD_TOPICREL", List(("",1.0)))
    }

    if(docAge >= 0) {
      featureMap.put("D_DOCAGE", List((docAge.toString,1.0)))
    }else{
      featureMap.put("D_DOCAGE", List(("",1.0)))
    }
    if(timeOfDay >= 0) {
      featureMap.put("E_TIMEOFDAY", List((timeOfDay.toString,1.0)))
    }else{
      featureMap.put("E_TIMEOFDAY", List(("",1.0)))
    }
    if(dayOfWeek >= 0){
      featureMap.put("E_DAYOFWEEK", List((dayOfWeek.toString,1.0)))
    }else{
      featureMap.put("E_DAYOFWEEK", List(("",1.0)))
    }
    if(lanRegion.nonEmpty && lanRegion != "unknown") {
      featureMap.put("E_LANREGION",List((lanRegion.toLowerCase,1.0)))
    }else{
      featureMap.put("E_LANREGION", List(("",1.0)))
    }


    if (uid.nonEmpty && uid != "unknown") {
      featureMap.put("U_UID", List((uid,1.0)))
    }else{
      featureMap.put("U_UID", List(("",1.0)))
    }
    if (uCity.nonEmpty && uCity != "unknown") {
      featureMap.put("U_CITY", List((uCity,1.0)))
    }else{
      featureMap.put("U_CITY", List(("",1.0)))
    }
    if (uModel.nonEmpty && uModel != "unknown") {
      featureMap.put("U_MODEL", List((uModel,1.0)))
    }else{
      featureMap.put("U_MODEL", List(("",1.0)))
    }
    if (uBrand.nonEmpty && uBrand != "unknown") {
      featureMap.put("U_BRAND", List((uBrand,1.0)))
    }else{
      featureMap.put("U_BRAND", List(("",1.0)))
    }
    if (uPf.nonEmpty && uPf!= "unknown") {
      featureMap.put("U_PF", List((uPf,1.0)))
    }else{
      featureMap.put("U_PF", List(("",1.0)))
    }
    if (uNet.nonEmpty && uNet != "unknown") {
      featureMap.put("U_NET", List((uNet,1.0)))
    }else{
      featureMap.put("U_NET", List(("",1.0)))
    }
    if (uIp.nonEmpty && uIp != "unknown") {
      featureMap.put("U_IP", List((uIp,1.0)))
    }else{
      featureMap.put("U_IP", List(("",1.0)))
    }
    if (uOsv.nonEmpty && uOsv != "unknown") {
      featureMap.put("U_OSV", List((uOsv,1.0)))
    }else{
      featureMap.put("U_OSV", List(("",1.0)))
    }
    if (uGender.nonEmpty && uGender != "unknown") {
      featureMap.put("U_GENDER", List((uGender,1.0)))
    }else{
      featureMap.put("U_GENDER", List(("",1.0)))
    }
    if (uAge.nonEmpty && uAge != "unknown") {
      featureMap.put("U_AGE", List((uAge,1.0)))
    }else{
      featureMap.put("U_AGE", List(("",1.0)))
    }
    if (uCat.nonEmpty && uCat != "unknown") {
      featureMap.put("U_CATEGORY", uCat.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("U_CATEGORY", List(("",1.0)))
    }
    if (uKey.nonEmpty && uKey != "unknown") {
      featureMap.put("U_KEYWORD", uKey.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("U_KEYWORD", List(("",1.0)))
    }


    if (uTopic.nonEmpty && uKey != "unknown") {
      featureMap.put("U_TOPIC", uTopic.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("U_TOPIC", List(("",1.0)))
    }


    if (dGroupid.nonEmpty && dGroupid != "unknown") {
      featureMap.put("D_GROUPID", List((dGroupid,1.0)))
    }else{
      featureMap.put("D_GROUPID", List(("",1.0)))
    }
    if (dTier != "" && dTier != "unknown") {
      featureMap.put("D_TIER", List((dTier,1.0)))
    }else{
      featureMap.put("D_TIER", List(("",1.0)))
    }
    if (dTitlemd5.nonEmpty && dTitlemd5 != "unknown") {
      featureMap.put("D_TITLEMD5", List((dTitlemd5,1.0)))
    }else {
      featureMap.put("D_TITLEMD5", List(("",1.0)))
    }
    if (dPublisher.nonEmpty && dPublisher != "unknown") {
      featureMap.put("D_PUBLISHER", List((dPublisher,1.0)))
    }else{
      featureMap.put("D_PUBLISHER", List(("",1.0)))
    }
    if (dCat.nonEmpty && dCat != "unknown") {
      featureMap.put("D_CATEGORY", dCat.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0),nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("D_CATEGORY", List(("",1.0)))
    }
    if (dKey.nonEmpty && dKey != "unknown") {
      featureMap.put("D_KEYWORD", dKey.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("D_KEYWORD", List(("",1.0)))
    }

    if (dTopic.nonEmpty && dTopic != "unknown") {
      featureMap.put("D_TOPIC", dTopic.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("D_TOPIC", List(("",1.0)))
    }

    if (dCid.nonEmpty && dCid != "unknown") {
      featureMap.put("D_CONID", List((dCid,1.0)))
    } else {
      featureMap.put("D_CONID", List(("",1.0)))
    }

    generateLRBFeatures(((feature._2._1, pid), (uid, dCid, label,dwell, featureMap)), cfbFeatureConfig)
  }

  def genRelevance(u_input:String,d_input:String):Double = {
    if (u_input == "" || u_input == "unknown" || d_input == "" || d_input == "unknown") {
      0.0
    } else {
      val u_input_dict = u_input.split(Utils.pairGap).map((line:String) => {
        val items = line.split(Utils.keyGap)
        val name = items(0)
        val value = items(1).toDouble
        (name, value)
      }).toMap

      val rel = d_input.split(Utils.pairGap).map((line:String) => {
        val items = line.split(Utils.keyGap)
        val name = items(0)
        val value = items(1).toDouble
        Try(u_input_dict(name)).getOrElse(0.0) * value
      }).sum
      if(rel.isNaN || rel.isInfinity) 0.0
      else rel
    }
  }

  /**
    *
    * @param baseData  ((batchId,pid), (uid, cid, label, dwell, featureMap))
    * @return ((batchId,pid), (uid, cid, label, dwell, featureMap))
    */
  def calculateWeight(baseData: DStream[((Long,String), (String, String, String, Double, String))]): DStream[((Long,String), (String, String, String, Double, String))] = {

    baseData.transform(rdd=> {
      val (spWt: Double, snWt: Double) = Try(rdd.map(x => {
        val label = x._2._3.toInt
        val wt = x._2._4
        val pos_wt = if (label == 1) wt else 0.0d
        val neg_wt = if (label == 1) 0.0d else 1.0d
        (pos_wt, neg_wt)
      }).reduce((x, y) => (x._1 + y._1, x._2 + y._2))).getOrElse((1d,0d))

      val ratio: Double = math.min(snWt / spWt, 10)

      rdd.map(x=>{
        val wt = x._2._4
        val weight: Double = if (x._2._3.toInt == 1) wt * ratio else 1.0d
        (x._1, (x._2._1, x._2._2, x._2._3, weight, x._2._5))})
    })
  }
}
