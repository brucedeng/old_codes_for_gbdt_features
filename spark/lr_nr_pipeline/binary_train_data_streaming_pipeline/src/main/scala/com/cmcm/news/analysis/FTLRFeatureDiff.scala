package com.cmcm.news.analysis

import com.cmcm.news.component.NormalComponent
import com.cmcm.news.config.Configurable
import com.cmcm.news.spark.SparkNormal
import org.apache.spark.storage.StorageLevel

/**
  * Created by lilonghua on 16/9/22.
  */
trait FTLRFeatureDiff extends NormalComponent {
  this: Configurable with SparkNormal =>

  override def workflowSetup(): Unit = {

    val leftFeat = sparkContext.textFile(modelConfigUnit.getString("model.lr.input.leftFeat", NormalComponent.defaultTenFeatPath))
      .map(f => {
        val ft = f.split("\t")
        (ft(0) + "\t" + ft(1), (ft(2), ft(3)))
      })

    val rightFeat = sparkContext.textFile(modelConfigUnit.getString("model.lr.input.rightFeat", NormalComponent.defaultOneFeatPath))
      .map(f => {
        val ft = f.split("\t")
        (ft(0) + "\t" + ft(1), (ft(2), ft(3)))
      })

    val joinFeat = leftFeat.fullOuterJoin(rightFeat).persist(StorageLevel.MEMORY_AND_DISK_SER)

    joinFeat.filter(f => {
      val l = f._2._1 match {
        case Some(x) => true
        case _ => false
      }

      val r = f._2._2 match {
        case Some(x) => true
        case _ => false
      }

      l && r
    }).map(f => {
      val lF = f._2._1.getOrElse(("",""))
      val rF = f._2._2.getOrElse(("",""))
      val lFeat = lF._2.split(" ")
      val rFeat = rF._2.split(" ")
      val lD = lFeat.filter(!rFeat.contains(_))
      val rD = rFeat.filter(!lFeat.contains(_))
      f._1.toString + "\t" + lF._1 + "\t" + rF._1 + "\t" + lD.mkString(" missing ") + "\t" + rD.mkString(" append ")
    }).saveAsTextFile(modelConfigUnit.getString("model.lr.out.join.all",NormalComponent.defaultModelOutputFilePrefix))

    joinFeat.filter(f => {
      val t = f._2._1 match {
        case Some(x) => true
        case _ => false
      }

      val o = f._2._2 match {
        case Some(x) => true
        case _ => false
      }

      t && !o
    }).map(f => {
      f._1.toString + "\t" + f._2._1.getOrElse(("","")).toString
    }).saveAsTextFile(modelConfigUnit.getString("model.lr.out.join.left",NormalComponent.defaultModelOutputFilePrefix))

    joinFeat.filter(f => {
      val t = f._2._1 match {
        case Some(x) => true
        case _ => false
      }

      val o = f._2._2 match {
        case Some(x) => true
        case _ => false
      }

      !t && o
    }).map(f => {
      f._1.toString + "\t" + f._2._2.getOrElse((-1,-1,"")).toString
    }).saveAsTextFile(modelConfigUnit.getString("model.lr.out.join.right",NormalComponent.defaultModelOutputFilePrefix))

    joinFeat.unpersist()
  }
}
