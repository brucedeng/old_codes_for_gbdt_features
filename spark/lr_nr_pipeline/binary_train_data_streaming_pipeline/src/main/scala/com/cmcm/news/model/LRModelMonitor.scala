package com.cmcm.news.model

import com.cmcm.news.component.LRMonitorModelComponent
import com.cmcm.news.config.Configurable
import com.cmcm.news.spark.SparkComponent

/**
  * Created by lilonghua on 16/9/20.
  */
trait LRModelMonitor extends LRMonitorModelComponent {
  this: Configurable with SparkComponent =>

  override def workflowSetup() = {
    lrbMetricsOutput(getInputDstream)
  }
}
