package com.cmcm.news.component

import com.cmcm.news.config.{ConfigUnit, Configurable}
import com.cmcm.news.monitor.{MonitorMetrics, MonitorProxy}
import com.cmcm.news.spark.SparkComponent
import com.cmcm.news.util.{ZKStringSerializer, ZKUtils}
import kafka.common.TopicAndPartition
import kafka.message.MessageAndMetadata
import kafka.serializer.StringDecoder
import org.I0Itec.zkclient.ZkClient
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.{LongWritable, Text}
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat
import org.apache.spark.Logging
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.kafka.{HasOffsetRanges, KafkaUtils, OffsetRange}

import scala.util.Try

/**
  * Created by lilonghua on 16/9/20.
  */
object LRMonitorModelComponent extends Serializable with Logging {
  val componentName = "model_component"
  val configFileName = "model.conf"
  val defaultModelOutputDest = "file"
  val defaultModelOutputFilePrefix = "/tmp/deeplearning_lr/model"
  val defaultPatitionsForFetchingCP = 10
  val defaultDecayWindow = 600
  var monitor: MonitorProxy = _
  var monitorBroadcastVar: Broadcast[MonitorProxy] = _

  val defaultZkquorum = "10.3.6.123:2181"
  val defaultZkoffset = "/news/dl/lr/offset"
  val defaultTopicMap = "spart-topic"
  val defaultGroupId = "news_dl_lr_"
  val defaultReadThread = 4
  val defaultInputSource = "file"
  val defaultInputFileDir = "/tmp/news"
  val defaultInputPartitions = 20
  val defaultReceiverNumPerTopic = 2
  val defaultMode = "receiver"
  val defaultBroker = "10.10.17.45:9092,10.10.17.46:9092,10.10.14.25:9092"
  var offsetRanges = Array[OffsetRange]()

  def saveKafkaOffsetToZkp(mode: String, zkquorum: String, groupId: String, offsetPath: String)() = {
    if (mode.compareTo("direct") == 0) try {
      val zkClient = new ZkClient(zkquorum, 10000, 10000, ZKStringSerializer)
      val offsetData = offsetRanges.map(offsetRange => {
        offsetRange.topic + "\002" + offsetRange.partition + "\002" + offsetRange.untilOffset
      }).mkString("\001")

      logInfo("offset data : " + offsetData)
      ZKUtils.retry(3)(ZKUtils.updateAppOffsets(zkClient, offsetPath, offsetData))
      offsetRanges = Array[OffsetRange]()
      zkClient.close()
      logInfo("Finish writing offset to zk")
    } catch {
      case e: Throwable =>
        logInfo("update offsets to zkp failed, catch exception : \n" + e.getMessage)
    }
  }
}

trait LRMonitorModelComponent extends Component {
  this: Configurable with SparkComponent =>

  override def init(configDir: String) = {
    super.init(configDir)
    initModelComponent(configDir + "/" + ModelComponent.configFileName)
    ModelComponent.monitor = new MonitorProxy()
    ModelComponent.monitor.init(modelConfigUnit)
    ModelComponent.monitorBroadcastVar = sparkContext.broadcast(ModelComponent.monitor)
  }

  override def preStart() = {
    super.preStart()
  }

  def modelConfigUnit = {
    getConfigUnit(ModelComponent.componentName).get
  }

  def initModelComponent(modelConfigFile: String) = {
    logInfo("Initializing model component...")
    loadComponentConfig(ModelComponent.componentName, modelConfigFile)
  }

  def workflowSetup()

  def saveKafak(dStream: DStream[AnyRef]) = {
    val mode = modelConfigUnit.getString("model.lr.input.kafka.mode", LRMonitorModelComponent.defaultMode)
    val zkquorum = modelConfigUnit.getString("model.lr.input.kafka.zkquorum", LRMonitorModelComponent.defaultZkquorum)
    val groupId = modelConfigUnit.getString("model.lr.input.kafka.groupid", LRMonitorModelComponent.defaultGroupId)
    val offsetPath = modelConfigUnit.getString("model.lr.input.kafka.offset", LRMonitorModelComponent.defaultZkoffset)

    dStream.transform(rdd => {
      LRMonitorModelComponent.saveKafkaOffsetToZkp(mode, zkquorum, groupId, offsetPath)
      rdd
    })
  }

  def getInputDstream = {
    val inputSource = modelConfigUnit.getString("model.lr.input.source", LRMonitorModelComponent.defaultInputSource)
    if (inputSource == "file") {
      val inputDir = modelConfigUnit.getString("model.lr.input.file.dir", LRMonitorModelComponent.defaultInputFileDir)
      logInfo(s"init file input dstream from $inputDir")
      streamingContext.fileStream[LongWritable, Text, TextInputFormat](inputDir,
        (t: Path) => true, newFilesOnly = true)
        .map(_._2.toString)
    } else if (inputSource == "kafka") {
      getLRTrainDataKafkaDstream(streamingContext, modelConfigUnit)

    } else {
      throw new IllegalStateException("Can't get input dstream from " + modelConfigUnit.getConfigName)
    }
  }

  private def getLRTrainDataKafkaDstream(ssc: StreamingContext, eventConfigUnit: ConfigUnit): DStream[(String)] = {
    val mode = eventConfigUnit.getString("model.lr.input.kafka.mode", LRMonitorModelComponent.defaultMode)
    val zkquorum = eventConfigUnit.getString("model.lr.input.kafka.zkquorum", LRMonitorModelComponent.defaultZkquorum)
    val topicmap = eventConfigUnit.getString("model.lr.input.kafka.topic", LRMonitorModelComponent.defaultTopicMap)
    val inputPartitions = eventConfigUnit.getInt("model.lr.input.partitions", LRMonitorModelComponent.defaultInputPartitions)

    if (mode.compareTo("receiver") == 0) {
      // no use any more
      val groupId = eventConfigUnit.getString("model.lr.input.kafka.groupid", LRMonitorModelComponent.defaultGroupId)
      val readthread = eventConfigUnit.getInt("model.lr.input.kafka.readthread", LRMonitorModelComponent.defaultReadThread)
      val receiverNumPerTopic = eventConfigUnit.getInt("model.lr.input.kafka.receiver_number_per_topic", LRMonitorModelComponent.defaultReceiverNumPerTopic)
      val topicMap = topicmap.split(",").map((_, readthread))
      val kafkaDStreams = topicMap.flatMap(f = (topic: (String, Int)) => {
        (1 to receiverNumPerTopic) map (_ => {
          val groupIdForTopic: String = groupId + topic._1
          val kafkaParams = Map[String, String](
            "zookeeper.connect" -> zkquorum,
            "group.id" -> groupIdForTopic,
            "zookeeper.session.timeout.ms" -> "10000",
            "rebalance.backoff.ms" -> "4000",
            "rebalance.max.retries" -> "10",
            "zookeeper.connection.timeout.ms" -> "10000")
          KafkaUtils.createStream[String, String, StringDecoder, StringDecoder](ssc, kafkaParams, Map(topic._1 -> topic._2 / receiverNumPerTopic), StorageLevel.MEMORY_AND_DISK_SER_2)
        })
      })
      val unionDStream = ssc.union(kafkaDStreams)
      //unionDStream.repartition(inputPartitions).map(line => {
      unionDStream.map(line => {
        line._2
      })
    } else if (mode.compareTo("direct") == 0) {
      val offsetPath = eventConfigUnit.getString("model.lr.input.kafka.offset", LRMonitorModelComponent.defaultZkoffset)
      val broker = eventConfigUnit.getString("model.lr.input.kafka.broker", LRMonitorModelComponent.defaultBroker)
      var partitionOffsets = scala.collection.mutable.Map[TopicAndPartition, Long]()
      try {
        val zkClient = new ZkClient(zkquorum, 10000, 10000, ZKStringSerializer)
        partitionOffsets = ZKUtils.retry(3)(ZKUtils.getTopicsPartitionAndOffsets(zkClient, offsetPath, topicmap.split(",").toSet))
        zkClient.close()
      } catch {
        case e: Throwable => logInfo("Connecting to zk to get offset ranges catch some exceptions:\n " + e.getMessage)
      }

      val kafkaParams = Map[String, String]("metadata.broker.list" -> broker)
      if (partitionOffsets.nonEmpty) {
        logInfo("has partition offsets in zkp")
        KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder, String](ssc, kafkaParams, partitionOffsets.toMap, (mmd: MessageAndMetadata[String, String]) => mmd.message())
          .transform(rdd => {
            LRMonitorModelComponent.offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
            rdd
          }).map((record: String) => record)
        //}).map((record: String) => record).repartition(inputPartitions)
      } else {
        logInfo("no partition offsets in zkp")
        KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](ssc, kafkaParams, topicmap.split(",").toSet)
          .transform(rdd => {
            LRMonitorModelComponent.offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
            rdd
          }).map(_._2)
        //}).map(_._2).repartition(inputPartitions)
      }
    } else {
      println("unknown kafka mode")
      sys.exit(-1)
    }
  }

  def lrbMetricsOutput(dStream: DStream[String]) = {
    val monitorEnabled = modelConfigUnit.getBoolean("model.monitor.enabled", false)
    if (monitorEnabled) {
      dStream.foreachRDD(rdd => {
        val trainDataCount = rdd.count()
        ModelComponent.monitor.recordFromDriver(MonitorMetrics.LR_OUTPUT_TRAIN_DATA_COUNT, trainDataCount.toString)

        rdd.map(feat => {
          (feat.split("\t")(2).split(":")(0), 1)
        }).reduceByKey((f1,f2) => {
          f1 + f2
        }).collect()
          .foreach(tuple => {
            ModelComponent
              .monitor
              .recordFromDriver(
                if (tuple._1 == "0") MonitorMetrics.LR_NEGATIVE_TRAIN_DATA_COUNT else MonitorMetrics.LR_POSITIVE_TRAIN_DATA_COUNT,
                tuple._2.toString
              )
          })

        val maxFeat = Try(rdd.map(feat => {
          feat.split("\t")(3).split(" ").length
        }).max()).getOrElse(0)

        ModelComponent.monitor.recordFromDriver(MonitorMetrics.LR_TRAIN_DATA_MAX_FEAT.toLowerCase, maxFeat.toString)

        rdd.flatMap(feat => {
          feat.split("\t")(3).split(" ").map(ft => {
            val t = ft.split("_", 2)
            (t(0), t(1))
          }).filter(t => {t._2.isEmpty || t._2 == "-1" || t._2 == "0"}).map(_._1).distinct.map((_, 1))
        }).reduceByKey((f1,f2) => {
          f1 + f2
        }).collect()
          .foreach(tuple => {
            ModelComponent.monitor.recordFromDriver((tuple._1 + "_" + MonitorMetrics.LR_EMPTY_TRAIN_DATA_FEAT).toLowerCase, (tuple._2.toFloat / trainDataCount).toString)
          })

        rdd.flatMap(feat => {
          feat.split("\t")(3).split(" ").map(ft => {
            val t = ft.split("_", 2)
            (t(0), t(1))
          }).filter(t => {t._2.nonEmpty && t._2 != "-1" && t._2 != "0"}).map(_._1).distinct.map((_, 1))
        }).reduceByKey((f1,f2) => {
          f1 + f2
        }).collect()
          .foreach(tuple => {
            ModelComponent.monitor.recordFromDriver((tuple._1 + "_" + MonitorMetrics.LR_NONEMPTY_TRAIN_DATA_FEAT).toLowerCase, (tuple._2.toFloat / trainDataCount).toString)
          })

        rdd.flatMap(feat => {
          feat.split("\t")(3).split(" ").map(ft => {
            val t = ft.split("_", 2)
            (t(0), t(1))
          }).filter(t => {t._2.nonEmpty}).map(t => (t._1, 1))
        }).reduceByKey((f1,f2) => {
          f1 + f2
        }).collect()
          .foreach(tuple => {
            ModelComponent.monitor.recordFromDriver((tuple._1 + "_" + MonitorMetrics.LR_NONEMPTY_LENGTH_TRAIN_DATA_FEAT).toLowerCase, (tuple._2.toFloat / trainDataCount).toString)
          })

      })
    }
  }
}
