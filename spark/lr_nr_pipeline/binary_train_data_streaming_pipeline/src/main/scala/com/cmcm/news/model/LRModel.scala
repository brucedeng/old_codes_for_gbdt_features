package com.cmcm.news.model

import com.cmcm.news.component.{EventComponent, FeatureComponent}
import com.cmcm.news.config.Configurable
import com.cmcm.news.output.StreamingOutput
import com.cmcm.news.processor.LRModelProcessor
import com.cmcm.news.spark.SparkComponent
import org.apache.spark.storage.StorageLevel

/**
  * Created by lilonghua on 16/8/24.
  */
trait LRModel extends LRModelProcessor {
  this: Configurable with SparkComponent with EventComponent with FeatureComponent  =>

  override def workflowSetup() = {
    val cfbModelOutput: StreamingOutput[((Long,String), (String, String, String, Double, String))] = getCfbModelOutput(modelConfigUnit)

    val eventDStream = getEventDStream()
    val parsedDStream = generalParse(eventDStream)
    val labelDStream = eventLabel(parsedDStream)
    val eventAggregationDStream = eventAggregation(labelDStream)
    val featureStreamWithCP = joinIndiaCP(eventAggregationDStream)

    val featureStreamWithCPAndUP = joinUP(featureStreamWithCP)

    val lrbFeatureStream = generateWorldwideLRBFeatures(featureStreamWithCPAndUP)/*.persist(StorageLevel.MEMORY_AND_DISK_SER)*/
    val part = modelConfigUnit.getInt("model.lr.partitions", 0)
    //save kafka offset
    //val modelStream = (if (part > 1) cfbAggregation(lrbFeatureStream).repartition(part) else cfbAggregation(lrbFeatureStream)).persist(StorageLevel.MEMORY_AND_DISK_SER)
    val modelStream = (if (part > 1) cfbAggregation(lrbFeatureStream).transform(_.coalesce(part)) else cfbAggregation(lrbFeatureStream)).persist(StorageLevel.MEMORY_AND_DISK_SER)
    //val modelStream = if (part > 1) cfbAggregation(lrbFeatureStream).transform(_.coalesce(part)) else cfbAggregation(lrbFeatureStream)

    cfbModelOutput.output(modelStream, "")

    //cfbModelOutput.output(modelStream.transform(_.coalesce(part)), "")

    lrbMetricsOutput(eventDStream, parsedDStream, labelDStream, modelStream)
    //cfbMetricsOutput(eventDStream, parsedDStream, labelDStream, featureStreamWithCP, featureStreamWithCPAndUP, modelStream)
    modelStream.foreachRDD(rdd => rdd.unpersist())
  }
}