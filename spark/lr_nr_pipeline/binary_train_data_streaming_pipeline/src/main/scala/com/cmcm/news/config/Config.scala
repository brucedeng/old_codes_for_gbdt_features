package com.cmcm.news.config

import java.io.File

import com.typesafe.config.ConfigFactory

/**
 * Created by tangdong on 27/4/16.
 */
class Config(val confFile:String) {
  val typesafeConfig = if(confFile.nonEmpty) ConfigFactory.parseFile(new File(confFile))
  else ConfigFactory.load()
}
