package com.cmcm.news.analysis

import com.cmcm.news.component.NormalComponent
import com.cmcm.news.config.Configurable
import com.cmcm.news.spark.SparkNormal
import org.apache.spark.storage.StorageLevel

/**
  * Created by lilonghua on 16/9/21.
  */
trait LRFeatureDiffWithPre extends NormalComponent {
  this: Configurable with SparkNormal =>

  override def workflowSetup(): Unit = {

    logInfo("Start LRFeatureDiffWithPre ....")

    logInfo("input preFeat file => " + modelConfigUnit.getString("model.lr.input.preFeat", NormalComponent.defaultModelOutputFilePrefix))

    logInfo("input newFeat file => " + modelConfigUnit.getString("model.lr.input.newFeat", NormalComponent.defaultModelOutputFilePrefix))

    val preFeat = sparkContext.textFile(modelConfigUnit.getString("model.lr.input.preFeat", NormalComponent.defaultModelOutputFilePrefix))
      .map(f => {
        val ft = f.split("\t")
        ((ft(0), ft(1)), ft(2), ft(3))
      }).persist(StorageLevel.MEMORY_AND_DISK_SER)

    val newFeat = sparkContext.textFile(modelConfigUnit.getString("model.lr.input.newFeat", NormalComponent.defaultModelOutputFilePrefix))
      .map(f => {
        val ft = f.split("\t")
        ((ft(0), ft(1)), ft(2), ft(3))
      }).persist(StorageLevel.MEMORY_AND_DISK_SER)

    val preCount = preFeat.count()
    val newCount = newFeat.count()

    preFeat.map(feat => {
      (feat._3.split(" ").length, 1)
    }).reduceByKey((f1,f2) => {
      f1 + f2
    }).sortByKey(false).map(f => f._1 + "\t" + f._2 + "\t" + f._2.toFloat / preCount)
      .saveAsTextFile(modelConfigUnit.getString("model.lr.out.preFeat.featDist", NormalComponent.defaultModelOutputFilePrefix))

    newFeat.map(feat => {
      (feat._3.split(" ").length, 1)
    }).reduceByKey((f1,f2) => {
      f1 + f2
    }).sortByKey(false).map(f => f._1 + "\t" + f._2 + "\t" + f._2.toFloat / newCount)
      .saveAsTextFile(modelConfigUnit.getString("model.lr.out.newFeat.featDist", NormalComponent.defaultModelOutputFilePrefix))

    preFeat.flatMap(feat => {
      feat._3.split(" ").map(ft => {
        val t = ft.split("_", 2)
        (t(0), t(1))
      }).filter(t => {t._2.isEmpty || t._2 == "-1" || t._2 == "0"}).map(_._1).distinct.map((_, 1))
    }).reduceByKey((f1,f2) => {
      f1 + f2
    }).sortByKey(false).map(f => f._1 + "\t" + f._2 + "\t" + f._2.toFloat / newCount)
      .saveAsTextFile(modelConfigUnit.getString("model.lr.out.preFeat.featEmptyDist", NormalComponent.defaultModelOutputFilePrefix))

    newFeat.flatMap(feat => {
      feat._3.split(" ").map(ft => {
        val t = ft.split("_", 2)
        (t(0), t(1))
      }).filter(t => {t._2.isEmpty || t._2 == "-1" || t._2 == "0"}).map(_._1).distinct.map((_, 1))
    }).reduceByKey((f1,f2) => {
      f1 + f2
    }).sortByKey(false).map(f => f._1 + "\t" + f._2 + "\t" + f._2.toFloat / newCount)
      .saveAsTextFile(modelConfigUnit.getString("model.lr.out.newFeat.featEmptyDist", NormalComponent.defaultModelOutputFilePrefix))

    preFeat.unpersist()
    newFeat.unpersist()
  }
}
