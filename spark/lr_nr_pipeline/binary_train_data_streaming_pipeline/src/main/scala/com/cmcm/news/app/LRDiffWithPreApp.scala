package com.cmcm.news.app

import com.cmcm.news.analysis.LRFeatureDiffWithPre
import com.cmcm.news.config.Configurable
import com.cmcm.news.spark.SparkNormal

/**
  * Created by lilonghua on 16/9/21.
  */
object LRDiffWithPreApp extends NormalApp
  with Configurable
  with SparkNormal
  with LRFeatureDiffWithPre
