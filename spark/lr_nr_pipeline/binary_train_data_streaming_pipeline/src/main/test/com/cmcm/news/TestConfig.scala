import com.cmcm.news.config.ConfigUnit
import org.scalatest.FunSuite

import scala.util.{Failure, Success, Try}

/**
  * Created by lilonghua on 16/9/1.
  */

class TestConfig extends FunSuite {
  test("test config") {
    Try(ConfigUnit("feature_component", "conf/india/india_hindi_v4_lr/conf/feature.conf")) match {
      case Success(conf) => {
        val up = conf.config.getConfig("feature").getConfig("up").getString("input_dir")
        println(up)
      }
      case Failure(err) =>
        println(err.getMessage)
    }
  }
}
