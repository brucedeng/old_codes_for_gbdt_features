name := "spark_binary_data_streaming"

version := "1.0"

scalaVersion := "2.10.4"

autoScalaLibrary := false

// libraryDependencies += "org.apache.spark" %% "spark-core" % "1.3.0" % "provided"
//libraryDependencies ++= Seq(
//        "org.slf4j" % "slf4j-api"       % "1.7.7",
//        "org.slf4j" % "jcl-over-slf4j"  % "1.7.7"
//        ).map(_.force())

//libraryDependencies ~= { _.map(_.exclude("org.slf4j", "slf4j-jdk14")) }

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.3.0" % "provided"

//libraryDependencies += "org.apache.spark" %% "spark-mllib" % "1.3.0" % "provided"
libraryDependencies += "org.apache.spark" %% "spark-streaming" % "1.3.0" % "provided"
libraryDependencies += "org.apache.spark" %% "spark-streaming-kafka" % "1.3.0"

libraryDependencies += "redis.clients" % "jedis" % "2.8.1"
// libraryDependencies += "it.unimi.dsi" % "fastutil" % "6.5.15"
libraryDependencies += "org.apache.httpcomponents" % "httpclient" % "4.2.5"
libraryDependencies += "com.google.code.gson" % "gson" % "2.3"
libraryDependencies += "com.facebook.jcommon" % "util" % "0.1.14"
libraryDependencies += "com.github.scopt" % "scopt_2.10" % "2.1.0"
libraryDependencies += "org.apache.kafka" % "kafka_2.10" % "0.8.2.0"

//libraryDependencies += "org.apache.spark" %% "spark-sql"  % "1.3.0"

//libraryDependencies += "org.apache.spark" % "spark-hive_2.10" % "1.3.0"

libraryDependencies += "org.json4s" % "json4s-jackson_2.10" % "3.2.4"

libraryDependencies += "org.json4s" % "json4s-native_2.10" % "3.2.4"
//
// libraryDependencies += "org.json4s" % "json4s-native_2.11" % "3.4.0"
// libraryDependencies += "org.json4s" % "json4s-jackson_2.11" % "3.4.0"



libraryDependencies += "com.typesafe.scala-logging" % "scala-logging-slf4j_2.10" % "2.1.2"

libraryDependencies += "log4j" % "log4j" % "1.2.14"
