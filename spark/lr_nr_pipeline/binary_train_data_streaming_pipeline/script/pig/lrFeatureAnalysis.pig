set mapreduce.job.queuename '$queue';
set job.name '$jobname.$queue.$startDate';

TENMINS_FEAT_DATA = LOAD '$tenMins' USING PigStorage('\t') AS (uid:chararray,cid:chararray,labw:chararray,feat:chararray);

TENMINS_FEAT_SPLIT_DATA = FOREACH TENMINS_FEAT_DATA GENERATE {
    $0,
    $1,
    FLATTEN(STRSPLIT($2, ':', 2)) AS (label:chararray, weight:double)
};

ONEMINS_FEAT_DATA = LOAD '$oneMins' USING PigStorage('\t') AS (uid:chararray,cid:chararray,labw:chararray,feat:chararray);

ONEMINS_FEAT_SPLIT_DATA = FOREACH ONEMINS_FEAT_DATA GENERATE {
    $0,
    $1,
    FLATTEN(STRSPLIT($2, ':', 2)) AS (label:chararray, weight:double)
};

DINSTINCT_ONEMINS_FEAT_DATA = FOREACH (GROUP ONEMINS_FEAT_SPLIT_DATA BY ($0,$1)) {
    na = SUM(CASE WHEN ONEMINS_FEAT_SPLIT_DATA.label == '0' THEN 1 ELSE 0 END);
    po = SUM(CASE WHEN ONEMINS_FEAT_SPLIT_DATA.label == '1' THEN 1 ELSE 0 END);
    GENERATE FLATTEN(group) AS (uid, cid), na AS nag, po AS pos;
};

