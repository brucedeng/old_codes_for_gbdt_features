#!/usr/bin/env bash
#export SPARK_CONF_DIR=./spark_conf

export LD_LIBRARY_PATH=$LDLIBRARY_PATH:/usr/lib/hadoop/lib/native
#export LD_LIBRARY_PATH=$LDLIBRARY_PATH:/usr/lib/hadoop/lib/native:/usr/lib/hadoop/lib/native/Linux-amd64-64/
#export HADOOP_CONF_DIR=/etc/hadoop/conf
#export YARN_CONF_DIR=/usr/lib/spark/conf
#export SPARK_CLASSPATH=/usr/lib/hadoop/lib/*
hadoop fs -rm -r /projects/news/model/online/en_us_lr_pid14_add_topic/gen_training_data_streaming/chekpoint

currentDir=$(cd `dirname $0`; pwd)

#echo $$ > ${currentDir}/run/lr.pid

spark-submit \
--conf spark.task.maxFailures=100 \
--conf spark.yarn.max.executor.failures=200 \
--driver-java-options "-Dlog4j.configuration=file:${currentDir}/conf/log4j.properties" \
--class com.cmcm.news.app.LRApp \
--master yarn-client \
--driver-memory 3g \
--executor-memory 3g \
--executor-cores 3 \
--num-executors 25 \
--queue fasttrack \
${currentDir}/lib/spark_binary_data_streaming-assembly-1.0.jar \
-c ${currentDir}/conf/ 1>${currentDir}/logs/stdout 2>${currentDir}/logs/stderr $@ &

echo $! > ${currentDir}/run/lr.pid
