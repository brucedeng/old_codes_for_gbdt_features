package org.apache.spark.mllib.evaluation

import org.apache.spark.rdd.RDD

/**
  * Created by kehan on 2016/10/27.
  * uid, score, label
  */
class BinaryClassificationPerUserMetrics(
                                          val userScoreAndLabels: RDD[(String,Double, Double)])
  extends BinaryClassificationMetrics(
    userScoreAndLabels.map(x => (x._2, x._3))
  ) {

  def peruuAreaUnderROC(): Double = {
    userScoreAndLabels
      .map(x => (x._1, (x._2, x._3)))
      .groupByKey()
      .mapValues(iter => iter.toArray.sortBy(_._1).reverse)
      .filter(
        x => {
          val pos = x._2.filter(_._2 == 1.0).length
          val neg = x._2.filter(_._2 == 0.0).length
          (pos > 0 && neg >0)
        }
      )
      .map(
        x => BinaryClassificationPerUserMetrics.metricAuc(x._2)
      ).mean()
  }
}

object BinaryClassificationPerUserMetrics extends Serializable{
  /*(score, label)*/
  def metricAuc(scoreAndLabels:Array[(Double,Double)]):Double = {
    val act_score_sorted = scoreAndLabels
    var auc = 0.0
    var k0 = 0.0
    var k1 = 0.0
    var tp0 = 0.0
    var fp0 = 0.0
    var tp1 = 1.0
    var fp1 = 1.0
    var P = 0.0
    var N = 0.0
    for ((score, label) <- act_score_sorted) {
      if (label == 1)
        P += 1.0
      else
        N += 1.0
    }

    if (P > 0 && N >0) {
      for ((score, label) <- act_score_sorted) {
        if (label == 1) {
          k1 += 1.0
          tp1 = k1 / P
          fp1 = k0 / N
          //auc += (fp1 - fp0) * (tp1 + tp0) / 2.0
          auc += (fp1 - fp0) * tp0
          tp0 = tp1
          fp0 = fp1
        }
        else {
          k0 += 1.0
        }
      }
      auc += 1.0 - fp1

      auc
    }
    else if (P == 0)
      0.0
    else
      1.0
  }
}
