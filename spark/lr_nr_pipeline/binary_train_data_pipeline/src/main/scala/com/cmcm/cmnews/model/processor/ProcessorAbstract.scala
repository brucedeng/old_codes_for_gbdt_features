package com.cmcm.cmnews.model.processor

import org.apache.spark.Logging
import org.apache.spark.rdd.RDD

/**
  * Created by lilonghua on 16/8/3.
  */
trait ProcessorAbstract[T] extends Logging with Serializable{
  def preprocess(line:String, batchContext: collection.mutable.Map[String,String]) : T


  def process(inputRDD:RDD[String],batchContext: collection.mutable.Map[String,String]): RDD[T]
}
