package com.cmcm.cmnews.model.feature

import com.cmcm.cmnews.model.util.{JsonUtil, LoggingUtils, Parameters}
import org.apache.spark.Logging

import scala.collection.immutable._
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.DStream

import scala.util.Try

/**
  * Created by lilonghua on 16/8/3.
  */

case class Instance(base: BaseInfo,
                    staticFeature: List[(String, (Int, Double))],
                    cfbFeature: List[(String, (Int, Double))],
                    discreFeature: List[(String, (Int, Double))]) {

  def mkLibSVM(map: HashMap[String, Int]): String = {
    staticFeature.map(x => map(x._1) + ":" + x._2._2).mkString(" ") + " "
    cfbFeature.map(x => map(x._1) + ":" + x._2._2).mkString(" ") + " "
    discreFeature.map(x => map(x._1) + ":" + x._2._2).mkString(" ")
  }

  def filter(strList: Set[String]): Instance =
    Instance(base,
      staticFeature.filter(strList contains _._1),
      cfbFeature.filter(strList contains _._1),
      discreFeature.filter(strList contains _._1))

  def filter(idxList: Set[Int], map: HashMap[String, Int]): Instance = {
    val reversMap = map.map(x => (x._2, x._1))
    val strList = idxList.map(reversMap(_))
    this.filter(strList)
  }
}

trait Functor[F[_]] {
  def map[A,B](fa: F[A])(f: A => B): F[B]
}

trait Monad[M[_]] extends Functor[M] {
  def unit[A](a: => A): M[A]
  def flatMap[A,B](ma: M[A])(f: A => M[B]): M[B]
  def apply[A, B](ma: M[A])(f: M[A] => M[B]): M[B] =
    flatMap(ma)(a => f(unit(a)))
  def map[A,B](ma: M[A])(f: A => B): M[B] =
    flatMap(ma)(x => unit(f(x)))
  def map2[A,B,C](ma: M[A], mb: M[B])(f: (A, B) => C): M[C] =
    flatMap(ma)(x => map(mb)(y => f(x, y)))
}

case class State[S, A](run: S => (A, S)) {
  def map[B](f: A => B): State[S, B] =
    State(sm => {
      val (s, s1) = run(sm)
      (f(s), s1)
    })

  def flatMap[B](f: A => State[S, B]): State[S, B] =
    State(s => {
      val (a, s1) = run(s)
      f(a).run(s1)
    })
}

object Monad {
}

trait Monoid[A] {
  def op(a1: A, a2: A): A
  def zero: A
}

object Monoid {
  def mapMergeMonoid[K, V](V: Monoid[V]): Monoid[HashMap[K, V]] = new Monoid[HashMap[K, V]] {
    def zero: HashMap[K, V] = HashMap.empty
    def op(a: HashMap[K, V], b: HashMap[K, V]): HashMap[K, V] = {
      val c = a.map {
        case (k, v) => (k, V.op(v, b.get(k) getOrElse V.zero))
      }
      b ++ c
    }
  }

  def floatAddition: Monoid[Float] = new Monoid[Float] {
    def zero: Float = 0.0f
    def op(a: Float, b: Float): Float = a + b
  }

  def stringMerge: Monoid[String] = new Monoid[String] {
    def zero: String = ""
    def op(s1: String, s2: String): String = s2
  }
}

trait Rule {
  def rule: Instance => Boolean

  def &&(rule1: Rule): Rule = new Rule {
    def rule = instance => (this.rule(instance) && rule1.rule(instance))
  }

  def ||(rule1: Rule): Rule = new Rule {
    def rule = instance => (this.rule(instance) || rule1.rule(instance))
  }

}

object Rule {
  def ruleAndMonoid = new Monoid[Rule] {
    def zero: Rule = new Rule { def rule = _ => true }
    def op(rule1: Rule, rule2: Rule): Rule = new Rule {
      def rule = instance => (rule1.rule(instance) && rule2.rule(instance))
    }
  }

  def ruleOrMonoid = new Monoid[Rule] {
    def zero: Rule = new Rule { def rule = _ => false }
    def op(rule1: Rule, rule2: Rule): Rule = new Rule {
      def rule = instance => (rule1.rule(instance) || rule2.rule(instance))
    }
  }

  def mergeRule(ruleMonoid: Monoid[Rule])(rules: List[Rule]): Rule =
    rules.foldLeft(ruleMonoid.zero)(ruleMonoid.op(_, _))
}

object RuleCase {
  def wordCountRule = new Rule {
    def rule: Instance => Boolean =
      instance => {
        instance.staticFeature.toMap.getOrElse("WORD_COUNT", (1, 1.0d))._2.toDouble < 10000.0d
      }
  }
}

case class BaseInfo(uid: String, dCid: String, label: String, dwell: String, weight: Double=1.0d) {
  override def toString: String =
    s"$uid\t$dCid\t$label\t$dwell"
}

case class Feature(fIndex: Int, value: Double) {
  override def toString: String = fIndex.toString + ":" + value.toString
}

object LRBFeature extends Logging {

  def encode(baseData: RDD[Instance], featureMaps: HashMap[String, Int], isStaticNeed: Boolean): RDD[(BaseInfo, List[Feature])] = {
    def instanceSwitch: Instance => (BaseInfo, List[Feature]) = {
      instance: Instance => {
        val staticInstance = if (isStaticNeed) {
          instance.staticFeature ++ instance.cfbFeature
        } else { List.empty[(String, Double)] }

        val featureInfo: List[Feature] = (staticInstance ++ instance.discreFeature).flatMap({
          case (fname: String, (pv, value: Double)) => {
            if (featureMaps contains fname)
              List(Feature(featureMaps(fname), value))
            else List.empty[Feature]
          }
        }).sortWith(_.fIndex < _.fIndex)

        (instance.base, featureInfo)
      }
    }
    baseData.map(instanceSwitch)
  }

  def filterInstance(rule: Rule)(baseData: RDD[Instance]): RDD[Instance] =
    baseData.filter(rule.rule)

  def filterInstanceStream(rule: Rule)(baseData: DStream[Instance]): DStream[Instance] =
    baseData.filter(rule.rule)

  def calculateWeight(baseData: RDD[Instance]): RDD[Instance] = {
    val (spWt: Double, snWt: Double) = Try(baseData.map(x => {
      val label = x.base.label.toInt
      // require(x.base.dwell.toDouble >= 0.0d, s"illegal dwell, ${x.base.dwell}")

      val wt = math.log(math.max(0.0d, Try(x.base.dwell.toDouble).toOption match {
        case Some(t) => t
        case None => 0.0d
      }) + 1.0d)
      val pos_wt = if (label == 1) wt else 0.0d
      val neg_wt = if (label == 1) 0.0d else 1.0d
      (pos_wt, neg_wt)
    }).reduce((x, y) => (x._1 + y._1, x._2 + y._2))).getOrElse((1d,0d))
    val ratio: Double = math.min(snWt / spWt, 10)
    println(s"ratio: $ratio, spWt: $spWt, snWt: $snWt")
    baseData.map(x => {
      val wt = math.log(Try(x.base.dwell.toDouble).toOption match {
        case Some(t) => t + + 1.0d
        case None => 1.0d
      })
      // val label = (x.base.label.toInt * weight * ratio * 10000).toInt.toString

      val weight: Double = if (x.base.label.toInt == 1) wt * ratio else 1.0d
      val label = x.base.label
      Instance(BaseInfo(x.base.uid, x.base.dCid, label, x.base.dwell, weight),
        x.staticFeature, x.cfbFeature, x.discreFeature)
    })
  }

  /*def calculateWeightStream(baseData: DStream[Instance]): DStream[Instance] = {
    val wt = baseData.map(x => {
      val label = x.base.label.toInt
      // require(x.base.dwell.toDouble >= 0.0d, s"illegal dwell, ${x.base.dwell}")

      val wt = math.log(math.max(0.0d, x.base.dwell.toDouble) + 1.0d)
      val pos_wt = if (label == 1) wt else 0.0d
      val neg_wt = if (label == 1) 0.0d else 1.0d
      (pos_wt, neg_wt)
    }).reduce((x, y) => (x._1 + y._1, x._2 + y._2))
    val ratio: Double = math.min(wt._2 / wt._1, 10)
    //println(s"ratio: $ratio, spWt: $spWt, snWt: $snWt")
    baseData.map(x => {
      val wt = math.log(x.base.dwell.toDouble + 1.0d)
      // val label = (x.base.label.toInt * weight * ratio * 10000).toInt.toString

      val weight: Double = if (x.base.label.toInt == 1) wt * ratio else 1.0d
      val label = x.base.label
      Instance(BaseInfo(x.base.uid, x.base.dCid, label, x.base.dwell, weight),
        x.staticFeature, x.cfbFeature, x.discreFeature)
    })
  }*/
}
