package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.EventStreamingCompute
import com.cmcm.cmnews.model.input.EventStreamingInput
import com.cmcm.cmnews.model.output.EventStreamingOutput
import com.cmcm.cmnews.model.spark.SparkStreamingContext
import com.cmcm.cmnews.model.util.LoggingUtils

/**
 * Created by tangdong on 27/4/16.
 */
trait StreamingApp {
  this: SparkStreamingContext
    with EventStreamingInput
    with EventStreamingCompute
    with EventStreamingOutput =>

  def initApp(): Unit = {
    logInfo("Start init app...")
    init()
  }

  def processApp(): Unit ={
    logInfo("Start process app...")
    getInput
    compute
    outPut
  }

  def main(args:Array[String]) = {
    var key = ""
    for (item <- args){
      if (key.isEmpty) key = item
      else{
        LoggingUtils.loggingInfo(s"argument: key($key), value($item)")
        batchContext += (key -> item)
        key = ""
      }
    }
    initApp()
    processApp()

    this.getSparkStreamingContext.start()
    this.getSparkStreamingContext.awaitTermination()
  }
}
