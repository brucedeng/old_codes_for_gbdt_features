package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.component.StreamingComponent
import com.cmcm.cmnews.model.spark.SparkStreamingContext

/**
 * Created by tangdong on 27/4/16.
 */
trait EventStreamingCompute extends StreamingComponent{
  this:SparkStreamingContext =>
  def compute = {
    logInfo("Start EventBatchCompute...")
  }
}
