package com.cmcm.cmnews.model.spark

import com.cmcm.cmnews.model.feature.{BaseInfo, Feature, Instance, Rule}
import com.cmcm.cmnews.model.processor.CPBaseInfo
import com.cmcm.cmnews.model.util.Parameters
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.immutable.HashMap
import scala.util.Try

/**
 * Created by tangdong on 1/5/16.
 */
trait XFBTrainDataSparkContext extends SparkBatchContext{
  import Parameters._

  override def init(): Unit ={
    super.init()
    logInfo("Start XFBTrainDataSparkContext...")
    val sparkConf = new SparkConf()
      .registerKryoClasses(Array(
        classOf[Feature],
        classOf[Instance],
        classOf[HashMap[String, Int]],
        classOf[Rule],
        classOf[BaseInfo],
        classOf[CPBaseInfo]))
      .set("spark.serializer","org.apache.spark.serializer.KryoSerializer")
      .set("spark.default.parallelism", Try(batchContext(constant_parallelism)).getOrElse("32"))
      //.setAppName(Try(batchContext(constant_jobName)).getOrElse(this.getClass.getName))
    sbc = new SparkContext(sparkConf)
  }

}
