package com.cmcm.cmnews.model.feature

import com.cmcm.cmnews.model.util.Parameters
import org.apache.spark.Logging
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.DStream

/**
 * Created by tangdong on 1/5/16.
 */
object UserStreamingFeature extends Logging{
  import Parameters._

  def joinWithBatchEvent(userFeatureRdd:RDD[(String, String)], eventRdd: DStream[(String, String)], parallel:Int=200) = {
    logInfo("Now, join user profile and event")
    eventRdd.filter(line => line._1.split(fieldDelimiter).length>=2)
      .map((line:(String, String)) => {
      val items = line._1.split(fieldDelimiter)
      val uuid = items(0)
      val cid = items(1)
      (uuid, (cid, line._2))
    }).transform( rdd => {
      rdd.leftOuterJoin(userFeatureRdd, parallel).map(result => {
        val cid = result._2._1._1
        val eventFeatures = result._2._1._2
        val upFeatures = result._2._2
        val uid = result._1
        val defaultValue = "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "0" + fieldDelimiter + "0"
        val upFeaturesStr = upFeatures.getOrElse(defaultValue)
        val afterFields = uid + fieldDelimiter + eventFeatures + fieldDelimiter + upFeaturesStr
        (cid, afterFields)
      })
    })
  }

}
