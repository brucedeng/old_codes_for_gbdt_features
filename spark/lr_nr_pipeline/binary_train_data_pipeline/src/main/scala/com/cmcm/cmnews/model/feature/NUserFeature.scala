package com.cmcm.cmnews.model.feature

import com.cmcm.cmnews.model.util.Parameters
import com.cmcm.cmnews.model.util.Parameters._
import org.apache.spark.Logging
import org.apache.spark.rdd.RDD

/**
 * Created by tangdong on 17/5/16.
 */
object NUserFeature extends Logging{
  import Parameters._

  def joinWithBatchEvent(userFeatureRdd:RDD[(String, String)], eventRdd: RDD[(String, String)]) = {
    logInfo("Now, join user profile and event")
    eventRdd.map((line:(String, String)) => {
      val items = line._1.split(fieldDelimiter)
      val ukey = items(0)
      val uuid = items(1)
      val cid = items(2)
      (ukey, (uuid,cid, line._2))
    }).leftOuterJoin(userFeatureRdd).map(result => {
      val uid = result._2._1._1
      val cid = result._2._1._2
      val eventFeatures = result._2._1._3
      val upFeatures = result._2._2
      val defaultValue = "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "0" + fieldDelimiter + "0"
      val upFeaturesStr = upFeatures.getOrElse(defaultValue)
      val afterFields = uid + fieldDelimiter + eventFeatures + fieldDelimiter + upFeaturesStr
      (cid, afterFields)
    })
  }

}
