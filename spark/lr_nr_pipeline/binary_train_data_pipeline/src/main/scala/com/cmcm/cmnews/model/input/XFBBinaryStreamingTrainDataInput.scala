package com.cmcm.cmnews.model.input

import com.cmcm.cmnews.model.spark.{SparkStreamingContext, SparkBatchContext}
import com.cmcm.cmnews.model.util.Parameters

/**
 * Created by tangdong on 3/5/16.
 */
trait XFBBinaryStreamingTrainDataInput extends EventStreamingInput{
  this: SparkStreamingContext =>
  import Parameters._

  override def getInput(): Unit ={
    logInfo("start XFBTrainDataInput...")
    setDStream(constant_event_input,constant_event_rdd, this)
    setDStream(constant_content_input, constant_content_rdd, this)
    setRDD(constant_user_input, constant_user_rdd, this)
    //setRDD(constant_xfb_input,constant_xfb_rdd, this)
  }

}
