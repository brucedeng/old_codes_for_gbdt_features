package com.cmcm.cmnews.model.output

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{FtrlModel, Parameters}
import org.apache.spark.rdd.RDD

/**
  * Created by lilonghua on 16/10/9.
  */
trait FTRLSerializeDataOutput extends EventBatchOutput{
  this:SparkBatchContext =>
  import Parameters._

  override def outPut: Unit ={
    super.outPut
    logInfo("Start FTRLSerializeDataOutput...")
    /*val serializeDataOut = rddContext(constant_lr_serialize_rdd).asInstanceOf[RDD[FtrlModel]]
    serializeDataOut.saveAsObjectFile(batchContext(constant_lr_serialize_ptah))*/
  }
}
