package com.cmcm.cmnews.model.util

import java.nio.{ByteBuffer, ByteOrder}

import scala.collection.immutable._
import scala.util.matching.Regex
import org.apache.spark.rdd.RDD


trait SerializeMap[A,B] {
  def serializeMap(m: HashMap[A, B]): String = {
    "{" + m.map({case (key: A, value: B) => key.toString + ":" + value.toString})
      .mkString(",") + "}"
  }

  def deSerializeMap(str: String): HashMap[A, B]
}

object StringStringMap extends SerializeMap[String,String] {

  def deSerializeMap(str: String): HashMap[String, String] = {
    val line = str.drop(1).dropRight(1)
    line.split(",").foldLeft(HashMap.empty[String, String])((y, x) => {
      val terms = x.split(":")
      y + (terms(0) -> terms(1))
    })
  }
}

object StringIntMap extends SerializeMap[String,Int] {

  def deSerializeMap(str: String): HashMap[String, Int] = {
    val line = str.drop(1).dropRight(1)
    line.split(",").foldLeft(HashMap.empty[String, Int])((y, x) => {
      val terms = x.split(":")
      y + (terms(0) -> terms(1).toInt)
    })
  }
}

object StringFloatMap extends SerializeMap[String,Float] {

  def deSerializeMap(str: String): HashMap[String, Float] = {
    val line = str.drop(1).dropRight(1)
    line.split(",").foldLeft(HashMap.empty[String, Float])((y, x) => {
      val terms = x.split(":")
      y + (terms(0) -> terms(1).toFloat)
    })
  }
}

object SerialTest {

  def main(arg: Array[String]) {
    val m = HashMap("a" -> 1, "b" -> 2)
    require(StringIntMap.deSerializeMap(StringIntMap.serializeMap(m))("a") == 1,
      s"""
         |-- ${m.toString}
         |-- ${StringIntMap.serializeMap(m)},
         |-- ${StringIntMap.deSerializeMap(StringIntMap.serializeMap(m)).toString}
        """)
    val m1 = HashMap("a" -> "asdf", "b" -> "asdf")
    require(StringStringMap.deSerializeMap(StringStringMap.serializeMap(m1))("a") == 1,
      s"""
         |-- ${m1.toString}
         |-- ${StringStringMap.serializeMap(m1)},
         |-- ${StringStringMap.deSerializeMap(StringStringMap.serializeMap(m1)).toString}
      """)
    val m2 = HashMap("a" -> 1f, "b" -> 2f)
    require(StringFloatMap.deSerializeMap(StringFloatMap.serializeMap(m2))("a") == 1,
      s"""
         |-- ${m2.toString}
         |-- ${StringFloatMap.serializeMap(m2)},
         |-- ${StringFloatMap.deSerializeMap(StringFloatMap.serializeMap(m2)).toString}
      """)
  }
}

object Util {
  lazy val hmapRegex: Regex = "\"([^\":\\s,]*)\":\"([^\":\\s,]*)\"".r
  def parseMap(mapRegex: Regex)(str: String): HashMap[String, String] = {
    mapRegex.findAllIn(str)
      .toArray
      .foldLeft(HashMap.empty[String, String])((y, x) => x match {
        case mapRegex(key, value) => y + (key -> value)
        case other                => y
      })
  }

  def parseArg(arg: Array[String]): HashMap[String, String] =
    parseMap("--([^=\\s]*)=([^=\\s]*)".r)(arg.mkString(" "))

  def time[R](tag: String)(block: => R): R = {
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val t1 = System.nanoTime()
    println(s"$tag Elapsed time: " + (t1 - t0) + "ns")
    result
  }

  def map2HashMap[A, B](map: scala.collection.Map[A, B]): HashMap[A, B] =
    map.foldLeft(HashMap.empty[A, B])(_ + _)

  def map2HashMap[A, B](map: Map[A, B]): HashMap[A, B] =
    map.foldLeft(HashMap.empty[A, B])(_ + _)

  def unsafe[T](default: T)(eval: => T) = try { val value = eval; value } catch { case _ => default }

  case class RddOPS[T](data: RDD[T]) {
    def saveTo(path: String): Unit = {
      data.map(_.toString).saveAsTextFile(path)
    }
  }

  implicit def put[T](data: RDD[T]): RddOPS[T] = RddOPS(data)

  def double2Bytes(data: Double) = {
    val bytes: ByteBuffer = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN)
    bytes.putDouble(data).array()
  }

  def long2Bytes(data: Long) = {
    val bytes: ByteBuffer = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN)
    bytes.putLong(data).array()
  }

  def tod2ID(tod: Int) = {
    val segments = Array(2,6,10,12,15,18,21)
      if (tod <0 || tod> 23){
        -1
      }
    else{
        val l = segments.zipWithIndex.filter(x => x._1>tod)
        if (l.length == 0){
          0
        }
        else{
          segments.length - l.length
        }
      }
  }

}
