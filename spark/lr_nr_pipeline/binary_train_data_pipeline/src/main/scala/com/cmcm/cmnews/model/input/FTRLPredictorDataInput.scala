package com.cmcm.cmnews.model.input

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters

/**
  * Created by lilonghua on 16/9/28.
  */
trait FTRLPredictorDataInput extends EventBatchInput{
  this: SparkBatchContext =>
  import Parameters._

  override def getInput(): Unit ={
    logInfo("start FTRLPredictorDataInput...")
    setRDD(constant_lr_model_path, constant_lr_model_rdd, this)
    setRDD(constant_lr_train_path, constant_lr_train_rdd, this)
  }

}
