package com.cmcm.cmnews.model.output

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters
import org.apache.spark.rdd.RDD

import scala.util.Try

/**
  * Created by lilonghua on 16/9/28.
  */
trait FTRLPredictorDataOutput extends EventBatchOutput{
  this:SparkBatchContext =>
  import Parameters._

  override def outPut: Unit ={
    super.outPut
    logInfo("Start FTRLPredictorDataOutput...")
    val xfbTrainDataOut = rddContext(constant_lr_score_rdd).asInstanceOf[RDD[String]]
    xfbTrainDataOut.saveAsTextFile(batchContext(constant_lr_score_path))

    val evalFlag = Try(batchContext(constant_lr_evaluate)).getOrElse("true").toBoolean
    if (evalFlag) {
      val evalRDD = rddContext(constant_lr_evaluate_rdd).asInstanceOf[RDD[String]]
      evalRDD.saveAsTextFile(Try(batchContext(constant_lr_evaluate_path)).getOrElse("/tmp/news_model/lr_eval"))
    }
  }
}
