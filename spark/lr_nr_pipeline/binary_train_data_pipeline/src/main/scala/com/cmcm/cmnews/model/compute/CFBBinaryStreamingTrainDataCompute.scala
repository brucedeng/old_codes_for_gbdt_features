package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.feature.{LRBFeature, RuleCase, _}
import com.cmcm.cmnews.model.processor._
import com.cmcm.cmnews.model.spark.SparkStreamingContext
import com.cmcm.cmnews.model.util.{LoggingUtils, Parameters}
import com.cmcm.cmnews.model.util.Parameters._
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream._
import org.apache.spark.storage.StorageLevel
import org.joda.time.DateTime

import scala.util.Try

/**
 * Created by tangdong on 1/5/16.
 */
trait CFBBinaryStreamingTrainDataCompute extends EventStreamingCompute{
  this: SparkStreamingContext =>
  import Parameters._

  override def compute: Unit = {
    super.compute
    logInfo("Start CFBTrainDataCompute")
    //get rdd from path
    val eventRdd = getDStream[DStream[String]](constant_event_rdd,this)
    val cpRdd = getDStream[DStream[String]](constant_content_rdd, this)
    val upRdd = getRDD[RDD[String]](constant_user_rdd, this)
    //val cfbRdd = getRDD[RDD[String]](constant_xfb_rdd,this)

    val parallel = batchContext(constant_parallelism).toInt
    //get parsed rdd
    val parsedEventRdd: DStream[(String, String)] = EventStreamingProcessor.process(eventRdd, this.getBatchContext)
    val parsedCpRdd: DStream[(String, String)] = CPStreamingProcessor.process(cpRdd, this.getBatchContext)
    val parsedUpRdd: RDD[(String, String)] = UPProcessor.process(upRdd, this.getBatchContext)
    //val parsedCfbRdd: RDD[(String,String)] = CFBProcessor.process(cfbRdd, this.getBatchContext)

    //join rdd
    val eventJoinUpRdd = UserStreamingFeature.joinWithBatchEvent(parsedUpRdd, parsedEventRdd,parallel)
    val eventJoinUpAndCpRdd = ContentStreamingFeature.joinWithBatchEvent(parsedCpRdd, eventJoinUpRdd,parallel)
      .map(line => {
        val r = scala.util.Random
        (r.nextInt(parallel*5),line)
      })
      .groupByKey(parallel)
      .flatMap(line => line._2)
     // .persist(StorageLevel.MEMORY_AND_DISK_SER)

    val runDateTime = batchContext(constant_date_time)
    val timeStamp = new DateTime(runDateTime).getMillis/1000

    val xfbConfigFileName = if(batchContext contains constant_config_file) batchContext(constant_config_file)
    else ""
    val config = Map[String,String] (constant_date_time -> timeStamp.toString,
      constant_d_ncat -> batchContext(constant_d_ncat),
      constant_d_nkey -> batchContext(constant_d_nkey),
      constant_u_ncat -> batchContext(constant_u_ncat),
      constant_u_nkey -> batchContext(constant_u_nkey),
      constant_parallelism -> batchContext(constant_parallelism),
      constant_config_file -> xfbConfigFileName,
      constant_multi_lan -> Try(batchContext(constant_multi_lan)).getOrElse("false").toLowerCase())

    val eventAndXFBFeature: (DStream[(String,String)]) = XFBBinaryFeatureKey.generateFullXFBFeaturesStreaming(eventJoinUpAndCpRdd, config)
    //val eventStaticFeatures = (eventAndXFBFeature).persist(StorageLevel.MEMORY_AND_DISK_SER)
    //val XFBFeatures:RDD[(String,String)] = (eventAndXFBFeature._2).persist(StorageLevel.MEMORY_AND_DISK_SER)

    // format xfb feature to json
    //val eventWithXFB: RDD[(String, String)] = CFBBinaryFeature.joinWithBatchEvent(parsedCfbRdd, XFBFeatures, this.batchContext)

    //val eventWithStaticFeatureAndXFB: RDD[(String,String)] = CFBBinaryFeature.joinWithStaticFeature(eventWithXFB,eventStaticFeatures)

    val trainDataOut = eventAndXFBFeature.map(line => {
      val key = line._2.split(fieldDelimiter).drop(0).mkString("\t")
      s"$key"
    })

    //LR train data format
    val parsedLrRdd: DStream[Instance] = LRBStreamingProcessor.process(trainDataOut, this.getBatchContext)

    val rule = RuleCase.wordCountRule

    /*val filteredData = LRBFeature.calculateWeightStream(LRBFeature.filterInstanceStream(rule)(parsedLrRdd))

    val lrTrainDataOut = filteredData map {
      case x =>
        x.base.uid + "\t" + x.base.dCid + "\t" + x.base.label + ":" + x.base.weight + "\t" + x.discreFeature.map(f => f._1.replaceAll(" ","_") + "_" + f._2._2.toString).mkString(" ")
    }*/

    //rddContext += (constant_train_rdd -> lrTrainDataOut)
    rddContext += (constant_train_rdd -> trainDataOut)

  }
}
