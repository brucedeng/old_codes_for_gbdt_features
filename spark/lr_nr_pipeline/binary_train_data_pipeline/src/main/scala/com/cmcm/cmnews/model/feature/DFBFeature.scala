package com.cmcm.cmnews.model.feature

import com.cmcm.cmnews.model.feature.CFBFeature._
import com.cmcm.cmnews.model.util.{LoggingUtils, JsonUtil, Parameters}
import com.cmcm.cmnews.model.util.Parameters._
import org.apache.spark.Logging
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.joda.time.DateTime

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.Random

/**
 * Created by tangdong on 9/5/16.
 */
object DFBFeature extends Logging{

  import Parameters._

  def joinWithBatchEvent(XFBFeatureRdd: RDD[(String, String)], eventRdd: RDD[(String, String)], batchContext:collection.mutable.Map[String,String]) = {
    logInfo("Now, join xfb feature and event")
    val windSize = batchContext(constant_wnd)
    val prevDateTime = batchContext(constant_prev_date_time)
    val prevTimeStamp = try { new DateTime(prevDateTime).getMillis/1000 - windSize.toLong } catch {
      case e:Exception => 0
    }
    val posDateTime = batchContext(constant_date_time)
    val posTimeStamp = try { new DateTime(posDateTime).getMillis/1000 - windSize.toLong } catch {
      case e:Exception => 0
    }
    val decayWind = batchContext(constant_decay_wind)
    val num = decayWind.slice(0, decayWind.size - 1).toInt
    val period = decayWind.last match {
      case x:Char if x.toString == "h" => 6
      case x:Char if x.toString == "d" => 144
      case _ => 0
    }
    val interval = num * period * windSize.toInt - windSize.toInt
    val topNum = batchContext(constant_top_num).toInt
    val randomNum = batchContext(constant_random_num).toInt
    val configMap = Map[String, String](
      constant_date_time -> posTimeStamp.toString,
      constant_prev_date_time -> prevTimeStamp.toString,
      constant_interval -> interval.toString,
      constant_wnd -> windSize,
      constant_decay_factor -> batchContext(constant_decay_factor))

    val eventRddPer = eventRdd.persist(StorageLevel.MEMORY_AND_DISK_SER)

    val skewedFeature = eventRddPer.sample(false,0.05).map(line=> (line._1,1)).reduceByKey(_+_)
      .map(line => (line._2,line._1))
      .sortByKey(false).take(topNum).map(line=>line._2).toSet
    skewedFeature.foreach(line => LoggingUtils.loggingError("skewed feature: " + line))
    //split eventRdd into two rdds ,one with normal feature keys, the other with skewed feature keys
    val normalEventRdd = eventRddPer.filter(line => {
      if(skewedFeature.contains(line._1)){
        false
      }else{
        true
      }
    })
    val skewedEventRdd = eventRddPer.filter(line => {
      if(skewedFeature.contains(line._1)){
        true
      }else{
        false
      }
    }).map(line => {
      val randomObject = new Random()
      val key = randomObject.nextInt(randomNum) + fieldDelimiter + line._1
      (key, line._2)
    })

    //split XFBfeatureRdd into two rdds
    val normalFeatureRdd = XFBFeatureRdd.filter(line => {
      if(skewedFeature.contains(line._1)){
        false
      }else{
        true
      }
    })
    val skewedFeatureRdd = XFBFeatureRdd.filter(line => {
      if(skewedFeature.contains(line._1)){
        true
      }else{
        false
      }
    }).flatMap(line=> {
      val outList = new ListBuffer[(String,String)]()
      for(index <- 0 to randomNum){
        outList += ((index + fieldDelimiter + line._1, line._2))
      }
      outList.toList
    })

    val initialSet = collection.mutable.HashSet.empty[String]
    val addToSet = (s: collection.mutable.HashSet[String], v: String) => s += v
    val mergePartitionSets = (p1: collection.mutable.HashSet[String], p2: collection.mutable.HashSet[String]) => p1 ++= p2
    val normalJoinFeatureRdd = normalEventRdd.aggregateByKey(initialSet)(addToSet, mergePartitionSets)
      .leftOuterJoin(normalFeatureRdd).flatMap(result => {
      flattenEvent(result,configMap,false)
    }).aggregateByKey(initialSet)(addToSet,mergePartitionSets)

    val screwJoinFeatureRdd = skewedEventRdd.aggregateByKey(initialSet)(addToSet, mergePartitionSets)
      .leftOuterJoin(skewedFeatureRdd).flatMap(result => {
      flattenEvent(result,configMap,true)
    }).aggregateByKey(initialSet)(addToSet, mergePartitionSets)

    normalJoinFeatureRdd.union(screwJoinFeatureRdd)
      .aggregateByKey(initialSet)(mergePartitionSets,mergePartitionSets)
      .map((line: (String, mutable.HashSet[String])) => {
      val key = line._1
      val records = line._2
      val xfb_out_dict = collection.mutable.Map[String, AnyRef]()
      records.map((record: String) => {
        val items = record.split(fieldDelimiter)
        val featureWeight = items(0)
        val featureType = if (items.size > 1) items(1) else ""
        val featureName = if (items.size > 2) items(2) else ""
        val click = if (items.size > 3) items(3) else ""
        val view = if (items.size > 4) items(4) else ""
        val dwell = if (items.size > 5) items(5) else ""
        if (featureType.contains("KEYWORD") || featureType.contains("CATEGORY")) {
          if (xfb_out_dict.contains(featureType)) {
            val featureDict = xfb_out_dict(featureType).asInstanceOf[Map[String, List[String]]]
            xfb_out_dict.put(featureType, featureDict + (featureName -> List(click, view, dwell, featureWeight)))
          } else {
            xfb_out_dict.put(featureType, Map(featureName -> List(click, view, dwell, featureWeight)))
          }
        } else {
          xfb_out_dict.put(featureType, List(click, view, dwell, featureWeight))
        }
      })
      (key, JsonUtil.toJson(xfb_out_dict))
    })
  }

  def flattenEvent(record:(String,(mutable.HashSet[String],Option[String])), config:Map[String,String],skewed:Boolean = false) = {
    val interval = config(constant_interval).toInt
    val decayFactor = config(constant_decay_factor).toDouble
    val beginTimeStamp = config(constant_prev_date_time).toLong
    val windSize = config(constant_wnd).toInt
    val endTimeStamp = config(constant_date_time).toLong
    val keyItems = record._1.split(fieldDelimiter)
    val featureType = if(skewed) keyItems(2) else keyItems(1)
    val featureName = if(skewed) keyItems(3) else keyItems(2)
    val eventSet = record._2._1
    val defaultFeature = featureType + fieldDelimiter + featureName + fieldDelimiter + "" + fieldDelimiter + "" + fieldDelimiter + ""
    val eventOut = new ListBuffer[(String, String)]
    record._2._2 match {
      case Some(feature) =>
        val recordDict = collection.mutable.Map[String, (Double, Double, Double)]()
        val records = feature.split(recordDelimiter)
        records.map(line => {
          val items = line.split(fieldDelimiter)
          val ts = items(0)
          val click = items(1).toDouble
          val view = items(2).toDouble
          val dwell = items(3).toDouble
          recordDict += (ts ->(click, view, dwell))
        })

        val aggFeatureDict = collection.mutable.Map[String,(Double,Double, Double)]()
        val origTimestamp = beginTimeStamp - interval
        var prevClick = 0.0
        var prevView = 0.0
        var prevDwell = 0.0
        for (timestamp <- origTimestamp to beginTimeStamp + 1 by windSize) {
          val values = if (recordDict.contains(timestamp.toString)) {
            val value = recordDict(timestamp.toString)
            val currentClick = value._1
            val currentView = value._2
            val currentDwell = value._3
            (prevClick * decayFactor + currentClick, prevView * decayFactor + currentView, prevDwell * decayFactor + currentDwell)
          } else {
            (prevClick * decayFactor, prevView * decayFactor,prevDwell * decayFactor)
          }
          prevClick = values._1
          prevView = values._2
          prevDwell = values._3
        }
        aggFeatureDict += (beginTimeStamp.toString -> (prevClick,prevView, prevDwell))

        for(timestamp <- beginTimeStamp + windSize to endTimeStamp + 1 by windSize) {
          val currentValues = recordDict.getOrElse(timestamp.toString, (0.0,0.0,0.0))
          val prevTimestamp = timestamp - windSize
          val prevValues = aggFeatureDict.getOrElse(prevTimestamp.toString, (0.0,0.0,0.0))
          val subtractTimestamp = timestamp - interval
          val subtractValues = recordDict.getOrElse(subtractTimestamp.toString, (0.0,0.0,0.0))
          val decayDwell = currentValues._3 + (prevValues._3 - subtractValues._3 * math.pow(decayFactor,interval - 1)) * decayFactor
          val decayView = currentValues._2 + (prevValues._2 - subtractValues._2 * math.pow(decayFactor,interval - 1)) * decayFactor
          val decayClick = currentValues._1 + (prevValues._1 - subtractValues._1 * math.pow(decayFactor,interval - 1)) * decayFactor

          aggFeatureDict += (timestamp.toString -> (decayClick,decayView, decayDwell))
        }

        eventSet.map(line => {
          val items = line.split(fieldDelimiter)
          val ts = items(0).toLong
          val weight = items(1)
          val prefix = items.slice(2,4).mkString(fieldDelimiter)
          if (aggFeatureDict.contains(ts.toString)){
            val values = aggFeatureDict(ts.toString)
            val aggXfbFeatures = featureType + fieldDelimiter + featureName + fieldDelimiter + values._1 + fieldDelimiter + values._2 + fieldDelimiter + values._3
            eventOut += ((prefix, weight + fieldDelimiter + aggXfbFeatures))
          }
        })
      case None =>
        eventSet.map(line => {
          val items = line.split(fieldDelimiter)
          val weight = items(1)
          val prefix = items.slice(2,4).mkString(fieldDelimiter)
          eventOut += ((prefix, weight + fieldDelimiter + defaultFeature))
        })
    }
    eventOut.toList

  }

  def joinWithStaticFeature(XFBFeatureRdd: RDD[(String, String)], eventRdd: RDD[(String, String)]) = {
    eventRdd.leftOuterJoin(XFBFeatureRdd).map(result => {
      val staticFeatures = result._2._1.split(fieldDelimiter).drop(0).mkString(fieldDelimiter)
      val xfbFeatures = result._2._2.getOrElse("")
      (staticFeatures,xfbFeatures)
    })
  }
}
