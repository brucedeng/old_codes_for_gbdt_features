package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.UPNewFilterCompute
import com.cmcm.cmnews.model.input.UPFilterDataInput
import com.cmcm.cmnews.model.output.XFBBinaryTrainDataOutput
import com.cmcm.cmnews.model.spark.NormalSparkContext

/**
  * Created by lilonghua on 16/9/22.
  */
object UPNewFilterApp extends BatchApp
  with NormalSparkContext
  with UPFilterDataInput
  with UPNewFilterCompute
  with XFBBinaryTrainDataOutput
