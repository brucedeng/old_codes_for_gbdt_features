package com.cmcm.cmnews.model.output

import com.cmcm.cmnews.model.component.StreamingComponent
import com.cmcm.cmnews.model.spark.SparkStreamingContext

/**
 * Created by tangdong on 27/4/16.
 */
trait EventStreamingOutput extends StreamingComponent{
  this:SparkStreamingContext =>
  def outPut: Unit = {
    logInfo("Empty output in EventStreamingOutput...")
  }
}
