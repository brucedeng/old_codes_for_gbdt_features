package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.{DFBTrainDataCompute, CFBTrainDataCompute}
import com.cmcm.cmnews.model.input.XFBTrainDataInput
import com.cmcm.cmnews.model.output.XFBTrainDataOutput
import com.cmcm.cmnews.model.spark.XFBTrainDataSparkContext

/**
 * Created by tangdong on 9/5/16.
 */
object DFBTrainDataApp extends BatchApp
with XFBTrainDataSparkContext
with XFBTrainDataInput
with DFBTrainDataCompute
with XFBTrainDataOutput
