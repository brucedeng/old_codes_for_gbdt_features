package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.config.Config
import com.cmcm.cmnews.model.feature._
import com.cmcm.cmnews.model.processor._
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.joda.time.DateTime

import scala.util.Try


/**
  * Created by tangdong on 1/5/16.
  */
trait CFBBinaryTrainDataCompute extends EventBatchCompute {
  this: SparkBatchContext =>

  import Parameters._

  override def compute: Unit = {
    super.compute
    logInfo("Start CFBTrainDataCompute")
    val dataType = Try(batchContext(constant_event_data_type)).getOrElse("minutes")
    val debugFlag = Try(batchContext(constant_debug)).getOrElse("false")
    val negDedup = Try(batchContext(constant_negdedup)).getOrElse("false")

    //generate event data from log
    val pvRdd = getRDD[RDD[String]](constant_event_pv_rdd, this)
    val clickRdd = getRDD[RDD[String]](constant_event_click_rdd, this)
    val readtimeRdd = getRDD[RDD[String]](constant_event_readtime_rdd, this)

    val parsePvRdd = SourceEventProcessor.processAct(pvRdd, this.getBatchContext, "1")
    val parseClickRdd: RDD[(String, String)] = SourceEventProcessor.processAct(clickRdd, this.getBatchContext, "2")

    /*
      负样本
      如果去重, 对于天级数据, 正样本是click, 负样本是未被click过的pv
      对于其他粒度的数据, 正样本是本窗口click, 负样本是上一个窗口的pv减去上一个窗口和本窗口的click
     */
    val pvNotclickRdd =
      if (negDedup == "true") {
        if (dataType == "day") {
          parsePvRdd.leftOuterJoin(parseClickRdd).filter(f => {
            f._2._2.getOrElse("").isEmpty
          }).map(f => f._2._1 + fieldDelimiter + "0")
        }
        else {
          val pvClickRdd = getRDD[RDD[String]](constant_event_pv_click_rdd, this)
          val parsePvClickRdd = SourceEventProcessor.processAct(pvClickRdd, this.getBatchContext, "2").map(line => (line._1, 1)).collectAsMap()
          val pvClickBroadcast = getSparkContext.broadcast(parsePvClickRdd)
          parsePvRdd.filter(x => !pvClickBroadcast.value.contains(x._1)).map(line => {
            line._2 + fieldDelimiter + "0"
          })
        }
      }
      else {
        println("PV is not deduped as negative samples")
        parsePvRdd.map(line => line._2 + fieldDelimiter + "0")
      }



    /*
      正样本当前窗口的click
    * */
    val parseReadtimeRdd = SourceEventProcessor.processAct(readtimeRdd, this.getBatchContext, "4").map(line => (line._1, line._2.toInt)).collectAsMap()
    val readtimeBroadcast = getSparkContext.broadcast(parseReadtimeRdd)
    val clickWithReadtimeRdd = parseClickRdd.map(line => {
      var readtime = readtimeBroadcast.value.getOrElse(line._1, 70)
      line._2 + fieldDelimiter + readtime.toString
    })


    //merge click and pv
    val pvUnionClickRdd = pvNotclickRdd.union(clickWithReadtimeRdd).zipWithUniqueId().map(line => line._2.toString + fieldDelimiter + line._1)
    if (debugFlag == "true") {
      println(s"event processPost count => ${pvNotclickRdd.count()}")
    }
    /*logInfo(pvNotclickRdd.count().toString)
    pvClickBroadcast.destroy()
    readtimeBroadcast.destroy()*/
    //DEBUG
    //pvUnionClickRdd.saveAsTextFile("/tmp/dl/pvUnionClickRdd")

    //get rdd from path
    //val eventRdd = getRDD[RDD[String]](constant_event_rdd,this)
    val cpRdd = getRDD[RDD[String]](constant_content_rdd, this)
    val upRdd = getRDD[RDD[String]](constant_user_rdd, this)

    //lr params
    val pvBound = Try(batchContext(constant_lr_pvBound)).getOrElse("0").toInt
    val startIdx = Try(batchContext(constant_lr_startIdx)).getOrElse("500").toInt

    val parallel = Try(batchContext(constant_parallelism)).getOrElse("100").toInt
    val parallelMax = Try(batchContext(constant_parallelismMax)).getOrElse("500").toInt


    val parsedEventRdd: RDD[(String, String)] = EventProcessor.processNew(pvUnionClickRdd, this.getBatchContext)
    val parsedCpRdd: RDD[(String, String)] = CPProcessor.process(cpRdd, this.getBatchContext)
    val parsedUpRdd: RDD[(String, String)] = UPProcessor.process(upRdd, this.getBatchContext)
    //logInfo("UPfilter Count : " + parsedUpRdd.count())
    //logInfo("parsedEventRdd Count : " + parsedEventRdd.count())
    //join rdd
    val eventJoinUpRdd = UserFeature.joinWithBatchEvent(parsedUpRdd, parsedEventRdd, parallelMax)
    /*val eventJoinUpAndCpRdd = ContentFeature.joinWithBatchEvent(parsedCpRdd, eventJoinUpRdd, parallelMax)
      .map(line => {
        val r = scala.util.Random
        (r.nextInt(parallel * 5), line)
      })
      .groupByKey(parallel)
      .flatMap(line => line._2)
      .persist(StorageLevel.MEMORY_AND_DISK_SER)*/
    //logInfo("eventJoinUpRdd Count : " + eventJoinUpRdd.count())
    if (debugFlag == "true") {
      println(s"eventJoinUpRdd processPost count => ${eventJoinUpRdd.count()}")
    }

    val eventJoinUpAndCpRdd = ContentFeature.joinWithBatchEvent(parsedCpRdd, eventJoinUpRdd, parallelMax)
        .coalesce(parallel)
      .persist(StorageLevel.MEMORY_AND_DISK_SER)
    //logInfo("eventJoinUpAndCpRdd Count : " + eventJoinUpAndCpRdd.count())
    if (debugFlag == "true") {
      println(s"eventJoinUpAndCpRdd processPost count => ${eventJoinUpAndCpRdd.count()}")
    }

    val runDateTime = batchContext(constant_date_time)
    val timeStamp = new DateTime(runDateTime).getMillis / 1000

    val xfbConfigFileName = if (batchContext contains constant_config_file) batchContext(constant_config_file)
    else ""
    val config = Map[String, String](constant_date_time -> timeStamp.toString,
      constant_d_ncat -> batchContext(constant_d_ncat),
      constant_d_nkey -> batchContext(constant_d_nkey),
      constant_u_ncat -> batchContext(constant_u_ncat),
      constant_u_nkey -> batchContext(constant_u_nkey),
      constant_parallelism -> batchContext(constant_parallelism),
      constant_config_file -> xfbConfigFileName,
      constant_multi_lan -> Try(batchContext(constant_multi_lan)).getOrElse("false").toLowerCase())

    val eventAndXFBFeature: (RDD[(String, String)]) = XFBBinaryFeatureKey.generateFullXFBFeatures(eventJoinUpAndCpRdd, config)

    val trainDataOut = eventAndXFBFeature.map(line => {
      val key = line._2.split(fieldDelimiter).drop(0).mkString("\t")
      s"$key"
    })
    eventJoinUpAndCpRdd.unpersist()

    //LR train data format
    val parsedLrRdd: RDD[Instance] = LRBProcessor.process(trainDataOut, this.getBatchContext)
    //logInfo("LRBProcessor parsedLrRdd Count : " + parsedLrRdd.count())

    val rule = RuleCase.wordCountRule

    val filteredData = LRBFeature.calculateWeight(LRBFeature.filterInstance(rule)(parsedLrRdd))
    if (debugFlag == "true") {
      println(s"filteredData processPost count => ${filteredData.count()}")
    }

    val typesafeConfig = new Config(config(constant_config_file)).typesafeConfig
    val featJoin = Try(typesafeConfig.getString("cfb.feat_join")).getOrElse("_")
    val featSplit = Try(typesafeConfig.getString("cfb.feat_split")).getOrElse(" ")
    val replaceSplit = Try(typesafeConfig.getString("cfb.replace_split")).getOrElse("_")
    val lrTrainDataOut = filteredData map {
      case x =>
        x.base.uid + "\t" + x.base.dCid + "\t" + x.base.label + ":" + x.base.weight + "\t" + x.discreFeature.map(f => f._1.replace("\n",featSplit).replace(featSplit, replaceSplit)).map(f => if (f.contains(featJoin)) f else f + featJoin).mkString(featSplit)
    }

    rddContext += (constant_train_rdd -> lrTrainDataOut)
  }

}
