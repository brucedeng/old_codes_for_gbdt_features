package com.cmcm.cmnews.model.spark

import com.cmcm.cmnews.model.util.Parameters
import org.apache.spark.{SparkConf, SparkContext}

import scala.util.Try

/**
  * Created by lilonghua on 16/9/14.
  */

trait NormalSparkContext extends SparkBatchContext{
  import Parameters._

  override def init(): Unit ={
    super.init()
    logInfo("Start NormalSparkContext...")
    val sparkConf = new SparkConf()
      .set("spark.serializer","org.apache.spark.serializer.KryoSerializer")
      .set("spark.default.parallelism", Try(batchContext(constant_parallelism)).getOrElse("32"))
    sbc = new SparkContext(sparkConf)
  }

}