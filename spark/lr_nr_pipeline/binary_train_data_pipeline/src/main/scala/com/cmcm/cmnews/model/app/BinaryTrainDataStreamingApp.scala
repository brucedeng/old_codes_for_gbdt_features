package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.CFBBinaryStreamingTrainDataCompute
import com.cmcm.cmnews.model.input.XFBBinaryStreamingTrainDataInput
import com.cmcm.cmnews.model.output.XFBBinaryStreamingTrainDataOutput
import com.cmcm.cmnews.model.spark.XFBBinaryStreamingTrainDataSparkContext

/**
 * Created by tangdong on 3/5/16.
 */
object BinaryTrainDataStreamingApp extends StreamingApp
with XFBBinaryStreamingTrainDataSparkContext
with XFBBinaryStreamingTrainDataInput
with CFBBinaryStreamingTrainDataCompute
with XFBBinaryStreamingTrainDataOutput
