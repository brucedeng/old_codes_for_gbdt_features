package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.CFBTrainDataCompute
import com.cmcm.cmnews.model.input.XFBTrainDataInput
import com.cmcm.cmnews.model.output.XFBTrainDataOutput
import com.cmcm.cmnews.model.spark.XFBTrainDataSparkContext

/**
 * Created by tangdong on 3/5/16.
 */
object CFBTrainDataApp extends BatchApp
with XFBTrainDataSparkContext
with XFBTrainDataInput
with CFBTrainDataCompute
with XFBTrainDataOutput
