package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.NDFBTrainDataCompute
import com.cmcm.cmnews.model.input.XFBTrainDataInput
import com.cmcm.cmnews.model.output.XFBTrainDataOutput
import com.cmcm.cmnews.model.spark.XFBTrainDataSparkContext

/**
 * Created by tangdong on 17/5/16.
 */
object NDFBTrainDataApp extends BatchApp
with XFBTrainDataSparkContext
with XFBTrainDataInput
with NDFBTrainDataCompute
with XFBTrainDataOutput

