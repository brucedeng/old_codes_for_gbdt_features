package com.cmcm.cmnews.model.input

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters

/**
  * Created by lilonghua on 16/10/9.
  */
trait FTRLSerializeDataInput extends EventBatchInput{
  this: SparkBatchContext =>
  import Parameters._

  override def getInput(): Unit ={
    logInfo("start FTRLSerializeDataInput...")
    setRDD(constant_lr_model_path, constant_lr_model_rdd, this)
  }
}
