package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.CFBBinaryTrainDataCompute
import com.cmcm.cmnews.model.input.XFBBinaryTrainDataInput
import com.cmcm.cmnews.model.output.XFBBinaryTrainDataOutput
import com.cmcm.cmnews.model.spark.XFBTrainDataSparkContext

/**
 * Created by tangdong on 3/5/16.
 */
object BinaryTrainDataApp extends BatchApp
with XFBTrainDataSparkContext
with XFBBinaryTrainDataInput
with CFBBinaryTrainDataCompute
with XFBBinaryTrainDataOutput