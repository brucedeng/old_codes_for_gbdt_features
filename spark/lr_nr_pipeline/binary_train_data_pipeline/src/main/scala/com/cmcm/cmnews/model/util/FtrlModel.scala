package com.cmcm.cmnews.model.util

/**
  * Created by lilonghua on 16/10/9.
  */
case class FtrlModel(key: Long, weight: Double) extends Serializable
