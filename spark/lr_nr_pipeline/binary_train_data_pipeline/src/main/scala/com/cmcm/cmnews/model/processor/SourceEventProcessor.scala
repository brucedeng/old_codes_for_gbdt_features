package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{JsonUtil, LoggingUtils, Parameters}
import com.cmcm.cmnews.model.util.Parameters._
import org.apache.spark.rdd.RDD
import org.json4s.JsonAST._

import scala.util.{Failure, Success, Try}
import scala.util.control.Breaks._

/**
 * Created by tangdong on 27/4/16.
 */
object SourceEventProcessor extends Processor {
  this: SparkBatchContext =>
  import Parameters._

  def preprocessAct(line:String,batchContext: collection.mutable.Map[String,String], filterAct:String) = {
    val eventPid = Try(batchContext(constant_event_pid)).getOrElse("0")
    val cType = Try(batchContext(constant_event_ctype)).getOrElse("1,0x01")
    val eventLanRegion = Try(batchContext(constant_event_lan_region)).getOrElse("")
    val position = Try(batchContext(constant_event_position)).getOrElse("")
    val WND = Try(batchContext(constant_wnd)).getOrElse("600").toInt
    try {
      Try(JsonUtil.convertToJValue(line)) match {
        case Success(jvalue) =>
          val uid = extractStr(jvalue, "aid", "")
          val tApplan = extractStr(jvalue, "app_lan", "en")
          val lan = if (tApplan.contains("hi")) "hi" else tApplan
          val country = extractStr(jvalue, "country", "")
          val cid = extractStr(jvalue, "contentid", "")
          val ip = extractStr(jvalue, "ip", "unknown")
          val ts = extractStr(jvalue, "servertime_sec", "0").toLong
          val pid = extractStr(jvalue, "pid", "unknown")
          val ctype = extractStr(jvalue, "ctype", "1")
          val act = extractStr(jvalue, "act", "unknown")
          val city = extractStr(jvalue, "city", "unknown")
          val des = extractObjectStr(jvalue, "cpack", "des", "unknown")
          val dwelltime = extractObjectStr(jvalue, "ext", "dwelltime", "0").toInt
          val level1Type = extractObjectStr(jvalue, "scenario", "level1_type", "unknown")
          val level1 = extractObjectStr(jvalue, "scenario", "level1", "unknown")
          val lanRegion = lan + "_" + country
          val join_ts = ts / WND * WND - WND
          var dwell = 0
          if ("4" == act) {
            if (dwelltime > 600) {
              dwell = 600
            } else {
              dwell = dwelltime
            }
          }

          val reqid = parseRid(des)

          val flag = if (pid.nonEmpty && position.nonEmpty && cType.nonEmpty) {
            position.split(",").contains(s"${level1Type}_$level1") && cType.split(",").contains(ctype)
          } else {
            if (pid == "11") {
              ("1" == level1Type || "10" == level1Type) && "1" == level1 && ("" == ctype || "1" == ctype || "0x01" == ctype)
            } else {
              "" == ctype || "1" == ctype || "0x01" == ctype
            }
          }

          if (uid.nonEmpty && uid != "unknown" && eventPid == pid && flag && filterAct == act && eventLanRegion.nonEmpty
            && eventLanRegion.split(",").exists(p => {p.contains(lanRegion.toLowerCase) || lanRegion.toLowerCase.contains(p)})
            && cid != "unknown" && cid.nonEmpty) {
            val label = act match {
              case "1" => "0"
              case "2" => "1"
              case _ => "-1"
            }
            val key = uid + fieldDelimiter + cid + fieldDelimiter + reqid + fieldDelimiter + pid
            var value = List(uid, lanRegion, cid, ts, join_ts, "", s"${pid}_${level1Type}_$level1", city, reqid, label).mkString(fieldDelimiter)
            if ("4" == filterAct) {
              value = dwell.toString
            }
            (key, value)
          } else {
            ("", "")
          }
        case Failure(ex) =>
          LoggingUtils.loggingError(LoggingUtils.getException(ex))
          ("", "")
      }
    } catch {
      case e: Exception =>
        LoggingUtils.loggingError(e.toString)
        ("", "")
    }
  }



  def processAct(inputRDD:RDD[String], batchContext: collection.mutable.Map[String,String], act:String): RDD[(String, String)] = {
    val afterProcess:RDD[(String,String)] = inputRDD.map((line:String) => preprocessAct(line, batchContext, act)).filter(X => X._1 != "")
    if (act == "4") {
      afterProcess.reduceByKey((x,y) => math.max(x.toInt, y.toInt).toString).map(line => {
        var value = line._2.toInt
        if (value > 600) {
          value = 600
        }
        (line._1, value.toString)
      })
    } else {
      afterProcess.reduceByKey((sx,sy) => {
        var x = sx.split(fieldDelimiter)
        var y = sy.split(fieldDelimiter)
        val smax = (x1:String,y1:String) => if(x1.compareTo(y1) > 0) x1 else y1
        val smin = (x1:String,y1:String) => if(x1.compareTo(y1) < 0) x1 else y1
        Array(x(0), smax(x(1).toString, y(1).toString), x(2), math.min(x(3).toInt, y(3).toInt), math.min(x(4).toInt,y(4).toInt),
          x(5), x(6), smin(x(7).toString,y(7).toString), x(8), x(9)).mkString(fieldDelimiter)
      })
    }
  }


  def catKwAssist(jsonItems: List[JValue], version:String, top:Int, default:String): String = {
    def parse_item(item:JValue) = {
      val name = (item \ "name").asInstanceOf[JString].s
      val L1_weight = item \ "weight" match {
        case JDouble(x) =>
          x.toDouble
        case JInt(x) =>
          x.toDouble
        case _ =>
          0.0
      }
      name + keyValueDelimiter + L1_weight
    }
    jsonItems.map(x => parse_item(x)).take(top).mkString(pairDelimiter)
    //     sb.append(name + keyValueDelimiter + L1_weight + pairDelimiter)
  }

  def extractStr(jvalue: JValue, fieldName: String, default: String = "") = {
    jvalue \ fieldName match {
      case JString(s) => if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString()
      case JDouble(s) => s.toString()
      case _ => default
    }
  }

  def extractObjectStr(jvalue : JValue, objectName: String, fieldName:String, default:  String = "") = {
    jvalue \ objectName \ fieldName match {
      case JString(s) =>  if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString()
      case JDouble(s) => s.toString()
      case _ => default
    }
  }

  def parseRid(des: String) = {
    val data = des.split("\\|")
    val dataMap = scala.collection.mutable.Map[String, String]()
    for (kv <- data) {
      val tArray = kv.split("=")
      if (2 == tArray.size ) {
        dataMap.update(tArray(0), tArray(1))
      }
    }
    dataMap.getOrElse("rid", "")
  }


  def getListFromJson(key:String, line:String, top:Int): String = {
    val sb = new StringBuilder
    var newLine = line.substring(line.indexOf("\"" + key + "\" : ") + 5 + key.length)
    newLine = newLine.substring(0, newLine.indexOf("]") + 1)
    var index = 0
    breakable {
      while (index < (if (top < 0) Integer.MAX_VALUE else top)) {
        if (newLine.indexOf("}") == -1) {
          break()
        }
        val name = getValueForKey("name", newLine)
        val weight = getValueForKey("weight", newLine).toDouble
        sb.append(name + keyValueDelimiter + weight + pairDelimiter)
        newLine = newLine.substring(newLine.indexOf("}") + 1)
        index += 1
      }
    }
    if (index > 0)
      sb.dropRight(1).toString()
    else
      sb.toString()
  }

  def getValueForKey(key: String, record: String) = {
    val startIndex = record.indexOf("\"" + key + "\" : " ) + 5 + key.length
    val commaIndex = record.indexOf(",", startIndex)
    val antiBracketIndex = record.indexOf("}", startIndex)
    val endIndex = if (commaIndex != -1 && antiBracketIndex != -1)  Math.min(record.indexOf(",", startIndex),
      record.indexOf("}", startIndex)) else if (commaIndex != -1) commaIndex else antiBracketIndex
    val mendedStartInedex =
      if (record.charAt(startIndex) == '\"' || record.charAt(startIndex) == '[')
        startIndex + 1
      else
        startIndex
    val mendedEndInedex =
      if (record.charAt(endIndex - 1) == '\"' || record.charAt(endIndex - 1) == ']')
        endIndex - 1
      else
        endIndex

    if (mendedStartInedex > mendedEndInedex)
      ""
    else
      record.substring(mendedStartInedex, mendedEndInedex)
  }

}
