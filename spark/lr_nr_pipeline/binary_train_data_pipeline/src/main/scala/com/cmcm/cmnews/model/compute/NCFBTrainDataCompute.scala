package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.feature._
import com.cmcm.cmnews.model.processor._
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters
import com.cmcm.cmnews.model.util.Parameters._
import org.apache.spark.rdd.RDD
import org.joda.time.DateTime

/**
 * Created by tangdong on 17/5/16.
 */
trait NCFBTrainDataCompute extends EventBatchCompute{
  this: SparkBatchContext =>
  import Parameters._

  override def compute: Unit = {
    super.compute
    logInfo("Start CFBTrainDataCompute")
    //get rdd from path
    val eventRdd = getRDD[RDD[String]](constant_event_rdd,this)
    val cpRdd = getRDD[RDD[String]](constant_content_rdd, this)
    val upRdd = getRDD[RDD[String]](constant_user_rdd, this)
    val cfbRdd = getRDD[RDD[String]](constant_xfb_rdd,this)

    //get parsed rdd
    val parsedEventRdd: RDD[(String, String)] = NEventProcessor.process(eventRdd, this.getBatchContext)
    val parsedCpRdd: RDD[(String, String)] = CPProcessor.process(cpRdd, this.getBatchContext)
    val parsedUpRdd: RDD[(String, String)] = UPProcessor.process(upRdd, this.getBatchContext)
    val parsedCfbRdd: RDD[(String,String)] = CFBProcessor.process(cfbRdd, this.getBatchContext)

    //join rdd
    val eventJoinUpRdd = NUserFeature.joinWithBatchEvent(parsedUpRdd, parsedEventRdd)
    val eventJoinUpAndCpRdd = ContentFeature.joinWithBatchEvent(parsedCpRdd, eventJoinUpRdd)

    val runDateTime = batchContext(constant_date_time)
    val timeStamp = try { new DateTime(runDateTime).getMillis/1000 } catch {
      case e:Exception => 0
    }

    val config = Map[String,String] (constant_date_time -> timeStamp.toString,
      constant_d_ncat -> batchContext(constant_d_ncat),
      constant_d_nkey -> batchContext(constant_d_nkey),
      constant_u_ncat -> batchContext(constant_u_ncat),
      constant_u_nkey -> batchContext(constant_u_nkey))

    val eventAndXFBFeature: (RDD[(String,String)],RDD[(String,String)]) = XFBFeatureKey.generateFullXFBFeatures(eventJoinUpAndCpRdd, config)
    val eventStaticFeatures = eventAndXFBFeature._1
    val XFBFeatures = eventAndXFBFeature._2

    // format xfb feature to json
    val eventWithXFB: RDD[(String, String)] = CFBFeature.joinWithBatchEvent(parsedCfbRdd, XFBFeatures, this.batchContext)

    val eventWithStaticFeatureAndXFB: RDD[(String,String)] = CFBFeature.joinWithStaticFeature(eventWithXFB,eventStaticFeatures)

    val trainDataOut = eventWithStaticFeatureAndXFB.map(line => {
      val key = line._1.split(fieldDelimiter).drop(0).mkString("\t")
      val value = line._2
      s"$key\t$value"
    })
    rddContext += (constant_train_rdd -> trainDataOut)

  }
}