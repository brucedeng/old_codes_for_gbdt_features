package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.util.Parameters._
import org.joda.time.DateTime
import org.scalatest.FunSuite

/**
  * Created by mengchong on 6/24/16.
  */
class TestSourceEventProcessor extends FunSuite{

  test("parse event hi click data") {
    val clickDate = """{"servertime_sec":"1470816047","upack":{"exp":"insta_hindi_094","new_user":"0"},"model":"CP8676_I02","mcc":"404","declared_lan":"","aid":"906d025c672f3193","net":"wifi","nmnc":"78","ext":{"eventtime":"1470816037"},"city":"35_Bhopal","id":"","refer":{},"lan":"hi_IN","action":"","value":"","mnc":"78","apiv":"3","contentid":"YD202f98c2j_in","osv":"5.1","ctype":"1","pid":"11","display":"0x02","api_server_ip":"10.5.10.198","ip":"122.168.42.57","country":"IN","appv":"1.2.0","pf":"android","scenario":{"level2":"29","level1":"1","level1_type":"1","source":"","column":"","is_level1_native":"1"},"servertime":"2016-08-10 08:00:47","app_lan":"hi","cpack":{"des":"rid=42efb16b|src=8|ord=7|page_n=0|hit_sc=0","md5":"YD202f98c2j_in","ext":{"big_img":0},"ishot":1},"brand":"Coolpad","nmcc":"404","uuid":"","ch":"20151117","act":"2","idfa":""}"""
    val parsed = SourceEventProcessor.preprocessAct(clickDate, collection.mutable.Map[String, String](constant_event_pid -> "11", constant_event_lan_region -> "hi_,hi_in", constant_wnd -> "600"), "2")
    println(parsed)
    //assert(parsed == ("906d025c672f3193\003YD202f98c2j_in\00342efb16b","906d025c672f3193\003hi_IN\003YD202f98c2j_in\0031470816047\0031470815400\00311\00335_Bhopal\00342efb16b\0031"))
  }

  test("parse  click data 1") {
    val clickDate = """{"servertime_sec":"1470816047","upack":{"exp":"insta_hindi_094","new_user":"0"},"model":"CP8676_I02","mcc":"404","declared_lan":"","aid":"906d025c672f3193","net":"wifi","nmnc":"78","ext":{"eventtime":"1470816037"},"city":"35_Bhopal","id":"","refer":{},"lan":"hi_IN","action":"","value":"","mnc":"78","apiv":"3","contentid":"YD202f98c2j_in","osv":"5.1","ctype":"1","pid":"11","display":"0x02","api_server_ip":"10.5.10.198","ip":"122.168.42.57","country":"IN","appv":"1.2.0","pf":"android","scenario":{"level2":"29","level1":"1","level1_type":"1","source":"","column":"","is_level1_native":"1"},"servertime":"2016-08-10 08:00:47","app_lan":"hi","cpack":{"des":"rid=42efb16b|src=8|ord=7|page_n=0|hit_sc=0","md5":"YD202f98c2j_in","ext":{"big_img":0},"ishot":1},"brand":"Coolpad","nmcc":"404","uuid":"","ch":"20151117","act":"2","idfa":""}"""
    val parsed = SourceEventProcessor.preprocessAct(clickDate, collection.mutable.Map[String, String](constant_event_pid -> "11", constant_event_lan_region -> "hi_,hi_in", constant_wnd -> "600"), "1")
    println(parsed)
    assert(parsed == ("",""))
  }

  test("parse readtime data") {
    val readtime = """{"servertime_sec":"1470816010","upack":{"exp":"insta_hindi_093","new_user":"0"},"model":"Lenovo K50a40","mcc":"404","declared_lan":"","aid":"fdd546c93b06b04a","net":"wifi","nmnc":"11","ext":{"dwelltime":"148"},"city":"GA_Atlanta","id":"","refer":{},"lan":"hi_IN","action":"","value":"","mnc":"11","apiv":"3","contentid":"eQ1fc0b10Mb_in","osv":"5.0","ctype":"1","pid":"11","display":"0x02","api_server_ip":"10.2.8.104","ip":"172.98.85.237","country":"IN","appv":"1.1.7","pf":"android","scenario":{"level2":"29","level1":"1","level1_type":"1","source":"","column":"","is_level1_native":"1"},"servertime":"2016-08-10 08:00:10","app_lan":"hi","cpack":{"des":"rid=42a15f43|src=8|ord=5|page_n=0|hit_sc=0","md5":"eQ1fc0b10Mb_in","ext":{"big_img":0},"ishot":1},"brand":"Lenovo","nmcc":"404","uuid":"","ch":"200001","act":"4","idfa":""}"""
    val parsed = SourceEventProcessor.preprocessAct(readtime, collection.mutable.Map[String, String](constant_event_pid -> "11", constant_event_lan_region -> "hi_,hi_in", constant_wnd -> "600"), "4")
    println(parsed)
    assert(parsed == ("fdd546c93b06b04a\003eQ1fc0b10Mb_in\00342a15f43\00311","148"))
  }

  test("parse cp") {
    val cp = """{"content_length": 92, "image_md5": "59922872", "videos": [{"title": "How influential will Bernie Sanders be moving forward?", "url": "https://www.youtube.com/watch?v=7g5PMDto7e8", "fun_level": "3", "fun_level_explain": "\u4e00\u822c\u641e\u7b11", "youtube_id": "7g5PMDto7e8", "duration": 240, "desc": "Democratic strategist provides insight about how the candidate might shape the party", "screen_url": "http://img1.store.ksmobile.net/cminstanews/20160611/20/22098_982bff33_146567823596_696_388.jpg", "view": 0}], "is_include_video": false, "word_count": 84, "thirdads": "0", "is_servable": true, "has_copyright": 0, "images": ["http://img1.store.ksmobile.net/cminstanews/20160611/20/4925_59922872_146567823882_200_140.jpg"], "head_image": "http://img1.store.ksmobile.net/cminstanews/20160611/20/16191_65108c50_146567823818_480_320.jpg", "editorial_item_level": 0, "cp_id": "", "l2_categories": [{"name": "1000661", "weight": 0.5771895840353047}], "fetch_time": 1465678238, "copyright": "", "author": "Fox News", "entities": [{"L1_weight": 0.534275714679507, "name": "bernie_sanders", "weight": 0.8316340801519122}, {"L1_weight": 0.353862255068235, "name": "party", "weight": 0.5508090727475483}, {"L1_weight": 0.027220173466787304, "name": "shape", "weight": 0.04236992867288833}, {"L1_weight": 0.020815426768719704, "name": "insight", "weight": 0.032400533691032254}, {"L1_weight": 0.020014833431461255, "name": "democratic", "weight": 0.031154359318300247}, {"L1_weight": 0.013049837858551178, "name": "moving_forward", "weight": 0.02031290138302085}, {"L1_weight": 0.01074692529527744, "name": "influential", "weight": 0.016728271727193638}, {"L1_weight": 0.010407713384359852, "name": "the_candidate", "weight": 0.016200266845516127}, {"L1_weight": 0.009607120047101401, "name": "strategist", "weight": 0.014954092472784118}], "adult_score": 0.0, "title_md5": "ac947f03", "channel_names": ["video"], "content_md5": "b0dbfd04", "opencms_id": "0", "type": "video", "title_entities": [{"L1_weight": 0.9573590096286106, "name": "bernie_sanders", "weight": 0.999499772803683}, {"L1_weight": 0.023383768913342505, "name": "moving_forward", "weight": 0.024413069163308352}, {"L1_weight": 0.01925722145804677, "name": "influential", "weight": 0.020104880487430407}], "location": [], "update_time": 1465731427, "publish_time": 1465672481, "large_image": "http://img1.store.ksmobile.net/cminstanews/20160611/20/12563_a6c63afe_146567823810_480_266.jpg", "body_images": [], "channel_ids": [28], "cp_version": 13, "discovery_time": 1465678237, "source_type": 9, "link": "https://www.youtube.com/watch?v=7g5PMDto7e8", "display_type": 0, "item_id": "16333987", "source_feed": ["https://www.youtube.com/"], "index_flag": 2048, "categories": [{"name": "1000661", "weight": 0.5771895840353047}], "publisher": "Fox News", "feature_scores": {"newsy_score": 0.0, "adult_score": 0.0}, "language": "en", "photos": [], "editorial_importance_level": 0, "region": "in", "title": "How influential will Bernie Sanders be moving forward?", "last_modified_time": 1465678238, "summary": "", "cp_disable": false, "newsy_score": 0, "group_id": "102234", "image_count": 1}"""
    val parsed = CPProcessor.preprocess(cp,collection.mutable.Map[String,String](constant_d_rel_ncat->"10",constant_d_rel_nkey->"10",constant_cat_version->"oversea",constant_kw_version->"oversea"))
    println(parsed)
    val res = new DateTime("2016-06-18T00:00Z")
    val res2 = res.getMillis/1000
    println(res)
  }
}
