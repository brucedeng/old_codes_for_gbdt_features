#!/bin/bash

set -x
#export AWS_CONFIG_FILE=/home/news_model_online/.aws/config

export AWS_CONFIG_FILE=/home/news_model/.aws/config
SmartModel=${1}
AidModel=${2}
Model_dir=${3}

ModelBak_s3dir=s3://com.cmcm.instanews.usw2.prod/user_profile/model/backup_models/nr_us_add_topic_dulimei/
s3path=s3://com.cmcm.instanews.usw2.prod/model/lr_en_us_topic_dulimei/

online_model_file=ctr_lr_nr_us_add_topic_test1.model
online_model_done_file=${online_model_file}.done

local_ip=`hostname`
#AidFeature_s3dir=s3://com.cmcm.instanews.usw2.prod/model/online_model/online_lr_aid_features/
#s3path=s3://com.cmcm.instanews.usw2.prod/model/online_lr_model/

DownloadModelTime=5
minModelSize=209715200
#minModelSize=314572800 #200M
subject=LR_ModelUpload_Fail
toemail=dulimei@cmcm.com
MaxUploadWaitTime=10

UploadWaitTime=0
while [ ${UploadWaitTime} -lt ${MaxUploadWaitTime} ]
do
    test -e _UploadModel_ing
    IsUpload=`echo $?`

    if [ ${IsUpload} -eq 0 ]; then
        UploadWaitTime=$((${UploadWaitTime}+1))
        sleep 5s
    else
        touch _UploadModel_ing
        tmpDateStr=`echo ${SmartModel} | awk -F "${Model_dir}" '{print $2}'`
        CurrentDay=`echo ${tmpDateStr} | awk -F "/" '{print $1}'`
        CurrentHour=`echo ${tmpDateStr} | awk -F "/" '{print $2}'`
        CurrentMin=`echo ${tmpDateStr} | awk -F "/" '{print $3}'`
        CurrentDate=${CurrentDay}${CurrentHour}${CurrentMin}·

        TryCount=0
        model_res=-1
        while [ ${TryCount} -lt ${DownloadModelTime} ]
        do
            hadoop fs -getmerge ${SmartModel} ./RawModel/RawModel${CurrentDate}
            model_res=`echo $?`
            if [ ${model_res} -eq 0 ] ; then
                echo "succeed in download raw model.....${SmartModel}."
                break
            fi
            TryCount=$((${TryCount}+1))
        done
        if [ ${model_res} -ne 0 ] ; then
            echo "failed to download raw model......${SmartModel}!!!!!!!"
            curl -XPOST "http://10.2.2.152:8000/mail" -d "subject=${subject}$(date)&content=RawModelDownloadFailDate${SmartModel}&tos=${toemail}&content_type=html"
        else
            rm ${online_model_file}
            ./lr_model_serializer ./RawModel/RawModel${CurrentDate} ${online_model_file}
            if [ $? -ne 0 ]; then
              echo "fail to serialize RawModel......${SmartModel}!!!!!!"
              curl -XPOST "http://10.2.2.152:8000/mail" -d "subject=${subject}$(date)&content=RawModelSerializeFailDate${CurrentDate}&tos=${toemail}&content_type=html"
            else
              size_info=`wc -c ${online_model_file} | awk '{print $1}'`
              if [ ${size_info} -gt ${minModelSize} ]; then
                # export AWS_CONFIG_FILE=/home/news_model_online/.aws/config
                 export AWS_CONFIG_FILE=/home/news_model/.aws/config
                 aws s3 cp ${online_model_file} ${s3path} --acl bucket-owner-full-control 
                 if [ $? -ne 0 ]; then
                   echo "fail to upload RawModel......${SmartModel}!!!!!!"
                   curl -XPOST "http://10.2.2.152:8000/mail" -d "subject=${subject}$(date)&content=RawModelUploadFailDate${CurrentDate}&tos=${toemail}&content_type=html"
                 else
                   md5new=`md5sum ${online_model_file} | awk '{print $1}'`
                   time_stamp=`date +%s`
                   echo "$size_info	$time_stamp	$md5new" > ${online_model_done_file}
                   aws s3 cp ${online_model_done_file} ${s3path} --acl bucket-owner-full-control
                   if [ $? -ne 0 ]; then
                     echo "fail to upload Model.done......${SmartModel}!!!!!!"
                     curl -XPOST "http://10.2.2.152:8000/mail" -d "subject=${subject}$(date)&content=ModelDoneUploadFailDate${CurrentDate}&tos=${toemail}&content_type=html"
                   else
                     current_date_stamp=`date --date="${CurrentDay} ${CurrentHour}${CurrentMin}" +%s`
                     delay=`date +%s`
                     ((delay-=current_date_stamp))
                     curl -XPOST '127.0.0.1:1988/v1/push' -d '[{"endpoint": "'$local_ip'","metric":"us.lr.add_topic.upload_delay","timestamp":'$time_stamp',"step":600,"value":'${delay}',"counterType": "GAUGE","tags": ""}]'
                   fi
                   rm ${online_model_done_file}
                 fi
              else
                 echo "model size less than ${minModelSize}!!!!!!${SmartModel}"
                 curl -XPOST "http://10.2.2.152:8000/mail" -d "subject=${subject}$(date)&content=ModelSizeSmallDate${SmartModel}&tos=${toemail}&content_type=html"
              fi
            fi

            aws s3 cp ./RawModel/RawModel${CurrentDate} ${ModelBak_s3dir} --acl bucket-owner-full-control
            cp ${online_model_file} ./serializerModel/${online_model_file}${CurrentDate}
            aws s3 cp ./serializerModel/${online_model_file}${CurrentDate} ${ModelBak_s3dir} --acl bucket-owner-full-control

            rm ./RawModel/RawModel${CurrentDate}
            rm ./serializerModel/${online_model_file}${CurrentDate}
            rm -rf ./RawModel/.RawModel${CurrentDate}*
            rm -rf ./serializerModel/.${online_model_file}${CurrentDate}*

            CurrentDatePre=`date --date="${CurrentDay} ${CurrentHour}${CurrentMin} -4320 min" +%Y%m%d%H%M`
            aws s3 rm ${ModelBak_s3dir}/${online_model_file}${CurrentDatePre}
            aws s3 rm ${ModelBak_s3dir}/RawModel${CurrentDatePre}
        fi
        rm _UploadModel_ing
        break
    fi
done

if [ ${UploadWaitTime} -ge ${MaxUploadWaitTime} ]; then
    echo "Model upload Overtime......${SmartModel}"
    curl -XPOST "http://10.2.2.152:8000/mail" -d "subject=${subject}$(date)&content=ModelUploadWaitOvertime${SmartModel}&tos=${toemail}&content_type=html"
fi
