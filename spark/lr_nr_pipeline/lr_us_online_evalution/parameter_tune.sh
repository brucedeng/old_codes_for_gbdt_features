#!/bin/bash
set -x

StartTimeStr=20161010/13/20/
EndTime=201610191850
frequency=1 #min
MaxCheckTimeForWarn=120 #10min
MinThresholdSize=10485760 # 10M
Email=dulimei@cmcm.com

# =================================train parameter (one min)=======================================
jarname=./spark_logistic_regression_ftrl-assembly-1.0.jar
retiredays=14
minpv=0
geoProb=0.9
iter_num=2
lambda1=0.1
lambda2=100
alpha=0.0029
SetFilter=./setFilter.txt
AidFilter=./aidFeatures.txt

# =================================train parameter (one min)=======================================

# =================================train input/output data (one min)=======================================

HadoopFolder_dir=hdfs:/projects/news/model/online/dulimei/one_min_logistic_regression_ftrl/model_exp/
TrainingData_dir=hdfs:/projects/news/model/online/en_us_lr_pid14_add_topic/gen_training_data_streaming/training_data/

# =================================train input/output data (one min)=======================================


CurrentTimeStr=${StartTimeStr}
CurrentDay=`echo ${CurrentTimeStr} | awk -F "/" '{print $1}'`
CurrentHour=`echo ${CurrentTimeStr} | awk -F "/" '{print $2}'`
CurrentMin=`echo ${CurrentTimeStr} | awk -F "/" '{print $3}'`
CurrentTime=${CurrentDay}${CurrentHour}${CurrentMin}

TrainingDataExistCheckTime=0
AbDelayWarn=0
LastWarnTime=0


while [ ${CurrentTime} -le ${EndTime} ]
do
    #=========================generate TraingData====================================
    preCurrentTimeStr=`date --date="${CurrentDay} ${CurrentHour}${CurrentMin}" +%Y%m%d/%H/%M/`
    MaxSuppleNum=10
    tmp_Current_TimeStr=`date --date="${CurrentDay} ${CurrentHour}${CurrentMin} +${frequency} min" +%Y%m%d/%H/%M/`
    tmp_Current_Day=`echo ${tmp_Current_TimeStr} | awk -F "/" '{print $1}'`
    tmp_Current_Hour=`echo ${tmp_Current_TimeStr} | awk -F "/" '{print $2}'`
    tmp_Current_Min=`echo ${tmp_Current_TimeStr} | awk -F "/" '{print $3}'`
    tmp_Current_Time=${tmp_Current_Day}${tmp_Current_Hour}${tmp_Current_Min}
    
    FinalDate=${tmp_Current_Time}
    FinalDateStr=${tmp_Current_TimeStr}
    Count=0
    while [ ${Count} -lt ${MaxSuppleNum} ]
    do
        hdfs dfs -test -e ${TrainingData_dir}${tmp_Current_TimeStr}_SUCCESS
        if [ $? -eq 0 ]; then
            if [ ${Count} -eq 0 ]; then
                TrainingData="${TrainingData_dir}${tmp_Current_TimeStr}"
                Count=$((${Count}+1))
            else
                TrainingData="${TrainingData},${TrainingData_dir}${tmp_Current_TimeStr}"
                Count=$((${Count}+1))
            fi
            FinalDate=${tmp_Current_Time}
            FinalDateStr=${tmp_Current_TimeStr}
	    CurrentTime=${tmp_Current_Time}
	    CurrentDay=${tmp_Current_Day}
	    CurrentHour=${tmp_Current_Hour}
	    CurrentMin=${tmp_Current_Min}
        fi
        tmp_Current_TimeStr=`date --date="${tmp_Current_Day} ${tmp_Current_Hour}${tmp_Current_Min} +${frequency} min" +%Y%m%d/%H/%M/`
        tmp_Current_Day=`echo ${tmp_Current_TimeStr} | awk -F "/" '{print $1}'`
        tmp_Current_Hour=`echo ${tmp_Current_TimeStr} | awk -F "/" '{print $2}'`
        tmp_Current_Min=`echo ${tmp_Current_TimeStr} | awk -F "/" '{print $3}'`
        tmp_Current_Time=${tmp_Current_Day}${tmp_Current_Hour}${tmp_Current_Min}
    done
    #=========================generate TraingData====================================
        
    InitModel=${HadoopFolder_dir}${preCurrentTimeStr}ftrlOut/model/

        #=========================ftrl Traing====================================

        testData=${TrainingData}

        HadoopFolder=${HadoopFolder_dir}${FinalDateStr}
        OutputFolder=${HadoopFolder}ftrlOut
        OutputCleanModel=${OutputFolder}/smart/
        OutputModel=${OutputFolder}/model/
        OutputOtherModel=${OutputFolder}/other/
        OutputDeltaAidModel=${OutputFolder}/delta_aid/
        tmpPath=${HadoopFolder}tmp

        TrainingDate=${CurrentDay}


        hdfs dfs -rm -r -skipTrash ${HadoopFolder}
        hdfs dfs -mkdir -p ${HadoopFolder}

        MASTER=yarn-cluster /usr/bin/spark-submit \
        --class com.ijinshan.mllib.ftrl.FtrlTrain \
        --master yarn-cluster \
        --driver-memory 4g \
        --executor-memory 5g \
        --executor-cores 4 \
        --queue experiment \
        --num-executors 10 \
        --verbose \
        --conf spark.akka.timeout=300 \
        --conf spark.network.timeout=360 \
        --conf spark.akka.frameSize=300 \
        --conf spark.driver.maxResultSize=4g \
        --conf spark.storage.memoryFraction=0.7 \
        --conf spark.shuffle.memoryFraction=0.3 \
        --conf spark.broadcast.blockSize=4096 \
        --conf spark.broadcast.factory=org.apache.spark.broadcast.TorrentBroadcastFactory \
        --conf spark.broadcast.compress=true \
        --conf spark.serializer=org.apache.spark.serializer.KryoSerializer \
        --conf spark.akka.threads=4 \
        --conf spark.default.parallelism=80 \
        --conf spark.app.name=ftrl_training_tongming_1_min${FinalDate} \
        --files ${SetFilter},${AidFilter} \
        ${jarname} \
        trainingdate=${TrainingDate} \
        retiredays=${retiredays} \
        filtersetfile=${SetFilter} \
        aidfeatureprefixfile=${AidFilter} \
        enabletest=false \
        trainingdata=${TrainingData} \
        tmpfolder=${tmpPath} \
        oldmodel=${InitModel} \
        colinfo=2 \
        testcolinfo=3 \
        samplemulti=4 \
        minpv=${minpv} \
        geoProb=${geoProb} \
        testdata=${testData} \
        smalldata=true \
        deltaoutputmodel=false \
        smartoutputmodel=true \
        mappingsizelimit=4G \
        outcleanmodel=${OutputCleanModel} \
        outfullmodel=${OutputModel} \
        outdeltaaidmodel=${OutputDeltaAidModel} \
        outdeltaothermodel=${OutputOtherModel} \
        iterations=${iter_num} \
        lambda1=${lambda1} \
        lambda2=${lambda2} \
        alpha=${alpha} > ./log/runSparkFTRLTrainer_correct${FinalDate} 2> ./log/runSparkFTRLTrainer_error_test1${FinalDate}


        nohup bash lrModel_test.sh ${OutputCleanModel} ${TrainingData_dir} ${HadoopFolder_dir} ${frequency} > ./log/aucCal_test1.log.${FinalDate} 2>&1 &


done

