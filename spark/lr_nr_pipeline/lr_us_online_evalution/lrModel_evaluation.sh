#!/bin/bash
set -x


SmartModel=${1}
TestData_dir=${2}
Model_dir=${3}
frequency=${4}

DownloadModelTime=5
MaxEvaluateWaitTime=10
MaxTestDataPiece=10

EvaluateWaitTime=0
local_ip=`hostname`

while [ ${EvaluateWaitTime} -lt ${MaxEvaluateWaitTime} ]
do
    test -e _EvaluateModel_ing
    IsEvaluation=`echo $?`
    if [ ${IsEvaluation} -eq 0 ]; then
        EvaluateWaitTime=$((${EvaluateWaitTime}+1))
        sleep 5s
    else
        touch _EvaluateModel_ing
        tmpDateStr=`echo ${SmartModel} | awk -F "${Model_dir}" '{print $2}'`
        CurrentDay=`echo ${tmpDateStr} | awk -F "/" '{print $1}'`
        CurrentHour=`echo ${tmpDateStr} | awk -F "/" '{print $2}'`
        CurrentMin=`echo ${tmpDateStr} | awk -F "/" '{print $3}'`
        ModelCurrentDate=${CurrentDay}${CurrentHour}${CurrentMin}

        TryCount=0
        model_res=-1
        while [ ${TryCount} -lt ${DownloadModelTime} ]
        do
            hadoop fs -getmerge ${SmartModel} ./EvaluateModel/Model${ModelCurrentDate}
            model_res=`echo $?`
            if [ ${model_res} -eq 0 ] ; then
                echo "succeed in download raw model.....${SmartModel}."
                break
            fi
            TryCount=$((${TryCount}+1))
        done
        if [ ${model_res} -ne 0 ] ; then
            echo "failed to download raw model......${SmartModel}!!!!!!!"
            rm _EvaluateModel_ing
            exit -1
        else
            TempCurrentDay=${CurrentDay}
            TempCurrentHour=${CurrentHour}
            TempCurrentMin=${CurrentMin}
            TestDataEndDate=`date --date="${CurrentDay} ${CurrentHour}${CurrentMin} +${MaxTestDataPiece} min" +%Y%m%d%H%M`
            CurrentDateStr=`date --date="${CurrentDay} ${CurrentHour}${CurrentMin} +${frequency} min" +%Y%m%d/%H/%M/`
            CurrentDay=`echo ${CurrentDateStr} | awk -F "/" '{print $1}'`
            CurrentHour=`echo ${CurrentDateStr} | awk -F "/" '{print $2}'`
            CurrentMin=`echo ${CurrentDateStr} | awk -F "/" '{print $3}'`
            CurrentDate=${CurrentDay}${CurrentHour}${CurrentMin}
            Count=0
            while [ ${CurrentDate} -le ${TestDataEndDate} ]
            do
                hdfs dfs -test -e ${TestData_dir}${CurrentDateStr}_SUCCESS
                if [ $? -eq 0 ]; then
                    if [ ${Count} -eq 0 ]; then
                        TestData="${TestData_dir}${CurrentDateStr}*"
                        Count=$((${Count}+1))
                    else
                        TestData="${TestData} ${TestData_dir}${CurrentDateStr}*"
                        Count=$((${Count}+1))
                    fi
                fi
                CurrentDateStr=`date --date="${CurrentDay} ${CurrentHour}${CurrentMin} +${frequency} min" +%Y%m%d/%H/%M/`
                CurrentDay=`echo ${CurrentDateStr} | awk -F "/" '{print $1}'`
                CurrentHour=`echo ${CurrentDateStr} | awk -F "/" '{print $2}'`
                CurrentMin=`echo ${CurrentDateStr} | awk -F "/" '{print $3}'`
                CurrentDate=${CurrentDay}${CurrentHour}${CurrentMin}
            done
            if [ ${Count} -eq 0 ]; then
                echo "testData for Model ${SmartModel} do not exist!!!!"
                rm _EvaluateModel_ing
                exit -1
            fi
            TrainDataEndDate=`date --date="${TempCurrentDay} ${TempCurrentHour}${TempCurrentMin} -${Count} min" +%Y%m%d%H%M`
            CurrentTrainDateStr=`date --date="${TempCurrentDay} ${TempCurrentHour}${TempCurrentMin}" +%Y%m%d/%H/%M/`
            CurrentDay=`echo ${CurrentTrainDateStr} | awk -F "/" '{print $1}'`
            CurrentHour=`echo ${CurrentTrainDateStr} | awk -F "/" '{print $2}'`
            CurrentMin=`echo ${CurrentTrainDateStr} | awk -F "/" '{print $3}'`
            CurrentDate=${CurrentDay}${CurrentHour}${CurrentMin}
            Count=0
            while [ ${CurrentDate} -gt ${TrainDataEndDate} ]
            do
                hdfs dfs -test -e ${TestData_dir}${CurrentTrainDateStr}_SUCCESS
                if [ $? -eq 0 ]; then
                    if [ ${Count} -eq 0 ]; then
                        TrainData="${TestData_dir}${CurrentTrainDateStr}*"
                        Count=$((${Count}+1))
                    else
                        TrainData="${TrainData} ${TestData_dir}${CurrentTrainDateStr}*"
                        Count=$((${Count}+1))
                    fi
                fi
                CurrentTrainDateStr=`date --date="${CurrentDay} ${CurrentHour}${CurrentMin} -${frequency} min" +%Y%m%d/%H/%M/`
                CurrentDay=`echo ${CurrentTrainDateStr} | awk -F "/" '{print $1}'`
                CurrentHour=`echo ${CurrentTrainDateStr} | awk -F "/" '{print $2}'`
                CurrentMin=`echo ${CurrentTrainDateStr} | awk -F "/" '{print $3}'`
                CurrentDate=${CurrentDay}${CurrentHour}${CurrentMin}
            done
            hadoop fs -cat ${TestData} ${TrainData} | ./predictor ./EvaluateModel/Model${ModelCurrentDate} >score.result
            if [ $? -ne 0 ] ; then
                echo "failed to predict model ${SmartModel}!!!!!!!"
                rm _EvaluateModel_ing
                exit -1
            fi
            echo "calculate AUC ......"
            testDataNum=`hadoop fs -cat ${TestData} | wc -l`
            trainDataNum=`hadoop fs -cat ${TrainData} | wc -l`
            head -n${testDataNum} score.result > score.result.test
            tail -n${trainDataNum} score.result > score.result.train
            ./peruser_auc -f score.result.test -m 1 > auc.result.test
            if [ $? -ne 0 ] ; then
                echo "failed to calculate AUC model ${SmartModel}!!!!!!!"
                rm _EvaluateModel_ing
                exit -1
            fi
            AUCRES=`cat auc.result.test | grep "std auc" | awk -F"std auc:" '{print $2}'`
            PERUAUC=`cat auc.result.test | grep "user auc" | awk -F"user auc:" '{print $2}'`
            ModelSize=`wc -l ./EvaluateModel/Model${ModelCurrentDate} | awk '{print $1}'`
            echo aucres:${AUCRES} peruauc:${PERUAUC} DateTime:${ModelCurrentDate} ModelSize:${ModelSize} testDataNum:${testDataNum} >> auc.list

            ts=`date -d"${CurrentDay} ${CurrentHour}${CurrentMin}" +%s`

            curl -XPOST '127.0.0.1:1988/v1/push' -d '[{"endpoint": "'$local_ip'","metric":"us.lr.add_topic_pid14_online.test_auc","timestamp":'$ts',"step":600,"value":'${AUCRES}',"counterType": "GAUGE","tags": ""}]'
            curl -XPOST '127.0.0.1:1988/v1/push' -d '[{"endpoint": "'$local_ip'","metric":"us.lr.add_topic_pid14_online.test_count","timestamp":'$ts',"step":600,"value":'${testDataNum}',"counterType": "GAUGE","tags": ""}]'
            ./peruser_auc -f score.result.train -m 1 > auc.result.train
            if [ $? -ne 0 ] ; then
                echo "failed to calculate train data AUC model ${SmartModel}!!!!!!!"
                rm _EvaluateModel_ing
                exit -1
            fi
            AUCRES=`cat auc.result.train | grep "std auc" | awk -F"std auc:" '{print $2}'`
            PERUAUC=`cat auc.result.train | grep "user auc" | awk -F"user auc:" '{print $2}'`
            echo aucres:${AUCRES} peruauc:${PERUAUC} DateTime:${ModelCurrentDate} ModelSize:${ModelSize} trainDataNum:${trainDataNum} >> auc.list
            curl -XPOST '127.0.0.1:1988/v1/push' -d '[{"endpoint": "'$local_ip'","metric":"us.lr.add_topic_pid14_online.train_auc","timestamp":'$ts',"step":600,"value":'${AUCRES}',"counterType": "GAUGE","tags": ""}]'
            curl -XPOST '127.0.0.1:1988/v1/push' -d '[{"endpoint": "'$local_ip'","metric":"us.lr.add_topic_pid14_online.train_count","timestamp":'$ts',"step":600,"value":'${trainDataNum}',"counterType": "GAUGE","tags": ""}]'

            rm ./EvaluateModel/Model${ModelCurrentDate}
            rm -rf ./EvaluateModel/.Model${ModelCurrentDate}*
        fi
        rm _EvaluateModel_ing
        break
    fi
done
