#!/bin/bash
set -x

StartTimeStr=20161013/09/25/
EndTime=201809070700
frequency=1 #min
MaxCheckTimeForWarn=60 #10min
MinThresholdSize=10485760 # 10M
MaxSuppleNum=20
Email=dulimei@cmcm.com

# =================================train parameter (one min)=======================================
jarname=./spark_logistic_regression_ftrl-assembly-1.0.jar
retiredays=14
minpv=0
geoProb=0.9
iter_num=5
lambda1=0.5
lambda2=0.1
alpha=0.0029
SetFilter=./setFilter.txt
AidFilter=./aidFeatures.txt

# =================================train parameter (one min)=======================================

# =================================train input/output data (one min)=======================================

HadoopFolder_dir=hdfs:/projects/news/model/online/en_us_lr_pid14_add_topic/one_min_logistic_regression_ftrl/
TrainingData_dir=hdfs:/projects/news/model/online/en_us_lr_pid14_add_topic/gen_training_data_streaming/training_data/

# =================================train input/output data (one min)=======================================


CurrentTimeStr=${StartTimeStr}
CurrentDay=`echo ${CurrentTimeStr} | awk -F "/" '{print $1}'`
CurrentHour=`echo ${CurrentTimeStr} | awk -F "/" '{print $2}'`
CurrentMin=`echo ${CurrentTimeStr} | awk -F "/" '{print $3}'`
CurrentTime=${CurrentDay}${CurrentHour}${CurrentMin}

TrainingDataExistCheckTime=0
AbDelayWarn=0
LastWarnTime=0


while [ ${CurrentTime} -le ${EndTime} ]
do
    #=========================generate TraingData====================================
    tmp_Current_TimeStr=`date +%Y%m%d/%H/%M/`
    tmp_Current_Day=`echo ${tmp_Current_TimeStr} | awk -F "/" '{print $1}'`
    tmp_Current_Hour=`echo ${tmp_Current_TimeStr} | awk -F "/" '{print $2}'`
    tmp_Current_Min=`echo ${tmp_Current_TimeStr} | awk -F "/" '{print $3}'`
    tmp_Current_Time=${tmp_Current_Day}${tmp_Current_Hour}${tmp_Current_Min}
    
    FinalDate=${tmp_Current_Time}
    FinalDateStr=${tmp_Current_TimeStr}
    Count=0
    while [ ${tmp_Current_Time} -ge ${CurrentTime} ]
    do
        hdfs dfs -test -e ${TrainingData_dir}${tmp_Current_TimeStr}_SUCCESS
        if [ $? -eq 0 ]; then
            if [ ${Count} -eq 0 ]; then
                TrainingData="${TrainingData_dir}${tmp_Current_TimeStr}"
                Count=$((${Count}+1))
                FinalDate=${tmp_Current_Time}
                FinalDateStr=${tmp_Current_TimeStr}
            else
                TrainingData="${TrainingData},${TrainingData_dir}${tmp_Current_TimeStr}"
                Count=$((${Count}+1))
            fi
            if [ ${Count} -ge ${MaxSuppleNum} ]; then
                echo "TrainingData have reached MaxSuppleNum ${MaxSuppleNum}, start training"
                break
            fi
        fi
        tmp_Current_TimeStr=`date --date="${tmp_Current_Day} ${tmp_Current_Hour}${tmp_Current_Min} -${frequency} min" +%Y%m%d/%H/%M/`
        tmp_Current_Day=`echo ${tmp_Current_TimeStr} | awk -F "/" '{print $1}'`
        tmp_Current_Hour=`echo ${tmp_Current_TimeStr} | awk -F "/" '{print $2}'`
        tmp_Current_Min=`echo ${tmp_Current_TimeStr} | awk -F "/" '{print $3}'`
        tmp_Current_Time=${tmp_Current_Day}${tmp_Current_Hour}${tmp_Current_Min}
    done
    #=========================generate TraingData====================================

    if [ ${Count} -gt 0 ]; then
        #=========================generate InitModel====================================
        tmp_Current_Day=${CurrentDay}
        tmp_Current_Hour=${CurrentHour}
        tmp_Current_Min=${CurrentMin}
        while true
        do
            tmp_Pre_DateStr=`date --date="${tmp_Current_Day} ${tmp_Current_Hour}${tmp_Current_Min} -${frequency} min" +%Y%m%d/%H/%M/`
            Pre_Model=${HadoopFolder_dir}${tmp_Pre_DateStr}
            hdfs dfs -test -e ${Pre_Model}ftrlOut/model/_SUCCESS
            if [ $? -eq 0 ]; then
                ModelSize=`hdfs dfs -du -s ${Pre_Model}ftrlOut/model/ | awk '{print $1}'`
                if [ ${ModelSize} -gt ${MinThresholdSize} ]; then
                    break
                fi
            fi
            tmp_Current_Day=`echo ${tmp_Pre_DateStr} | awk -F "/" '{print $1}'`
            tmp_Current_Hour=`echo ${tmp_Pre_DateStr} | awk -F "/" '{print $2}'`
            tmp_Current_Min=`echo ${tmp_Pre_DateStr} | awk -F "/" '{print $3}'`
        done
        #=========================generate InitModel====================================
        preCurrentTimeStr=${tmp_Pre_DateStr}
        InitModel=${HadoopFolder_dir}${preCurrentTimeStr}ftrlOut/model/

        #=========================ftrl Traing====================================

        testData=${TrainingData}

        HadoopFolder=${HadoopFolder_dir}${FinalDateStr}
        OutputFolder=${HadoopFolder}ftrlOut
        OutputCleanModel=${OutputFolder}/smart/
        OutputModel=${OutputFolder}/model/
        OutputOtherModel=${OutputFolder}/other/
        OutputDeltaAidModel=${OutputFolder}/delta_aid/
        tmpPath=${HadoopFolder}tmp

        TrainingDate=${CurrentDay}


        hdfs dfs -rm -r -skipTrash ${HadoopFolder}
        hdfs dfs -mkdir -p ${HadoopFolder}

        MASTER=yarn-client /usr/bin/spark-submit \
        --class com.ijinshan.mllib.ftrl.FtrlTrain \
        --master yarn-cluster \
        --driver-memory 4g \
        --executor-memory 5g \
        --executor-cores 4 \
        --queue fasttrack \
        --num-executors 10 \
        --verbose \
        --conf spark.akka.timeout=300 \
        --conf spark.network.timeout=360 \
        --conf spark.akka.frameSize=300 \
        --conf spark.driver.maxResultSize=4g \
        --conf spark.storage.memoryFraction=0.7 \
        --conf spark.shuffle.memoryFraction=0.3 \
        --conf spark.broadcast.blockSize=4096 \
        --conf spark.broadcast.factory=org.apache.spark.broadcast.TorrentBroadcastFactory \
        --conf spark.broadcast.compress=true \
        --conf spark.serializer=org.apache.spark.serializer.KryoSerializer \
        --conf spark.akka.threads=4 \
        --conf spark.default.parallelism=80 \
        --conf spark.app.name=ftrl_training_dulimei_add_topic_test2_1_min${FinalDate} \
        --files ${SetFilter},${AidFilter} \
        ${jarname} \
        trainingdate=${TrainingDate} \
        retiredays=${retiredays} \
        filtersetfile=${SetFilter} \
        aidfeatureprefixfile=${AidFilter} \
        enabletest=false \
        trainingdata=${TrainingData} \
        tmpfolder=${tmpPath} \
        oldmodel=${InitModel} \
        colinfo=2 \
        testcolinfo=3 \
        samplemulti=4 \
        minpv=${minpv} \
        geoProb=${geoProb} \
        testdata=${testData} \
        smalldata=true \
        deltaoutputmodel=true \
        smartoutputmodel=true \
        mappingsizelimit=4G \
        outcleanmodel=${OutputCleanModel} \
        outfullmodel=${OutputModel} \
        outdeltaaidmodel=${OutputDeltaAidModel} \
        outdeltaothermodel=${OutputOtherModel} \
        iterations=${iter_num} \
        lambda1=${lambda1} \
        lambda2=${lambda2} \
        alpha=${alpha} > ./log/runSparkFTRLTrainer_correct${FinalDate} 2> ./log/runSparkFTRLTrainer_error${FinalDate}

        if [ $? -ne 0 ]; then
            echo "${CurrentTime} training fail!!!!!! TrainingData is ${TrainingData}"
            curl -XPOST "http://10.2.2.238:8000/mail" -d "subject=FtrlTrainingFail$(date)&content=${CurrentTime} training fail!!!!!!&tos=${Email}&content_type=html"
        else
            echo "${CurrentTime} training successfully!!!!!! TrainingData is ${TrainingData}"
            echo "${CurrentTime} uploading.....${OutputCleanModel}"
            #=========================upload model on background====================================

            nohup bash lrModel_upload.sh ${OutputOtherModel} ${OutputDeltaAidModel} ${HadoopFolder_dir} > ./log/upload${FinalDate}.log 2>&1 &

            #=========================upload model on background====================================

            #=========================evaluate model on background====================================

            nohup bash lrModel_evaluation.sh ${OutputCleanModel} ${TrainingData_dir} ${HadoopFolder_dir} ${frequency} > ./log/aucCal.log 2>&1 &

            #=========================evaluate model on background====================================

            CurrentTimeStr=${FinalDateStr}
            CurrentDay=`echo ${CurrentTimeStr} | awk -F "/" '{print $1}'`
            CurrentHour=`echo ${CurrentTimeStr} | awk -F "/" '{print $2}'`
            CurrentMin=`echo ${CurrentTimeStr} | awk -F "/" '{print $3}'`

            CurrentTimeStr=`date --date="${CurrentDay} ${CurrentHour}${CurrentMin} +${frequency} min" +%Y%m%d/%H/%M/`
            CurrentDay=`echo ${CurrentTimeStr} | awk -F "/" '{print $1}'`
            CurrentHour=`echo ${CurrentTimeStr} | awk -F "/" '{print $2}'`
            CurrentMin=`echo ${CurrentTimeStr} | awk -F "/" '{print $3}'`
            CurrentTime=${CurrentDay}${CurrentHour}${CurrentMin}

            hdfs dfs -rm -r -skipTrash ${tmpPath}

            TrainingDataExistCheckTime=0
            AbDelayWarn=0
        fi
        #=========================ftrl Traing====================================
    else
        echo echo "TrainingData for ${CurrentTime} do not exist!!!!!!  $(date)"
        TrainingDataExistCheckTime=$((${TrainingDataExistCheckTime}+1))
        if [ ${TrainingDataExistCheckTime} -gt ${MaxCheckTimeForWarn} ]; then
            echo "TrainingData delay!!!!!! ${TrainingData} $(date)"
            curl -XPOST "http://10.2.2.238:8000/mail" -d "subject=FtrlTrainingDataDelay$(date)&content=TrainingData for ${CurrentTime} delay!!!!!!  $(date)&tos=${Email}&content_type=html"
            TrainingDataExistCheckTime=0
        fi
        CurrentStamptime=`date --date="${CurrentDay} ${CurrentHour}${CurrentMin}" +%s`
        NowStamptime=`date +%s`
        DeltaStamptime=$((${NowStamptime}-${CurrentStamptime}))
        if [ ${DeltaStamptime} -gt 2700  ]; then
            echo "Training${CurrentTime} delay more than 45min absolutely!!!!!!"
            AbDelayWarn=$((${AbDelayWarn}+1))
            DeltaWarnTime=$((${NowStamptime}-${LastWarnTime}))
            if [ $((${AbDelayWarn} % 120)) -eq 1  ] && [ ${DeltaWarnTime} -gt 1800  ]; then
                curl -XPOST "http://10.2.2.238:8000/mail" -d "subject=FtrlTrainingDataDelay$(date)&content=Training${CurrentTime} delay more than 45min absolutely!!!!!!&tos=${Email}&content_type=html"
                LastWarnTime=`date +%s`
            fi
        fi
        sleep 5s
    fi
done

