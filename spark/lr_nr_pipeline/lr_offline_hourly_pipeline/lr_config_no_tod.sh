
APP_NAME="ftrl_hourly_ckh_no_tod"
log_dir="./${APP_NAME}_logs"
 
FTRL_CONF="--class com.ijinshan.mllib.ftrl.FtrlTrain \
 --master yarn-cluster \
 --driver-memory 4g \
 --executor-memory 5g \
 --executor-cores 4 \
 --queue experiment \
 --num-executors 20 \
 --verbose \
 --conf spark.akka.timeout=300 \
 --conf spark.network.timeout=360 \
 --conf spark.akka.frameSize=300 \
 --conf spark.driver.maxResultSize=4g \
 --conf spark.storage.memoryFraction=0.7 \
 --conf spark.shuffle.memoryFraction=0.3 \
 --conf spark.broadcast.blockSize=4096 \
 --conf spark.broadcast.factory=org.apache.spark.broadcast.TorrentBroadcastFactory \
 --conf spark.broadcast.compress=true \
 --conf spark.serializer=org.apache.spark.serializer.KryoSerializer \
 --conf spark.akka.threads=4 \
 --conf spark.default.parallelism=63"

FTRL_PARAM="iter_num=2 lambda1=0.1 lambda2=1.0 alpha=0.003 retiredays=14 enabletest=false colinfo=2 testcolinfo=3 samplemulti=4 minpv=0 geoProb=0.9 mappingsizelimit=4g smalldata=true deltaoutputmodel=false smartoutputmodel=true"

EVAL_CONF="--class com.cmcm.cmnews.model.app.FTRLPredictorApp \
--master yarn-cluster \
--driver-memory 2g \
--executor-memory 3g \
--executor-cores 4 \
--queue experiment \
--num-executors 25"

EVAL_PARAM="lr_evaluate true parallelism 100 lr_broadcast false" 

event_start=20161021
event_end=20161022
run_ftrl_train=true
run_eval=true

train_data_template="hdfs://mycluster/projects/news/model/chenkehan/ckh_gen_training_data_hour/en_us/training_data/%s"
output_template="hdfs:///projects/news/model/chenkehan/${APP_NAME}/%s"

SetFilter="setFilter_no_tod.txt"

