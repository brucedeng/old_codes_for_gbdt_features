
train_config_file=$1
source $train_config_file

test_var="${APP_NAME?} ${log_dir?}"

start_ts=`date -d "$event_start" +%s`
end_ts=`date -d "$event_end" +%s`
end_ts=$((end_ts+86400))

interval=3600

mkdir -p $log_dir
done_flag=`ls $log_dir/*.done | wc -l`
if [[ "$2" == "new" ]]; then
    rm -rf $log_dir/*.log
    dt=`date -d@$start_ts +%Y%m%d%H`
    echo "clear logs and start pipline from $dt..."
elif [[ $done_flag -gt 0 ]]; then
    start_ts=$((start_ts + done_flag*interval)) 
    dt=`date -d@$start_ts +%Y%m%d%H`
    echo "resume pipeline from $dt..."
else
    dt=`date -d@$start_ts +%Y%m%d%H`
    echo "start pipline from $dt..."
fi

#ftrl_jar=spark_lr-assembly-1.0.jar
ftrl_jar=spark_logistic_regression_ftrl-assembly-1.0.jar
eval_jar=content_xfb_train_data_pipeline-1.1-SNAPSHOT-jar-with-dependencies.jar

function wait_for_file {
    file_name=$1
    echo "waiting for file $file_name"
    while [[ ! -e $file_name ]]; do
        sleep 5
    done
}

function run_one_batch {
    ts=$1

    cur_run_log=`date -d@$ts +%Y%m%d_%H`
    if [[ "$run_ftrl_train" == "true" ]]; then
        # test if all needed variables are assigned.
        test_var="${train_data_template?} ${output_template?} ${FTRL_CONF?} ${FTRL_PARAM?} ${SetFilter?}"

        input_ts=`date -d@$ts +%Y%m%d/%H`
        input_ts_file=`date -d@$ts +%Y%m%d_%H`
        prev_ts=`date -d@$((ts-interval)) +%Y%m%d/%H`
        prev_ts_file=`date -d@$((ts-interval)) +%Y%m%d_%H`
        next_ts=`date -d@$((ts+interval)) +%Y%m%d/%H`

        trainingdata=`printf "$train_data_template" "$input_ts"`
        testdata=`printf "$train_data_template" "$next_ts"`
        tmpfolder=`printf "$output_template/tmp" "$input_ts"`
        oldmodel=`printf "$output_template/ftrlOut/model" "$prev_ts"`
        outcleanmodel=`printf "$output_template/ftrlOut/smart" "$input_ts"`
        outfullmodel=`printf "$output_template/ftrlOut/model" "$input_ts"`
        outdeltaaidmodel=`printf "$output_template/ftrlOut/delta_aid" "$input_ts"`
        outdeltaothermodel=`printf "$output_template/ftrlOut/other" "$input_ts"`
        
        if [[ $ts -eq $first_ts ]]; then
            hadoop fs -mkdir -p $oldmodel
            hadoop fs -touchz $oldmodel/_SUCCESS
        fi
        
        if [[ $ts -gt $first_ts ]]; then
            echo "waiting for file $log_dir/ftrl_train.$prev_ts.done"
            wait_for_file $log_dir/ftrl_train.$prev_ts_file.done
        fi

        output=`printf "$output_template" "$input_ts"`
        hadoop fs -rm -r $output || echo "$output does not exist"
        date_time=`date -d@$ts +%Y%m%d`

        cmd="spark-submit --conf spark.app.name=${APP_NAME}_ftrl_train_$cur_run_log ${FTRL_CONF} --files ${SetFilter} ${ftrl_jar} trainingdate=${date_time} trainingdata=${trainingdata} tmpfolder=${tmpfolder} oldmodel=${oldmodel} testdata=${testdata} outcleanmodel=${outcleanmodel} outfullmodel=${outfullmodel} outdeltaaidmodel=${outdeltaaidmodel} outdeltaothermodel=${outdeltaothermodel} filtersetfile=${SetFilter} ${FTRL_PARAM}"
        echo "$cmd"
        eval $cmd > $log_dir/$cur_run_log-ftrl_train.log 2>&1

        if [ $? != 0 ]; then echo "ERROR: Failed to run training!"; exit -1; fi
        touch $log_dir/ftrl_train.$input_ts_file.done
    fi

    if [[ "$run_eval" == "true" ]]; then
        # test if all needed variables are assigned.
        test_var="${train_data_template?} ${output_template?} ${EVAL_CONF?} ${EVAL_PARAM?}"

        input_ts=`date -d@$ts +%Y%m%d/%H`
        next_ts=`date -d@$((ts+interval)) +%Y%m%d/%H`
        traindata=`printf "$train_data_template" "$input_ts"`
        testdata=`printf "$train_data_template" "$next_ts"`
        outcleanmodel=`printf "$output_template/ftrlOut/smart" "$input_ts"`
        score_path=`printf "$output_template/ftrlOut/score" "$input_ts"`
        eval_path=`printf "$output_template/ftrlOut/eval" "$input_ts"`

        cmd_train="spark-submit --conf spark.app.name=${APP_NAME}_ftrl_evaltrain_$cur_run_log ${EVAL_CONF} ${eval_jar} lr_train_path ${traindata} lr_model_path ${outcleanmodel} lr_score_path ${score_path}/train lr_evaluate_path ${eval_path}/train"
        cmd_test="spark-submit --conf spark.app.name=${APP_NAME}_ftrl_evaltest_$cur_run_log ${EVAL_CONF} ${eval_jar} lr_train_path ${testdata} lr_model_path ${outcleanmodel} lr_score_path ${score_path}/test lr_evaluate_path ${eval_path}/test"

        echo "$cmd_train"
        eval $cmd_train > $log_dir/$cur_run_log-ftrl_eval.log 2>&1
        if [ $? != 0 ]; then echo "ERROR: Failed to run eval!"; exit -1; fi
        
        echo "$cmd_test"
        eval $cmd_test >> $log_dir/$cur_run_log-ftrl_eval.log 2>&1
        if [ $? != 0 ]; then echo "ERROR: Failed to run eval!"; exit -1; fi

    fi
}

first_ts=start_ts
if [[ "$run_ftrl_train" == true || "$run_eval" == true ]]; then
    for ((tmp_ts=first_ts; tmp_ts<end_ts; tmp_ts+=3600)); do

        command_log_ts=`date -d@$tmp_ts +%Y%m%d_%H` 
        echo "start running for $command_log_ts"
        run_one_batch $tmp_ts > $log_dir/batch_$command_log_ts.log 2>&1 
        sleep 10
    done
fi
