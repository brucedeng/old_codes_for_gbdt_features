
### Hourly LR pipeline
1. fix a few bugs in binary feature pipeline, remove pv-click dedup, use 2h up
2. auc calculator in spark
3. use shell instead of oozie for training

### Files
1. hourly binary feature spark: india_news_ranking/spark/lr_nr_pipeline/binary_train_data_pipeline
2. hourly oozie pipline: india_news_ranking/news_offline/gen_train_data/phase2_spark_dnn/gen_training_data_2hour_up
3. hourly train shell pipline: spark/lr_nr_pipeline/lr_offline_hourly_pipeline

### Train pipeline
1. run_train_hourly.sh [config_file] to start or resume failed pipeline (if log existed).
2. use "run_train_hourly.sh [config_file] new" to restart pipeline (will delete log)
3. lr_config_no_tod.sh is an example of config



