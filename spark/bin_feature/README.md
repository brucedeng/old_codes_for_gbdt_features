# This project calculate view, click and CTR by range of bins. 

## Start Program: run.sh

## Usage: 
### sbt package to compile jar.

### change config in run.sh

TRAIN_DATA_INPUT_PATH: Input training data(feature value must be origin).
RESULT_OUTPUT_PATH: Reduced bined feature value.
BIN_CONFIG: json config file for feature you want to bin.

### config.json

featureIDs: Single edge features you want to bin.
FeatureBinMethods: equal view statistics. Number represents how many bins.
Metric: output ctr, view and click

### sh run.sh


## Output: bin.txt

### range
Left open right close upper bound. For example 0 1 3 7 represents (-inf,0] (0,1] (1,3] (3,7] (7,inf)*[]: 
 Origin value will be mapping from index 0. For example 2, 1<=2<3 will be map to 3rd range, thus
 bined value is 3. -1.0E7 represents default null value or missing value.

### click
click in current range

### view
view in current range

### ctr
click/view in current range