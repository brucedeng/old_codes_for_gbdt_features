/**
  * Created by guozhenyuan on 16/11/09.
  */

import org.apache.commons.lang.SystemUtils
import org.apache.spark._
import org.apache.spark.rdd.RDD
import org.json4s.DefaultFormats

import scala.util.Try
import collection.mutable.ListBuffer
import org.json4s.jackson.JsonMethods._


object BinFeatureApp {

  val defaultNu = -10000000.0

  /**
    *
    * @param line schema:(featureName, featureVal, label)
    * @param featureIDs schema:String
    * @return ret schema: List[(featureName, featureVal, label)]
    */
  def getFeaturesWithLabel(line: String, featureIDs: Array[String]): List[(String, Double, Int)] = {
    val ret = ListBuffer[(String, Double, Int)]()
    val fields = line.split('\t')
    val label = fields(2).split(":")(0)
    val featureStrings = fields(3).split(' ')
    featureStrings.foreach(featureString => {
      val fields = featureString.split('\001')
      val FeatureID = fields(0)
      if(featureIDs.contains(FeatureID)){
        val featureVal = if(!Try(fields(1).toDouble).isSuccess) defaultNu else fields(1).toDouble
        ret += Tuple3(FeatureID.toString, featureVal, label.toInt)
      }
    })
    ret.toList
    //schema: FeatureID_featureVal_label, cnt
  }

  /**
    *
    * @param featureClickView schema:(featureName, featureVal, label)
    * @param featureBinMethod schema:String
    * @return featureBinRange schema: Array[Double]
    */
  def getFeatureBinRange(featureClickView:RDD[(String, Double, Int)],featureBinMethod:String): Array[Double] = {
    if(featureBinMethod == "" || featureBinMethod.split(":")(0) == "equal_view_interval"){
      val viewCnt = featureClickView.count()
      val binNumber = featureBinMethod.split(":")(1).toInt
      val interval = viewCnt / binNumber
      val binSample = featureClickView.sortBy(line => line._2).zipWithIndex().filter(line => (0L until viewCnt by interval).contains(line._2))
      val range = binSample.map(line => line._1._2).collect().distinct
      return range
    }
    Array(-1.0)
  }

  def binFeature(featureValDouble: Double, featureRange: Array[Double]): String = {
    if (featureValDouble == defaultNu) {
      defaultNu.toString
    }
    else {
      (featureRange.size - featureRange.filter(a => a>=featureValDouble).size).toString
    }
  }

  def main(args: Array[String]): Unit = {
    //config and spark context
    if(args.length < 3){
      println("usage: program config.json trainDataPath resultOutputPath")
    }
    val configString = scala.io.Source.fromFile(args(0)).getLines().mkString
    val configJson = parse(configString)
    implicit val formats = DefaultFormats
    val featureIDs = (configJson \ "FeatureIDs").extract[String].split(",")
    val featureBinMethod = (configJson \ "FeatureBinMethods").extract[String]
    val trainDataPath = args(1)
    val resultOutputPath = args(2)
    val conf = if(SystemUtils.IS_OS_MAC) new SparkConf().setMaster("local[1]").setAppName("BinaryFeatureStatistics") else new SparkConf().setAppName("BinaryFeatureStatistics")
    val sc = new SparkContext(conf)
    val trainFile = sc.textFile(trainDataPath).coalesce(120)

    //get feature bin range
    val featureWithLabel = trainFile.flatMap(line => getFeaturesWithLabel(line, featureIDs))
    featureWithLabel.cache()
    var featureBinedAll: RDD[String] = sc.emptyRDD[String]
    val featureBinRange = scala.collection.mutable.Map[String, Array[Double]]()
    featureIDs.foreach(featureID =>{
      val featureSingle = featureWithLabel.filter(line => line._1 == featureID)
      featureSingle.cache()
      featureBinRange(featureID) = getFeatureBinRange(featureSingle, featureBinMethod)
      val featureBined = featureSingle.map(line => (line._1 + "_" + binFeature(line._2,
        featureBinRange(featureID)) + "_" + line._3.toString, 1)).reduceByKey(_ + _).map(x => x._1 + "\t" + x._2)
      featureBinedAll = featureBinedAll.union(featureBined)
      //output: FeatureID_featureVal_label \t cnt
    })
    sc.parallelize(featureBinRange.toList).sortByKey().map(line => line._1 + "\t" + line._2.mkString(",")).saveAsTextFile(resultOutputPath + "/range")
    for (elem <- featureBinRange) { println(elem._1, elem._2.mkString(" "))}
    featureBinedAll.saveAsTextFile(resultOutputPath + "/binFeatureCnt")

  }
}

