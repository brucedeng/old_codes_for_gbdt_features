import sys
import json
from itertools import product
"""
Print feature distribution by bin.
"""

# load config
configJson = json.load(file('config.json'))
config_file_path = 'config.json'
if len(sys.argv) > 1:
    config_file_path = sys.argv[1]
allFeatureID = configJson['FeatureIDs'].split(',')
allMetric = configJson['Metric'].split(',')

# {FeatureID_featureVal, {click:?, view:?, ctr:?}}
result = {}
# get all feature click and view
for line in file('binFeatureCnt'):
    FeatureID_featureVal_label, cnt = line.rstrip().split('\t')
    cnt = int(cnt)
    FeatureID, featureVal, label = FeatureID_featureVal_label.split('_')
    FeatureID_featureVal = '_'.join((FeatureID, featureVal))
    event_type = 'click' if label == '1' else 'view'
    if FeatureID_featureVal in result:
        result[FeatureID_featureVal][event_type] = cnt
    else:
        result[FeatureID_featureVal] = {event_type: cnt}

# calculate ctr of all features
for metric in result.itervalues():
    metric['ctr'] = float(metric.get('click', 0)) / metric.get('view', sys.maxint)

# get bin range
featureID_range = {}
for line in file('range'):
    line = line.rstrip()
    featureID, featureRange = line.split('\t')
    featureRange = '\n'.join(featureRange.split(','))
    featureID_range[featureID] = featureRange

# print all features bined
for curFeatureID, curMetric in product(allFeatureID, allMetric):
    D = dict()
    for FeatureID_featureVal in result.keys():
        FeatureID, featureVal = FeatureID_featureVal.split('_')
        if FeatureID == curFeatureID:
            featureVal = float(featureVal)
            D[featureVal] = result[FeatureID_featureVal].get(curMetric, 0)
    if allMetric.index(curMetric) == 0:
        print '-' * 40
        print curFeatureID, 'range'
        print featureID_range[curFeatureID]
        print ''
    print curFeatureID, curMetric
    for bin in sorted(D.keys()):
        print D[bin]
    print ''
