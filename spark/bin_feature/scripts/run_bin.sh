#!/usr/bin/env bash
export LD_LIBRARY_PATH=:/usr/lib/hadoop/lib/native
LD_LIBRARY_PATH=:/usr/lib/hadoop/lib/native
TRAIN_DATA_INPUT_PATH=hdfs:/projects/news/deeplearning/model/training/gen_training_data_streaming/us_cm_cxq_test/training_data/20161125/*/*
RESULT_OUTPUT_PATH="/tmp/guozhenyuan/bin_feature/"
BIN_CONFIG=config.json
QUEUE=experiment
JAR_FILE=../target/scala-2.10/binaryfeaturestatistics_2.10-1.0.jar

rm -f binFeatureCnt range
hdfs dfs -rm -r -f ${RESULT_OUTPUT_PATH}
cmd="spark-submit --files $BIN_CONFIG \
    --class BinFeatureApp --master yarn-client \
    --driver-memory 4g --executor-memory 4g --executor-cores 4 --num-executors 30 \
    --conf spark.app.name=binaryfeaturestatistics --queue $QUEUE \
    $JAR_FILE $BIN_CONFIG "${TRAIN_DATA_INPUT_PATH?}" ${RESULT_OUTPUT_PATH?}"
echo ${cmd}
eval ${cmd}
hdfs dfs -text ${RESULT_OUTPUT_PATH}/binFeatureCnt/* > binFeatureCnt
hdfs dfs -text ${RESULT_OUTPUT_PATH}/range/* > range
python ./bin_feature.py ${BIN_CONFIG} > bin.txt
echo "Bin program success, stored in bin.txt"
