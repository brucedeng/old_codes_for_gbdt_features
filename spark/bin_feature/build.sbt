name := "BinaryFeatureStatistics"

version := "1.0"

scalaVersion := "2.10.4"

autoScalaLibrary := false

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.3.0" % "compile"
libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.2.4" % "compile"

