package com.cmcm.ranking.india.runtime

import com.cmcm.ranking.india.runtime.joinUpCpCategory2._
import org.apache.spark.Logging
import org.scalatest.{Matchers, FunSuite}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import com.cmcm.ranking.india.featureUtils._

/**
 * Created by mengchong on 4/11/16.
 */
@RunWith(classOf[JUnitRunner])
class testJoinUpCpCategory2 extends FunSuite {
  test("Load up correctly.") {
    val line = """{"aid" : "1181f9b74c432b", "uid" : "1181f9b74c432b", "version" : "1", "join_flag" : 1, "gender" : "1", "age" : "1", "new_user" : "0", "u_cat_len" : 0, "u_kw_len" : 0, "keywords" : [], "u_cat_len_hindi" : 4, "u_kw_len_hindi" : 63, "categories" : [{"name" : "1000780", "weight" : 0.272702},{"name" : "1000931", "weight" : 0.259067},{"name" : "1000031", "weight" : 0.246114},{"name" : "1000290", "weight" : 0.222117}],"lasttime": "1456112346", "doclist" : [8587194,8632850,8517266,8511034,8511034,8604428]}"""
    val upobj = new upData(new upParser(line))
//    assert(upobj == "1181f9b74c432b\tList((1000780,0.272702), (1000931,0.259067), (1000031,0.246114), (1000290,0.222117))")
//    println(upobj)
  }

  test("Load cp correctly.") {
    val line = """{"aid" : "1181f9b74c432b", "uid" : "1181f9b74c432b", "version" : "1", "join_flag" : 1, "gender" : "1", "age" : "1", "new_user" : "0", "u_cat_len" : 0, "u_kw_len" : 0, "keywords" : [], "u_cat_len_hindi" : 4, "u_kw_len_hindi" : 63, "categories" : [{"name" : "1000780", "weight" : 0.272702},{"name" : "1000931", "weight" : 0.259067},{"name" : "1000031", "weight" : 0.246114},{"name" : "1000290", "weight" : 0.222117}],"lasttime": "1456112346", "doclist" : [8587194,8632850,8517266,8511034,8511034,8604428]}"""
    val upobj = new cpData(new cpParser(line))
//    println(upobj)
  }

  test("Load pv.") {
    val line = """{"act":"1","action":"","aid":"1072c29b503e5ee4","apiv":"3","app_lan":"hi","appv":"1.2.2","brand":"samsung","ch":"200001","contentid":"10936010","cpack":{"des":"rid=9e8fe0ff|src=8|ord=12","ishot":1,"md5":"d5b21744"},"ctype":"1","display":"0x02","ext":{"eventtime":"1459918838","requesttime":"1459918801"},"id":"","idfa":"","ip":"49.15.184.230, 107.167.111.66","lan":"hi_IN","mcc":"404","mnc":"78","model":"SM-A500G","net":"3G","nmcc":"404","nmnc":"78","osv":"5.0.2","pf":"android","pid":"11","refer":{},"scenario":{"column":"","is_level1_native":"1","level1":"1","level1_type":"1","level2":"29","source":""},"servertime":"2016-04-06 05:00:40","servertime_sec":"1459918840","upack":{"exp":"insta_hindi_042"},"uuid":"","value":""}"""
    val obj = new eventData(new featureParser(line))
//    println(obj)
  }

  test("Load click.") {
    val line = """{"act":"2","action":"","aid":"2f517f7f1606f922","apiv":"3","app_lan":"en","appv":"1.2.1","brand":"htc","ch":"200001","contentid":"10902841","cpack":{"des":"rid=ef1b3154|src=8|ord=13","ishot":1,"md5":"a7099e1a"},"ctype":"1","display":"0x02","ext":{"eventtime":"1459919300"},"id":"","idfa":"","ip":"197.156.77.118","lan":"en_IN","mcc":"636","mnc":"01","model":"HTC Desire 620G dual sim","net":"3G","nmcc":"636","nmnc":"01","osv":"4.4.2","pf":"android","pid":"11","refer":{},"scenario":{"column":"","is_level1_native":"1","level1":"1","level1_type":"1","level2":"29","source":""},"servertime":"2016-04-06 05:08:24","servertime_sec":"1459919304","upack":{"exp":"insta_045"},"uuid":"","value":""}"""
    val obj = new eventData(new featureParser(line))
//    println(obj)
  }

  test("Load dwelltime.") {
    val line = """{"act":"4","action":"","aid":"e6c0ac8f202b0157","apiv":"3","app_lan":"hi","appv":"1.2.1","brand":"YU","ch":"200001","contentid":"10916299","cpack":{"des":"rid=b238800d|src=8|ord=19","ishot":1,"md5":"af57d873"},"ctype":"1","display":"0x02","ext":{"dwelltime":"799","eventtime":"1459918962"},"id":"","idfa":"","ip":"117.220.23.170","lan":"en_US","mcc":"404","mnc":"14","model":"YU5010","net":"wifi","nmcc":"404","nmnc":"14","osv":"5.1.1","pf":"android","pid":"11","refer":{},"scenario":{"column":"","is_level1_native":"1","level1":"1","level1_type":"1","level2":"29","source":""},"servertime":"2016-04-06 05:02:55","servertime_sec":"1459918975","upack":{"exp":"insta_hindi_040"},"uuid":"","value":""}"""
    val obj = new eventData(new featureParser(line))
//    println(obj)
  }
}
