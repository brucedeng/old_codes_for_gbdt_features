package com.cmcm.ranking.india.trainUp.flows

import java.io.File

import com.cmcm.ranking.india.trainUp._
import com.typesafe.config.{ConfigFactory, Config}
import org.apache.spark.ml.classification.LogisticRegression
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Row, DataFrame}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by mengchong on 5/11/16.
 */
class trainCategoryOnly(config:Config) extends Flow {
  val elasticNetParam = config.getDouble("elastic_net_param")
  val regParam = config.getDouble("reg_param")
  val catInputPath = config.getString("formatted_data_path")
  val appName = config.getString("app_name")
  val catModelDir = config.getString("category_model_output")
  val localModelFile = config.getString("local_model_file")

  override def execute() = {

    val desc = "train up with " +
      "1. only use category cross, weighted to 1:1 \n" +
      "2. filtered only events with cp category not empty\n" +
      "3. empty gmp is -1 in training data. must be set to 0 before use\n" +
      "4. L1 normalize up, added up filter. remove user profile less than upThreshold clicks\n" +
      "6. normalize up factors. minus mean and divide by std.\n" +
      "6. filter out cubes with less than 100 samples\n" +
      "7. normalize up categories."

    println(desc)

    // initializing context
    val conf = new SparkConf().setAppName(appName)
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._

    sc.broadcast(modelUtils)

    println("loading data from "+ catInputPath + "/train_rdd")
    val trainData = sc.objectFile[event](catInputPath + "/train_rdd")
    val testData = sc.objectFile[event](catInputPath + "/test_rdd")

    val crossBlackList = sc.objectFile[String](catInputPath + "/cat_cross_black_list")
      .collect().toSet
    println("black list is : " + crossBlackList)
    sc.broadcast(crossBlackList)

    val catWeights = sc.objectFile[(String,(Double,Double))](catInputPath + "/cat_weights")
      .collect().toMap
    sc.broadcast(catWeights)
    println("weights:")
    println(catWeights)

    val upAvgStd: Map[String, Statistic] = sc.objectFile[(String,Statistic)](catInputPath + "/up_cat_statistic").collect().toMap
    println("up category average:")
    println(upAvgStd)
    sc.broadcast(upAvgStd)

    // category training data
    val training = trainData
      .map(v => {
        val wt = modelUtils.genWeight(v.cpCats, catWeights, v.label)
        val normUcat = modelUtils.normalize(v.upCats, upAvgStd)
        (v.label, modelUtils.genFeatures(0.0, normUcat, v.cpCats, 0.0, crossBlackList), wt,v.uid,v.dwelltime)
      })
      .filter(x => x._2.numNonzeros != 0)
      .toDF("label", "features", "weight","uid","dwelltime")
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    training.printSchema()
    training.write.parquet(catModelDir + "/training")

    // category testing data
    val test = testData
      .map(v => {
        val normUcat = modelUtils.normalize(v.upCats, upAvgStd)
        (v.label, modelUtils.genFeatures(0.0, normUcat, v.cpCats, 0.0, crossBlackList), 1.0,v.uid,v.dwelltime)
      })
      .filter(x => x._2.numNonzeros != 0)
      .toDF("label", "features", "weight","uid","dwelltime")
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    test.write.parquet(catModelDir + "/testing")
    //    MLUtils.saveAsLibSVMFile(test,outputDataPath + "/test")

    // Create a LogisticRegression instance.  This instance is an Estimator.
    val lr = new LogisticRegression()
    // Print out the parameters, documentation, and any default values.
    println("LogisticRegression parameters:\n" + lr.explainParams() + "\n")

    // We may set parameters using setter methods.
    lr.setMaxIter(1000)
      .setRegParam(regParam)
      .setStandardization(false)
      .setElasticNetParam(elasticNetParam)
      .setFitIntercept(true)
      .setWeightCol("weight")

    // We may alternatively specify parameters using a ParamMap,
    // which supports several methods for specifying parameters.
    val paramMap = ParamMap(lr.maxIter -> 20)
          .put(lr.maxIter, 1000) // Specify 1 Param.  This overwrites the original maxIter.
          .put(lr.regParam -> regParam) // Specify multiple Params.
          .put(lr.probabilityCol -> "probability")

    // Now learn a new model using the paramMapCombined parameters.
    // paramMapCombined overrides all parameters set earlier via lr.set* methods.
    val model = lr.fit(training, paramMap)
    //    training.unpersist()

    println("saving model to " + catModelDir + "/cat_models")
    model.save(catModelDir + "/cat_models")

    training.unpersist()
    // print coefficients
    val param = model.coefficients.toArray
    println("paramater size is : " + param.length.toString)
    println("paramaters:")
    modelUtils.printCatCoef(param)
    println("model summary:")
    modelUtils.printModelSummary(model)

    ioUtils.saveCoef2File(localModelFile, modelUtils.genCoefList(param))

    println("\nformatted_data_path\t" + catInputPath)
    println("category_model_output\t" + catModelDir)
    println("elastic_net_param\t" + elasticNetParam)
    println("reg_param\t" + regParam)

    rddUtils.evaluate(model.transform(training),"category train")
    rddUtils.evaluate(model.transform(test),"category train")
  }
}
