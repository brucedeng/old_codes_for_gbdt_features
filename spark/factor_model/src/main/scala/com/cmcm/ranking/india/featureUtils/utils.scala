package com.cmcm.ranking.india.featureUtils

import java.io.InputStream

/**
 * Created by mengchong on 4/10/16.
 */

object utils {
  def formatCatKwList(data:List[Map[String,Any]], name:String = "name", weight:String = "weight"):  List[(String,Double)]={
    def parseCatMap (data:Map[String,Any]): (String,Double) =
      if (data.contains(name) && data.contains(weight)) (data(name).asInstanceOf[String],data(weight).asInstanceOf[Double])
      else null

    data.map(parseCatMap).filter(x => x!=null)
  }

  def formatCatKwListSeq(data:Seq[Map[String,Any]], name:String = "name", weight:String = "weight"):  Seq[(String,Double)]={
    def parseCatMap (data:Map[String,Any]): (String,Double) =
      if (data.contains(name) && data.contains(weight)) (data(name).asInstanceOf[String],data(weight).asInstanceOf[Double])
      else null

    data.map(parseCatMap).filter(x => x!=null)
  }

  def getCatTree : Map[String,(String,String)] = {
    val stream : InputStream = getClass.getResourceAsStream("/cat_tree.txt")
    val lines = scala.io.Source.fromInputStream( stream ).getLines()
    val cats = lines.map(x => {
      val fields = x.split('\t')
      val origCat = fields(0)
      val destCat = fields(1)
      val name = fields(2)
      (origCat,(destCat,name))
    })
    cats.toMap
  }

  def getL1Cat (cat:String, catMap:Map[String,(String,String)]) :String = {
    if (cat == "1000849" ) cat
    else {
      catMap get cat match {
        case Some((parent,name)) => {
          if(parent == "root" ) cat
          else getL1Cat(parent,catMap)
        }
        case None => "null"
      }
    }
  }

  def L1_norm(l : Seq[(String,Double)],factor:Double = 0) = {
    val sum = l.filter(x => x._2 > 0).map(x => x._2).sum + factor
    if (sum > 0) l.map(x => (x._1,x._2/sum))
    else Seq[(String,Double)]()
  }

//  def simplizeMap(config:String): Map[String,Any] = {}

}
