package com.cmcm.ranking.india.trainUp.dep

import com.cmcm.ranking.india.featureUtils.utils
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by mengchong on 4/20/16.
 */
object trainUpCatKw {
  val catMap = utils.getCatTree
  val normFactor = 3.0
  val upThreshold = 3.0

  def main(args: Array[String]) {
    val desc = "train up with 1. inception for each category. 2. without weight"
    val dataPath = args(0)
    val outputPath = args(1)

    upUtils.upThreshold = upThreshold
    upUtils.normFactor = normFactor
    //    val outputDataPath = args(2)
    println("Loading data: %s".format(dataPath))

    val conf = new SparkConf().setAppName("train up")
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._
    val eventData = sc.textFile(dataPath).map(upUtils.parseEventIntercept).cache()

    val catNames = eventData.flatMap(x => x._3).reduceByKey((a, b) => a + b).collect()
    println("up categories")
    catNames.foreach(x => {
      println(x._1 + ',' + x._2.toString)
    })
    println("cp categories")
    val catNames2 = eventData.flatMap(x => x._4).reduceByKey((a, b) => a + b).collect()
    catNames2.foreach(x => {
      println(x._1 + ',' + x._2.toString)
    })
    val allCat = (catNames ++ catNames2).filter(x => x._2 > 1000).map(x => x._1)
    val catMap = allCat.toSet[String].toList.sorted.zipWithIndex.toMap
    println(catMap)

    val trainData = eventData.map(x => upUtils.genFeature(x._1, x._2, x._3, x._4, x._5, catMap))
    trainData.toDF().printSchema()
//    println("saving to " + outputPath + "/libsvm_data")
//    MLUtils.saveAsLibSVMFile(trainData, outputPath + "/libsvm_data")
//    eventData.unpersist()
//
//    val splits = trainData.randomSplit(Array(0.6, 0.4), seed = 11L)
//    val training = splits(0).cache()
//    //    MLUtils.saveAsLibSVMFile(training,outputDataPath + "/train")
//    val test = splits(1)
//    //    MLUtils.saveAsLibSVMFile(test,outputDataPath + "/test")
//
//    // Create a LogisticRegression instance.  This instance is an Estimator.
//    val lr = new LogisticRegression()
//    // Print out the parameters, documentation, and any default values.
//    println("LogisticRegression parameters:\n" + lr.explainParams() + "\n")
//
//    // We may set parameters using setter methods.
//    lr.setMaxIter(1000)
//      .setRegParam(0.01)
//      .setElasticNetParam(0.0)
//      .setFitIntercept(false)
//
//    // We may alternatively specify parameters using a ParamMap,
//    // which supports several methods for specifying parameters.
//    val paramMap = ParamMap(lr.maxIter -> 20)
//      .put(lr.maxIter, 1000) // Specify 1 Param.  This overwrites the original maxIter.
//      .put(lr.regParam -> 0.1) // Specify multiple Params.
//
//    // One can also combine ParamMaps.
//    val paramMap2 = ParamMap(lr.probabilityCol -> "myProbability") // Change output column name
//    val paramMapCombined = paramMap ++ paramMap2
//
//    // Now learn a new model using the paramMapCombined parameters.
//    // paramMapCombined overrides all parameters set earlier via lr.set* methods.
//    val model2 = lr.fit(training.toDF(), paramMapCombined)
//    training.unpersist()
//    val param = model2.coefficients.toArray
//    println("Model 2 was fit using parameters: " + model2.parent.extractParamMap)
//    println("Model 2 coef : " + model2.coefficients.toArray.toList)
//    println("Model 2 inception " + model2.intercept.toString)
//    println("saving model to " + outputPath + "/model")
//    model2.save(outputPath + "/model")
//
//    // Make predictions on test data using the Transformer.transform() method.
//    // LogisticRegression.transform will only use the 'features' column.
//    // Note that model2.transform() outputs a 'myProbability' column instead of the usual
//    // 'probability' column since we renamed the lr.probabilityCol parameter previously.
//    val predictionAndLabels = model2.transform(test.toDF())
//      .select("features", "label", "myProbability", "prediction")
//      .map { case Row(features: Vector, label: Double, prob: Vector, prediction: Double) =>
//        (prob(1), label)
//      }.cache()
//
//    val trainingSummary = model2.summary
//    val binarySummary = trainingSummary.asInstanceOf[BinaryLogisticRegressionSummary]
//    // Obtain the receiver-operating characteristic as a dataframe and areaUnderROC.
//    println("train auc: " + binarySummary.areaUnderROC)
//
//    predictionAndLabels.saveAsTextFile(outputPath + "/test")
//    val metrics = new BinaryClassificationMetrics(predictionAndLabels)
//    val auROC = metrics.areaUnderROC()
//    println("test auc: " + auROC)
//    predictionAndLabels.unpersist()
//
//    val catTree = utils.getCatTree
//    val catMap2 = catMap
//      .map(x => {
//        val catName = catTree.get(x._1)
//        catName match {
//          case Some((cid, cname)) => (x._1 + "," + cname, x._2)
//          case None => x
//        }
//      })
//
//    println("paramater size is : " + param.length.toString)
//    println("paramaters:")
//
//    runUtils.printArg(runUtils.formatArg(param.toList, catMap2, 2 + catMap2.size))
//    println("\nintercepts:")
//    runUtils.printIntercept(param,catMap2,2)

  }
}
