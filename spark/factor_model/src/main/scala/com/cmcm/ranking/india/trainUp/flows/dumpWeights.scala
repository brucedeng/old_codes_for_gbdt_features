package com.cmcm.ranking.india.trainUp.flows

import redis.clients.jedis.Jedis
import com.cmcm.ranking.india.trainUp.{ioUtils, modelUtils}
import com.typesafe.config.Config

/**
 * Created by mengchong on 5/26/16.
 */
class dumpWeights(config:Config) extends Flow {

  val localModelFile = config.getString("local_model_file")
  val catModelTruncate = config.getInt("category_weight_truncate")
  val redisKeys = config.getString("redis_key")
  val redisHost = config.getString("redis_host")
  val redisPort = config.getInt("redis_port")
  val redisDb = config.getInt("redis_db")

  override def execute(): Unit = {
    val param = modelUtils.parseCoefListWithTruncate(ioUtils.loadCoefFile(localModelFile), catModelTruncate)
    val jsonData = modelUtils.formatJsonCoefList(param)
    val redisClient = new Jedis(redisHost, redisPort)
    redisClient.select(redisDb)
    redisClient.set(redisKeys,jsonData)
    println(jsonData)
    redisClient.close()
  }

}
