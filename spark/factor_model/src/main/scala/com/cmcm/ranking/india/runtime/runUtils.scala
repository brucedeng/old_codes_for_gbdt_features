package com.cmcm.ranking.india.runtime

/**
 * Created by mengchong on 4/18/16.
 */
object runUtils {
  def formatArg (args:List[Double],featMap:Map[String,Int],shift:Int) : List[List[String]] = {
    val fnames = featMap.toList.sortBy(_._2).map(_._1)
    val length = featMap.size
    val data = for (ucat <- fnames) yield{
        val line = fnames.map(x => {
          val lidx = featMap(ucat)
          val ridx = featMap(x)
          args(lidx * length + ridx + shift).toString
        })
        List(ucat) ++ line
    }
    ("" :: fnames.toList) :: data
  }

  def printArg (data:List[List[String]]) : List[List[String]] = {
    data.map(x => {
      println(x.mkString("\t"))
      x
    })
  }

  def printIntercept (data:Array[Double],featMap:Map[String,Int],shift:Int) = {
    println(featMap.map(x => (x._1,data(x._2 + shift))))
  }
}
