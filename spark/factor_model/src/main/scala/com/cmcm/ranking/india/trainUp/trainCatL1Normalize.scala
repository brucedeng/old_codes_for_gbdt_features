package com.cmcm.ranking.india.trainUp

import java.io.File
import com.cmcm.ranking.india.featureUtils.utils
import com.typesafe.config.ConfigFactory
import org.apache.spark.ml.classification.LogisticRegression
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Row, DataFrame}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by mengchong on 5/4/16.
 */
object trainCatL1Normalize {
  val catMap = utils.getCatTree

  def crossCats (x:(Seq[(String,Double)],Seq[(String,Double)])) = {
    for(a <- x._1; b <- x._2) yield {
      (a._1 + "-" + b._1,1)
    }
  }

  def main(args: Array[String]) {
    val desc = "train up with " +
      "1. only use category cross, weighted to 1:1 \n" +
      "2. filtered only events with cp category not empty\n" +
      "3. empty gmp is -1 in training data. must be set to 0 before use\n" +
      "4. L1 normalize up, added up filter. remove user profile less than upThreshold clicks\n" +
      "6. normalize up factors. minus mean and divide by std.\n" +
      "6. filter out cubes with less than 100 samples\n" +
      "7. normalize up categories."

    println(desc)
    val configPath = args(0)

    val config = ConfigFactory.parseFile(new File(configPath))

    val l1Norm = config.getString("l1_norm")
    val normFactor = config.getDouble("norm_factor")
    val rollUpCats = config.getString("roll_up_cats")
    val normalizeUP = config.getString("normalize_up")
    val upThreshold = config.getDouble("up_threshold")
    val trainSampleThreshold = config.getDouble("train_sample_threshold")
    val elasticNetParam = config.getDouble("elastic_net_param")
    val regParam = config.getDouble("reg_param")
    val dataPath = config.getString("data")
    val testPath = config.getString("test")
    val outputPath = config.getString("output")
    val appName = config.getString("app_name")
    val phase1Dir = config.getString("phase1_output")
    val localModelFile = config.getString("local_model_file")
    val trainStages = config.getString("train_stages").split(',').toSet

    println("Loading data: %s".format(dataPath))

    // initializing context
    val conf = new SparkConf().setAppName(appName)
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._

    sc.broadcast(modelUtils)

    // initialize normalize and rollup functions
    val normFunc:(Seq[(String,Double)],Double)=> Seq[(String,Double)] =
      if(l1Norm.toLowerCase == "true"){
        println("Using L1 normalize on categories and keywords")
        modelUtils.L1_norm
      }
      else (a,b) => a
    val rollupFunc:(Seq[(String,Double)])=> Seq[(String,Double)] =
      if(rollUpCats.toLowerCase == "true" ) {
        println("Using rollup on categories")
        modelUtils.rollupCats
      }
      else x=>x
    sc.broadcast(normFunc)
    sc.broadcast(rollupFunc)
    val parseInputFunc:(DataFrame,Double,Double) => RDD[event] = rddUtils.parseInputDataFrameNorm(normFunc,rollupFunc)

    // load input data
    val inputData = sqlContext.read.parquet(dataPath)
      .select("uid","content_id","label","dwelltime","u_cats","u_keywords","d_cats","d_keywords","gmp")

    inputData.printSchema()

    //    val trainData = parseInputDataFrame(inputData)
    val trainData = parseInputFunc(inputData,upThreshold,normFactor )

    trainData.saveAsTextFile(outputPath+ "/raw_train_data")
    val crossBlackList = rddUtils.genCrossBlackList(trainData,trainSampleThreshold)

    println("black list is : " + crossBlackList)
    sc.broadcast(crossBlackList)

    val catWeights = rddUtils.genCatWeight(trainData)
    sc.broadcast(catWeights)
    println("weights:")
    println(catWeights)

    val upAvgStd:Map[String,Statistic] = if(normalizeUP == "true") rddUtils.genUserCatAvg(trainData)
      else Map()
    println("up category average:")
    println(upAvgStd)

    sc.broadcast(upAvgStd)

//    val splits = trainData.randomSplit(Array(0.8, 0.2), seed = 11L)
    //    trainData.saveAsTextFile(outputPath + "/raw_train_data")
    // load input data
    val inputTestData = sqlContext.read.parquet(testPath)
      .select("uid","content_id","label","dwelltime","u_cats","u_keywords","d_cats","d_keywords","gmp")
    val testData = parseInputFunc(inputTestData,upThreshold,normFactor )

    // category training data
    val training = trainData
      .map(v => {
        val wt = modelUtils.genWeight(v.cpCats,catWeights,v.label)
        val normUcat = modelUtils.normalize(v.upCats,upAvgStd)
        (v.label, modelUtils.genFeatures(0.0,normUcat, v.cpCats, 0.0,crossBlackList), wt)
      })
      .filter(x => x._2.numNonzeros != 0 )
      .toDF("label","features","weight")
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    training.printSchema()
    training.write.parquet(outputPath + "/training")

    // category testing data
    val test = testData
      .map(v => {
        val normUcat = modelUtils.normalize(v.upCats,upAvgStd)
        (v.label, modelUtils.genFeatures(0.0, normUcat, v.cpCats, 0.0,crossBlackList), 1.0)
      })
      .filter(x => x._2.numNonzeros != 0 )
      .toDF("label","features","weight")
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    test.write.parquet(outputPath + "/testing")
    //    MLUtils.saveAsLibSVMFile(test,outputDataPath + "/test")

    // Create a LogisticRegression instance.  This instance is an Estimator.
    val lr = new LogisticRegression()
    // Print out the parameters, documentation, and any default values.
    println("LogisticRegression parameters:\n" + lr.explainParams() + "\n")

    // We may set parameters using setter methods.
    lr.setMaxIter(1000)
      .setRegParam(regParam)
      .setStandardization(false)
      .setElasticNetParam(elasticNetParam)
      .setFitIntercept(true)
      .setWeightCol("weight")

    // We may alternatively specify parameters using a ParamMap,
    // which supports several methods for specifying parameters.
    val paramMap = ParamMap(lr.maxIter -> 20)
          .put(lr.maxIter, 1000) // Specify 1 Param.  This overwrites the original maxIter.
          .put(lr.regParam -> regParam) // Specify multiple Params.
          .put(lr.probabilityCol -> "probability")

    // Now learn a new model using the paramMapCombined parameters.
    // paramMapCombined overrides all parameters set earlier via lr.set* methods.
    val model = lr.fit(training, paramMap)
    //    training.unpersist()

    println("saving model to " + outputPath + "/cat_model")
    model.save(outputPath + "/cat_models")

    training.unpersist()
    // print coefficients
    val param = model.coefficients.toArray
    println("paramater size is : " + param.length.toString)
    println("paramaters:")
    modelUtils.printCatCoef(param)
    println("model summary:")
    modelUtils.printModelSummary(model)

    ioUtils.saveCoef2File(localModelFile,modelUtils.genCoefList(param))


    // Make predictions on test data using the Transformer.transform() method.
    // LogisticRegression.transform will only use the 'features' column.
    // Note that model.transform() outputs a 'probability' column instead of the usual
    // 'probability' column since we renamed the lr.probabilityCol parameter previously.
    val predictionAndLabels = model.transform(test)
      .select("features", "label", "probability", "prediction")
      .map { case Row(features: Vector, label: Double, prob: Vector, prediction: Double) =>
        (prob(1), label)
      }
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    test.unpersist()

    // calculate test auc
    predictionAndLabels.saveAsTextFile(outputPath + "/cat_test")
    val metrics = new BinaryClassificationMetrics(predictionAndLabels,1000)
    val auROC = metrics.areaUnderROC()
    println("cat test auc:\t" + auROC)
    predictionAndLabels.unpersist()


    // generate training data for phase-1
    val overallWtRaw = trainData.map(x => (x.label,if(x.label > 0) 0.0 else 1.0)).reduce((a,b) => (a._1 + b._1,a._2 + b._2))
    val overallWt = (overallWtRaw._2/overallWtRaw._1,1.0)
    sc.broadcast(overallWt)

    val gmpTraining = trainData
      .map(v => {
        //      case (label:Double,gmp:Double,kwRel:Double,upCats,cpCats,uid:String, content_id:String) =>
        val wtOverall = if(v.label > 0) overallWt._1 else overallWt._2
        val normUcat = modelUtils.normalize(v.upCats,upAvgStd)

        (v.label,modelUtils.genFeatureWithCatRel(v.gmp,normUcat,v.cpCats,v.kwRel,param),wtOverall)
      })
      .filter(x => x._2.numNonzeros != 0 )
      .toDF("label","features","weight")

    gmpTraining.printSchema()
    println("writing gmp train data into " + outputPath + "/phase_1_train")
    gmpTraining.write.parquet(outputPath + "/phase_1_train")

    // category testing data
    val gmpTest = testData
      .map(v => {
        val normUcat = modelUtils.normalize(v.upCats,upAvgStd)
        (v.label,modelUtils.genFeatureWithCatRel(v.gmp,normUcat,v.cpCats,v.kwRel,param),1.0,v.uid)
      })
//      .filter(x => x._2.numNonzeros != 0 )
      .toDF("label","features","weight","uid")

    println("writing gmp test data into " + outputPath + "/phase_1_test")
    gmpTest.write.parquet(outputPath + "/phase_1_test")

    val gmpTraining2 = trainData
      .map(v => {
        //      case (label:Double,gmp:Double,kwRel:Double,upCats,cpCats,uid:String, content_id:String) =>
        val wtOverall = if(v.label > 0) overallWt._1 else overallWt._2
        val normUcat = modelUtils.normalize(v.upCats,upAvgStd)

        (v.label,modelUtils.genFeatureWithCatRel(v.gmp,normUcat,v.cpCats,0,param),wtOverall)
      })
      .filter(x => x._2.numNonzeros != 0 )
      .toDF("label","features","weight")
    println("writing gmp train data into " + outputPath + "/phase_1_train_nokwrel")
    gmpTraining2.write.parquet(outputPath + "/phase_1_train_nokwrel")

    // category testing data
    val gmpTest2 = testData
      .map(v => {
        val normUcat = modelUtils.normalize(v.upCats,upAvgStd)
        (v.label,modelUtils.genFeatureWithCatRel(v.gmp,normUcat,v.cpCats,0,param),1.0,v.uid)
      })
//      .filter(x => x._2.numNonzeros != 0 )
      .toDF("label","features","weight","uid")

    println("writing gmp test data into " + outputPath + "/phase_1_test_nokwrel")
//    gmpTest2.write.parquet(outputPath + "/phase_1_test_nokwrel")
  }
}

