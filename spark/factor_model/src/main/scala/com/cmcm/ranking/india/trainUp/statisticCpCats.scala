package com.cmcm.ranking.india.trainUp

import com.cmcm.ranking.india.featureUtils.utils
import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by mengchong on 4/21/16.
 */
object statisticCpCats {
  def main(args: Array[String]) {
    val desc = "stat each cp category content count"
    val dataPath = args(0)
    val lan = args(1)
    //    val outputPath = args(1)
    //    val skipMap = args(2)

    println("Loading data: %s".format(dataPath))

    val conf = new SparkConf().setAppName("up_cat")
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
//    import sqlContext.implicits._

    val df = sqlContext.read.format("json").load(dataPath)
    df.printSchema()

    val neededFileds = df.select("item_id","is_servable","cp_disable","type","categories","update_time","language")
      .filter("is_servable and not cp_disable and type = 'article' and language = '" + lan + "'")
      .select("item_id","categories","update_time")
      .map(x => (x.getString(0),(x.getSeq[org.apache.spark.sql.Row](1),x.getLong(2))))
      .reduceByKey((a,b) => if(a._2 > b._2) a else b)
      .flatMap(x =>  utils.L1_norm( x._2._1.map( row =>(row.getString(0),row.getDouble(1)))))
      .reduceByKey((a,b) => a+b)

    neededFileds.collect().sortBy(x => - x._2).foreach(x => println(x._1 + "\t" + x._2))

  }
}
