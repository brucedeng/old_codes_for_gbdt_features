package com.cmcm.ranking.india.trainUp.flows

import java.io.File

import com.cmcm.ranking.india.featureUtils.utils
import com.cmcm.ranking.india.trainUp._
import com.typesafe.config.{ConfigFactory, Config}
import org.apache.spark.ml.classification.LogisticRegression
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Row, DataFrame}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by mengchong on 5/10/16.
 */
class parseRawData(config:Config) extends Flow{
  val catMap = utils.getCatTree
  val l1Norm = config.getString("l1_norm")
  val normFactor = config.getDouble("norm_factor")
  val rollUpCats = config.getString("roll_up_cats")
  val normalizeUP = config.getString("normalize_up")
  val upThreshold = config.getDouble("up_threshold")
  val trainSampleThreshold = config.getDouble("train_sample_threshold")
  val dataPath = config.getString("data")
  val testPath = config.getString("test")
  val outputPath = config.getString("formatted_data_path")
  val appName = config.getString("app_name")

  override def execute() {
    val desc = "train up with " +
      "1. only use category cross, weighted to 1:1 \n" +
      "2. filtered only events with cp category not empty\n" +
      "3. empty gmp is -1 in training data. must be set to 0 before use\n" +
      "4. L1 normalize up, added up filter. remove user profile less than upThreshold clicks\n" +
      "6. normalize up factors. minus mean and divide by std.\n" +
      "6. filter out cubes with less than 100 samples\n" +
      "7. normalize up categories."
    println(desc)

    println("Loading data: %s".format(dataPath))

    // initializing context
    val conf = new SparkConf().setAppName(appName)
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._

    sc.broadcast(modelUtils)

    // initialize normalize and rollup functions
    val normFunc:(Seq[(String,Double)],Double)=> Seq[(String,Double)] =
      if(l1Norm.toLowerCase == "true"){
        println("Using L1 normalize on categories and keywords")
        modelUtils.L1_norm
      }
      else (a,b) => a
    val rollupFunc:(Seq[(String,Double)])=> Seq[(String,Double)] =
      if(rollUpCats.toLowerCase == "true" ) {
        println("Using rollup on categories")
        modelUtils.rollupCats
      }
      else x=>x
    sc.broadcast(normFunc)
    sc.broadcast(rollupFunc)
    val parseInputFunc:(DataFrame,Double,Double) => RDD[event] = rddUtils.parseInputDataFrameNorm(normFunc,rollupFunc)

    // load input data
    val inputData = sqlContext.read.parquet(dataPath)
      .select("uid","content_id","label","dwelltime","u_cats","u_keywords","d_cats","d_keywords","gmp")

    inputData.printSchema()

    //    val trainData = parseInputDataFrame(inputData)
    val trainData = parseInputFunc(inputData,upThreshold,normFactor )

    trainData.saveAsTextFile(outputPath + "/raw_train_data")
    trainData.saveAsObjectFile(outputPath + "/train_rdd")
    val crossBlackList = rddUtils.genCrossBlackList(trainData,trainSampleThreshold)

    println("black list is : " + crossBlackList)
    val blackListRdd = sc.parallelize(crossBlackList.toSeq)
    blackListRdd.saveAsObjectFile(outputPath + "/cat_cross_black_list")

    val catWeights = rddUtils.genCatWeight(trainData)
    val catWeightsRdd = sc.parallelize(catWeights.toSeq)
    catWeightsRdd.saveAsObjectFile(outputPath + "/cat_weights")
    println("weights:")
    println(catWeights)

    val upAvgStd:Map[String,Statistic] = if(normalizeUP == "true") rddUtils.genUserCatAvg(trainData)
    else Map()
    val upAvgStdRdd = sc.parallelize(upAvgStd.toSeq)
    upAvgStdRdd.saveAsObjectFile(outputPath+ "/up_cat_statistic")
    println("up category average:")
    println(upAvgStd)

    val overallWtRaw = trainData.map(x => (x.label,if(x.label > 0) 0.0 else 1.0)).reduce((a,b) => (a._1 + b._1,a._2 + b._2))
    val overallWt = List((overallWtRaw._2/overallWtRaw._1,1.0))
    val overallWtRdd = sc.parallelize(overallWt)
    overallWtRdd.saveAsObjectFile(outputPath + "/overall_weight")

    // load input data
    val inputTestData = sqlContext.read.parquet(testPath)
      .select("uid","content_id","label","dwelltime","u_cats","u_keywords","d_cats","d_keywords","gmp")
    val testData = parseInputFunc(inputTestData,upThreshold,normFactor )

    testData.saveAsObjectFile(outputPath + "/test_rdd")

//    val test2 = sc.objectFile[event](outputPath + "/test_rdd")
//    val context = Map("catWeights"-> catWeights,"crossBlackList"->crossBlackList,"upAvgStd"->upAvgStd,"trainSampleThreshold"->trainSampleThreshold)

//    import scala.pickling.Defaults._
//    import scala.pickling.json._
//
//    val pkl = context.pickle
//    println(pkl)
  }
}
