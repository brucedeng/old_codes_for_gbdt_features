package com.cmcm.ranking.india.trainUp.dep

import com.cmcm.ranking.india.featureUtils.{cpParser, upParser, utils}
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.LabeledPoint

/**
 * Created by mengchong on 4/20/16.
 */
object upUtils {
  val catMap = utils.getCatTree
  var upThreshold = 3.0
  var normFactor = 3.0

  def parseUpCatKwNorm(upstr:String) : (List[(String,Double)],Map[String,Double]) = {
    val upMap = new upParser(upstr)
    val l1Cat = upMap.getCategoryPair
      .map( x => (utils.getL1Cat(x._1,catMap),x._2))
      .filter( _._1 != "null" )
      .groupBy(x => x._1)
      .toList
      .map(x => (x._1, x._2.map(_._2).sum))
    val sumClk = l1Cat.map(_._2).sum
    if (sumClk < upThreshold) (List(),Map().withDefaultValue(0.0))
    else (L1_norm(l1Cat,normFactor),L1_norm(upMap.getKeywordsPair,normFactor).toMap.withDefaultValue(0.0))
  }

  def parseCpCatKw(cpstr:String) : (List[(String,Double)],List[(String,Double)]) = {
    val cpMap = new cpParser(cpstr)
    val l1Cat = cpMap.getCategoryPair
      .map( x => (utils.getL1Cat(x._1,catMap),x._2))
      .filter( _._1 != "null" )
      .groupBy(x => x._1)
      .toList
      .map(x => (x._1, x._2.map(_._2).sum))
    (l1Cat,cpMap.getKeywordsPair)
  }

  def parseCpCatKwNorm(cpstr:String) : (List[(String,Double)],List[(String,Double)]) = {
    val cpNorm = parseCpCatKw(cpstr)
    (L1_norm(cpNorm._1,0),cpNorm._2)
  }

  def kwRel (kw1:Iterable[(String,Double)],kw2:Map[String,Double]): Double = {
    kw1.map(x => x._2 * kw2(x._1)).sum
  }

  def parseEvent(line:String) = {
    val fields : Array[String] = line.split('\t')
    val label = fields(3).toDouble
    val (upCat,upKw) = parseUpCatKwNorm(fields(5))
    val (cpCat,cpKw) = parseCpCatKwNorm(fields(6))
    val gmp = fields(7).toDouble
    (label,if(gmp<0) 0.0; else gmp,upCat,cpCat,kwRel(cpKw,upKw))
  }

  def parseEventIntercept(line:String) = {
    val fields : Array[String] = line.split('\t')
    val label = fields(3).toDouble
    val (upCat,upKw) = parseUpCatKwNorm(fields(5))
    val (cpCat,cpKw) = parseCpCatKwNorm(fields(6))
    val gmp = fields(7).toDouble
    (label,if(gmp<0) 0.0; else gmp,upCat,cpCat,kwRel(cpKw,upKw))
  }

  def L1_norm(l : List[(String,Double)],factor:Double = 0): List[(String,Double)] = {
    val sum = l.filter(x => x._2 > 0).map(x => x._2).sum + factor
    if (sum > 0) l.map(x => (x._1,x._2/sum))
    else List[(String,Double)]()
  }

  def genFeature(label:Double,gmp:Double, up:List[(String,Double)], cp:List[(String,Double)],kwrel:Double,catMap:Map[String,Int] ): LabeledPoint = {
    val length = catMap.size
    val shift = 2 + length
    val features:List[(Int,Double)] = for (u <- up; c <- cp) yield{
      val (ucat,uweight) = u
      val (ccat,cweight) = c
      val uCatId = catMap.get(ucat)
      val cCatId = catMap.get(ccat)
      if (uCatId.isDefined && cCatId.isDefined) (cCatId.get*length + uCatId.get+ shift,uweight*cweight)
      else (-1,0.0)
    }
    val gmpP : Double = if(gmp < 0) 0.0 else gmp
    val intercepts = cp.filter(x => catMap contains x._1).map(x=> (catMap(x._1)+ shift - length,x._2))
    val feature_filter = features.filter(x=>x._1 >= 0) ++ List((0,gmpP),(1,kwrel)) ++ intercepts
    new LabeledPoint(label,Vectors.sparse(length*length+shift,feature_filter))
  }

  def genFeatureWeight(weights:Map[String,(Double,Double)])(label:Double,gmp:Double, up:List[(String,Double)], cp:List[(String,Double)],kwrel:Double,catMap:Map[String,Int] ) = {
    val length = catMap.size
    val shift = 0
    val features:List[(Int,Double)] = for (u <- up; c <- cp) yield{
      val (ucat,uweight) = u
      val (ccat,cweight) = c
      val uCatId = catMap.get(ucat)
      val cCatId = catMap.get(ccat)
      if (uCatId.isDefined && cCatId.isDefined) (cCatId.get*length + uCatId.get+ shift,uweight*cweight)
      else (-1,0.0)
    }
    val gmpP : Double = if(gmp < 0) 0.0 else gmp
    val feature_filter = features.filter(x=>x._1 >= 0) // ++ List((0,gmpP),(1,kwrel))
    val weightVecList = cp.map(x=> {
      val w = weights(x._1)
      (w._1 * x._2, w._2 *x._2)
    })
    val weightVec = weightVecList match {
      case (a,b)::tail => weightVecList.reduce((a,b) => (a._1 + b._1, a._2 + b._2))
      case Nil => weights("Nonexist")
    }
    val wt = if(label > 0) weightVec._1 else weightVec._2
//    Instance(label,weight,Vectors.sparse(length*length+shift,feature_filter))
//    for (x <- 0 to weight.toInt) yield new LabeledPoint(label,Vectors.sparse(length*length+shift,feature_filter))
    (label,wt,Vectors.sparse(length*length+shift,feature_filter),up,cp)
    new LabeledPoint(label,Vectors.sparse(length*length+shift,feature_filter))
  }

}

//class SetVectorizer(override val uid: String)
//  extends Transformer {
//
//  def transformSchema(schema: StructType, params: ParamMap) = {
//    val outc = paramMap.get(outputCol).getOrElse("features")
//    StructType(schema.fields ++ Seq(StructField(outc, VT, true)))
//  }
//
//  def transform(df: DataFrame ) = {
//    val (inCol, outCol, maxSize) = pvals(paramMap)
//    df.withColumn(outCol, callUDF({ a: Seq[Int] =>
//      Vectors.sparse(maxSize, a.toArray, Array.fill(a.size)(1.0))
//    }, VT, df(inCol)))
//  }
//}
