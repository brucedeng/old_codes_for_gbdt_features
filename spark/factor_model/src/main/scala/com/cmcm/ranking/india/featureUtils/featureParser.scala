package com.cmcm.ranking.india.featureUtils

import scala.util.parsing.json.JSON

/**
 * Created by mengchong on 4/9/16.
 */
class featureParser(val line:String) {
  val jsonObj = parseUPJSON(line)

  def parseUPJSON (line: String): Map[String,Any] = {
    try {
      val jsonobj = JSON.parseFull(line)
      val catArray = jsonobj.get.asInstanceOf[Map[String, Any]]
      catArray
    }catch {
      case e:Exception =>
        Map[String,Any]()
    }
  }

  def getStrVal (key:String) : String =
    if (jsonObj.contains(key)) jsonObj(key).asInstanceOf[String]
    else ""

  def getDoubleVal (key:String) : Double =
    if (jsonObj.contains(key)) jsonObj(key).asInstanceOf[Double]
    else 0.0

  def getLongVal (key:String) : Long =
    if (jsonObj.contains(key)) jsonObj(key).asInstanceOf[Long]
    else 0

  def getAnyVal (key:String) : Any = jsonObj(key)

}
