package com.cmcm.ranking.india.trainUp.flows

import com.typesafe.config.Config

/**
 * Created by mengchong on 5/11/16.
 */
object FlowFactory {
  def flows(name:String,config:Config):Flow = {
    if (name == "parseRawData") new parseRawData(config)
    else if(name == "trainCategoryOnly") new trainCategoryOnly(config)
    else if(name == "trainPhase1") new trainPhase1(config)
    else if(name == "StatisticUpCategory") new StatisticUpCategory(config)
    else if(name == "trainCategoryGmpWeight") new trainCategoryGmpWeight(config)
    else if(name == "dumpWeights") new dumpWeights(config)
    else throw new Error("unrecognized flow type")
  }
}
