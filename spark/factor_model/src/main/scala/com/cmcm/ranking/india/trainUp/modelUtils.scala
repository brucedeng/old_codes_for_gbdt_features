package com.cmcm.ranking.india.trainUp

import java.io.InputStream

import com.cmcm.ranking.india.featureUtils.utils
import org.apache.spark.ml.classification.{BinaryLogisticRegressionSummary, LogisticRegressionModel}
import org.apache.spark.mllib.linalg.{Vectors,Vector}
import org.apache.spark.sql.Row
import java.io._
import org.json4s.JsonAST._
import org.json4s.jackson.JsonMethods._

/**
 * Created by mengchong on 4/24/16.
 */

object modelUtils extends Serializable{
  def getFeatureList: Array[(Int,String)] = {
    val stream : InputStream = getClass.getResourceAsStream("/featmap.txt")
    val lines = scala.io.Source.fromInputStream( stream ).getLines()
    lines.map(x => {
      val fields = x.split('\t')
      val fid = fields(0).toInt
      val fname = fields(1)
      val ftype = fields(2)
      (fid,fname)
    }).toArray
  }

  lazy val featureList = getFeatureList
  lazy val featMapName2Id = featureList.map(x => (x._2,x._1)).toMap
  lazy val featMapId2Name = featureList.toMap
  lazy val allCats = featureList.map(_._2).filter(_.startsWith("b_cat-")).map(_.replace("b_cat-",""))

  def rollupCats(l : Seq[(String,Double)]): Seq[(String,Double)] = {
    val catMap = utils.getCatTree
    l.map( x => (utils.getL1Cat(x._1,catMap),x._2))
      .filter( _._1 != "null" )
      .groupBy(x => x._1)
      .toList
      .map(x => (x._1, x._2.map(_._2).sum))
  }

  def L1_norm(l : Seq[(String,Double)],factor:Double = 0): Seq[(String,Double)] = {
    val sum = l.filter(x => x._2 > 0).map(x => x._2).sum + factor
    if (sum > 0) l.map(x => (x._1,x._2/sum))
    else Seq[(String,Double)]()
  }

  def parseSeqRows(cats:Seq[Row]): Seq[(String,Double)] ={
    def toDouble(value:Any) = {
      if (value == null) 0.0
      else value.asInstanceOf[Double]
    }
    cats match {
      case a:Any =>  a.filter (x => x != null).map (row => (row.getString (0),toDouble(row.get(1))) )
      case null => Nil
    }
  }


  def printModelSummary(model:LogisticRegressionModel) : LogisticRegressionModel = {
    val param = model.coefficients.toArray
    println("Model 2 was fit using parameters: " + model.parent.extractParamMap)
    println("Model 2 coef : " + model.coefficients.toArray.toList)
    println("Model 2 inception " + model.intercept.toString)

    val trainingSummary = model.summary
    val binarySummary = trainingSummary.asInstanceOf[BinaryLogisticRegressionSummary]
    // Obtain the receiver-operating characteristic as a dataframe and areaUnderROC.
    println("train auc:\t" + binarySummary.areaUnderROC)
    model
  }

  def genCross (uCats:Seq[(String,Double)], dCats:Seq[(String,Double)], crossBlackList:Set[String] = Set()) : Seq[(Int,Double)] = {
    (for (u <- uCats; c <- dCats) yield{
      val (ucat,uweight) = u
      val (dcat,dweight) = c
      val catName = ucat + "-" + dcat
      val fname = "c_cats-"+ ucat + "-" + dcat
      if (featMapName2Id.contains(fname) && (! crossBlackList.contains(catName))) (featMapName2Id(fname),uweight*dweight)
      else (-1,0.0)
    }).filter(_._1>=0)
  }

  def genFeatures (gmp:Double, uCats:Seq[(String,Double)], dCats:Seq[(String,Double)],kwRel:Double, crossBlackList:Set[String] = Set())
    : Vector = {
    val crossFeatures:Seq[(Int,Double)] = genCross(uCats,dCats,crossBlackList)
    val statFeatures = Seq((featMapName2Id("gmp"),gmp),(featMapName2Id("kw_rel"),kwRel)).filter(_._2 !=0)
    Vectors.sparse(featureList.length,statFeatures ++ crossFeatures)
  }

  def getKwRel (ukw:Seq[(String,Double)],dKw:Seq[(String,Double)]):Double = {
    val kwMap = dKw.toMap.withDefaultValue(0.0)
    ukw.map(x => x._2 * kwMap(x._1)).sum
  }

  def genFeatureWithCatRel (gmp:Double, uCats:Seq[(String,Double)], dCats:Seq[(String,Double)],kwRel:Double,coef:Array[Double])
  : Vector = {
    val catRel = genCatRel(uCats,dCats,coef)
    val statFeatures = Array((featMapName2Id("gmp"),gmp),(featMapName2Id("kw_rel"),kwRel),(featMapName2Id("cat_rel"),catRel)).filter(_._2 !=0)
    Vectors.sparse(5,statFeatures)
  }

  def genFeatureWithCatRel (gmp:Double, catRel:Double ,kwRel:Double)
  : Vector = {
    val statFeatures = Array((featMapName2Id("gmp"),gmp),(featMapName2Id("kw_rel"),kwRel),(featMapName2Id("cat_rel"),catRel)).filter(_._2 !=0)
    Vectors.sparse(5,statFeatures)
  }

  def genCatRel ( uCats:Seq[(String,Double)], dCats:Seq[(String,Double)], coef:Array[Double]) : Double = {
    (for (u <- uCats; c <- dCats) yield{
      val (ucat,uweight) = u
      val (dcat,dweight) = c
      val fname = "c_cats-" + ucat + "-" + dcat
      if (featMapName2Id contains fname) uweight*dweight*coef(featMapName2Id(fname))
      else 0.0
    }).sum
  }

  def transformCat ( uCats:Seq[(String,Double)], coef:Array[Double]) : Seq[(String,Double)] = {
    allCats.map(targetCat => {
      (targetCat, uCats.map{case (ucat:String,uweight:Double) =>
        val fname = "c_cats-" + ucat + "-" + targetCat
        if (featMapName2Id contains fname) uweight * coef(featMapName2Id(fname))
        else 0.0
      }.sum)
    }).filter(_._2 != 0)
  }

  def printCatCoef (coefs : Array[Double]) = {
    val nameMap = utils.getCatTree.map(x => (x._1,x._1 + "," + x._2._2))
    println("\t" + allCats.mkString("\t"))
    for (l <- allCats) yield {
      val line = (for (c<- allCats) yield {
        val fname = "c_cats-" + l + "-" + c
        coefs(featMapName2Id(fname)).toString
      }).mkString("\t")
      println((if(nameMap contains l) nameMap(l) else l) + "\t" + line)
      line
    }
  }

  def flatCatWeight (catAndLabel:(Seq[(String,Double)],Double)) = {
    val (cats,label) = catAndLabel
    modelUtils.L1_norm(modelUtils.rollupCats(cats)).map{
      case (category,weight) => (category,(weight * label, weight * (if(label > 0) 0.0 else 1.0 ) ))
    }
  }

  def genWeight (cpCats:Seq[(String,Double)], catWeights:Map[String,(Double,Double)], label:Double) = {
    val weightVecList = cpCats.map {
      case (cat, weight) => {
        val w = catWeights(cat)
        (w._1 * weight, w._2 * weight)
      }
    }
    val weightVec = if (weightVecList.isEmpty) (0.0, 0.0)
    else weightVecList.reduce((a, b) => (a._1 + b._1, a._2 + b._2))
    if (label > 0) weightVec._1 else weightVec._2
  }

  def normalize(data:Seq[(String,Double)],stat:Map[String,Statistic]) = {
    data.map{case (name,weight) =>
      if (stat contains name){
        val curStat = stat(name)
        val std2 = if(curStat.std > 0) curStat.std else 0.0
        (name,(weight-curStat.mean)/std2)
      }
      else
        (name,weight)
    }
  }

  def genCoefList (coefs : Array[Double]) : Array[(Int,String,Double)] = {
    coefs.zipWithIndex.map{case (w,idx) =>
      (idx,featMapId2Name(idx),w)
    }
  }

  def parseCoefList (feature:Array[(Int,String,Double)]) : Array[Double] = {
    Vectors.sparse(featureList.length,feature.map{case (fid,fname,wt) => (fid,wt) }).toArray
  }

  def parseCoefListWithTruncate (feature:Array[(Int,String,Double)],size:Int) : Array[Double] = {
    if(size <= 0) parseCoefList(feature)
    else {
      val shortParam = feature
        .filter(x => x._2.startsWith("c_cats-"))
        .groupBy(x => x._2.split('-')(2))
        .flatMap { case (resCat, features) => {
          val featureSorted = features.filter(x => x._3 != 0).sortBy { case (fid, fname, wt) => if (fname.endsWith(resCat + "-" + resCat)) -1000000 else -wt }
//          println(featureSorted.toList)
          featureSorted.take(size).filter(_._3 != 0).map(_._1)
        }
        }
        .toSet

      Vectors.sparse(featureList.length, feature.map { case (fid, fname, wt) =>
        if (fname.startsWith("c_cats-")) {
          if (shortParam.contains(fid)) (fid, wt)
          else (-1, -1.0)
        }
        else (fid, wt)
      }.filter(x => x._1 >= 0)).toArray
    }
  }

  def formatJsonCoefList (feature:Array[Double]) = {
    val objList = feature.zipWithIndex.map(x => {
      val (weight,idx) = x
      featMapId2Name.get(idx) match {
        case Some(fname) => if (fname.startsWith("c_cats-")) {
          val cats = fname.replace("c_cats-", "").split('-')
          val dcat = cats(1); val ucat = cats(0)
          (ucat,dcat,weight)
          }
          else ("","",0.0)
        case None => ("","",0.0)
      }
    })
    .filter(x => x._1 != "" && x._2!="" && ( x._3>0.001 || x._3< -0.001))
    .groupBy(x => x._1)
    .map{case (srcCats,targetCats) => (srcCats,JObject(targetCats
      .map { case (uCat, dCat, weight) => (dCat, JDouble(weight)) }.toList)
      )}
    .toList
    compact(render(JObject(objList)))
  }

  private def trapezoid(points: Seq[(Double, Double)]): Double = {
    require(points.length == 2)
    val x = points.head
    val y = points.last
    (y._1 - x._1) * (y._2 + x._2) / 2.0
  }

  /**
   *
   * @param data array of tuple, tuple schema is prob, label, weight
   * @return auc
   */
  def evalAuRoc (data: Array[PredPoint], cutoff:Double ) = {
    if (data.length < cutoff) -1.0
    else {
      val totalPosCnt = data.filter(x => x.label > 0).map(_.weight).sum
      val totalNegCnt = data.map(x => x.weight).sum - totalPosCnt
      println((totalPosCnt,totalNegCnt))
      if (totalPosCnt > 0 && totalNegCnt > 0) {
        val res = data.groupBy(x => x.score)
        .map { case (prob, aggList) =>
          val curPosCnt = aggList.filter(x => x.label > 0).map(_.weight).sum
          val curNegCnt = aggList.map(_.weight).sum - curPosCnt
          (prob, (curPosCnt,curNegCnt))
        }
        .toSeq
        .sortBy(-_._1)
        .map(x => x._2)
        .foldLeft((0.0,0.0,0.0))((accu, value) => {
          val (pCnt, nCnt, auc) = accu
          val (curPCnt, curNCnt) = value
          val newPCnt = pCnt+curPCnt
          val newNCnt = nCnt+curNCnt
          val newAuc = auc + (newNCnt-nCnt)/totalNegCnt*(newPCnt+pCnt)/2/totalPosCnt
//          println((newPCnt,newNCnt,newPCnt/totalPosCnt, newNCnt/totalNegCnt, newAuc))
          (newPCnt, newNCnt, newAuc)
        })
        res._3
      }
      else -1.0
    }
  }

  def evalAccuWeight(data: Array[PredPoint]) = {
    data.sortBy(-_.score)
    .zipWithIndex
    .map{case (point,index) => point.weight /(index + 1)}
    .sum
  }

  def printPhase1Coef (coef: Array[Double]) = {
    println("gmp\t" + coef(featMapName2Id("gmp")))
    println("kw_rel\t" + coef(featMapName2Id("kw_rel")))
    println("cat_rel\t" + coef(featMapName2Id("cat_rel")))
  }
}
