package com.cmcm.ranking.india.runtime

import org.apache.spark.mllib.classification.LogisticRegressionWithLBFGS
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.h2o._
import org.apache.spark.examples.h2o._
import java.io.File

/**
 * Created by mengchong on 4/17/16.
 */
object trainLibSVMWater {

  def main(args: Array[String]) {
    val dataPath = args(0)
    val outputPath = args(1)
    val outputDataPath = args(2)
    println("Loading data: %s".format(dataPath))

    val conf = new SparkConf().setAppName("train up")
    val sc = new SparkContext(conf)
    val h2oContext = H2OContext.getOrCreate(sc)

    val trainData = MLUtils.loadLibSVMFile(sc, dataPath)

//    import h2oContext._
//    // Implicit call of H2OContext.asH2OFrame[Weather](rdd ) is used
//    val hf: H2OFrame = trainData
//    // Explicit call of of H2OContext API with name for resulting H2OFrame
//    val hfNamed: H2OFrame = h2oContext.asH2OFrame(trainData, "hfNamed")
  }

//    import _root_.hex.glm
//    glm
//    import _root_.hex.glm.GLM
//    import _root_.hex.glm.GLMModel.GLMParameters
//    val glmParams = new GLMParameters(Family.binomial)
//    glmParams._train = bigTable
//    glmParams._response_column = 'IsDepDelayed
//    glmParams._alpha = Array[Double](0.5)
//    val glm = new GLM(glmParams, Key.make("glmModel.hex"))
//    val glmModel = glm.trainModel().get()
//
//    // Use model to estimate delay on training data
//    val predGLMH2OFrame = glmModel.score(bigTable)('predict)
//    val predGLMFromModel = asRDD[DoubleHolder](predictionH2OFrame).collect.map(_.result.getOrElse(Double.NaN))
//
//
//    val splits = trainData.randomSplit(Array(0.6, 0.4), seed = 11L)
//    val training = splits(0).cache()
//    //    MLUtils.saveAsLibSVMFile(training,outputDataPath + "/train")
//    val test = splits(1)
//    //    MLUtils.saveAsLibSVMFile(test,outputDataPath + "/test")
//
//    val model = new LogisticRegressionWithLBFGS()
//      .setNumClasses(2)
//      .run(training)
//
//    model.clearThreshold()
//
//    val trainPredAndLabels = training.map { case LabeledPoint(label, features) =>
//      val prediction = model.predict(features)
//      (prediction, label)
//    }
//    println("saving to " + outputDataPath + "/train_eval")
//    trainPredAndLabels.saveAsTextFile(outputDataPath + "/train_eval")
//
//    val predictionAndLabels = test.map { case LabeledPoint(label, features) =>
//      val prediction = model.predict(features)
//      (prediction, label)
//    }
//    println("saving to " + outputDataPath + "/test_eval")
//    predictionAndLabels.saveAsTextFile(outputDataPath + "/test_eval")
//
//    println("saving model: %s".format(outputPath))
//    model.save(sc, outputPath)
//
//    val metricstrain = new BinaryClassificationMetrics(trainPredAndLabels)
//    val auROCtrain = metricstrain.areaUnderROC()
//    val metrics = new BinaryClassificationMetrics(predictionAndLabels)
//    val auROC = metrics.areaUnderROC()
//    println("train auc: " + auROCtrain)
//    println("test auc: " + auROC)
//
//    //    val result =
//    println(model.weights.toArray.toList)
//  }
}
