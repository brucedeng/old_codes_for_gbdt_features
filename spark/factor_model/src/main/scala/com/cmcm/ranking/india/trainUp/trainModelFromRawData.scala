package com.cmcm.ranking.india.trainUp

import java.io.File

import com.cmcm.ranking.india.featureUtils.utils
import com.typesafe.config.ConfigFactory
import org.apache.spark.ml.classification.LogisticRegression
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.sql.{Row, DataFrame}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by mengchong on 4/26/16.
 */
object trainModelFromRawData {
  val catMap = utils.getCatTree

  def parseRows (x:(Seq[(String,Double)],Double)) = {
    modelUtils.L1_norm(modelUtils.rollupCats(x._1)).map(y => (y._1, (y._2 * x._2, y._2 * (if (x._2 > 0) 0.0 else 1.0))))
  }

  def crossCats (x:(Seq[(String,Double)],Seq[(String,Double)])) = {
    for(a <- x._1; b <- x._2) yield {
      (a + "-" + b,1)
    }
  }

  def main(args: Array[String]) {
    val desc = "train up with 1. only use category cross, weighted to 1:1 \n" +
      "2. use the weighted cat_rel train phase-1 features\n" +
      "3. filtered only events with cp category not empty\n" +
      "4. empty gmp is -1 in training data. must be set to 0 before use\n" +
      "5. added up filter. remove user profile less than upThreshold clicks\n" +
      "6. filter out cubes with less than 100 samples\n" +
      "7. normalize non zero up before usage\n"

    println(desc)
    val configPath = args(0)

    val config = ConfigFactory.parseFile(new File(configPath))

    val normFactor = config.getDouble("norm_factor")
    val upThreshold = config.getDouble("up_threshold")
    val trainSampleThreshold = config.getDouble("train_sample_threshold")
    val elasticNetParam = config.getDouble("elastic_net_param")
    val regParam = config.getDouble("reg_param")
    val dataPath = config.getString("data")
    val outputPath = config.getString("output")
    val appName = config.getString("app_name")
    val modelDir = config.getString("model_dir")

    println("Loading data: %s".format(dataPath))

    // initializing context
    val conf = new SparkConf().setAppName(appName)
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._

    sc.broadcast(modelUtils)
    sc.broadcast(trainModelFromRawData)

    // load input data
    val inputData = sqlContext.read.parquet(dataPath)
      .select("uid","content_id","label","dwelltime","u_cats","u_keywords","d_cats","d_keywords","gmp")
    //      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    inputData.printSchema()
    //    inputData.limit(10).collect().foreach(println(_))

    // parse dataframe function
    def parseInputDataFrame (data:DataFrame) = {
      data.select("label","u_cats","d_cats","u_keywords","d_keywords","gmp","uid","content_id")
        .map(row => {
          //        case Row(label:Int,u_cats, d_cats,u_kws,d_kws,gmp:Double) =>
          val label = row.getInt(0)
          val gmpRaw = row.getFloat(5).toDouble
          val gmp = if(gmpRaw < 0) 0.0 else gmpRaw
          val upCatsRaw =modelUtils.rollupCats(modelUtils.parseSeqRows(row.getSeq[Row](1)))
          val upCatsClk = upCatsRaw.map(_._2).sum
          val upCats:Seq[(String,Double)] = if (upCatsClk < upThreshold) List() else  modelUtils.L1_norm(upCatsRaw,normFactor)
          val cpCats = modelUtils.L1_norm(modelUtils.rollupCats(modelUtils.parseSeqRows(row.getSeq[Row](2))))
          val upKws = modelUtils.L1_norm(modelUtils.parseSeqRows(row.getSeq[Row](3)))
          val cpKws = modelUtils.L1_norm(modelUtils.parseSeqRows(row.getSeq[Row](4)))
          val kwRel = modelUtils.getKwRel(upKws, cpKws)
          val uid = row.getString(6)
          val content_id = row.getString(7)
          (label.toDouble, gmp, kwRel, upCats, cpCats, uid, content_id)
        })
    }

    val trainData = parseInputDataFrame(inputData)
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    val crossBlackList = trainData.map(x => (x._4,x._5))
      .flatMap(crossCats)
      .reduceByKey((a,b) => a+b)
      .filter(_._2 < 100)
      .collect()
      .map(_._1)
      .toSet

    println("black list is : " + crossBlackList)
    sc.broadcast(crossBlackList)

    // get label and cp category
    val cCatMapRaw = trainData.map(x => (x._5,x._1))
      .filter(_._1.nonEmpty)
      .flatMap(parseRows)
      .reduceByKey((a,b) => (a._1 + b._1, a._2 + b._2))
      .collect()

    println("sample category raw " + cCatMapRaw)

    val catWeights = cCatMapRaw
      .map(x => (x._1,(x._2._2/x._2._1,1.0)))
      .toMap
      .withDefaultValue((0.0,0.0))
    sc.broadcast(catWeights)
    println("weights:")
    println(catWeights)

    //      .filter(x => x._2.numNonzeros != 0 )
    //      .toDF("label","features")

    val splits = trainData.randomSplit(Array(0.8, 0.2), seed = 11L)
    //    trainData.saveAsTextFile(outputPath + "/raw_train_data")

    // category training data
    val trainingRaw = splits(0)
      .map{
        case (label:Double,gmp:Double,kwRel:Double,upCats,cpCats,uid:String, content_id:String) => {
          val weightVecList = cpCats.map(x => {
            val w = catWeights(x._1)
            (w._1 * x._2, w._2 * x._2)
          })
          val weightVec = if(weightVecList.isEmpty) (0.0,0.0)
          else weightVecList.reduce((a, b) => (a._1 + b._1, a._2 + b._2))
          val wt = if (label > 0) weightVec._1 else weightVec._2
          (label, modelUtils.genFeatures(0.0, upCats, cpCats, 0.0,crossBlackList), wt)
        }
      }

    trainingRaw.saveAsTextFile(outputPath + "/training_data")
    val training = trainingRaw.filter(x => x._2.numNonzeros != 0 )
      .toDF("label","features","weight")
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    // category testing data
    val test = splits(1)
      .map{
        case (label:Double,gmp:Double,kwRel:Double,upCats,cpCats,uid:String, content_id:String) =>
          (label,modelUtils.genFeatures(0.0,upCats,cpCats,0.0),1.0)
      }
      .filter(x => x._2.numNonzeros != 0 )
      .toDF("label","features","weight")
      .persist(StorageLevel.MEMORY_AND_DISK_SER)
    //    MLUtils.saveAsLibSVMFile(test,outputDataPath + "/test")

    // Create a LogisticRegression instance.  This instance is an Estimator.
    val lr = new LogisticRegression()
    // Print out the parameters, documentation, and any default values.
    println("LogisticRegression parameters:\n" + lr.explainParams() + "\n")

    // We may set parameters using setter methods.
    lr.setMaxIter(1000)
      .setRegParam(regParam)
      .setElasticNetParam(elasticNetParam)
      .setFitIntercept(true)
      .setWeightCol("weight")

    // We may alternatively specify parameters using a ParamMap,
    // which supports several methods for specifying parameters.
    val paramMap = ParamMap(lr.maxIter -> 20)
          .put(lr.maxIter, 1000) // Specify 1 Param.  This overwrites the original maxIter.
          .put(lr.regParam -> 0.1) // Specify multiple Params.
          .put(lr.probabilityCol -> "probability")

    // Now learn a new model using the paramMapCombined parameters.
    // paramMapCombined overrides all parameters set earlier via lr.set* methods.
    val model = lr.fit(training, paramMap)
    //    training.unpersist()

    println("saving model to " + outputPath + "/cat_model")
    model.save(modelDir)

    training.unpersist()
    // print coefficients
    val param = model.coefficients.toArray
    println("paramater size is : " + param.length.toString)
    println("paramaters:")
    modelUtils.printCatCoef(param)
    println("model summary:")
    modelUtils.printModelSummary(model)


    // Make predictions on test data using the Transformer.transform() method.
    // LogisticRegression.transform will only use the 'features' column.
    // Note that model.transform() outputs a 'probability' column instead of the usual
    // 'probability' column since we renamed the lr.probabilityCol parameter previously.
    val predictionAndLabels = model.transform(test)
      .select("features", "label", "probability", "prediction")
      .map { case Row(features: Vector, label: Double, prob: Vector, prediction: Double) =>
        (prob(1), label)
      }
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    test.unpersist()

    // calculate test auc
    predictionAndLabels.saveAsTextFile(outputPath + "/cat_test")
    val metrics = new BinaryClassificationMetrics(predictionAndLabels,1000)
    val auROC = metrics.areaUnderROC()
    println("test auc:\t" + auROC)
    predictionAndLabels.unpersist()

    val overallWtRaw = trainData.map(x => (x._1,if(x._1 > 0) 0.0 else 1.0)).reduce((a,b) => (a._1 + b._1,a._2 + b._2))
    val overallWt = (overallWtRaw._2/overallWtRaw._1,1.0)
    println("overall weight is :\t" + overallWt)
    val libsvmData = splits(0)
      .map({
        case (label:Double,gmp:Double,kwRel:Double,upCats,cpCats,uid:String, content_id:String) =>
          //          val wtOverall = if(label > 0) overallWt._1 else overallWt._2
          LabeledPoint(label,modelUtils.genFeatureWithCatRel(gmp,upCats,cpCats,kwRel,param))
      })
    MLUtils.saveAsLibSVMFile(libsvmData, outputPath + "/phase1_libsvm")

    val gmpTraining = splits(0)
      .map{
        case (label:Double,gmp:Double,kwRel:Double,upCats,cpCats,uid:String, content_id:String) =>
          val wtOverall = if(label > 0) overallWt._1 else overallWt._2
          (label,modelUtils.genFeatureWithCatRel(gmp,upCats,cpCats,kwRel,param),wtOverall)
      }
      .filter(x => x._2.numNonzeros != 0 )
      .toDF("label","features","weight")
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    // category testing data
    val gmpTest = splits(1)
      .map{
        case (label:Double,gmp:Double,kwRel:Double,upCats,cpCats,uid:String, content_id:String) =>

          (label,modelUtils.genFeatureWithCatRel(gmp,upCats,cpCats,kwRel,param),1.0)
      }
      .filter(x => x._2.numNonzeros != 0 )
      .toDF("label","features","weight")
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    val model2 = lr.fit(gmpTraining, paramMap)

    println("saving model to " + outputPath + "/overall_model")
    model2.save(outputPath + "/overall_model")
    modelUtils.printModelSummary(model2)

    // re calculate overall train labels
    val overallTrainAndLabels = model2.transform(gmpTraining)
      .select("features", "label", "probability", "prediction")
      .map { case Row(features: Vector, label: Double, prob: Vector, prediction: Double) =>
        (prob(1), label)
      }
    val gmpTrainMetrics = new BinaryClassificationMetrics(overallTrainAndLabels,1000)
    val gmpTrainauROC = gmpTrainMetrics.areaUnderROC()
    println("overall train auc:\t" + gmpTrainauROC)

    val overallPredictionAndLabels = model2.transform(gmpTest)
      .select("features", "label", "probability", "prediction")
      .map { case Row(features: Vector, label: Double, prob: Vector, prediction: Double) =>
        (prob(1), label)
      }
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    // calculate test auc
    overallPredictionAndLabels.saveAsTextFile(outputPath + "/overall_test")
    val gmpMetrics = new BinaryClassificationMetrics(overallPredictionAndLabels,1000)
    val gmpauROC = gmpMetrics.areaUnderROC()
    println("overall test auc:\t" + gmpauROC)
    overallPredictionAndLabels.unpersist()
    println("elasticNetParam:\t" + elasticNetParam)
    println("regParam:\t" + regParam)
    println("trainSampleThreshold:\t" + trainSampleThreshold)
  }
}
