package com.cmcm.ranking.india.trainUp.flows

import com.cmcm.ranking.india.trainUp.{ioUtils, rddUtils, event, modelUtils}
import com.typesafe.config.Config
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Row, DataFrame}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by mengchong on 5/13/16.
 */
class StatisticUpCategory (config:Config) extends Flow with Serializable{
  val inputUPPath = config.getString("raw_up_path")
  val isTransform = config.getString("use_transform")
  val localModelFile = config.getString("local_model_file")
  val catModelTruncate = config.getInt("category_weight_truncate")
  val appName = config.getString("app_name")
  val l1Norm = config.getString("l1_norm")
  val rollUpCats = config.getString("roll_up_cats")
  val upThreshold = config.getDouble("up_threshold")
  val normFactor = config.getDouble("norm_factor")

  override def execute() = {

    val normFunc:(Seq[(String,Double)],Double)=> Seq[(String,Double)] =
      if(l1Norm.toLowerCase == "true"){
        println("Using L1 normalize on categories and keywords")
        modelUtils.L1_norm
      }
      else (a,b) => a
    val rollupFunc:(Seq[(String,Double)])=> Seq[(String,Double)] =
      if(rollUpCats.toLowerCase == "true" ) {
        println("Using rollup on categories")
        modelUtils.rollupCats
      }
      else x=>x


    val param = modelUtils.parseCoefListWithTruncate(ioUtils.loadCoefFile(localModelFile), catModelTruncate)

    val conf = new SparkConf().setAppName(appName)
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._

    sc.broadcast(modelUtils)
    sc.broadcast(normFunc)
    sc.broadcast(rollupFunc)

    sc.broadcast(this)

    val inputData = sqlContext.read.parquet(inputUPPath)
      .select("aid","categories")
      .map(row  => {
        val upCatsRaw = modelUtils.parseSeqRows(row.getSeq[Row](1))
        val upCatsClk = upCatsRaw.map(_._2).sum
        val upCats:Seq[(String,Double)] = if (upCatsClk < upThreshold) Seq() else normFunc(upCatsRaw,normFactor)
        upCats
      })

    val transformUp = inputData.map(upCats =>
        if(isTransform.toLowerCase == "true") modelUtils.transformCat(upCats,param) else upCats
      )
      .map(upCats => modelUtils.L1_norm( upCats.filter(_._2 > 0),0))
      .filter(upCats => upCats.nonEmpty)
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    val userUpSize = transformUp.map(x => (1.0,x.length)).reduce((a,b)=> (a._1+b._1,a._2+b._2))
    println("total users:\t" + userUpSize._1)
    println("average category count:\t" + userUpSize._2/userUpSize._1)

    val catAccu = transformUp.flatMap(x => x).reduceByKey((w1,w2) => w1+w2).collect()
      .sortBy(_._1)
      .foreach(x => println(x._1 + "\t" + x._2))

  }
}
