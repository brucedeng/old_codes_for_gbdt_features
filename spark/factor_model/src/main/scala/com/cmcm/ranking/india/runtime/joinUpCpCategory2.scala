package com.cmcm.ranking.india.runtime

import com.cmcm.ranking.india.featureUtils.{featureParser, cpParser, upParser}
import org.apache.spark.{SparkContext, SparkConf}
import java.io._

/**
 * Created by mengchong on 4/11/16.
 */
object joinUpCpCategory2 {

  class upData(d:upParser) extends Serializable {
    val uid = d.getStrVal("uid")
    val categories = d.getCategoryPair
    override def toString = {
      uid + "\t" + categories.toString()
    }
  }

  class cpData(d:cpParser) extends Serializable  {
    val content_id = d.getStrVal("item_id")
    val update_time = d.getDoubleVal("update_time")
    val categories = d.getCategoryPair
    override def toString = content_id + "\t" + update_time.toString + "\t" + categories.toString()
  }

//  class eventData (ctype:String,act:String, level1:String, level1_type:String,dwelltime:Double, view:Double,click:Double) {}

  class eventData(d:featureParser) extends Serializable  {
    val ctype = d.getStrVal("ctype")
    val act = d.getStrVal("act")
    val pid = d.getStrVal("pid")
    val uid = d.getStrVal("aid")
    val content_id = d.getStrVal("contentid")
    val language = d.getStrVal("app_lan")
//    def scen = d.jsonObj.get("scenario")
    val scenario = if (d.jsonObj contains "scenario") d.jsonObj("scenario").asInstanceOf[Map[String,String]]
      else Map[String,String]()
    val level1 = if(scenario contains "level1") scenario("level1") else ""
    val level1_type = if(scenario contains "level1_type") scenario("level1_type") else ""
    var dwelltime:Double =
      if(act == "4" && d.jsonObj.contains("ext")){
        val ext = d.getAnyVal("ext").asInstanceOf[Map[String,String]]
        if(ext contains "dwelltime"){
          val tmpdwell = ext("dwelltime").toDouble
          if (tmpdwell > 600.0) 600.0
          else tmpdwell
        }
        else 0.0
      }
      else 0.0
    var view:Double = {if(act=="1") 1.0 else 0}
    var click:Double = {if(act=="2") 1 else 0}
    override def toString = (ctype,act,pid,uid,content_id,language,level1,level1_type,dwelltime,view,click).toString()
  }

  def addEvent(a:eventData,b:eventData):eventData = {
    a.dwelltime = a.dwelltime + b.dwelltime
    a.dwelltime = if (a.dwelltime > 600 ) 600.0 else a.dwelltime
    a.click = max(a.click,b.click)
    a.view = max(max(a.view,b.view),a.click)
    a
  }

  def max(a:Double,b:Double):Double = if(a>b) a else b
  def getMapElem (s:Map[String,Any],key:String):Map[String,Any] =
    s(key).asInstanceOf[Map[String,Any]]

  def getStrElem (s:Map[String,Any],key:String):String =
    s(key).asInstanceOf[String]

  def main(args: Array[String]){
    val upPath = args(0)
    val cpPath = args(1)
    val eventPath = args(2)
    val lan = args(3)
    val outputPath = args(4)
    println("Loading data. \nUP: %s,\nCP: %s,\nevent: %S".format(upPath,cpPath,eventPath))
    val conf = new SparkConf().setAppName("join categories")
    val sc = new SparkContext(conf)
//    val upData = sc.textFile(upPath)
//    val upFormat = upData.map( x => new upData(new upParser(x)) ).map( x => (x.uid,x))

    // load cp data
    val cpData = sc.textFile(cpPath)
    val cpDistinct = cpData.map( x => new cpData(new cpParser(x)) )
      .map(x => (x.content_id,x))
      .reduceByKey((a,b) => if (a.update_time>b.update_time) a else b,400)

    // load events
    val eventData = sc.textFile(eventPath)
    val eventDistinct = eventData
      .map(s => new eventData(new featureParser(s)))
      .filter(x => x.pid == "11" && (x.ctype=="1" || x.ctype=="0x01") &&
        Set("1","2","3","4").contains(x.act) && x.level1 == "1" && x.level1_type == "1" && x.language == lan)
      .map(x => ((x.uid,x.content_id),x))
      .reduceByKey((a, b) => addEvent(a, b), 400)
      .map( x => (x._2.content_id,x._2))

//    val eventWithCp = eventDistinct.join(cpDistinct,400)
//      .map(x => (x._2._1.uid ,x._2))

//    val eventJoinAll = eventWithCp.leftOuterJoin(upFormat,400)
//      .map(x => (x._2._1._1,x._2._1._2,x._2._2) )
//      .cache()

//    val up_dist = eventJoinAll
//      .filter(x => x._3.isDefined)
//      .flatMap(x => {
//        val view = x._1.view
//        val click = x._1.click
//        val dwell = x._1.dwelltime
//        x._3.get.categories.map(y => (y._1,(y._2*view, y._2 * click, y._2 * dwell)))})
//      .reduceByKey( (a,b) => (a._1 + b._1,a._2 + b._2, a._3 + b._3),400 )
//      .map( x => (x._1, x._2._1,x._2._2,x._2._3 ))
//      .saveAsTextFile(outputPath + "/up_stat")
//
//    println("saved up stat to %s".format(outputPath+"/up_stat"))
//
//    val cp_dist = eventJoinAll
//      .flatMap(x => {
//        val view = x._1.view
//        val click = x._1.click
//        val dwell = x._1.dwelltime
//        x._2.categories.map(y => (y._1,(y._2*view, y._2 * click, y._2 * dwell)))})
//      .reduceByKey( (a,b) => (a._1 + b._1,a._2 + b._2, a._3 + b._3) ,400)
//      .map( x => (x._1, x._2._1,x._2._2,x._2._3 ))
//      .saveAsTextFile(outputPath + "/cp_stat")

    val eventWithCp = eventDistinct.join(cpDistinct,400)
      .map(x => x._2)

    val cp_dist = eventWithCp
      .flatMap(x => {
        val view = x._1.view
        val click = x._1.click
        val dwell = x._1.dwelltime
        x._2.categories.map(y => (y._1,(y._2*view, y._2 * click, y._2 * dwell)))})
      .reduceByKey( (a,b) => (a._1 + b._1,a._2 + b._2, a._3 + b._3) ,400)
      .map( x => (x._1, x._2._1,x._2._2,x._2._3 ))
      .saveAsTextFile(outputPath + "/cp_stat")
    println("saved cp stat to %s".format(outputPath+"/cp_stat"))


    //    logFormat.collect().foreach(println)
  }
}
