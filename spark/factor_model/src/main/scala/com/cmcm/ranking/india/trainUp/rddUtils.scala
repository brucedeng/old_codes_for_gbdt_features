package com.cmcm.ranking.india.trainUp

import com.google.gson.Gson
import org.apache.spark.ml.classification.LogisticRegressionModel
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.linalg.Vector

import scala.math.sqrt
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Row, DataFrame}
import org.apache.spark.storage.StorageLevel

/**
 * Created by mengchong on 4/27/16.
 */
object rddUtils {
  def parseInputDataFrameNorm(normFunc:(Seq[(String,Double)],Double)=> Seq[(String,Double)], rollupFunc:(Seq[(String,Double)])=> Seq[(String,Double)] )(data:DataFrame, upThreshold:Double, normFactor:Double) = {
    data.select("label","u_cats","d_cats","u_keywords","d_keywords","gmp","uid","content_id","dwelltime")
      .map(row => {
        //        case Row(label:Int,u_cats, d_cats,u_kws,d_kws,gmp:Double) =>
        val label1 = row.getInt(0)
        val gmpRaw = row.getFloat(5).toDouble
        val gmp1 = if(gmpRaw < 0) 0.0 else gmpRaw
        val upCatsRaw = rollupFunc(modelUtils.parseSeqRows(row.getSeq[Row](1)))
        val upCatsClk = upCatsRaw.map(_._2).sum
        val upCats1:Seq[(String,Double)] = if (upCatsClk < upThreshold) Seq() else normFunc(upCatsRaw,normFactor)
        val cpCats1 = normFunc(rollupFunc(modelUtils.parseSeqRows(row.getSeq[Row](2))),0)
        val upKws = normFunc(modelUtils.parseSeqRows(row.getSeq[Row](3)),0)
        val cpKws = normFunc(modelUtils.parseSeqRows(row.getSeq[Row](4)),0)
        val kwRel1 = modelUtils.getKwRel(upKws, cpKws)
        val uid1 = row.getString(6)
        val content_id1 = row.getString(7)
        val dwelltime = row.getLong(8).toDouble
        new event(label1,gmp1,if(kwRel1.isNaN || kwRel1.isInfinite) 0.0 else kwRel1,upCats1,cpCats1,uid1,content_id1,dwelltime)
      })
  }

  def genCrossBlackList (trainData:RDD[event],sampleThreshold:Double) = {
    trainData.flatMap(x => {
      for (a <- x.upCats; b <- x.cpCats) yield {
        (a._1 + "-" + b._1, 1)
      }
    })
    .reduceByKey((a, b) => a + b)
    .filter(_._2 < sampleThreshold)
    .map(_._1)
    .collect()
    .toSet
  }

  def genCatWeight (trainData:RDD[event]) = {
    trainData.map(x => (x.cpCats, x.label))
      .filter(_._1.nonEmpty)
      .flatMap(modelUtils.flatCatWeight)
      .reduceByKey((a, b) => (a._1 + b._1, a._2 + b._2))
      .collect()
      .map(x => (x._1, (x._2._2 / x._2._1, 1.0)))
      .toMap
      .withDefaultValue((0.0, 0.0))
  }

  def genUserCatAvg (trainData:RDD[event]) = {
    val localData = trainData.flatMap(x => x.upCats)
      .filter( x => x._2 > 0)
      .map{case (category,weight) => (category,(weight,weight*weight,1.0))}
      .reduceByKey{case ((w1,w1_square,cnt1),(w2,w2_square,cnt2)) => (w1+w2,w1_square+w2_square,cnt1 + cnt2)}
      .collect()
    println(localData.toSeq.toList)
    localData
      .map{case (category, (w,w_square,cnt)) =>
        val cntnorm = if(cnt <=0) 1 else cnt
        (category,new Statistic(w/cntnorm, sqrt((w_square - w*w/cntnorm)/cntnorm)))
      }
      .toMap
  }

  case class dataStat ( catCrossCount:Seq[(String,Int)], catWeight:Map[String,(Double,Double)],userCatAvg:Map[String,Statistic]){
    def toJson() = {
      val gson = new Gson
      gson.toJson(this)
    }
  }

  def runStatistics (trainData:RDD[event],desc:String) = {
    val catCrossCount = trainData.flatMap(x => {
        for (a <- x.upCats; b <- x.cpCats) yield {
          (a._1 + "-" + b._1, 1)
        }
      })
      .reduceByKey((a, b) => a + b)
      .collect().toSeq

    val catWeight = genCatWeight(trainData)
    val userCatAvg = genUserCatAvg(trainData)
    new dataStat(catCrossCount, catWeight, userCatAvg )
  }

  def evalAuc(data:DataFrame):Double = {
    val overallTrainAndLabels = data
      .select( "label", "probability", "prediction")
      .map { case Row(label: Double, prob: Vector, prediction: Double) =>
        (prob(1), label)
      }
    val gmpTrainMetrics = new BinaryClassificationMetrics(overallTrainAndLabels, 1000)
    gmpTrainMetrics.areaUnderROC()
  }

  def evaluate(data:DataFrame,name:String) = {
    val overallAuc = evalAuc(data)
    println(name+" overall auc:\t" + overallAuc)

    val exploreAuc = evalAuc(data.filter("explore_label"))
    println(name+" explore auc:\t" + exploreAuc)

    val perUserAucGrp = data
      .select("label", "probability", "prediction","uid","dwelltime")
      .map { case Row(label:Double,prob:Vector, prediction, uid:String,dwelltime:Double) =>
        val evalWeight = if(label>0) math.log(1 + dwelltime) else 1.0
        (uid, (new PredPoint(prob(1), label),new PredPoint(prob(1), label,evalWeight),new PredPoint(prob(1), label,dwelltime))) }
      .groupByKey()
      .cache()

    val perUserAuc = perUserAucGrp
      .map { case (uid, probLabels) => (uid, modelUtils.evalAuRoc(probLabels.map(_._1).toArray,5)) }
      .map(_._2)
      .filter(_ >= 0.0)
      .mean()
    println(name+ " peruser auc:\t" + perUserAuc)

    val perUserAucWeight = perUserAucGrp
      .map { case (uid, probLabels) => (uid, modelUtils.evalAuRoc(probLabels.map(_._2).toArray,5)) }
      .map(_._2)
      .filter(_ >= 0.0)
      .mean()
    println(name+" weighted peruser auc:\t" + perUserAucWeight)

    val accumulateDwell = perUserAucGrp
      .map { case (uid, probLabels) => (uid, modelUtils.evalAccuWeight(probLabels.map(_._3).toArray)) }
      .map(_._2)
      .filter(_ >= 0.0)
      .mean()
    println(name+" peruser accumulate dwelltime:\t" + accumulateDwell)

    perUserAucGrp.unpersist()
  }

}
