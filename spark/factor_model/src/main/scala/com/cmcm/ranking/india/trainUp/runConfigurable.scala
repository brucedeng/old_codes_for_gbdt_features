package com.cmcm.ranking.india.trainUp

import java.io.File

import com.cmcm.ranking.india.trainUp.flows.FlowFactory
import com.typesafe.config.ConfigFactory

/**
 * Created by mengchong on 5/11/16.
 */
object runConfigurable {
  def main(args: Array[String]) {

    val configPath = args(0)
    val config = ConfigFactory.parseFile(new File(configPath))

    val runFlows = config.getString("flows").split(',')
    val flowObjs = runFlows.map(flowType => FlowFactory.flows(flowType,config))
    flowObjs.foreach(flow => {
      println("running flow " + flow.getClass)
      flow.execute()
      println("running end")
    })
  }
}
