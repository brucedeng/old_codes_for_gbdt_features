package com.cmcm.ranking.india.trainUp.flows

import java.io.File

import com.cmcm.ranking.india.trainUp._
import com.typesafe.config.{ConfigFactory, Config}
import org.apache.spark.ml.classification.{LogisticRegressionModel, LogisticRegression}
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.linalg.{Vectors, Vector}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by mengchong on 5/11/16.
 */
class trainPhase1(config:Config) extends Flow {

  val inputStatPath = config.getString("formatted_data_path")
  val localModelFile = config.getString("local_model_file")
  val catModelTruncate = config.getInt("category_weight_truncate")
//  val phase1Output = config.getString("phase1_output")
  val p1ElasticNetParam = config.getDouble("p1_elastic_net_param")
  val p1RegParam = config.getDouble("p1_reg_param")
  val appName = config.getString("app_name")

  override def execute() {
    val desc = "train up with " +
      "1. only use category cross, weighted to 1:1 \n" +
      "2. use the weighted cat_rel train phase-1 features\n" +
      "3. filtered only events with cp category not empty\n" +
      "4. empty gmp is -1 in training data. must be set to 0 before use\n" +
      "5. added up filter. remove user profile less than upThreshold clicks\n" +
      "6. filter out cubes with less than 100 samples\n" +
      "7. normalize up categories."

    println(desc)
    //    println("Loading data: %s".format(dataPath))

    // initializing context
    val conf = new SparkConf().setAppName(appName)
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._

    sc.broadcast(modelUtils)

    println("loading data from " + inputStatPath + "/train_rdd")
    val trainData = sc.objectFile[event](inputStatPath + "/train_rdd")
    val testData = sc.objectFile[event](inputStatPath + "/test_rdd")

    val overallWt = sc.objectFile[(Double,Double)](inputStatPath + "/overall_weight").collect().head
    sc.broadcast(overallWt)

    val upAvgStd: Map[String, Statistic] = sc.objectFile[(String,Statistic)](inputStatPath + "/up_cat_statistic").collect().toMap
    println("up category average:")
    println(upAvgStd)
    sc.broadcast(upAvgStd)

    val param = modelUtils.parseCoefListWithTruncate(ioUtils.loadCoefFile(localModelFile), catModelTruncate)
    println("paramaters:")
    modelUtils.printCatCoef(param)

    def genCatRelData(raw: RDD[event]) = {
      raw.map(v => {
        //      case (label:Double,gmp:Double,kwRel:Double,upCats,cpCats,uid:String, content_id:String) =>
        val wtOverall = if (v.label > 0) overallWt._1 else overallWt._2
        val normUcat = modelUtils.normalize(v.upCats, upAvgStd)
        new Serializable {
          val label = v.label
          val gmp = v.gmp
          val catRel = modelUtils.genCatRel(normUcat, v.cpCats, param)
          val catRelRaw = modelUtils.getKwRel(normUcat,v.cpCats)
          val kwRel = v.kwRel
          val weight = wtOverall
          val uid = v.uid
          val dwelltime = v.dwelltime
        }
      })
    }

    // train function
    def train(runName: String, train: DataFrame, test: DataFrame) = {
      println("\n\nexperiment: " + runName + "\n\n")

      val lr = new LogisticRegression()

      lr.setElasticNetParam(p1ElasticNetParam)
        .setFitIntercept(true)
        .setWeightCol("weight")
      val paramMap = ParamMap(lr.maxIter -> 1000)
          .put(lr.regParam -> p1RegParam) // Specify multiple Params.
          .put(lr.probabilityCol -> "probability")

      val model2 = lr.fit(train, paramMap)

//      println("saving model to " + phase1Output + "/" + runName + "/overall_model")
//      model2.save(phase1Output + "/" + runName + "/overall_model")
      modelUtils.printModelSummary(model2)

      println("\nformatted_data_path\t"+inputStatPath)
      println("local_model_file\t" + localModelFile)
      println("category_weight_truncate\t" + catModelTruncate)
//      println("phase1_output\t" + phase1Output)
      println("p1_elastic_net_param\t" + p1ElasticNetParam)
      println("p1_reg_param\t" + p1RegParam)
      modelUtils.printPhase1Coef(model2.coefficients.toArray)
      rddUtils.evaluate(model2.transform(train),runName + " train")
      rddUtils.evaluate(model2.transform(test),runName + " test")
    }

    val gmpTrainingRaw = genCatRelData(trainData).persist(StorageLevel.MEMORY_AND_DISK_SER)
    val gmpTestRaw = genCatRelData(testData).persist(StorageLevel.MEMORY_AND_DISK_SER)

    // calculate category explore
    val allTestCount = gmpTestRaw.count()
    val exploreEvent = gmpTestRaw.filter(v => v.catRel!=0.0 && v.catRelRaw==0.0).cache()
    val exploreCount = exploreEvent.count()
    println("test event count\t" + allTestCount)
    println("explore rate\t" + exploreCount.toDouble/allTestCount)
    val exploreCatEval = exploreEvent.map(v => (v.label,Vectors.dense(0.0,v.catRel),1.0))
      .toDF("label", "probability", "prediction")
    println("explore category auc\t" + rddUtils.evalAuc(exploreCatEval))

    // train model with cat relevence and kw relevence
    val training = gmpTrainingRaw
      .map(v => {
        (v.label, modelUtils.genFeatureWithCatRel(v.gmp, v.catRel, v.kwRel), v.weight, v.uid,v.dwelltime,v.catRel>0 && v.catRelRaw==0.0)
      })
      .filter(x => x._2.numNonzeros != 0)
      .toDF("label", "features", "weight","uid","dwelltime","explore_label")

//    println("writing gmp train data into " + phase1Output + "/phase_1_train")
//    training.write.parquet(phase1Output + "/phase_1_train")

    // category testing data
    val testing = gmpTestRaw
      .map(v => {
        (v.label, modelUtils.genFeatureWithCatRel(v.gmp, v.catRel, v.kwRel), 1.0, v.uid, v.dwelltime,v.catRel>0 && v.catRelRaw==0.0)
      })
      .filter(x => x._2.numNonzeros != 0)
      .toDF("label", "features", "weight","uid","dwelltime","explore_label")

//    println("writing gmp test data into " + phase1Output + "/phase_1_test")
//    testing.write.parquet(phase1Output + "/phase_1_test")

    train("gmp-catrel-kwrel",training,testing)

    // train model with cat relevence without kw relevence
    val trainingNoKwlen = gmpTrainingRaw.map(v => {
        (v.label, modelUtils.genFeatureWithCatRel(v.gmp, v.catRel, 0.0), v.weight, v.uid,v.dwelltime,v.catRel>0 && v.catRelRaw==0.0)
      })
      .filter(x => x._2.numNonzeros != 0)
      .toDF("label", "features", "weight","uid","dwelltime","explore_label")

    // train model with cat relevence and kw relevence
    train("gmp-catrel",trainingNoKwlen,testing)

    val trainCatOnly = gmpTrainingRaw.map(v => {
      (v.label, modelUtils.genFeatureWithCatRel(0.0, v.catRel, 0.0), v.weight, v.uid,v.dwelltime,v.catRel>0 && v.catRelRaw==0.0)
    })
      .filter(x => x._2.numNonzeros != 0)
      .toDF("label", "features", "weight","uid","dwelltime","explore_label")

    train("catrel",trainCatOnly,testing)



  }
}
