package com.cmcm.ranking.india.trainUp

import java.io.File
import com.cmcm.ranking.india.featureUtils.utils
import com.typesafe.config.ConfigFactory
import org.apache.spark.ml.classification.LogisticRegression
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.linalg.{Vectors, Vector}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Row, DataFrame}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by mengchong on 4/25/16.
 */
object trainCatBaseline extends Serializable{
  val catMap = utils.getCatTree
  val normFactor = 3.0
  val upThreshold = 3.0

  def parseRows (x:(Seq[Row],Int)) = {
    modelUtils.L1_norm(modelUtils.rollupCats(modelUtils.parseSeqRows(x._1))).map(y => (y._1, (y._2 * x._2, y._2 * (if (x._2 > 0) 0.0 else 1.0))))
  }

  def filterRows (x:(Seq[Row],Int)): Boolean = {
    x match {
      case (cats: Any, label: Any) => true
      case _ => false
    }
  }

  def main(args: Array[String]) {
    val desc = "train up with " +
      "1. only use category cross, weighted to 1:1 \n" +
      "2. use the weighted cat_rel train phase-1 features\n" +
      "3. filtered only events with cp category not empty\n" +
      "4. empty gmp is -1 in training data. must be set to 0 before use\n" +
      "5. added up filter. remove user profile less than upThreshold clicks\n" +
      "6. filter out cubes with less than 100 samples\n" +
      "7. normalize up categories."

    println(desc)
    val configPath = args(0)

    val config = ConfigFactory.parseFile(new File(configPath))

    val l1Norm = config.getString("l1_norm")
    val normFactor = config.getDouble("norm_factor")
    val rollUpCats = config.getString("roll_up_cats")
    val normalizeUP = config.getString("normalize_up")
    val upThreshold = config.getDouble("up_threshold")
    val trainSampleThreshold = config.getDouble("train_sample_threshold")
    val elasticNetParam = config.getDouble("elastic_net_param")
    val regParam = config.getDouble("reg_param")
    val p1ElasticNetParam = config.getDouble("p1_elastic_net_param")
    val p1RegParam = config.getDouble("p1_reg_param")
    val dataPath = config.getString("data")
    val testPath = config.getString("test")
    val outputPath = config.getString("output")
    val appName = config.getString("app_name")
    val phase1Dir = config.getString("phase1_output")
    val localModelFile = config.getString("local_model_file")
    val trainStages = config.getString("train_stages").split(',').toSet

    //    println("Loading data: %s".format(dataPath))

    // initializing context
    val conf = new SparkConf().setAppName(appName)
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._

    sc.broadcast(modelUtils)

    // initialize normalize and rollup functions
    val normFunc:(Seq[(String,Double)],Double)=> Seq[(String,Double)] =
      if(l1Norm.toLowerCase == "true"){
        println("Using L1 normalize on categories and keywords")
        modelUtils.L1_norm
      }
      else (a,b) => a
    val rollupFunc:(Seq[(String,Double)])=> Seq[(String,Double)] =
      if(rollUpCats.toLowerCase == "true" ) {
        println("Using rollup on categories")
        modelUtils.rollupCats
      }
      else x=>x
    sc.broadcast(normFunc)
    sc.broadcast(rollupFunc)
    val parseInputFunc:(DataFrame,Double,Double) => RDD[event] = rddUtils.parseInputDataFrameNorm(normFunc,rollupFunc)

     //load input data
    val inputData = sqlContext.read.parquet(dataPath)
      .select("uid","content_id","label","dwelltime","u_cats","u_keywords","d_cats","d_keywords","gmp")

    inputData.printSchema()

    val trainData = parseInputFunc(inputData,upThreshold,normFactor )

    // load input test  data
    val inputTestData = sqlContext.read.parquet(testPath)
      .select("uid","content_id","label","dwelltime","u_cats","u_keywords","d_cats","d_keywords","gmp")
    val testData = parseInputFunc(inputTestData,upThreshold,normFactor )


    val overallWtRaw = trainData.map(x => (x.label,if(x.label > 0) 0.0 else 1.0)).reduce((a,b) => (a._1 + b._1,a._2 + b._2))
    val overallWt = (overallWtRaw._2/overallWtRaw._1,1.0)
    sc.broadcast(overallWt)

    val goals = List(Set("gmp","cat_rel"),Set("gmp","cat_rel","kw_rel"),Set("gmp"),Set("cat_rel"))

    // train multiple results
    goals.foreach(train_goal => {
      println("training with " + train_goal.mkString("-"))
      val gmpTraining = trainData
        .map(v => {
          val wtOverall = if (v.label > 0) overallWt._1 else overallWt._2
          val catRel = if(train_goal contains "cat_rel") modelUtils.getKwRel(v.upCats, v.cpCats) else 0.0
          val kwRel = if(train_goal contains "kw_rel" ) v.kwRel else 0.0
          val gmp = if(train_goal contains "gmp" ) v.gmp else 0.0
          (v.label, modelUtils.genFeatureWithCatRel(gmp, catRel, kwRel), wtOverall)
        })
        .filter(x => x._2.numNonzeros != 0)
        .toDF("label", "features", "weight")
        .persist(StorageLevel.MEMORY_AND_DISK_SER)

      // category testing data
      val gmpTest = testData
        .map(v => {
          val catRel = if(train_goal contains "cat_rel") modelUtils.getKwRel(v.upCats, v.cpCats) else 0.0
          val kwRel = if(train_goal contains "kw_rel" ) v.kwRel else 0.0
          val gmp = if(train_goal contains "gmp" ) v.gmp else 0.0
          (v.label, modelUtils.genFeatureWithCatRel(gmp, catRel, kwRel), 1.0, v.uid)
        })
        .toDF("label", "features", "weight", "uid")
        .persist(StorageLevel.MEMORY_AND_DISK_SER)

      val lr = new LogisticRegression()
      // Print out the parameters, documentation, and any default values.
      println("LogisticRegression parameters:\n" + lr.explainParams() + "\n")

      // We may set parameters using setter methods.
      lr.setMaxIter(1000)
        .setElasticNetParam(p1ElasticNetParam)
        .setFitIntercept(true)
        .setWeightCol("weight")

      // We may alternatively specify parameters using a ParamMap,
      // which supports several methods for specifying parameters.
      val paramMap = ParamMap(lr.maxIter -> 20)
          .put(lr.maxIter, 1000) // Specify 1 Param.  This overwrites the original maxIter.
          .put(lr.regParam -> p1RegParam) // Specify multiple Params.
          .put(lr.probabilityCol -> "probability")

      val model2 = lr.fit(gmpTraining, paramMap)

      println("saving model to " + phase1Dir + "/overall_model")
//      model2.save(phase1Dir + "/overall_model")
      modelUtils.printModelSummary(model2)

      // re calculate overall train labels
      val overallTrainAndLabels = model2.transform(gmpTraining)
        .select("features", "label", "probability", "prediction")
        .map { case Row(features: Vector, label: Double, prob: Vector, prediction: Double) =>
          (prob(1), label)
        }
      val gmpTrainMetrics = new BinaryClassificationMetrics(overallTrainAndLabels, 1000)
      val gmpTrainauROC = gmpTrainMetrics.areaUnderROC()
      println("overall train auc:\t" + gmpTrainauROC)

      val overallPredictionAndLabels = model2.transform(gmpTest)
        .select("features", "label", "probability", "prediction", "uid")
        .map { case Row(features: Vector, label: Double, prob: Vector, prediction: Double, uid: String) =>
          (prob(1), label, uid)
        }
        .persist(StorageLevel.MEMORY_AND_DISK_SER)

      // calculate test auc
//      overallPredictionAndLabels.saveAsTextFile(phase1Dir + "/overall_test")
      val gmpMetrics = new BinaryClassificationMetrics(overallPredictionAndLabels.map {
        case (prob, label, uid) => (prob, label)
      }, 1000)
      val gmpauROC = gmpMetrics.areaUnderROC()
      println("regParam:\t" + p1RegParam)
      println("trainSampleThreshold:\t" + trainSampleThreshold)
      println("overall test auc:\t" + gmpauROC)
      overallPredictionAndLabels.unpersist()
      println("elasticNetParam:\t" + p1ElasticNetParam)

      val perUserAucGrp = overallPredictionAndLabels.map { case (prob, label, uid) => (uid, new PredPoint(prob, label)) }
        .groupByKey()
        .map { case (uid, probLabel) => (uid, probLabel, modelUtils.evalAuRoc(probLabel.toArray, 0)) }
      //    perUserAucGrp.saveAsTextFile(phase1Dir + "/test_peruser_grouped")

      val perUserAuc = perUserAucGrp
        .map(_._3)
        .filter(_ > 0.0)
        .mean()
      println("peruser auc:\t" + perUserAuc)
      gmpTraining.unpersist()
      gmpTest.unpersist()
      modelUtils.printPhase1Coef(model2.coefficients.toArray)
    })
  }
}
