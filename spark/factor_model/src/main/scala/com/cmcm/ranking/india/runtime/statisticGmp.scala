package com.cmcm.ranking.india.runtime

/**
 * Created by mengchong on 4/15/16.
 */

import org.apache.spark.mllib.classification.LogisticRegressionWithLBFGS
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.{SparseVector, Vectors}
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics

object statisticGmp {

    def parseEventGmp(line:String) : (String,String,Double,Double,Double) = {
      val fields : Array[String] = line.split('\t')
      val uid = fields(0)
      val cid = fields(1)
      val label = fields(3).toDouble
      val gmp = fields(7).toDouble
      val gmpP = if(gmp < 0) 0.0 else gmp
      (uid,cid,label,gmp,gmpP)
    }

    def L1_norm(l : List[(String,Double)],base:Double = 0, fnames:Option[Map[String,Int]] = Option(null)): List[(String,Double)] = {
      val sum = l.filter(x => x._2 > 0).filter( x => fnames.isEmpty || fnames.get.contains(x._1) ).map(x => x._2).sum + base
      if (sum > base) l.map(x => (x._1,x._2/sum))
      else List[(String,Double)]()
    }

    def main(args: Array[String]) {
      val dataPath = args(0)
//      val outputPath = args(1)
      val outputDataPath = args(1)
      println("Loading data: %s".format(dataPath))

      val conf = new SparkConf().setAppName("train up")
      val sc = new SparkContext(conf)
      val eventData = sc.textFile(dataPath,100).map(parseEventGmp).cache()

      val outputTsv = eventData.map(x => x.productIterator.mkString("\t"))
      outputTsv.saveAsTextFile(outputDataPath + "/gmp")

      val predictionAndLabels = eventData.map(x => (x._5,x._3))

      val metrics = new BinaryClassificationMetrics(predictionAndLabels)
      val auROC = metrics.areaUnderROC()
      println("gmp auc: " + auROC)

    }

}
