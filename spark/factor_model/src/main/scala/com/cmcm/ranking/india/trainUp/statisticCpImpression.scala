package com.cmcm.ranking.india.trainUp

import com.cmcm.ranking.india.featureUtils.{cpParser, utils}
import com.cmcm.ranking.india.runtime.runUtils
import com.cmcm.ranking.india.trainUp.dep.upUtils
import org.apache.spark.ml.classification.{BinaryLogisticRegressionSummary, LogisticRegression}
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.sql.Row
import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by mengchong on 4/22/16.
 */
object statisticCpImpression {
  val catMap = utils.getCatTree
  val normFactor = 3.0
  val upThreshold = 3.0

  def parseCpCatKw(cpstr:String) : List[(String,Double)] = {
    val cpMap = new cpParser(cpstr)
    cpMap.getCategoryPair
//    val l1Cat = cpMap.getCategoryPair
//      .map( x => (utils.getL1Cat(x._1,catMap),x._2))
//      .filter( _._1 != "null" )
//      .groupBy(x => x._1)
//      .toList
//      .map(x => (x._1, x._2.map(_._2).sum))
//    l1Cat
  }

  def main(args: Array[String]) {
    val desc = "train up with 1. no inception for each category. " +
      "2. added weight, balance pos and neg events for each category." +
      "3. filtered only events with cp category not empty"
    val dataPath = args(0)
    val outputPath = args(1)
    //    val skipMap = args(2)

    val cachePath = dataPath + ".buffer"

    upUtils.upThreshold = upThreshold
    upUtils.normFactor = normFactor
    //    val outputDataPath = args(2)
    println("Loading data: %s".format(dataPath))

    val conf = new SparkConf().setAppName("up_cat")
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._

    val hadoopConf = sc.hadoopConfiguration
    val fs = org.apache.hadoop.fs.FileSystem.get(hadoopConf)
    val exists = fs.exists(new org.apache.hadoop.fs.Path(cachePath))

    val eventData = sc.textFile(dataPath)
      .flatMap(x => utils.L1_norm(parseCpCatKw(x.split('\t')(6))))
      .reduceByKey((a,b) => a + b )

    eventData.collect().foreach(x => println(x._1 + "\t" + x._2))

  }
}
