package com.cmcm.ranking.india.featureUtils

/**
 * Created by mengchong on 4/10/16.
 */
class cpParser(override val line:String) extends featureParser(line) {
  def getCategoryPair : List[(String,Double)] =
    if (jsonObj.contains("categories")) utils.formatCatKwList(jsonObj("categories").asInstanceOf[List[Map[String,Any]]])
    else List()

  def getKeywordsPair : List[(String,Double)] =
    if( jsonObj.contains("entities") ) utils.formatCatKwList(jsonObj("entities").asInstanceOf[List[Map[String,Any]]],"name","L1_weight")
    else List()

  def getDataMap : Map[String,Any] = {
    val cats = getCategoryPair
    val kws = getKeywordsPair
    jsonObj - ("categories","entities") + ("categories" -> cats,"keywords" -> kws)
  }
}
