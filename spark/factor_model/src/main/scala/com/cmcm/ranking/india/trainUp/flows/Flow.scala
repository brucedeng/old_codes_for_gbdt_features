package com.cmcm.ranking.india.trainUp.flows

/**
 * Created by mengchong on 5/11/16.
 */
abstract class Flow {
  def execute();
}
