package com.cmcm.ranking.india.runtime

import com.cmcm.ranking.india.featureUtils._
import org.apache.spark.mllib.classification.LogisticRegressionWithLBFGS
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.{SparseVector, Vectors}
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.util.MLUtils
/**
 * Created by mengchong on 4/13/16.
 */
object trainUpCategory {
  val catMap = utils.getCatTree

  def parseUpCat(upstr:String) : List[(String,Double)] = {
    val upMap = new upParser(upstr)
    upMap.getCategoryPair
      .map( x => (utils.getL1Cat(x._1,catMap),x._2))
      .filter( _._1 != "null" )
      .groupBy(x => x._1)
      .toList
      .map(x => (x._1, x._2.map(_._2).sum))
  }

  def parseCpCat(cpstr:String) : List[(String,Double)] = {
    val cpMap = new cpParser(cpstr)
    cpMap.getCategoryPair
      .map( x => (utils.getL1Cat(x._1,catMap),x._2))
      .filter( _._1 != "null" )
      .groupBy(x => x._1)
      .toList
      .map(x => (x._1, x._2.map(_._2).sum))
  }

  def parseEvent(line:String) : (Double,List[(String,Double)],List[(String,Double)],Double) = {
    val fields : Array[String] = line.split('\t')
    val label = fields(3).toDouble
    val upCat = parseUpCat(fields(5))
    val cpCat = parseCpCat(fields(6))
    val gmp = fields(7).toDouble
    (label,upCat,cpCat,if(gmp<0) 0.0; else gmp)
  }

  def L1_norm(l : List[(String,Double)],base:Double = 0, fnames:Option[Map[String,Int]] = Option(null)): List[(String,Double)] = {
    val sum = l.filter(x => x._2 > 0).filter( x => fnames.isEmpty || fnames.get.contains(x._1) ).map(x => x._2).sum + base
    if (sum > base) l.map(x => (x._1,x._2/sum))
    else List[(String,Double)]()
  }

  def genFeatureNorm(label:Double,gmp:Double, up:List[(String,Double)], cp:List[(String,Double)],catMap:Map[String,Int], normFactor:Double = 3 ): LabeledPoint = {
    val length = catMap.size
    def upNorm = L1_norm(up,normFactor,Option(catMap))
    val features:List[(Int,Double)] = for (u <- upNorm; c <- cp) yield{
      val (ucat,uweight) = u
      val (ccat,cweight) = c
      val uCatId = catMap.get(ucat)
      val cCatId = catMap.get(ccat)
      if (uCatId.isDefined && cCatId.isDefined) (uCatId.get*length + cCatId.get+1,uweight*cweight)
      else (-1,0.0)
    }
    val gmpP : Double = if(gmp < 0) 0.0 else gmp
    val feature_filter = features.filter(x=>x._1 >= 0) ++ List((0,gmpP))
    new LabeledPoint(label,Vectors.sparse(length*length+1,feature_filter))
  }

  def main(args: Array[String]) {
    val dataPath = args(0)
    val outputPath = args(1)
    val outputDataPath = args(2)
    println("Loading data: %s".format(dataPath))

    val conf = new SparkConf().setAppName("train up")
    val sc = new SparkContext(conf)
    val eventData = sc.textFile(dataPath).map(parseEvent).cache()

    val catNames = eventData.flatMap(x => x._2).reduceByKey( (a,b) => a+b ).collect()
    println(catNames.map(x => x._1 + ',' + x._2.toString))
    val catNames2 = eventData.flatMap(x => x._3).reduceByKey( (a,b) => a+b ).collect()
    println(catNames2.map(x => x._1 + ',' + x._2.toString))
    val allCat = (catNames ++ catNames2).filter(x => x._2>20).map(x => x._1).sorted
    val catMap = allCat.toSet[String].zipWithIndex.toMap
    println(catMap)
//    println(catMap.to)

    val trainData = eventData.map(x => genFeatureNorm(x._1,x._4,x._2,x._3,catMap,3))
    println("saving to " + outputDataPath + "/libsvm_data")
    MLUtils.saveAsLibSVMFile(trainData,outputDataPath + "/libsvm_data")
    eventData.unpersist()

    val splits = trainData.randomSplit(Array(0.6, 0.4), seed = 11L)
    val training = splits(0).cache()
//    MLUtils.saveAsLibSVMFile(training,outputDataPath + "/train")
    val test = splits(1)
//    MLUtils.saveAsLibSVMFile(test,outputDataPath + "/test")

    val model = new LogisticRegressionWithLBFGS()
      .setNumClasses(2)
      .run(training)

    val trainPredAndLabels = training.map { case LabeledPoint(label, features) =>
      val prediction = model.predict(features)
      (prediction, label)
    }
    println("saving to " + outputDataPath + "/train_eval")
    trainPredAndLabels.saveAsTextFile(outputDataPath + "/train_eval")

    val predictionAndLabels = test.map { case LabeledPoint(label, features) =>
      val prediction = model.predict(features)
      (prediction, label)
    }
    println("saving to " + outputDataPath + "/test_eval")
    predictionAndLabels.saveAsTextFile(outputDataPath + "/test_eval")

    println("saving model: %s".format(outputPath))
    model.save(sc,outputPath)

    val metricstrain = new BinaryClassificationMetrics(trainPredAndLabels)
    val auROCtrain = metricstrain.areaUnderROC()
    val metrics = new BinaryClassificationMetrics(predictionAndLabels)
    val auROC = metrics.areaUnderROC()
    println("train auc: " + auROCtrain)
    println("test auc: " + auROC)

//    val result =
    println(model.weights.toArray)

    //
    //  def genFeature(label:Double,gmp:Double, up:List[(String,Double)], cp:List[(String,Double)], catMap:Map[String,Int]): LabeledPoint = {
    //    val length = catMap.size
    //    val features:List[(Int,Double)] = for (u <- up; c <- cp) yield{
    //      val (ucat,uweight) = u
    //      val (ccat,cweight) = c
    //      val uCatId = catMap.get(ucat)
    //      val cCatId = catMap.get(ccat)
    //      if (uCatId.isDefined && cCatId.isDefined) (uCatId.get*length + cCatId.get + 1,uweight*cweight)
    //      else (-1,0.0)
    //    }
    //    val gmpP : Double = if(gmp < 0) 0.0 else gmp
    //    val feature_filter = features.filter(x=>x._1 >= 0) ++ List((0,gmpP))
    //    new LabeledPoint(label,Vectors.sparse(length*length+1,feature_filter))
    //  }

//    // Precision by threshold
//    val precision = metrics.precisionByThreshold()
//    precision.foreach { case (t, p) =>
//      println(s"Threshold: $t, Precision: $p")
//    }
//
//    // Recall by threshold
//    val recall = metrics.recallByThreshold()
//    recall.foreach { case (t, r) =>
//      println(s"Threshold: $t, Recall: $r")
//    }
//
//    // Precision-Recall Curve
//    val PRC = metrics.pr
//
//    // F-measure
//    val f1Score = metrics.fMeasureByThreshold()
//    f1Score.foreach { case (t, f) =>
//      println(s"Threshold: $t, F-score: $f, Beta = 1")
//    }
//
//    val beta = 0.5
//    val fScore = metrics.fMeasureByThreshold(beta)
//    f1Score.foreach { case (t, f) =>
//      println(s"Threshold: $t, F-score: $f, Beta = 0.5")
//    }
//
//    // AUPRC
//    val auPRC = metrics.areaUnderPR()
//    println("Area under precision-recall curve = " + auPRC)
//
//    // Compute thresholds used in ROC and PR curves
//    val thresholds = precision.map(_._1)

    // ROC Curve
//    val roc = metrics.roc()

    // AUROC

  }
}
