package com.cmcm.ranking.india.trainUp

/**
 * Created by mengchong on 5/5/16.
 */
class event(val label:Double,val gmp:Double,val kwRel:Double,val upCats:Seq[(String,Double)],val cpCats:Seq[(String,Double)],val uid:String,val content_id:String, val dwelltime:Double = 0.0) extends Serializable {
  override def toString = "{\"uid\"=\"" + uid + "\",\"content_id\"=\"" + content_id + "\",\"label\"=" + label+ ",\"gmp\"=" + gmp + ",\"kwRel\"=" + kwRel + ",\"upCats\"=\"" + upCats + "\",\"cpCats\"=" + cpCats+ ",\"dwelltime\":" + dwelltime + "}"
}

case class Statistic ( mean:Double,  std:Double) extends Serializable {
  override def toString = "Statistic(" + mean + "," + std + ")"
}

class PredPoint (val score:Double, val label:Double, val weight:Double = 1.0) extends Serializable {
  override def toString = s"($score,$label,$weight)"
}
