package com.cmcm.ranking.india.trainUp

import java.io.File

import com.cmcm.ranking.india.featureUtils.utils
import com.typesafe.config.ConfigFactory
import org.apache.spark.sql.Row
import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by mengchong on 5/18/16.
 */
object EmulatePhase1 extends Serializable {
  def main(args: Array[String]) {
    val desc = "stat each cp category content count"
    val configPath = args(0)

    val config = ConfigFactory.parseFile(new File(configPath))
    val cpPath = config.getString("cp_path")
    val upPath = config.getString("up_path")
    val localModelFile = config.getString("local_model_file")
    val language = config.getString("language")
    val ts = config.getLong("timestamp")
    val interval = config.getInt("interval") * 86400

    println("Loading data: %s".format(cpPath))

    val conf = new SparkConf().setAppName("emulate phase1")
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    //    import sqlContext.implicits._

    sc.broadcast(modelUtils)
    sc.broadcast(this)

    val df = sqlContext.read.format("json").load(cpPath)
    df.printSchema()
    df.show(100)

    val catCounts = df //.select("item_id","is_servable","cp_disable","type","categories","update_time","language","")
      .filter("is_servable and not cp_disable and" +
        " ( type='article' or type='video' or type='photostory' or type='slideshow' ) " +
        " and language = '" + language + "' and ( editor_level is null or editor_level != -5 ) " +
        " and publish_time > "+(ts - interval))
      .select("item_id","update_time","categories")
      .map(row => { val item_id = row.getString(0)
        val updateTime = row.getLong(1)
        val categories = row.getSeq[Row](2)
        (item_id,(updateTime,modelUtils.parseSeqRows(categories)))
      })

      .filter(_._2._2.nonEmpty)
      .reduceByKey((a,b) => if(a._1 >= b._1) a else b )
      .map{case (item_id,data) => (data._2.map(pair => pair._1).mkString(","), 1.0)}
      .reduceByKey((a,b) => a+b)
      .collect()
      .toSeq

    catCounts.foreach(x => println(x._1 + "\t" + x._2))
    sc.broadcast(catCounts)
    val totolCnt = catCounts.map(_._2).sum
    println("total_count\t" + totolCnt)

    println(catCounts)

    val upData = sqlContext.read.parquet(upPath)
    upData.printSchema()
    val rawCoef = ioUtils.loadCoefFile(localModelFile)
    val transformed = for (i <- 2 to 7 ) yield {(i.toString,modelUtils.parseCoefListWithTruncate(rawCoef,i))}
    val params = Seq(("raw",null),("inf",modelUtils.parseCoefListWithTruncate(rawCoef,4000))) ++ transformed
    sc.broadcast(params)

    val result = upData.select("categories")
    .map(row => modelUtils.parseSeqRows(row.getSeq[Row](0)))
    .filter(categories => categories.length > 0)
    .flatMap(cats => {
      val newCatList = params.map{
        case (exp,param:Any) => (exp,modelUtils.transformCat(cats,param))
        case (exp,null) => (exp,cats)
      }
      def compare(a:Map[String,Double], b:String) = {
        val seq2 = b.split(',')
        seq2.map(x => if(a contains x) 1 else 0).sum > 0
      }
      newCatList.map{case (experiment,category) =>
        val catMap = category.filter(_._2>0).toMap
        val matched = catCounts.map{case (cpCats,count) => if(compare(catMap,cpCats)) count else 0.0}.sum;
        (experiment,( matched/totolCnt, 1))
      }
    })
    .reduceByKey((a,b) => (a._1 + b._1, a._2 + b._2))
    .map(result => {
      val (exp,summed) = result
      val (sumVal,count) = summed
      (exp,sumVal/count, count)
    })
    .collect()
    .foreach{case (exp,avg,count) => println(exp + "\t" + avg + "\t" + count)}

  }
}
