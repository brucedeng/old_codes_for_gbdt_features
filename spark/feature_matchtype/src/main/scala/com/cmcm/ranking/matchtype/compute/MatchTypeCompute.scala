package com.cmcm.ranking.matchtype.compute

import com.cmcm.ranking.matchtype.feature.parseCrossCfbKeys.{cfbFeatureValue, cfbCrossData}
import com.cmcm.ranking.matchtype.feature.{parseCrossCfbKeys, AggregateXfb}
import com.cmcm.ranking.matchtype.processor._
import com.cmcm.ranking.matchtype.spark.SparkBatchContext
import com.cmcm.ranking.matchtype.util.Parameters
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.DataFrame
import org.apache.spark.storage.StorageLevel

/**
 * Created by mengchong on 5/23/16.
 */
trait MatchTypeCompute extends EventBatchCompute{
  this: SparkBatchContext =>
  import Parameters._

  override def compute: Unit = {
    super.compute
    logInfo("Start Matchtype Compute")
    val xfbRdd = getRDD[RDD[String]](constant_xfb_rdd,this)
    val featureLogRdd = getRDD[RDD[String]](constant_featurelog_rdd,this)
    val cpRdd = getRDD[RDD[String]](constant_cp_rdd,this)
    val eventRdd = getRDD[RDD[String]](constant_event_log_rdd,this)

    val sampleRatio = this.getBatchContext.get(constant_sample_ratio) match {
      case Some(rate) => rate.toDouble
      case None => 1.0
    }
    val cfbThreshold = this.getBatchContext.get(constant_cfb_pv_threshold) match {
      case Some(rate) => rate.toDouble
      case None => 20.0
    }
    val offlineCutoff = this.getBatchContext.get(constant_cfb_offline_cutoff) match {
      case Some(rate) => rate.toInt
      case None => 2000
    }
    val docAgeCutoff = this.getBatchContext.get(constant_cfb_docage_cutoff) match {
      case Some(rate) => rate.toInt
      case None => 86400
    }
    println("sample ratio is :\t"+ sampleRatio)
    println("cfb threshold is :\t" + cfbThreshold)
    println("offline cutoff is :\t" + offlineCutoff)
    println("doc_age cutoff is :\t" + docAgeCutoff)

//    val cfbData = xfbRdd.collect()
//    cfbData.map(line => try{println(line); new xfb(line)}
//    catch {case e:Exception => println("error parsing"); null})

    val xfbParsed = xfbRdd.map(line => try{println(line); new xfb(line)}
      catch {case e:Exception => println("error parsing"); null})
      .filter(x => x != null)
//      .cache()
    // filter out threshold


    val featureLogParsed = FeaturelogProcessor.process(featureLogRdd,this.getBatchContext)
    val cpParsed = CPProcessor.process(cpRdd,this.getBatchContext)

    val xfbFilter = xfbParsed.filter(xfb => xfb.featureType == "C_D_U_CONID_CATEGORY" )
      // filter out pv less than threshold
//      .map(xfb => {val contentKey = xfb.featureName.split(keyValueDelimiter)(0)
//        val pv = xfb.rawPv
//        (contentKey,(pv,xfb))
//      })
//      .groupByKey()
//      .map{case (contentKey,xfbAgg) =>
//        val sumPv = xfbAgg.map(x => x._1).sum
//        (sumPv,xfbAgg.map(x=>x._2))}
//      .filter(data => data._1 >= cfbThreshold)
//      .flatMap(data => data._2)
      // filter out only one key item
//      .filter(xfb => xfb.featureName.split(keyValueDelimiter).length==2)
//      .persist(StorageLevel.MEMORY_AND_DISK)

    val eventParsed = EventLogProcessor.process(eventRdd,this.getBatchContext)
    val eventFilter = eventParsed.filter(e => e.content_id != "" && e.level1=="1" && e.level1_type=="1"
      && e.app_lan=="hi" && e.pid=="11" && e.channel=="29")

    val cpMd5 = cpParsed.filter(cp => cp._1 != "")
      .map{case (conid,title_md5,publsihTime) => (conid,(title_md5,publsihTime))}
//    cpParsed.saveAsTextFile("/tmp/mengchong/cp_md5_dump")
    
    val start_ts = this.getBatchContext(constant_xfb_start_ts).toLong
    val end_ts = this.getBatchContext(constant_xfb_end_ts).toLong

    val aggCfb = xfbFilter.groupBy(xfb => xfb.featureName )
      .map(x => (x._1,AggregateXfb.calcAgg(decay_factor,decay_window,aggregate_interval)(start_ts,end_ts,x._2.toSeq)))
      .flatMap{case (keys,agg_cfb) => agg_cfb.map(x => (keys,x))}
      .map{case (fname,(ts,aggclk,aggpv)) =>
        val splits = fname.split(keyValueDelimiter)
        val contentKey = splits(0)
        val userKey = if(splits.length > 1) splits(1) else ""
        (contentKey,(ts,userKey,aggclk,aggpv))
      }.join(cpMd5)
      .map{case (contentKey,((ts,userKey,aggclk,aggpv),(titleMd5,publishTime))) =>
        ((ts,contentKey),(userKey,aggclk,aggpv,ts-publishTime))} // ts - publishTime is doc_age
      .filter(x => x._2._4 < docAgeCutoff) //x._2._3 > cfbThreshold &&
      .groupByKey()
      .filter{case ((ts,contentKey),eventList) =>
        eventList.map{case (userKey,aggClk,aggPv,docAge) => aggPv}
          .sum > cfbThreshold
      }
      .map{case ((ts,contentKey),cfbKeys) =>
        (ts, (contentKey, cfbKeys.map{case (uKey,aggClk,aggPv,docAge) => (uKey,new cfbFeatureValue(aggClk,aggPv))}
          .filter(x => x._1!="").toMap))
      }
      .groupByKey(145)

//    xfbFilter.sortBy(x => x.ts).saveAsTextFile("/tmp/mengchong/cfb_filter_result")
//    xfbFilter.unpersist()

//    aggCfb.saveAsTextFile("/tmp/mengchong/aggregatedCfb")

    val featurelogRaw = featureLogParsed
      .sample(false,sampleRatio)
      .filter(x => x.prodId=="11" && x.predictor.startsWith("hindi") && x.articles.length > 400 && x.up.get[Seq[(String,Double)]]("categories",Seq()).nonEmpty)
      .map(x => {
        val upCat = x.up.get[Seq[(String,Double)]]("categories",Seq()).map {
          case (cat, weight) =>
            (cat, weight)
        }
        val docs = x.articles.map(feature => (feature.item_id, feature.features.get("C_D_U_CONID_CATEGORY_MICRO_MEAN_COEC")))
        // join_ts, up_category, key
        (x.ts/decay_window*decay_window-decay_window,upCat,x.req_id+fieldDelimiter+x.predictor+fieldDelimiter+x.uid+fieldDelimiter+x.ts,docs)
      })

    // join featurelog with cp
    val featurelogContent = featurelogRaw
      .flatMap{case (join_ts, upCats,key,docs) => docs.map{case (conid,feature) => (conid,(key,join_ts,upCats,feature))}}
      .join(cpMd5)
      .map{case (conid,((key,join_ts,upCats,feature),(titleMd5,publishTime))) => ((key,join_ts,upCats),(conid,feature,titleMd5)) }
      .groupByKey(256)
      .map{case ((key,join_ts,upCats),docs) =>
        (key,join_ts, upCats, docs.toSeq)}
//      .map(x => (x._1,1))
//      .reduceByKey((a,b) => a+b)

    val eventGen = eventFilter.map(e => (e.reqId,1)).reduceByKey((a,b) => a+b)
    val featurelogEvent = featurelogContent.map{case (key,join_ts,upCats,docs) =>
        val reqId = key.split(fieldDelimiter)(0)
        (reqId,(key,join_ts,upCats,docs))
      }.join(eventGen)
      .map{case (reqId,((key,join_ts,upCats,docs),events)) => (key,join_ts,upCats,docs)}
      .persist(StorageLevel.MEMORY_AND_DISK)

    val featurelogUp = featurelogEvent
      .map{case (key,join_ts, upCats, docs) => (join_ts,(key,upCats))}
      .groupByKey()

    val cfbFeature = aggCfb.join(featurelogUp)
      .flatMap{case (ts,(cfb,ups)) =>
        ups.map{case (key,categories) =>
          val contentList = cfb.map{case (contentKey,features) =>
              (contentKey,parseCrossCfbKeys.calcSingleFeature(features,categories))
            }
            .toSeq
            .sortBy(x => -x._2)
            .zipWithIndex
            .filter(x => x._2 < offlineCutoff)
            .map{case ((contentKey,score),index) => (index,contentKey,score)}
          (key,contentList) //categories.toList
        }
      }//.persist(StorageLevel.MEMORY_AND_DISK)

//    cfbFeature.sortByKey().saveAsTextFile("/tmp/mengchong/match_type_output/test_cfb_features")
//    featurelogEvent.sortBy(x => x._1).saveAsTextFile("/tmp/mengchong/match_type_output/test_featurelog_data")

    val cfbScores = cfbFeature
      .join(featurelogEvent.map{case (key,join_ts,upCats,docs) => (key,docs)})
      .map{case (key,(offlineScores,onlineScores)) =>
          val onlineMap = onlineScores.map{case (conid,feature,titleMd5) => (conid,(feature,titleMd5))}.toMap
          val scoreJnd = offlineScores.map{case (index,contentKey,feature) =>
            if (onlineMap contains contentKey) {
              val (onlineFeature,titleMd5) = onlineMap(contentKey)
              (index,contentKey,feature,onlineFeature)
            }
            else (index,contentKey,feature,None)
          }
        (key,onlineScores.length,scoreJnd)
      }.cache()
    featurelogEvent.unpersist()

    // calculate coverage
    def printValue(name:String,value:Double) = {
      println(name + "\t" + value)
    }

    val avg = cfbScores.map{case (key,count,docs) =>
      def coverage(data:Seq[(Int,String,Double,Option[Double])],count:Int) = {
        val overallCnt = data
          .filter{case (index,conid,offlineFeature,onlineFeature) => index < count}
          .map{case (index,conid,offlineFeature,onlineFeature) =>
            onlineFeature match {
              case Some(f) => (1.0,1.0)
              case None => (0.0,1.0)
            }
          }
          .reduce((a,b) => (a._1+b._1,a._2+b._2))

        overallCnt._1/overallCnt._2
      }

      val top10 = coverage(docs,10)
      val top50 = coverage(docs,50)
      val top100 = coverage(docs,100)
      val top1k = coverage(docs,1000)
      val top2k = coverage(docs,2000)
      (top10,top50,top100,top1k,top2k,1)
    }.reduce((a,b) => (a._1+b._1,a._2+b._2,a._3+b._3,a._4+b._4,a._5+b._5,a._6+b._6))

    val (top10,top50,top100,top1k,top2k,cnt) = avg
    printValue("top10",top10/cnt)
    printValue("top50",top50/cnt)
    printValue("top100",top100/cnt)
    printValue("top1k",top1k/cnt)
    printValue("top2k",top2k/cnt)

    rddContext += (constant_feature_rdd -> cfbScores)

  }
}
