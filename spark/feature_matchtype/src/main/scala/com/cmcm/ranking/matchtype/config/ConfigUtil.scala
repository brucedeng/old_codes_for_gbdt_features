package com.cmcm.ranking.matchtype.config

import java.io.File

import com.typesafe.config.ConfigFactory
import collection.JavaConversions._
/**
 * Created by mengchong on 5/25/16.
 */
object ConfigUtil {
  def parseConfig(configPath:String) = {
    val config = ConfigFactory.parseFile(new File(configPath))
    val entrySet = config.entrySet()
    val keySet = for (entry <- config.entrySet()) yield {
      entry.getKey
    }
    keySet.map(x => x -> config.getString(x)).toMap
  }
}
