package com.cmcm.ranking.matchtype.util

import java.io.{PrintWriter, StringWriter}

import org.apache.spark.Logging

/**
 * Created by mengchong on 5/24/16.
 */
object LoggingUtils extends Logging{
  def getException(ex:Throwable) = {
    val sw = new StringWriter
    ex.printStackTrace(new PrintWriter(sw))
    sw.toString
  }

  def loggingInfo(msg: => String): Unit = super.logInfo(msg)

  def loggingDebug(msg: => String): Unit = super.logDebug(msg)

  def loggingTrace(msg: => String): Unit = super.logTrace(msg)

  def loggingWarning(msg: => String): Unit = super.logWarning(msg)

  def loggingError(msg: => String): Unit = super.logError(msg)

  def loggingInfo(msg: => String, throwable: Throwable): Unit = super.logInfo(msg, throwable)

  def loggingDebug(msg: => String, throwable: Throwable): Unit = super.logDebug(msg, throwable)

  def loggingTrace(msg: => String, throwable: Throwable): Unit = super.logTrace(msg, throwable)

  def loggingWarning(msg: => String, throwable: Throwable): Unit = super.logWarning(msg, throwable)

  def loggingError(msg: => String, throwable: Throwable): Unit = super.logError(msg, throwable)
}
