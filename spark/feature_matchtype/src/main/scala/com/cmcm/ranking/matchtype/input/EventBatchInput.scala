package com.cmcm.ranking.matchtype.input

import com.cmcm.ranking.matchtype.component.BatchComponent
import com.cmcm.ranking.matchtype.spark.SparkBatchContext

/**
 * Created by mengchong on 5/23/16.
 */
trait EventBatchInput extends BatchComponent{
  this:SparkBatchContext =>
  def getInput()
}
