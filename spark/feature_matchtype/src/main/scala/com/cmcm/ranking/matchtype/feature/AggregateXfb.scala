package com.cmcm.ranking.matchtype.feature

import com.cmcm.ranking.matchtype.processor.xfb
import com.cmcm.ranking.matchtype.util.Parameters._
/**
 * Created by mengchong on 5/24/16.
 */
object AggregateXfb {
  def calcAgg(decayFactor:Double,wnd:Int,window:String)(start_ts:Long,end_ts:Long,events:Seq[xfb]) = {
    val num = window.slice(0, window.size - 1).toInt
    val period = window.last match {
      case x:Char if x.toString == "h" => 6
      case x:Char if x.toString == "d" => 144
      case _ => 0
    }
    val interval = num * period * wnd - wnd
    val tsMap = events.map(xfb => (xfb.ts,xfb)).toMap.withDefaultValue(new xfb(0,"","",0.0,0.0))
    val removeDecay = math.pow(decayFactor,interval - 1)
    val start_ts_wnd = start_ts/wnd*wnd
    val origTimestamp = start_ts_wnd - interval

    def agg_cfb(decay_val:List[(Long,Double,Double)],curts:Long):List[(Long,Double,Double)] = {
      if(curts >= end_ts) decay_val
      else {
        val curAgg = decay_val match {
          case Nil =>
            var aggClk = 0.0;
            var aggPv = 0.0
            for (timestamp <- origTimestamp to start_ts_wnd + wnd by wnd) {
              val current_value = tsMap(timestamp)
              aggClk = aggClk * decayFactor + current_value.rawClk
              aggPv = aggPv * decayFactor + current_value.rawPv
            }
            (start_ts_wnd, aggClk, aggPv)
          case x :: xs =>
            val (prevts, aggClk, aggPv) = x
            val rmVal = tsMap(curts-interval)
            val curVal = tsMap(curts)
            (curts,(aggClk-rmVal.rawClk*removeDecay)*decayFactor+curVal.rawClk,(aggPv-rmVal.rawPv*removeDecay)*decayFactor+curVal.rawPv)
        }
        agg_cfb(curAgg::decay_val,curts+wnd)
      }
    }

    agg_cfb(Nil,start_ts_wnd)
  }
}
