package com.cmcm.ranking.matchtype.processor

import com.cmcm.ranking.matchtype.util.{FeatureMap, LoggingUtils, JsonUtil}
import org.apache.spark.rdd.RDD
import org.json4s.JsonAST._

import scala.collection.mutable
import scala.util.{Failure, Success, Try}

/**
 * Created by mengchong on 5/24/16.
 */
object FeaturelogProcessor extends Processor[featurelog] with Serializable{
  override def process(inputRDD: RDD[String], batchContext: mutable.Map[String, String]): RDD[featurelog] = {
    inputRDD.map(line => preprocess(line))
  }

  def preprocess(line:String)  = {

    Try(JsonUtil.convertToJValue(line)) match{
      case Success(jvalue) =>
        val up = jvalue \ "up" match {
          case JObject(obj) => extractUp(JObject(obj))
          case _ => Map[String,Any]()
        }
        val articles = extractFeatureList(jvalue \ "docs")
        val req_id = extractStr(jvalue \ "req_id")
        val prod_id = extractStr(jvalue \ "prod_id")
        val predictor = extractStr(jvalue \ "predictor_id")
        val ts = jvalue \ "now" match {
          case JInt(value) => value.toLong
          case _ => 0
        }
        val uid = extractStr(jvalue \ "uid")
        new featurelog(req_id,prod_id,predictor,ts,uid,new upData(up),articles)
      case Failure(ex) =>
        LoggingUtils.loggingError(LoggingUtils.getException(ex))
        new featurelog("","","",0,"",new upData(Map()),List())
    }
  }

  def extractUp(jvalue:JValue) = {
    val catStr = jvalue \ "cg" match{
      case JObject(obj) =>
        obj.toList.map( x => (x._1,extractDouble(x._2)) )
      case _ => Nil
    }
    val age = extractInt(jvalue,"gender",0)
    val gender = extractInt(jvalue,"age",0)
    Map("age"->age,"gender"->gender,"categories"->catStr)
  }

  def extractFeatureList(jvalue:JValue) = {
    jvalue match {
      case JArray(v) => v.map {
        case JObject(obj) =>
          val (id,features) = obj.toList.partition(x => x._1 == "id")
          val conid = extractStr(id.head._2)
          val feature = features
            .map(x => if(FeatureMap.featureLoggerMap contains x._1) (FeatureMap.featureLoggerMap(x._1),extractDouble(x._2))
              else (x._1,extractDouble(x._2)))
          new articleFeature(conid,feature.toMap)
        case _ => new articleFeature("",Map())
      }
      case _ => List(new articleFeature("",Map()))
    }
  }

  def extractDouble(jvalue:JValue):Double = {
    jvalue match {
      case JDouble(d) => d
      case JInt(d) => d.toDouble
      case _ => 0.0
    }
  }

  def extractInt(jvalue: JValue, fieldName: String, default:Int = 0):Int = {
    jvalue \ fieldName match {
      case JInt(s) => s.toInt
      case _ => default
    }
  }

  def extractStr(jvalue: JValue, default: String = "") = {
    jvalue match {
      case JString(s) => if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString()
      case _ => default
    }
  }
}
