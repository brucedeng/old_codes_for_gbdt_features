package com.cmcm.ranking.matchtype.output

import com.cmcm.ranking.matchtype.component.BatchComponent
import com.cmcm.ranking.matchtype.spark.SparkBatchContext

/**
 * Created by mengchong on 5/30/16.
 */
trait EventBatchOutput extends BatchComponent{
  this:SparkBatchContext =>
  def outPut: Unit = {
    logInfo("Empty output in EventBatchOutput...")
  }
}
