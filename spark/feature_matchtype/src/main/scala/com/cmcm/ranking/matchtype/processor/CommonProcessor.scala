package com.cmcm.ranking.matchtype.processor

import org.apache.spark.sql.Row

/**
 * Created by mengchong on 5/24/16.
 */
object CommonProcessor {
  def parseSeqRows(cats:Seq[Row]): Seq[(String,Double)] ={
    def toDouble(value:Any) = {
      if (value == null) 0.0
      else value.asInstanceOf[Double]
    }
    cats match {
      case a:Any =>  a.filter (x => x != null).map (row => (row.getString (0),toDouble(row.get(1))) )
      case null => Nil
    }
  }
}
