package com.cmcm.ranking.matchtype.spark

import com.cmcm.ranking.matchtype.util.Parameters
import org.apache.spark.{SparkContext, SparkConf}

import scala.util.Try

/**
 * Created by mengchong on 5/23/16.
 */
trait TrainDataSparkContext extends SparkBatchContext{
  import Parameters._

  override def init(): Unit ={
    super.init()
    logInfo("Start TrainDataSparkContext...")
    val sparkConf = new SparkConf()
      .set("spark.default.parallelism", Try(batchContext(constant_parallelism)).getOrElse("32"))
      .setAppName("cfb matchtype pipeline")
    sbc = new SparkContext(sparkConf)
//    sqlc = new org.apache.spark.sql.SQLContext(sbc)
  }

}
