package com.cmcm.ranking.matchtype.tmp

import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mengchong on 6/3/16.
  */
object statisticRecallRate {
  def main(args:Array[String]): Unit = {
    val sparkConf = new SparkConf()
      .setAppName("statistic recall rate")
    val sbc = new SparkContext(sparkConf)

    val inputRdd = sbc.objectFile[(String,Seq[(Int,String,Double,Option[Double])])](args(0))

    def coverage(data:Seq[(Int,String,Double,Option[Double])],count:Int) = {
      val overallCnt = data
        .filter{case (index,conid,offlineFeature,onlineFeature) => index < count}
        .map{case (index,conid,offlineFeature,onlineFeature) =>
          onlineFeature match {
            case Some(f) => (1.0,1.0)
            case None => (0.0,1.0)
          }
        }
        .reduce((a,b) => (a._1+b._1,a._2+b._2))

      overallCnt._1/overallCnt._2
    }

    def printValue(name:String,value:Double) = {
      println(name + "\t" + value)
    }

    val avg = inputRdd.map{case (key,docs) =>
      val top10 = coverage(docs,10)
      val top50 = coverage(docs,50)
      val top100 = coverage(docs,100)
      val top1k = coverage(docs,1000)
      val top2k = coverage(docs,2000)
      (top10,top50,top100,top1k,top2k,1)
    }.reduce((a,b) => (a._1+b._1,a._2+b._2,a._3+b._3,a._4+b._4,a._5+b._5,a._6+b._6))

    val (top10,top50,top100,top1k,top2k,cnt) = avg
    printValue("top10",top10/cnt)
    printValue("top50",top50/cnt)
    printValue("top100",top100/cnt)
    printValue("top1k",top1k/cnt)
    printValue("top2k",top2k/cnt)
  }
}
