package com.cmcm.ranking.matchtype.app

import com.cmcm.ranking.matchtype.compute.MatchTypeCompute
import com.cmcm.ranking.matchtype.input.MatchTypeDataInput
import com.cmcm.ranking.matchtype.output.MatchTypeDataOutput
import com.cmcm.ranking.matchtype.spark.TrainDataSparkContext

/**
 * Created by mengchong on 5/30/16.
 */
object ConidCategoryMatchType extends BatchApp
  with TrainDataSparkContext
  with MatchTypeDataInput
  with MatchTypeCompute
  with MatchTypeDataOutput
