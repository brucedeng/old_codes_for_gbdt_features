package com.cmcm.ranking.matchtype.spark

import com.cmcm.ranking.matchtype.component.BatchComponent
import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by mengchong on 5/23/16.
 */
trait SparkBatchContext extends BatchComponent{
  @transient protected var sbc:SparkContext = _
  @transient protected var sqlc:SQLContext = _
  protected val rddContext:scala.collection.mutable.Map[String, Any] = scala.collection.mutable.Map[String, Any]()
  protected val batchContext:scala.collection.mutable.Map[String,String] = scala.collection.mutable.Map[String, String]()

  override def init() = {
    super.init()
    logInfo("SparkBatchContext finished init ...")
  }

  def getSparkContext : SparkContext = sbc

  def setSparkContext(_ssc:SparkContext) = {
    sbc = _ssc
  }

  def getSqlContext : SQLContext = sqlc

  def setSqlContext(_ssqlc:SQLContext) = {
    sqlc = _ssqlc
  }

  def getRddContext = rddContext
  def setRddContext(key:String, value:Any) = rddContext += (key -> value)
  def getBatchContext = batchContext
  def setBatchContext(key:String, value:String) = batchContext += (key -> value)
}
