package com.cmcm.ranking.matchtype.output

import com.cmcm.ranking.matchtype.spark.SparkBatchContext
import com.cmcm.ranking.matchtype.util.Parameters
import com.cmcm.ranking.matchtype.util.Parameters._
import org.apache.spark.rdd.RDD

/**
 * Created by mengchong on 5/30/16.
 */
trait MatchTypeDataOutput extends EventBatchOutput{
  this:SparkBatchContext =>
  import Parameters._

  override def outPut: Unit ={
    super.outPut
    logInfo("Start MatchtypeDataOutput...")
    val xfbTrainDataOut = rddContext(constant_feature_rdd).asInstanceOf[RDD[String]]
    xfbTrainDataOut.saveAsTextFile(batchContext(constant_train_out) + "/text")
    xfbTrainDataOut.saveAsObjectFile(batchContext(constant_train_out) + "/object")
  }
}
