package com.cmcm.ranking.matchtype.processor

import org.apache.spark.Logging
import org.apache.spark.rdd.RDD

/**
 * Created by mengchong on 5/24/16.
 */
trait Processor[T] extends Logging with Serializable{
//  def preprocess(line:String, batchContext: collection.mutable.Map[String,String]) : (String, String) = {
//    logInfo("Start preprocess each line...")
//    ("","")
//  }
//

  def process(inputRDD:RDD[String],batchContext: collection.mutable.Map[String,String]): RDD[T];
////    inputRDD.map((line:String) => preprocess(line, batchContext))
//  }
}
