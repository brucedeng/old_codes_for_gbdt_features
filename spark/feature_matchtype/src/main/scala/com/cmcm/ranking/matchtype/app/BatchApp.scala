package com.cmcm.ranking.matchtype.app

import java.io.File
import java.util.Map

import com.cmcm.ranking.matchtype.compute.EventBatchCompute
import com.cmcm.ranking.matchtype.input.EventBatchInput
import com.cmcm.ranking.matchtype.output.EventBatchOutput
import com.cmcm.ranking.matchtype.spark.SparkBatchContext
import com.cmcm.ranking.matchtype.config.ConfigUtil
import com.cmcm.ranking.matchtype.util.LoggingUtils
import com.typesafe.config.ConfigFactory
import org.apache.spark.{SparkContext, SparkConf}


/**
 * Created by mengchong on 5/23/16.
 */
trait BatchApp {
  this: SparkBatchContext
    with EventBatchInput
    with EventBatchCompute
    with EventBatchOutput =>

  def initApp(): Unit = {
    logInfo("Start init app...")
    init()
  }

  def processApp(): Unit ={
    logInfo("Start process app...")
    getInput
    compute
    outPut
  }

  def main(args:Array[String]) = {
//    val conf = new SparkConf().setAppName("")
//    val sc = new SparkContext(conf)
//    val sqlc = new org.apache.spark.sql.SQLContext(sc)

    val configPath = args(0)
    val confMap = ConfigUtil.parseConfig(configPath)
    batchContext ++= confMap

    var key = ""
    for (item <- args){
      if (key.isEmpty) key = item
      else{
        LoggingUtils.loggingInfo(s"argument: key($key), value($item)")
        batchContext += (key -> item)
        key = ""
      }
    }
    initApp()
    processApp()
  }
}
