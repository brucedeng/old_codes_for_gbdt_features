package com.cmcm.ranking.matchtype.processor

import com.cmcm.ranking.matchtype.util.{LoggingUtils, JsonUtil}
import org.apache.spark.rdd.RDD
import org.json4s.JsonAST._
import com.cmcm.ranking.matchtype.util.Parameters._

import scala.util.control.Breaks._
import scala.util.{Failure, Success, Try}

/**
 * Created by mengchong on 5/31/16.
 */
object EventLogProcessor extends Processor[events]{
  override  def process(inputRDD:RDD[String], batchContext: collection.mutable.Map[String,String]) = {
    inputRDD.map(x => preprocess(x) )
      .map(e => (e.uid+fieldDelimiter+e.content_id+fieldDelimiter+e.reqId+fieldDelimiter+e.pid+fieldDelimiter+e.channel,e))
      .reduceByKey((a,b) => a )
      .map{case (key,event) => event}
  }

  def preprocess(line:String):events = {
    Try(JsonUtil.convertToJValue(line)) match{
      case Success(jvalue) =>
        val uid = extractStr(jvalue,"aid")
        val app_lan = extractStr(jvalue,"app_lan")
        val cpack = jvalue \ "cpack"
        val exp = extractStr(jvalue\"upack","exp")
        val scenario = jvalue \ "scenario"
        val cid = extractStr(jvalue,"contentid")
        val channel = extractStr(scenario,"level2")
        val level1 = extractStr(scenario,"level1")
        val level1_type = extractStr(scenario,"level1_type")
        val des = extractStr(cpack,"des")
        val reqId = extractReqId(des)
        val pid = extractStr(jvalue,"pid")
        new events(uid,cid,app_lan,pid,channel,level1,level1_type,reqId,exp)
      case Failure(ex) =>
        LoggingUtils.loggingError(LoggingUtils.getException(ex))
        new events("","","","","","","","","")
    }
  }

  def extractReqId(des:String)= {
    var reqid = ""
    for (item <- des.split('|')) {
      val fields = item.split('=')
      if(fields.length==2 && fields(0)=="rid")
        reqid = fields(1)
    }
    reqid
  }

  def extractStr(jvalue: JValue, fieldName: String, default: String = "") = {
    jvalue \ fieldName match {
      case JString(s) => if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString()
      case _ => default
    }
  }

}
