package com.cmcm.ranking.matchtype.input

import com.cmcm.ranking.matchtype.spark.SparkBatchContext
import com.cmcm.ranking.matchtype.util.Parameters

/**
 * Created by mengchong on 5/23/16.
 */
trait MatchTypeDataInput extends EventBatchInput{
  this: SparkBatchContext =>
  import Parameters._

  override def getInput(): Unit ={
    logInfo("start XFBTrainDataInput...")
    setRDD(constant_xfb_input,constant_xfb_rdd,"text", this)
    setRDD(constant_featurelog_input, constant_featurelog_rdd,"text", this)
    setRDD(constant_cp_input,constant_cp_rdd,"text",this)
    setRDD(constant_event_log_input,constant_event_log_rdd,"text",this)
  }

}