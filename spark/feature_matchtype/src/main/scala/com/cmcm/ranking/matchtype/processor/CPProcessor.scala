package com.cmcm.ranking.matchtype.processor


import com.cmcm.ranking.matchtype.util.JsonUtil
import com.cmcm.ranking.matchtype.util.LoggingUtils
import com.cmcm.ranking.matchtype.util.Parameters._
import org.apache.spark.rdd.RDD
import org.json4s.JsonAST._

import scala.util.control.Breaks._
import scala.util.{Failure, Success, Try}

/**
 * Created by mengchong on 5/24/16.
 */
// schema is conid, title_md5
object CPProcessor extends Processor[(String,String,Long)]{
  override  def process(inputRDD:RDD[String], batchContext: collection.mutable.Map[String,String]): RDD[(String,String,Long)] = {
    inputRDD.map(x => preprocess(x) )
      .reduceByKey((a,b) => if(a._2 > b._2) a else b)
      .map(x => (x._1,x._2._1,x._2._3))
    // conid, titlemd5
  }

  def preprocess(line:String) = {
    Try(JsonUtil.convertToJValue(line)) match{
      case Success(jvalue) =>
        val cid = extractStr(jvalue,"item_id")
        val titleMd5 = extractStr(jvalue,"title_md5")
        val updateTime = extractStr(jvalue,"update_time") match {
          case x:String if x != "" => x
          case _ => "0"
        }
        val publishTime = extractStr(jvalue,"publish_time","0").toLong
        val channelIds = extractSeq(jvalue,"channel_ids")
        (cid,(titleMd5,updateTime.toLong,publishTime))
      case Failure(ex) =>
        LoggingUtils.loggingError(LoggingUtils.getException(ex))
        ("",("",0L,0L))
    }


  }

  def extractStr(jvalue: JValue, fieldName: String, default: String = "") = {
    jvalue \ fieldName match {
      case JString(s) => if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString()
      case _ => default
    }
  }

  def extractSeq(jvalue: JValue, fieldName: String, default: List[String] = List()) : List[String] = {
    jvalue \ fieldName match {
      case JArray(s) => s.map{
        case JString(v) => v;
        case JInt(v) => v.toString()
        case _ => ""
      }.filter(x => x!="")
      case _ => default
    }
  }

  def catKwAssist(jsonItems: List[JValue], version:String, top:Int, default:String): String = {
    if (jsonItems.isEmpty) default
    else {
      jsonItems.head \ "version" match {
        case JString(v) if v == version => {
          val sb = new StringBuilder
          val items = (jsonItems.head \ "list").asInstanceOf[JArray].arr
          var  index = 0
          breakable {
            for (item <- items) {
              if (index >= top)
                break()
              index += 1
              val name = (item \ "name").asInstanceOf[JString].s
              val L1_weight = (item \ "L1_weight") match {
                case JDouble(x) =>
                  x.toDouble
                case JInt(x) =>
                  x.toDouble
                case _ =>
                  0.0
              }
              sb.append(name + keyValueDelimiter + L1_weight + pairDelimiter)
            }
          }
          sb.toString()
        }
        case _ => catKwAssist(jsonItems.tail, version, top, default)
      }
    }
  }
}
