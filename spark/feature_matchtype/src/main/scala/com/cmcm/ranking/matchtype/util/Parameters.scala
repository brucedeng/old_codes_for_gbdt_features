package com.cmcm.ranking.matchtype.util

/**
 * Created by mengchong on 5/23/16.
 */
object Parameters {
  //pipeline parameters
  val constant_parallelism = "parallelism"
  val constant_date_time = "date_time"
  val constant_prev_date_time = "prev_data_time"
  val constant_sample_ratio = "sample_ratio"
  val constant_cfb_pv_threshold = "cfb_pv_threshold"
  val constant_cfb_offline_cutoff = "cfb_offline_cutoff"
  val constant_cfb_docage_cutoff = "docage_cutoff_sec"
  val constant_xfb_input = "xfb_input"
  val constant_xfb_rdd = "xfb_rdd"
  val constant_featurelog_input = "featurelog_input"
  val constant_featurelog_rdd = "featurelog_rdd"
  val constant_event_log_input = "event_log_input"
  val constant_event_log_rdd = "event_log_rdd"
  val constant_cp_input = "cp_input"
  val constant_cp_rdd = "cp_df"
  val constant_train_out = "train_output"
  val constant_train_rdd = "train_rdd"
  val constant_wnd = "wnd"
  val constant_u_ncat = "u_ncat"
  val constant_u_nkey = "u_nkey"
  val constant_d_ncat = "d_ncat"
  val constant_d_nkey = "d_nkey"
  val constant_u_rel_ncat = "rel_u_ncat"
  val constant_u_rel_nkey = "rel_u_nkey"
  val constant_d_rel_ncat = "rel_d_ncat"
  val constant_d_rel_nkey = "rel_d_nkey"
  val constant_cat_version = "cat_version"
  val constant_kw_version = "kw_version"
  val constant_decay_factor = "decay_factor"
  val constant_decay_wind = "decay_wind"
  val constant_interval = "decay_interval"
  val constant_pid = "pid"
  val constant_random_num = "random_num"
  val constant_top_num = "top_num"
  val constant_xfb_start_ts = "cfb_start_ts"
  val constant_xfb_end_ts = "cfb_end_ts"
  val constant_feature_rdd = "features_result"


  //process parameters
  val fieldDelimiter = "\003"
  val pairDelimiter = "\001"
  val keyValueDelimiter = "\002"
  val recordDelimiter = "\004"
  val constant_expired_time = "expired_time"

  // constant values that dont need config
  val decay_factor = 0.995
  val decay_window = 600
  val aggregate_interval = "2d"
}

