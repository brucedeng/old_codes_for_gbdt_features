package com.cmcm.ranking.matchtype.compute

import com.cmcm.ranking.matchtype.component.BatchComponent
import com.cmcm.ranking.matchtype.spark.SparkBatchContext

/**
 * Created by mengchong on 5/23/16.
 */
trait EventBatchCompute extends BatchComponent{
  this:SparkBatchContext =>
  def compute = {
    logInfo("Start EventBatchCompute...")
  }
}
