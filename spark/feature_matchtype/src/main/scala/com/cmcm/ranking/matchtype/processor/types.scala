package com.cmcm.ranking.matchtype.processor

/**
 * Created by mengchong on 5/24/16.
 */
case class gmp ( conid:String, batchId:Long, rawPv:Double, rawClk:Double, aggPv:Double, aggClk:Double, ts:Long ) extends Serializable {
  def this(fields:Array[String]) {
    this(fields(0),fields(1).toLong,fields(2).toDouble,fields(3).toDouble,fields(4).toDouble,fields(5).toDouble,fields(6).toLong)
  }

  def this(line:String) {
    this(line.split('\t'))
  }
}

case class xfb( ts:Long, featureType:String, featureName:String, rawClk:Double, rawPv:Double ) extends Serializable {
  def this(fields:Array[String]) {
    this(fields(0).toLong,fields(1),fields(2),fields(3).toDouble,fields(4).toDouble)
  }
  def this(line:String) {
    this(line.replace('\001','\002').split('\t'))
  }

  override def toString = ts+"\t"+featureType+"\t"+featureName+"\t"+rawClk+"\t"+rawPv
}

case class featurelog(req_id:String,prodId:String,predictor:String,ts:Long,uid:String,up:upData,articles:Seq[articleFeature] ) extends Serializable
case class articleFeature ( item_id:String, features:Map[String,Double]) extends Serializable
case class upData(upMap:Map[String,Any]) extends Serializable {
  def get[T](key:String,default:T):T = {
    upMap.get(key) match {
      case Some(v) => v.asInstanceOf[T]
      case None => default
    }
  }
}

case class cpData(item_id:String,cpMap:Map[String,Any]) extends Serializable {
  def get[T](key:String,default:T = null):T = cpMap.get(key) match {
    case Some(v) => v.asInstanceOf[T]
    case None => default
  }
}

case class events(uid:String,content_id:String,app_lan:String,pid:String,channel:String,level1:String,level1_type:String,reqId:String,exp:String)