package com.cmcm.ranking.matchtype.feature

import com.cmcm.ranking.matchtype.util.Parameters._

/**
 * Created by mengchong on 5/26/16.
 */
object parseCrossCfbKeys {

  case class cfbFeatureValue(aggClk:Double,aggPv:Double)
  case class cfbCrossData(ftype:String,fname:String,value:cfbFeatureValue)

  def genCfbCrossMap(cfb:Seq[cfbCrossData]) = {
    cfb.groupBy(x => {
      val cross = x.fname.split(keyValueDelimiter)
      val cpKey = cross(0)
      x.ftype + fieldDelimiter + cpKey
    })
    .map ({ case (key, data) =>
        val dataMap = data.map(x => {
          val cross = x.fname.split(keyValueDelimiter)
          val upKey = cross(1)
          (upKey, cfbFeatureValue)
        }).toMap
      (key, dataMap)
    })
  }

  def calcSingleFeature(cfbMap:Map[String,cfbFeatureValue],upCat:Seq[(String,Double)]) = {
    val avgAggtmp = for(uCat <- upCat) yield {
      cfbMap.get(uCat._1) match {
        case Some(cfbValue) => (uCat._2 * cfbValue.aggClk, uCat._2 * cfbValue.aggPv)
        case None => (0.0,0.0)
      }
    }

    val avgAgg = avgAggtmp.reduce((a,b) => (a._1 + b._1,a._2+b._2))
//    val mergedContents = cfbMap.map(x => x._2.conids).reduce((a,b) => a++b)
    if (avgAgg._2 == 0) 0.0
    else avgAgg._1/avgAgg._2
  }
}
