package com.cmcm.ranking.matchtype.app

import com.cmcm.ranking.matchtype.compute.MatchTypeComputeSingle
import com.cmcm.ranking.matchtype.input.MatchTypeDataInput
import com.cmcm.ranking.matchtype.output.MatchTypeDataOutput
import com.cmcm.ranking.matchtype.spark.TrainDataSparkContext

/**
  * Created by mengchong on 6/14/16.
  */
object ConidGenderMatchType  extends BatchApp
  with TrainDataSparkContext
  with MatchTypeDataInput
  with MatchTypeComputeSingle
  with MatchTypeDataOutput
