package com.cmcm.ranking.matchtype.app

import scala.io.Source
import com.cmcm.ranking.matchtype.config.ConfigUtil
import org.scalatest.FunSuite
/**
 * Created by mengchong on 5/25/16.
 */
class testMain extends FunSuite{
  test("config file parsed correctly" ) {
    val source = getClass.getResource("/test.conf").getPath
    val confMap = ConfigUtil.parseConfig(source)
    val batchContext:scala.collection.mutable.Map[String,String] = scala.collection.mutable.Map[String, String]()
    batchContext+=("test"->"testout")
    batchContext++=confMap
    assert(batchContext("app_name")=="stat_matchtype")
    assert(batchContext("test")=="testout")
  }
}
