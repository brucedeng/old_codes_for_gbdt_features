package com.cmcm.ranking.matchtype.app

import com.cmcm.ranking.matchtype.feature.parseCrossCfbKeys.{cfbFeatureValue,calcSingleFeature}
import org.scalatest.FunSuite

/**
  * Created by mengchong on 6/2/16.
  */
class testCrossFeature extends FunSuite{
  test("calculate cross feature" ) {
    val cfbMap = Map( "1000637" -> cfbFeatureValue(0.04732858643505219,0.09184593040866834), "1000031" -> cfbFeatureValue(0.0,0.0700263682906079), "1000620" -> cfbFeatureValue(0.03781138376977623,0.0), "1000374" -> cfbFeatureValue(0.03420622099215131,0.0), "1000661" -> cfbFeatureValue(0.027362224579952056,0.0), "1000992" -> cfbFeatureValue(0.0,0.08425069856785151), "1001039" -> cfbFeatureValue(0.0,0.028293739496311362), "6000007" -> cfbFeatureValue(0.053236592905972796,0.018439998769572856))
    val up:Seq[(String,Double)] = List(("1000288",0.090409), ("1000298",0.178926), ("1000395",0.064254), ("1000661",0.557876), ("1000780",0.091984), ("6000007",0.016551))
    val result = calcSingleFeature(cfbMap,up)
//    println(result)
    assert(Math.abs(result-52.9024) < 0.001)
  }
}
//  val inputMap = Map(1000637 -> cfbFeatureValue(0.04732858643505219,0.               09184593040866834), 1000031 -> cfbFeatureValue(0.0,0.0700263682906079), 1000620 -> cfbFeatureValue(0.03781138376977623,0.0),           1000374 -> cfbFeatureValue(0.03420622099215131,0.0), 1000661 -> cfbFeatureValue(0.027362224579952056,0.0), 1000992 ->                  cfbFeatureValue(0.0,0.08425069856785151), 1001039 -> cfbFeatureValue(0.0,0.028293739496311362), 6000007 -> cfbFeatureValue(0.          053236592905972796,0.018439998769572856))

