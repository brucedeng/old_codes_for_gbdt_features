package com.cmcm.ranking.matchtype.processor

import org.scalatest.FunSuite

/**
 * Created by mengchong on 5/31/16.
 */
class testEventLogProcessor extends FunSuite{
  test("parse event data" ) {
    val event = """{"servertime_sec":"1464681925","upack":{"exp":"insta_060"},"model":"SM-J500F","mcc":"404","aid":"12aa45724d6839a6","nmnc":"02","net":"3G","ext":{"eventtime":"1464681924","requesttime":"1464681922"},"id":"","refer":{},"lan":"en","action":"","value":"","mnc":"02","apiv":"3","contentid":"14820946","osv":"5.1.1","ctype":"1","pid":"11","display":"0x02","api_server_ip":"10.2.8.101","ip":"223.225.254.137, 107.167.111.43","appv":"1.2.9","pf":"android","scenario":{"level2":"29","level1":"1","level1_type":"1","source":"","column":"","is_level1_native":"1"},"servertime":"2016-05-31 08:05:25","app_lan":"en","cpack":{"des":"rid=1e6dd2a2|src=8|ord=22","md5":"14820946","ishot":1},"brand":"samsung","nmcc":"404","uuid":"","ch":"200000","act":"1","idfa":""}"""
    val parsed = EventLogProcessor.preprocess(event)
    assert(parsed.uid == "12aa45724d6839a6")
    assert(parsed.content_id=="14820946")
    assert(parsed.exp == "insta_060")
    assert(parsed.reqId=="1e6dd2a2")
  }
}
