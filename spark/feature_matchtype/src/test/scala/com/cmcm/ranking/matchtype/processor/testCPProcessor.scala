package com.cmcm.ranking.matchtype.processor

import org.scalatest.FunSuite
import com.cmcm.ranking.matchtype.util.Parameters._

/**
 * Created by mengchong on 5/31/16.
 */
class testCPProcessor extends FunSuite{
  test("parse cp data" ) {
    val cp = """{"content_length": 1284, "image_md5": "fdb968e3", "is_include_video": false, "word_count": 1132, "thirdads": "0", "is_servable": true, "has_copyright": 0, "images": ["http://img.store.ksmobile.net/cmnow/20160530/00/25759_fdb968e3_1464566665162_200_140.jpg"], "head_image": "", "editorial_item_level": 0, "cp_id": "", "l2_categories": [{"name": "1000667", "weight": 0.6141089038529011}, {"name": "1000681", "weight": 0.5074004546145627}], "fetch_time": 1464566663, "copyright": "", "author": "", "entities": [{"L1_weight": 0.22215713072881435, "name": "commits_suicide", "weight": 0.6157737472120189}, {"L1_weight": 0.1926219688597083, "name": "chandigarh", "weight": 0.5339083700396171}, {"L1_weight": 0.11436667951383499, "name": "beant_singh", "weight": 0.31700084786563204}, {"L1_weight": 0.09318766478905073, "name": "government_house", "weight": 0.2582969871497742}, {"L1_weight": 0.07900693319071693, "name": "father", "weight": 0.2189909238878521}, {"L1_weight": 0.0786386024998511, "name": "coma", "weight": 0.21796998717975022}, {"L1_weight": 0.06716378370871133, "name": "grandson", "weight": 0.18616415613386622}, {"L1_weight": 0.06611535901041346, "name": "village", "weight": 0.18325813910428645}, {"L1_weight": 0.008471605889913704, "name": "family", "weight": 0.023481544286343116}, {"L1_weight": 0.007919109853614981, "name": "security", "weight": 0.0219501392241903}, {"L1_weight": 0.007734944508182077, "name": "death", "weight": 0.021439670870139368}, {"L1_weight": 0.007734944508182077, "name": "police", "weight": 0.021439670870139368}, {"L1_weight": 0.007182448471883357, "name": "wife", "weight": 0.019908265807986554}, {"L1_weight": 0.007182448471883357, "name": "brother", "weight": 0.019908265807986554}, {"L1_weight": 0.006998283126450449, "name": "head", "weight": 0.019397797453935616}, {"L1_weight": 0.006998283126450449, "name": "cms", "weight": 0.019397797453935616}, {"L1_weight": 0.006814117781017544, "name": "accident", "weight": 0.018887329099884678}, {"L1_weight": 0.006629952435584636, "name": "punjab", "weight": 0.01837686074583374}, {"L1_weight": 0.006629952435584636, "name": "treatment", "weight": 0.01837686074583374}, {"L1_weight": 0.006445787090151731, "name": "shooting", "weight": 0.017866392391782806}], "adult_score": 0.0, "title_md5": "762d3049", "channel_names": ["society", "hot", "politics"], "content_md5": "d002fb07", "opencms_id": "0", "type": "article", "title_entities": [{"L1_weight": 0.4673913043478261, "name": "commits_suicide", "weight": 0.7469511556775412}, {"L1_weight": 0.391304347826087, "name": "chandigarh", "weight": 0.6253544559160811}, {"L1_weight": 0.14130434782608697, "name": "grandson", "weight": 0.2258224424141404}], "location": [{"pcode": 11, "cname": "chandigarh", "pname": "punjab", "ccode": 1014}], "update_time": 1464566686, "publish_time": 1464566663, "large_image": "", "body_images": ["http://img.store.ksmobile.net/cmnow/20160530/00/25759_fdb968e3_1464566664130_s.jpg"], "channel_ids": [2, 29, 1], "cp_version": 13, "discovery_time": 1464566663, "source_type": 0, "link": "http://timesofindia.indiatimes.com/city/chandigarh/Beants-grandson-commits-suicide-in-Chandigarh/articleshow/52496765.cms", "display_type": 0, "item_id": "14715959", "source_feed": ["http://timesofindia.indiatimes.com"], "categories": [{"name": "1000780", "weight": 0.5531625532970205}, {"name": "1000661", "weight": 0.40988293032443707}], "publisher": "TOI", "feature_scores": {"newsy_score": 0.0, "adult_score": 0.0}, "language": "en", "editorial_importance_level": 0, "region": "in", "title": "Beant\u2019s grandson \u2018commits suicide\u2019 in Chandigarh", "last_modified_time": 1464566663, "summary": "Harkirat Singh (41), grandson of former Punjab chief minister Beant Singh, allegedly committed suicide in Chandigarh on Sunday by shooting himself in his head with his licensed pistol. Sarpanch from the family\u2019s home village Kotli, he was suffering from acute depression, police said.", "cp_disable": true, "newsy_score": 0, "group_id": "684398", "image_count": 1}"""

    val parsed = CPProcessor.preprocess(cp)
    println(parsed)
    assert(parsed._1.equals("14715959"))
    assert(parsed._2._1 equals "762d3049")
    assert(parsed._2._2 == 1464566686)
    assert(parsed._2._3 == 1464566663)
  }

  test("parse xfb data" ) {
    val xfbRaw = "1464566400\tC_D_U_KEYWORD_CATEGORY\ttest\0011000637\t0.0\t8.668289476636195E-4\thi_sp"
    val parsed = new xfb(xfbRaw)
    assert(parsed.ts == 1464566400)
    assert(parsed.featureType == "C_D_U_KEYWORD_CATEGORY")
//    assert(parsed.featureName.split(pairDelimiter).length == 2)
  }
}
