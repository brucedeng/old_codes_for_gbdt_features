package com.cmcm.ranking.matchtype.processor

import org.scalatest.FunSuite

/**
 * Created by mengchong on 5/25/16.
 */
class testFeaturelogProcessor extends FunSuite{
  test("parse featurelog data" ) {
    val source = getClass.getResourceAsStream("/featurelog_data.txt")
    val lines = scala.io.Source.fromInputStream( source ).getLines()
    val test_data = lines.next()
    val parsed = FeaturelogProcessor.preprocess(test_data)
    assert(parsed.req_id == "11c974e9")
    assert(parsed.uid == "2d4b15166544c36a")
    assert(parsed.prodId == "11")
    assert(parsed.ts == 1463958016)
    assert(parsed.articles.nonEmpty)
    assert(parsed.up.upMap.nonEmpty)
  }
}
