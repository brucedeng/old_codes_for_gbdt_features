package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.EventBatchCompute
import com.cmcm.cmnews.model.input.EventBatchInput
import com.cmcm.cmnews.model.output.EventBatchOutput
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.LoggingUtils

/**
 * Created by tangdong on 27/4/16.
 */
trait BatchApp {
  this: SparkBatchContext
    with EventBatchInput
    with EventBatchCompute
    with EventBatchOutput =>

  def initApp(): Unit = {
    logInfo("Start init app...")
    init()
  }

  def processApp(): Unit ={
    logInfo("Start process app...")
    getInput
    compute
    outPut
  }

  def main(args:Array[String]) = {
    var key = ""
    for (item <- args){
      if (key.isEmpty) key = item
      else{
        LoggingUtils.loggingInfo(s"argument: key($key), value($item)")
        batchContext += (key -> item)
        key = ""
      }
    }
    initApp()
    processApp()
  }
}
