package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{JsonUtil, LoggingUtils, Parameters}
import com.cmcm.cmnews.model.util.Parameters._
import org.apache.spark.rdd.RDD
import org.json4s.JsonAST._

import scala.reflect.internal.Trees
import scala.util.{Failure, Success, Try}
import scala.util.control.Breaks._
import scala.util.Try
/**
  * Created by tangdong on 27/4/16.
  */
object UPProcessor extends Processor {
  this: SparkBatchContext =>

  import Parameters._

  override def process(inputRDD:RDD[String], batchContext: collection.mutable.Map[String,String]): RDD[(String, String)] = {
    val parsedUp = inputRDD.map((line:String) => preprocessup(line, batchContext))
    val dedupUp = batchContext.getOrElse(constant_dedup_up,"no")
    if(dedupUp == "yes" || dedupUp == "true"){
      parsedUp.reduceByKey((a,b)=> if(a._1>b._1) a else b).map(x => (x._1, x._2._2))
    }else parsedUp.map(x => (x._1, x._2._2))
  }

  def preprocessup(line: String, batchContext: collection.mutable.Map[String, String]) = {
    val topRelCat = Try(batchContext(constant_u_rel_ncat)).getOrElse("100").toInt
    val topRelKw = Try(batchContext(constant_u_rel_nkey)).getOrElse("100").toInt
    /*val bg = line.trim.indexOf("\"neg_categories\":\"")

    var ltr = line

    if (bg >= 0) {
      val end = line.trim.indexOf("}\"",bg)
      ltr = line.trim.substring(0,bg - 1) + line.trim.substring(end + 2,line.trim.length)
    }*/

    Try(JsonUtil.convertToJValue(line)) match {
      case Success(jvalue) =>
        val uuid = extractStr(jvalue, "uid", extractStr(jvalue, "aid", "unknown"))
        val categories = jvalue \ "categories" match {
          case JArray(cats) => catKwAssist(cats.toList, "", topRelCat, "unknown")
          case _ => "unknown"
        }
        val keywords = jvalue \ "keywords" match {
          case JArray(cats) => catKwAssist(cats.toList, "", topRelKw, "unknown")
          case _ => "unknown"
        }
        val gender = extractStr(jvalue, "gender", "unknown")
        val age = extractStr(jvalue, "age", "unknown")
        val catLen = extractStr(jvalue, "u_cat_len", "0")
        val kwLen = extractStr(jvalue, "u_kw_len", "0")
        val ts = extractLong(jvalue,"date_time",0L)

        (uuid, (ts, categories + fieldDelimiter + keywords + fieldDelimiter + gender + fieldDelimiter + age + fieldDelimiter + catLen + fieldDelimiter + kwLen))
      case Failure(ex) =>
        LoggingUtils.loggingError(LoggingUtils.getException(ex))
        ("", (-1L,"unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "0" + fieldDelimiter + "0"))

    }

    //    try {
    //      val uuid = getValueForKey("uid", line)
    //      val categories = getListFromJson("categories",line, topRelCat) match {
    //        case x:String if x != "" => x
    //        case _ => "unknown"
    //      }
    //      val keywords = getListFromJson("keywords", line, topRelKw) match {
    //        case x:String if x != "" => x
    //        case _ => "unknown"
    //      }
    //      val gender = getValueForKey("gender", line) match {
    //        case x:String if x != "" => x
    //        case _ => "unknown"
    //      }
    //      val age = getValueForKey("age", line) match {
    //        case x:String if x!= "" => x
    //        case _ => "unknown"
    //      }
    //      val u_cat_len = getValueForKey("u_cat_len", line) match {
    //        case x:String if x!= "" => x
    //        case _ => "0"
    //      }
    //      val u_key_len = getValueForKey("u_kw_len", line) match {
    //        case x:String if x!= "" => x
    //        case _ => "0"
    //      }
    //      (uuid, categories + fieldDelimiter + keywords + fieldDelimiter + gender + fieldDelimiter + age + fieldDelimiter + u_cat_len + fieldDelimiter + u_key_len)
    //    }catch {
    //      case e : Exception =>
    //        ("","unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter  + "0" + fieldDelimiter + "0")
    //    }
  }

  def catKwAssist(jsonItems: List[JValue], version: String, top: Int, default: String): String = {
    def parse_item(item: JValue) = {
      val name = (item \ "name").asInstanceOf[JString].s
      val L1_weight = item \ "weight" match {
        case JDouble(x) =>
          x.toDouble
        case JInt(x) =>
          x.toDouble
        case _ =>
          0.0
      }
      name + keyValueDelimiter + L1_weight
    }
    jsonItems.map(x => parse_item(x)).take(top).mkString(pairDelimiter)
    //     sb.append(name + keyValueDelimiter + L1_weight + pairDelimiter)
  }

  def extractStr(jvalue: JValue, fieldName: String, default: String = "") = {
    jvalue \ fieldName match {
      case JString(s) => if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString()
      case _ => default
    }
  }

  def extractLong(jvalue: JValue, fieldName: String, default: Long = 0) : Long = {
    jvalue \ fieldName match {
      case JString(s) => if (s.trim.isEmpty) default else Try( s.toLong ).getOrElse(default)
      case JInt(s) => s.toLong
      case _ => default
    }
  }

  def getListFromJson(key: String, line: String, top: Int): String = {
    val sb = new StringBuilder
    var newLine = line.substring(line.indexOf("\"" + key + "\" : ") + 5 + key.length)
    newLine = newLine.substring(0, newLine.indexOf("]") + 1)
    var index = 0
    breakable {
      while (index < (if (top < 0) Integer.MAX_VALUE else top)) {
        if (newLine.indexOf("}") == -1) {
          break()
        }
        val name = getValueForKey("name", newLine)
        val weight = getValueForKey("weight", newLine).toDouble
        sb.append(name + keyValueDelimiter + weight + pairDelimiter)
        newLine = newLine.substring(newLine.indexOf("}") + 1)
        index += 1
      }
    }
    if (index > 0)
      sb.dropRight(1).toString()
    else
      sb.toString()
  }

  def getValueForKey(key: String, record: String) = {
    val startIndex = record.indexOf("\"" + key + "\" : ") + 5 + key.length
    val commaIndex = record.indexOf(",", startIndex)
    val antiBracketIndex = record.indexOf("}", startIndex)
    val endIndex = if (commaIndex != -1 && antiBracketIndex != -1) Math.min(record.indexOf(",", startIndex),
      record.indexOf("}", startIndex))
    else if (commaIndex != -1) commaIndex else antiBracketIndex
    val mendedStartInedex =
      if (record.charAt(startIndex) == '\"' || record.charAt(startIndex) == '[')
        startIndex + 1
      else
        startIndex
    val mendedEndInedex =
      if (record.charAt(endIndex - 1) == '\"' || record.charAt(endIndex - 1) == ']')
        endIndex - 1
      else
        endIndex

    if (mendedStartInedex > mendedEndInedex)
      ""
    else
      record.substring(mendedStartInedex, mendedEndInedex)
  }

  def main(args:Array[String]) = {
    val key = "{\"aid\":\"567738DD-2F5E-491A-A8C5-A13D23C5FB9E\",\"uid\":\"567738DD-2F5E-491A-A8C5-A13D23C5FB9E\",\"version\":\"104\",\"gender\":\"\",\"age\":\"\",\"u_cat_len\":22,\"u_kw_len\":5466,\"categories\":[{\"name\":\"1000028\",\"weight\":0.160144},{\"name\":\"1000922\",\"weight\":0.121802},{\"name\":\"6000007\",\"weight\":0.115555},{\"name\":\"1000323\",\"weight\":0.101347},{\"name\":\"1000707\",\"weight\":0.068718},{\"name\":\"1000299\",\"weight\":0.068550},{\"name\":\"1000930\",\"weight\":0.047343},{\"name\":\"1001117\",\"weight\":0.045212},{\"name\":\"1000992\",\"weight\":0.041422},{\"name\":\"1000713\",\"weight\":0.041181},{\"name\":\"1000849\",\"weight\":0.040873},{\"name\":\"1000802\",\"weight\":0.038363},{\"name\":\"1000979\",\"weight\":0.022069},{\"name\":\"1000798\",\"weight\":0.019345},{\"name\":\"1000667\",\"weight\":0.015249},{\"name\":\"1000031\",\"weight\":0.012236},{\"name\":\"1001039\",\"weight\":0.010667},{\"name\":\"1000780\",\"weight\":0.010556},{\"name\":\"1000073\",\"weight\":0.008431},{\"name\":\"1000069\",\"weight\":0.006201},{\"name\":\"1000393\",\"weight\":0.004228},{\"name\":\"1000742\",\"weight\":0.000508}],\"keywords\":[{\"name\":\"पत्नी\",\"weight\":0.009350},{\"name\":\"मौत\",\"weight\":0.009187},{\"name\":\"महिला\",\"weight\":0.008703},{\"name\":\"शादी\",\"weight\":0.008396},{\"name\":\"बॉलीवुड\",\"weight\":0.008375},{\"name\":\"पुलिस\",\"weight\":0.008334},{\"name\":\"दिल्ली\",\"weight\":0.008166},{\"name\":\"चीन\",\"weight\":0.008107},{\"name\":\"वीडियो\",\"weight\":0.008007},{\"name\":\"तुर्की\",\"weight\":0.007983},{\"name\":\"पति\",\"weight\":0.007960},{\"name\":\"लड़की\",\"weight\":0.007791},{\"name\":\"समय\",\"weight\":0.007755},{\"name\":\"तस्वीरें\",\"weight\":0.007558},{\"name\":\"पाकिस्तान\",\"weight\":0.007411},{\"name\":\"भारत\",\"weight\":0.007241},{\"name\":\"देखें\",\"weight\":0.007232},{\"name\":\"फोटो\",\"weight\":0.007131},{\"name\":\"रेप\",\"weight\":0.007099},{\"name\":\"अमेरिका\",\"weight\":0.006907},{\"name\":\"देश\",\"weight\":0.006664},{\"name\":\"फ्रांस\",\"weight\":0.006605},{\"name\":\"जानकारी\",\"weight\":0.006605},{\"name\":\"दुनिया\",\"weight\":0.006563},{\"name\":\"सोशल_मीडिया\",\"weight\":0.006548},{\"name\":\"प्यार\",\"weight\":0.006542},{\"name\":\"लड़कियां\",\"weight\":0.006542},{\"name\":\"शहर\",\"weight\":0.006425},{\"name\":\"कोशिश\",\"weight\":0.006276},{\"name\":\"शेयर\",\"weight\":0.006206},{\"name\":\"सीरिया\",\"weight\":0.006110},{\"name\":\"बेटी\",\"weight\":0.006095},{\"name\":\"संबंध\",\"weight\":0.006077},{\"name\":\"हत्या\",\"weight\":0.006072},{\"name\":\"महिलाएं\",\"weight\":0.006069},{\"name\":\"अनुसार\",\"weight\":0.005835},{\"name\":\"जिंदगी\",\"weight\":0.005833},{\"name\":\"कार\",\"weight\":0.005813},{\"name\":\"गर्लफ्रेंड\",\"weight\":0.005811},{\"name\":\"वायरल\",\"weight\":0.005746},{\"name\":\"क्रिकेट\",\"weight\":0.005734},{\"name\":\"नजर\",\"weight\":0.005709},{\"name\":\"हैरान\",\"weight\":0.005695},{\"name\":\"देखे\",\"weight\":0.005686},{\"name\":\"लड़की\",\"weight\":0.005646},{\"name\":\"फेसबुक\",\"weight\":0.005620},{\"name\":\"बंद\",\"weight\":0.005619},{\"name\":\"सुबह\",\"weight\":0.005617},{\"name\":\"विराट_कोहली\",\"weight\":0.005606},{\"name\":\"सेक्स\",\"weight\":0.005585},{\"name\":\"जर्मनी\",\"weight\":0.005547},{\"name\":\"फिल्म\",\"weight\":0.005517},{\"name\":\"सुरक्षा\",\"weight\":0.005505},{\"name\":\"दिल\",\"weight\":0.005493},{\"name\":\"गिरफ्तार\",\"weight\":0.005488},{\"name\":\"लड़कियों\",\"weight\":0.005459},{\"name\":\"ट्रेन\",\"weight\":0.005459},{\"name\":\"हमला\",\"weight\":0.005405},{\"name\":\"रोनाल्डो\",\"weight\":0.005384},{\"name\":\"शराब\",\"weight\":0.005382},{\"name\":\"दोस्त\",\"weight\":0.005371},{\"name\":\"राज\",\"weight\":0.005336},{\"name\":\"कहानी\",\"weight\":0.005316},{\"name\":\"सोना\",\"weight\":0.005315},{\"name\":\"इंटरनेट\",\"weight\":0.005291},{\"name\":\"कंदील\",\"weight\":0.005276},{\"name\":\"संग\",\"weight\":0.005240},{\"name\":\"मां\",\"weight\":0.005236},{\"name\":\"तस्वीर\",\"weight\":0.005231},{\"name\":\"पाक\",\"weight\":0.005218},{\"name\":\"सरकार\",\"weight\":0.005209},{\"name\":\"करती_हैं\",\"weight\":0.005162},{\"name\":\"अनुष्का_शर्मा\",\"weight\":0.005146},{\"name\":\"इतिहास\",\"weight\":0.005133},{\"name\":\"सलमान_खान\",\"weight\":0.005107},{\"name\":\"सूचना\",\"weight\":0.005085},{\"name\":\"यहाँ\",\"weight\":0.005068},{\"name\":\"राजस्थान\",\"weight\":0.005068},{\"name\":\"सलमान\",\"weight\":0.005025},{\"name\":\"रिपोर्ट\",\"weight\":0.005006},{\"name\":\"पुरुष\",\"weight\":0.004979},{\"name\":\"मुस्लिम\",\"weight\":0.004976},{\"name\":\"मस्ती\",\"weight\":0.004938},{\"name\":\"बराक_ओबामा\",\"weight\":0.004925},{\"name\":\"राष्ट्रपति\",\"weight\":0.004906},{\"name\":\"खूबसूरत\",\"weight\":0.004843},{\"name\":\"पढ़ें\",\"weight\":0.004835},{\"name\":\"आईएस\",\"weight\":0.004811},{\"name\":\"अभिनेत्री\",\"weight\":0.004787},{\"name\":\"मार_डाला\",\"weight\":0.004784},{\"name\":\"ममता_कुलकर्णी\",\"weight\":0.004775},{\"name\":\"देखिए\",\"weight\":0.004760},{\"name\":\"होश\",\"weight\":0.004741},{\"name\":\"लड़कियां\",\"weight\":0.004717},{\"name\":\"हैरान_रह\",\"weight\":0.004693},{\"name\":\"धोनी\",\"weight\":0.004665},{\"name\":\"अवैध_संबंध\",\"weight\":0.004648},{\"name\":\"हॉट\",\"weight\":0.004617},{\"name\":\"किम_कार्दशियन\",\"weight\":0.004616},{\"name\":\"हाथ\",\"weight\":0.004607},{\"name\":\"मीडिया\",\"weight\":0.004593},{\"name\":\"रात_में\",\"weight\":0.004568},{\"name\":\"कश्मीर\",\"weight\":0.004560},{\"name\":\"कपड़े\",\"weight\":0.004490},{\"name\":\"जाते_हैं\",\"weight\":0.004479},{\"name\":\"बम\",\"weight\":0.004479},{\"name\":\"करीब\",\"weight\":0.004476},{\"name\":\"पानी\",\"weight\":0.004475},{\"name\":\"हमले\",\"weight\":0.004475},{\"name\":\"फोटोज\",\"weight\":0.004419},{\"name\":\"इस्लामिक_स्टेट\",\"weight\":0.004359},{\"name\":\"भोपाल\",\"weight\":0.004356},{\"name\":\"बहू\",\"weight\":0.004351},{\"name\":\"जीवन\",\"weight\":0.004338},{\"name\":\"शादी_से_पहले\",\"weight\":0.004334},{\"name\":\"खुलासा\",\"weight\":0.004328},{\"name\":\"शरीर\",\"weight\":0.004327},{\"name\":\"विडियो\",\"weight\":0.004303},{\"name\":\"मन\",\"weight\":0.004300},{\"name\":\"विमान\",\"weight\":0.004296},{\"name\":\"मदद\",\"weight\":0.004286},{\"name\":\"तलाक\",\"weight\":0.004284},{\"name\":\"मर्दानगी\",\"weight\":0.004281},{\"name\":\"सऊदी_अरब\",\"weight\":0.004277},{\"name\":\"बेटे\",\"weight\":0.004273},{\"name\":\"शादी_के_बाद\",\"weight\":0.004271},{\"name\":\"सिंह\",\"weight\":0.004265},{\"name\":\"चौंक_जाएंगे\",\"weight\":0.004250},{\"name\":\"ईद\",\"weight\":0.004242},{\"name\":\"रोमांस\",\"weight\":0.004240},{\"name\":\"बच्चों\",\"weight\":0.004240},{\"name\":\"मार\",\"weight\":0.004239},{\"name\":\"मर्द\",\"weight\":0.004226},{\"name\":\"नौकरी\",\"weight\":0.004223},{\"name\":\"पूनम_पांडे\",\"weight\":0.004217},{\"name\":\"सोने\",\"weight\":0.004217},{\"name\":\"उत्तर_प्रदेश\",\"weight\":0.004212},{\"name\":\"मंदिर\",\"weight\":0.004210},{\"name\":\"बोले\",\"weight\":0.004208},{\"name\":\"हवस\",\"weight\":0.004178},{\"name\":\"उम्मीद\",\"weight\":0.004171},{\"name\":\"देहरादून\",\"weight\":0.004149},{\"name\":\"देखिये\",\"weight\":0.004149},{\"name\":\"सोच\",\"weight\":0.004148},{\"name\":\"घर_में\",\"weight\":0.004144},{\"name\":\"बीवी\",\"weight\":0.004134},{\"name\":\"लड़कियों\",\"weight\":0.004124},{\"name\":\"क्रिकेटर\",\"weight\":0.004096},{\"name\":\"जो_रूट\",\"weight\":0.004087},{\"name\":\"मरे\",\"weight\":0.004068},{\"name\":\"जी\",\"weight\":0.004067},{\"name\":\"गन्दा_काम\",\"weight\":0.004064},{\"name\":\"जबलपुर\",\"weight\":0.004062},{\"name\":\"बिकिनी\",\"weight\":0.004052},{\"name\":\"गेम\",\"weight\":0.004040},{\"name\":\"बेटा\",\"weight\":0.004031},{\"name\":\"अक्षय_कुमार\",\"weight\":0.003999},{\"name\":\"बहन\",\"weight\":0.003993},{\"name\":\"दुबई\",\"weight\":0.003993},{\"name\":\"छात्र\",\"weight\":0.003989},{\"name\":\"कपिल_शर्मा\",\"weight\":0.003983},{\"name\":\"मर्डर\",\"weight\":0.003980},{\"name\":\"तख्तापलट\",\"weight\":0.003979},{\"name\":\"रेड_लाइट_एरिया\",\"weight\":0.003975},{\"name\":\"इस्लाम\",\"weight\":0.003938},{\"name\":\"हार\",\"weight\":0.003931},{\"name\":\"अपनाएं_ये\",\"weight\":0.003927},{\"name\":\"धोखा\",\"weight\":0.003907},{\"name\":\"देती_है\",\"weight\":0.003891},{\"name\":\"घटना\",\"weight\":0.003886},{\"name\":\"रहता_है\",\"weight\":0.003873},{\"name\":\"आरोप\",\"weight\":0.003873},{\"name\":\"तस्वीरों_में\",\"weight\":0.003854},{\"name\":\"तीसरा\",\"weight\":0.003840},{\"name\":\"फोन\",\"weight\":0.003830},{\"name\":\"बारिश\",\"weight\":0.003826},{\"name\":\"समर्थन\",\"weight\":0.003822},{\"name\":\"सैनिक\",\"weight\":0.003772},{\"name\":\"बाप\",\"weight\":0.003769},{\"name\":\"भाई_ने_किया\",\"weight\":0.003760},{\"name\":\"उम्र\",\"weight\":0.003756},{\"name\":\"पहचान\",\"weight\":0.003737},{\"name\":\"मारी\",\"weight\":0.003735},{\"name\":\"परेशान\",\"weight\":0.003723},{\"name\":\"ओसामा_बिन_लादेन\",\"weight\":0.003719},{\"name\":\"साली\",\"weight\":0.003719},{\"name\":\"सपना\",\"weight\":0.003711},{\"name\":\"सेना\",\"weight\":0.003710},{\"name\":\"काबुल\",\"weight\":0.003710},{\"name\":\"इराक\",\"weight\":0.003710},{\"name\":\"कबाली\",\"weight\":0.003696},{\"name\":\"टीम_इंडिया\",\"weight\":0.003684},{\"name\":\"लखनऊ\",\"weight\":0.003682},{\"name\":\"मजाक\",\"weight\":0.003678},{\"name\":\"पेरिस\",\"weight\":0.003674},{\"name\":\"सनी_लियोन\",\"weight\":0.003671},{\"name\":\"कैद\",\"weight\":0.003656},{\"name\":\"आत्महत्या\",\"weight\":0.003655},{\"name\":\"एक्ट्रेस\",\"weight\":0.003655},{\"name\":\"बांग्लादेश\",\"weight\":0.003646}],\"prefer_hindi\":\"1\",\"new_user\":\"0\",\"city\":\"16_Pune\",\"neg_categories\":\"{\"name\":\"1000681\",\"weight\":3.028129}\"}"
    val bg = key.trim.indexOf("\"neg_categories\":\"")
    val end = key.trim.indexOf("}\"",bg)

    println(key.trim.substring(0,bg - 1) + key.trim.substring(end + 2,key.trim.length))
    println(bg.toString + "\t" + end.toString)
  }

}