package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.util.{JsonUtil, LoggingUtils}
import com.cmcm.cmnews.model.util.Parameters._
import com.cmcm.cmnews.model.util.Util._
import org.apache.spark.rdd.RDD
import org.json4s.JsonAST.{JObject, JValue}
import org.json4s._
import org.json4s.jackson.JsonMethods._

import scala.collection.mutable
import scala.util.{Failure, Success, Try}

/**
  * Created by lilonghua on 16/8/10.
  */

case class CPBaseInfo(cid: String, updateTime: String, line: String) {
  override def toString: String =
    s"$cid\t$updateTime\t$line"
}

object CPBProcessor extends ProcessorAbstract[CPBaseInfo] {

  def extractStr(jvalue: JValue, fieldName: String, default: String = "") = {
    jvalue \ fieldName match {
      case JString(s) => if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString()
      case _ => default
    }
  }



  override def preprocess(line: String, batchContext: mutable.Map[String, String]): CPBaseInfo = {
    //Use eywordVersion to recognize different version
    val keywordVersion =  Try(batchContext(constant_kw_version)).getOrElse("v2")
    ///According to pig file, only need to extract the item_it,update_time field
    Try(JsonUtil.convertToJValue(line)) match{
      case Success(jvalue) =>
        val cid = if ( keywordVersion == "oversea" ) extractStr(jvalue,"item_id")
        else extractStr(jvalue,"content_id")
        val updateTime = extractStr(jvalue,"update_time") match {
          case x:String if x != "" => x
          case _ => "0"
        }
        CPBaseInfo(cid,updateTime,line)
      case Failure(ex) =>
        LoggingUtils.loggingError(LoggingUtils.getException(ex))
        CPBaseInfo("unknown","0","unknown")
    }
  }

  override def process(inputRDD:RDD[String], batchContext: mutable.Map[String, String]) : RDD[CPBaseInfo] = {
    inputRDD.map((line:String) => preprocess(line, batchContext))
  }
}