package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.feature.{CFBFeature, ContentFeature, UserFeature, XFBFeatureKey}
import com.cmcm.cmnews.model.processor.{CFBProcessor, CPProcessor, EventProcessor, UPProcessor}
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{LoggingUtils, Parameters}
import com.cmcm.cmnews.model.util.Parameters._
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.joda.time.DateTime

import scala.util.Try

/**
 * Created by tangdong on 1/5/16.
 */
trait CFBTrainDataCompute extends EventBatchCompute{
  this: SparkBatchContext =>
  import Parameters._

  override def compute: Unit = {
    super.compute
    logInfo("Start CFBTrainDataCompute")
    //get rdd from path
    val eventRdd = getRDD[RDD[String]](constant_event_rdd,this)
    val cpRdd = getRDD[RDD[String]](constant_content_rdd, this)
    val upRdd = getRDD[RDD[String]](constant_user_rdd, this)
    val cfbRdd = getRDD[RDD[String]](constant_xfb_rdd,this)

    val parallel = batchContext(constant_parallelism).toInt
    //get parsed rdd
    val parsedEventRdd: RDD[(String, String)] = EventProcessor.process(eventRdd, this.getBatchContext)
    val parsedCpRdd: RDD[(String, String)] = CPProcessor.process(cpRdd, this.getBatchContext)
    val parsedUpRdd: RDD[(String, String)] = UPProcessor.process(upRdd, this.getBatchContext)
    val parsedCfbRdd: RDD[(String,String)] = CFBProcessor.process(cfbRdd, this.getBatchContext)

    //join rdd
    val eventJoinUpRdd = UserFeature.joinWithBatchEvent(parsedUpRdd, parsedEventRdd,parallel)
    val eventJoinUpAndCpRdd = ContentFeature.joinWithBatchEvent(parsedCpRdd, eventJoinUpRdd,parallel)
//      .sample(false,0.1)
      .map(line => {
        val r = scala.util.Random
        (r.nextInt(parallel*2),line)
      })
      .groupByKey(parallel)
      .flatMap(line => line._2)
      .persist(StorageLevel.DISK_ONLY)

    val runDateTime = batchContext(constant_date_time)
    val timeStamp = new DateTime(runDateTime).getMillis/1000

    val xfbConfigFileName = if(batchContext contains constant_config_file) batchContext(constant_config_file)
    else ""
    val config = Map[String,String] (constant_date_time -> timeStamp.toString,
      constant_d_ncat -> batchContext(constant_d_ncat),
      constant_d_nkey -> batchContext(constant_d_nkey),
      constant_u_ncat -> batchContext(constant_u_ncat),
      constant_u_nkey -> batchContext(constant_u_nkey),
      constant_parallelism -> batchContext(constant_parallelism),
      constant_config_file -> xfbConfigFileName,
      constant_random_num -> Try(batchContext(constant_random_num)).getOrElse("1000"),
      constant_multi_lan -> Try(batchContext(constant_multi_lan)).getOrElse("false").toLowerCase())

    val eventAndXFBFeature: (RDD[(String,String)],RDD[(String,String)]) = XFBFeatureKey.generateFullXFBFeatures(eventJoinUpAndCpRdd, config)
//    rddContext += (constant_train_rdd -> eventAndXFBFeature._2)
    val eventStaticFeatures = (eventAndXFBFeature._1).persist(StorageLevel.MEMORY_AND_DISK_SER)
    val XFBFeatures:RDD[(String,String)] = (eventAndXFBFeature._2).persist(StorageLevel.MEMORY_AND_DISK_SER)
//    XFBFeatures.saveAsObjectFile("/tmp/news_model/XFBFeaturesObj")
//    eventStaticFeatures.saveAsObjectFile("/tmp/news_model/eventStaticFeaturesObj")
//
//    val XFBFeatures = sbc.objectFile[(String,String)]("/tmp/news_model/XFBFeaturesObj")
//    val eventStaticFeatures = sbc.objectFile[(String,String)]("/tmp/news_model/eventStaticFeaturesObj")

    /* format xfb feature to json
    *  join cfb key to raw agg cfb, return (key, cfb_json)
    *  key is made of uniqId+pid
    */
    val eventWithXFB: RDD[(String, String)] = CFBFeature.joinWithBatchEvent(parsedCfbRdd, XFBFeatures, this.batchContext)

//    eventWithXFB.saveAsObjectFile("/tmp/news_model/eventWithXFBObj")
//    val eventWithXFB = sbc.objectFile[(String,String)]("/tmp/news_model/eventWithXFBObj")
//    val eventStaticFeatures = sbc.objectFile[(String,String)]("/tmp/news_model/eventStaticFeaturesObj")

    /*
    * join static feature(with label) and cfb feature by same key(uniqId+pid)
    *
    * */
    val trainDataOut = eventStaticFeatures.leftOuterJoin(eventWithXFB,parallel).map(line => {
        val key = line._2._1.split(fieldDelimiter,-1).mkString("\t") //static feature
        val value = line._2._2.getOrElse("") //cfb feature json
        s"$key\t$value"
    })

    rddContext += (constant_train_rdd -> trainDataOut)

  }
}
