package com.cmcm.cmnews.model.feature

import com.cmcm.cmnews.model.util.{JsonUtil, Parameters}
import org.apache.jute.compiler.JFloat
import org.apache.spark.Logging
import org.apache.spark.rdd.RDD
import org.json4s.JsonAST._
import scala.util.{Failure, Success, Try}
import java.text.DecimalFormat

/**
  * Created by mengchong on 7/11/16.
  */
object FormatLibsvmFeature extends Logging {
  import Parameters._

  /*
  * Transform joined event feature to libsvm format
  * @input featureData(label + static_feature + cfb json) featmap (fid, fname)
  * @return (uid,dCid,label,dwell,label + " " + feature_with_id)
  * */
  def formatLibsvmData(featureData: RDD[String],featureList:Array[(Int,String)]) = {
    val featmap = featureList.map(line => (line._2,line._1)).sortBy(x => x._2)
    featureData.map(line => formatFeature(line,featmap))
      .filter(line => line.nonEmpty)
  }

  def formatFeature(line:String,featureList:Array[(String,Int)]):String = {
// uniqueId,uid,dCid,joinTs,dwell,pid,"0.0","0.0",label,uCity,uCatLen,uKeyLen,catRel,keyRel,wordCount,imageCount,newsyScore, docAge,timeOfDay,dayOfWeek
//    val all_fields = line.split("\t")
//    val fields = all_fields(0).split(pairDelimiter)
    val fields = line.split("\t")
    val uniqueId = fields(0)
    val uid = fields(1)
    val dCid = fields(2)
    val joinTs = fields(3)
    val dwell = Try(fields(4).toDouble).getOrElse(0.0)
    val pid = fields(5)
    val label = fields(8)
    val uCity = fields(9)
    val uCatLen = fields(10)
    val uKeyLen = fields(11)
    val catRel = fields(12)
    val keyRel = fields(13)
    val wordCount = fields(14)
    val imageCount = fields(15)
    val newsyScore = fields(16)
    val docAge = fields(17)
    val timeOfDay = fields(18)
    val dayOfWeek = fields(19)
    val cfbJson =  Try(fields(20)).getOrElse("{}")
//    val cfbJson =  Try(all_fields(1)).getOrElse("{}")

    val cFBFeature = parseCalcCfbFeature(cfbJson)

    val staticFeature = Map[String,Double](
      "U_CAT_LEN" -> Try(uCatLen.toDouble).getOrElse(0.0),
      "U_KW_LEN" -> Try(uKeyLen.toDouble).getOrElse(0.0),
      "CAT_REL" -> Try(catRel.toDouble).getOrElse(0.0),
      "KW_REL" -> Try(keyRel.toDouble).getOrElse(0.0),
      "WORD_COUNT" -> Try(wordCount.toDouble).getOrElse(0.0),
      "IMAGE_COUNT" -> Try(imageCount.toDouble).getOrElse(0.0),
      "NEWSY_SCORE" -> Try(newsyScore.toDouble).getOrElse(0.0),
      "DOC_AGE" -> Try(docAge.toDouble).getOrElse(0.0),
      "TIME_OF_DAY" -> Try(timeOfDay.toDouble).getOrElse(0.0),
      "DAY_OF_WEEK" -> Try(dayOfWeek.toDouble).getOrElse(0.0)
    )

    val formatter =  new DecimalFormat("#.######")
    val allFeatures = staticFeature ++ cFBFeature

    val result = featureList.filter(x => allFeatures contains x._1)
      .map(line => {
        def formatDouble(v:Double) : String =
          if(v.isNaN || v.isInfinity) "0"
          else formatter.format(v)
        line._2 + ":" + formatDouble(allFeatures(line._1))
      } )

    List(uid,dCid,label,dwell,label + " " + result.mkString(" ")).mkString("\t")
  }

  /*
  * Parse json object,
  * for 1to1 feature (key_type, (click, view))
  * for 1tomulti feature like keyword/cat (key_type, map(key_name -> (click, view, weight)))
  * @return map(feature_id, feature_score)
  * */
  def parseCalcCfbFeature(cfbJson:String):Map[String,Double] = {
    if (cfbJson.isEmpty) return Map()
    def jvalue2Double(v:JValue) = {
      try {
        v(0) match {
          case JDouble(f) => f.toDouble
          case JInt(f) => f.toDouble
          case _ => 0.0
        }
      } catch {
        case e: Exception => 0.0
      }
    }

    val jvalue = JsonUtil.convertToJValue(cfbJson).asInstanceOf[JObject]
//    val values = jvalue.values

    jvalue.values.toList
      .flatMap(feature => {
        val (key, value) = feature
        value match {
          case feature: Map[_,_] =>
            try {
              val (meanClk, meanPv, maxClk, maxPv, microMaxCoec, macroMeanCoec, macroMaxCoec, cnt) =
                feature.toList.foldLeft(List(): List[(String, Double, Double, Double)])((l, t) => {
                  val (fname, fvalue) = t
                  val fvalueList = fvalue.asInstanceOf[List[Any]] //.fold(List(): List[String])((l, t) => t.toString :: l)
                  val (clk, pv, weight) = try {
                      val clk1 = Try(fvalueList(0).toString.toDouble).getOrElse(0.0)
                      val pv1 = Try(fvalueList(1).toString.toDouble).getOrElse(0.0)
                      val weight1 = Try(fvalueList(2).toString.toDouble).getOrElse(0.0)
                      (clk1, pv1, weight1)
                    } catch {
                      case e: Exception => (0.0, 0.0, 0.0)
                    }
                  (fname.toString, clk, pv, weight) :: l
                })
                  .filter(x => x._4 > 0)
                  .map(fields => {
                    val (fname, clk, pv, weight) = fields
                    val coec = if (pv > 1e-12) clk / pv else 0.0
                    (weight * clk, weight * pv, weight * clk, weight * pv, coec, weight * coec, weight * coec, 1)
                  })
                  .reduce((a, b) => {
                    val (meanClk2, meanPv2, maxClk2, maxPv2, microMaxCoec2, macroMeanCoec2, macroMaxCoec2, cnt2) = a
                    val (meanClk1, meanPv1, maxClk1, maxPv1, microMaxCoec1, macroMeanCoec1, macroMaxCoec1, cnt1) = b
                    (meanClk1 + meanClk2, meanPv1 + meanPv2, math.max(maxClk1, maxClk2), math.max(maxPv1, maxPv2),
                      math.max(microMaxCoec1, microMaxCoec2),
                      macroMeanCoec1 + macroMeanCoec2, math.max(macroMaxCoec1, macroMaxCoec2),
                      cnt1 + cnt2)
                  })
              val microMeanCoec = if (meanPv > 0.0) meanClk / meanPv else 0.0

              if (cnt > 0) {
                List[(String, Double)](
                  (key + "_MEAN_C", meanClk), (key + "_MEAN_EC", meanPv),
                  (key + "_MAX_C", maxClk), (key + "_MAX_EC", maxPv),
                  (key + "_MICRO_MEAN_COEC", microMeanCoec), (key + "_MICRO_MAX_COEC", microMaxCoec),
                  (key + "_MACRO_MEAN_COEC", macroMeanCoec), (key + "_MACRO_MAX_COEC", macroMaxCoec)
                )
              }
              else
                List[(String, Double)]()
            }catch {
              case e:Exception =>
                e.printStackTrace()
                List[(String, Double)]()
            }

          case feature: List[Any] =>
            val dataList = feature.toArray
            val clk = Try(dataList(0).toString.toDouble).getOrElse(0.0)
            val pv = Try(dataList(1).toString.toDouble).getOrElse(0.0)
            if (pv > 1e-12)
              List[(String, Double)]((key + "_EC", pv), (key + "_C", clk), (key + "_COEC", clk / pv))
            else
              List[(String, Double)]()
          case _ => List[(String, Double)]()
        }
      }).toMap
  }
}