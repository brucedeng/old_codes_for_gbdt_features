package com.cmcm.cmnews.model.feature

import com.cmcm.cmnews.model.util.Parameters
import org.apache.spark.Logging
import org.apache.spark.rdd.RDD

/**
 * Created by tangdong on 1/5/16.
 */
object ContentFeature extends Logging{
  import Parameters._
  def joinWithBatchEvent(contentFeatureRdd:RDD[(String, String)], eventRdd: RDD[(String, String)],parallel:Int = 200) = {
    logInfo("Now, join content profile and event")
    eventRdd.join(contentFeatureRdd,parallel*2).map(result => {
      val cid = result._1
      val eventFeatures = result._2._1
      val cpFeatures = result._2._2
      val defaultValue = "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "0" + fieldDelimiter + "0" + fieldDelimiter + "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "0" + fieldDelimiter + "0" + fieldDelimiter + "0" + fieldDelimiter + "unknown"
      val cpFeaturesStr = cpFeatures //.getOrElse(defaultValue)
      val afterValue =  eventFeatures + fieldDelimiter + cpFeaturesStr
      (cid, afterValue)
    })
  }
}
