package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.util.Parameters._
import org.scalatest.FunSuite

/**
  * Created by lilonghua on 2016/11/2.
  */
class TestEventProcessor extends FunSuite {

  test("parse event drv = 1 list data") {
    val clickDate = """{"servertime_sec":"1478044803","upack":{},"model":"GT-P5210","mcc":"","declared_lan":"","aid":"25c0f9a7ac30c432","net":"wifi","nmnc":"","ext":{"drv":"1","eventtime":"1478044731","requesttime":"1477983559"},"city":"CA_La Mesa","id":"","old_app_lan":"","refer":{},"lan":"en","action":"","value":"","mnc":"","apiv":"4","contentid":"49416eca0ZL_wd","osv":"4.4.2","ctype":"1","pid":"1","display":"0x04","api_server_ip":"10.2.9.193","ip":"170.213.2.45","country":"us","appv":"5.14.5","pf":"android","scenario":{"level2":"0","level1":"17","level1_type":"1","source":"","column":"","is_level1_native":"1"},"servertime":"2016-11-02 00:00:03","app_lan":"en","cpack":{"des":"rid=1085cce0|src=4194312|ord=4|page_n=0|hit_sc=0|s_up=0","md5":"49416eca0ZL_wd","ext":{"rec_reason":"index,index_gmp","big_img":0},"cmnt_cnt":0,"ishot":1},"brand":"samsung","nmcc":"","uuid":"","ch":"200001","act":"1","idfa":""}"""
    val parsed = EventProcessor.preprocess(clickDate, collection.mutable.Map[String, String](constant_event_position -> "1_1_11,1_1_17,21_1_11,21_1_17", constant_event_lan_region -> "(?i).*_us"))
    println(parsed)
    assert(parsed.nonEmpty)
  }

  test("parse event drv = 0 list data") {
    val clickDate = """{"servertime_sec":"1478044803","upack":{},"model":"GT-P5210","mcc":"","declared_lan":"","aid":"25c0f9a7ac30c432","net":"wifi","nmnc":"","ext":{"drv":"0","eventtime":"1478044731","requesttime":"1477983559"},"city":"CA_La Mesa","id":"","old_app_lan":"","refer":{},"lan":"en","action":"","value":"","mnc":"","apiv":"4","contentid":"49416eca0ZL_wd","osv":"4.4.2","ctype":"1","pid":"1","display":"0x04","api_server_ip":"10.2.9.193","ip":"170.213.2.45","country":"us","appv":"5.14.5","pf":"android","scenario":{"level2":"0","level1":"17","level1_type":"1","source":"","column":"","is_level1_native":"1"},"servertime":"2016-11-02 00:00:03","app_lan":"en","cpack":{"des":"rid=1085cce0|src=4194312|ord=4|page_n=0|hit_sc=0|s_up=0","md5":"49416eca0ZL_wd","ext":{"rec_reason":"index,index_gmp","big_img":0},"cmnt_cnt":0,"ishot":1},"brand":"samsung","nmcc":"","uuid":"","ch":"200001","act":"1","idfa":""}"""
    val parsed = EventProcessor.preprocess(clickDate, collection.mutable.Map[String, String](constant_event_position -> "1_1_11,1_1_17,21_1_11,21_1_17", constant_event_lan_region -> "(?i).*_us"))
    println(parsed)
    assert(parsed.isEmpty)
  }

  test("parse event drv null list data") {
    val clickDate = """{"servertime_sec":"1478044803","upack":{},"model":"GT-P5210","mcc":"","declared_lan":"","aid":"25c0f9a7ac30c432","net":"wifi","nmnc":"","ext":{"eventtime":"1478044731","requesttime":"1477983559"},"city":"CA_La Mesa","id":"","old_app_lan":"","refer":{},"lan":"en","action":"","value":"","mnc":"","apiv":"4","contentid":"49416eca0ZL_wd","osv":"4.4.2","ctype":"1","pid":"1","display":"0x04","api_server_ip":"10.2.9.193","ip":"170.213.2.45","country":"us","appv":"5.14.5","pf":"android","scenario":{"level2":"0","level1":"17","level1_type":"1","source":"","column":"","is_level1_native":"1"},"servertime":"2016-11-02 00:00:03","app_lan":"en","cpack":{"des":"rid=1085cce0|src=4194312|ord=4|page_n=0|hit_sc=0|s_up=0","md5":"49416eca0ZL_wd","ext":{"rec_reason":"index,index_gmp","big_img":0},"cmnt_cnt":0,"ishot":1},"brand":"samsung","nmcc":"","uuid":"","ch":"200001","act":"1","idfa":""}"""
    val parsed = EventProcessor.preprocess(clickDate, collection.mutable.Map[String, String](constant_event_position -> "1_1_11,1_1_17,21_1_11,21_1_17", constant_event_lan_region -> "(?i).*_us"))
    println(parsed)
    assert(parsed.nonEmpty)
  }
}
