package com.cmcm.cmnews.model.processor

import org.scalatest.FunSuite

/**
  * Created by lilonghua on 2016/11/15.
  */
class TestUserProcessor extends FunSuite {

  test("parse user") {
    val user = """{"topics":[],"u_cat_len":1,"age":"","u_kw_len":17,"entities_v2":[],"u_entities_v2_len":0,"entitiesExplore":[],"categories":[{"name":"1000296","weight":1.0}],"keywords":[{"name":"experiences","weight":0.17076157156400018},{"name":"worst","weight":0.14901290937281653},{"name":"revealing","weight":0.12106208204607702},{"name":"select","weight":0.05431451009676734},{"name":"lady","weight":0.053231690721988696},{"name":"hurts","weight":0.05319536138245001},{"name":"sensation","weight":0.052863082312793394},{"name":"girls","weight":0.052694788523106925},{"name":"sliding","weight":0.05056622763334403},{"name":"michael_bay","weight":0.0473616400139238},{"name":"the_club","weight":0.045101980263771886},{"name":"nice","weight":0.04436448749558157},{"name":"guy","weight":0.041604804898723174},{"name":"the_dance","weight":0.01641908087254298},{"name":"the_sex","weight":0.01618852807781489},{"name":"the_story","weight":0.016055534011362563},{"name":"real_life","weight":0.015201720712934835}],"uid":"b3ba0198549276e2","u_topic_len":0,"gender":""}"""
    val parsed = UserProcessor.preprocess(user, collection.mutable.Map.empty[String, String])
    println(parsed)
    assert(parsed.nonEmpty)
  }
}
