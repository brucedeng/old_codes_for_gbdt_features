package com.cmcm.cmnews.model.spark

import com.cmcm.cmnews.model.component.StreamingComponent
import com.cmcm.cmnews.model.config.ConfigUnit
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
  * Created by lilonghua on 2016/11/9.
  */

object SparkStreamingContext {
  val componentName = "spark_component"
  val configFileName = "spark.conf"
  val defaultAppName = "News_dl lr Spark Streaming"
  val defaultRecoverMode = false
  val defaultCheckPoint = "/tmp/news_dl_lr_checkpoint/"
  val defaultBatchSeconds = 600

}

trait SparkStreamingContext extends StreamingComponent {

  private var _streamingContext: StreamingContext =_
  private var _isNewContext: Boolean =_

  override def init(configDir: String) = {
    super.init(configDir)
    val(streamingContext, isNewContext) = initSparkStreamingContext(configDir + "/" + SparkStreamingContext.configFileName)
    _streamingContext = streamingContext
    _isNewContext = isNewContext
  }

  override def preStart() = {
    super.preStart()
  }

  def streamingContext: StreamingContext = {
    _streamingContext
  }

  def isNewContext: Boolean = {
    _isNewContext
  }

  def sparkContext: SparkContext = {
    _streamingContext.sparkContext
  }

  def sparkConfigUnit = {
    getConfigUnit(SparkStreamingContext.componentName).get
  }

  private def createNewSparkStreamingContext(sparkConfigUnit: ConfigUnit): StreamingContext = {
    logInfo("Initializing new spark streaming context...")
    val sparkConf = new SparkConf().setAppName(sparkConfigUnit.getString("spark.app.name",
      SparkStreamingContext.defaultAppName))
    if(sparkConfigUnit.getBoolean("spark.test",false)){
      sparkConf.setMaster(sparkConfigUnit.getString("spark.master","local[1]"))
    }
    val batchSeconds = sparkConfigUnit.getInt("streaming.batch.seconds",
      SparkStreamingContext.defaultBatchSeconds)
    val sparkContext = new SparkContext(sparkConf)
    val streamingContext = new StreamingContext(sparkContext, Seconds(batchSeconds))
    if (sparkConfigUnit.getBoolean("spark.app.checkpointEnable", true)) {
      streamingContext.checkpoint(sparkConfigUnit.getString("spark.app.checkpoint",
        SparkStreamingContext.defaultCheckPoint))
    }
    streamingContext
  }

  def initSparkStreamingContext(sparkConfigFile: String): (StreamingContext, Boolean) = {
    logInfo("Initializing spark component...")

    @volatile var isNewContext: Boolean = false
    val sparkConfigUnit = loadComponentConfig(SparkStreamingContext.componentName, sparkConfigFile)
    val checkpoint = sparkConfigUnit.getString("spark.app.checkpoint",
      SparkStreamingContext.defaultCheckPoint)
    val streamingContext = StreamingContext.getOrCreate(checkpoint,
      () => {
        isNewContext = true
        createNewSparkStreamingContext(sparkConfigUnit)
      })

    (streamingContext, isNewContext)
  }

}
