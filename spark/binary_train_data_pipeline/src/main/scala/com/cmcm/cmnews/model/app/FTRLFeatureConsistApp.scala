package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.FTRLFeatureConsistCompute
import com.cmcm.cmnews.model.input.FTRLFeatureConsistDataInput
import com.cmcm.cmnews.model.monitor.FTRLFeatureHitRateMonitor
import com.cmcm.cmnews.model.output.FTRLFeatureConsistDataOutput
import com.cmcm.cmnews.model.spark.NormalSparkContext

/**
  * Created by lilonghua on 2016/10/21.
  */
object FTRLFeatureConsistApp extends BatchWithMonitorApp
  with NormalSparkContext
  with FTRLFeatureConsistDataInput
  with FTRLFeatureConsistCompute
  with FTRLFeatureHitRateMonitor
  with FTRLFeatureConsistDataOutput
