package com.cmcm.cmnews.model.input

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters

/**
  * Created by lilonghua on 2016/11/14.
  */
trait UPExploreDataInput extends EventBatchInput {
  this: SparkBatchContext =>
  import Parameters._

  override def getInput(): Unit ={
    logInfo("start UPExploreDataInput...")
    setRDD(constant_user_input, constant_user_rdd, this)
    setRDD(constant_user_explore_input, constant_user_explore_rdd, this)
  }

}
