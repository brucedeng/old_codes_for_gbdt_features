package com.cmcm.cmnews.model.feature

import com.cmcm.cmnews.model.util.JsonUtil

/**
  * Created by lilonghua on 2016/10/28.
  */

case class ContentFeature (
                            baseFeature: BaseFeature,
                            titleEntity: List[(String,Double)] = List.empty[(String,Double)],
                            displayEntity: List[(String,Double)] = List.empty[(String,Double)],
                            publisher: String,
                            publishTime: Long,
                            groupID: String,
                            titleMD5: String,
                            mediaLevel: Int,
                            commentCount: Int,
                            hasBigImg: Int,
                            tier: String,
                            wordCount: Int,
                            imageCount: Int,
                            newsyScore: Float,
                            cid: String
                          ) extends Serializable {

  def filterWordCount(threshold: Int): Boolean = {
    wordCount < threshold
  }

  override def toString: String = {
    JsonUtil.toJson(Map("category" -> baseFeature.category,
      "keyword" -> baseFeature.keyword,
      "topic" -> baseFeature.topic,
      "entity" -> baseFeature.entity,
      "imageLabel" -> baseFeature.imageLabel,
      "titleEntity" -> titleEntity,
      "displayEntity" -> displayEntity,
      "publisher" -> publisher,
      "publishTime" -> publishTime,
      "groupID" -> groupID,
      "titleMD5" -> titleMD5,
      "mediaLevel" -> mediaLevel,
      "commentCount" -> commentCount,
      "hasBigImg" -> hasBigImg,
      "tier" -> tier,
      "wordCount" -> wordCount,
      "imageCount" -> imageCount,
      "newsyScore" -> newsyScore,
      "cid" -> cid))
  }

}

/*object ContentFeature extends Serializable {

  def apply(
             baseFeature: BaseFeature,
             publisher: String,
             publishTime: Long,
             groupID: String,
             titleMD5: String,
             mediaLevel: Int,
             commentCount: Int,
             hasBigImg: Int,
             tier: String,
             wordCount: Int,
             imageCount: Int,
             newsyScore: Float,
             cid: String
           ): ContentFeature = new ContentFeature(baseFeature, publisher, publishTime, groupID, titleMD5, mediaLevel, commentCount, hasBigImg, tier, wordCount, imageCount, newsyScore, cid)
}*/
