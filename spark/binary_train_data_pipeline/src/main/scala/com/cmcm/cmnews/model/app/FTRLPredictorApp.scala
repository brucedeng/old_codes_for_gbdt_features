package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.FTRLPredictorCompute
import com.cmcm.cmnews.model.input.FTRLPredictorDataInput
import com.cmcm.cmnews.model.output.FTRLPredictorDataOutput
import com.cmcm.cmnews.model.spark.NormalSparkContext

/**
  * Created by lilonghua on 16/9/28.
  */
object FTRLPredictorApp extends BatchApp
  with NormalSparkContext
  with FTRLPredictorDataInput
  with FTRLPredictorCompute
  with FTRLPredictorDataOutput
