package com.cmcm.cmnews.model.processor

import org.apache.spark.Logging
import org.apache.spark.rdd.RDD

/**
 * Created by tangdong on 27/4/16.
 */
trait Processor extends Logging with Serializable {
  def preprocess(line:String, batchContext: collection.mutable.Map[String,String]) : (String, String) = {
    logInfo("Start preprocess each line...")
    ("","")
  }


  def process(inputRDD:RDD[String],batchContext: collection.mutable.Map[String,String]): RDD[(String, String)] = {
    inputRDD.map((line:String) => preprocess(line, batchContext))
  }
}
