package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.FTRLDataFilterCompute
import com.cmcm.cmnews.model.input.FTRLDataFilterDataInput
import com.cmcm.cmnews.model.output.XFBBinaryTrainDataOutput
import com.cmcm.cmnews.model.spark.BinaryTrainDataSparkContext

/**
  * Created by lilonghua on 2016/11/14.
  */
object FTRLDataFilterApp extends BatchNewApp
  with BinaryTrainDataSparkContext
  with FTRLDataFilterDataInput
  with FTRLDataFilterCompute
  with XFBBinaryTrainDataOutput
