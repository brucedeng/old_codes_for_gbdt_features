package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.feature.{BaseFeature, UserFeature}
import com.cmcm.cmnews.model.util.{JsonUtil, LoggingUtils}
import com.cmcm.cmnews.model.util.Parameters._
import org.apache.spark.rdd.RDD
import org.json4s.JsonAST.JArray

import scala.collection.mutable
import scala.util.{Failure, Success, Try}

/**
  * Created by lilonghua on 2016/11/18.
  */
object UserExploreProcessor extends ProcessorAbstract[Option[UserFeature]] {

  override def preprocess(line: String, batchContext: mutable.Map[String, String]): Option[UserFeature] = {
    val topRelNotifyEntity = Try(batchContext(constant_u_rel_nnotify_entity)).getOrElse("200").toInt
    val uTag = Try(batchContext(constant_up_uid_tag)).getOrElse("aid")
    val appLan = Try(batchContext(constant_up_app_lan_region)).getOrElse("").split(",")
    //val userFilter = Try(batchContext(constant_up_user_filter)).getOrElse("true").toLowerCase

    Try(JsonUtil.convertToJValue(line)) match {
      case Success(jvalue) =>
        val app_lan = JsonUtil.getStr(jvalue, "app_lan", "")
        val aid = JsonUtil.getStr(jvalue, "aid", "")
        val uuid = if (aid.isEmpty) JsonUtil.getStr(jvalue, uTag, "") else aid
        val notifyEntity = jvalue \ "entities" match {
          case JArray(cats) => JsonUtil.catKwAssist(cats, topRelNotifyEntity)
          case _ => List.empty[(String, Double)]
        }
        val gender = JsonUtil.getStr(jvalue, "gender", "")
        val age = JsonUtil.getStr(jvalue, "age", "")
        val catLen = Try(JsonUtil.getStr(jvalue, "u_cat_len", "0").toInt).getOrElse(0)
        val kwLen = Try(JsonUtil.getStr(jvalue, "u_kw_len", "0").toInt).getOrElse(0)
        val topicLen = Try(JsonUtil.getStr(jvalue, "u_topic_len", "0").toInt).getOrElse(0)
        val entityLen = Try(JsonUtil.getStr(jvalue, "u_entities_v2_len", "0").toInt).getOrElse(0)
        val imageLabelLen = Try(JsonUtil.getStr(jvalue, "u_image_label_len", "0").toInt).getOrElse(0)

        val feature = UserFeature(
          BaseFeature(),
          notifyEntity,
          catLen,
          kwLen,
          topicLen,
          entityLen,
          imageLabelLen,
          gender,
          age,
          uuid
        )

        if (uuid.nonEmpty && (appLan.exists(f => app_lan.matches(f)) || app_lan.isEmpty)) {
          Option(feature)
        } else {
          None
        }
      case Failure(ex) =>
        LoggingUtils.loggingError(LoggingUtils.getException(ex))
        None

    }
  }

  override def process(inputRDD: RDD[String], batchContext: mutable.Map[String, String]): RDD[Option[UserFeature]] = {
    inputRDD.map(preprocess(_, batchContext)).filter(_.nonEmpty)
  }
}
