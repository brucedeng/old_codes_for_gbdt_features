package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.{BinaryTrainDataCompute, UpHitrateCompute}
import com.cmcm.cmnews.model.input.{UpHitrateInput, XFBBinaryTrainDataInput}
import com.cmcm.cmnews.model.output.{UpHitrateOutput, XFBBinaryTrainDataOutput}
import com.cmcm.cmnews.model.spark.{BinaryTrainDataSparkContext, UpHitrateSparkContext}

/**
  * Created by lilonghua on 2016/10/31.
  */
object UpHitrateApp extends BatchNewApp
  with UpHitrateSparkContext
  with UpHitrateInput
  with UpHitrateCompute
  with UpHitrateOutput
