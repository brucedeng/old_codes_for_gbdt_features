package com.cmcm.cmnews.model.input

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters

import scala.util.Try

/**
 * Created by tangdong on 3/5/16.
 */
trait UpHitrateInput extends EventBatchInput {
  this: SparkBatchContext =>
  import Parameters._

  override def getInput: Unit ={
    logInfo("start XFBTrainDataInput...")
    //setRDD(constant_event_input,constant_event_rdd, this)
    setRDD(constant_event_pv_input,constant_event_pv_rdd, this)
    /*
    val dataType = Try(batchContext(constant_event_data_type)).getOrElse("minutes")
    if (dataType != "day") {
      setRDD(constant_event_pv_click_input,constant_event_pv_click_rdd, this)
    }*/
    setRDD(constant_user_input, constant_user_rdd, this)
    setRDD(constant_cate_coverage_output,constant_cate_coverage_rdd, this)
  }

}
