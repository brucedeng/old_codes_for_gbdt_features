package com.cmcm.cmnews.model.feature

import com.cmcm.cmnews.model.config.Config
import com.cmcm.cmnews.model.util.{FeatureTools, Parameters}
import org.apache.spark.Logging
import org.apache.spark.rdd.RDD
import org.joda.time.{DateTime, DateTimeZone}

import scala.collection.mutable.ListBuffer
import scala.collection.mutable
import scala.util.Try

/**
  * Created by tangdong on 1/5/16.
  */
object XFBBinaryFeatureKey extends Logging with Serializable {
  import Parameters._

  def generateFullXFBFeatures(fullFeature: RDD[(String, String)], config:collection.immutable.Map[String,String]) : RDD[((String, String, String, String), String)] = {
    val typesafeConfig = new Config(config(constant_config_file)).typesafeConfig
    fullFeature.map(genBinaryStaticFeature(_,config))
      .map(f => {
        (f._1, generateBinaryFeatures(f._2, config, typesafeConfig))
      })
  }

  def getBinaryFeatureValueRecursive(featureMap:mutable.HashMap[String, List[(String, Double)]],
                                     featureList: List[String],
                                     featJoin: String) : List[(String, Double)] = {
    var featureValues = new ListBuffer[(String, Double)]
    if (featureList.size > 1) {
      val preFeatures = featureMap.getOrElse(featureList.head,List())
      val newFeatureList = featureList.drop(1)
      for (preFeature <- preFeatures) {
        for (posFeature <- getBinaryFeatureValueRecursive(featureMap, newFeatureList, featJoin)) {
          featureValues += ((preFeature._1 + featJoin + posFeature._1, preFeature._2 * posFeature._2))
        }
      }
    } else {
      for (preFeature <- featureMap.getOrElse(featureList.head,List())){
        featureValues += preFeature
      }
    }
    featureValues.toList
  }

  def getMatchBinaryFeatures(binaryFeatures: ListBuffer[(String,String)],
                             featureConfig: String,
                             featureMap: mutable.HashMap[String, List[(String,Double)]],
                             featJoin: String) = {
    for (feature <- featureConfig.split(",")) {
      val fields = feature.split("_")
      val preFeature = fields(1) + "_" + fields(3)
      val posFeature = fields(2) + "_" + fields(4)
      val featureId = fields(5)
      for (value1 <- featureMap.getOrElse(preFeature,List())) {
        for (value2 <- featureMap.getOrElse(posFeature,List())) {
          if (value1._1.equals(value2._1)) {
            binaryFeatures += ((featureId + featJoin + value1._1 + featJoin + value1._1, "%.3f".format(value1._2 * value2._2)))
          }
        }
      }
    }
  }
  def getCrossBinaryFeatures(binaryFeatures: ListBuffer[(String, String)],
                             featureConfig: String,
                             featureMap: mutable.HashMap[String, List[(String,Double)]],
                             featJoin: String) = {
    for(feature <- featureConfig.split(",")){
      var featureList = new ListBuffer[String]
      val fields = feature.split("_")
      var i = 1
      val catNameIndex = fields.length/2
      while(i <= ((fields.length-1)/2)){
        featureList += (fields(i) + "_" + fields(i+catNameIndex-1))
        i = i + 1
      }
      val featureId = fields(fields.length-1)
      for (value <- getBinaryFeatureValueRecursive(featureMap, featureList.toList, featJoin)) {
        binaryFeatures += ((featureId + featJoin + value._1, "%.3f".format(value._2)))
      }
    }
  }
  def getSingleEdgeBinaryFeatures(binaryFeatures: ListBuffer[(String, String)],
                                  featureConfig: String,
                                  featureMap: mutable.HashMap[String, List[(String,Double)]],
                                  featJoin: String) = {
    for (feature <- featureConfig.split(",")){
      val fields = feature.split("_")
      val featureName = fields(0) + "_" + fields(1)
      val featureId = fields(2)

      val values = featureMap.getOrElse(featureName,List())
      for (value <- values) {
        binaryFeatures += ((featureId + featJoin + value._1, "%.3f".format(value._2)))
      }
    }
  }
  def generateBinaryFeatures(featureMap:collection.mutable.HashMap[String, List[(String,Double)]], config:collection.immutable.Map[String,String],typesafeConfig:com.typesafe.config.Config) :String = {
    val binaryFeatures = new ListBuffer[(String, String)]

    val userFeatureConfig = typesafeConfig.getString("cfb.bin_uf")
    val docFeatureConfig = typesafeConfig.getString("cfb.bin_df")
    val crossFeatureConfig = typesafeConfig.getString("cfb.bin_cf")
    val matchFeatureConfig = typesafeConfig.getString("cfb.bin_mf")
    val featJoin = Try(typesafeConfig.getString("cfb.feat_join")).getOrElse("_")
    val featSplit = Try(typesafeConfig.getString("cfb.feat_split")).getOrElse(" ")
    val replaceSplit = Try(typesafeConfig.getString("cfb.replace_split")).getOrElse("_")

    if (userFeatureConfig.nonEmpty) getSingleEdgeBinaryFeatures(binaryFeatures, userFeatureConfig, featureMap, featJoin)
    if (docFeatureConfig.nonEmpty) getSingleEdgeBinaryFeatures(binaryFeatures, docFeatureConfig, featureMap, featJoin)
    if (crossFeatureConfig.nonEmpty) getCrossBinaryFeatures(binaryFeatures, crossFeatureConfig, featureMap, featJoin)
    if (matchFeatureConfig.nonEmpty) getMatchBinaryFeatures(binaryFeatures, matchFeatureConfig, featureMap, featJoin)

    binaryFeatures.map(f => f._1.replace("\n",featSplit).replace(featSplit,replaceSplit)).map(f => if (f.contains(featJoin)) f else f + featJoin).mkString(featSplit)
  }

  def genBinaryStaticFeature(feature:(String,String), config:collection.immutable.Map[String,String]) = {
    val featureMap = new mutable.HashMap[String, List[(String,Double)]]
    val dCid = feature._1
    val items = feature._2.split(fieldDelimiter)
    val uid = items(0)

    val eventFeature = FeatureTools(items(1))
    val upFeature = FeatureTools(items(2))
    val cpFeature = FeatureTools(items(3))

    val ts = eventFeature.getFeatureValue("SERVER_TIME").getOrElse("0").toLong
    val position = eventFeature.getFeatureValue("U_POSITION").getOrElse("")
    val net = eventFeature.getFeatureValue("U_NET").getOrElse("")
    val osVersion = eventFeature.getFeatureValue("U_OSVERSION").getOrElse("")
    val appVersion = eventFeature.getFeatureValue("U_APPVERSION").getOrElse("")
    val platform = eventFeature.getFeatureValue("U_PLATFORM").getOrElse("")
    val brand = eventFeature.getFeatureValue("U_BRAND").getOrElse("")
    val model = eventFeature.getFeatureValue("U_MODEL").getOrElse("")
    val channel = eventFeature.getFeatureValue("U_CHANNEL").getOrElse("")
    val isHot = eventFeature.getFeatureValue("U_ISHOT").getOrElse("")
    val appIv = eventFeature.getFeatureValue("U_APPIV").getOrElse("")
    val uCity = eventFeature.getFeatureValue("U_CITY").getOrElse("")
    val dwell = eventFeature.getFeatureValue("DWELL").getOrElse("0")
    val label = eventFeature.getFeatureValue("LABEL").getOrElse("0")
    val country = eventFeature.getFeatureValue("U_COUNTRY").getOrElse("")
    val lan = eventFeature.getFeatureValue("U_LAN").getOrElse("")
    val lanRegion = if (lan.isEmpty && country.isEmpty) "" else lan + "_" + country
    val uRelCat = upFeature.getFeatureValue("U_CATEGORY").getOrElse("")
    val uRelKey = upFeature.getFeatureValue("U_KEYWORD").getOrElse("")
    val uRelTopic = upFeature.getFeatureValue("U_TOPIC").getOrElse("")
    val uGender = upFeature.getFeatureValue("U_GENDER").getOrElse("")
    val uAge = upFeature.getFeatureValue("U_AGE").getOrElse("")
    val uCatLen = upFeature.getFeatureValue("U_CATLEN").getOrElse("")
    val uKeyLen = upFeature.getFeatureValue("U_KWLEN").getOrElse("")
    val uTopicLen = upFeature.getFeatureValue("U_TOPICLEN").getOrElse("")
    val dGroupid = cpFeature.getFeatureValue("D_GROUPID").getOrElse("")
    val dTier = cpFeature.getFeatureValue("D_TIER").getOrElse("")
    val dPublisher = cpFeature.getFeatureValue("D_PUBLISHER").getOrElse("")
    val dPublishTime = cpFeature.getFeatureValue("C_PUBLISHTIME").getOrElse("-1").toLong
    val dRelCat = cpFeature.getFeatureValue("D_CATEGORY").getOrElse("")
    val dRelKey = cpFeature.getFeatureValue("D_KEYWORD").getOrElse("")
    val dRelTopic = cpFeature.getFeatureValue("D_TOPIC").getOrElse("")
    val wordCount = cpFeature.getFeatureValue("D_WORDCOUNT").getOrElse("")
    val imageCount = cpFeature.getFeatureValue("D_IMAGECOUNT").getOrElse("")
    val newsyScore = cpFeature.getFeatureValue("D_NEWSYSCORE").getOrElse("")
    val dTitlemd5 = cpFeature.getFeatureValue("D_TITLEMD5").getOrElse("")
    val mediaLevel = cpFeature.getFeatureValue("D_MEDIALEVEL").getOrElse("")
    val commentCount = cpFeature.getFeatureValue("D_COMMENTCOUNT").getOrElse("")
    val hasBigImg = cpFeature.getFeatureValue("D_HASBIGIMG").getOrElse("")

    val catRel = genRelevance(uRelCat,dRelCat)
    val keyRel = genRelevance(uRelKey,dRelKey)
    val topicRel = genRelevance(uRelTopic,dRelTopic)

    val uCat = top(uRelCat,Try(config(constant_u_ncat)).getOrElse("5").toInt)
    val uKey = top(uRelKey,Try(config(constant_u_nkey)).getOrElse("25").toInt)
    val uTopic = top(uRelTopic,Try(config(constant_u_rel_ntopic)).getOrElse("20").toInt)
    val dCat = top(dRelCat,Try(config(constant_d_ncat)).getOrElse("5").toInt)
    val dKey = top(dRelKey,Try(config(constant_d_nkey)).getOrElse("15").toInt)
    val dTopic = top(dRelTopic,Try(config(constant_d_ntopic)).getOrElse("10").toInt)

    val docAge = Try(math.floor((ts - dPublishTime)/3600.0d).toLong).getOrElse(0l) match {
      case x:Long if x >= 0 => x
      case _ => 0
    }
    val timeTuple = if (ts <= 0) {
      (-1,-1)
    } else{
      val dateD = new DateTime(ts * 1000L).withZone(DateTimeZone.UTC)
      (dateD.getHourOfDay, dateD.getDayOfWeek%7)
    }
    val timeOfDay = timeTuple._1
    val dayOfWeek = timeTuple._2

    if(uCatLen != "" && uCatLen != "unknown") {
      if (uCatLen == "0") {
        featureMap.put("U_CATLEN", List(("-1",1.0)))
      } else {
        featureMap.put("U_CATLEN", List(("%.0f".format(math.floor(Try(uCatLen.toDouble).getOrElse(-1.0) * 1)),1.0)))
      }
    }else{
      featureMap.put("U_CATLEN", List(("-1",1.0)))
    }
    if(uKeyLen != "" && uKeyLen != "unknown") {
      if (uKeyLen == "0") {
        featureMap.put("U_KWLEN", List(("-1",1.0)))
      } else {
        featureMap.put("U_KWLEN", List(("%.0f".format(math.floor(Try(uKeyLen.toDouble).getOrElse(0.0) * 0.1)),1.0)))
      }
    }else{
      featureMap.put("U_KWLEN", List(("-1",1.0)))
    }
    if(uTopicLen.nonEmpty && uTopicLen != "unknown") {
      if (uTopicLen == "0") {
        featureMap.put("U_TOPICLEN", List(("0",1.0)))
      } else {
        featureMap.put("U_TOPICLEN", List(("%.0f".format(math.floor(Try(uTopicLen.toDouble).getOrElse(0.0) * 0.1)),1.0)))
      }
    }else{
      featureMap.put("U_TOPICLEN", List(("0",1.0)))
    }
    if(wordCount != "" && wordCount != "unknown") {
      featureMap.put("D_WORDCOUNT", List(("%.0f".format(math.floor(Try(wordCount.toDouble).getOrElse(0.0) * 0.1)),1.0)))
    }else{
      featureMap.put("D_WORDCOUNT", List(("",1.0)))
    }
    if(imageCount != "" && imageCount != "unknown") {
      featureMap.put("D_IMAGECOUNT", List(("%.0f".format(math.floor(Try(imageCount.toDouble).getOrElse(0.0) * 1)),1.0)))
    }else{
      featureMap.put("D_IMAGECOUNT", List(("",1.0)))
    }
    if(newsyScore != "" && newsyScore != "unknown") {
      featureMap.put("D_NEWSYSCORE", List(("%.0f".format(math.floor(Try(newsyScore.toDouble).getOrElse(0.0) * 100)),1.0)))
    }else{
      featureMap.put("D_NEWSYSCORE", List(("",1.0)))
    }
    if(catRel >= 0) {
      featureMap.put("UD_CATREL", List(("%.0f".format(math.floor(catRel * 1000)),1.0)))
    }else{
      featureMap.put("UD_CATREL", List(("",1.0)))
    }
    if(keyRel >= 0) {
      featureMap.put("UD_KWREL", List(("%.0f".format(math.floor(keyRel * 1000)),1.0)))
    }else{
      featureMap.put("UD_KWREL", List(("",1.0)))
    }
    if(topicRel >= 0) {
      featureMap.put("UD_TOPICREL", List(("%.0f".format(math.floor(topicRel * 1000)),1.0)))
    }else{
      featureMap.put("UD_TOPICREL", List(("",1.0)))
    }
    if(docAge >= 0) {
      featureMap.put("D_DOCAGE", List((docAge.toString,1.0)))
    }else{
      featureMap.put("D_DOCAGE", List(("",1.0)))
    }
    if(timeOfDay >= 0) {
      featureMap.put("E_TIMEOFDAY", List((timeOfDay.toString,1.0)))
    }else{
      featureMap.put("E_TIMEOFDAY", List(("",1.0)))
    }
    if(dayOfWeek >= 0){
      featureMap.put("E_DAYOFWEEK", List((dayOfWeek.toString,1.0)))
    }else{
      featureMap.put("E_DAYOFWEEK", List(("",1.0)))
    }
    if(lanRegion != "" && lanRegion != "unknown") {
      featureMap.put("E_LANREGION",List((lanRegion.toLowerCase,1.0)))
    }else{
      featureMap.put("E_LANREGION", List(("",1.0)))
    }


    if (uid != "" && uid != "unknown") {
      featureMap.put("U_UID", List((uid,1.0)))
    }else{
      featureMap.put("U_UID", List(("",1.0)))
    }
    if (uCity != "" && uCity != "unknown") {
      featureMap.put("U_CITY", List((uCity,1.0)))
    }else{
      featureMap.put("U_CITY", List(("",1.0)))
    }
    if (position.nonEmpty && position != "unknown") {
      featureMap.put("U_POSITION", List((position,1.0)))
    }else{
      featureMap.put("U_POSITION", List(("",1.0)))
    }
    if (uGender != "" && uGender != "unknown") {
      featureMap.put("U_GENDER", List((uGender,1.0)))
    }else{
      featureMap.put("U_GENDER", List(("",1.0)))
    }
    if (uAge != "" && uAge != "unknown") {
      featureMap.put("U_AGE", List((uAge,1.0)))
    }else{
      featureMap.put("U_AGE", List(("",1.0)))
    }
    if (net.nonEmpty && net != "unknown") {
      featureMap.put("U_NET", List((net,1.0)))
    }else{
      featureMap.put("U_NET", List(("",1.0)))
    }
    if (osVersion.nonEmpty && osVersion != "unknown") {
      featureMap.put("U_OSVERSION", List((osVersion,1.0)))
    }else{
      featureMap.put("U_OSVERSION", List(("",1.0)))
    }
    if (appVersion.nonEmpty && appVersion != "unknown") {
      featureMap.put("U_APPVERSION", List((appVersion,1.0)))
    }else{
      featureMap.put("U_APPVERSION", List(("",1.0)))
    }
    if (platform.nonEmpty && platform != "unknown") {
      featureMap.put("U_PLATFORM", List((platform,1.0)))
    }else{
      featureMap.put("U_PLATFORM", List(("",1.0)))
    }
    if (brand.nonEmpty && brand != "unknown") {
      featureMap.put("U_BRAND", List((brand,1.0)))
    }else{
      featureMap.put("U_BRAND", List(("",1.0)))
    }
    if (model.nonEmpty && model != "unknown") {
      featureMap.put("U_MODEL", List((model,1.0)))
    }else{
      featureMap.put("U_MODEL", List(("",1.0)))
    }
    if (channel.nonEmpty && channel != "unknown") {
      featureMap.put("U_CHANNEL", List((channel,1.0)))
    }else{
      featureMap.put("U_CHANNEL", List(("",1.0)))
    }
    if (isHot.nonEmpty && isHot != "unknown") {
      featureMap.put("U_ISHOT", List((isHot,1.0)))
    }else{
      featureMap.put("U_ISHOT", List(("0",1.0)))
    }
    if (appIv.nonEmpty && appIv != "unknown") {
      featureMap.put("U_APPIV", List((appIv,1.0)))
    }else{
      featureMap.put("U_APPIV", List(("0",1.0)))
    }
    if (uCat != "" && uCat != "unknown") {
      featureMap.put("U_CATEGORY", uCat.split(pairGap).map(record => {
        val nameAndWeight = record.split(keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("U_CATEGORY", List(("",1.0)))
    }
    if (uKey != "" && uKey != "unknown") {
      featureMap.put("U_KEYWORD", uKey.split(pairGap).map(record => {
        val nameAndWeight = record.split(keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("U_KEYWORD", List(("",1.0)))
    }

    if (uTopic != "" && uTopic != "unknown") {
      featureMap.put("U_TOPIC", uTopic.split(pairGap).map(record => {
        val nameAndWeight = record.split(keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("U_TOPIC", List(("",1.0)))
    }

    if (dGroupid != "" && dGroupid != "unknown") {
      featureMap.put("D_GROUPID", List((dGroupid,1.0)))
    }else{
      featureMap.put("D_GROUPID", List(("",1.0)))
    }
    if (dTier != "" && dTier != "unknown") {
      featureMap.put("D_TIER", List((dTier,1.0)))
    }else{
      featureMap.put("D_TIER", List(("",1.0)))
    }
    if (dTitlemd5 != "" && dTitlemd5 != "unknown") {
      featureMap.put("D_TITLEMD5", List((dTitlemd5,1.0)))
    }else {
      featureMap.put("D_TITLEMD5", List(("",1.0)))
    }
    if (dPublisher != "" && dPublisher != "unknown") {
      featureMap.put("D_PUBLISHER", List((dPublisher,1.0)))
    }else{
      featureMap.put("D_PUBLISHER", List(("",1.0)))
    }
    if (mediaLevel.nonEmpty && mediaLevel != "unknown") {
      featureMap.put("D_MEDIALEVEL", List((mediaLevel,1.0)))
    }else{
      featureMap.put("D_MEDIALEVEL", List(("0",1.0)))
    }
    if (commentCount.nonEmpty && commentCount != "unknown") {
      featureMap.put("D_COMMENTCOUNT", List((commentCount,1.0)))
    }else{
      featureMap.put("D_COMMENTCOUNT", List(("0",1.0)))
    }
    if (hasBigImg.nonEmpty && hasBigImg != "unknown") {
      featureMap.put("D_HASBIGIMG", List((hasBigImg,1.0)))
    }else{
      featureMap.put("D_HASBIGIMG", List(("0",1.0)))
    }
    if (dCat != "" && dCat != "unknown") {
      featureMap.put("D_CATEGORY", dCat.split(pairGap).map(record => {
        val nameAndWeight = record.split(keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0),nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("D_CATEGORY", List(("",1.0)))
    }
    if (dKey != "" && dKey != "unknown") {
      featureMap.put("D_KEYWORD", dKey.split(pairGap).map(record => {
        val nameAndWeight = record.split(keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("D_KEYWORD", List(("",1.0)))
    }
    if (dTopic != "" && dTopic != "unknown") {
      featureMap.put("D_TOPIC", dTopic.split(pairGap).map(record => {
        val nameAndWeight = record.split(keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("D_TOPIC", List(("",1.0)))
    }
    if (dCid != "" && dCid != "unknown") {
      featureMap.put("D_CONID", List((dCid,1.0)))
    } else {
      featureMap.put("D_CONID", List(("",1.0)))
    }


    ((uid, dCid, label, dwell), featureMap)
  }


  def genRelevance(u_input:String,d_input:String):Double = {
    if (u_input == "" || u_input == "unknown" || d_input == "" || d_input == "unknown") {
      0.0
    } else {
      val u_input_dict = u_input.split(pairGap).map((line:String) => {
        val items = line.split(keyGap)
        val name = items(0)
        val value = items(1).toDouble
        (name, value)
      }).toMap

      val rel = d_input.split(pairGap).map((line:String) => {
        val items = line.split(keyGap)
        val name = items(0)
        val value = items(1).toDouble
        Try(u_input_dict(name)).getOrElse(0.0) * value
      }).sum
      if(rel.isNaN || rel.isInfinity) 0.0
      else rel
    }
  }

  def top(inputStr:String,topNum:Int): String = {
    if (inputStr == "" || inputStr == "unknown") {
      inputStr
    } else {
      inputStr.split(pairGap).slice(0,topNum).mkString(pairGap)
    }
  }

  def calculateWeight(baseData: RDD[((String, String, String, String), String)]) = {
    val (spWt: Double, snWt: Double) = Try(baseData.map(x => {
      val label = x._1._3
      val dwell = x._1._4

      val wt = math.log(math.max(0.0d, Try(dwell.toDouble).toOption match {
        case Some(t) => t
        case None => 0.0d
      }) + 1.0d)
      val pos_wt = if (label == "1") wt else 0.0d
      val neg_wt = if (label == "1") 0.0d else 1.0d
      (pos_wt, neg_wt)
    }).reduce((x, y) => (x._1 + y._1, x._2 + y._2))).getOrElse((1d,0d))
    val ratio: Double = math.min(snWt / spWt, 10)
    println(s"ratio: $ratio, spWt: $spWt, snWt: $snWt")
    baseData.map(x => {
      val label = x._1._3
      val dwell = x._1._4
      val wt = math.log(Try(dwell.toDouble).toOption match {
        case Some(t) => t + + 1.0d
        case None => 1.0d
      })
      // val label = (x.base.label.toInt * weight * ratio * 10000).toInt.toString

      val weight: Double = if (label.toInt == 1) wt * ratio else 1.0d
      ((x._1._1, x._1._2, x._1._3, weight.toString), x._2)
    })
  }

  def calcuWeight(baseData:RDD[(String,String)]) = {
    val (spWt: Double, snWt: Double) = Try(baseData.map(x => {
      val items = x._2.split(fieldDelimiter)
      val eventFeature = FeatureTools(items(1))
      val dwell = eventFeature.getFeatureValue("DWELL").getOrElse("0")
      val label = eventFeature.getFeatureValue("LABEL").getOrElse("0")

      val wt = math.log(math.max(0.0d, Try(dwell.toDouble).toOption match {
        case Some(t) => t
        case None => 0.0d
      }) + 1.0d)
      val pos_wt = if (label == "1") wt else 0.0d
      val neg_wt = if (label == "1") 0.0d else 1.0d
      (pos_wt, neg_wt)
    }).reduce((x, y) => (x._1 + y._1, x._2 + y._2))).getOrElse((1d,0d))
    val ratio: Double = math.min(snWt / spWt, 10)
    println(s"ratio: $ratio, spWt: $spWt, snWt: $snWt")
    baseData.map(x => {
      val items = x._2.split(fieldDelimiter)
      val eventFeature = FeatureTools(items(1))
      val dwell = eventFeature.getFeatureValue("DWELL").getOrElse("0")
      val label = eventFeature.getFeatureValue("LABEL").getOrElse("0")
      val wt = math.log(Try(dwell.toDouble).toOption match {
        case Some(t) => t + + 1.0d
        case None => 1.0d
      })
      // val label = (x.base.label.toInt * weight * ratio * 10000).toInt.toString

      val weight: Double = if (label.toInt == 1) wt * ratio else 1.0d
      (x._1, items(0) + fieldDelimiter + eventFeature.addFeature("DWELL", weight.toString).genFeatureString() + fieldDelimiter + items(2) + fieldDelimiter + items(3))
    })
  }

  def main(args:Array[String]) = {
    println(math.floor("10.634".toDouble))
    println(math.floor(1234354/3600.0d).toLong)
    println(1234354/3600.0d)
  }
}
