package com.cmcm.cmnews.model.spark

import com.cmcm.cmnews.model.util.Parameters
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.immutable.HashMap
import scala.util.Try

/**
 * Created by tangdong on 1/5/16.
 */
trait XFBTrainDataSparkContext extends SparkBatchContext {
  import Parameters._

  override def init(configDir: String = "conf"): Unit ={
    super.init(configDir)
    logInfo("Start XFBTrainDataSparkContext...")
    val sparkConf = new SparkConf()
      .registerKryoClasses(Array(
        classOf[HashMap[String, Int]],
        classOf[HashMap[String, List[(String,Double)]]],
        classOf[HashMap[String, String]]))
      .set("spark.serializer","org.apache.spark.serializer.KryoSerializer")
      .set("spark.default.parallelism", Try(batchContext(constant_parallelism)).getOrElse("32"))
      //.setAppName(Try(batchContext(constant_jobName)).getOrElse(this.getClass.getName))
    sbc = new SparkContext(sparkConf)
  }

}
