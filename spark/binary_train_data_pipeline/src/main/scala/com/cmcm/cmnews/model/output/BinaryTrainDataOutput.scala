package com.cmcm.cmnews.model.output

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.rdd.RDD

/**
  * Created by lilonghua on 2016/11/7.
  */
trait BinaryTrainDataOutput extends EventBatchOutput {
  this: SparkBatchContext =>
  import Parameters._

  override def outPut: Unit ={
    super.outPut
    logInfo("Start BinaryTrainDataOutput...")
    val xfbTrainDataOut = rddContext(constant_train_rdd).asInstanceOf[RDD[String]]
    xfbTrainDataOut.saveAsTextFile(batchContext(constant_train_out), classOf[GzipCodec])
  }
}
