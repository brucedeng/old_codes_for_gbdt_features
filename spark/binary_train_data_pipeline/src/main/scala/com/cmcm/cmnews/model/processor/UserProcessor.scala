package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.feature.{BaseFeature, UserFeature}
import com.cmcm.cmnews.model.util.{JsonUtil, LoggingUtils}
import com.cmcm.cmnews.model.util.Parameters._
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.RDD
import org.json4s.JsonAST._

import scala.collection.mutable
import scala.util.{Failure, Success, Try}

/**
	* Created by lilonghua on 2016/10/28.
	*/
object UserProcessor extends ProcessorAbstract[Option[UserFeature]] {

  override def preprocess(line: String, batchContext: mutable.Map[String, String]): Option[UserFeature] = {
    val topRelCat = Try(batchContext(constant_u_rel_ncat)).getOrElse("200").toInt
    val topRelKw = Try(batchContext(constant_u_rel_nkey)).getOrElse("200").toInt
    val topRelTopic = Try(batchContext(constant_u_rel_ntopic)).getOrElse("200").toInt
    val topRelEntity = Try(batchContext(constant_u_rel_nentity)).getOrElse("200").toInt
    val topRelImageLabel = Try(batchContext(constant_u_rel_nimage_label)).getOrElse("200").toInt
    val topRelNotifyEntity = Try(batchContext(constant_u_rel_nnotify_entity)).getOrElse("200").toInt

    Try(JsonUtil.convertToJValue(line)) match {
      case Success(jvalue) =>
        val aid = JsonUtil.getStr(jvalue, "aid", "")
        val uuid = if (aid.isEmpty) JsonUtil.getStr(jvalue, "uid", "") else aid
        val categories = jvalue \ "categories" match {
          case JArray(cats) =>JsonUtil.catKwAssist(cats, topRelCat)
          case _ => List.empty[(String, Double)]
        }
        val keywords = jvalue \ "keywords" match {
          case JArray(cats) => JsonUtil.catKwAssist(cats, topRelKw)
          case _ => List.empty[(String, Double)]
        }
        val topics = jvalue \ "topics" match {
          case JArray(cats) => JsonUtil.catKwAssist(cats, topRelTopic)
          case _ => List.empty[(String, Double)]
        }
        val entities = jvalue \ "entities_v2" match {
          case JArray(cats) => JsonUtil.catKwAssist(cats, topRelEntity)
          case _ => List.empty[(String, Double)]
        }
        val imageLabel = jvalue \ "image_label" match {
          case JArray(cats) => JsonUtil.catKwAssist(cats, topRelImageLabel)
          case _ => List.empty[(String, Double)]
        }
        val notifyEntity = jvalue \ "notify_entities" match {
          case JArray(cats) => JsonUtil.catKwAssist(cats, topRelNotifyEntity)
          case _ => List.empty[(String, Double)]
        }
        val gender = JsonUtil.getStr(jvalue, "gender", "")
        val age = JsonUtil.getStr(jvalue, "age", "")
        val catLen = JsonUtil.getStr(jvalue, "u_cat_len", "0")
        val kwLen = JsonUtil.getStr(jvalue, "u_kw_len", "0")
        val topicLen = JsonUtil.getStr(jvalue, "u_topic_len", "0")
        val entityLen = JsonUtil.getStr(jvalue, "u_entities_v2_len", "0")
        val imageLabelLen = Try(JsonUtil.getStr(jvalue, "u_image_label_len", "0").toInt).getOrElse(0)

        val feature = UserFeature(
          BaseFeature(categories,keywords,topics, entities, imageLabel),
          notifyEntity,
          Try(catLen.toInt).getOrElse(0),
          Try(kwLen.toInt).getOrElse(0),
          Try(topicLen.toInt).getOrElse(0),
          Try(entityLen.toInt).getOrElse(0),
          imageLabelLen,
          gender,
          age,
          uuid
        )
        Option(feature)
      case Failure(ex) =>
        LoggingUtils.loggingError(LoggingUtils.getException(ex))
        None
		}
	}

	override def process(inputRDD: RDD[String], batchContext: mutable.Map[String, String]): RDD[Option[UserFeature]] = {
		inputRDD.map(preprocess(_, batchContext)).filter(_.nonEmpty)
	}

	def processAppRun(inputRDD: RDD[String], batchContext: mutable.Map[String, String], aidSet: Broadcast[Set[String]]) = {
		val topRelPkg = batchContext.getOrElse(constant_u_rel_npkg, "15").toInt
		inputRDD.flatMap(x => try {
			val jValue = JsonUtil.convertToJValue(x)
			val aid = JsonUtil.getStr(jValue, "aid", "", "")
			if (aidSet.value.contains(aid)) {
				val packages = jValue \ "pkg_list" match {
					case JArray(pkgs) => JsonUtil.catKwAssist(pkgs, topRelPkg, "weight", 0.0, "name")
					case _ => List.empty[(String, Double)]
				}
				Some((aid, packages.sortBy(x => -1.0 * x._2).take(topRelPkg)))
			} else None
		} catch {
			case ex: Throwable =>
				LoggingUtils.loggingError(s"$x extract error!", ex)
				None
		})
	}
}
