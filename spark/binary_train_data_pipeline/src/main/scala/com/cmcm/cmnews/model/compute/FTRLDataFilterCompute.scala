package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.processor.UserProcessor
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters
import org.apache.spark.rdd.RDD

import scala.util.Try

/**
  * Created by lilonghua on 2016/11/14.
  */
trait FTRLDataFilterCompute extends EventBatchCompute {
  this: SparkBatchContext =>

  import Parameters._

  override def compute: Unit = {
    super.compute
    logInfo("Start FTRLDataFilterCompute")

    val parallel = Try(batchContext(constant_parallelism)).getOrElse("100").toInt

    val trainData = getRDD[RDD[String]](constant_lr_train_rdd, this)
    val userData = getRDD[RDD[String]](constant_user_explore_rdd, this)

    val parseTrain = trainData.map(f => {
      (f.split("\t")(0), f)
    })

    val parseUser = UserProcessor.process(userData, this.batchContext).map(_.get.aid)

    val broadcastUser = getSparkContext.broadcast(parseUser.collect())

    val outputData = parseTrain.filter(f => {
      broadcastUser.value.contains(f._1)
    }).map(_._2)

    rddContext += (constant_train_rdd -> outputData)
  }

}
