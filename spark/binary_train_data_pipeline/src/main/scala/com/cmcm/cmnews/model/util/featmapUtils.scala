package com.cmcm.cmnews.model.util

import java.io.InputStream

/**
  * Created by mengchong on 7/11/16.
  */
object featmapUtils {
  import Parameters._
  def getFeatureList(filename:String): Array[(Int,String)] = {
    val lines = scala.io.Source.fromFile(filename).getLines()
    lines.map(x => {
      val fields = x.split('\t')
      val fid = fields(0).toInt
      val fname = fields(1)
      val ftype = fields(2)
      (fid,fname)
    }).toArray
  }

}
