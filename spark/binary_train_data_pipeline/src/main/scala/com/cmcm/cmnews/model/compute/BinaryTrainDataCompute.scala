package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.feature.LRBinaryFeatures
import com.cmcm.cmnews.model.processor._
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.joda.time.DateTime

import scala.util.Try

/**
  * Created by lilonghua on 2016/10/28.
  */
trait BinaryTrainDataCompute extends EventBatchCompute {
  this: SparkBatchContext =>

  import Parameters._

  override def compute: Unit = {
    super.compute
    logInfo("Start BinaryTrainDataCompute")
    val dataType = Try(batchContext(constant_event_data_type)).getOrElse("minutes")
    val debugFlag = Try(batchContext(constant_debug)).getOrElse("false")
    //generate event data from log
    val pvRdd = getRDD[RDD[String]](constant_event_pv_rdd,this)
    val clickRdd = getRDD[RDD[String]](constant_event_click_rdd,this)
    val readRdd = getRDD[RDD[String]](constant_event_readtime_rdd,this)

    val parsePvRdd = EventProcessor.process(pvRdd, this.batchContext).map(_.get).filter(_.eventBase.act == 1)
    val parseClickRdd = EventProcessor.process(clickRdd, this.batchContext).map(_.get).filter(_.eventBase.act == 2)

    val pvUnClickRdd = if (dataType == "day") {
      parsePvRdd
        .map(f => (f.getUnqId,f))
        .leftOuterJoin(parseClickRdd.map(f => (f.getUnqId,f)))
        .filter(_._2._2.isEmpty).map(_._2._1)
    } else {
      val pvClickRdd = getRDD[RDD[String]](constant_event_pv_click_rdd,this)
      val parsePvClickRdd = EventProcessor.process(pvClickRdd, this.batchContext).map(_.get).filter(_.eventBase.act == 2).map(_.getUnqId).collect()
      val pvClickBroadcast = getSparkContext.broadcast(parsePvClickRdd)
      parsePvRdd.filter(x => !pvClickBroadcast.value.contains(x.getUnqId))
    }

    val defaultRead = Try(batchContext(constant_event_defaultRead)).getOrElse("70").toInt

    val parseReadRdd = EventProcessor.process(readRdd, this.batchContext).map(_.get).filter(_.eventBase.act == 4)
      .map(f => {
        (f.getUnqId, f.readTime)
      }).collectAsMap()
    val readBroadcast = getSparkContext.broadcast(parseReadRdd)
    val clickWithReadRdd = parseClickRdd.map(f => {
      f.copy(readTime = readBroadcast.value.getOrElse(f.getUnqId, defaultRead))
    })

    val parsedEventRdd = EventProcessor.processDudupMerge(sbc.union(Seq(pvRdd, clickRdd, readRdd)), this.batchContext, this.sbc)
    
    val globalAid = getSparkContext.broadcast(parsedEventRdd.map(_.aid).distinct().collect().toSet)
    val globalCid = getSparkContext.broadcast(parsedEventRdd.map(_.cid).distinct().collect().toSet)

    //val topAid = Try(batchContext(constant_event_topAid)).getOrElse("20").toInt
    val topCid = Try(batchContext(constant_event_topCid)).getOrElse("20").toInt
    val duplication = Try(batchContext(constant_event_duplication)).getOrElse("10").toInt

    //println(s"topCid => $topCid\tduplication => $duplication")
    //parsedEventRdd.map(e=>(e.gmp,e.cid)).collect().foreach(println)


    /*val aidTop = parsedEventRdd.map(f => {(f.aid, 1)})
      .reduceByKey(_ + _)
      .sortBy(f => f._2, false)
      .take(topAid)
      .map(_._1)*/

    val cidTop = parsedEventRdd.map(f => {(f.cid, 1)})
      .reduceByKey(_ + _)
      .sortBy(f => f._2, false)
      .take(topCid)
      .map(_._1)

    //val globalAidTop = getSparkContext.broadcast(aidTop)
    val globalCidTop = getSparkContext.broadcast(cidTop)

    if (debugFlag == "true") {
      println(s"event processPost count => ${parsedEventRdd.count()}")

      println("\nTop200 By aid")
      parsedEventRdd.map(f => {
        (f.aid, 1)
      }).reduceByKey(_ + _).sortBy(f => f._2, false).take(200).foreach(f => println("Top200 BY AID => " + f._1 + "\t" + f._2))

      println("\nTop200 By cid")
      parsedEventRdd.map(f => {
        (f.cid, 1)
      }).reduceByKey(_ + _).sortBy(f => f._2, false).take(200).foreach(f => println("Top200 BY CID => " + f._1 + "\t" + f._2))
    }

    val cpRdd = getRDD[RDD[String]](constant_content_rdd, this)
    val upRdd = getRDD[RDD[String]](constant_user_rdd, this)

    val parallel = Try(batchContext(constant_parallelism)).getOrElse("100").toInt
    val parallelMax = Try(batchContext(constant_parallelismMax)).getOrElse("500").toInt


    //val parsedEventRdd: RDD[(String, String)] = EventProcessor.processNew(pvUnionClickRdd, this.getBatchContext)
    val parsedCpRdd = ContentProcessor.process(cpRdd, this.getBatchContext)
      .filter(f => globalCid.value.contains(f.get.cid))
      /*.map(f => (f.get.cid, f.get))*/
      //.persist(StorageLevel.DISK_ONLY)

    globalCid.unpersist()

    if (debugFlag == "true") {
      println(s"parsedCpRdd count => ${parsedCpRdd.count()}")
      //parsedCpRdd.saveAsTextFile("/tmp/dl/parsedCpRdd", classOf[GzipCodec])
    }

    val appRunRdd = getRDD[RDD[String]](constant_app_run_rdd, this)
    val appRunProcess = UserProcessor.processAppRun(appRunRdd, this.batchContext, globalAid)

    val parsedUpRdd = UserProcessor.process(upRdd, this.getBatchContext)
        .flatMap(f => if(globalAid.value.contains(f.get.aid)) Some((f.get.aid, f.get)) else None)


    globalAid.unpersist()


    if (debugFlag == "true") {
      println(s"parsedUpRdd count => ${parsedUpRdd.count()}")
      //parsedUpRdd.saveAsTextFile("/tmp/dl/parsedUpRdd", classOf[GzipCodec])
    }

    val appRunUp =  parsedUpRdd.leftOuterJoin(appRunProcess).map(x => (x._1, x._2 match {
      case (uf, Some(pkgs)) => uf.copy(baseFeature = uf.baseFeature.copy(pkg = pkgs))
      case (uf, None) => uf
    }))


    //join rdd
    /*val eventJoinUpRdd = parsedEventRdd.map(f => (f.aid, f)).leftOuterJoin(parsedUpRdd, parallelMax).map(f => {
      (f._2._1.cid, f._2)
    })*/

    val eventJoinUpRdd = parsedEventRdd.map(f => (f.aid, f)).leftOuterJoin(appRunUp, parallelMax)
      .map(f => {
        if (globalCidTop.value.contains(f._2._1.cid)) {
          val rand = scala.util.Random.nextInt(duplication)
          (f._2._1.cid + "_" + rand, f._2)
        } else {
          (f._2._1.cid, f._2)
        }
      })

    if (debugFlag == "true") {
      println(s"eventJoinUpRdd processPost count => ${eventJoinUpRdd.count()}")
      //eventJoinUpRdd.saveAsTextFile("/tmp/dl/eventJoinUpRdd", classOf[GzipCodec])
    }

    val dupCpRdd = parsedCpRdd.flatMap(f => {
      if (globalCidTop.value.contains(f.get.cid)) {
        (0 until duplication).map(t => {
          (f.get.cid + "_" + t, f.get)
        })
      } else {
        List((f.get.cid, f.get))
      }
    })

    val eventJoinUpAndCpRdd = eventJoinUpRdd.join(dupCpRdd, parallelMax * 2)
      .map(f => (f._2._1._1, f._2._1._2, f._2._2))
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    if (debugFlag == "true") {
      println(s"eventJoinUpAndCpRdd processPost count => ${eventJoinUpAndCpRdd.count()}")
      //eventJoinUpAndCpRdd.saveAsTextFile("/tmp/dl/eventJoinUpAndCpRdd", classOf[GzipCodec])
    }

    val eventWeight = LRBinaryFeatures.calFeatureWeight(eventJoinUpAndCpRdd)

    val runDateTime = batchContext(constant_date_time)
    val timeStamp = new DateTime(runDateTime).getMillis / 1000

    val xfbConfigFileName = if (batchContext contains constant_config_file) batchContext(constant_config_file)
    else ""
    val config = Map[String, String](constant_date_time -> timeStamp.toString,
      constant_d_ncat -> batchContext(constant_d_ncat),
      constant_d_nkey -> batchContext(constant_d_nkey),
      constant_u_ncat -> batchContext(constant_u_ncat),
      constant_u_nkey -> batchContext(constant_u_nkey),
      constant_u_ntopic ->batchContext(constant_u_ntopic),
      constant_d_ntopic ->batchContext(constant_d_ntopic),
      constant_parallelism -> batchContext(constant_parallelism),
      constant_config_file -> xfbConfigFileName,
      constant_multi_lan -> Try(batchContext(constant_multi_lan)).getOrElse("false").toLowerCase(),
      constant_gmpLowerBound -> Try(batchContext(constant_gmpLowerBound)).getOrElse("0.0"),
      constant_gmpUpperBound -> Try(batchContext(constant_gmpUpperBound)).getOrElse("1.0"),
      constant_n_gmpInterval ->Try(batchContext(constant_n_gmpInterval)).getOrElse("100"),
      constant_u_npkg -> batchContext(constant_u_npkg),
      constant_multi_lan -> Try(batchContext(constant_multi_lan)).getOrElse("false").toLowerCase(),
      constant_gmpLowerBound -> Try(batchContext(constant_gmpLowerBound)).getOrElse("0.0"),
      constant_gmpUpperBound -> Try(batchContext(constant_gmpUpperBound)).getOrElse("1.0"),
      constant_n_gmpInterval ->Try(batchContext(constant_n_gmpInterval)).getOrElse("100"),
      constant_u_npkg -> batchContext(constant_u_npkg),
      constant_multi_lan -> Try(batchContext(constant_multi_lan)).getOrElse("false").toLowerCase())

    val eventAndXFBFeature = LRBinaryFeatures.generateFullXFBFeatures(eventWeight, config)

    val lrTrainDataOut = eventAndXFBFeature.map(f => {
      s"${f._1._1}\t${f._1._2}\t${f._1._3}:${f._1._4}\t${f._2}"
    }).coalesce(parallel)

    eventJoinUpAndCpRdd.unpersist()
    parsedEventRdd.unpersist()
    //parsedUpRdd.unpersist()
    //parsedCpRdd.unpersist()

    rddContext += (constant_train_rdd -> lrTrainDataOut)
  }

}
