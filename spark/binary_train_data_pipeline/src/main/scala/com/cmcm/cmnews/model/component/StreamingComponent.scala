package com.cmcm.cmnews.model.component

import com.cmcm.cmnews.model.config.ConfigUnit
import org.apache.spark.Logging

import scala.collection.mutable
import scala.util.Try

/**
  * Created by lilonghua on 2016/11/9.
  */
trait StreamingComponent extends Logging {

  val componentsConfigs = mutable.HashMap[String, ConfigUnit]()

  def init(configDir: String) = {}
  def preStart() = {}

  def loadConfigUnit(configName: String, configFilePath: String) = {
    val newConfigUnit = ConfigUnit(configName, configFilePath)
    componentsConfigs.put(newConfigUnit.getConfigName, newConfigUnit)
    newConfigUnit
  }

  def getConfigUnit(configName: String): Option[ConfigUnit] = {
    componentsConfigs.get(configName)
  }

  def registComponent(configName: String, configFilePath: String) = {
    val newConfigUnitTry = Try(ConfigUnit(configName, configFilePath))
    if (newConfigUnitTry.isFailure)
      logError(s"Can't load config file $configFilePath for $configName ")
    //TODO  should throw exception here ? and add format code  if{ } else { }

    newConfigUnitTry.foreach(configUnit =>
      componentsConfigs.put(configUnit.getConfigName, configUnit))
    componentsConfigs.get(configName)
  }

  def loadComponentConfig(name: String, eventConfigFile: String): ConfigUnit = {
    logInfo(s"Loading $name component config from $eventConfigFile")
    val componentConfigUnit = registComponent(name, eventConfigFile)
    if (componentConfigUnit.isEmpty) {
      logError(s"Can not init $name component from $eventConfigFile")
      throw new IllegalStateException(s"Can not init $name component from $eventConfigFile")
    }
    componentConfigUnit.get
  }

}
