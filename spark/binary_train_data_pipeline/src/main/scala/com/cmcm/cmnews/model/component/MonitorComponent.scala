package com.cmcm.cmnews.model.component

import com.cmcm.cmnews.model.monitor.MonitorProxy
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters
import org.apache.spark.Logging
import org.apache.spark.broadcast.Broadcast

/**
  * Created by lilonghua on 2016/11/24.
  */
object MonitorComponent extends Logging {
  val componentName = "monitor_component"
  val configFileName = "monitor.conf"
  val defaultDecayWindow = 600
  var monitor: MonitorProxy = _
  var monitorBroadcastVar: Broadcast[MonitorProxy] = _
}

trait MonitorComponent extends BatchComponent {
  this: SparkBatchContext =>

  def initMonitor(configDir: String) = {
    initMonitorComponent(configDir + batchContext.getOrElse(Parameters.constant_conf_monitor, MonitorComponent.configFileName))
    MonitorComponent.monitor = new MonitorProxy()
    MonitorComponent.monitor.init(modelConfigUnit)
    MonitorComponent.monitorBroadcastVar = getSparkContext.broadcast(MonitorComponent.monitor)
    logInfo(s"Initializing monitor driver is ok ... ${MonitorComponent.monitor.monitor}")
  }

  def modelConfigUnit = {
    getConfigUnit(MonitorComponent.componentName).get
  }

  def initMonitorComponent(monitorConfigFile: String) = {
    logInfo(s"Initializing monitor component with $monitorConfigFile ....")
    loadComponentConfig(MonitorComponent.componentName, monitorConfigFile)
  }

  def monitor = {
    logInfo("Start MonitorComponent...")
  }
}
