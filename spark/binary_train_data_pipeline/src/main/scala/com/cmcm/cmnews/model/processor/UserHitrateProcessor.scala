package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.feature.{BaseFeature, UserFeature}
import com.cmcm.cmnews.model.util.{JsonUtil, LoggingUtils}
import com.cmcm.cmnews.model.util.Parameters._
import org.apache.spark.rdd.RDD
import org.json4s.JsonAST.JArray

import scala.collection.mutable
import scala.util.{Failure, Success, Try}

/**
  * Created by zhangruichang on 16/12/24.
  */
object UserHitrateProcessor {
  def preprocess(line: String, batchContext: mutable.Map[String, String]): (String, List[String], String, String, String) = {
    val topRelCat = 200
    println("line: " + line)
    Try(JsonUtil.convertToJValue(line)) match {
      case Success(jvalue) =>
        val aid = JsonUtil.getStr(jvalue, "aid", "")
        val uuid = if (aid.isEmpty) JsonUtil.getStr(jvalue, "uid", "") else aid
        val app_lan = JsonUtil.getStr(jvalue, "app_lan", "")
        val categories = jvalue \ "categories" match {
          case JArray(cats) =>JsonUtil.catKwNameAssist(cats, topRelCat)
          case _ => List.empty[(String)]
        }
        val catLen = JsonUtil.getStr(jvalue, "u_cat_len", "0")
        val kwLen = JsonUtil.getStr(jvalue, "u_kw_len", "0")

        (uuid, categories, kwLen, catLen, app_lan)
      case Failure(ex) =>
        LoggingUtils.loggingError(LoggingUtils.getException(ex))
        ("", List.empty[(String)], "", "", "")
    }
  }

  def process(inputRDD: RDD[String], batchContext: mutable.Map[String, String]): RDD[(String,List[String],String,String,String)] = {
    inputRDD.map(preprocess(_, batchContext))
  }
}
