package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.feature.LRBinaryFeatures
import com.cmcm.cmnews.model.processor._
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.joda.time.DateTime

import scala.util.Try

/**
  * Created by lilonghua on 2016/10/28.
  */
trait UpHitrateCompute extends EventBatchCompute {
  this: SparkBatchContext =>

  import Parameters._

  override def compute: Unit = {
    super.compute
    logInfo("Start BinaryTrainDataCompute")
    //val pvRdd = getRDD[RDD[String]](constant_event_pv_rdd,this)
    //val parsePvRdd = EventProcessor.process(pvRdd, this.batchContext).map(_.get).filter(_.eventBase.act == 1)
    val upRdd = getRDD[RDD[String]](constant_user_rdd, this)
    val parsedUpRdd = UserHitrateProcessor.process(upRdd, this.getBatchContext)
      .filter(_._5 == "en_us")
      .map(record => (record._1, record._2, record._3, record._4))
    val upCount = parsedUpRdd.count()
    println("upCount: " + upCount)
    val flattenUpRdd = parsedUpRdd
      .filter(record => record._4.toInt > 0)
      .flatMap {
        case (aid, list, kwLen, catLen) => list.map(category => (category, 1))
      }.reduceByKey(_ + _)
      .map(line => (line._1, line._2.toFloat / upCount))
        .sortBy(_._2, false).take(30)
    rddContext += (constant_cate_coverage_rdd -> flattenUpRdd)
  }

}
