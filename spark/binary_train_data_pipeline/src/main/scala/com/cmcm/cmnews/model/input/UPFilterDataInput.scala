package com.cmcm.cmnews.model.input

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters

/**
  * Created by lilonghua on 16/9/14.
  */

trait UPFilterDataInput extends EventBatchInput{
  this: SparkBatchContext =>
  import Parameters._

  override def getInput(): Unit ={
    logInfo("start UPFilterDataInput...")
    setRDD(constant_user_input, constant_user_rdd, this)
  }

}