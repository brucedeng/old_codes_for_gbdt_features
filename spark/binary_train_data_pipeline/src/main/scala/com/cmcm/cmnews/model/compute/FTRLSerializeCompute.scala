package com.cmcm.cmnews.model.compute

import java.io.{BufferedOutputStream, FileOutputStream}

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{Parameters, Util}
import org.apache.spark.rdd.RDD
import redis.clients.util.MurmurHash

import scala.util.Try

/**
  * Created by lilonghua on 16/10/9.
  */
trait FTRLSerializeCompute extends EventBatchCompute {
  this: SparkBatchContext =>

  import Parameters._

  override def compute: Unit = {
    super.compute
    logInfo("Start FTRLSerializeCompute")

    val parallel = Try(batchContext(constant_parallelism)).getOrElse("100").toInt
    val serializePath = batchContext(constant_lr_serialize_ptah)

    val modelData = getRDD[RDD[String]](constant_lr_model_rdd, this)

    val parseModel = modelData.map(m => {
      val f = m.split("\t")
      if (f.length == 2) {
        (f(0),Try(f(1).toDouble).getOrElse(0d))
      } else {
        ("", 0d)
      }
    }).filter(f => f != null && f._1.nonEmpty)
      .map(f => {
        Util.long2Bytes(MurmurHash.hash64A(f._1.getBytes(), 0)) ++ Util.double2Bytes(f._2)
        //FtrlModel(MurmurHash.hash64A(f._1.getBytes(), 0), f._2)
      })

    val result = parseModel.collect()

    val bos = new BufferedOutputStream(new FileOutputStream(serializePath))
    if (bos != null) {
      /*result.foreach(f => {
        Stream.continually(bos.write(f))
      })*/
      result.foreach(b => bos.write(b))
      bos.close()
    } else {
      logInfo(s"Can't open $serializePath")
    }

    //rddContext += (constant_lr_serialize_rdd -> parseModel)
  }

}
