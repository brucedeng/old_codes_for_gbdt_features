package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.parse.FeatureHitRate
import com.cmcm.cmnews.model.processor.{ContentProcessor, FeatureLogProcessor, UserProcessor}
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{Parameters, Util}
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.rdd.RDD

import scala.util.Try

/**
  * Created by lilonghua on 2016/10/21.
  */
trait FTRLFeatureConsistCompute extends EventBatchCompute {
  this: SparkBatchContext =>

  import Parameters._

  override def compute: Unit = {
    super.compute
    logInfo("Start FTRLFeatureConsistCompute")

    val parallel = Try(batchContext(constant_parallelism)).getOrElse("100").toInt
    val tmpPtah = Try(batchContext(constant_debug_path)).getOrElse("")
    val debugFlag = Try(batchContext(constant_debug)).getOrElse("false")
    val stat = Try(batchContext(constant_feature_stat_type)).getOrElse("all")
    /*val faetJoin = Try(batchContext(constant_feature_join)).getOrElse("\u0001")
    val config = Try(batchContext(constant_conf_feature)).getOrElse("")
    val typesafeConfig = new Config(config).typesafeConfig
    val features = Try(typesafeConfig.getString("lr.inFeat").split(",")).getOrElse(Array.empty[String])
    val filter = Try(typesafeConfig.getString("lr.filter")).getOrElse("false")*/
    val featureConf = featureConfigUnit
    val features = featureConf.getString("lr.inFeat","").split(",")
    val filter = featureConf.getBoolean("lr.filter", false)
    val faetJoin = featureConf.getString("lr.feat_join","\u0001")

    println(s"Config Features\t" + features.mkString("\t"))

    val trainFeat = getRDD[RDD[String]](constant_lr_train_feature_rdd, this)
    val serverFeat = getRDD[RDD[String]](constant_lr_server_feature_rdd, this)

    val parseTrain = trainFeat.filter(f => {
      f.split("\t").length == 4
    }).map(f => {
      val feat = f.split("\t")
      (feat(0) + fieldDelimiter + feat(1), feat.last)
    }).filter(f => f._1.nonEmpty)

    if (debugFlag == "true") {
      println(s"event offline feature parse count => ${parseTrain.count()}")
      if (tmpPtah.nonEmpty) {
        parseTrain.map(f => f._1 + "\t" + f._2).saveAsTextFile(s"$tmpPtah/offlineFeature", classOf[GzipCodec])
      }
    }

    val parseSever = FeatureLogProcessor.process(serverFeat, this.getBatchContext)

    if (debugFlag == "true") {
      println(s"event featurelog parse count => ${parseSever.count()}")
      if (tmpPtah.nonEmpty) {
        parseSever.map(f => f._1 + "\t" + f._2).saveAsTextFile(s"$tmpPtah/featurelogParse", classOf[GzipCodec])
      }
    }

    val joinFaetures = parseSever.join(parseTrain, parallel)

    if (debugFlag == "true") {
      println(s"event joinFaetures count => ${joinFaetures.count()}")
    }

    val compareOut = if (stat == "all") {
      joinFaetures.flatMap(f => {
        val feat = f._2._1.split(fieldDelimiter)
        Util.statFeat(feat.last, f._2._2, faetJoin)
      })
        .reduceByKey((f1,f2) => (f1._1 + f2._1, f1._2 + f2._2, f1._3 + f2._3))
        .filter(f => {
          if (filter/* == "true"*/) {
            features.contains(f._1)
          } else {
            true
          }
        })
        .sortBy(_._1)
        .map(f => FeatureHitRate(f._1, f._2._1, f._2._2, Try(f._2._1 / f._2._2.toFloat).getOrElse(0f), f._2._3, Try(f._2._1 / f._2._3.toFloat).getOrElse(0f)))
    } else if ("trace" == stat) {
      val cpRdd = getRDD[RDD[String]](constant_content_rdd, this)
      val upRdd = getRDD[RDD[String]](constant_user_rdd, this)
      val parsedCpRdd = ContentProcessor.process(cpRdd, this.getBatchContext).map(f => (f.get.cid, f.get))
      val parsedUpRdd = UserProcessor.process(upRdd, this.getBatchContext).map(f => (f.get.aid, f.get))
      joinFaetures.map(f => {
        val feat = f._2._1.split(fieldDelimiter)
        val uc = f._1.split(fieldDelimiter)
        (uc(0), (uc(1), Util.compareFeat(feat.last, f._2._2, faetJoin)))
      }).join(parsedUpRdd)
        .map(f => (f._2._1._1, (f._2._1._2, f._2._2))).map(f => f._2._1 + "\t" + f._2._2)
        /*.join(parsedCpRdd)
        .map(f => (f._2._1._1, f._2._1._2, f._2._2)).map(f => f._1 + "\t" + f._2 + "\t" + f._3)*/
    } else {
      joinFaetures.map(f => {
        val feat = f._2._1.split(fieldDelimiter)
        f._1.split(fieldDelimiter).mkString("\t") + "\t" + Util.caculatScore(feat.last) + "\t" + feat.head + "\t" + Util.compareFeat(feat.last, f._2._2, faetJoin)
      })
    }

    if (debugFlag == "true") {
      println(s"event featurelog compareOut count => ${compareOut.count()}")
    }

    rddContext += (constant_lr_feature_consist_monitor_rdd -> compareOut)
    rddContext += (constant_lr_feature_consist_rdd -> compareOut.map(_.toString))
  }

}
