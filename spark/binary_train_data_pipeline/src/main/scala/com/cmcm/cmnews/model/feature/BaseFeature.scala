package com.cmcm.cmnews.model.feature

import com.cmcm.cmnews.model.util.JsonUtil

/**
  * Created by lilonghua on 2016/10/28.
  */
case class BaseFeature (

                         category: List[(String,Double)] = List.empty[(String,Double)],
                         keyword: List[(String,Double)] = List.empty[(String,Double)],
                         topic: List[(String,Double)] = List.empty[(String,Double)],
                         entity: List[(String,Double)] = List.empty[(String,Double)],
                         imageLabel: List[(String,Double)] = List.empty[(String,Double)],
                        pkg: List[(String, Double)] = List.empty[(String, Double)]
                       ) extends Serializable {

  override def toString: String = {
    JsonUtil.toJson(Map(
      "category" -> category,
      "keyword" -> keyword,
      "topic" -> topic,
      "entity" -> entity,
      "imageLabel" -> imageLabel,
      "package" -> pkg)
    )
  }
}


/*
object BaseFeature extends Serializable {

  def apply(
             category: List[(String,Double)],
             keyword: List[(String,Double)],
             topic: List[(String,Double)],
             entity: List[(String,Double)]
           ): BaseFeature = new BaseFeature(category, keyword, topic, entity)
}*/
