package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.processor.UPNewProcessor
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{JsonUtil, Parameters}
import org.apache.spark.rdd.RDD

import scala.util.Try

/**
  * Created by lilonghua on 16/9/22.
  */
trait UPNewFilterCompute extends EventBatchCompute {
  this: SparkBatchContext =>

  import Parameters._

  override def compute: Unit = {
    super.compute
    logInfo("Start UPNewFilterCompute")

    val parallel = Try(batchContext(constant_parallelism)).getOrElse("100").toInt
    val userFilter = Try(batchContext(constant_up_user_filter)).getOrElse("true").toLowerCase

    //get rdd from path
    val upRdd = getRDD[RDD[String]](constant_user_rdd, this)

    val parsedUpRdd: RDD[String] = UPNewProcessor.process(upRdd, this.getBatchContext).filter(f => {
      val feat = f._2.split(fieldDelimiter)
      //val entityLen = Try(feat(feat.length - 1).toInt).getOrElse(0)
      //val topicLen = Try(feat(feat.length - 2).toInt).getOrElse(0)
      val catLen = Try(feat(feat.length - 3).toInt).getOrElse(0)
      val kwLen = Try(feat(feat.length - 4).toInt).getOrElse(0)
      val flag = f._1.nonEmpty && f._1 != "unknown" && feat.length == 10

      if (userFilter == "true") {
        flag && catLen > 0 && kwLen > 0
      } else {
        flag
      }
    }).reduceByKey((f1, f2) => f1).map(f => {
      val feat = f._2.split(fieldDelimiter)
      val categories = feat(0).split(pairDelimiter).map(cat => {
        val ct = cat.split(keyValueDelimiter)
        Map("name" -> ct(0), "weight" -> Try(ct(1).toDouble).getOrElse(0.0))
      })
      val keywords = feat(1).split(pairDelimiter).map(cat => {
        val ct = cat.split(keyValueDelimiter)
        Map("name" -> ct(0), "weight" -> Try(ct(1).toDouble).getOrElse(0.0))
      })
      val topics = feat(2).split(pairDelimiter).map(cat => {
        val ct = cat.split(keyValueDelimiter)
        Map("name" -> ct(0), "weight" -> Try(ct(1).toDouble).getOrElse(0.0))
      })
      val entities = feat(3).split(pairDelimiter).map(cat => {
        val ct = cat.split(keyValueDelimiter)
        Map("name" -> ct(0), "weight" -> Try(ct(1).toDouble).getOrElse(0.0))
      })
      val mapFeat = Map("uid" -> f._1,
        "gender" -> feat(4),
        "age" -> feat(5),
        "u_cat_len" -> feat(6),
        "u_kw_len" -> feat(7),
        "u_topic_len" -> feat(8),
        "u_entities_v2_len" -> feat(9),
        "categories" -> categories,
        "keywords" -> keywords,
        "topics" -> topics,
        "entities_v2" -> entities
      )

      JsonUtil.toJson(mapFeat)
    }).coalesce(parallel)

    rddContext += (constant_train_rdd -> parsedUpRdd)
  }

}
