package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.processor._
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{JsonUtil, Parameters}
import org.apache.spark.rdd.RDD

import scala.util.Try

/**
  * Created by lilonghua on 16/9/14.
  */

trait UPFilterCompute extends EventBatchCompute {
  this: SparkBatchContext =>

  import Parameters._

  override def compute: Unit = {
    super.compute
    logInfo("Start UPFilterCompute")
    //generate event data from log

    //get rdd from path
    val upRdd = getRDD[RDD[String]](constant_user_rdd, this)

    val parsedUpRdd: RDD[String] = UPProcessor.process(upRdd, this.getBatchContext).filter(f => {
      val feat = f._2.split(fieldDelimiter)
      val catLen = Try(feat(feat.length - 1).toInt).getOrElse(0)
      val kwLen = Try(feat(feat.length - 2).toInt).getOrElse(0)

      f._1.nonEmpty && catLen > 0 && kwLen > 0 && feat.length == 6
    }).map(f => {
      val feat = f._2.split(fieldDelimiter)
      val categories = feat(0).split(pairDelimiter).map(cat => {
        val ct = cat.split(keyValueDelimiter)
        Map("name" -> ct(0), "weight" -> Try(ct(1).toDouble).getOrElse(0.0))
      })
      val keywords = feat(1).split(pairDelimiter).map(cat => {
        val ct = cat.split(keyValueDelimiter)
        Map("name" -> ct(0), "weight" -> Try(ct(1).toDouble).getOrElse(0.0))
      })
      val mapFeat = Map("uid" -> f._1,
        "gender" -> feat(2),
        "age" -> feat(3),
        "u_cat_len" -> feat(4),
        "u_kw_len" -> feat(5),
        "categories" -> categories,
        "keywords" -> keywords
      )

      JsonUtil.toJson(mapFeat)
    })

    rddContext += (constant_train_rdd -> parsedUpRdd)
  }

}