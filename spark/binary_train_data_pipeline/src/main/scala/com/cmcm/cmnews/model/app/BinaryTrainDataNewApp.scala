package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.BinaryTrainDataCompute
import com.cmcm.cmnews.model.input.XFBBinaryTrainDataInput
import com.cmcm.cmnews.model.output.XFBBinaryTrainDataOutput
import com.cmcm.cmnews.model.spark.BinaryTrainDataSparkContext

/**
  * Created by lilonghua on 2016/10/31.
  */
object BinaryTrainDataNewApp extends BatchNewApp
  with BinaryTrainDataSparkContext
  with XFBBinaryTrainDataInput
  with BinaryTrainDataCompute
  with XFBBinaryTrainDataOutput
