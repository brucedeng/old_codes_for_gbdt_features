package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{JsonUtil, LoggingUtils, Parameters}
import org.apache.spark.rdd.RDD
import org.json4s.JsonAST._

import scala.util.{Failure, Success, Try}

/**
  * Created by lilonghua on 2016/10/21.
  */
object FeatureLogProcessor extends Processor {
  this: SparkBatchContext =>

  import Parameters._

  override def process(inputRDD:RDD[String], batchContext: collection.mutable.Map[String,String]): RDD[(String, String)] = {
    val pidFilter = Try(batchContext(constant_event_pid)).getOrElse("")
    val modelTypeFilter = Try(batchContext(constant_feature_model_type)).getOrElse("lr")
    val scenarioFilter = Try(batchContext(constant_feature_scenario)).getOrElse("")
    val predictorIdFilter = Try(batchContext(constant_predictor_id)).getOrElse("")
    val expIdFilter = Try(batchContext(constant_exp_id)).getOrElse("")

    println(s"Filter => scenarioFilter: $scenarioFilter \t predictorIdFilter: $predictorIdFilter \t expIdFilter: $expIdFilter")

    inputRDD.map(preprocess(_, batchContext)).filter(f => f._1.nonEmpty).filter(f => {
      val key = f._1.split(fieldDelimiter)
      val pid = key.last
      val expId = key(key.length - 2)
      val predictorId = key(key.length - 3)
      val scenario = key(key.length - 4)
      /*val  modelType = key.last*/
      val flag = if (scenarioFilter.nonEmpty && predictorIdFilter.nonEmpty && expIdFilter.nonEmpty) {
        scenario.matches(scenarioFilter) && predictorId.matches(predictorIdFilter) && expId.matches(expIdFilter)
      } else if (scenarioFilter.nonEmpty && predictorIdFilter.nonEmpty && expIdFilter.isEmpty) {
        scenario.matches(scenarioFilter) && predictorId.matches(predictorIdFilter)
      } else if (scenarioFilter.nonEmpty && predictorIdFilter.isEmpty && expIdFilter.nonEmpty) {
        scenario.matches(scenarioFilter) && expId.matches(expIdFilter)
      } else if (scenarioFilter.isEmpty && predictorIdFilter.nonEmpty && expIdFilter.nonEmpty) {
        predictorId.matches(predictorIdFilter) && expId.matches(expIdFilter)
      } else if (scenarioFilter.nonEmpty && predictorIdFilter.isEmpty && expIdFilter.isEmpty) {
        scenario.matches(scenarioFilter)
      } else if (scenarioFilter.isEmpty && predictorIdFilter.isEmpty && expIdFilter.nonEmpty) {
        expId.matches(expIdFilter)
      } else if (scenarioFilter.isEmpty && predictorIdFilter.nonEmpty && expIdFilter.isEmpty) {
        predictorId.matches(predictorIdFilter)
      } else {
        true
      }

      (pidFilter.split(",").contains(pid) || pidFilter.trim.isEmpty) && flag
    }).flatMap(f => {
      val key = f._1.split(fieldDelimiter)
      f._2.split(recordDelimiter).map(ft => {
        val t = ft.split(pairDelimiter).map(m => {
          val mt = m.split(keyValueDelimiter)
          if (mt.length < 2) {
            (mt(0), "")
          } else {
            (mt(0), mt.last)
          }
        }).toMap
        ((key(0), t.getOrElse("id", "")), (key(1), t.getOrElse("model_type", ""), t.getOrElse("gbdt_score", ""), t.getOrElse("lr_feas", "")))
      })
        .filter(f => f._2._4.nonEmpty && f._1._2.nonEmpty && f._2._2.matches(modelTypeFilter))
        .map(f => (f._1._1 + fieldDelimiter + f._1._2, (f._2._1, f._2._3 + fieldDelimiter + f._2._4)))
    }).reduceByKey((l,r) => {
      val lf = l._1
      val rf = r._1
      if (lf.toLong > rf.toLong) {
        l
      } else {
        r
      }
    }).map(f => (f._1, f._2._2))
  }

  override def preprocess(line: String, batchContext: collection.mutable.Map[String, String]) = {

    Try(JsonUtil.convertToJValue(line)) match {
      case Success(jvalue) =>
        val rid = extractStr(jvalue, "req_id")
        val pid = extractStr(jvalue, "prod_id")
        val predictorId = extractStr(jvalue, "predictor_id")
        val expId = extractStr(jvalue, "exp_id")
        val scenario = extractStr(jvalue, "scenario")
        val ts = extractStr(jvalue, "now", "0").toLong
        val docSize = extractStr(jvalue, "doc_size", "0").toLong
        val uuid = extractStr(jvalue, "uid", "")

        val key = uuid + fieldDelimiter + ts + fieldDelimiter + scenario + fieldDelimiter + predictorId + fieldDelimiter + expId + fieldDelimiter + pid
        val vaule = jvalue \ "docs" match {
          case JArray(s) => elementAssist(s, Array("id", "model_type", "gbdt_score", "lr_feas"))
          case _ => ""
        }

        if (vaule.nonEmpty) {
          (key , vaule)
        } else {
          ("", "")
        }
      case Failure(ex) =>
        LoggingUtils.loggingError(LoggingUtils.getException(ex))
        ("", "")

    }
  }

  def extFeatures(jvalue: List[(String, Any)]): String = {
    jvalue.map(f => {
      f._1.split('^').head + keyGap + Try(f._2.asInstanceOf[JDouble].num.toFloat).getOrElse(0f)
    }).mkString(pairGap)
  }

  def features(jvalue: JValue): String = {
    jvalue.asInstanceOf[Map[String, Any]].map(f => f._1 + keyGap + Try(f._2.toString.toFloat).getOrElse(0f)).mkString(pairGap)
  }

  def elementAssist(jsonItems: List[JValue], keys: Array[String], default: String = ""): String = {
    def parse_item(item: JValue) = {
      keys.map(k => {
        val values = item \ k match {
          case JDouble(x) =>
            x.toDouble
          case JInt(x) =>
            x.toDouble
          case JString(x) =>
            x
          case JArray(x) =>
            catKwAssist(x)
          case JObject(x) =>
            extFeatures(x)
            //x.toString
          case _ =>
            default
        }
        //(k, values)
        k + keyValueDelimiter + values
      }).mkString(pairDelimiter)
    }
    jsonItems.map(x => parse_item(x)).mkString(recordDelimiter)
  }

  def catKwAssist(jsonItems: List[JValue]): String = {
    def parse_item(item: JValue) = {
      /*Try(item.asInstanceOf[Map[String, Any]].map(f => f._1 + keyGap + Try(f._2.toString.toFloat).getOrElse(0f)).head) match {
        case Success(s) => s
        case Failure(s) =>
          println("invalid parse =>" + item.toString + "\t" + s)
          ""
      }*/
      item.toString
    }
    jsonItems.map(x => parse_item(x)).mkString(pairGap)
  }

  def extractStr(jvalue: JValue, fieldName: String, default: String = "") = {
    jvalue \ fieldName match {
      case JString(s) => if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString
      case JDecimal(s) => s.toString
      case JDouble(s) => s.toString
      case _ => default
    }
  }

  def main(args:Array[String]) = {
    val key = "{\"aid\":\"567738DD-2F5E-491A-A8C5-A13D23C5FB9E\",\"uid\":\"567738DD-2F5E-491A-A8C5-A13D23C5FB9E\",\"version\":\"104\",\"gender\":\"\",\"age\":\"\",\"u_cat_len\":22,\"u_kw_len\":5466,\"categories\":[{\"name\":\"1000028\",\"weight\":0.160144},{\"name\":\"1000922\",\"weight\":0.121802},{\"name\":\"6000007\",\"weight\":0.115555},{\"name\":\"1000323\",\"weight\":0.101347},{\"name\":\"1000707\",\"weight\":0.068718},{\"name\":\"1000299\",\"weight\":0.068550},{\"name\":\"1000930\",\"weight\":0.047343},{\"name\":\"1001117\",\"weight\":0.045212},{\"name\":\"1000992\",\"weight\":0.041422},{\"name\":\"1000713\",\"weight\":0.041181},{\"name\":\"1000849\",\"weight\":0.040873},{\"name\":\"1000802\",\"weight\":0.038363},{\"name\":\"1000979\",\"weight\":0.022069},{\"name\":\"1000798\",\"weight\":0.019345},{\"name\":\"1000667\",\"weight\":0.015249},{\"name\":\"1000031\",\"weight\":0.012236},{\"name\":\"1001039\",\"weight\":0.010667},{\"name\":\"1000780\",\"weight\":0.010556},{\"name\":\"1000073\",\"weight\":0.008431},{\"name\":\"1000069\",\"weight\":0.006201},{\"name\":\"1000393\",\"weight\":0.004228},{\"name\":\"1000742\",\"weight\":0.000508}],\"keywords\":[{\"name\":\"पत्नी\",\"weight\":0.009350},{\"name\":\"मौत\",\"weight\":0.009187},{\"name\":\"महिला\",\"weight\":0.008703},{\"name\":\"शादी\",\"weight\":0.008396},{\"name\":\"बॉलीवुड\",\"weight\":0.008375},{\"name\":\"पुलिस\",\"weight\":0.008334},{\"name\":\"दिल्ली\",\"weight\":0.008166},{\"name\":\"चीन\",\"weight\":0.008107},{\"name\":\"वीडियो\",\"weight\":0.008007},{\"name\":\"तुर्की\",\"weight\":0.007983},{\"name\":\"पति\",\"weight\":0.007960},{\"name\":\"लड़की\",\"weight\":0.007791},{\"name\":\"समय\",\"weight\":0.007755},{\"name\":\"तस्वीरें\",\"weight\":0.007558},{\"name\":\"पाकिस्तान\",\"weight\":0.007411},{\"name\":\"भारत\",\"weight\":0.007241},{\"name\":\"देखें\",\"weight\":0.007232},{\"name\":\"फोटो\",\"weight\":0.007131},{\"name\":\"रेप\",\"weight\":0.007099},{\"name\":\"अमेरिका\",\"weight\":0.006907},{\"name\":\"देश\",\"weight\":0.006664},{\"name\":\"फ्रांस\",\"weight\":0.006605},{\"name\":\"जानकारी\",\"weight\":0.006605},{\"name\":\"दुनिया\",\"weight\":0.006563},{\"name\":\"सोशल_मीडिया\",\"weight\":0.006548},{\"name\":\"प्यार\",\"weight\":0.006542},{\"name\":\"लड़कियां\",\"weight\":0.006542},{\"name\":\"शहर\",\"weight\":0.006425},{\"name\":\"कोशिश\",\"weight\":0.006276},{\"name\":\"शेयर\",\"weight\":0.006206},{\"name\":\"सीरिया\",\"weight\":0.006110},{\"name\":\"बेटी\",\"weight\":0.006095},{\"name\":\"संबंध\",\"weight\":0.006077},{\"name\":\"हत्या\",\"weight\":0.006072},{\"name\":\"महिलाएं\",\"weight\":0.006069},{\"name\":\"अनुसार\",\"weight\":0.005835},{\"name\":\"जिंदगी\",\"weight\":0.005833},{\"name\":\"कार\",\"weight\":0.005813},{\"name\":\"गर्लफ्रेंड\",\"weight\":0.005811},{\"name\":\"वायरल\",\"weight\":0.005746},{\"name\":\"क्रिकेट\",\"weight\":0.005734},{\"name\":\"नजर\",\"weight\":0.005709},{\"name\":\"हैरान\",\"weight\":0.005695},{\"name\":\"देखे\",\"weight\":0.005686},{\"name\":\"लड़की\",\"weight\":0.005646},{\"name\":\"फेसबुक\",\"weight\":0.005620},{\"name\":\"बंद\",\"weight\":0.005619},{\"name\":\"सुबह\",\"weight\":0.005617},{\"name\":\"विराट_कोहली\",\"weight\":0.005606},{\"name\":\"सेक्स\",\"weight\":0.005585},{\"name\":\"जर्मनी\",\"weight\":0.005547},{\"name\":\"फिल्म\",\"weight\":0.005517},{\"name\":\"सुरक्षा\",\"weight\":0.005505},{\"name\":\"दिल\",\"weight\":0.005493},{\"name\":\"गिरफ्तार\",\"weight\":0.005488},{\"name\":\"लड़कियों\",\"weight\":0.005459},{\"name\":\"ट्रेन\",\"weight\":0.005459},{\"name\":\"हमला\",\"weight\":0.005405},{\"name\":\"रोनाल्डो\",\"weight\":0.005384},{\"name\":\"शराब\",\"weight\":0.005382},{\"name\":\"दोस्त\",\"weight\":0.005371},{\"name\":\"राज\",\"weight\":0.005336},{\"name\":\"कहानी\",\"weight\":0.005316},{\"name\":\"सोना\",\"weight\":0.005315},{\"name\":\"इंटरनेट\",\"weight\":0.005291},{\"name\":\"कंदील\",\"weight\":0.005276},{\"name\":\"संग\",\"weight\":0.005240},{\"name\":\"मां\",\"weight\":0.005236},{\"name\":\"तस्वीर\",\"weight\":0.005231},{\"name\":\"पाक\",\"weight\":0.005218},{\"name\":\"सरकार\",\"weight\":0.005209},{\"name\":\"करती_हैं\",\"weight\":0.005162},{\"name\":\"अनुष्का_शर्मा\",\"weight\":0.005146},{\"name\":\"इतिहास\",\"weight\":0.005133},{\"name\":\"सलमान_खान\",\"weight\":0.005107},{\"name\":\"सूचना\",\"weight\":0.005085},{\"name\":\"यहाँ\",\"weight\":0.005068},{\"name\":\"राजस्थान\",\"weight\":0.005068},{\"name\":\"सलमान\",\"weight\":0.005025},{\"name\":\"रिपोर्ट\",\"weight\":0.005006},{\"name\":\"पुरुष\",\"weight\":0.004979},{\"name\":\"मुस्लिम\",\"weight\":0.004976},{\"name\":\"मस्ती\",\"weight\":0.004938},{\"name\":\"बराक_ओबामा\",\"weight\":0.004925},{\"name\":\"राष्ट्रपति\",\"weight\":0.004906},{\"name\":\"खूबसूरत\",\"weight\":0.004843},{\"name\":\"पढ़ें\",\"weight\":0.004835},{\"name\":\"आईएस\",\"weight\":0.004811},{\"name\":\"अभिनेत्री\",\"weight\":0.004787},{\"name\":\"मार_डाला\",\"weight\":0.004784},{\"name\":\"ममता_कुलकर्णी\",\"weight\":0.004775},{\"name\":\"देखिए\",\"weight\":0.004760},{\"name\":\"होश\",\"weight\":0.004741},{\"name\":\"लड़कियां\",\"weight\":0.004717},{\"name\":\"हैरान_रह\",\"weight\":0.004693},{\"name\":\"धोनी\",\"weight\":0.004665},{\"name\":\"अवैध_संबंध\",\"weight\":0.004648},{\"name\":\"हॉट\",\"weight\":0.004617},{\"name\":\"किम_कार्दशियन\",\"weight\":0.004616},{\"name\":\"हाथ\",\"weight\":0.004607},{\"name\":\"मीडिया\",\"weight\":0.004593},{\"name\":\"रात_में\",\"weight\":0.004568},{\"name\":\"कश्मीर\",\"weight\":0.004560},{\"name\":\"कपड़े\",\"weight\":0.004490},{\"name\":\"जाते_हैं\",\"weight\":0.004479},{\"name\":\"बम\",\"weight\":0.004479},{\"name\":\"करीब\",\"weight\":0.004476},{\"name\":\"पानी\",\"weight\":0.004475},{\"name\":\"हमले\",\"weight\":0.004475},{\"name\":\"फोटोज\",\"weight\":0.004419},{\"name\":\"इस्लामिक_स्टेट\",\"weight\":0.004359},{\"name\":\"भोपाल\",\"weight\":0.004356},{\"name\":\"बहू\",\"weight\":0.004351},{\"name\":\"जीवन\",\"weight\":0.004338},{\"name\":\"शादी_से_पहले\",\"weight\":0.004334},{\"name\":\"खुलासा\",\"weight\":0.004328},{\"name\":\"शरीर\",\"weight\":0.004327},{\"name\":\"विडियो\",\"weight\":0.004303},{\"name\":\"मन\",\"weight\":0.004300},{\"name\":\"विमान\",\"weight\":0.004296},{\"name\":\"मदद\",\"weight\":0.004286},{\"name\":\"तलाक\",\"weight\":0.004284},{\"name\":\"मर्दानगी\",\"weight\":0.004281},{\"name\":\"सऊदी_अरब\",\"weight\":0.004277},{\"name\":\"बेटे\",\"weight\":0.004273},{\"name\":\"शादी_के_बाद\",\"weight\":0.004271},{\"name\":\"सिंह\",\"weight\":0.004265},{\"name\":\"चौंक_जाएंगे\",\"weight\":0.004250},{\"name\":\"ईद\",\"weight\":0.004242},{\"name\":\"रोमांस\",\"weight\":0.004240},{\"name\":\"बच्चों\",\"weight\":0.004240},{\"name\":\"मार\",\"weight\":0.004239},{\"name\":\"मर्द\",\"weight\":0.004226},{\"name\":\"नौकरी\",\"weight\":0.004223},{\"name\":\"पूनम_पांडे\",\"weight\":0.004217},{\"name\":\"सोने\",\"weight\":0.004217},{\"name\":\"उत्तर_प्रदेश\",\"weight\":0.004212},{\"name\":\"मंदिर\",\"weight\":0.004210},{\"name\":\"बोले\",\"weight\":0.004208},{\"name\":\"हवस\",\"weight\":0.004178},{\"name\":\"उम्मीद\",\"weight\":0.004171},{\"name\":\"देहरादून\",\"weight\":0.004149},{\"name\":\"देखिये\",\"weight\":0.004149},{\"name\":\"सोच\",\"weight\":0.004148},{\"name\":\"घर_में\",\"weight\":0.004144},{\"name\":\"बीवी\",\"weight\":0.004134},{\"name\":\"लड़कियों\",\"weight\":0.004124},{\"name\":\"क्रिकेटर\",\"weight\":0.004096},{\"name\":\"जो_रूट\",\"weight\":0.004087},{\"name\":\"मरे\",\"weight\":0.004068},{\"name\":\"जी\",\"weight\":0.004067},{\"name\":\"गन्दा_काम\",\"weight\":0.004064},{\"name\":\"जबलपुर\",\"weight\":0.004062},{\"name\":\"बिकिनी\",\"weight\":0.004052},{\"name\":\"गेम\",\"weight\":0.004040},{\"name\":\"बेटा\",\"weight\":0.004031},{\"name\":\"अक्षय_कुमार\",\"weight\":0.003999},{\"name\":\"बहन\",\"weight\":0.003993},{\"name\":\"दुबई\",\"weight\":0.003993},{\"name\":\"छात्र\",\"weight\":0.003989},{\"name\":\"कपिल_शर्मा\",\"weight\":0.003983},{\"name\":\"मर्डर\",\"weight\":0.003980},{\"name\":\"तख्तापलट\",\"weight\":0.003979},{\"name\":\"रेड_लाइट_एरिया\",\"weight\":0.003975},{\"name\":\"इस्लाम\",\"weight\":0.003938},{\"name\":\"हार\",\"weight\":0.003931},{\"name\":\"अपनाएं_ये\",\"weight\":0.003927},{\"name\":\"धोखा\",\"weight\":0.003907},{\"name\":\"देती_है\",\"weight\":0.003891},{\"name\":\"घटना\",\"weight\":0.003886},{\"name\":\"रहता_है\",\"weight\":0.003873},{\"name\":\"आरोप\",\"weight\":0.003873},{\"name\":\"तस्वीरों_में\",\"weight\":0.003854},{\"name\":\"तीसरा\",\"weight\":0.003840},{\"name\":\"फोन\",\"weight\":0.003830},{\"name\":\"बारिश\",\"weight\":0.003826},{\"name\":\"समर्थन\",\"weight\":0.003822},{\"name\":\"सैनिक\",\"weight\":0.003772},{\"name\":\"बाप\",\"weight\":0.003769},{\"name\":\"भाई_ने_किया\",\"weight\":0.003760},{\"name\":\"उम्र\",\"weight\":0.003756},{\"name\":\"पहचान\",\"weight\":0.003737},{\"name\":\"मारी\",\"weight\":0.003735},{\"name\":\"परेशान\",\"weight\":0.003723},{\"name\":\"ओसामा_बिन_लादेन\",\"weight\":0.003719},{\"name\":\"साली\",\"weight\":0.003719},{\"name\":\"सपना\",\"weight\":0.003711},{\"name\":\"सेना\",\"weight\":0.003710},{\"name\":\"काबुल\",\"weight\":0.003710},{\"name\":\"इराक\",\"weight\":0.003710},{\"name\":\"कबाली\",\"weight\":0.003696},{\"name\":\"टीम_इंडिया\",\"weight\":0.003684},{\"name\":\"लखनऊ\",\"weight\":0.003682},{\"name\":\"मजाक\",\"weight\":0.003678},{\"name\":\"पेरिस\",\"weight\":0.003674},{\"name\":\"सनी_लियोन\",\"weight\":0.003671},{\"name\":\"कैद\",\"weight\":0.003656},{\"name\":\"आत्महत्या\",\"weight\":0.003655},{\"name\":\"एक्ट्रेस\",\"weight\":0.003655},{\"name\":\"बांग्लादेश\",\"weight\":0.003646}],\"prefer_hindi\":\"1\",\"new_user\":\"0\",\"city\":\"16_Pune\",\"neg_categories\":\"{\"name\":\"1000681\",\"weight\":3.028129}\"}"
    val bg = key.trim.indexOf("\"neg_categories\":\"")
    val end = key.trim.indexOf("}\"",bg)

    println(key.trim.substring(0,bg - 1) + key.trim.substring(end + 2,key.trim.length))
    println(bg.toString + "\t" + end.toString)
  }

}
