package com.cmcm.cmnews.model.spark

//import com.cmcm.cmnews.model.feature.{BaseFeature, ContentFeature, EventFeature, UserFeature}
import com.cmcm.cmnews.model.util.Parameters
import org.apache.spark.{SparkConf, SparkContext}

//import scala.collection.immutable.HashMap
import scala.util.Try

/**
  * Created by lilonghua on 2016/10/28.
  */
trait UpHitrateSparkContext extends SparkBatchContext {
  import Parameters._

  override def init(configDir: String = ""): Unit ={
    super.init(configDir)
    logInfo("Start BinaryTrainDataSparkContext...")
    val sparkConf = new SparkConf()
      /*.registerKryoClasses(Array(
        classOf[BaseFeature],
        classOf[EventFeature],
        classOf[UserFeature],
        classOf[ContentFeature],
        classOf[List[(String, Double)]],
        classOf[HashMap[String, Int]],
        classOf[HashMap[String, List[(String,Double)]]],
        classOf[HashMap[String, String]]))*/
      .set("spark.serializer","org.apache.spark.serializer.KryoSerializer")
      .set("spark.default.parallelism", Try(batchContext(constant_parallelism)).getOrElse("32"))
    //.setAppName(Try(batchContext(constant_jobName)).getOrElse(this.getClass.getName))
    sbc = new SparkContext(sparkConf)
  }

}
