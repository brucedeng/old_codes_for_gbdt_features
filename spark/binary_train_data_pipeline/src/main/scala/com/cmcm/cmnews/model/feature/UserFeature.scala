package com.cmcm.cmnews.model.feature

import com.cmcm.cmnews.model.util.JsonUtil

/**
  * Created by lilonghua on 2016/10/28.
  */

case class UserFeature (baseFeature: BaseFeature,
                        notifyEntity: List[(String,Double)] = List.empty[(String,Double)],
                        catLen: Int,
                        keyLen: Int,
                        topicLen: Int,
                        entityLen: Int,
                        imageLabelLen: Int,
                        gender: String,
                        age: String,
                        aid: String
                      ) extends Serializable {

  override def toString: String = {
    JsonUtil.toJson(Map("category" -> baseFeature.category,
      "keyword" -> baseFeature.keyword,
      "topic" -> baseFeature.topic,
      "entity" -> baseFeature.entity,
      "imageLabel" -> baseFeature.imageLabel,
      "notifyEntity" -> notifyEntity,
      "catLen" -> catLen,
      "keyLen" -> keyLen,
      "topicLen" -> topicLen,
      "entityLen" -> entityLen,
      "imageLabelLen" -> imageLabelLen,
      "gender" -> gender,
      "age" -> age,
      "aid" -> aid))
  }
}
