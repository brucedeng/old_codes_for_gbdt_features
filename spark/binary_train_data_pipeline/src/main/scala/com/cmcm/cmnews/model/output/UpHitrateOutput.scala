package com.cmcm.cmnews.model.output

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.rdd.RDD


/**
 * Created by tangdong on 1/5/16.
 */
trait UpHitrateOutput extends EventBatchOutput {
  this: SparkBatchContext =>
  import Parameters._

  override def outPut: Unit ={
    super.outPut
    logInfo("Start XfbTrainDataOutput...")
    val cateCoverageOut = rddContext(constant_cate_coverage_rdd).asInstanceOf[RDD[String]]
    cateCoverageOut.saveAsTextFile(batchContext(constant_cate_coverage_output), classOf[GzipCodec])
  }
}
