package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.feature.{BaseFeature, ContentFeature}
import com.cmcm.cmnews.model.util.{JsonUtil, LoggingUtils, Util}
import com.cmcm.cmnews.model.util.Parameters._
import org.apache.spark.rdd.RDD
import org.json4s.JsonAST._

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.{Failure, Success, Try}

/**
  * Created by lilonghua on 2016/10/28.
  */
object ContentProcessor extends ProcessorAbstract[Option[ContentFeature]]{

  override def preprocess(line: String, batchContext: mutable.Map[String, String]): Option[ContentFeature] = {
    val categoryVersion = Try(batchContext(constant_cat_version)).getOrElse("v4")
    val keywordVersion =  Try(batchContext(constant_kw_version)).getOrElse("v2")
    val topicVersion =  Try(batchContext(constant_topic_version)).getOrElse("v2")
    val topRelCat =  Try(batchContext(constant_d_rel_ncat)).getOrElse("200").toInt
    val topRelKw = Try(batchContext(constant_d_rel_nkey)).getOrElse("200").toInt
    val topRelTopic = Try(batchContext(constant_d_rel_ntopic)).getOrElse("200").toInt
    val topRelTitleEntity = Try(batchContext(constant_d_rel_ntitle_entity)).getOrElse("200").toInt
    val topRelDisplayEntity = Try(batchContext(constant_d_rel_ndisplay_entity)).getOrElse("200").toInt
    val l1_norm = Try(batchContext(constant_l1_norm)).getOrElse("false").toLowerCase
    val l1Flag = if(l1_norm=="yes"||l1_norm=="true") true else false

    Try(JsonUtil.convertToJValue(line)) match{
      case Success(jvalue) =>
        val cid = if ( keywordVersion == "oversea" )
          JsonUtil.getStr(jvalue,"item_id")
        else
          JsonUtil.getStr(jvalue,"content_id")
        val groupId = JsonUtil.getStr(jvalue,"group_id")
        val tier = JsonUtil.getStr(jvalue,"tier")
        val publisher = JsonUtil.getStr(jvalue,"publisher")
        val publishTime = Try(JsonUtil.getStr(jvalue,"publish_time", "0").toLong).getOrElse(0l)
        val updateTime = JsonUtil.getStr(jvalue,"update_time", "0")

        val wordCount = Try(JsonUtil.getStr(jvalue, "word_count", Util.defaultNu.toString).toInt).getOrElse(Util.defaultNu)
        val imageCount = Try(JsonUtil.getStr(jvalue, "image_count", Util.defaultNu.toString).toInt).getOrElse(Util.defaultNu)
        val newsyScore = Try(JsonUtil.getStr(jvalue, "newsy_score", Util.defaultNu.toString).toFloat).getOrElse(Util.defaultNu.toFloat)
        val titlemd5 = JsonUtil.getStr(jvalue,"title_md5")
        val mediaLevel = Try(JsonUtil.getStr(jvalue,"media_level", Util.defaultNu.toString).toInt).getOrElse(Util.defaultNu)
        val commentCount = Try(JsonUtil.getStr(jvalue,"comment_count", Util.defaultNu.toString).toInt).getOrElse(Util.defaultNu)
        val hasBigImg = Try(JsonUtil.getStr(jvalue,"has_big_img", Util.defaultNu.toString).toInt).getOrElse(0)

        val listImageCount = Try(JsonUtil.getStr(jvalue, "list_image_count", Util.defaultNu.toString).toInt).getOrElse(Util.defaultNu)
        val adultScore = Try(JsonUtil.getStr(jvalue, "adult_score", Util.defaultNu.toString).toFloat).getOrElse(Util.defaultNu.toFloat)
        val quality = Try(JsonUtil.getStr(jvalue, "quality", Util.defaultNu.toString).toFloat).getOrElse(Util.defaultNu.toFloat)
        val title = JsonUtil.getStr(jvalue,"title")
        val titleType = Try(JsonUtil.getStr(jvalue, "title_type", Util.defaultNu.toString).toInt).getOrElse(Util.defaultNu)
        val up = Try(JsonUtil.getStr(jvalue, "up", Util.defaultNu.toString).toInt).getOrElse(Util.defaultNu)
        val down = Try(JsonUtil.getStr(jvalue, "down", Util.defaultNu.toString).toInt).getOrElse(Util.defaultNu)
        if (wordCount < 100000) {
          val category = if ( categoryVersion == "oversea" ){
            jvalue \ "categories" match {
              case JArray(arr) =>
                overseaCatKwAssist(arr, topRelCat ,l1Flag)
              case _ => List.empty[(String, Double)]
            }
          } else if( categoryVersion == "oversea_l2") {
            jvalue \ "l2_categories" match {
              case JArray(arr) =>
                overseaCatKwAssist(arr, topRelCat ,l1Flag)//arr, 200, true
              case _ => List.empty[(String, Double)]
            }
          } else {
            jvalue \ "categories" match {
              case JArray(arr) =>
                catKwAssist(arr, categoryVersion, List.empty[(String, Double)]).sortWith(_._2 > _._2).take(topRelCat)
              case _ => List.empty[(String, Double)]
            }
          }

          val keyword = if ( keywordVersion == "oversea" ){
            jvalue \ "entities" match {
              case JArray(arr) =>
                overseaCatKwAssist(arr, topRelKw, l1Flag)
              case _ => List.empty[(String, Double)]
            }
          } else {
            jvalue \ "keywords" match {
              case JArray(arr) =>
                catKwAssist(arr, keywordVersion, List.empty[(String, Double)]).sortWith(_._2 > _._2).take(topRelKw)
              case _ => List.empty[(String, Double)]
            }
          }

          val topic = if ( topicVersion == "oversea" ){
            jvalue \ "topics" match {
              case JArray(arr) =>
                overseaCatKwAssist(arr, topRelTopic, l1Flag)
              case _ => List.empty[(String, Double)]
            }
          } else {
            List.empty[(String, Double)]
          }

          val titleEntities = jvalue \ "title_entities" match {
            case JArray(arr) =>
              overseaCatKwAssist(arr, topRelTitleEntity, l1Flag)
            case _ => List.empty[(String, Double)]
          }

          val displayEntities = jvalue \ "display_entities" match {
            case JArray(arr) =>
              overseaCatKwAssist(arr, topRelDisplayEntity, l1Flag)
            case _ => List.empty[(String, Double)]
          }


          val imageLabel = jvalue \ "image_label" match {
              case JArray(arr) =>
                imageTopicAssist(arr)
              case JString(arr) =>
                imageTopicAssistString(arr)
              case _ => List.empty[(String, Double)]
            }

          val feature = ContentFeature(
            BaseFeature(category,keyword,topic, List.empty[(String, Double)], imageLabel),
            titleEntities,
            displayEntities,
            publisher,
            publishTime,
            groupId,
            titlemd5,
            mediaLevel,
            commentCount,
            hasBigImg,
            tier,
            wordCount,
            imageCount,
            newsyScore,
            cid
          )
          Option(feature)
        } else {
          None
        }
      case Failure(ex) =>
        LoggingUtils.loggingError(LoggingUtils.getException(ex))
        None
    }
  }

  override def process(inputRDD: RDD[String], batchContext: mutable.Map[String, String]): RDD[Option[ContentFeature]] = {
    val parallelMax = Try(batchContext(constant_parallelismMax)).getOrElse("500").toInt
    inputRDD.map(preprocess(_, batchContext)).filter(_.nonEmpty).map(f => (f.get.cid, f))
      .reduceByKey((left,right) => {
        if (left.get.publishTime < right.get.publishTime)
          right
        else
          left
      },parallelMax * 2).map(_._2)
  }

  def overseaCatKwAssist(jsonItems: List[JValue], top:Int, l1Norm:Boolean) = {
    def parse_item(item: JValue) = {
      Try((item \ "name").asInstanceOf[JString].s) match {
        case Success(name) =>
          val L1_weight = item \ "weight" match {
            case JDouble(x) =>
              x.toDouble
            case JInt(x) =>
              x.toDouble
            case _ =>
              0.0
          }
          (name, L1_weight)
        case Failure(ex) =>
          LoggingUtils.loggingError(LoggingUtils.getException(ex))
          ("unknown", 0.0)
      }
    }
    val data = jsonItems.map(x => parse_item(x)).filter(x => x._1.nonEmpty)
    if (l1Norm) {
      val sumWt = data.map(x => x._2).sum
      val sumWeight = if (sumWt > 0) sumWt else 1.0
      data.map(x => (x._1, x._2 / sumWeight)).sortWith(_._2 > _._2).take(top)
    }
    else
      data.sortWith(_._2 > _._2).take(top)
  }

  def catKwAssist(jsonItems: List[JValue], version:String, default: List[(String, Double)]): List[(String, Double)] = {
    if (jsonItems.isEmpty) default
    else {
      jsonItems.head \ "version" match {
        case JString(v) if v == version => {
          val sb = new ListBuffer[(String, Double)]()
          for (item <- (jsonItems.head \ "list").asInstanceOf[JArray].arr) {
            val name = (item \ "name").asInstanceOf[JString].s
            val L1_weight = (item \ "L1_weight") match {
              case JDouble(x) =>
                x.toDouble
              case JInt(x) =>
                x.toDouble
              case JString(x) =>
                Try(x.toDouble).getOrElse(0.0)
              case _ =>
                0.0
            }
            sb += ((name,L1_weight))
          }
          sb.toList
        }
        case _ => catKwAssist(jsonItems.tail, version, default)
      }
    }
  }

  def imageTopicAssist(jsonItems: List[JValue]): List[(String, Double)] = {
    if (jsonItems.isEmpty) List.empty[(String, Double)]
    else {
      def parse_item(item: JValue) = {
        val name = item.asInstanceOf[JInt].num.toString
        val L1_weight = 0.05
        (name, L1_weight)
      }

      val data = jsonItems.map(x => parse_item(x)).filter(x => x._1.nonEmpty)
      data
    }
  }

  def imageTopicAssistString(Items: String): List[(String, Double)] = {
    if (Items.isEmpty) List.empty[(String, Double)]
    else {
      Items.replace("[","").replace("]","").trim.split(",").map(f => (f.replace(" ", ""),0.05)).toList
    }

  }
}
