package com.cmcm.cmnews.model.feature

import com.cmcm.cmnews.model.config.Config
import com.cmcm.cmnews.model.util.{Parameters, Util}
import org.apache.spark.Logging
import org.apache.spark.rdd.RDD
import org.joda.time.{DateTime, DateTimeZone}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.Try

/**
  * Created by lilonghua on 2016/10/31.
  */
object LRBinaryFeatures extends Serializable with Logging {
  import Parameters._

  def generateFullXFBFeatures(fullFeature: RDD[(EventFeature, Option[UserFeature], ContentFeature)], config:collection.immutable.Map[String,String]) : RDD[((String, String, String, Double), String)] = {
    val typesafeConfig = new Config(config(constant_config_file)).typesafeConfig
    fullFeature.map(genBinaryStaticFeature(_,config)).map(f => (f._1, generateBinaryFeatures(f._2, config, typesafeConfig)))
  }

  def getBinaryFeatureValueRecursive(featureMap:mutable.HashMap[String, List[(String, Double)]],
                                     featureList: List[String],
                                     featJoin: String) : List[(String, Double)] = {
    var featureValues = new ListBuffer[(String, Double)]
    if (featureList.size > 1) {
      val preFeatures = featureMap.getOrElse(featureList.head,List())
      val newFeatureList = featureList.drop(1)
      for (preFeature <- preFeatures) {
        for (posFeature <- getBinaryFeatureValueRecursive(featureMap, newFeatureList, featJoin)) {
          featureValues += ((preFeature._1 + featJoin + posFeature._1, preFeature._2 * posFeature._2))
        }
      }
    } else {
      for (preFeature <- featureMap.getOrElse(featureList.head,List())){
        featureValues += preFeature
      }
    }
    featureValues.toList
  }

  def getMatchBinaryFeatures(binaryFeatures: ListBuffer[(String,String)],
                             featureConfig: String,
                             featureMap: mutable.HashMap[String, List[(String,Double)]],
                             featJoin: String) = {
    for (feature <- featureConfig.split(",")) {
      val fields = feature.split("_")
      val preFeature = fields(1) + "_" + fields(3)
      val posFeature = fields(2) + "_" + fields(4)
      val featureId = fields(5)
      for (value1 <- featureMap.getOrElse(preFeature,List())) {
        for (value2 <- featureMap.getOrElse(posFeature,List())) {
          if (value1._1.equals(value2._1)) {
            binaryFeatures += ((featureId + featJoin + value1._1 + featJoin + value1._1, "%.3f".format(value1._2 * value2._2)))
          }
        }
      }
    }
  }
  def getCrossBinaryFeatures(binaryFeatures: ListBuffer[(String, String)],
                             featureConfig: String,
                             featureMap: mutable.HashMap[String, List[(String,Double)]],
                             featJoin: String) = {
    for(feature <- featureConfig.split(",")){
      var featureList = new ListBuffer[String]
      val fields = feature.split("_")
      var i = 1
      val catNameIndex = fields.length/2
      while(i <= ((fields.length-1)/2)){
        featureList += (fields(i) + "_" + fields(i+catNameIndex-1))
        i = i + 1
      }
      val featureId = fields(fields.length-1)
      for (value <- getBinaryFeatureValueRecursive(featureMap, featureList.toList, featJoin)) {
        binaryFeatures += ((featureId + featJoin + value._1, "%.3f".format(value._2)))
      }
    }
  }
  def getSingleEdgeBinaryFeatures(binaryFeatures: ListBuffer[(String, String)],
                                  featureConfig: String,
                                  featureMap: mutable.HashMap[String, List[(String,Double)]],
                                  featJoin: String) = {
    for (feature <- featureConfig.split(",")){
      val fields = feature.split("_")
      val featureName = fields(0) + "_" + fields(1)
      val featureId = fields(2)

      val values = featureMap.getOrElse(featureName,List())
      for (value <- values) {
        binaryFeatures += ((featureId + featJoin + value._1, "%.3f".format(value._2)))
      }
    }
  }
  def generateBinaryFeatures(featureMap:collection.mutable.HashMap[String, List[(String,Double)]], config:collection.immutable.Map[String,String],typesafeConfig:com.typesafe.config.Config) :String = {
    val binaryFeatures = new ListBuffer[(String, String)]

    val userFeatureConfig = typesafeConfig.getString("cfb.bin_uf")
    val docFeatureConfig = typesafeConfig.getString("cfb.bin_df")
    val crossFeatureConfig = typesafeConfig.getString("cfb.bin_cf")
    val matchFeatureConfig = typesafeConfig.getString("cfb.bin_mf")
    val featJoin = Try(typesafeConfig.getString("cfb.feat_join")).getOrElse("_")
    val featSplit = Try(typesafeConfig.getString("cfb.feat_split")).getOrElse(" ")
    val replaceSplit = Try(typesafeConfig.getString("cfb.replace_split")).getOrElse("_")

    if (userFeatureConfig.nonEmpty) getSingleEdgeBinaryFeatures(binaryFeatures, userFeatureConfig, featureMap, featJoin)
    if (docFeatureConfig.nonEmpty) getSingleEdgeBinaryFeatures(binaryFeatures, docFeatureConfig, featureMap, featJoin)
    if (crossFeatureConfig.nonEmpty) getCrossBinaryFeatures(binaryFeatures, crossFeatureConfig, featureMap, featJoin)
    if (matchFeatureConfig.nonEmpty) getMatchBinaryFeatures(binaryFeatures, matchFeatureConfig, featureMap, featJoin)

    binaryFeatures.map(f => f._1.replace("\n",featSplit).replace(featSplit,replaceSplit)).map(f => if (f.contains(featJoin)) f else f + featJoin).mkString(featSplit)
  }

  def genBinaryStaticFeature(feature:(EventFeature, Option[UserFeature], ContentFeature), config:collection.immutable.Map[String,String]) = {
    val featureMap = new mutable.HashMap[String, List[(String,Double)]]

    val eventFeature = feature._1
    val upFeature = feature._2.getOrElse(UserFeature(BaseFeature(),List.empty[(String,Double)],0,0,0,0,0,"","",""))
    val cpFeature = feature._3

    val dCid = eventFeature.cid
    val uid = eventFeature.aid
    val src = eventFeature.src
    val ts = eventFeature.serverTime
    val position = eventFeature.getPosition
    val net = eventFeature.eventBase.net
    val osVersion = eventFeature.eventBase.osVersion
    val appVersion = eventFeature.eventBase.appVersion
    val platform = eventFeature.eventBase.platform
    val brand = eventFeature.eventBase.brand
    val model = eventFeature.eventBase.model
    val channel = eventFeature.eventBase.channel
    val isHot = eventFeature.eventBase.isHot
    val appIv = eventFeature.eventBase.appIV
    val uCity = eventFeature.eventBase.city
    val dwell = eventFeature.readTime
    val label = eventFeature.getLable
    /*val country = eventFeature.country
    val lan = eventFeature.lan*/
    val lanRegion = eventFeature.getLanRegion
    val uRelCat = upFeature.baseFeature.category
    val uRelKey = upFeature.baseFeature.keyword
    val uRelTopic = upFeature.baseFeature.topic
    val uRelPkg = upFeature.baseFeature.pkg
    val uRelEntity = upFeature.baseFeature.entity
    val uRelImageLabel = upFeature.baseFeature.imageLabel
    val uRelNotifyEntity = upFeature.notifyEntity
    val uGender = upFeature.gender
    val uAge = upFeature.age
    val uCatLen = upFeature.catLen
    val uKeyLen = upFeature.keyLen
    val uTopicLen = upFeature.topicLen
    val uEntityLen = upFeature.entityLen
    val uImageLabelLen = upFeature.imageLabelLen
    val dGroupid = cpFeature.groupID
    val dTier = cpFeature.tier
    val dPublisher = cpFeature.publisher
    val dPublishTime = cpFeature.publishTime
    val dRelCat = cpFeature.baseFeature.category
    val dRelKey = cpFeature.baseFeature.keyword
    val dRelTopic = cpFeature.baseFeature.topic
    val dRelImageLabel = cpFeature.baseFeature.imageLabel
    val wordCount = cpFeature.wordCount
    val imageCount = cpFeature.imageCount
    val newsyScore = cpFeature.newsyScore
    val dTitlemd5 = cpFeature.titleMD5
    val mediaLevel = cpFeature.mediaLevel
    val commentCount = cpFeature.commentCount
    val hasBigImg = cpFeature.hasBigImg
    val dTitleEntities = cpFeature.titleEntity
    val dDisplayEntities = cpFeature.displayEntity

    val catRel = genRelevance(uRelCat,dRelCat)
    val keyRel = genRelevance(uRelKey,dRelKey)
    val topicRel = genRelevance(uRelTopic,dRelTopic)

    val uCat = uRelCat.take(Try(config(constant_u_ncat)).getOrElse("5").toInt)
    val uKey = uRelKey.take(Try(config(constant_u_nkey)).getOrElse("25").toInt)

    val uPkg = uRelPkg.take(config.getOrElse(constant_u_npkg, "15").toInt)
    val uTopic = uRelTopic.take(Try(config(constant_u_ntopic)).getOrElse("20").toInt)
    val uImageLabel = uRelImageLabel.take(Try(config(constant_u_nimage_label)).getOrElse("20").toInt)
    val uEntity = uRelEntity.take(Try(config(constant_u_nentity)).getOrElse("25").toInt)
    val uNotifyEntity = uRelNotifyEntity.take(Try(config(constant_u_nnotify_entity)).getOrElse("10").toInt)
    val dCat = dRelCat.take(Try(config(constant_d_ncat)).getOrElse("5").toInt)
    val dKey = dRelKey.take(Try(config(constant_d_nkey)).getOrElse("15").toInt)
    val dTopic = dRelTopic.take(Try(config(constant_d_ntopic)).getOrElse("10").toInt)
    val dGmp = eventFeature.gmp



    ////////////////////////////////////////////////////////////////////////////////
    //////Add phase1 new features///////
     var uCatTop3 = List[(String,Double)]()
     var uCatTop3Single = ""
     if(uRelCat.nonEmpty)
     {
        uCatTop3 = uRelCat.sortWith((e1,e2)=>
                                     {
                                       if(e1._2 >= e2._2)
                                        true
                                       else
                                        false
                                     }).take(3)
        uCatTop3Single = uCatTop3.map(_._1).mkString("_")
     }
   ////////////////////////////////////////////////////////////////////////////////

    val docAge = Try(math.floor((ts - dPublishTime)/3600.0d).toLong).getOrElse(0l) match {
      case x:Long if x >= 0 => x
      case _ => 0
    }
    val timeTuple = if (ts <= 0) {
      (-1,-1)
    } else{
      val dateD = new DateTime(ts * 1000L).withZone(DateTimeZone.UTC)
      (dateD.getHourOfDay, dateD.getDayOfWeek%7)
    }
    val timeOfDay = timeTuple._1
    val dayOfWeek = timeTuple._2

    if (uCatLen == 0) {
      featureMap.put("U_CATLEN", List(("-1",1.0)))
    } else {
      featureMap.put("U_CATLEN", List(("%.0f".format(math.floor(Try(uCatLen.toFloat).getOrElse(-1.0f) * 1)),1.0)))
    }
    if (uKeyLen == 0) {
      featureMap.put("U_KWLEN", List(("-1",1.0)))
    } else {
      featureMap.put("U_KWLEN", List(("%.0f".format(math.floor(Try(uKeyLen.toFloat).getOrElse(0.0f) * 0.1)),1.0)))
    }
    if (uTopicLen == 0) {
      featureMap.put("U_TOPICLEN", List(("0",1.0)))
    } else {
      featureMap.put("U_TOPICLEN", List(("%.0f".format(math.floor(Try(uTopicLen.toFloat).getOrElse(0.0f) * 0.1)),1.0)))
    }
    if (uEntityLen == 0) {
      featureMap.put("U_ENTITYLEN", List(("0",1.0)))
    } else {
      featureMap.put("U_ENTITYLEN", List(("%.0f".format(math.floor(Try(uEntityLen.toFloat).getOrElse(0.0f) * 0.1)),1.0)))
    }
    if (uImageLabelLen == 0) {
      featureMap.put("U_IMAGELABELLEN", List(("0",1.0)))
    } else {
      featureMap.put("U_IMAGELABELLEN", List(("%.0f".format(math.floor(Try(uImageLabelLen.toFloat).getOrElse(0.0f) * 0.1)),1.0)))
    }

    if(wordCount >= 0) {
      featureMap.put("D_WORDCOUNT", List(("%.0f".format(math.floor(Try(wordCount.toFloat).getOrElse(0.0f) * 0.1)),1.0)))
    }else{
      featureMap.put("D_WORDCOUNT", List(("",1.0)))
    }
    if(imageCount != Util.defaultNu) {
      featureMap.put("D_IMAGECOUNT", List(("%.0f".format(math.floor(Try(imageCount.toFloat).getOrElse(0.0f) * 1)),1.0)))
    }else{
      featureMap.put("D_IMAGECOUNT", List(("",1.0)))
    }
    if(newsyScore != Util.defaultNu.toFloat) {
      featureMap.put("D_NEWSYSCORE", List(("%.0f".format(math.floor(newsyScore * 100)),1.0)))
    }else{
      featureMap.put("D_NEWSYSCORE", List(("",1.0)))
    }
    if(catRel >= 0) {
      featureMap.put("UD_CATREL", List(("%.0f".format(math.floor(catRel * 1000)),1.0)))
    }else{
      featureMap.put("UD_CATREL", List(("",1.0)))
    }
    if(keyRel >= 0) {
      featureMap.put("UD_KWREL", List(("%.0f".format(math.floor(keyRel * 1000)),1.0)))
    }else{
      featureMap.put("UD_KWREL", List(("",1.0)))
    }
    if(topicRel >= 0) {
      featureMap.put("UD_TOPICREL", List(("%.0f".format(math.floor(topicRel * 1000)),1.0)))
    }else{
      featureMap.put("UD_TOPICREL", List(("",1.0)))
    }
    if(docAge >= 0) {
      featureMap.put("D_DOCAGE", List((docAge.toString,1.0)))
    }else{
      featureMap.put("D_DOCAGE", List(("",1.0)))
    }
    if(timeOfDay >= 0) {
      featureMap.put("E_TIMEOFDAY", List((timeOfDay.toString,1.0)))
    }else{
      featureMap.put("E_TIMEOFDAY", List(("",1.0)))
    }
    if(dayOfWeek >= 0){
      featureMap.put("E_DAYOFWEEK", List((dayOfWeek.toString,1.0)))
    }else{
      featureMap.put("E_DAYOFWEEK", List(("",1.0)))
    }

    featureMap.put("E_LANREGION",List((lanRegion,1.0)))

    featureMap.put("U_UID", List((uid,1.0)))
    featureMap.put("D_SRC", List((src,1.0)))
    featureMap.put("U_CITY", List((uCity,1.0)))
    featureMap.put("U_POSITION", List((position,1.0)))
    featureMap.put("U_GENDER", List((uGender,1.0)))
    featureMap.put("U_AGE", List((uAge,1.0)))
    featureMap.put("U_NET", List((net,1.0)))
    featureMap.put("U_OSVERSION", List((osVersion,1.0)))
    featureMap.put("U_APPVERSION", List((appVersion,1.0)))
    featureMap.put("U_PLATFORM", List((platform,1.0)))
    featureMap.put("U_BRAND", List((brand,1.0)))
    featureMap.put("U_MODEL", List((model,1.0)))
    featureMap.put("U_CHANNEL", List((channel,1.0)))
    featureMap.put("U_ISHOT", List((isHot.toString,1.0)))
    featureMap.put("U_APPIV", List((appIv.toString,1.0)))
    if (uCat.nonEmpty) {
      featureMap.put("U_CATEGORY", uCat)
    }else{
      featureMap.put("U_CATEGORY", List(("",1.0)))
    }
    if (uKey.nonEmpty) {
      featureMap.put("U_KEYWORD", uKey)
    }else{
      featureMap.put("U_KEYWORD", List(("",1.0)))
    }

    if (uTopic.nonEmpty) {
      featureMap.put("U_TOPIC", uTopic)
    }else{
      featureMap.put("U_TOPIC", List(("",1.0)))
    }

    if(uPkg.nonEmpty) {
     featureMap.put("U_PACKAGE", uPkg)
    } else {
      featureMap.put("U_PACKAGE", List(("",1.0)))
    }

    if (uEntity.nonEmpty) {
      featureMap.put("U_ENTITY", uEntity)
    }else{
      featureMap.put("U_ENTITY", List(("",1.0)))
    }

    if (uImageLabel.nonEmpty) {
      featureMap.put("U_IMAGELABEL", uImageLabel)
    }else{
      featureMap.put("U_IMAGELABEL", List(("",1.0)))
    }

    if (uNotifyEntity.nonEmpty) {
      featureMap.put("U_NOTIFYENTITY", uNotifyEntity)
    }else{
      featureMap.put("U_NOTIFYENTITY", List(("",1.0)))
    }
    ////////////////////////////////////////////////////////////////////////////////
    //////Add phase1 new features/////// 
    if(uCatTop3.nonEmpty){
      featureMap.put("U_CATEGORYTOP3",uCatTop3)
    }else{
       featureMap.put("U_CATEGORYTOP3",List(("",1.0)))
    }

    featureMap.put("U_CATEGORYTOP3SINGLE",List((uCatTop3Single,1.0)))

    if(uKey.nonEmpty && dKey.nonEmpty)
    {
      val ukeyname=uKey.map(record=>record._1).toSet
      val dkeyname=dKey.map(record=>record._1).toSet
      val intersection=ukeyname.&(dkeyname).toList.filter(elem=>elem.nonEmpty).map(line=>(line,1.0))
      featureMap.put("UD_KEYWORD", intersection)
    }
    else
    {
      featureMap.put("UD_KEYWORD", List(("",1.0)))
    }

    if(uCat.nonEmpty && dCat.nonEmpty)
    {
      val ucatname = uCat.map(_._1).toSet
      val dcatname = dCat.map(_._1).toSet
      val intersectionCat=ucatname.&(dcatname).toList.filter(_.nonEmpty).map(line=>(line,1.0))
      featureMap.put("UD_CATEGORY", intersectionCat)
    }
    else
    {
      featureMap.put("UD_CATEGORY", List(("",1.0)))
    }

    val gmplo = Try(config(constant_gmpLowerBound)).getOrElse("0.0").toDouble
    val gmpup = Try(config(constant_gmpUpperBound)).getOrElse("1.0").toDouble
    val gmpintv = Try(config(constant_n_gmpInterval)).getOrElse("200").toDouble
    val gmpScore = dGmp.getGmpScore(gmpup, gmplo)
    val gmpVersion= dGmp.getGmpVersion.toInt
    if( gmpScore > -0.5 && gmpVersion > -1)
    {
        val score:Int = math.floor(((gmpScore-gmplo)/(gmpup - gmplo))*gmpintv).toInt
        val gmpVal:String = score.toString+"_"+ math.max(gmpVersion,0).toString
        featureMap.put("D_GMPSCORE", List((gmpVal,1.0)))
    }
    else
      featureMap.put("D_GMPSCORE", List(("",1.0)))
    ////////////////////////////////////////////////////////////////////////////////
    featureMap.put("D_GROUPID", List((dGroupid,1.0)))
    featureMap.put("D_TIER", List((dTier,1.0)))
    featureMap.put("D_TITLEMD5", List((dTitlemd5,1.0)))
    featureMap.put("D_PUBLISHER", List((dPublisher,1.0)))
    if (mediaLevel != Util.defaultNu) {
      featureMap.put("D_MEDIALEVEL", List((mediaLevel.toString,1.0)))
    } else {
      featureMap.put("D_MEDIALEVEL", List(("",1.0)))
    }
    if (commentCount != Util.defaultNu) {
      if (commentCount > 100)
        featureMap.put("D_COMMENTCOUNT", List(("5",1.0)))
      else if (commentCount > 50)
        featureMap.put("D_COMMENTCOUNT", List(("4",1.0)))
      else if (commentCount > 10)
        featureMap.put("D_COMMENTCOUNT", List(("3",1.0)))
      else if (commentCount > 0)
        featureMap.put("D_COMMENTCOUNT", List(("2",1.0)))
      else
        featureMap.put("D_COMMENTCOUNT", List(("1",1.0)))
    } else {
      featureMap.put("D_COMMENTCOUNT", List(("",1.0)))
    }
    if (hasBigImg != Util.defaultNu) {
      featureMap.put("D_HASBIGIMG", List((hasBigImg.toString,1.0)))
    } else {
      featureMap.put("D_HASBIGIMG", List(("",1.0)))
    }
    if (dCat.nonEmpty) {
      featureMap.put("D_CATEGORY", dCat)
    }else{
      featureMap.put("D_CATEGORY", List(("",1.0)))
    }
    if (dKey.nonEmpty) {
      featureMap.put("D_KEYWORD", dKey)
    }else{
      featureMap.put("D_KEYWORD", List(("",1.0)))
    }
    if (dTopic.nonEmpty) {
      featureMap.put("D_TOPIC", dTopic)
    }else{
      featureMap.put("D_TOPIC", List(("",1.0)))
    }
    if (dTitleEntities.nonEmpty) {
      featureMap.put("D_TITLEENTITIES", dTitleEntities)
    }else{
      featureMap.put("D_TITLEENTITIES", List(("",1.0)))
    }
    if (dDisplayEntities.nonEmpty) {
      featureMap.put("D_DISPLAYENTITIES", dDisplayEntities)
    }else{
      featureMap.put("D_DISPLAYENTITIES", List(("",1.0)))
    }
    if (dRelImageLabel.nonEmpty) {
      featureMap.put("D_IMAGELABEL", dRelImageLabel)
    }else{
      featureMap.put("D_IMAGELABEL", List(("",1.0)))
    }
    featureMap.put("D_CONID", List((dCid,1.0)))

    ((uid, dCid, label.toString, dwell), featureMap)
  }


  def genRelevance(u_input: List[(String, Double)],d_input: List[(String, Double)]): Double = {
    if (u_input.isEmpty || d_input.isEmpty) {
      0.0
    } else {
      val u_input_dict = u_input.toMap

      val rel = d_input.toMap.map(f => {
        Try(u_input_dict(f._1)).getOrElse(0.0) * f._2
      }).sum
      if(rel.isNaN || rel.isInfinity) 0.0
      else rel
    }
  }

  def calFeatureWeight(baseData:RDD[(EventFeature, Option[UserFeature], ContentFeature)]) = {
    val (spWt: Double, snWt: Double) = Try(baseData.map(x => {

      val dwell = x._1.readTime
      val label = x._1.getLable

      val wt = math.log(math.max(0.0d, Try(dwell.toDouble).toOption match {
        case Some(t) => t
        case None => 0.0d
      }) + 1.0d)
      val pos_wt = if (label == 1) wt else 0.0d
      val neg_wt = if (label == 1) 0.0d else 1.0d
      (pos_wt, neg_wt)
    }).reduce((x, y) => (x._1 + y._1, x._2 + y._2))).getOrElse((1d,0d))

    val ratio: Double = math.min(snWt / spWt, 10)
    println(s"ratio: $ratio, spWt: $spWt, snWt: $snWt")

    baseData.map(x => {
      val dwell = x._1.readTime
      val label = x._1.getLable
      val wt = math.log(Try(dwell.toDouble).toOption match {
        case Some(t) => t + + 1.0d
        case None => 1.0d
      })

      val weight: Double = if (label == 1) wt * ratio else 1.0d
      (x._1.copy(readTime = weight), x._2, x._3)
    })
  }

  def main(args:Array[String]) = {
    println(math.floor("10.634".toDouble))
    println(math.floor(1234354/3600.0d).toLong)
    println(1234354/3600.0d)
  }
}
