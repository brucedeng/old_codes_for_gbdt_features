package com.cmcm.cmnews.model.component

import java.util.Properties

import com.cmcm.cmnews.model.monitor.MonitorProxy
import com.cmcm.cmnews.model.spark.SparkStreamingContext
import com.cmcm.cmnews.model.util.KafkaSink
import org.apache.spark.Logging
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.streaming.dstream.DStream

/**
  * Created by lilonghua on 2016/11/9.
  */
object ModelComponent extends Logging {
  val componentName = "model_component"
  val configFileName = "model.conf"
  val defaultModelOutputDest = "file"
  val defaultModelOutputFilePrefix = "/tmp/deeplearning_lr/model"
  val defaultPatitionsForFetchingCP = 10
  val defaultDecayWindow = 600
  var monitor: MonitorProxy = _
  var monitorBroadcastVar: Broadcast[MonitorProxy] = _
  var kafkaConf: Broadcast[KafkaSink] = _
}

trait ModelComponent extends StreamingComponent {
  this: SparkStreamingContext with EventComponent =>

  override def init(configDir: String) = {
    super.init(configDir)
    initModelComponent(configDir + "/" + ModelComponent.configFileName)
    ModelComponent.monitor = new MonitorProxy()
    ModelComponent.monitor.init(modelConfigUnit)
    ModelComponent.monitorBroadcastVar = sparkContext.broadcast(ModelComponent.monitor)
    ModelComponent.kafkaConf = sparkContext.broadcast(getKafkas)
  }

  override def preStart() = {
    super.preStart()
  }

  def getKafkas = {
    val broker = modelConfigUnit.getString("model.lr.output.kafka.broker.list", "10.2.2.251:9092,10.2.2.253:9092,10.2.2.5:9092,10.2.2.186:9092,10.2.2.187:9092")
    //val acks = modelConfigUnit.getString("model.lr.output.kafka.producer.type", "all")
    val compression = modelConfigUnit.getString("model.lr.output.kafka.producer.compression", "")
    val retries = modelConfigUnit.getInt("model.lr.output.kafka.producer.retries", 0)
    val batch = modelConfigUnit.getInt("model.lr.output.kafka.producer.batch", 16384)
    val linger = modelConfigUnit.getInt("model.lr.output.kafka.producer.linger", 0)
    val buffer = modelConfigUnit.getLong("model.lr.output.kafka.producer.buffer", 33554432)
    val props = new Properties()
    props.put("bootstrap.servers", broker)
    //props.put("serializer.class", "kafka.serializer.StringEncoder")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    if (compression.nonEmpty) props.put("compression.type", compression)
    //props.put("acks", acks)
    props.put("retries", Int.box(retries))
    props.put("batch.size", Int.box(batch))
    props.put("linger.ms", Int.box(linger))
    props.put("buffer.memory", Long.box(buffer))
    //props.put("producer.type", producerType)
    KafkaSink(props)
    //Map("default" ->(broker, producerType, topic))
  }

  def modelConfigUnit = {
    getConfigUnit(ModelComponent.componentName).get
  }

  def initModelComponent(modelConfigFile: String) = {
    logInfo("Initializing model component...")
    loadComponentConfig(ModelComponent.componentName, modelConfigFile)
  }

  def workflowSetup()

  def getEventDStream = {
    getInputDstream(streamingContext)(eventConfigUnit)
  }

  def generalParse(inputDStream: DStream[String]) = {
    val cTypeFilter: Array[String] = eventConfigUnit.getString("event.ctype_filter", "article")
      .split(",")
      .map {
        case "video" => "2"
        case _ => "1"
      }
    val regionFilter = eventConfigUnit.getString("event.region_filter", "").split(",").toList
    val eventWindow = eventConfigUnit.getInt("event.event_window", 600)
    val actFilter = eventConfigUnit.getString("event.act", "1,2,4").split(",").toList
    val mDwell = eventConfigUnit.getInt("event.mDwell", 600)
    /*inputDStream.flatMap(Event.getEventParserForWorldwide(regionFilter, actFilter, eventWindow, mDwell)).filter(record => {
      val eventFeature = EventFeature(record._1._3)
      cTypeFilter.contains(eventFeature.getFeatureValue("C_TYPE").getOrElse("1")) && record._1._2 != "0"
    })*/
  }

  /**
    * Deduplication by same key (uid,cid,rid,pid)
    *
    * @param inputDStream ((uid, cid, eventFeature, batchId), (act, value))
    * @return ((uid, cid, eventFeature, batchId), (label, value))
    */
  def eventLabel(inputDStream: DStream[((String, String, String, Long), (String, Double))]) = {
    val ration = eventConfigUnit.getDouble("event.ration",2.0)
    val dwell = eventConfigUnit.getDouble("event.dwell",70.0)
    /*inputDStream.map(record => {
      val eventFeature = EventFeature(record._1._3)
      //val eventTime = eventFeature.getFeatureValue("EVENT_TIME").getOrElse("")
      val pid = eventFeature.getFeatureValue("PID").getOrElse("11")
      val rid = eventFeature.getFeatureValue("RID").getOrElse("-1")
      ((record._1._1, record._1._2, rid, pid), (record._1._3, record._1._4, record._2._1, record._2._2))
    }).reduceByKey((r, l) => Utils.labelFun(l,r))
      .map(record => Utils.labelTag(record, dwell, ration))*/
  }

  /**
    *
    * @param inputDStream ((uid, contentId, eventFeature, batchId), (act, value))
    * @return (contentId, (uid, batchId, act, value, eventFeature))
    */
  def eventAggregation(inputDStream: DStream[((String, String, String, Long), (String, Double))]) = {
    inputDStream
      //      .reduceByKey((r, l) => (r._1 + l._1, r._2 + l._2))
      .map(record => (record._1._2, (record._1._1, record._1._4, record._2._1, record._2._2, record._1._3)))
      .repartition(eventConfigUnit.getInt("event.partitions", ModelComponent.defaultPatitionsForFetchingCP))
  }
}
