package com.cmcm.cmnews.model.input

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters

/**
  * Created by mengchong on 7/11/16.
  */
trait MergedXFBInput extends EventBatchInput{
  this: SparkBatchContext =>
  import Parameters._

  override def getInput(): Unit ={
    logInfo("start MergedXFBInput ...")
    setRDD(constant_merge_cfb_input,constant_merge_cfb_rdd, this)
  }

}