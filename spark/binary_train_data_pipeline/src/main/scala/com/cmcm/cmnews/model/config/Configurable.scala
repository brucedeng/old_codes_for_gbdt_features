package com.cmcm.cmnews.model.config

import org.apache.spark.Logging

import scala.collection.mutable
import scala.util.Try

/**
  * Created by lilonghua on 2016/11/9.
  */
trait Configurable extends Logging {

  val componentsConfigs = mutable.HashMap[String, ConfigUnit]()

  def loadConfigUnit(configName: String, configFilePath: String) = {
    val newConfigUnit = ConfigUnit(configName, configFilePath)
    componentsConfigs.put(newConfigUnit.getConfigName, newConfigUnit)
    newConfigUnit
  }

  def getConfigUnit(configName: String): Option[ConfigUnit] = {
    componentsConfigs.get(configName)
  }

  def registComponent(configName: String, configFilePath: String) = {
    val newConfigUnitTry = Try(ConfigUnit(configName, configFilePath))
    if (newConfigUnitTry.isFailure)
      logError(s"Can't load config file $configFilePath for $configName ")
    //TODO  should throw exception here ? and add format code  if{ } else { }

    newConfigUnitTry.foreach(configUnit =>
      componentsConfigs.put(configUnit.getConfigName, configUnit))
    componentsConfigs.get(configName)
  }

}
