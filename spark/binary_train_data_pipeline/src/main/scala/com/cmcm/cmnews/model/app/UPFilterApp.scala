package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.UPFilterCompute
import com.cmcm.cmnews.model.input.UPFilterDataInput
import com.cmcm.cmnews.model.output.XFBBinaryTrainDataOutput
import com.cmcm.cmnews.model.spark.NormalSparkContext

/**
  * Created by lilonghua on 16/9/14.
  */
object UPFilterApp extends BatchApp
  with NormalSparkContext
  with UPFilterDataInput
  with UPFilterCompute
  with XFBBinaryTrainDataOutput
