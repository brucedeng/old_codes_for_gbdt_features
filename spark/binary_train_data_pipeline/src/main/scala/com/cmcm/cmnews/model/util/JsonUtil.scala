package com.cmcm.cmnews.model.util

import org.json4s.{DefaultFormats, Extraction}
import org.json4s.JsonAST.{JDouble, _}
import org.json4s.jackson.JsonMethods._
import org.json4s.jackson.Serialization

import scala.util.Try

/**
 * Created by tangdong on 27/4/16.
 */
object JsonUtil {
  implicit val formats = DefaultFormats

  def toJson(objectToWrite: AnyRef): String = Serialization.write(objectToWrite)

  def fromJsonOption[T](jsonString: String)(implicit mf: Manifest[T]): Option[T] = Try(Serialization.read[T](jsonString)).toOption

  def jsonStrToMap(jsonStr: String): Map[String, Any] = parse(jsonStr).extract[Map[String, Any]]

  def toMap(obj: AnyRef): Map[String, Any] = jsonStrToMap(toJson(obj))

  def encodeJson(obj: AnyRef): JValue = Extraction.decompose(obj)

  def decodeJson[T](obj: JValue)(implicit mf: Manifest[T]): Option[T] = Try(Extraction.extract[T](obj)).toOption

  def convertToJValue(jsonStr: String) = parse(jsonStr)

  def convertToJString(jValue: JValue) = compact(jValue)

  def extractValue[T](jsonStr: String, key: String)(implicit mf: Manifest[T]): Option[T] = extractValue[T](parse(jsonStr), key)

  def extractValue[T](json: JValue, key: String)(implicit mf: Manifest[T]): Option[T] = {
    val value = json \ key
    value match {
      case JNothing => None
      case JNull => None
      case _ => value.extractOpt[T]
    }
  }

  def getStr(json: JValue, fieldName: String, default: String = "", invalid: String = "unknown") = {
    json \ fieldName match {
      case JString(s) =>
        if (s.trim.isEmpty) {
          default
        } else {
          if (s.toLowerCase.equals(invalid)) {
            default
          } else {
            s
          }
        }
      case JInt(s) => s.toString
      case JDouble(s) => s.toString
      case _ => default
    }
  }

  def getObjStr(json : JValue, objectName: String, fieldName:String, default:  String = "", invalid: String = "unknown") = {
    json \ objectName \ fieldName match {
      case JString(s) =>
        if (s.trim.isEmpty) {
          default
        } else {
          if (s.toLowerCase.equals(invalid)) {
            default
          } else {
            s
          }
        }
      case JInt(s) => s.toString
      case JDouble(s) => s.toString
      case _ => default
    }
  }

  def catKwAssist(jsonItems: List[JValue],
                  top: Int,
                  weight: String = "weight",
                  default: Double = 0.0,
                  name: String = "name"
                 ): List[(String, Double)] = {
    jsonItems.map(x => parseItem(x, weight, default, name)).sortWith(_._2 > _._2).take(top)
  }

  def parseItem(item: JValue, weight: String, default: Double, fieldName:String) = {
    val name = (item \ fieldName).asInstanceOf[JString].s
    val L1_weight = item \ weight match {
      case JDouble(x) =>
        x.toDouble
      case JInt(x) =>
        x.toDouble
      case JString(x) =>
        Try(x.toDouble).getOrElse(default)
      case _ =>
        default
    }
    (name, L1_weight)
  }

  def catKwNameAssist(jsonItems: List[JValue],
                  top: Int,
                  name: String = "name"
                 ): List[String] = {
    jsonItems.map(x => parseNameItem(x, name)).take(top)
  }

  def parseNameItem(item: JValue, fieldName:String) = {
    (item \ fieldName).asInstanceOf[JString].s
  }

  def main(args:Array[String]): Unit ={
    val featureDict = collection.mutable.Map[String,Any]()
    featureDict.put("D_CONID",List("0.0","1.0","0.01"))
    val categoryDict = collection.mutable.Map[String,Any]()
    categoryDict.put("cos_nomerge_14705823|military/chinese", List("0.1","1.2","0.03"))
    categoryDict.put("cos_nomerge_14705823|politics/government", List("0.5","1.7","0.3"))
    featureDict.put("C_D_U_GROUPID_CATEGORY", categoryDict)
    println(JsonUtil.toJson(featureDict))

  }
}
