package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.UserFilterCompute
import com.cmcm.cmnews.model.input.UPFilterDataInput
import com.cmcm.cmnews.model.output.BinaryTrainDataOutput
import com.cmcm.cmnews.model.spark.BinaryTrainDataSparkContext

/**
  * Created by lilonghua on 2016/11/7.
  */
object UserFilterApp extends BatchNewApp
  with BinaryTrainDataSparkContext
  with UPFilterDataInput
  with UserFilterCompute
  with BinaryTrainDataOutput
