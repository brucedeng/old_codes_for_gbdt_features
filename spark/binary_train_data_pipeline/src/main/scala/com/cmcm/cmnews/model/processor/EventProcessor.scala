package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.feature.{EventBase, EventFeature,GmpInfo}
import com.cmcm.cmnews.model.util.{JsonUtil, LoggingUtils, Util}
import com.cmcm.cmnews.model.util.Parameters._
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

import scala.collection.mutable
import scala.util.{Failure, Success, Try}
import org.json4s.JsonAST.{JDouble, _}

/**
  * Created by lilonghua on 2016/10/28.
  */
object EventProcessor extends ProcessorAbstract[Option[EventFeature]] {

  override def preprocess(line: String, batchContext: mutable.Map[String, String]): Option[EventFeature] = {
    //val eventPid = Try(batchContext(constant_event_pid)).getOrElse("0")
    val cType = Try(batchContext(constant_event_ctype)).getOrElse("1,0x01")
    val eventLanRegion = Try(batchContext(constant_event_lan_region)).getOrElse("")
    val position = Try(batchContext(constant_event_position)).getOrElse("")

    //val WND = Try(batchContext(constant_wnd)).getOrElse("600").toInt
    val thresholdRead = Try(batchContext(constant_event_thresholdRead)).getOrElse("600").toInt
    try {
      Try(JsonUtil.convertToJValue(line)) match {
        case Success(jvalue) =>
          val driver = Try(JsonUtil.getObjStr(jvalue, "ext", "drv", "1").toInt).getOrElse(1)
          val uid = JsonUtil.getStr(jvalue, "aid", "")
          val tApplan = JsonUtil.getStr(jvalue, "app_lan", "")
          val lan = if (tApplan.contains("hi")) "hi" else tApplan
          val region = JsonUtil.getStr(jvalue, "country", "")
          val country = if ("PY,PA,HN,AR,SV,VE,CU,DO,PE,CO,CL,CR,EC,NI,BO,GT,UY".contains(region)) "LM" else region
          val cid = JsonUtil.getStr(jvalue, "contentid", "")
          //val ip = JsonUtil.getStr(jvalue, "ip", "unknown")
          val eventTime = Try(JsonUtil.getStr(jvalue, "eventtime", "0").toLong).getOrElse(0l)
          val ts = Try(JsonUtil.getStr(jvalue, "servertime_sec", "0").toLong).getOrElse(0l)
          val pid = JsonUtil.getStr(jvalue, "pid", "")
          val ctype = JsonUtil.getStr(jvalue, "ctype", "1")
          val act = Try(JsonUtil.getStr(jvalue, "act", "-1").toInt).getOrElse(-1)
          val city = JsonUtil.getStr(jvalue, "city", "")
          val des = JsonUtil.getObjStr(jvalue, "cpack", "des", "")
          val dwelltime = JsonUtil.getObjStr(jvalue, "ext", "dwelltime", "0").toInt
          val level1Type = JsonUtil.getObjStr(jvalue, "scenario", "level1_type", "")
          val level1 = JsonUtil.getObjStr(jvalue, "scenario", "level1", "")
          val net = JsonUtil.getStr(jvalue, "net","")
          val osVersion = JsonUtil.getStr(jvalue, "osv", "")
          val appVersion = JsonUtil.getStr(jvalue, "appv", "")
          val appIv = Try(JsonUtil.getStr(jvalue, "apiv", "0").toInt).getOrElse(0)
          val platForm = JsonUtil.getStr(jvalue, "pf", "")
          val brand = JsonUtil.getStr(jvalue, "brand", "")
          val model = JsonUtil.getStr(jvalue, "model", "")
          val channel = JsonUtil.getStr(jvalue, "ch", "")
          val isHot = Try(JsonUtil.getObjStr(jvalue, "cpack", "ishot", "0").toInt).getOrElse(0)
          //val lanRegion = lan + "_" + country
          val gmpinfo = parseGmp(jvalue,"cpack","ext")

          val rid = Try(parseRid(des, "rid")).getOrElse("")
          val src = Try(parseSrc(des, "src")).getOrElse("")
          val feature = EventFeature(
            EventBase(
              ctype,
              act,
              city,
              country,
              lan,
              pid,
              level1Type,
              level1,
              net,
              osVersion,
              appVersion,
              platForm,
              brand,
              model,
              channel,
              isHot,
              appIv),
              Util.getDwell(Try(act.toInt).getOrElse(-1), dwelltime, thresholdRead),
              ts,
              eventTime,
              uid,
              cid,
              rid,
              src,
              gmpinfo)
          if (feature.validLog(cType, eventLanRegion, position) && driver > 0) {
            Option(feature)
          } else {
            None
          }
        case Failure(ex) =>
          LoggingUtils.loggingError(LoggingUtils.getException(ex))
          None
      }
    } catch {
      case e: Exception =>
        LoggingUtils.loggingError(e.toString)
        None
    }
  }

  override def process(inputRDD: RDD[String], batchContext: mutable.Map[String, String]): RDD[Option[EventFeature]] = {
    inputRDD.map(preprocess(_,batchContext)).filter(_.nonEmpty).map(f => {
      (f.get.getUnqId,f)
    }).reduceByKey((l,r) => {
       val validgmp = (l.get.gmp.gmpScore > r.get.gmp.gmpScore) match
       {
          case true  => l.get.gmp
          case false => r.get.gmp
       }

      val feature = EventFeature(
        EventBase(
          l.get.eventBase.ctype,
          math.max(l.get.eventBase.act, r.get.eventBase.act),
          Util.getMax(l.get.eventBase.city, r.get.eventBase.city),
          Util.getMax(l.get.eventBase.country, r.get.eventBase.country),
          Util.getMax(l.get.eventBase.lan, r.get.eventBase.lan),
          l.get.eventBase.pid,
          l.get.eventBase.level1Type,
          l.get.eventBase.level1, l.get.eventBase.net,
          l.get.eventBase.osVersion,
          l.get.eventBase.appVersion,
          l.get.eventBase.platform,
          l.get.eventBase.brand,
          l.get.eventBase.model,
          l.get.eventBase.channel,
          l.get.eventBase.isHot, l.get.eventBase.appIV),
        math.max(l.get.readTime, r.get.readTime),
        math.min(l.get.serverTime, r.get.serverTime),
        math.min(l.get.eventTime, r.get.eventTime),
        l.get.aid,
        l.get.cid,
        "",
        l.get.src,
        validgmp
      )
      Option(feature)
    }).map(_._2)
  }

  def processCM(inputRDD: RDD[String], batchContext: mutable.Map[String, String]): RDD[Option[EventFeature]] = {
    inputRDD.map(preprocess(_,batchContext)).filter(_.nonEmpty).map(f => {
      (f.get.getUnqIdCm,f)
    }).reduceByKey((l,r) => {
       val validgmp = (l.get.gmp.gmpScore > r.get.gmp.gmpScore) match
       {
          case true  => l.get.gmp
          case false => r.get.gmp
       }
      val feature = EventFeature(
        EventBase(
          l.get.eventBase.ctype,
          math.max(l.get.eventBase.act, r.get.eventBase.act),
          Util.getMax(l.get.eventBase.city, r.get.eventBase.city),
          Util.getMax(l.get.eventBase.country, r.get.eventBase.country),
          Util.getMax(l.get.eventBase.lan, r.get.eventBase.lan),
          l.get.eventBase.pid,
          l.get.eventBase.level1Type,
          l.get.eventBase.level1, l.get.eventBase.net,
          l.get.eventBase.osVersion,
          l.get.eventBase.appVersion,
          l.get.eventBase.platform,
          l.get.eventBase.brand,
          l.get.eventBase.model,
          l.get.eventBase.channel,
          l.get.eventBase.isHot, l.get.eventBase.appIV),
        math.max(l.get.readTime, r.get.readTime),
        math.min(l.get.serverTime, r.get.serverTime),
        math.min(l.get.eventTime, r.get.eventTime),
        l.get.aid,
        l.get.cid,
        "",
        l.get.src,
        validgmp
      )
      Option(feature)
    }).map(_._2)
  }

  def parseSrc(des: String, tag: String = "src"): String = {
    if (des.isEmpty) {
      des
    } else {
      des.split("\\|").map(f => {
        val d = f.split("=")
        (d(0), Try(d(1)).getOrElse(""))
      }).filter(f => tag.equals(f._1)).take(1).head._2
    }
  }

  def parseRid(des: String, tag: String = "rid"): String = {
    if (des.isEmpty) {
      des
    } else {
      des.split("\\|").map(f => {
        val d = f.split("=")
        (d(0), Try(d(1)).getOrElse(""))
      }).filter(f => tag.equals(f._1)).take(1).head._2
    }
  }
  
  def processNewFea(l: EventFeature, r: EventFeature) = l.copy(eventBase = l.eventBase.copy(
    city = Util.getMax(l.eventBase.city, r.eventBase.city),
    country = Util.getMax(l.eventBase.country, r.eventBase.country),
    act = math.max(l.eventBase.act, r.eventBase.act),
    lan = Util.getMax(l.eventBase.lan, r.eventBase.lan)),
    serverTime = math.min(l.serverTime, r.serverTime),
    readTime = math.max(l.readTime, r.readTime)
  )


  def processDudupMerge(inputRDD: RDD[String], batchContext: mutable.Map[String, String], sparkContext:SparkContext) = {
    val eventMerge = inputRDD.flatMap(preprocess(_, batchContext)).map(f => {
      ((f.aid, f.cid, f.eventBase.pid, f.riq), f)
    }).reduceByKey(processNewFea(_, _)).map(x => if (x._2.readTime <= 0 && x._2.eventBase.act != 1) x._2.copy(readTime = 70) else x._2)

    eventMerge
  }

  def parseGmp(jvalue:JValue, pack:String, field:String):GmpInfo = {  
   jvalue \ pack \ field match {
          case elem:JValue =>
               val score = Try(JsonUtil.getStr(elem, "gmp_score", "-1.0").toDouble).getOrElse(-1.0)
               val ver = JsonUtil.getStr(elem, "gmp_ver", "-1")
               if(ver.toInt < 0 && score > -0.5)
                  GmpInfo(score,"0")
                else
                  GmpInfo(score,ver)    
          case _ =>
                  GmpInfo(-1.0,"-1")
   }
  }
}
