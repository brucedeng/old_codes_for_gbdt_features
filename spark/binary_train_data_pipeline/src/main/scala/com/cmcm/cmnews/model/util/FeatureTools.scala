package com.cmcm.cmnews.model.util

/**
  * Created by lilonghua on 2016/10/19.
  */

class FeatureTools {
  var features: Map[String, String] = Map()

  def addFeature(key: String, value: String) = {
    features = features.+((key, value))
    this
  }

  def genFeatureString() = {
    features.map(pair => {
      pair._1 + Parameters.keyValueDelimiter + pair._2
    }).mkString(Parameters.pairDelimiter)
  }

  def getFeatureValue(key: String) = {
    features.get(key)
  }

  def updateFeatureValue(key: String, value: String) = {
    features.updated(key,value)
  }
}


object FeatureTools {

  def apply(featureString: String) = {
    val eventFeature = new FeatureTools()
    featureString.split(Parameters.pairDelimiter).foreach(kvString => {
      val pair = kvString.split(Parameters.keyValueDelimiter)
      if (pair.size == 2)
        eventFeature.addFeature(pair(0), pair(1))
    })
    eventFeature
  }

  def apply() = {
    new FeatureTools()
  }

  def main(args: Array[String]) {
    val feature = FeatureTools()
    feature.addFeature("U_ISHOT", "isHot")
    feature.addFeature("U_APPIV", "appIv")
    feature.addFeature("LABEL", "label")

    println(feature.genFeatureString())

    println(FeatureTools(feature.genFeatureString()).addFeature("LABEL", "label_new").genFeatureString())
  }

}