package com.cmcm.cmnews.model.component

import com.cmcm.cmnews.model.config.ConfigUnit
import com.cmcm.cmnews.model.spark.SparkBatchContext
import org.apache.spark.Logging

import scala.collection.mutable
import scala.util.Try

/**
 * Created by tangdong on 27/4/16.
 */
trait BatchComponent extends Logging with Serializable {

  val componentsConfigs = mutable.HashMap[String, ConfigUnit]()

  def init(configDir: String = ""): Unit = {
    logInfo("BatchComponent finished init")
  }

  protected def getRDD[T](name: String, sbc: SparkBatchContext): T = sbc.getRddContext.get(name) match{
    case Some(rdd) => rdd.asInstanceOf[T]
    case _ =>
      logError(s"lack of $name in Components")
      throw new RuntimeException(s"lack of $name in Components")
  }

  protected def setRDD(path:String, name:String, sbc: SparkBatchContext) = {
    sbc.getBatchContext.get(path) match {
      case Some(p) =>
        val rdd = sbc.getSparkContext.textFile(p)
        sbc.setRddContext(name,rdd)
        val rddContextTmp = sbc.getRddContext
        val keys = rddContextTmp.keys.mkString(",")
        logInfo(s"sbc rddcontext info $keys")
        logInfo(s"Finish set $path to $name RDD with path $p")
      case _ =>
        logError(s"lack of $path!")
        throw new RuntimeException(s"lack of $path!")
    }
  }
  protected def getParam[T](name:String, sbc:SparkBatchContext): T = sbc.getBatchContext.get(name) match{
    case Some(value) => value.asInstanceOf[T]
    case _ =>
      logInfo(s"lack of $name in Components")
      throw new RuntimeException(s"lack of $name in Components")
  }

  def loadConfigUnit(configName: String, configFilePath: String) = {
    val newConfigUnit = ConfigUnit(configName, configFilePath)
    componentsConfigs.put(newConfigUnit.getConfigName, newConfigUnit)
    newConfigUnit
  }

  def getConfigUnit(configName: String): Option[ConfigUnit] = {
    componentsConfigs.get(configName)
  }

  def registComponent(configName: String, configFilePath: String) = {
    val newConfigUnitTry = Try(ConfigUnit(configName, configFilePath))
    if (newConfigUnitTry.isFailure) {
      logError(s"Can't load config file $configFilePath for $configName ")
    } else {
      logInfo(s"Load config file $configFilePath for $configName $newConfigUnitTry")
      newConfigUnitTry.foreach(f => logInfo(f.getConfig.entrySet().toString))
    }
    newConfigUnitTry.foreach(configUnit =>
      componentsConfigs.put(configUnit.getConfigName, configUnit))
    componentsConfigs.get(configName)
  }

  def loadComponentConfig(name: String, eventConfigFile: String): ConfigUnit = {
    logInfo(s"Loading $name component config from $eventConfigFile")
    val componentConfigUnit = registComponent(name, eventConfigFile)
    if (componentConfigUnit.isEmpty) {
      logError(s"Can not init $name component from $eventConfigFile")
      //throw new IllegalStateException(s"Can not init $name component from $eventConfigFile")
    }
    componentConfigUnit.orNull
  }
}
