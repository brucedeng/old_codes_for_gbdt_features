package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.util.{FeatureTools, JsonUtil, LoggingUtils, Parameters}
import org.apache.spark.rdd.RDD
import org.json4s.JsonAST._

import scala.util.control.Breaks._
import scala.util.{Failure, Success, Try}

/**
 * Created by tangdong on 27/4/16.
 */
object CPProcessor extends Processor {
  import Parameters._

  override  def process(inputRDD:RDD[String], batchContext: collection.mutable.Map[String,String]): RDD[(String, String)] = {
    val parallelMax = Try(batchContext(constant_parallelismMax)).getOrElse("500").toInt
    inputRDD.map(preprocess(_, batchContext))
      .filter(_._1.nonEmpty)
      .reduceByKey((left,right) => {
        val lFeat = FeatureTools(left)
        val rFeat = FeatureTools(right)
        val leftUpdateTime = lFeat.getFeatureValue("C_PUBLISHTIME").getOrElse("0").toLong
        val rightUpdateTime = rFeat.getFeatureValue("C_PUBLISHTIME").getOrElse("0").toLong
        if (leftUpdateTime < rightUpdateTime)
          right
        else
          left
      },parallelMax * 2)
  }

  override def preprocess(line:String, batchContext: collection.mutable.Map[String,String]) = {
    val categoryVersion = Try(batchContext(constant_cat_version)).getOrElse("v4")
    val keywordVersion =  Try(batchContext(constant_kw_version)).getOrElse("v2")
    val topicVersion =  Try(batchContext(constant_topic_version)).getOrElse("v2")
    val topRelCat =  Try(batchContext(constant_d_rel_ncat)).getOrElse("100").toInt
    val topRelKw = Try(batchContext(constant_d_rel_nkey)).getOrElse("100").toInt
    val topRelTopic = Try(batchContext(constant_d_rel_ntopic)).getOrElse("100").toInt
    val l1_norm = Try(batchContext(constant_l1_norm)).getOrElse("true").toLowerCase
    val l1Flag = if(l1_norm=="yes"||l1_norm=="true") true else false

    Try(JsonUtil.convertToJValue(line)) match{
      case Success(jvalue) =>
        val cid = if ( keywordVersion == "oversea" ) extractStr(jvalue,"item_id")
          else extractStr(jvalue,"content_id")
        val groupId = extractStr(jvalue,"group_id") match {
          case x:String if x != "" => x
          case _ => ""
        }
        val tier = extractStr(jvalue,"tier") match {
          case x:String if x != "" => x
          case _ => ""
        }
        val publisher = extractStr(jvalue,"publisher") match {
          case x:String if x != "" => x
          case _ => ""
        }
        val publishTime = extractStr(jvalue,"publish_time") match {
          case x:String if x != ""  => x
          case _ => "0"
        }
        val updateTime = extractStr(jvalue,"update_time") match {
          case x:String if x != "" => x
          case _ => "0"
        }
        val catStr = if ( categoryVersion == "oversea" ){
          jvalue \ "categories" match {
            case JArray(arr) =>
              overseaCatKwAssist(arr, topRelCat ,l1Flag)
            case _ => ""
          }
        } else if( categoryVersion == "oversea_l2") {
          jvalue \ "l2_categories" match {
            case JArray(arr) =>
              overseaCatKwAssist(arr, topRelCat ,l1Flag)
            case _ => ""
          }
        } else {
          jvalue \ "categories" match {
            case JArray(arr) =>
              catKwAssist(arr, categoryVersion, topRelCat, "")
            case _ => ""
          }
        }

        val kwStr = if ( keywordVersion == "oversea" ){
          jvalue \ "entities" match {
            case JArray(arr) =>
              overseaCatKwAssist(arr, topRelKw, l1Flag)
            case _ => ""
          }
        } else {
          jvalue \ "keywords" match {
            case JArray(arr) =>
              catKwAssist(arr, keywordVersion, topRelKw, "")
            case _ => ""
          }
        }

        val topicStr = if ( topicVersion == "oversea" ){
          jvalue \ "topics" match {
            case JArray(arr) =>
              overseaCatKwAssist(arr, topRelTopic, l1Flag)
            case _ => ""
          }
        } else {
          ""
        }

        val wordCount = extractStr(jvalue, "word_count") match {
          case x:String if x != "" => x
          case _ => "0"
        }
        val imageCount = extractStr(jvalue, "image_count") match {
          case x:String if x != "" => x
          case _ => "0"
        }
        val newsyScore = extractStr(jvalue, "newsy_score") match {
          case x:String if x != "" => x
          case _ => "0.0"
        }
        val titlemd5 = extractStr(jvalue,"title_md5") match {
          case x:String if x != "" => x
          case _ => "unknown"
        }
        val mediaLevel = extractStr(jvalue,"media_level") match {
          case x:String if x != "" => x
          case _ => "0"
        }
        val commentCount = extractStr(jvalue,"comment_count") match {
          case x:String if x != "" => x
          case _ => "0"
        }
        val hasBigImg = extractStr(jvalue,"has_big_img") match {
          case x:String if x != "" => x
          case _ => "0"
        }

        if (Try(wordCount.toLong).getOrElse(0l) < 10000l) {
          val feature = FeatureTools()
          feature.addFeature("D_KEYWORD", kwStr)
          feature.addFeature("D_CATEGORY", catStr)
          feature.addFeature("D_TOPIC", topicStr)

          feature.addFeature("D_PUBLISHER", publisher)
          feature.addFeature("C_PUBLISHTIME", publishTime.toString)
          feature.addFeature("D_GROUPID", groupId)
          feature.addFeature("D_TITLEMD5", titlemd5)
          feature.addFeature("D_MEDIALEVEL", mediaLevel)
          feature.addFeature("D_COMMENTCOUNT", commentCount)
          feature.addFeature("D_HASBIGIMG", hasBigImg)
          feature.addFeature("D_TIER", tier)
          feature.addFeature("D_WORDCOUNT", wordCount)
          feature.addFeature("D_IMAGECOUNT", imageCount)
          feature.addFeature("D_NEWSYSCORE", newsyScore)
          (cid, feature.genFeatureString())
        } else {
          ("","")
        }
      case Failure(ex) =>
        LoggingUtils.loggingError(LoggingUtils.getException(ex))
        ("","")
    }
  }

  def extractStr(jvalue: JValue, fieldName: String, default: String = "") = {
    jvalue \ fieldName match {
      case JString(s) => if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString()
      case _ => default
    }
  }

  def overseaCatKwAssist(jsonItems: List[JValue], top:Int, l1Norm:Boolean): String = {
    def parse_item(item: JValue) = {
      val name = (item \ "name").asInstanceOf[JString].s
      val L1_weight = item \ "weight" match {
        case JDouble(x) =>
          x.toDouble
        case JInt(x) =>
          x.toDouble
        case _ =>
          0.0
      }
      (name, L1_weight)
    }
    val data = jsonItems.map(x => parse_item(x)).filter(x => x._1.nonEmpty)
    if (l1Norm) {
      val sumWt = data.map(x => x._2).sum
      val sumWeight = if (sumWt > 0) sumWt else 1.0
      data.map(x => x._1 + keyGap + x._2 / sumWeight).take(top).mkString(pairGap)
    }
    else
      data.map(x => x._1 + keyGap + x._2).take(top).mkString(pairGap)
  }

  def catKwAssist(jsonItems: List[JValue], version:String, top:Int, default:String): String = {
    if (jsonItems.isEmpty) default
    else {
      jsonItems.head \ "version" match {
        case JString(v) if v == version => {
          val sb = new StringBuilder
          val items = (jsonItems.head \ "list").asInstanceOf[JArray].arr
          var  index = 0
          breakable {
            for (item <- items) {
              if (index >= top)
                break()
              index += 1
              val name = (item \ "name").asInstanceOf[JString].s
              val L1_weight = (item \ "L1_weight") match {
                case JDouble(x) =>
                  x.toDouble
                case JInt(x) =>
                  x.toDouble
                case _ =>
                  0.0
              }
              sb.append(name + keyGap + L1_weight + pairGap)

            }
          }
          sb.toString()
        }
        case _ => catKwAssist(jsonItems.tail, version, top, default)
      }
    }
  }
}