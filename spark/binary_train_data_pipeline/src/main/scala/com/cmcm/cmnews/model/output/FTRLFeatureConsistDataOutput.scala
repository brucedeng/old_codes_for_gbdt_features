package com.cmcm.cmnews.model.output

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters
import org.apache.spark.rdd.RDD

/**
  * Created by lilonghua on 2016/10/21.
  */
trait FTRLFeatureConsistDataOutput extends EventBatchOutput{
  this: SparkBatchContext =>
  import Parameters._

  override def outPut: Unit ={
    super.outPut
    logInfo("Start FTRLFeatureConsistDataOutput...")
    val xfbTrainDataOut = rddContext(constant_lr_feature_consist_rdd).asInstanceOf[RDD[String]]
    xfbTrainDataOut.saveAsTextFile(batchContext(constant_lr_feature_consist_path))
  }
}
