package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.component.BatchComponent
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters

/**
 * Created by tangdong on 27/4/16.
 */

object EventBatchCompute {
  val componentName = "feature_component"
  val configFileName = "feature.conf"
}

trait EventBatchCompute extends BatchComponent {
  this: SparkBatchContext =>
  def compute = {
    logInfo("Start EventBatchCompute...")
  }

  override def init(configDir: String) = {
    super.init(configDir)
    initFeatureComponent(configDir + batchContext.getOrElse(Parameters.constant_conf_feature, EventBatchCompute.configFileName))
  }

  def featureConfigUnit = {
    getConfigUnit(EventBatchCompute.componentName).get
  }

  def initFeatureComponent(monitorConfigFile: String) = {
    logInfo(s"Initializing feature component with $monitorConfigFile ....")
    loadComponentConfig(EventBatchCompute.componentName, monitorConfigFile)
  }
}
