package com.cmcm.cmnews.model.util

import org.I0Itec.zkclient.ZkClient
import org.I0Itec.zkclient.exception.{ZkNodeExistsException, ZkNoNodeException, ZkMarshallingError, ZkBadVersionException}
import org.I0Itec.zkclient.serialize.ZkSerializer
import org.apache.spark.Logging
import collection._
import org.apache.zookeeper.data.Stat
import kafka.common.TopicAndPartition

/**
 * Created by wangwei5 on 15/11/25.
 */
object ZKUtils extends Logging {
    val root = ""
    /**
     *  make sure a persistent path exists in ZK. Create the path if not exist.
     */
    def retry[T](n: Int)(fn: => T): T = {
      util.Try { fn } match {
        case util.Success(x) => x
        case _ if n > 1 => retry(n - 1)(fn)
        case util.Failure(e) => throw e
      }
    }

    def getTopicsPartitionAndOffsets(client: ZkClient, offsetPath: String, topics: Set[String]) = {
      import scala.collection.mutable.Map
      var partitionOffsets = Map[TopicAndPartition, Long]()
      val path = s"$root$offsetPath"
      val dataStat = readDataMaybeNull(client, path)
      if(dataStat._1.isDefined) {
        val data = dataStat._1.get
        println(s"Get data($data) from zk...")
        val topicPartitionOffset = dataStat._1.get.split("\001")
        for(tpo <- topicPartitionOffset) {
          val items = tpo.split("\002")
          if(items.length == 3 && topics.contains(items(0))) {
            val topic = items(0)
            val partition = items(1).toInt
            val offset = items(2).toLong
            println(s"topic:$topic, partition:$partition, offset:$offset")
            partitionOffsets += (new TopicAndPartition(topic, partition) -> offset)
          } else {
            println(s"Can't add $tpo in TopicsPartitionAndOffsets")
          }
        }
      } else {
        println(s"Can't get data from $path")
      }
      partitionOffsets
    }

    def updateAppOffsets(client: ZkClient, appName: String, data: String) = {
      val path = s"$root$appName"
      logInfo(s"Update $data in $path")
      updatePersistentPath(client, path, data)
    }

    def makeSurePersistentPathExists(client: ZkClient, path: String) = {
      if (!client.exists(path)) {
        client.createPersistent(path, true) // won't throw NoNodeException or NodeExistsException
        false
      } else true
    }

    /**
     *  create the parent path
     */
    private def createParentPath(client: ZkClient, path: String): Unit = {
      val parentDir = path.substring(0, path.lastIndexOf('/'))
      if (parentDir.length != 0)
        client.createPersistent(parentDir, true)
    }

    /**
     * Create an ephemeral node with the given path and data. Create parents if necessary.
     */
    private def createEphemeralPath(client: ZkClient, path: String, data: String): Unit = {
      try {
        client.createEphemeral(path, data)
      } catch {
        case e: ZkNoNodeException => {
          createParentPath(client, path)
          client.createEphemeral(path, data)
        }
      }
    }

    /**
     * Create an ephemeral node with the given path and data.
     * Throw NodeExistException if node already exists.
     */
    def createEphemeralPathExpectConflict(client: ZkClient, path: String, data: String): Unit = {
      try {
        createEphemeralPath(client, path, data)
      } catch {
        case e: ZkNodeExistsException => {
          // this can happen when there is connection loss; make sure the data is what we intend to write
          var storedData: String = null
          try {
            storedData = readData(client, path)._1
          } catch {
            case e1: ZkNoNodeException => // the node disappeared; treat as if node existed and let caller handles this
            case e2: Throwable => throw e2
          }
          if (storedData == null || storedData != data) {
            logInfo("conflict in " + path + " data: " + data + " stored data: " + storedData)
            throw e
          } else {
            // otherwise, the creation succeeded, return normally
            logInfo(path + " exists with value " + data + " during connection loss; this is ok")
          }
        }
        case e2: Throwable => throw e2
      }
    }

    /**
     * Create an ephemeral node with the given path and data.
     * Throw NodeExistsException if node already exists.
     * Handles the following ZK session timeout bug:
     *
     * https://issues.apache.org/jira/browse/ZOOKEEPER-1740
     *
     * Upon receiving a NodeExistsException, read the data from the conflicted path and
     * trigger the checker function comparing the read data and the expected data,
     * If the checker function returns true then the above bug might be encountered, back off and retry;
     * otherwise re-throw the exception
     */
    def createEphemeralPathExpectConflictHandleZKBug(zkClient: ZkClient, path: String, data: String, expectedCallerData: Any, checker: (String, Any) => Boolean, backoffTime: Int): Unit = {
      while (true) {
        try {
          createEphemeralPathExpectConflict(zkClient, path, data)
          return
        } catch {
          case e: ZkNodeExistsException => {
            // An ephemeral node may still exist even after its corresponding session has expired
            // due to a Zookeeper bug, in this case we need to retry writing until the previous node is deleted
            // and hence the write succeeds without ZkNodeExistsException
            readDataMaybeNull(zkClient, path)._1 match {
              case Some(writtenData) => {
                if (checker(writtenData, expectedCallerData)) {
                  logInfo("I wrote this conflicted ephemeral node [%s] at %s a while back in a different session, ".format(data, path)
                    + "hence I will backoff for this node to be deleted by Zookeeper and retry")

                  Thread.sleep(backoffTime)
                } else {
                  throw e
                }
              }
              case None => // the node disappeared; retry creating the ephemeral node immediately
            }
          }
          case e2: Throwable => throw e2
        }
      }
    }

    /**
     * Create an persistent node with the given path and data. Create parents if necessary.
     */
    def createPersistentPath(client: ZkClient, path: String, data: String = ""): Unit = {
      try {
        client.createPersistent(path, data)
      } catch {
        case e: ZkNoNodeException => {
          createParentPath(client, path)
          client.createPersistent(path, data)
        }
      }
    }

    def createSequentialPersistentPath(client: ZkClient, path: String, data: String = ""): String = {
      client.createPersistentSequential(path, data)
    }

    /**
     * Update the value of a persistent node with the given path and data.
     * create parrent directory if necessary. Never throw NodeExistException.
     * Return the updated path zkVersion
     */
    def updatePersistentPath(client: ZkClient, path: String, data: String) = {
      try {
        client.writeData(path, data)
      } catch {
        case e: ZkNoNodeException => {
          createParentPath(client, path)
          try {
            client.createPersistent(path, data)
          } catch {
            case e: ZkNodeExistsException =>
              client.writeData(path, data)
            case e2: Throwable => throw e2
          }
        }
        case e2: Throwable => throw e2
      }
    }

    /**
     * Conditional update the persistent path data, return (true, newVersion) if it succeeds, otherwise (the path doesn't
     * exist, the current version is not the expected version, etc.) return (false, -1)
     *
     * When there is a ConnectionLossException during the conditional update, zkClient will retry the update and may fail
     * since the previous update may have succeeded (but the stored zkVersion no longer matches the expected one).
     * In this case, we will run the optionalChecker to further check if the previous write did indeed succeeded.
     */
    def conditionalUpdatePersistentPath(client: ZkClient, path: String, data: String, expectVersion: Int,
                                        optionalChecker:Option[(ZkClient, String, String) => (Boolean,Int)] = None): (Boolean, Int) = {
      try {
        val stat = client.writeDataReturnStat(path, data, expectVersion)
        logDebug("Conditional update of path %s with value %s and expected version %d succeeded, returning the new version: %d"
          .format(path, data, expectVersion, stat.getVersion))
        (true, stat.getVersion)
      } catch {
        case e1: ZkBadVersionException =>
          optionalChecker match {
            case Some(checker) => return checker(client, path, data)
            case _ => logDebug("Checker method is not passed skipping zkData match")
          }
          logWarning("Conditional update of path %s with data %s and expected version %d failed due to %s".format(path, data,
            expectVersion, e1.getMessage))
          (false, -1)
        case e2: Exception =>
          logWarning("Conditional update of path %s with data %s and expected version %d failed due to %s".format(path, data,
            expectVersion, e2.getMessage))
          (false, -1)
      }
    }

    /**
     * Conditional update the persistent path data, return (true, newVersion) if it succeeds, otherwise (the current
     * version is not the expected version, etc.) return (false, -1). If path doesn't exist, throws ZkNoNodeException
     */
    def conditionalUpdatePersistentPathIfExists(client: ZkClient, path: String, data: String, expectVersion: Int): (Boolean, Int) = {
      try {
        val stat = client.writeDataReturnStat(path, data, expectVersion)
        logDebug("Conditional update of path %s with value %s and expected version %d succeeded, returning the new version: %d"
          .format(path, data, expectVersion, stat.getVersion))
        (true, stat.getVersion)
      } catch {
        case nne: ZkNoNodeException => throw nne
        case e: Exception =>
          logError("Conditional update of path %s with data %s and expected version %d failed due to %s".format(path, data,
            expectVersion, e.getMessage))
          (false, -1)
      }
    }

    /**
     * Update the value of a persistent node with the given path and data.
     * create parrent directory if necessary. Never throw NodeExistException.
     */
    def updateEphemeralPath(client: ZkClient, path: String, data: String): Unit = {
      try {
        client.writeData(path, data)
      } catch {
        case e: ZkNoNodeException => {
          createParentPath(client, path)
          client.createEphemeral(path, data)
        }
        case e2: Throwable => throw e2
      }
    }

    def deletePath(client: ZkClient, path: String): Boolean = {
      try {
        client.delete(path)
      } catch {
        case e: ZkNoNodeException =>
          // this can happen during a connection loss event, return normally
          logInfo(path + " deleted during connection loss; this is ok")
          false
        case e2: Throwable => throw e2
      }
    }

    def deletePathRecursive(client: ZkClient, path: String) {
      try {
        client.deleteRecursive(path)
      } catch {
        case e: ZkNoNodeException =>
          // this can happen during a connection loss event, return normally
          logInfo(path + " deleted during connection loss; this is ok")
        case e2: Throwable => throw e2
      }
    }

    def maybeDeletePath(zkUrl: String, dir: String) {
      val zk = new ZkClient(zkUrl, 30*1000, 30*1000, ZKStringSerializer)
      maybeDeletePath(zk, dir)
    }

    def maybeDeletePath(zk: ZkClient, dir: String): Unit = {
      try {

        zk.deleteRecursive(dir)
        zk.close()
      } catch {
        case _: Throwable => // swallow
      }
    }

    def readData(client: ZkClient, path: String): (String, Stat) = {
      val stat: Stat = new Stat()
      val dataStr: String = client.readData(path, stat)
      (dataStr, stat)
    }

    def readDataMaybeNull(client: ZkClient, path: String): (Option[String], Stat) = {
      val stat: Stat = new Stat()
      val dataAndStat = try {
        (Some(client.readData(path, stat)), stat)
      } catch {
        case e: ZkNoNodeException =>
          (None, stat)
        case e2: Throwable => throw e2
      }
      dataAndStat
    }

    def getChildren(client: ZkClient, path: String): Seq[String] = {
      import scala.collection.JavaConversions._
      // triggers implicit conversion from java list to scala Seq
      client.getChildren(path)
    }

    def getChildrenParentMayNotExist(client: ZkClient, path: String): Seq[String] = {
      import scala.collection.JavaConversions._
      // triggers implicit conversion from java list to scala Seq
      try {
        client.getChildren(path)
      } catch {
        case e: ZkNoNodeException => return Nil
        case e2: Throwable => throw e2
      }
    }

    /**
     * Check if the given path exists
     */
    def pathExists(client: ZkClient, path: String): Boolean = {
      client.exists(path)
    }
}

object ZKStringSerializer extends ZkSerializer {

  @throws(classOf[ZkMarshallingError])
  def serialize(data : Object) : Array[Byte] = data.asInstanceOf[String].getBytes("UTF-8")

  @throws(classOf[ZkMarshallingError])
  def deserialize(bytes : Array[Byte]) : Object = {
    if (bytes == null)
      null
    else
      new String(bytes, "UTF-8")
  }
}
