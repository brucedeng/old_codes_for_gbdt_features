package com.cmcm.cmnews.model.parse

/**
  * Created by lilonghua on 2016/11/24.
  */
case class FeatureHitRate (
                            feature: String,
                            offHit: Int,
                            offAll: Int,
                            offRate: Float,
                            onlineAll: Int,
                            onlineRate: Float) {

  override def toString: String = {
    s"$feature\toffHit:$offHit\toffAll:$offAll\toffRate:$offRate\tonlineAll:$onlineAll\tonlineRate:$onlineRate"
  }
}
