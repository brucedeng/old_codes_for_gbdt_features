package com.cmcm.cmnews.model.output

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.rdd.RDD

/**
  * Created by mengchong on 7/13/16.
  */
trait LibSvmDataOutput extends EventBatchOutput {
  this: SparkBatchContext =>
  import Parameters._

  override def outPut: Unit ={
    super.outPut
    logInfo("Start XfbTrainDataOutput...")
    val libsvmOutput = rddContext(constant_libsvm_rdd).asInstanceOf[RDD[String]]
    libsvmOutput.saveAsTextFile(batchContext(constant_libsvm_out),classOf[GzipCodec])
  }
}
