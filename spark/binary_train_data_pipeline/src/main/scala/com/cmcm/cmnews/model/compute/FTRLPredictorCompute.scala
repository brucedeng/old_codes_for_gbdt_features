package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters
import org.apache.spark.rdd.RDD

import scala.util.Try

/**
  * Created by lilonghua on 16/9/28.
  */
trait FTRLPredictorCompute extends EventBatchCompute {
  this: SparkBatchContext =>

  import Parameters._

  override def compute: Unit = {
    super.compute
    logInfo("Start FTRLPredictorCompute")

    val broad = Try(batchContext(constant_lr_broadcast)).getOrElse("false").toBoolean
    val parallel = Try(batchContext(constant_parallelism)).getOrElse("100").toInt

    val trainData = getRDD[RDD[String]](constant_lr_train_rdd, this)
    val modelData = getRDD[RDD[String]](constant_lr_model_rdd, this)

    val parseTrain = trainData.filter(f => {
      f.split("\t").length == 4
    }).zipWithUniqueId().flatMap(f => {
      val feat = f._1.split("\t")
      feat(3).split(" ").map((_,feat(0) + "\t" + feat(1) + "\t" + feat(2).split(":")(0) + "\t" + f._2.toString))
    }).filter(f => f != null && f._1.nonEmpty)

    val parseModel = modelData.map(m => {
      val f = m.split("\t")
      if (f.length == 2) {
        (f(0),Try(f(1).toDouble).getOrElse(0d))
      } else {
        ("", 0d)
      }
    }).filter(f => f != null && f._1.nonEmpty)

    val scoreData =  if (broad) {
      val broadcastModel = getSparkContext.broadcast(parseModel.collectAsMap())

      parseTrain.map(f => {
        (f._2, broadcastModel.value.getOrElse(f._1, 0d))
      }).reduceByKey((f1, f2) => { f1 + f2})
    } else {
      /*parseTrain.join(parseModel, parallel).filter(f => {
        f._2._1 != null && f._2._1.nonEmpty
      }).map(f => {
        (f._2._1, f._2._2)
      }).reduceByKey((f1, f2) => { f1 + f2})*/
      parseTrain.leftOuterJoin(parseModel, parallel).map(f => {
        (f._2._1, f._2._2.getOrElse(0d))
      }).reduceByKey((f1, f2) => { f1 + f2})
    }

    val scoreOutputData = scoreData
      .map(f => f._1.split("\t").dropRight(1).mkString("\t") + "\t" + f._2.toString)

    rddContext += (constant_lr_score_rdd -> scoreOutputData)
  }

}
