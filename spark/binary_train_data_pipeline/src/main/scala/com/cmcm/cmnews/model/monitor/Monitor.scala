package com.cmcm.cmnews.model.monitor

import com.cmcm.cmnews.model.config.ConfigUnit
import com.cmcm.cmnews.model.monitor.falcon.FalconMonitor

/**
 * Created by hanbin on 15/9/28.
 */

case object  MonitorMetrics {

  // lr train data falcon metrics
  val LR_OUTPUT_TRAIN_DATA_COUNT = "lr_output_train_data_count"
  val LR_POSITIVE_TRAIN_DATA_COUNT = "lr_positive_train_data_count"
  val LR_NEGATIVE_TRAIN_DATA_COUNT = "lr_negative_train_data_count"
  val LR_TRAIN_DATA_MAX_FEAT = "lr_train_data_max_feat"
  val LR_EMPTY_TRAIN_DATA_FEAT = "lr_empty_train_data_feat"
  val LR_NONEMPTY_TRAIN_DATA_FEAT = "lr_nonempty_train_data_feat"
  val LR_NONEMPTY_LENGTH_TRAIN_DATA_FEAT = "lr_nonempty_length_train_data_feat"

  val CFB_INPUT_EVENT_COUNT = "lr_input_event_count"
  val CFB_EVENT_COUNT = "lr_event_count"
  val CFB_LABEL_COUNT = "lr_label_count"
  val CFB_PARSED_COUNT = "lr_parsed_count"
  val CFB_AGGREGATION_COUNT = "lr_aggregation_count"

  val CFB_CP_COUNT = "lr_cp_count"
  val CFB_CP_RATE = "lr_cp_rate"
  val CFB_UP_COUNT = "lr_up_count"
  val CFB_UP_RATE = "lr_up_rate"

  val CFB_AGGRE_MAX_LATENCY = "lr_aggre_max_latency"
  val CFB_AGGRE_MIN_LATENCY = "lr_aggre_min_latency"
  val CFB_AGGRE_AVG_LATENCY = "lr_aggre_avg_latency"

  val CFB_OUTPUT_TOTAL_COUNT = "lr_output_total_count"
  val CFB_OUTPUT_USER_FEATURE_COUNT = "lr_output_user_feature_count"
  val CFB_OUTPUT_DOCUMENT_FEATURE_COUNT = "lr_output_document_feature_count"
  val CFB_OUTPUT_CROSS_FEATURE_COUNT = "lr_output_cross_feature_count"
  val CFB_OUTPUT_MATCH_FEATURE_COUNT = "lr_output_match_feature_count"
  val CFB_OUTPUT_DUMMY_NO_CP_FEATURE_COUNT = "lr_output_dummy_no_cp_feature_count"
  val CFB_OUTPUT_DUMMY_NO_CP_FEATURE_RATIO = "lr_output_dummy_no_cp_feature_ratio"
  val CFB_OUTPUT_DUMMY_OUT_OF_DATE_FEATURE_COUNT = "lr_output_dummy_out_of_date_feature_count"

  val LR_FEATURE_HIT_RATION = "lr_feature_hit_rate"
}

trait Monitor {

  def init(modelConfigUnit: ConfigUnit)
  def recordFromDriver(key: String, value: String, dims: Map[String, String] = Map())
  def recordFromPartition(key: String, value: String, partitionId: Int, dims: Map[String, String] = Map())

}

case class MonitorProxy() extends Monitor {

  var monitor: Monitor = _

  override def init(modelConfigUnit: ConfigUnit): Unit = {

    val monitorType = modelConfigUnit.getString("monitor.type", "falcon")
    if (monitorType == "falcon") {
      monitor = new FalconMonitor()
      monitor.init(modelConfigUnit)
      println("Initial monitor falcon is ok")
    } else {
      println("Initial monitor falcon is error")
    }

  }

  override def recordFromPartition(key: String, value: String, partitionId: Int, dims: Map[String, String] = Map()): Unit =
    monitor.recordFromPartition(key, value, partitionId)

  override def recordFromDriver(key: String, value: String, dims: Map[String, String] = Map()): Unit =
    monitor.recordFromDriver(key, value)
}

