package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.component.MonitorComponent
import com.cmcm.cmnews.model.compute.EventBatchCompute
import com.cmcm.cmnews.model.input.EventBatchInput
import com.cmcm.cmnews.model.output.EventBatchOutput
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{LoggingUtils, Parameters}

/**
  * Created by lilonghua on 2016/11/24.
  */
trait BatchWithMonitorApp {
  this: SparkBatchContext
    with EventBatchInput
    with EventBatchCompute
    with MonitorComponent
    with EventBatchOutput =>

  def initApp(configDir: String = ""): Unit = {
    logInfo(s"Start init app with $configDir ...")
    init(configDir)
    initMonitor(configDir)
  }

  def processApp(): Unit ={
    logInfo("Start process app...")
    getInput
    compute
    monitor
    outPut
  }

  def postApp(): Unit = {
    sbc.stop()
  }

  def main(args:Array[String]) = {
    args.foreach(arg => {
      val items = arg.split("=")
      if (items.length == 2) {
        LoggingUtils.loggingInfo(s"argument: key(${items.head}), value(${items.last})")
        batchContext += (items.head -> items.last)
      } else {
        logError(s"Invalid parama => $arg")
      }
    })

    initApp(batchContext.getOrElse(Parameters.constant_conf_dir, ""))
    processApp()
    postApp()
  }
}
