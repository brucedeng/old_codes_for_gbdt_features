package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.UserExploreCompute
import com.cmcm.cmnews.model.input.UPExploreDataInput
import com.cmcm.cmnews.model.output.BinaryTrainDataOutput
import com.cmcm.cmnews.model.spark.BinaryTrainDataSparkContext

/**
  * Created by lilonghua on 2016/11/14.
  */
object UserExploreApp extends BatchNewApp
  with BinaryTrainDataSparkContext
  with UPExploreDataInput
  with UserExploreCompute
  with BinaryTrainDataOutput
