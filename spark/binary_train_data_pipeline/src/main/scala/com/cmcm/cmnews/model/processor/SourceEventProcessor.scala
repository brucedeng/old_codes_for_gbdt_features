package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{FeatureTools, JsonUtil, LoggingUtils, Parameters}
import org.apache.spark.rdd.RDD
import org.json4s.JsonAST._

import scala.util.{Failure, Success, Try}

/**
 * Created by tangdong on 27/4/16.
 */
object SourceEventProcessor extends Processor {
  this: SparkBatchContext =>
  import Parameters._

  def preprocessAct(line:String,batchContext: collection.mutable.Map[String,String], filterAct:String) = {
    val eventPid = Try(batchContext(constant_event_pid)).getOrElse("0")
    val cType = Try(batchContext(constant_event_ctype)).getOrElse("1,0x01")
    val eventLanRegion = Try(batchContext(constant_event_lan_region)).getOrElse("")
    val position = Try(batchContext(constant_event_position)).getOrElse("")
    val WND = Try(batchContext(constant_wnd)).getOrElse("600").toInt
    try {
      Try(JsonUtil.convertToJValue(line)) match {
        case Success(jvalue) =>
          val uid = extractStr(jvalue, "aid", "")
          val tApplan = extractStr(jvalue, "app_lan", "en")
          val lan = if (tApplan.contains("hi")) "hi" else tApplan
          val country = extractStr(jvalue, "country", "")
          val cid = extractStr(jvalue, "contentid", "")
          val ip = extractStr(jvalue, "ip", "unknown")
          val ts = extractStr(jvalue, "servertime_sec", "0").toLong
          val pid = extractStr(jvalue, "pid", "")
          val ctype = extractStr(jvalue, "ctype", "1")
          val act = extractStr(jvalue, "act", "")
          val city = extractStr(jvalue, "city", "")
          val des = extractObjectStr(jvalue, "cpack", "des", "unknown")
          val dwelltime = extractObjectStr(jvalue, "ext", "dwelltime", "0").toInt
          val level1Type = extractObjectStr(jvalue, "scenario", "level1_type", "")
          val level1 = extractObjectStr(jvalue, "scenario", "level1", "")
          val net = extractStr(jvalue, "net","")
          val osVersion = extractStr(jvalue, "osv", "")
          val appVersion = extractStr(jvalue, "appv", "")
          val appIv = extractStr(jvalue, "apiv", "")
          val platForm = extractStr(jvalue, "pf", "")
          val brand = extractStr(jvalue, "brand", "")
          val model = extractStr(jvalue, "model", "")
          val channel = extractStr(jvalue, "ch", "")
          val isHot = extractObjectStr(jvalue, "cpack", "ishot", "")
          val lanRegion = lan + "_" + country
          //val join_ts = ts / WND * WND - WND
          var dwell = 0
          if ("4" == act) {
            if (dwelltime > 600) {
              dwell = 600
            } else {
              dwell = dwelltime
            }
          }

          val reqid = parseRid(des)

          val flag = if (pid.nonEmpty && position.nonEmpty && cType.nonEmpty) {
            position.split(",").contains(s"${level1Type}_$level1") && cType.split(",").contains(ctype)
          } else {
            if (pid == "11") {
              ("1" == level1Type || "10" == level1Type) && "1" == level1 && ("" == ctype || "1" == ctype || "0x01" == ctype)
            } else {
              "" == ctype || "1" == ctype || "0x01" == ctype
            }
          }

          if (uid.nonEmpty && uid != "unknown" && eventPid == pid && flag && filterAct == act && eventLanRegion.nonEmpty
            && eventLanRegion.split(",").exists(p => {p.contains(lanRegion.toLowerCase) || lanRegion.toLowerCase.contains(p)})
            && cid != "unknown" && cid.nonEmpty) {
            val label = act match {
              case "1" => "0"
              case "2" => "1"
              case _ => "-1"
            }
            val key = uid + fieldDelimiter + cid + fieldDelimiter + reqid + fieldDelimiter + pid

            if (act == "4") {
              (key, dwell.toString)
            } else {
              val feature = FeatureTools()
              feature.addFeature("U_CITY", city)
              feature.addFeature("U_COUNTRY",
                if ("PY,PA,HN,AR,SV,VE,CU,DO,PE,CO,CL,CR,EC,NI,BO,GT,UY".contains(country)) "LM" else country)
              //feature.addFeature("C_TYPE", if (ctype.length > 0) ctype else "1")
              feature.addFeature("U_LAN", lan)
              //feature.addFeature("PID", pid)
              feature.addFeature("SERVER_TIME",ts.toString)
              feature.addFeature("U_POSITION", s"${pid}_${level1Type}_$level1")
              feature.addFeature("U_NET", net)
              feature.addFeature("U_OSVERSION", osVersion)
              feature.addFeature("U_APPVERSION", appVersion)
              feature.addFeature("U_PLATFORM", platForm)
              feature.addFeature("U_BRAND", brand)
              feature.addFeature("U_MODEL", model)
              feature.addFeature("U_CHANNEL", channel)
              feature.addFeature("U_ISHOT", isHot)
              feature.addFeature("U_APPIV", appIv)
              feature.addFeature("LABEL", label)
              /*feature.addFeature("DWELL", dwell.toString)*/
              (key, feature.genFeatureString())
            }
          } else {
            ("", "")
          }
        case Failure(ex) =>
          LoggingUtils.loggingError(LoggingUtils.getException(ex))
          ("", "")
      }
    } catch {
      case e: Exception =>
        LoggingUtils.loggingError(e.toString)
        ("", "")
    }
  }



  def processAct(inputRDD:RDD[String], batchContext: collection.mutable.Map[String,String], act:String): RDD[(String, String)] = {

    if (act == "4") {
      inputRDD.map(preprocessAct(_, batchContext, act))
        .filter(x => x._1.nonEmpty)
        .reduceByKey((x,y) => math.max(x.toInt, y.toInt).toString)
    } else {
      inputRDD.map(preprocessAct(_, batchContext, act))
        .filter(x => x._1.nonEmpty)
        .reduceByKey((x,y) => {
          val xFeat = FeatureTools(x)
          val yFeat = FeatureTools(y)

          val smax = (x1:String,y1:String) => if(x1.compareTo(y1) > 0) x1 else y1
          //val smin = (x1:String,y1:String) => if(x1.compareTo(y1) < 0) x1 else y1

          val feature = FeatureTools()
          feature.addFeature("U_CITY", smax(xFeat.getFeatureValue("U_CITY").getOrElse(""), yFeat.getFeatureValue("U_CITY").getOrElse("")))
          feature.addFeature("U_COUNTRY", smax(xFeat.getFeatureValue("U_COUNTRY").getOrElse(""), yFeat.getFeatureValue("U_COUNTRY").getOrElse("")))
          feature.addFeature("U_LAN", smax(xFeat.getFeatureValue("U_LAN").getOrElse(""), yFeat.getFeatureValue("U_LAN").getOrElse("")))
          feature.addFeature("SERVER_TIME", math.min(xFeat.getFeatureValue("SERVER_TIME").getOrElse("0").toLong, yFeat.getFeatureValue("SERVER_TIME").getOrElse("0").toLong).toString)
          feature.addFeature("U_POSITION", xFeat.getFeatureValue("U_POSITION").getOrElse(""))
          feature.addFeature("U_NET", xFeat.getFeatureValue("U_NET").getOrElse(""))
          feature.addFeature("U_OSVERSION", xFeat.getFeatureValue("U_OSVERSION").getOrElse(""))
          feature.addFeature("U_APPVERSION", xFeat.getFeatureValue("U_APPVERSION").getOrElse(""))
          feature.addFeature("U_PLATFORM", xFeat.getFeatureValue("U_PLATFORM").getOrElse(""))
          feature.addFeature("U_BRAND", xFeat.getFeatureValue("U_BRAND").getOrElse(""))
          feature.addFeature("U_MODEL", xFeat.getFeatureValue("U_MODEL").getOrElse(""))
          feature.addFeature("U_CHANNEL", xFeat.getFeatureValue("U_CHANNEL").getOrElse(""))
          feature.addFeature("U_ISHOT", xFeat.getFeatureValue("U_ISHOT").getOrElse(""))
          feature.addFeature("U_APPIV", xFeat.getFeatureValue("U_APPIV").getOrElse(""))

          feature.genFeatureString()
        })
    }
  }

  def extractStr(jvalue: JValue, fieldName: String, default: String = "") = {
    jvalue \ fieldName match {
      case JString(s) => if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString
      case JDouble(s) => s.toString
      case _ => default
    }
  }

  def extractObjectStr(jvalue : JValue, objectName: String, fieldName:String, default:  String = "") = {
    jvalue \ objectName \ fieldName match {
      case JString(s) =>  if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString
      case JDouble(s) => s.toString
      case _ => default
    }
  }

  def parseRid(des: String) = {
    val data = des.split("\\|")
    val dataMap = scala.collection.mutable.Map[String, String]()
    for (kv <- data) {
      val tArray = kv.split("=")
      if (2 == tArray.size ) {
        dataMap.update(tArray(0), tArray(1))
      }
    }
    dataMap.getOrElse("rid", "")
  }
}
