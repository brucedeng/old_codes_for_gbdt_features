package com.cmcm.cmnews.model.monitor

import com.cmcm.cmnews.model.component.MonitorComponent
import com.cmcm.cmnews.model.parse.FeatureHitRate
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters._
import org.apache.spark.rdd.RDD

/**
  * Created by lilonghua on 2016/11/24.
  */
trait FTRLFeatureHitRateMonitor extends MonitorComponent {
  this: SparkBatchContext =>
  override def monitor: Unit = {
    super.monitor
    logInfo("Start FTRLFeatureHitRateMonitor")

    val monitorRdd = rddContext(constant_lr_feature_consist_monitor_rdd).asInstanceOf[RDD[FeatureHitRate]]

    //monitorRdd.foreach(f => MonitorComponent.monitor.recordFromDriver(f.feature + "_" + MonitorMetrics.LR_FEATURE_HIT_RATION, f.offRate.toString))

  }
}
