package com.cmcm.cmnews.model.util

import java.nio.{ByteBuffer, ByteOrder}

import scala.util.Try

object Util extends Serializable {

  val defaultNu = -10000000
  val defaultStr = ""

  def double2Bytes(data: Double) = {
    val bytes: ByteBuffer = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN)
    bytes.putDouble(data).array()
  }

  def long2Bytes(data: Long) = {
    val bytes: ByteBuffer = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN)
    bytes.putLong(data).array()
  }

  def caculatScore(features: String) = {
    val scores = Try(features.split(Parameters.pairGap).map(f => {
      val ft = f.split(Parameters.keyGap)
      Try(ft.last.toFloat).getOrElse(0f)
    }).sum).getOrElse(0f)

    1 / (1 + math.exp((-1.0) * scores))
  }

  def getMax(x: String, y: String): String = {
    if(x.compareTo(y) > 0) x else y
  }

  def getDwell(act: Int, readTime: Int,threshold: Int = 600): Int = {
    if (4 == act) {
      if (readTime > threshold) {
        threshold
      } else {
        readTime
      }
    } else {
      0
    }
  }

  def compareFeat(onlineFeat: String, offlineFeat: String, gap: String = "\u0001") : String = {
    val online = onlineFeat.split(Parameters.pairGap).map(f => {
      f.split(Parameters.keyGap).head
    })

    val offline = offlineFeat.split(" ").map(f => f.replace(gap, "_"))

    val onlineFeatures = online.map(f => {
      val t = f.split("_",2)
      (t.head,t.last)
    }).groupBy(_._1).map(f => (f._1, f._2.map(_._2)))

    val offlineFeatures = offline.map(f => {
      val t = f.split("_",2)
      (t.head,t.last)
    }).groupBy(_._1).map(f => (f._1, f._2.map(_._2)))

    val stat = offlineFeatures.map(f => {
      val t = onlineFeatures.getOrElse(f._1,Array.empty[String])
      if (t.isEmpty) {
        (f._1, 0f)
      } else {
        if (f._2.isEmpty) {
          (f._1, -t.length.toFloat)
        } else {
          (f._1, f._2.count(p => t.contains(p)) / f._2.length.toFloat)
        }
      }
    }).toSeq.sortBy(_._1).map(f => f._1 + ":" + f._2).mkString(",")

    val allStat = if (offline.nonEmpty) {
      offline.count(online.contains(_)) / offline.length.toFloat
    } else {
      0.0f
    }

    val append = offline.filter(f => !online.contains(f)).map(f => f + ":append")/*.sorted*/
    val missing = online.filter(f => !offline.contains(f)).map(f => f + ":missing")/*.sorted*/
    val matching = online.filter(f => offline.contains(f)).map(f => f + ":matching")/*.sorted*/

    //append.mkString(",") + "\t" + missing.mkString(",")
    (append ++ missing).sorted.mkString(",") + "\t" + matching.sorted.mkString(",") + "\t" + stat + "\tall:" + allStat
  }

  def statFeat(onlineFeat: String, offlineFeat: String, gap: String = "\u0001"): List[(String, (Int, Int, Int))] = {
    val online = onlineFeat.split(Parameters.pairGap).map(f => {
      f.split(Parameters.keyGap).head
    })

    val offline = offlineFeat.split(" ").map(f => f.replace(gap, "_"))

    val onlineFeatures = online.map(f => {
      val t = f.split("_",2)
      (t.head,t.last)
    }).groupBy(_._1).map(f => (f._1, f._2.map(_._2)))

    val offlineFeatures = offline.map(f => {
      val t = f.split("_",2)
      (t.head,t.last)
    }).groupBy(_._1).map(f => (f._1, f._2.map(_._2)))

    //val allStat = offline.count(online.contains(_))

    val onlineOnly = onlineFeatures.filter(f => !offlineFeatures.contains(f._1)).map(f => (f._1, (0, 0, f._2.length)))

    val offlineJoin = offlineFeatures.map(f => {
      val t = onlineFeatures.getOrElse(f._1,Array.empty[String])
      (f._1, (f._2.count(t.contains(_)), f._2.length, t.length))
    })

    (offlineJoin ++ onlineOnly).toList
  }
}
