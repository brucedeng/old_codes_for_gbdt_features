package com.cmcm.cmnews.model.input

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters

import scala.util.Try

/**
  * Created by lilonghua on 2016/10/21.
  */
trait FTRLFeatureConsistDataInput extends EventBatchInput{
  this: SparkBatchContext =>
  import Parameters._

  override def getInput: Unit ={
    logInfo("start FTRLFeatureConsistDataInput...")
    setRDD(constant_lr_train_feature_path, constant_lr_train_feature_rdd, this)
    setRDD(constant_lr_server_feature_path, constant_lr_server_feature_rdd, this)
    val stat = Try(batchContext(constant_feature_stat_type)).getOrElse("all")
    if ("trace" == stat) {
      setRDD(constant_content_input, constant_content_rdd, this)
      setRDD(constant_user_input, constant_user_rdd, this)
    }
  }

}
