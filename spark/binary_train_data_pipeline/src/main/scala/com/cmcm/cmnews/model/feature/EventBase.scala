package com.cmcm.cmnews.model.feature

import com.cmcm.cmnews.model.util.JsonUtil
import org.apache.spark.Logging

/**
  * Created by lilonghua on 2016/11/17.
  */
case class EventBase (
                       ctype: String,
                       act: Int,
                       city: String,
                       country: String,
                       lan: String,
                       pid: String,
                       level1Type: String,
                       level1: String,
                       net: String,
                       osVersion: String,
                       appVersion: String,
                       platform: String,
                       brand: String,
                       model: String,
                       channel: String,
                       isHot: Int,
                       appIV: Int
                     ) extends Serializable with Logging {

  override def toString: String = {
    JsonUtil.toJson(Map("ctype" -> ctype,
      "act" -> act,
      "city" -> city,
      "country" -> country,
      "lan" -> lan,
      "pid" -> pid,
      "level1Type" -> level1Type,
      "level1" -> level1,
      "net" -> net,
      "osVersion" -> osVersion,
      "appVersion" -> appVersion,
      "platform" -> platform,
      "brand" -> brand,
      "model" -> model,
      "channel" -> channel,
      "isHot" -> isHot,"appIV" -> appIV))
  }

}
