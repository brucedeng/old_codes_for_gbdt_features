package com.cmcm.cmnews.model.spark

import com.cmcm.cmnews.model.component.BatchComponent
import org.apache.spark.SparkContext

/**
 * Created by tangdong on 27/4/16.
 */
trait SparkBatchContext extends BatchComponent {
  @transient protected var sbc:SparkContext = _
  protected val rddContext:scala.collection.mutable.Map[String, Any] = scala.collection.mutable.Map[String, Any]()
  protected val batchContext:scala.collection.mutable.Map[String,String] = scala.collection.mutable.Map[String, String]()

  override def init(configDir: String = "") = {
    super.init(configDir)
    logInfo("SparkBatchContext finished init ...")
  }

  def getSparkContext : SparkContext = sbc

  def setSparkContext(_ssc:SparkContext) = {
    sbc = _ssc
  }

  def getRddContext = rddContext
  def setRddContext(key:String, value:Any) = rddContext += (key -> value)
  def getBatchContext = batchContext
  def setBatchContext(key:String, value:String) = batchContext += (key -> value)
}
