package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.FTRLSerializeCompute
import com.cmcm.cmnews.model.input.FTRLSerializeDataInput
import com.cmcm.cmnews.model.output.FTRLSerializeDataOutput
import com.cmcm.cmnews.model.spark.NormalSparkContext

/**
  * Created by lilonghua on 16/10/9.
  */
object FTRLSerializeApp extends BatchApp
  with NormalSparkContext
  with FTRLSerializeDataInput
  with FTRLSerializeCompute
  with FTRLSerializeDataOutput
