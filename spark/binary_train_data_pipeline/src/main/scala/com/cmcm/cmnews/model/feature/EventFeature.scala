package com.cmcm.cmnews.model.feature

import com.cmcm.cmnews.model.util.JsonUtil
import org.apache.spark.Logging

/**
  * Created by lilonghua on 2016/10/28.
  */
///////////
//gmp
case class GmpInfo (
                      gmpScore: Double,
                      gmpVersion: String)extends Serializable with Logging 
{
    def getGmpInfo(gmpUpperBound:Double = 1.0 , gmpLowerBound:Double = 0.0):String={
      getGmpScore(gmpUpperBound, gmpLowerBound).toString + "_" + gmpVersion
    }

    def getGmpScore(gmpUpperBound:Double = 1.0, gmpLowerBound:Double = 0.0):Double ={
      if(gmpScore > -0.5)
      {
        val clipedGmp = (gmpScore >= gmpLowerBound && gmpScore <= gmpUpperBound) match {
            case true => gmpScore
            case false => if (gmpScore < gmpLowerBound) gmpLowerBound else gmpUpperBound
      }
        clipedGmp
      }
      else
        gmpScore
    }

    def getGmpVersion:String = gmpVersion

    override def toString:String={
      JsonUtil.toJson(Map("gmpScore" -> gmpScore,
      "gmpVersion" -> gmpVersion))
    }

}
//////////

case class EventFeature (
                          eventBase: EventBase,
                          readTime: Double,
                          serverTime: Long,
                          eventTime: Long,
                          aid: String,
                          cid: String,
                          riq: String,
                          src: String,
                          gmp: GmpInfo
                        ) extends Serializable with Logging {

  def getPosition: String = {
    s"${eventBase.pid}_${eventBase.level1Type}_${eventBase.level1}"
  }

  def getLanRegion: String = {
    if (eventBase.lan.isEmpty && eventBase.country.isEmpty) {
      ""
    } else {
      (eventBase.lan + "_" + eventBase.country).toLowerCase
    }
  }

  def getLable: Int = {
    eventBase.act match {
      case 1 => 0
      case 2 => 1
      case 4 => 1
      case _ => -1
    }
  }

  def getDwell(threshold: Int = 600): Double = {
    if (4 == eventBase.act) {
      if (readTime > threshold) {
        threshold
      } else {
        readTime
      }
    } else {
      0.0d
    }
  }

  def extractAct(eventPid: String, cType: String, eventLanRegion: String, position: String): Boolean = {
    val flag = if (eventBase.pid.nonEmpty && position.nonEmpty && cType.nonEmpty) {
      position.split(",").contains(s"${eventBase.level1Type}_${eventBase.level1}") && cType.split(",").contains(eventBase.ctype)
    } else {
      if (eventBase.pid == "11") {
        ("1" == eventBase.level1Type || "10" == eventBase.level1Type) && "1" == eventBase.level1 && ("" == eventBase.ctype || "1" == eventBase.ctype || "0x01" == eventBase.ctype)
      } else {
        "" == eventBase.ctype || "1" == eventBase.ctype || "0x01" == eventBase.ctype
      }
    }

    aid.nonEmpty && eventPid == eventBase.pid && flag && eventLanRegion.nonEmpty && eventLanRegion.split(",").exists(p => {p.contains(getLanRegion.toLowerCase) || getLanRegion.toLowerCase.contains(p)}) && cid != "unknown" && cid.nonEmpty
  }

  def validLog(cType: String, eventLanRegion: String, position: String): Boolean = {
    val flag = eventLanRegion.split(",").exists(getLanRegion.matches(_))
    aid.nonEmpty && cid.nonEmpty && position.split(",").contains(getPosition) && cType.split(",").contains(eventBase.ctype) && flag
  }

  def getUnqId = {
    aid + cid + eventBase.pid + riq
  }

  def getUnqIdCm = {
    aid + cid + riq
  }

  /////////////////////////
  //gmp
  def getGmpInfo(gmpUpperBound:Double, gmpLowerBound:Double):String={
    gmp.getGmpInfo(gmpUpperBound, gmpLowerBound)
  }

  def getGmpScore(gmpUpperBound:Double, gmpLowerBound:Double):Double ={
    gmp.getGmpScore(gmpUpperBound, gmpLowerBound)
  }

  def getGmpVersion:String = gmp.getGmpVersion
  /////////////////////////

  override def toString: String = {
    JsonUtil.toJson(Map("ctype" -> eventBase.ctype,
      "act" -> eventBase.act,
      "city" -> eventBase.city,
      "country" -> eventBase.country,
      "lan" -> eventBase.lan,
      "pid" -> eventBase.pid,
      "serverTime" -> serverTime,
      "level1Type" -> eventBase.level1Type,
      "level1" -> eventBase.level1,
      "net" -> eventBase.net,
      "osVersion" -> eventBase.osVersion,
      "appVersion" -> eventBase.appVersion,
      "platform" -> eventBase.platform,
      "brand" -> eventBase.brand,
      "model" -> eventBase.model,
      "channel" -> eventBase.channel,
      "isHot" -> eventBase.isHot,
      "appIV" -> eventBase.appIV,
      "readTime" -> readTime,
      "aid" -> aid,
      "cid" -> cid,
      "riq" -> riq,
      "src" -> src,
      "gmpScore" -> gmp.gmpScore,
      "gmpVersion" -> gmp.gmpVersion))
  }
}

/*object EventFeature extends Serializable {
  def apply(ctype: String,
            act: Int,
            city: String,
            country: String,
            lan: String,
            pid: String,
            serverTime: Long,
            level1Type: String,
            level1: String,
            net: String,
            osVersion: String,
            appVersion: String,
            platform: String,
            brand: String,
            model: String,
            channel: String,
            isHot: String,
            appIV: String,
            readTime: Int,
            aid: String,
            cid: String,
            riq: String) = {
    new EventFeature(
      ctype,
      act,
      city,
      country,
      lan,
      pid,
      serverTime,
      level1Type,
      level1,
      net,
      osVersion,
      appVersion,
      platform,
      brand,
      model,
      channel,
      isHot,
      appIV,
      readTime,
      aid,
      cid,
      riq
    )
  }

  def getMax(x: String, y: String): String = {
    if(x.compareTo(y) > 0) x else y
  }
}*/
