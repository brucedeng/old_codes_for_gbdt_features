package com.cmcm.cmnews.model.output

import com.cmcm.cmnews.model.component.BatchComponent
import com.cmcm.cmnews.model.config.Configurable
import com.cmcm.cmnews.model.spark.SparkBatchContext

/**
 * Created by tangdong on 27/4/16.
 */
trait EventBatchOutput extends BatchComponent {
  this: SparkBatchContext =>
  def outPut: Unit = {
    logInfo("Empty output in EventBatchOutput...")
  }
}
