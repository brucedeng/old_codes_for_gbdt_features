package com.cmcm.cmnews.model.monitor.falcon

import java.net.InetAddress

import com.cmcm.cmnews.model.config.ConfigUnit
import com.cmcm.cmnews.model.monitor.Monitor
import org.apache.spark.Logging

/**
 * Created by hanbin on 15/11/17.
 */
case class FalconMonitor() extends Logging with Monitor {

  var enabled: Boolean = false
  var debugType: Boolean = false
  var url: String = "http://127.0.0.1:1988/v1/push"
  var driver: String =_
  var partitionPostfix: String =_
  var sender: FalconSender =_
  var step: Int = 600

  override def init(modelConfigUnit: ConfigUnit): Unit = {
    enabled = modelConfigUnit.getBoolean("monitor.enabled", false)
    if (enabled) {
      debugType = modelConfigUnit.getBoolean("monitor.debug", false)
      url = modelConfigUnit.getString("monitor.falcon.url", url)
      driver = modelConfigUnit.getString("monitor.driver", "unknown_driver")
      partitionPostfix = modelConfigUnit.getString("monitor.partition_postfix", "unknown_partition")
      step = modelConfigUnit.getInt("monitor.falcon.step", step)
      sender = new FalconSender(url)
    }
  }

  def init() = {
    enabled = true
    driver = "driver.news.dl.lr_feature_hit.cmcm.com"
    partitionPostfix = ".news.dl.lr_feature_hit.cmcm.com"
    sender = new FalconSender(url)
  }

  override def recordFromPartition(key: String, value: String, partitionId: Int, dims: Map[String, String] = Map()): Unit = {
    val partitionName = f"partition$partitionId%04d$partitionPostfix%s"
    record(key, value, partitionName, dims)
  }

  override def recordFromDriver(key: String, value: String, dims: Map[String, String] = Map()): Unit = {
    record(key, value, driver, dims)
  }

  def record(key: String, value: String, identification: String, dims: Map[String, String]) = {
    if (enabled) {
      val inetAddress = InetAddress.getLocalHost
      val timestamp = System.currentTimeMillis() / 1000
      val tags = (dims + (("id" ,identification))).map(pair => pair._1 + "=" + pair._2).mkString(",")
      val record = new FalconRecord(inetAddress.getHostName, key, timestamp,
        step, value.toDouble, "GAUGE", tags + s",id=$identification")
      if (!debugType) {
        val result = sender.sendRecord(record)
        if (!result) {
          logWarning(s"Fail to record key = $key, value = $value, tags = $tags")
        } else {
          //logInfo(s"Success to record key = $key, value = $value, tags = $tags")
        }
      } else {
        println(s"Falcon test => record key = $key, value = $value, tags = $tags")
      }
    }
  }
}
