package com.cmcm.cmnews.model.monitor.falcon

import com.google.gson.Gson

/**
 * Created by hanbin on 15/11/17.
 */
case class FalconRecord(endpoint: String,
                         metric: String,
                         timestamp: Long,
                         step: Int,
                         value: Double,
                         counterType: String,
                         tags: String) {

  def toJson() = {
    val gson = new Gson;
    gson.toJson(this)
  }

}
