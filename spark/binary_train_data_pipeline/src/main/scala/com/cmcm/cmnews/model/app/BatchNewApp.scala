package com.cmcm.cmnews.model.app

import com.cmcm.cmnews.model.compute.EventBatchCompute
import com.cmcm.cmnews.model.input.EventBatchInput
import com.cmcm.cmnews.model.output.EventBatchOutput
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.LoggingUtils

/**
  * Created by lilonghua on 2016/10/21.
  */
trait BatchNewApp {
  this: SparkBatchContext
    with EventBatchInput
    with EventBatchCompute
    with EventBatchOutput =>

  def initApp(configDir: String = ""): Unit = {
    logInfo(s"Start init app with $configDir ...")
    init(configDir)
  }

  def processApp(): Unit ={
    logInfo("Start process app...")
    getInput
    compute
    outPut
  }

  def postApp(): Unit = {
    sbc.stop()
  }

  def main(args:Array[String]) = {
    args.foreach(arg => {
      val items = arg.split("=")
      if (items.length == 2) {
        LoggingUtils.loggingInfo(s"argument: key(${items.head}), value(${items.last})")
        batchContext += (items.head -> items.last)
      } else {
        logError(s"Invalid parama => $arg")
      }
    })

    initApp()
    processApp()
    postApp()
  }
}
