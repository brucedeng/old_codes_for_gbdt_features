package com.cmcm.cmnews.model.compute

import com.cmcm.cmnews.model.processor.UserFilterProcessor
import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{JsonUtil, Parameters}
import org.apache.spark.rdd.RDD

import scala.util.Try

/**
  * Created by lilonghua on 2016/11/7.
  */
trait UserFilterCompute extends EventBatchCompute {
  this: SparkBatchContext =>

  import Parameters._

  override def compute: Unit = {
    super.compute
    logInfo("Start UserFilterCompute")

    val parallel = Try(batchContext(constant_parallelism)).getOrElse("100").toInt
    val userFilter = Try(batchContext(constant_up_user_filter)).getOrElse("true").toLowerCase
    println(s"No tags user filter => $userFilter")

    //get rdd from path
    val upRdd = getRDD[RDD[String]](constant_user_rdd, this)

    val parsedUpRdd = UserFilterProcessor.process(upRdd, this.getBatchContext).filter(f => {
      if (userFilter == "true") {
        f.get.catLen > 0 && f.get.keyLen > 0
      } else {
        true
      }
    }).map(f => (f.get.aid, f.get)).reduceByKey((f1, f2) => f1).map(f => {
      val categories = f._2.baseFeature.category.map(cat => {
        Map("name" -> cat._1, "weight" -> cat._2)
      })
      val keywords = f._2.baseFeature.keyword.map(cat => {
        Map("name" -> cat._1, "weight" -> cat._2)
      })
      val topics = f._2.baseFeature.topic.map(cat => {
        Map("name" -> cat._1, "weight" -> cat._2)
      })
      val entities = f._2.baseFeature.entity.map(cat => {
        Map("name" -> cat._1, "weight" -> cat._2)
      })
      val imageLabel = f._2.baseFeature.imageLabel.map(cat => {
        Map("name" -> cat._1, "weight" -> cat._2)
      })
      val notifyEntities = f._2.notifyEntity.map(cat => {
        Map("name" -> cat._1, "weight" -> cat._2)
      })
      val mapFeat = Map("uid" -> f._2.aid,
        "gender" -> f._2.gender,
        "age" -> f._2.age,
        "u_cat_len" -> f._2.catLen,
        "u_kw_len" -> f._2.keyLen,
        "u_topic_len" -> f._2.topicLen,
        "u_entities_v2_len" -> f._2.entityLen,
        "categories" -> categories,
        "keywords" -> keywords,
        "topics" -> topics,
        "entities_v2" -> entities,
        "image_label" -> imageLabel,
        "notify_entities" -> notifyEntities
      )

      JsonUtil.toJson(mapFeat)
    }).coalesce(parallel)

    rddContext += (constant_train_rdd -> parsedUpRdd)
  }
}
