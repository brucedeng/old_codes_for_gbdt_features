package com.cmcm.cmnews.model.processor

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.{JsonUtil, LoggingUtils, Parameters}
import org.apache.spark.rdd.RDD
import org.json4s.JsonAST._

import scala.util.{Failure, Success, Try}

/**
  * Created by lilonghua on 16/9/22.
  */
object UPNewProcessor extends Processor {
  this: SparkBatchContext =>

  import Parameters._

  override def process(inputRDD:RDD[String], batchContext: collection.mutable.Map[String,String]): RDD[(String, String)] = {
    val appLan = Try(batchContext(constant_up_app_lan_region)).getOrElse("").split(",")
    logInfo("filter appLan => " + appLan.mkString("\t"))
    inputRDD.map((line:String) => preprocess(line, batchContext)).filter(feat => {
      val f = feat._2.split(fieldDelimiter)
      //f.length == 7 && f.last.nonEmpty && f.last != "unknown"
      //f.length == 7 && appLan.contains(f.last)
      //f.length == 7 && appLan.exists(p => p.contains(f.last))
      f.length == 11 && f.last.nonEmpty && appLan.exists(p => f.last.contains(p))
    }).map(feat => {
      val f = feat._2.split(fieldDelimiter)
      (feat._1, f.dropRight(1).mkString(fieldDelimiter))
    })
  }

  override def preprocess(line: String, batchContext: collection.mutable.Map[String, String]) = {
    val topRelCat = Try(batchContext(constant_u_rel_ncat)).getOrElse("100").toInt
    val topRelKw = Try(batchContext(constant_u_rel_nkey)).getOrElse("100").toInt
    val topRelTopic = Try(batchContext(constant_u_rel_ntopic)).getOrElse("100").toInt
    val topRelEntity = Try(batchContext(constant_u_rel_nentity)).getOrElse("100").toInt
    val uTag = Try(batchContext(constant_up_uid_tag)).getOrElse("aid")


    Try(JsonUtil.convertToJValue(line)) match {
      case Success(jvalue) =>
        val app_lan = JsonUtil.getStr(jvalue, "app_lan", "")
        val uuid = JsonUtil.getStr(jvalue, uTag, "")
        val categories = jvalue \ "categories" match {
          case JArray(cats) => catKwAssist(cats, "", topRelCat, 0.0)
          case _ => ""
        }
        val keywords = jvalue \ "keywords" match {
          case JArray(cats) => catKwAssist(cats, "", topRelKw, 0.0)
          case _ => ""
        }
        val topics = jvalue \ "topics" match {
          case JArray(cats) => catKwAssist(cats, "", topRelTopic, 0.0)
          case _ => ""
        }
        val entities = jvalue \ "entities_v2" match {
          case JArray(cats) => catKwAssist(cats, "", topRelEntity, 0.0)
          case _ => ""
        }
        val gender = JsonUtil.getStr(jvalue, "gender", "")
        val age = JsonUtil.getStr(jvalue, "age", "")
        val catLen = JsonUtil.getStr(jvalue, "u_cat_len", "0")
        val kwLen = JsonUtil.getStr(jvalue, "u_kw_len", "0")
        val topicLen = JsonUtil.getStr(jvalue, "u_topic_len", "0")
        val entityLen = JsonUtil.getStr(jvalue, "u_entities_v2_len", "0")

        (uuid, categories + fieldDelimiter + keywords + fieldDelimiter + topics + fieldDelimiter + entities + fieldDelimiter + gender + fieldDelimiter + age + fieldDelimiter + catLen + fieldDelimiter + kwLen + fieldDelimiter + topicLen + fieldDelimiter + entityLen + fieldDelimiter + app_lan)
      case Failure(ex) =>
        LoggingUtils.loggingError(LoggingUtils.getException(ex))
        ("", "")

    }
  }

  def catKwAssist(jsonItems: List[JValue], version: String, top: Int, default: Double): String = {
    def parse_item(item: JValue) = {
      val name = (item \ "name").asInstanceOf[JString].s
      val L1_weight = item \ "weight" match {
        case JDouble(x) =>
          x.toDouble
        case JInt(x) =>
          x.toDouble
        case JString(x) =>
          Try(x.toDouble).getOrElse(default)
        case _ =>
          default
      }
      //name + keyValueDelimiter + L1_weight
      (name, L1_weight)
    }
    jsonItems.map(x => parse_item(x))
      .sortWith(_._2 > _._2).take(top).map(f => f._1 + keyValueDelimiter + f._2).mkString(pairDelimiter)
  }

  def extractStr(jvalue: JValue, fieldName: String, default: String = "") = {
    jvalue \ fieldName match {
      case JString(s) => if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString
      case JDouble(s) => s.toString
      case _ => default
    }
  }

  def getValueForKey(key: String, record: String) = {
    val startIndex = record.indexOf("\"" + key + "\" : ") + 5 + key.length
    val commaIndex = record.indexOf(",", startIndex)
    val antiBracketIndex = record.indexOf("}", startIndex)
    val endIndex = if (commaIndex != -1 && antiBracketIndex != -1) Math.min(record.indexOf(",", startIndex),
      record.indexOf("}", startIndex))
    else if (commaIndex != -1) commaIndex else antiBracketIndex
    val mendedStartInedex =
      if (record.charAt(startIndex) == '\"' || record.charAt(startIndex) == '[')
        startIndex + 1
      else
        startIndex
    val mendedEndInedex =
      if (record.charAt(endIndex - 1) == '\"' || record.charAt(endIndex - 1) == ']')
        endIndex - 1
      else
        endIndex

    if (mendedStartInedex > mendedEndInedex)
      ""
    else
      record.substring(mendedStartInedex, mendedEndInedex)
  }

}
