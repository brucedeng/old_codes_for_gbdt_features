package com.cmcm.cmnews.model.input

import com.cmcm.cmnews.model.spark.SparkBatchContext
import com.cmcm.cmnews.model.util.Parameters

/**
  * Created by lilonghua on 2016/11/14.
  */
trait FTRLDataFilterDataInput extends EventBatchInput {
  this: SparkBatchContext =>
  import Parameters._

  override def getInput: Unit ={
    logInfo("start FTRLDataFilterDataInput...")
    setRDD(constant_user_explore_input, constant_user_explore_rdd, this)
    setRDD(constant_lr_train_path, constant_lr_train_rdd, this)
  }
}
