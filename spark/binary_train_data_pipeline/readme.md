#1.代码地址
##1.1天级代码
`http://git.liebaopay.com/cmnews/india_news_ranking/tree/master/spark/binary_train_data_pipeline`
##1.2streaming版代码
`http://git.liebaopay.com/cmnews/india_news_ranking/tree/master/spark/binary_train_data_streaming_pipeline`
##1.3up过滤代码（oozie）
	http://git.liebaopay.com/cmnews/india_news_ranking/tree/master/news_offline/gen_train_data/phase2_spark_dnn/filter_up_data_2hour_cm_us
##1.4天级训练数据生成代码（oozie）
	http://git.liebaopay.com/cmnews/india_news_ranking/tree/master/news_offline/gen_train_data/phase2_spark_dnn/gen_training_data_day_cm_us
##1.5十分钟级别训练数据生成（oozie）
	http://git.liebaopay.com/cmnews/india_news_ranking/tree/master/news_offline/gen_train_data/phase2_spark_dnn/gen_training_data_10min

	
#2.代码说明
##2.1 oozie相关的代码主要是修改
	coordinator.properties配置文件里面的app_name，可能还需要修改队列名。
	
	http://git.liebaopay.com/cmnews/india_news_ranking/blob/master/news_offline/gen_train_data/phase2_spark_dnn/gen_training_data_day_cm_us/lib/cfb_features_dnn.conf
	
	修改特征的文件
	
##2.2 streaming版本的代码修改的地方
	http://git.liebaopay.com/cmnews/india_news_ranking/blob/master/spark/binary_train_data_streaming_pipeline/conf/us/us_cm_v4_lr/conf/event.conf
	
	读取Kafka配置 offset = "/news_dl/us_cm_v4_lr/offset" （需要修改）
	
	http://git.liebaopay.com/cmnews/india_news_ranking/blob/master/spark/binary_train_data_streaming_pipeline/conf/us/us_cm_v4_lr/conf/feature.conf
	
	加特征需要修改的文件 coec-feat （主要是修改这个里面，加单边特征需要改代码）
	
	http://git.liebaopay.com/cmnews/india_news_ranking/blob/master/spark/binary_train_data_streaming_pipeline/conf/us/us_cm_v4_lr/conf/model.conf
	
	写 Kafka需要修改output.kafka.topic，
	写文件需要修改output.file.prefix
	监控需要改driver和partition_postfix（名字不一样就行）
	output.dest = "kafka_N_file" （同时写Kafka和文件）
	output.dest = "kafka" （只写Kafka）
	output.dest = "file" （只写文件）