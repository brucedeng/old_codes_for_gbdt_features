#!/usr/bin/env bash

set -x
export LD_LIBRARY_PATH=$LDLIBRARY_PATH:/usr/lib/hadoop/lib/native

feature_conf=./conf/features.conf
monitor_conf=./conf/monitor.conf
libs=./lib
#debug_path="/tmp/dl/lilonghua"
debug_path=""
stat_type=all
lr_train_feature_path="/projects/news/deeplearning/model/training/gen_training_data_streaming/us_cm/training_data/20161027/05/30"
lr_server_feature_path="s3://com.cmcm.instanews.usw2.prod/model/feature_log_v2/en_us/20161027/05/{25,30,35}/"
lr_feature_consist_path="/projects/news/deeplearning/lilonghua/lr_feature_consist_path_201610270530_$stat_type"

hdfs dfs -rmr $lr_feature_consist_path

spark-submit \
--class com.cmcm.cmnews.model.app.FTRLFeatureConsistApp \
--master yarn-cluster \
--driver-memory 1g \
--executor-memory 3g \
--executor-cores 4 \
--queue deeplearning \
--num-executors 25 \
--conf spark.default.parallelism=100 \
--conf spark.app.name=FTRLFeatureConsistApp_lilonghua \
--files $feature_conf \
--files $monitor_conf \
$libs/binary_train_data_pipeline-1.0-SNAPSHOT.jar \
parallelism=100 debug=true  debug_path=${debug_path} lr_train_feature_path=${lr_train_feature_path} lr_server_feature_path=${lr_server_feature_path} lr_feature_consist_path=${lr_feature_consist_path} event_pid=1,21 feature_stat_type=$stat_type feature_model_type=lr predictor_id=en_us_exp9