#!/usr/bin/env bash
set -x
cate_coverage_output=/projects/news/deeplearning/zhangruichang/cate_coverage_output
hdfs dfs -rm -r $cate_coverage_output
export LD_LIBRARY_PATH=$LDLIBRARY_PATH:/usr/lib/hadoop/lib/native
jar_file=binary_train_data_pipeline-1.0-SNAPSHOT.jar
spark-submit \
--class com.cmcm.cmnews.model.app.UpHitrateApp \
--master yarn \
--deploy-mode cluster \
--conf spark.yarn.executor.memoryOverhead=1024 \
--conf spark.rdd.compress=true \
--conf spark.rpc.askTimeout=300 \
--conf spark.task.maxFailures=50 \
--driver-memory 5g \
--executor-memory 5g \
--executor-cores 4 \
--queue experiment \
--num-executors 30 \
--conf spark.app.name=categoryCoverageCalculate \
$jar_file \
parallelism=128 \
event_pv_input=/projects/news/data/raw_data/impression/20161220 \
user_input=/projects/news/user_profile/contents_user_profile/up_v4_json_hourly/20161221/22/*00000* \
cate_coverage_output=$cate_coverage_output \



