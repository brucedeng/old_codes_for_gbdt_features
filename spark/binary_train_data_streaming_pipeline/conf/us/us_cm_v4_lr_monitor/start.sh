#!/usr/bin/env bash
#export SPARK_CONF_DIR=./spark_conf

export LD_LIBRARY_PATH=$LDLIBRARY_PATH:/usr/lib/hadoop/lib/native

hadoop fs -rm -r /projects/news/deeplearning/model/training/streaming_monitor/us_cm_zrc/checkpoint

currentDir=$(cd `dirname $0`; pwd)

#echo $$ > ${currentDir}/run/lr.pid

spark-submit \
--conf spark.task.maxFailures=100 \
--conf spark.yarn.max.executor.failures=200 \
--driver-java-options "-Dlog4j.configuration=file:${currentDir}/conf/log4j.properties" \
--class com.cmcm.news.app.LRMonitorApp \
--master yarn-client \
--driver-memory 1g \
--executor-memory 1g \
--executor-cores 2 \
--num-executors 10 \
--queue experiment \
${currentDir}/lib/binary_train_data_streaming_pipeline-1.0-SNAPSHOT.jar \
-c ${currentDir}/conf/ 1>${currentDir}/logs/stdout 2>${currentDir}/logs/stderr $@ &

echo $! > ${currentDir}/run/lr.pid