#!/usr/bin/env bash

currentDir=$(cd `dirname $0`; pwd)

if [ ! -f "${currentDir}/run/lr.pid" ]
then
    echo "${currentDir}/run/lr.pid file is not existed"
else
    gid=`cat ${currentDir}/run/lr.pid`
    echo "the group id is $gid, whill kill all processes in this group!"

    ps -ef | grep "$currentDir/" | awk '{print $2}'
    ps -ef | grep "$currentDir/" | awk '{print $2}' | xargs kill -9
    #kill $gid
    #kill -9 "$gid"
fi