#!/usr/bin/env bash
source_template=/projects/news/deeplearning/model/training/gen_training_data_streaming/us_cm_zrc_kafka/training_data/
dest_template=s3://com.cmcm.instanews.usw2.prod/deeplearning/zhangruichang/data/window_join/
date=`date -d "1 day ago" +%Y%m%d`
hdfs dfs -mkdir $dest_template/$date
hadoop distcp -Dmapred.job.queue.name=experiment $source_template/$date/ $dest_template/$date/