#!/usr/bin/env bash
#export SPARK_CONF_DIR=./spark_conf

export LD_LIBRARY_PATH=$LDLIBRARY_PATH:/usr/lib/hadoop/lib/native
#export LD_LIBRARY_PATH=$LDLIBRARY_PATH:/usr/lib/hadoop/lib/native:/usr/lib/hadoop/lib/native/Linux-amd64-64/
#export HADOOP_CONF_DIR=/etc/hadoop/conf
#export YARN_CONF_DIR=/usr/lib/spark/conf
#export SPARK_CLASSPATH=/usr/lib/hadoop/lib/*

currentDir=$(cd `dirname $0`; pwd)

#echo $$ > ${currentDir}/run/lr.pid

spark-submit \
--conf spark.task.maxFailures=100 \
--conf spark.yarn.max.executor.failures=200 \
--driver-java-options "-Dlog4j.configuration=file:${currentDir}/conf/log4j.properties" \
--class com.cmcm.news.app.LRDiffApp \
--master yarn-client \
--driver-memory 3g \
--executor-memory 2g \
--executor-cores 2 \
--num-executors 20 \
--queue deeplearning \
${currentDir}/lib/binary_train_data_streaming_pipeline-1.0-SNAPSHOT.jar \
-c ${currentDir}/conf/ 1>${currentDir}/logs/stdout 2>${currentDir}/logs/stderr $@ &

echo $! > ${currentDir}/run/lr.pid