package com.cmcm.news.output

import com.cmcm.news.util.KafkaSink
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.streaming.dstream.DStream
import org.joda.time.DateTime

/**
  * Created by lilonghua on 16/9/13.
  */

class LRStreamingFileOutput(val outputFilePrefix: String) extends StreamingOutput[((Long,String), (String, String, String, Double, String))] {

  /**
    *
    * @param dStream ((batchId,pid), (uid, cid, label, dwell, featureMap))
    * @param appendedKey
    * @return
    */
  override def output(dStream: DStream[((Long,String), (String, String, String, Double, String))], appendedKey: String = ""): Option[DStream[Int]] = {
    val splitKey = appendedKey.split("_")
    if (splitKey.size > 1 && splitKey(0).equals("multilan")) {
      multilanOutput(dStream, splitKey(1))
    } else {
      normalOutput(dStream, appendedKey)
    }
  }

  def multilanOutput(dStream: DStream[((Long,String), (String, String, String, Double, String))], appendedKey: String = ""): Option[DStream[Int]] = {
    val countries = appendedKey.split(",")
    countries.foreach(countryTag => {
      dStream.foreachRDD((rdd, time) => {
        val datePrefix = new DateTime(time.milliseconds).toString("yyyyMMddHHmm")
        rdd.map(f => generateOutputString(f._2._1 + "\t" + f._2._2 + "\t" + f._2._3 + ":" + f._2._4.toString + "\t" + f._2._5,f._1._2))
          .saveAsTextFile(outputFilePrefix + "/" + countryTag + "/" + datePrefix.substring(0, 8) + "/" + datePrefix.substring(8,10) + "/" + datePrefix.substring(10))
      })
    })
    None
  }

  def normalOutput(dStream: DStream[((Long,String), (String, String, String, Double, String))], appendedKey: String = ""): Option[DStream[Int]] ={
    dStream.foreachRDD((rdd, time) => {
      val datePrefix = new DateTime(time.milliseconds).toString("yyyyMMddHHmm")
      if (appendedKey.nonEmpty) {
        rdd.map(f => generateOutputString(f._2._1 + "\t" + f._2._2 + "\t" + f._2._3 + ":" + f._2._4.toString + "\t" + f._2._5,appendedKey))
            .coalesce(10)
          .saveAsTextFile(outputFilePrefix + "/" + appendedKey + "/" + datePrefix.substring(0, 8) + "/" + datePrefix.substring(8,10) + "/" + datePrefix.substring(10))
      } else {
        rdd.map(f => generateOutputString(f._2._1 + "\t" + f._2._2 + "\t" + f._2._3 + ":" + f._2._4.toString + "\t" + f._2._5,appendedKey))
            .coalesce(10)
          .saveAsTextFile(outputFilePrefix + "/" + datePrefix.substring(0, 8) + "/" + datePrefix.substring(8,10) + "/" + datePrefix.substring(10))
      }
    })
    None
  }

  /**
    *
    * @param record (feature, batchId, view, click, ?)
    * @param appendedKey a tag
    * @return (batchId \t featureKey \t featureValue \t click \t view)
    */
  def generateOutputString(record: String, appendedKey: String): Any = {
    val outputString = record

    if (appendedKey.nonEmpty)
      outputString + "\t" + appendedKey
    else
      outputString
  }

}


/**
  *
  * @param kafka (producer_name,broker,producerType,topic)
  */
class LRStreamingOutput(val kafka: Broadcast[KafkaSink], val topic: String ) extends StreamingOutput[((Long,String), (String, String, String, Double, String))] {
  override def output(dStream: DStream[((Long,String), (String, String, String, Double, String))], appendedKey: String = ""): Option[DStream[Int]] = {
    outputDStream(dStream, appendedKey)
  }

  def outputDStream(dStream: DStream[((Long,String), (String, String, String, Double, String))],
                    appendedKey: String = ""): Option[DStream[Int]] = {
    dStream.foreachRDD(rdd => {
      rdd.foreach(f => {
        kafka.value.send(topic,f._2._1 + "\t" + f._2._2 + "\t" + f._2._3 + ":" + f._2._4.toString + "\t" + f._2._5)
      })
    })
    None
  }
}

class LRStreamingMixedOutput(val outputs: List[StreamingOutput[((Long,String), (String, String, String, Double, String))]]) extends StreamingOutput[((Long,String), (String, String, String, Double, String))] {
  override def output(dStream: DStream[((Long,String), (String, String, String, Double, String))], appendedKey: String = ""): Option[DStream[Int]] = {
    outputs.foreach(output => {
      output.output(dStream, appendedKey)
    })
    None
  }
}