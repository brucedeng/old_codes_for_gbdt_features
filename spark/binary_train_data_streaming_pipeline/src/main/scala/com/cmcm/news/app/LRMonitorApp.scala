package com.cmcm.news.app

import com.cmcm.news.config.Configurable
import com.cmcm.news.model.LRModelMonitor
import com.cmcm.news.spark.SparkComponent

/**
  * Created by lilonghua on 16/9/21.
  */
object LRMonitorApp extends DLMonitorApp
  with Configurable
  with SparkComponent
  with LRModelMonitor
