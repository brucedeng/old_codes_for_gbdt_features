package com.cmcm.news.util

import org.json4s.{DefaultFormats, Extraction}
import org.json4s.JsonAST.{JNothing, JNull, JValue}
import org.json4s.jackson.JsonMethods._
import org.json4s.jackson.Serialization

import scala.util.Try

/**
  * Created by lilonghua on 16/9/2.
  */
object JsonUtil {

  implicit val formats = DefaultFormats

  def toJson(objectToWrite: AnyRef): String = Serialization.write(objectToWrite)

  def fromJsonOption[T](jsonString: String)(implicit mf: Manifest[T]): Option[T] = Try(Serialization.read[T](jsonString)).toOption

  def jsonStrToMap(jsonStr: String): Map[String, Any] = parse(jsonStr).extract[Map[String, Any]]

  def toMap(obj: AnyRef): Map[String, Any] = jsonStrToMap(toJson(obj))

  def encodeJson(obj: AnyRef): JValue = Extraction.decompose(obj)

  def decodeJson[T](obj: JValue)(implicit mf: Manifest[T]): Option[T] = Try(Extraction.extract[T](obj)).toOption

  def convertToJValue(jsonStr: String) = parse(jsonStr)

  def convertToJString(jValue: JValue) = compact(jValue)

  def extractValue[T](jsonStr: String, key: String)(implicit mf: Manifest[T]): Option[T] = extractValue[T](parse(jsonStr), key)

  def extractValue[T](json: JValue, key: String)(implicit mf: Manifest[T]): Option[T] = {
    val value = json \ key
    value match {
      case JNothing => None
      case JNull => None
      case _ => value.extractOpt[T]
    }
  }

  def extractValue[T](json: JValue, key: String, default: T)(implicit mf: Manifest[T]): Option[T] = {
    val value = json \ key
    value match {
      case JNothing => Option(default)
      case JNull => Option(default)
      case _ => value.extractOpt[T]
    }
  }


  def main(args:Array[String]): Unit ={
    val featureDict = collection.mutable.Map[String,Any]()
    featureDict.put("D_CONID",List("0.0","1.0","0.01"))
    val categoryDict = collection.mutable.Map[String,Any]()
    categoryDict.put("cos_nomerge_14705823|military/chinese", List("0.1","1.2","0.03"))
    categoryDict.put("cos_nomerge_14705823|politics/government", List("0.5","1.7","0.3"))
    featureDict.put("C_D_U_GROUPID_CATEGORY", categoryDict)
    println(JsonUtil.toJson(featureDict))

  }
}
