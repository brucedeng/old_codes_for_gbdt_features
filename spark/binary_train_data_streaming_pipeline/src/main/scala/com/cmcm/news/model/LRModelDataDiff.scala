package com.cmcm.news.model

import com.cmcm.news.component.LRMonitorModelComponent
import com.cmcm.news.config.Configurable
import com.cmcm.news.spark.SparkComponent

/**
  * Created by lilonghua on 2016/10/19.
  */
trait LRModelDataDiff extends LRMonitorModelComponent {
  this: Configurable with SparkComponent =>

  override def workflowSetup() = {

    val sData = getInputDstream

    val featSplit = modelConfigUnit.getString("model.lr.feat.split", " ")

    sData.foreachRDD(rdd => {
      rdd.filter(f => {
        f.endsWith("\t")
      }).collect()
        .foreach(f => println(f))
    })

    sData.foreachRDD(rdd => {
      rdd.filter(f => {
        val feat = f.split("\t")
        feat.length == 4 && feat.last.split(featSplit).exists(t => t.startsWith(" "))
      }).collect()
        .foreach(f => println(f))
    })
  }
}