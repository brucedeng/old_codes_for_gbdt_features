package com.cmcm.news.analysis

import com.cmcm.news.component.NormalComponent
import com.cmcm.news.config.Configurable
import com.cmcm.news.spark.SparkNormal

/**
  * Created by lilonghua on 16/9/28.
  */
trait FTRLFeatureDistribute extends NormalComponent {
  this: Configurable with SparkNormal =>

  override def workflowSetup(): Unit = {

    sparkContext.textFile(modelConfigUnit.getString("model.lr.input", NormalComponent.defaultTenFeatPath))
      .flatMap(feat => {
        feat.split("\t")(3).split(" ").distinct
      }).distinct()
      .map(f => {
        val t = f.split("_", 2)
        (t(0), 1)
      }).reduceByKey((f1, f2) => {
        f1 + f2
      }).map(f => {
        f._1 + "\t" + f._2
      }).coalesce(modelConfigUnit.getInt("model.lr.part", 2)).saveAsTextFile(modelConfigUnit.getString("model.lr.output",NormalComponent.defaultModelOutputFilePrefix))
  }
}
