package com.cmcm.news.util

import java.io.IOException
import java.net.URI

import com.cmcm.news.component.ModelComponent
import com.cmcm.news.feature.{CmnewsNewsProfile, EventFeature, ProxyService}
import com.cmcm.news.monitor.MonitorMetrics
import com.google.protobuf.ByteString
import org.apache.http.HttpEntity
import org.apache.http.client.ClientProtocolException
import org.apache.http.client.methods.{HttpGet, HttpPost}
import org.apache.http.entity.ByteArrayEntity
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.spark.Logging
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.Time

import collection.JavaConversions._
import scala.collection.mutable.ListBuffer
import scala.util.Try

/**
  * Created by lilonghua on 16/9/1.
  */
object Utils extends Logging {

  val fieldDelimiter = "\001"
  val pairDelimiter = "\002"
  val keyValueDelimiter = "\003"
  val pairGap = "\004"
  val keyGap = "\005"

  val defaultNu = -10000000
  val defaultStr = ""

  /**
    * Deduplication by same key (uid,cid,rid,pid)
    *
    * @param l (eventFeature, batchId, act, value, sparkStreamingTime, label)
    * @param r (eventFeature, batchId, act, value, sparkStreamingTime, label)
    * @return (eventFeature, batchId, act, value, sparkSteamingTime, label)
    */
  def labelFun(l: (String, Long, String, Double, Long, Int), r: (String, Long, String, Double, Long, Int)): (String, Long, String, Double, Long, Int) = {
    val fR = EventFeature(r._1)
    val fL = EventFeature(l._1)
    val stR = Try(fR.getFeatureValue("SERVER_TIME").getOrElse("0").toLong).getOrElse(0l)
    val stL = Try(fL.getFeatureValue("SERVER_TIME").getOrElse("0").toLong).getOrElse(0l)
    val gmpL = fL.getFeatureValue("D_GMP_SCORE").getOrElse("0").toDouble
    val gmpR = fR.getFeatureValue("D_GMP_SCORE").getOrElse("0").toDouble
    //if ( (l._3 == r._3 && ((l._3 == "4" && (l._4 > r._4 || gmpL > gmpR)) || (l._3 != "4" && (stR > stL || gmpL > gmpR))))
    //if ( (l._3 == r._3  && (r._5 > l._5 || (r._5 == l._5 && stR > stL)))
    if ( (l._3 == r._3 && ((l._3 == "4" && l._4 > r._4) || (l._3 != "4" && stR > stL)))
          || l._3 > r._3) {
      (l._1, l._2, l._3, l._4, math.min(l._5, r._5), math.max(l._6, r._6))
    } else {
      (r._1, r._2, r._3, r._4, math.min(l._5, r._5), math.max(l._6, r._6))
    }
  }

  def labelTag(record: ((String, String, String, String), (String, Long, String, Double, Long, Int)), dw: Double, ration: Double): ((String, String, String, Long), (String, Double)) = {
    val k = (record._1._1, record._1._2, record._2._1, record._2._2)

    val label = if (record._2._6 == 0) "0" else "1"

    val dwell = if (record._2._3 == "2") Math.log(dw + 1) else record._2._4

    (k, (label,if (label == "1") if (dwell.isNaN || dwell <= 0.0) Math.log(dw + 1) else dwell * ration else dwell))
  }

  def cfbParsedMetrics(rdd: RDD[((String, String, String, Long, Long), (String, Double))], time: Time, window: Int): Unit = {
    val parsedCount = rdd.count()
    rdd.map(event => (event._2._1, 1))
      .reduceByKey(_ + _)
      .collect()
      .foreach(t => {
        ModelComponent.monitor.recordFromDriver(MonitorMetrics.CFB_EVENT_COUNT + "_" + t._1, t._2.toString)
      })
    ModelComponent.monitor.recordFromDriver(MonitorMetrics.CFB_PARSED_COUNT, parsedCount.toString)

    val batchId = System.currentTimeMillis() / 1000 / window

    val partitionedMetrics = rdd.mapPartitions(partition => {
      var min = Long.MaxValue
      var max = Long.MinValue
      var sum = 0L
      var count = 0L
      partition.foreach(record => {
        count += 1L
        val delta = batchId - record._1._4
        min = Math.min(min, delta)
        max = Math.max(max, delta)
        sum += delta
      })
      var array = new ListBuffer[(Long, Long, Long, Long)]()
      array.+=((min, max, sum, count))
      array.toIterator
    })
    val metricsResult = partitionedMetrics.reduce((l, r) => {
      (Math.min(l._1, r._1), Math.max(l._2, r._2), l._3 + r._3, l._4 + r._4)
    })
    val avgLatency = Try(metricsResult._3 * 1.0 / metricsResult._4).getOrElse(-1L)
    ModelComponent.monitor.recordFromDriver(MonitorMetrics.CFB_AGGRE_MIN_LATENCY, metricsResult._1.toString)
    ModelComponent.monitor.recordFromDriver(MonitorMetrics.CFB_AGGRE_MAX_LATENCY, metricsResult._2.toString)
    ModelComponent.monitor.recordFromDriver(MonitorMetrics.CFB_AGGRE_AVG_LATENCY, avgLatency.toString)
  }

  /**
    *
    * @param rdd ((batchId,pid), (uid, cid, label, dwell, featureMap))
    */
  def outputModelMetrics(rdd: RDD[((Long,String), (String, String, String, Double, String))]): Unit = {
    val cfbTotalCount: Long = rdd.count()
    ModelComponent.monitor.recordFromDriver(MonitorMetrics.CFB_OUTPUT_TOTAL_COUNT, cfbTotalCount.toString)
    rdd.map(record => {
      (record._2._3, 1)
    }).reduceByKey((l, r) => {
      l + r
    }).collect()
      .foreach(tuple => {
        ModelComponent.monitor.recordFromDriver((tuple._1 + "_LABEL_COUNT").toLowerCase, tuple._2.toString)
      })
  }

  def outputFeatureCPnUPMetrics(rdd: RDD[((String, String), (Long, String, Double, String, String, Option[String]))],
                                        cfbAggregationCount: Long): Unit = {
    val upCount = rdd.filter(_._2._6.getOrElse("").nonEmpty).count()
    ModelComponent.monitor.recordFromDriver(MonitorMetrics.CFB_UP_COUNT, upCount.toString)
    val upJoinRate: Float = if (cfbAggregationCount != 0) upCount.toFloat / cfbAggregationCount else 0.0F
    ModelComponent.monitor.recordFromDriver(MonitorMetrics.CFB_UP_RATE, upJoinRate.toString)
  }

  def outputFeatureCPMetrics(rdd: RDD[(String, (String, Long, String, Double, String, String))],
                                     cfbAggregationCount:Long): Unit ={
    val cpCount = rdd.filter(_._2._6.nonEmpty).count()
    ModelComponent.monitor.recordFromDriver(MonitorMetrics.CFB_CP_COUNT, cpCount.toString)
    val cpJoinRate: Float = if (cfbAggregationCount != 0) cpCount.toFloat / cfbAggregationCount else 0.0F
    ModelComponent.monitor.recordFromDriver(MonitorMetrics.CFB_CP_RATE, cpJoinRate.toString)
  }

  def httpGetEntity(url: URI): Option[HttpEntity] = {
    val client = new DefaultHttpClient()
    val request = new HttpGet(url)
    // add request header
    //request.addHeader("User-Agent", USER_AGENT)

    try {
      val response = client.execute(request)
      val entity = response.getEntity
      Option[HttpEntity](entity)
    } catch {
      case e: ClientProtocolException =>
        logError(s"get up \t$url\t error",e)
        None
      case e: IOException =>
        logError(s"get up \t$url\t error",e)
        None
    }
  }

  def getContentFeature(url: String, cids: List[String]): Map[String, CmnewsNewsProfile.NewsProfile] = {
    val client = new DefaultHttpClient()
    val request = new HttpPost(url)

    request.addHeader("Content-Type", "application/octet-stream")
    val proxyBuilder = ProxyService.ProxyRequest.newBuilder()
    proxyBuilder.setAct("mget")

    cids.foreach(t => {
      proxyBuilder.addKeys(ByteString.copyFromUtf8("n_"+t))
    })
    request.setEntity(new ByteArrayEntity(proxyBuilder.build().toByteArray))
    val response = client.execute(request)

    if (response.getStatusLine.getStatusCode == 200) {
      val responseProxy = ProxyService.ProxyResponse.parseFrom(response.getEntity.getContent)
      val cList = responseProxy.getValuesList
      logError(s"cluster response content size is ${cList.length}")
      response.getEntity.getContent.close()
      cList.map(f => {
        val c = getContent(f.toByteArray)
        (Try(c.getContentId).getOrElse(""), c)
      }).toMap
    } else {
      logError(s"cluster response status code is ${response.getStatusLine.getStatusCode}")
      response.getEntity.getContent.close()
      Map.empty[String, CmnewsNewsProfile.NewsProfile]
    }
  }

  def getContent(cf: Array[Byte]): CmnewsNewsProfile.NewsProfile = {
    Try(if (cf.isEmpty || cf.length < 10) {
        logError(s"cf is empty of size < 10")
        null
      } else {
        CmnewsNewsProfile.NewsProfile.parseFrom(cf)
      }).getOrElse(null)
  }

  def main(args: Array[String]) {
    println("\001")
    println(if (Math.log(1).isNaN) if (true) "-3" else "-2" else "-1")
  }

}
