package com.cmcm.news.parse

import java.text.{DateFormat, SimpleDateFormat}
import java.util.TimeZone

import com.cmcm.news.config.ConfigUnit
import com.cmcm.news.feature.EventFeature
import com.cmcm.news.util.JsonUtil
import org.apache.spark.{Logging, SparkContext}
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.RDD
import org.json4s.JsonAST.{JDouble, JInt, JString}

import scala.util.{Failure, Success, Try}

/**
  * Created by lilonghua on 16/8/24.
  */
object Event extends Logging with Serializable {

  val defaultProductIds = "3"
  val defaultSources = "listpage,newscard"
  val defaultApiV = "2,3"

  var productIds: Set[String] = _
  var productIdsBroadcastVar: Broadcast[Set[String]] = _
  var sources: Set[String] = _
  var sourcesBroadcastVar: Broadcast[Set[String]] = _
  var apiv: Set[String] = _
  var apivBroadcastVar: Broadcast[Set[String]] = _
  val actMapping:Map[String,(String,String)] = Map("1" -> ("PV","impression"),
    "2" -> ("CL","click"),
    "3" -> ("LD","listpagedwelltime"),
    "4" -> ("DW","readtime"),
    "5" -> ("CP","completeness"),
    "6" -> ("PR","praise"),
    "7" -> ("DL","dislike"),
    "8" -> ("CM","comment"),
    "9" -> ("SH","share"),
    "10" -> ("LI","listpageimpression"),
    "11" -> ("TR","tread"),
    "12" -> ("AF","adsfill"),
    "13" -> ("RT","rsstimeout"),
    "14" -> ("SR","search"),
    "15" -> ("PA","pushacceptance"),
    "16" -> ("MD","mood"),
    "17" -> ("CC","channelchange"),
    "18" -> ("CN","channelnodata"),
    "101" -> ("PS","push")
  )
  var actMappingBroadcastVar:Broadcast[Map[String,(String,String)]] = _

  def initEventParser(eventConfigUnit: ConfigUnit) = {
    productIds = eventConfigUnit.getString("event.productids", defaultProductIds).trim.split(",").toSet
    sources = eventConfigUnit.getString("event.sources", defaultSources).trim.split(",").toSet
    apiv = eventConfigUnit.getString("event.apiv", defaultApiV).trim.split(",").toSet
  }

  def generateProductIdsBroadcastVar(sparkContext: SparkContext) = {
    logInfo("Init productIds broardcast value.")
    productIdsBroadcastVar = sparkContext.broadcast(productIds)
  }

  def generateSourcesBroadcastVar(sparkContext: SparkContext) = {
    logInfo("Init Sources broardcast value.")
    sourcesBroadcastVar = sparkContext.broadcast(sources)
  }

  def generateApiVBroadcastVar(sparkContext: SparkContext) = {
    logInfo("Init ApiV broardcast value.")
    apivBroadcastVar = sparkContext.broadcast(apiv)
  }

  def generateActMappingBroadcastVar(sparkContext: SparkContext) = {
    logInfo("Init ActMapping value.")
    actMappingBroadcastVar = sparkContext.broadcast(actMapping)
  }

  def parse(sc: SparkContext, impressionFile: String, clickFile: String): RDD[String] = {
    val impressionRDD: RDD[String] = sc.textFile(impressionFile)
    val clickRDD: RDD[String] = sc.textFile(clickFile)
    impressionRDD.union(clickRDD)
  }


  def getBatchId(date: String, window: Int): Long = {
    val sdf: DateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"))
    (sdf.parse(date).getTime / (1000 * window)) + 1
  }

  def getListForKey(key: String, record: String): Array[String] = {
    val keyIndex = record.indexOf("\"" + key + "\":[")
    if (keyIndex < 0) {
      //      logWarning(s"Error when geting $key from $record")
      Array[String]()
    } else {
      val start = keyIndex + 4 + key.length
      val end = record.indexOf("]", start)
      if (end < 0 || (start > end)) {
        //        logWarning(s"Error when geting $key from $record")
        Array[String]()
      } else {
        val listString = record.substring(start, end)
        listString.split(",").map(field => {
          val fieldSize = field.length
          val startIndex = if (field.startsWith("\"")) 1 else 0
          val endIndex = if (field.endsWith("\"")) fieldSize - 1 else fieldSize
          field.substring(startIndex, endIndex)
        }).filter(_.length > 0)
      }
    }
  }

  def getValueForKey(key: String, record: String) = {
    val keyIndex = record.indexOf("\"" + key + "\":")
    if (keyIndex < 0) {
      //      logWarning(s"Error when geting $key from $record")
      ""
    } else {
      val startIndex = keyIndex + 3 + key.length
      val commaIndex = record.indexOf(",", startIndex)
      val antiBracketIndex = record.indexOf("}", startIndex)
      val endIndex =
        if (commaIndex != -1 && antiBracketIndex != -1)
          Math.min(record.indexOf(",", startIndex), record.indexOf("}", startIndex))
        else if (commaIndex != -1)
          commaIndex
        else
          antiBracketIndex

      if (endIndex < 0) {
        //        logWarning(s"Error when geting $key from $record")
        ""
      } else {
        val mendedStartInedex =
          if (record.charAt(startIndex) == '\"')
            startIndex + 1
          else
            startIndex
        val mendedEndInedex =
          if (record.charAt(endIndex - 1) == '\"')
            endIndex - 1
          else
            endIndex

        if (mendedStartInedex > mendedEndInedex) {
          //          logWarning(s"Error when geting $key from $record")
          ""
        } else
          record.substring(mendedStartInedex, mendedEndInedex)
      }
    }
  }

  def parseEventCN(productIds: Set[String], sources: Set[String], apivs: Set[String], window: Int)(record: String): List[((String, String, String, Long), (Int, Int))] = {
    parseRecordCN(productIds, sources, apivs, window, record)
  }

  def parseRecordCN(productIds: Set[String], sources: Set[String], apivs: Set[String], window: Int, record: String):
  List[((String, String, String, Long), (Int, Int))] = {
    val apiv = if (getValueForKey("apiv", record).equals("1")) "2" else "3"
    val uid = if (apiv.equals("2")) getValueForKey("uuid", record) else getValueForKey("aid", record)
    val productid = getValueForKey("pid", record)
    val pf = getValueForKey("pf", record)
    val contentid = getValueForKey("contentid", record)
    val servertime_sec = getValueForKey("servertime_sec", record)
    val source = getValueForKey("level1", record)
    val act = getValueForKey("act", record)
    val ctype = getValueForKey("ctype", record)
    val isNewUser = getValueForKey("new_user", record)
    val city = getValueForKey("city", record)

    // filter record
    if (uid.length > 0 &&
      (act.equals("1") || act.equals("2")) &&
      //      contentid.length>0 &&
      (sources.contains(source) || sources.contains("NULL")) &&
      productIds.contains(productid) &&
      pf != "web" &&
      apivs.contains(apiv)) {

      val eventFeature = EventFeature()
      eventFeature.addFeature("C_TYPE", ctype)
      eventFeature.addFeature("NEW_USER", isNewUser)
      eventFeature.addFeature("CHANNEL", getValueForKey("level2", record))
      val cpackDes = getValueForKey("des", record)
      cpackDes.split("\\|").foreach(field => {
        val keyValue = field.split("=")
        if (keyValue.length == 2 && keyValue(0) == "t") eventFeature.addFeature("CPACK_TYPE", keyValue(1))
      })
      eventFeature.addFeature("U_CITY", city)
      eventFeature.addFeature("PID", getValueForKey("pid", record))

      val clickShowTuple = if (act.equals("1")) (1, 0) else (0, 1)
      val batchId = servertime_sec.toLong / window + 1
      ((uid, contentid, eventFeature.genFeatureString(), batchId), clickShowTuple) :: Nil
    } else {
      Nil
    }
  }

  def parseEventCN(productIdsBroadcastVar: Broadcast[Set[String]],
                   sourcesBroadcastVar: Broadcast[Set[String]],
                   apivBroadcastVar: Broadcast[Set[String]],
                   window: Int)
                  (record: String): List[((String, String, String, Long), (Int, Int))] = {
    val productIds = productIdsBroadcastVar.value
    val sources = sourcesBroadcastVar.value
    val apivs = apivBroadcastVar.value
    parseEventCN(productIds, sources, apivs, window)(record)
  }

  def getEventPaser(window: Int = 600) = {
    parseEventCN(productIdsBroadcastVar, sourcesBroadcastVar, apivBroadcastVar, window) _
  }

  def getEventParserForWorldwide(region: List[String],
                                 actFilter:List[String] = List("1","2","4"),
                                 window: Int = 600,
                                 mDwell: Int = 600) = {
    parseEventForWorldwide(region,actFilter, actMappingBroadcastVar.value, window, mDwell) _
  }

  def getEventParserForGMP(region: List[String], useDwell:Boolean = false, window: Int = 600) = {
    parseEventForGMP(region, useDwell, window) _
  }

  /**
    *
    * @param countries country setting
    * @param window default 600
    * @param record sparkStreamTime + '\t' + event
    * @return (aid, contentid, eventFeature, batchId), (act,value))
    */
  def parseEventForWorldwide(countries: List[String],
                             actFilter:List[String],
                             actMapping:Map[String,(String,String)],
                             window: Int = 600,
                             mDwell: Int = 600)
                            (record: String): List[((String, String, String, Long, Long), (String, Double))] = {
    if (isEventValid(record, countries, actFilter)) {
      val eventFeature = EventFeature()
      setRegularFeature(record, eventFeature)
      val aid = getValueForKey("aid", record)
      val act = if(getValueForKey("level1_type", record).equals("3")) "101" else getValueForKey("act", record)
      val contentid = getValueForKey("contentid", record)
      val servertime_sec = eventFeature.getFeatureValue("SERVER_TIME").getOrElse(eventFeature.getFeatureValue("EVENT_TIME").getOrElse("0"))
      val batchId = servertime_sec.toLong / window + 1
      val timeId = record.split('\t')(0)
      val t1 = (aid, contentid, eventFeature.genFeatureString(), batchId, timeId.toLong)

      act match {
        case "4" =>
          val dwellTime = Try(getValueForKey("dwelltime", record).toInt).getOrElse(0)
          val dwell = if (dwellTime > mDwell) Math.log(mDwell + 1) else Math.log(dwellTime + 1)
          //(t1, (actMapping.get(act).get._1, dwell)) :: Nil //Dwell
          (t1, (act, dwell)) :: Nil //Dwell
        case "5" =>
          val completeness: Double = 1.0 * Try(getValueForKey("completeness", record).toInt).getOrElse(0) / 100
          //(t1, (actMapping.get(act).get._1, completeness)) :: Nil //Dwell
          (t1, (act, completeness)) :: Nil //Dwell
        case _ =>
          //(t1, (actMapping.get(act).get._1, 1.0)) :: Nil   //view
          (t1, (act, 1.0)) :: Nil   //view
      }
    } else {
      Nil
    }
  }

  /**
    *
    * @param countries country setting
    * @param window default 600
    * @param record event
    * @return (aid, contentid, eventFeature, batchId), (act,value))
    */
  def parseEventForGMP(countries: List[String],
                       useDwell:Boolean,
                       window: Int = 600)
                      (record: String): List[((String, String, String, Long), (Double, Double))] = {
    if (isEventValid(record, countries,if (useDwell) List("1","4") else List("1","2"))) {
      val eventFeature = EventFeature()
      setRegularFeature(record, eventFeature)

      val act = getValueForKey("act", record) // pv=1, click=2, dwell=4, dislike=7
      val aid = getValueForKey("aid", record)
      val contentid = getValueForKey("contentid", record)
      //val servertime_sec = getValueForKey("servertime_sec", record)
      val batchId = eventFeature.getFeatureValue("SERVER_TIME").getOrElse(eventFeature.getFeatureValue("EVENT_TIME").getOrElse("0")).toLong / window + 1

      act match {
        case "1" =>
          ((aid, contentid, eventFeature.genFeatureString(), batchId), (1.0, 0.0)) :: Nil  //View
        case "2" =>
          ((aid, contentid, eventFeature.genFeatureString(), batchId), (0.0, 1.0)) :: Nil //CLick
        case "4" =>
          val dwellTime = Try(getValueForKey("dwelltime", record).toInt).getOrElse(0)
          val dwell = if (dwellTime > 600) Math.log(601) else Math.log(dwellTime + 1)
          ((aid, contentid, eventFeature.genFeatureString(), batchId), (0.0, dwell)) :: Nil //Dwell
      }
    } else {
      Nil
    }
  }

  def setRegularFeature(recordWithTime:String, eventFeature: EventFeature): Unit ={
    val record = recordWithTime.split('\t')(1)
    val eventtime = getValueForKey("eventtime", record)
    val city = getValueForKey("cur_city", record)
    val country = getValueForKey("country", record)
    val app_lan = getValueForKey("app_lan", record)
    val ctype = getValueForKey("ctype", record)
    val pid = getValueForKey("pid", record)
    val position = getValueForKey("level1", record)
    val position_type = getValueForKey("level1_type", record)
    val isNewUser = getValueForKey("new_user", record)
    val sTime = getValueForKey("stime", record)
    val requestTime = getValueForKey("requesttime", record)

    val des = getValueForKey("des", record)
    val psTime = parseDes(des, "ps_time")
    val rid = parseDes(des, "rid")
    val srid = parseDes(des, "srid")
    val src = parseDes(des, "src")

    val servertime_sec = getValueForKey("servertime_sec", record)
    val net = getValueForKey("net", record)
    val osVersion = getValueForKey("osv", record)
    val appVersion = getValueForKey("appv", record)
    val appIv = getValueForKey("apiv", record)
    val platForm = getValueForKey("pf", record)
    val brand = getValueForKey("brand", record)
    val model = getValueForKey("model", record)
    val channel = getValueForKey("ch", record)
    val isHot = getValueForKey("ishot", record)
    val query_pkg = json3Parse(record, "upack", "user_info", "query_pkgs")
    val gmp_score = json3Parse(record, "cpack", "ext", "gmp_score")
    val gmp_ver = json3Parse(record, "cpack", "ext", "gmp_ver")

    eventFeature.addFeature("EVENT_TIME", eventtime)
    eventFeature.addFeature("U_CITY", city)
    eventFeature.addFeature("U_COUNTRY",
      if ("PY,PA,HN,AR,SV,VE,CU,DO,PE,CO,CL,CR,EC,NI,BO,GT,UY".contains(country)) "LM" else country)
    eventFeature.addFeature("NEW_USER", isNewUser)
    eventFeature.addFeature("C_TYPE", if (ctype.length > 0) ctype else "1")
    eventFeature.addFeature("U_LAN", app_lan)
    eventFeature.addFeature("PID", pid)
    eventFeature.addFeature("U_POSITION", s"${pid}_${position_type}_$position")
    eventFeature.addFeature("SERVER_TIME",servertime_sec)
    eventFeature.addFeature("STIME", sTime)
    eventFeature.addFeature("REQUEST_TIME", requestTime)
    eventFeature.addFeature("PS_TIME", psTime)
    eventFeature.addFeature("RID",rid)
    eventFeature.addFeature("SRID",srid)
    eventFeature.addFeature("D_SRC", src)
    eventFeature.addFeature("U_NET", net)
    eventFeature.addFeature("U_OSVERSION", osVersion)
    eventFeature.addFeature("U_APPVERSION", appVersion)
    eventFeature.addFeature("U_PLATFORM", platForm)
    eventFeature.addFeature("U_BRAND", brand)
    eventFeature.addFeature("U_MODEL", model)
    eventFeature.addFeature("U_CHANNEL", channel)
    eventFeature.addFeature("U_ISHOT", isHot)
    eventFeature.addFeature("U_APPIV", appIv)
    eventFeature.addFeature("D_GMP_SCORE", gmp_score)
    eventFeature.addFeature("D_GMP_VERSION", gmp_ver)
    eventFeature.addFeature("U_QUERYPACKAGE", query_pkg)
  }

  def json3Parse(record: String, level1: String, level2: String, level3: String): String = {
    val value = Try({
      val jvalue = JsonUtil.convertToJValue(record)
      jvalue \ level1 \ level2 \ level3 match {
        case JString(s) => s.trim
        case JDouble(s) => s.toString
        case JInt(s) => s.toString
        case _ => ""
      }
    }) match {
      case Success(s) => s
      case Failure(ex) =>
        logError(s"Parse $record error!",ex)
        ""
    }
    value
  }

  def isEventValid(record: String,
                   countries: List[String],
                   actFilter: List[String]): Boolean = {
    val pid = getValueForKey("pid", record)
    val position = getValueForKey("level1", record)
    val position_type = getValueForKey("level1_type", record)
    val country = getValueForKey("country", record)
    val app_lan = getValueForKey("app_lan", record)
    val driver = Try(getValueForKey("drv", record).toInt).getOrElse(1)

    def isCMTab: Boolean = {
      pid.equals("1") && position.equals("1") && position_type.equals("1")
    }
    def isCMTabScreen: Boolean = {
      Array("1","21").contains(pid) && position.equals("11") && position_type.equals("1")
    }
    def isCMScreen: Boolean = {
      Array("1","21").contains(pid) && position.equals("17") && position_type.equals("1")
    }
    def isInstaNews: Boolean = {
      pid.equals("11") && position.equals("1") && (position_type.equals("1") || position_type.equals("10"))
    }
    def isInstaNewsV2: Boolean = {
      pid.equals("11") && position.equals("1") && position_type.equals("1")
    }
    def isNewsRepublic: Boolean = {
      pid.equals("14") && position.equals("1") && position_type.equals("1")
    }
    def isNewsRepublicV2: Boolean = {
      pid.equals("14") &&
        (
          (position.equals("13") && position_type.equals("13")) ||
            (position.equals("14") && position_type.equals("14")) ||
            (position.equals("15") && position_type.equals("15"))
          )
    }
    def isNewsRepublicLocker: Boolean = {
      pid.equals("14") && position.equals("25") && position_type.equals("1")
    }
    def isNewsRepublicPush: Boolean = {
      pid.equals("14") && position.equals("0") && position_type.equals("3")
    }
    def isVideoApp: Boolean = {
      pid.equals("15") && position.equals("1") && position_type.equals("1")
    }
    def isValidContent: Boolean = {
      val aid = getValueForKey("aid", record)
      val contentid = getValueForKey("contentid", record)
      val act = getValueForKey("act", record)
      val isActValid = actFilter.contains(act)
      aid.length > 0 && contentid.length > 0 && country.length == 2 && app_lan.length == 2 && isActValid
    }

    def isValidPidAndRegion(ct: String): Boolean = {
      ct match {
        case "US" => country.equals("US") && isCMTab // for us_cfb
        case "US_CM" => country.equals("US") && (isCMScreen || isCMTabScreen) // for us_cfb
        case "NR_US" => country.equals("US") && (isNewsRepublicV2 || isNewsRepublic)
        case "ALL_US" => country.equals("US") && (isCMTab || isCMTabScreen || isNewsRepublic)
        case "CMSC_NR_US" => country.equals("US") && (isCMScreen || isNewsRepublic || isNewsRepublicLocker)
        case "RU" => country.equals("RU") && isCMTab // for ru_cfb
        case "NR_RU" => country.equals("RU") && isNewsRepublic // for ru_cfb
        case "IN" => country.equals("IN") && isInstaNews
        case "IN_en" => country.equals("IN") && isInstaNews && app_lan.equals("en")
        case "IN_hi" => country.equals("IN") && isInstaNews && app_lan.equals("hi")
        case "hi" => isInstaNews && app_lan.equals("hi")
        case "IN_hi_V2" => country.equals("IN") && isInstaNewsV2 && app_lan.equals("hi")
        case "US_N_CA" => (country.equals("US") || country.equals("CA")) && isNewsRepublic
        case "OTHER_THAN_IN_US_RU" => !country.equals("IN") && !country.equals("US") && !country.equals("RU") && isCMTab //for multilan_cfb
        case "WORLDWIDE" => (country.equals("IN") && isInstaNews) ||
          (isCMTab && !country.equals("IN")) ||
          (isCMTabScreen && !country.equals("IN")) //for group_gmp
        case "WORLDWIDE_NR"=> isNewsRepublic
        case "WORLDWIDE_VIDEO" => (country.equals("IN") && isInstaNews) ||
          (!country.equals("IN") && (isVideoApp || isCMTabScreen)) ||
          isNewsRepublic
        case _ => false
      }
    }

    isValidContent && (countries.count(c => isValidPidAndRegion(c)) > 0) && driver > 0
  }

  def parseDes(des: String, key: String) = {
    val data = des.split("\\|")
    val dataMap = scala.collection.mutable.Map[String, String]()
    for (kv <- data) {
      val tArray = kv.split("=")
      if (2 == tArray.size ) {
        dataMap.update(tArray(0), tArray(1))
      }
    }
    dataMap.getOrElse(key, "")
  }
}
