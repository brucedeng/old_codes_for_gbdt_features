package com.cmcm.news.app

import com.cmcm.news.component.{EventComponent, FeatureComponent}
import com.cmcm.news.config.Configurable
import com.cmcm.news.model.LRModel
import com.cmcm.news.spark.SparkComponent

/**
  * Created by lilonghua on 16/8/25.
  */

object LRApp extends DLApp
  with Configurable
  with SparkComponent
  with EventComponent
  with FeatureComponent
  with LRModel