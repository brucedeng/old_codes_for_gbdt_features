package com.cmcm.news.component

import com.cmcm.news.config.Configurable
import com.cmcm.news.monitor.MonitorProxy
import com.cmcm.news.spark.SparkNormal
import org.apache.spark.Logging
import org.apache.spark.broadcast.Broadcast

/**
  * Created by lilonghua on 16/9/9.
  */

object NormalComponent extends Logging {
  val componentName = "model_component"
  val configFileName = "model.conf"
  val defaultTenFeatPath = "/projects/news/deeplearning/model/training/gen_training_data_10min/hi_test/training_data/20160909/05/10"
  val defaultOneFeatPath = "/projects/news/deeplearning/model/training/gen_training_data_streaming/hi_test/training_data/20160909/05/{0[1-9],10}"
  val defaultModelOutputDest = "file"
  val defaultModelOutputFilePrefix = "/tmp/deeplearning_lr/model"
  val defaultDecayWindow = 600
  var monitor: MonitorProxy = _
  var monitorBroadcastVar: Broadcast[MonitorProxy] = _
}

trait NormalComponent extends Component {
  this: Configurable with SparkNormal =>

  override def init(configDir: String) = {
    super.init(configDir)
    initModelComponent(configDir + "/" + NormalComponent.configFileName)
    NormalComponent.monitor = new MonitorProxy()
    NormalComponent.monitor.init(modelConfigUnit)
    NormalComponent.monitorBroadcastVar = sparkContext.broadcast(NormalComponent.monitor)
  }

  override def preStart() = {
    super.preStart()
  }

  def modelConfigUnit = {
    getConfigUnit(ModelComponent.componentName).get
  }

  def initModelComponent(modelConfigFile: String) = {
    logInfo("Initializing model component ... ")
    loadComponentConfig(ModelComponent.componentName, modelConfigFile)
  }

  def workflowSetup()
}