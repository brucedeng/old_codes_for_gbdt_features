package com.cmcm.news.parse

import java.util.concurrent.ArrayBlockingQueue
import java.util.{Timer, TimerTask}

import com.cmcm.news.component.FeatureComponent
import com.cmcm.news.feature.UserFeature
import com.typesafe.config.Config
import org.apache.spark.rdd.RDD
import org.apache.spark.{Logging, SparkContext}
import org.joda.time.DateTime

import scala.util.{Failure, Success, Try}

/**
  * Created by hanbin on 15/9/7.
  */
case object UPTimingLoader extends Logging {

  val upQueue = new ArrayBlockingQueue[(String, RDD[(String, String)])](10)
  val timer = new Timer(true)
  var lastUPTime: String = ""

  def initUPTimingLoader(upConfigUnit: Config, sparkContext: SparkContext) = {
    logInfo("Init UP timing loader...")
    val period = Try(upConfigUnit.getInt("load_period")).getOrElse(FeatureComponent.defaultUPLoadPeriod)
    val dateTimeFormat = Try(upConfigUnit.getString("datetime_format")).getOrElse(FeatureComponent.defaultUPDatetimeFormat)
    val dataType = Try(upConfigUnit.getString("data_type")).getOrElse(FeatureComponent.defaultUPDataType)
    val offset = Try(upConfigUnit.getInt("load_datetime_offset")).getOrElse(FeatureComponent.defaultUPDatetimeOffset)
    val task = new TimerTask {
      override def run(): Unit = {
        val loadDate = Try((offset to -1).map( d => {
          if (dataType == "day") {
            if (dateTimeFormat.contains("/")) {
              logError(s"Maybe wrong dateTimeFormat $dateTimeFormat with dataType $dataType")
            }
            Try(new DateTime().plusDays(d).toString(dateTimeFormat)).getOrElse("")
          } else if (dataType == "hours") {
            Try(new DateTime().plusHours(d).toString(dateTimeFormat)).getOrElse("")
          } else {
            ""
          }
        }).filter( date => {
          (lastUPTime == "" || date > lastUPTime) && UserFeature.checkDirSuccess(upConfigUnit, date)
        }).sortWith(_>_).head).getOrElse("")

        if (loadDate.nonEmpty) {
          logInfo(s"Begin to load user profile for $loadDate")

          Try(UserFeature.loadJsonFeature(upConfigUnit, loadDate, sparkContext)) match {
            case Success(rdd) => {
              logInfo(s"Success to load user profile for $loadDate")
              if (upQueue.size() >= 10) {
                logError(s"Can not add user profile for $loadDate to the queue, the queue is full now!")
                sys.exit(-1)
              }
              upQueue.add((loadDate, rdd))
              lastUPTime = loadDate
            }
            case Failure(error) => {
              logError(s"Fail to load user profile for $loadDate, because: " + error.getMessage,error)
            }
          }
        }
      }
    }
    timer.schedule(task, 0L, (period / 24) * 1000)
  }

  def getCurrentUserFeatureRDD(): Option[(String, RDD[(String, String)])] = {
    if (upQueue.isEmpty)
      None
    else {
      while (upQueue.size() > 1) {
        val oldUp = upQueue.remove()
        oldUp._2.unpersist()
        logInfo("User feature: unpersist the old user feature RDD")
      }
      Some(upQueue.element())
    }
  }

  def waitForReady(times: Int) = {
    var tryCount = 0
    while (getCurrentUserFeatureRDD.isEmpty) {
      logInfo("The user feature is not available!")
      Thread.sleep(10000)
      tryCount = tryCount + 1
      if (tryCount > times) {
        logError("Can not get the user feature!")
        sys.exit(-1)
      }
    }
  }
}
