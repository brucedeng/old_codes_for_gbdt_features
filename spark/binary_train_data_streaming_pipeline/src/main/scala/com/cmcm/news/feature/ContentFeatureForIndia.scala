package com.cmcm.news.feature

import com.cmcm.news.feature.Profile.{CategoryProfile, EntityProfile, KeywordProfile, TopicProfile}
import com.cmcm.news.util.Utils
import com.typesafe.config.Config
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.Logging
import redis.clients.jedis.{HostAndPort, Jedis, JedisCluster}

import collection.JavaConversions._
import scala.util.Try

/**
  * Created by wangwei5 on 15/8/12.
  */
object ContentFeatureForIndia extends Logging {

  def getCategories(cc: List[CategoryProfile], topCategories: Int) = {
    cc.map(f => (f.getName, f.getL1Weight))
      .sortWith(_._2 > _._2).take(topCategories)
      .map(f => f._1 + Utils.keyGap + f._2)
      .mkString(Utils.pairGap)
  }

  def getKeywords(ck: List[KeywordProfile], topKeywords: Int) = {
    ck.map(f => (f.getName, f.getL1Weight))
      .sortWith(_._2 > _._2).take(topKeywords)
      .map(f => f._1 + Utils.keyGap + f._2)
      .mkString(Utils.pairGap)
  }

  def getTopics(ct: List[TopicProfile], topTopics: Int) = {
    ct.map(f => (f.getName, f.getL1Weight))
      .sortWith(_._2 > _._2).take(topTopics)
      .map(f => f._1 + Utils.keyGap + f._2)
      .mkString(Utils.pairGap)
  }



  def getEntities(ct: List[EntityProfile], topEntities: Int) = {
    ct.map(f => (f.getName, f.getL1Weight))
      .sortWith(_._2 > _._2).take(topEntities)
      .map(f => f._1 + Utils.keyGap + f._2)
      .mkString(Utils.pairGap)
  }

  def getImageLabels(ci: List[String]) = {
    ci.map(f => (f, 0.05))
      .map(f => f._1 + Utils.keyGap + f._2)
      .mkString(Utils.pairGap)
  }



  def getContentFeature(news_id: String,
                        redisClient: Jedis,
                        topRelCat: Int,
                        topRelKw: Int,
                        topRelTopic: Int,
                        topRelEntity: Int,
                        topRelTitleEntity: Int,
                        topRelDisplayEntity: Int,
                        topCategories: Int,
                        topKeywords: Int,
                        topTopics: Int,
                        topEntities: Int,
                        topTitleEntities: Int,
                        topDisplayEntities: Int,
                        categoryVersion: String,
                        keywordVersion: String,
                        topicVersion: String,
                        entityVersion: String,
                        titleEntityVersion: String,
                        displayEntityVersion: String) = {
    val cf = redisClient.get(("n_" + news_id).getBytes())

    if (cf != null) {
      val newsProfile = CmnewsNewsProfile.NewsProfile.parseFrom(cf)
      val cc = if(categoryVersion.length <= 0){
        newsProfile.getCategoriesList.toList
      } else {
        var ccList = List[CategoryProfile]()
        for (category <- newsProfile.getVcategoriesList) {
          if (category.getVersion.equals(categoryVersion)) {
            ccList = category.getCategoriesList.toList
          }
        }
        ccList
      }
      val ccString = getCategories(cc, topCategories)
      val ccRel = getCategories(cc,topRelCat)

      val ck = if(keywordVersion.length <= 0){
        newsProfile.getKeywordsList.toList
      } else {
        var ckList = List[KeywordProfile]()
        for (keyword <- newsProfile.getVkeywordsList) {
          if (keyword.getVersion.equals(keywordVersion)) {
            ckList = keyword.getKeywordsList.toList
          }
        }
        ckList
      }
      val ckString = getKeywords(ck, topKeywords)
      val ckRel = getKeywords(ck, topRelKw)

      val ct = if(topicVersion.length <= 0){
        List[TopicProfile]()
      } else {
        var ctList = List[TopicProfile]()
        for (topic <- newsProfile.getVtopicsList) {
          if (topic.getVersion.equals(topicVersion)) {
            ctList = topic.getTopicsList.toList
          }
        }
        ctList
      }
      val ctString = getTopics(ct, topTopics)
      val ctRel = getTopics(ct, topRelTopic)


      val ce = if(entityVersion.length <= 0){
        List[EntityProfile]()
      } else {
        var ceList = List[EntityProfile]()
        for (entity <- newsProfile.getVentitiesList) {
          if (entity.getVersion.equals(entityVersion)) {
            ceList = entity.getEntitiesList.toList
          }
        }
        ceList
      }
      val ceString = getEntities(ce, topEntities)
      val ceRel = getEntities(ce, topRelEntity)

      // ---------------------by cxq---------------------------
      val ctitleEnt = if(titleEntityVersion.length <= 0){
        List[EntityProfile]()
      } else {
        var ctitleEntList = List[EntityProfile]()
        for (titleEnt <- newsProfile.getTitleEntitiesList) {
          if (titleEnt.getVersion.equals(titleEntityVersion)) {
            ctitleEntList = titleEnt.getEntitiesList.toList
          }
        }
        ctitleEntList
      }
      val ctitleEntString = getEntities(ctitleEnt, topTitleEntities)
      val ctitleEntRel = getEntities(ctitleEnt, topRelTitleEntity)
      val ctitleEntitiesLength = ctitleEnt.size


      val cdisplayEnt = if(displayEntityVersion.length <= 0){
        List[EntityProfile]()
      } else {
        var cdisplayEntList = List[EntityProfile]()
        for (displayEnt <- newsProfile.getDisplayEntitiesList) {
          if (displayEnt.getVersion.equals(titleEntityVersion)) {
            cdisplayEntList = displayEnt.getEntitiesList.toList
          }
        }
        cdisplayEntList
      }
      val cdisplayEntString = getEntities(cdisplayEnt, topDisplayEntities)
      val cdisplayEntRel = getEntities(cdisplayEnt, topRelDisplayEntity)
      val cdisplayEntitiesLength = cdisplayEnt.size
      // ---------------------by cxq---------------------------




      val ci = newsProfile.getImageLabelsList()
      val ciString = getImageLabels(ci.toList)
      val ciRel = getImageLabels(ci.toList)

      val publisher = newsProfile.getPublisher
      val publishTime = newsProfile.getPublishTime
      val groupId = newsProfile.getGroupId
      val titleMd5 = newsProfile.getTitleMd5
      //val updateTime = newsProfile.getUpdateTime
      val tier  = newsProfile.getTier
      val listImageCount = newsProfile.getListImageCount   //  by cxq
      val titleType = newsProfile.getTitleType   //  by cxq
      val titleLength = newsProfile.getTitle.length   //  by cxq
      val quanlity = if(newsProfile.hasQuality) newsProfile.getQuality else Utils.defaultNu.toFloat  //  by cxq


      val wordCount = if (newsProfile.hasWordCount) newsProfile.getWordCount else Utils.defaultNu
      val imageCount = if (newsProfile.hasImageCount) newsProfile.getImageCount else Utils.defaultNu
      val newsyScore = if (newsProfile.hasNewsyScore) newsProfile.getNewsyScore else Utils.defaultNu
      val mediaLevel = if (newsProfile.hasMediaLevel) newsProfile.getMediaLevel else Utils.defaultNu
      val commentCount =  if (newsProfile.hasCommentCount) newsProfile.getCommentCount else Utils.defaultNu
      val hasBigImg = if (newsProfile.hasHasBigImg) Try(newsProfile.getHasBigImg.toInt).getOrElse(0) else Utils.defaultNu

      val feature = EventFeature()
      feature.addFeature("D_KEYWORD", ckString)
      feature.addFeature("D_KEYWORD_REL", ckRel)
      feature.addFeature("D_CATEGORY", ccString)
      feature.addFeature("D_CATEGORY_REL", ccRel)
      feature.addFeature("D_TOPIC", ctString)
      feature.addFeature("D_TOPIC_REL", ctRel)
      feature.addFeature("D_ENTITY",ceString)
      feature.addFeature("D_ENTITY_REL",ceRel)
      feature.addFeature("D_IMAGELABEL", ciString)
      feature.addFeature("D_IMAGELABEL_REL", ciRel)
      feature.addFeature("D_PUBLISHER", publisher)
      feature.addFeature("C_PUBLISHTIME", publishTime.toString)
      feature.addFeature("D_GROUPID", groupId)
      feature.addFeature("D_TITLEMD5", titleMd5)
      feature.addFeature("D_TIER", tier)
      feature.addFeature("D_WORDCOUNT", wordCount.toString)
      feature.addFeature("D_IMAGECOUNT", imageCount.toString)
      feature.addFeature("D_NEWSYSCORE", newsyScore.toString)
      feature.addFeature("D_MEDIALEVEL", mediaLevel.toString)
      feature.addFeature("D_COMMENTCOUNT", commentCount.toString)
      feature.addFeature("D_HASBIGIMG", hasBigImg.toString)
      feature.addFeature("D_LISTIMAGECOUNT", listImageCount)   //  by cxq
      feature.addFeature("D_TITLETYPE", titleType)   //  by cxq
      feature.addFeature("D_TITLELENGTH", titleLength.toString)   //  by cxq
      feature.addFeature("D_QUALITY", quanlity.toString)   //  by cxq
      feature.addFeature("D_TITLEENTITIESCOUNT", ctitleEntitiesLength.toString)   //  by cxq
      feature.addFeature("D_DISPLAYENTITIESCOUNT", cdisplayEntitiesLength.toString)   //  by cxq
      feature.addFeature("D_TITLEENTITY",ctitleEntString)   //  by cxq
      feature.addFeature("D_TITLEENTITY_REL",ctitleEntRel)   //  by cxq
//      feature.addFeature("D_DISPLAYENTITY",cdisplayEntString)   //  by cxq
//      feature.addFeature("D_DISPLAYENTITY_REL",cdisplayEntRel)   //  by cxq
      feature.addFeature("D_DISPLAYENTITY",cdisplayEntString.toLowerCase)   //  by cxq
      feature.addFeature("D_DISPLAYENTITY_REL",cdisplayEntRel.toLowerCase)   //  by cxq


      feature.genFeatureString()
    } else {
      ""
    }
  }

  /**
    *
    * @param batchEvent (contentId, (uid, batchId, act, value, eventFeature))
    * @param contentFeatureConfig cf config
    * @return (uid, (cid, batchId, act, value, eventFeature, contentFeature))
    */
  def getContentFeatureFromRedis(batchEvent: DStream[(String, (String, Long, String, Double, String))], contentFeatureConfig: Config) = {
    val host:String = contentFeatureConfig.getString("redis_host")
    val port = contentFeatureConfig.getInt("redis_port")
    val topRelCat = Try(contentFeatureConfig.getInt("rel_d_ncat")).getOrElse(200)
    val topRelKw = Try(contentFeatureConfig.getInt("rel_d_nkey")).getOrElse(200)
    val topRelTopic = Try(contentFeatureConfig.getInt("rel_d_ntopic")).getOrElse(200)
    val topRelEntity = Try(contentFeatureConfig.getInt("rel_d_nentity")).getOrElse(200)
    val topRelTitleEntity = Try(contentFeatureConfig.getInt("rel_d_ntitleentity")).getOrElse(200)   // by cxq
    val topRelDisplayEntity = Try(contentFeatureConfig.getInt("rel_d_ndisplayentity")).getOrElse(200)   // by cxq

    val topCategories = Try(contentFeatureConfig.getInt("top_categories")).getOrElse(5)
    val topKeywords = Try(contentFeatureConfig.getInt("top_keywords")).getOrElse(15)
    val topTopics= Try(contentFeatureConfig.getInt("top_topics")).getOrElse(5)
    val topEntity= Try(contentFeatureConfig.getInt("top_entities")).getOrElse(10)
    val topTitleEntities= Try(contentFeatureConfig.getInt("top_titleentities")).getOrElse(10)   // by cxq
    val topDisplayEntities= Try(contentFeatureConfig.getInt("top_displayentities")).getOrElse(10)   // by cxq

    val categoryVersion = Try(contentFeatureConfig.getString("category_version")).getOrElse("v4")
    val keywordVersion = Try(contentFeatureConfig.getString("keyword_version")).getOrElse("v2")
    val topicVersion = Try(contentFeatureConfig.getString("topic_version")).getOrElse("v2")
    val entityVersion = Try(contentFeatureConfig.getString("entity_version")).getOrElse("v2")
    val titleEntityVersion = Try(contentFeatureConfig.getString("titleentity_version")).getOrElse("v1")   // by cxq
    val displayEntityVersion = Try(contentFeatureConfig.getString("displayentity_version")).getOrElse("v1")   // by cxq

    val auth = contentFeatureConfig.getString("redis_password")
    val hosts = host.split(",")
    batchEvent.transform(rdd => {
      val timestamp = System.currentTimeMillis()
      rdd.mapPartitionsWithIndex((index, events) => {
        val redisHost = hosts(index % hosts.length)
        var result = List.empty[(String, (String, Long, String, Double, String, String))].toIterator
        val redisClient = new Jedis(redisHost, port)
        try {
          if (auth.nonEmpty) redisClient.auth(auth)
          result = events.map(event => {
            (event._2._1, (event._1, event._2._2, event._2._3, event._2._4, event._2._5,
              getContentFeature(event._1, redisClient, topRelCat, topRelKw, topRelTopic, topRelEntity, topRelTitleEntity, topRelDisplayEntity, topCategories, topKeywords, topTopics, topEntity, topTitleEntities, topDisplayEntities, categoryVersion, keywordVersion, topicVersion, entityVersion, titleEntityVersion, displayEntityVersion)))
          })
        } catch {
          case ex: Throwable =>
            logError(s"Get cp from ${redisHost} failed", ex)
        } finally {
          try {
            redisClient.quit()
            redisClient.close()
            logInfo(s"Close ${redisHost} from partition $index at $timestamp")
          } catch {
            case e: Throwable =>
              logError(s"Close ${redisHost} failed",e)
          }
        }
        result
      })
    })
  }

  def getCPFromRedisCluster(batchEvent: DStream[(String, (String, Long, String, Double, String))], contentFeatureConfig: Config) = {
    val host:String = contentFeatureConfig.getString("redis_host")
    val port = contentFeatureConfig.getInt("redis_port")
    val topRelCat = Try(contentFeatureConfig.getInt("rel_d_ncat")).getOrElse(200)
    val topRelKw = Try(contentFeatureConfig.getInt("rel_d_nkey")).getOrElse(200)
    val topRelTopic = Try(contentFeatureConfig.getInt("rel_d_ntopic")).getOrElse(200)
    val topRelEntity = Try(contentFeatureConfig.getInt("rel_d_nentity")).getOrElse(200)
    val topRelTitleEntity = Try(contentFeatureConfig.getInt("rel_d_ntitleentity")).getOrElse(200)   // by cxq
    val topRelDisplayEntity = Try(contentFeatureConfig.getInt("rel_d_ndisplayentity")).getOrElse(200)   // by cxq

    val topCategories = Try(contentFeatureConfig.getInt("top_categories")).getOrElse(5)
    val topKeywords = Try(contentFeatureConfig.getInt("top_keywords")).getOrElse(15)
    val topTopics= Try(contentFeatureConfig.getInt("top_topics")).getOrElse(10)
    val topEntities= Try(contentFeatureConfig.getInt("top_entities")).getOrElse(10)
    val topTitleEntities= Try(contentFeatureConfig.getInt("top_titleentities")).getOrElse(10)   // by cxq
    val topDisplayEntities= Try(contentFeatureConfig.getInt("top_dispalyentities")).getOrElse(10)   // by cxq

    val categoryVersion = Try(contentFeatureConfig.getString("category_version")).getOrElse("v4")
    val keywordVersion = Try(contentFeatureConfig.getString("keyword_version")).getOrElse("v2")
    val topicVersion = Try(contentFeatureConfig.getString("topic_version")).getOrElse("v2")
    val entityVersion = Try(contentFeatureConfig.getString("entity_version")).getOrElse("v2")
    val titleEntityVersion = Try(contentFeatureConfig.getString("titleentity_version")).getOrElse("v1")   // by cxq
    val displayEntityVersion = Try(contentFeatureConfig.getString("displayentity_version")).getOrElse("v1")   // by cxq
    //val auth = contentFeatureConfig.getString("redis_password")
    val nodes = host.split(",").distinct.map {h => new HostAndPort(h, port)}.toSet
    val cpFeat = batchEvent.mapPartitions(events => {
      val cl = new JedisCluster(nodes)
      val result = events.map(event => {
        (event._2._1, (event._1, event._2._2, event._2._3, event._2._4, event._2._5,
          contentFeature(event._1, cl, topRelCat, topRelKw, topRelTopic, topCategories, topRelEntity, topRelTitleEntity, topRelDisplayEntity, topKeywords, topTopics, topEntities, topTitleEntities, topDisplayEntities, categoryVersion, keywordVersion, topicVersion, entityVersion, titleEntityVersion, displayEntityVersion)))
      })
      cl.close()
      result
    })
    cpFeat
  }

  def contentFeature(news_id: String,
                     cl: JedisCluster,
                     topRelCat: Int,
                     topRelKw: Int,
                     topRelTopic: Int,
                     topRelEntity: Int,
                     topRelTitleEntity: Int,
                     topRelDisplayEntity: Int,
                     topCategories: Int,
                     topKeywords: Int,
                     topTopics: Int,
                     topEntities: Int,
                     topTitleEntities: Int,
                     topDisplayEntities: Int,
                     categoryVersion: String,
                     keywordVersion: String,
                     topicVersion: String,
                     entityVersion: String,
                     titleEntityVersion: String,
                     displayEntityVersion: String) = {
    val cf = cl.get(("n_" + news_id).getBytes())

    if (cf != null) {
      val newsProfile = CmnewsNewsProfile.NewsProfile.parseFrom(cf)
      val cc = if(categoryVersion.length <= 0){
        newsProfile.getCategoriesList.toList
      } else {
        var ccList = List[CategoryProfile]()
        for (category <- newsProfile.getVcategoriesList) {
          if (category.getVersion.equals(categoryVersion)) {
            ccList = category.getCategoriesList.toList
          }
        }
        ccList
      }
      val ccString = getCategories(cc, topCategories)
      val ccRel = getCategories(cc,topRelCat)

      val ck = if(keywordVersion.length <= 0){
        newsProfile.getKeywordsList.toList
      } else {
        var ckList = List[KeywordProfile]()
        for (keyword <- newsProfile.getVkeywordsList) {
          if (keyword.getVersion.equals(keywordVersion)) {
            ckList = keyword.getKeywordsList.toList
          }
        }
        ckList
      }
      val ckString = getKeywords(ck, topKeywords)
      val ckRel = getKeywords(ck, topRelKw)

      val ct = if(topicVersion.length <= 0){
        List[TopicProfile]()
      } else {
        var ctList = List[TopicProfile]()
        for (topic <- newsProfile.getVtopicsList) {
          if (topic.getVersion.equals(topicVersion)) {
            ctList = topic.getTopicsList.toList
          }
        }
        ctList
      }
      val ctString = getTopics(ct, topTopics)
      val ctRel = getTopics(ct, topRelTopic)

      val ce = if(entityVersion.length <= 0){
        List[EntityProfile]()
      } else {
        var ceList = List[EntityProfile]()
        for (entity <- newsProfile.getVentitiesList) {
          if (entity.getVersion.equals(entityVersion)) {
            ceList = entity.getEntitiesList.toList
          }
        }
        ceList
      }
      val ceString = getEntities(ce, topEntities)
      val ceRel = getEntities(ce, topRelEntity)

      // ---------------------by cxq---------------------------
      val ctitleEnt = if(titleEntityVersion.length <= 0){
        List[EntityProfile]()
      } else {
        var ctitleEntList = List[EntityProfile]()
        for (titleEnt <- newsProfile.getTitleEntitiesList) {
          if (titleEnt.getVersion.equals(titleEntityVersion)) {
            ctitleEntList = titleEnt.getEntitiesList.toList
          }
        }
        ctitleEntList
      }
      val ctitleEntString = getEntities(ctitleEnt, topTitleEntities)
      val ctitleEntRel = getEntities(ctitleEnt, topRelTitleEntity)
      val ctitleEntitiesLength = ctitleEnt.size


      val cdisplayEnt = if(displayEntityVersion.length <= 0){
        List[EntityProfile]()
      } else {
        var cdisplayEntList = List[EntityProfile]()
        for (displayEnt <- newsProfile.getDisplayEntitiesList) {
          if (displayEnt.getVersion.equals(titleEntityVersion)) {
            cdisplayEntList = displayEnt.getEntitiesList.toList
          }
        }
        cdisplayEntList
      }
      val cdisplayEntString = getEntities(cdisplayEnt, topDisplayEntities)
      val cdisplayEntRel = getEntities(cdisplayEnt, topRelDisplayEntity)
      val cdisplayEntitiesLength = cdisplayEnt.size
      // ---------------------by cxq---------------------------

      val ci = newsProfile.getImageLabelsList()
      val ciString = getImageLabels(ci.toList)
      val ciRel = getImageLabels(ci.toList)


      val publisher = newsProfile.getPublisher
      val publishTime = newsProfile.getPublishTime
      val groupId = newsProfile.getGroupId
      val titleMd5 = newsProfile.getTitleMd5
      //val updateTime = newsProfile.getUpdateTime
      val tier  = newsProfile.getTier
      val listImageCount = newsProfile.getListImageCount   //  by cxq
      val titleType = newsProfile.getTitleType   //  by cxq
      val titleLength=newsProfile.getTitle.length   //  by cxq
      val quanlity = if(newsProfile.hasQuality) newsProfile.getQuality else Utils.defaultNu.toFloat  //  by cxq

      val wordCount = if (newsProfile.hasWordCount) newsProfile.getWordCount else Utils.defaultNu
      val imageCount = if (newsProfile.hasImageCount) newsProfile.getImageCount else Utils.defaultNu
      val newsyScore = if (newsProfile.hasNewsyScore) newsProfile.getNewsyScore else Utils.defaultNu
      val mediaLevel = if (newsProfile.hasMediaLevel) newsProfile.getMediaLevel else Utils.defaultNu
      val commentCount =  if (newsProfile.hasCommentCount) newsProfile.getCommentCount else Utils.defaultNu
      val hasBigImg = if (newsProfile.hasHasBigImg) Try(newsProfile.getHasBigImg.toInt).getOrElse(0) else Utils.defaultNu

      val feature = EventFeature()
      feature.addFeature("D_KEYWORD", ckString)
      feature.addFeature("D_KEYWORD_REL", ckRel)
      feature.addFeature("D_CATEGORY", ccString)
      feature.addFeature("D_CATEGORY_REL", ccRel)
      feature.addFeature("D_TOPIC", ctString)
      feature.addFeature("D_TOPIC_REL", ctRel)
      feature.addFeature("D_ENTITY", ceString)
      feature.addFeature("D_ENTITY_REL", ceRel)
      feature.addFeature("D_IMAGELABEL", ciString)
      feature.addFeature("D_IMAGELABEL_REL", ciRel)
      feature.addFeature("D_PUBLISHER", publisher)
      feature.addFeature("C_PUBLISHTIME", publishTime.toString)
      feature.addFeature("D_GROUPID", groupId)
      feature.addFeature("D_TITLEMD5", titleMd5)
      feature.addFeature("D_TIER", tier)
      feature.addFeature("D_WORDCOUNT", wordCount.toString)
      feature.addFeature("D_IMAGECOUNT", imageCount.toString)
      feature.addFeature("D_NEWSYSCORE", newsyScore.toString)
      feature.addFeature("D_MEDIALEVEL", mediaLevel.toString)
      feature.addFeature("D_COMMENTCOUNT", commentCount.toString)
      feature.addFeature("D_HASBIGIMG", hasBigImg.toString)
      feature.addFeature("D_LISTIMAGECOUNT", listImageCount)   //  by cxq
      feature.addFeature("D_TITLETYPE", titleType)   //  by cxq
      feature.addFeature("D_TITLELENGTH", titleLength.toString)   //  by cxq
      feature.addFeature("D_QUALITY", quanlity.toString)   //  by cxq
      feature.addFeature("D_TITLEENTITIESCOUNT", ctitleEntitiesLength.toString)   //  by cxq
      feature.addFeature("D_DISPLAYENTITIESCOUNT", cdisplayEntitiesLength.toString)   //  by cxq
      feature.addFeature("D_TITLEENTITY",ctitleEntString)   //  by cxq
      feature.addFeature("D_TITLEENTITY_REL",ctitleEntRel)   //  by cxq
//      feature.addFeature("D_DISPLAYENTITY",cdisplayEntString)   //  by cxq
//      feature.addFeature("D_DISPLAYENTITY_REL",cdisplayEntRel)   //  by cxq
      feature.addFeature("D_DISPLAYENTITY",cdisplayEntString.toLowerCase)   //  by cxq
      feature.addFeature("D_DISPLAYENTITY_REL",cdisplayEntRel.toLowerCase)   //  by cxq


      feature.genFeatureString()
    } else {
      ""
    }
  }

}
