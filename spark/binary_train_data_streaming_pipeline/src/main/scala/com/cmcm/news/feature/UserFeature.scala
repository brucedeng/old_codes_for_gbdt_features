package com.cmcm.news.feature

import java.net.URI

import com.cmcm.news.feature.UfsUp.{UserProfile, WeightedTag}
import com.cmcm.news.util.{JsonUtil, Utils}
import com.typesafe.config.Config
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path, PathFilter}
import org.apache.http.HttpEntity
import org.apache.http.client.utils.URIBuilder
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.{HashPartitioner, Logging, SparkContext}
import org.json4s.JsonAST._

import scala.collection.JavaConversions._
import scala.util.control.Breaks._
import scala.util.{Failure, Success, Try}

/**
  *  Created by wangwei5 on 15/8/12.
  */

case class UPNotReadyException(msg: String) extends Throwable {
  override def getMessage: String = "UP load error : " + msg
}

object UserFeature extends Logging {

  def jsonLineParser(line: String,
                     topRelCat: Int,
                     topRelKw: Int,
                     topRelTopic: Int,
                     topRelEntity: Int,
                     topRelImageLabel: Int,
                     topCategories: Int,
                     topKeywords: Int,
                     topTopics: Int,
                     topEntities: Int,
                     topImageLabels: Int,
                     topNotifyEntities: Int,
                     topPhase1Cat: Int,
                     categoryKey: String,
                     keywordKey: String,
                     topicKey:String,
                     entityKey:String,
                     imageLabelKey: String,
                     notifyEntityKey:String): (String, String) = {

    Try(JsonUtil.convertToJValue(line)) match {
      case Success(jvalue) =>
        val uuid = extractStr(jvalue, "uid", "")
        val categoriesRel = jvalue \ categoryKey match {
          case JArray(cats) => catKwAssist(cats, topRelCat, 0.0)
          case _ => "unknown"
        }

        val categories = topCatOrKw(categoriesRel, topCategories)

        val categorytop3 = topCatOrKw(categoriesRel, topPhase1Cat)
        val categorytop10 = topCatOrKw(categoriesRel, 10)
        val categorytop3single = crossCatTop(categorytop3)

        val keywordsRel = jvalue \ keywordKey match {
          case JArray(cats) => catKwAssist(cats, topRelKw, 0.0)
          case _ => "unknown"
        }
        val keywords = topCatOrKw(keywordsRel, topKeywords)

        val topicsRel = jvalue \ topicKey match {
          case JArray(cats) => catKwAssist(cats, topRelTopic, 0.0)
          case _ => "unknown"
        }
        val topics = topCatOrKw(topicsRel, topTopics)

        val entityRel = jvalue \ entityKey match {
          case JArray(cats) => catKwAssist(cats, topRelEntity, 0.0)
          case _ => "unknown"
        }
        val entities = topCatOrKw(entityRel, topEntities)

        val imageLabelRel = jvalue \ imageLabelKey match {
          case JArray(cats) => catKwAssist(cats, topRelImageLabel, 0.0)
          case _ => "unknown"
        }
        val image_labels = topCatOrKw(imageLabelRel, topImageLabels)


        val notifyEntities = jvalue \ notifyEntityKey match {
          case JArray(cats) => catKwAssist(cats, topNotifyEntities, 0.0)
          case _ => ""
        }


        val gender = extractStr(jvalue, "gender", "unknown")
        val age = extractStr(jvalue, "age", "unknown")
        val catLen = extractStr(jvalue, "u_cat_len", "0")
        val kwLen = extractStr(jvalue, "u_kw_len", "0")
        val topicLen = extractStr(jvalue, "u_topic_len", "0")
        val entityLen = extractStr(jvalue, "u_entities_v2_len", "0")
        val imageLabelLen = extractStr(jvalue, "u_image_label_len", "0")
        val feature = EventFeature()
        feature.addFeature("U_AGE",age)
        feature.addFeature("U_GENDER",gender)
        feature.addFeature("U_CATEGORY",categories)
        feature.addFeature("U_CATEGORY_REL", categoriesRel)
        feature.addFeature("U_KEYWORD",keywords)
        feature.addFeature("U_KEYWORD_REL", keywordsRel)
        feature.addFeature("U_TOPIC",topics)
        feature.addFeature("U_TOPIC_REL", topicsRel)
        feature.addFeature("U_ENTITY",entities)
        feature.addFeature("U_ENTITY_REL", entityRel)
        feature.addFeature("U_IMAGELABEL", image_labels)
        feature.addFeature("U_IMAGELABEL_REL", imageLabelRel)
        feature.addFeature("U_NOTIFYENTITY",notifyEntities)
        feature.addFeature("U_CATEGORYTOP3",categorytop3)
        feature.addFeature("U_CATEGORYTOP10",categorytop10)
        feature.addFeature("U_CATEGORYTOP3SINGLE",categorytop3single)
        feature.addFeature("U_CATLEN",catLen)
        feature.addFeature("U_KWLEN", kwLen)
        feature.addFeature("U_TOPICLEN", topicLen)
        feature.addFeature("U_ENTITYLEN",entityLen)
        feature.addFeature("U_IMAGELABELLEN", imageLabelLen)
        (uuid, feature.genFeatureString())
      case Failure(ex) =>
        logInfo(ex.getMessage, ex)
        ("", "")

    }
  }

  def checkDirSuccess(upConfig: Config, date: String): Boolean = {
    Try(dirContainsSuccess(upConfig.getString("input_dir") + "/" + date)).getOrElse(false)
  }

  def dirContainsSuccess(hdfsDir: String): Boolean = {
    val hdfs = FileSystem.get(URI.create(hdfsDir), new Configuration())
    val fileNameList = hdfs.listStatus(new Path(hdfsDir)).map(filePath => filePath.getPath.getName).toList
    //fileNameList.foreach(println(_))
    //fileNameList.exists(fileName => fileName.equals("_SUCCESS"))
    /*def flag = fileNameList.contains("_SUCCESS")
    logInfo(if (flag) "find up file ...." else "not found !!!!")
    flag*/
    fileNameList.contains("_SUCCESS")
  }

  def loadJsonFeature(upConfig: Config, date: String, sc: SparkContext): RDD[(String, String)] =  {
    val upPath = upConfig.getString("input_dir") + "/" + date
    if (!dirContainsSuccess(upPath)) {
      throw UPNotReadyException(upPath + " not ready")
    }
    val categoryKey = Try(upConfig.getString("category_key")).getOrElse("categories")
    val keywordKey = Try(upConfig.getString("keyword_key")).getOrElse("keywords")
    val topicKey = Try(upConfig.getString("topic_key")).getOrElse("topics")
    val entityKey = Try(upConfig.getString("entity_key")).getOrElse("entities_v2")
    
    val imageLabelKey = Try(upConfig.getString("image_label_key")).getOrElse("image_label")

    val notifyEntityKey = Try(upConfig.getString("notify_entity_key")).getOrElse("notify_entities")

    val hdfs = FileSystem.get(URI.create(upPath),new Configuration())
    val files: Array[String] = hdfs.listStatus(new Path(upPath), new PathFilter() {
      override def accept(path: Path): Boolean = {
        (! path.toString.contains("_SUCCESS")) && (! path.toString.contains("_temporary"))
      }
    }).map(status => status.getPath.toString)

    val userProifleLines: RDD[String] = sc.textFile(files.mkString(","))
    val topRelCat = Try(upConfig.getInt("rel_u_ncat")).getOrElse(200)
    val topRelKw = Try(upConfig.getInt("rel_u_nkey")).getOrElse(200)
    val topRelTopic = Try(upConfig.getInt("rel_u_ntopic")).getOrElse(200)
    val topRelEntity = Try(upConfig.getInt("rel_u_nentity")).getOrElse(200)
    val topRelImageLabel = Try(upConfig.getInt("rel_u_nimage_label")).getOrElse(200)
    val top_categories = Try(upConfig.getInt("top_categories")).getOrElse(5)
    val top_keywords = Try(upConfig.getInt("top_keywords")).getOrElse(25)
    val top_topics = Try(upConfig.getInt("top_topics")).getOrElse(20)
    val top_entities = Try(upConfig.getInt("top_entities")).getOrElse(25)
    val top_image_labels = Try(upConfig.getInt("top_image_labels")).getOrElse(20)
    val top_notifyEntities = Try(upConfig.getInt("top_notify_entities")).getOrElse(25)
    val top_Phase1Cat = Try(upConfig.getInt("top_phase1_cat")).getOrElse(3)

    logInfo(s"User feature category key is $categoryKey\ttopRelCat is $topRelCat\ttop_categories is $top_categories")
    logInfo(s"User feature keyword key is $keywordKey\ttopRelKw is $topRelKw\ttop_keywords is $top_keywords")
    logInfo(s"User feature topic key is $topicKey\ttopRelTopic is $topRelTopic\ttop_topics is $top_topics")
    logInfo(s"User feature entity key is $entityKey\ttopRelEntity is $topRelEntity\ttop_entities is $top_entities")
    logInfo(s"User feature image label key is $imageLabelKey\ttopRelImageLabel is $topRelImageLabel\ttop_image_labels is $top_image_labels")
    logInfo(s"User feature notifyEntity key is $notifyEntityKey\ttop_notifyEntities is $top_notifyEntities")
    logInfo(s"User feature top_Phase1Cat is $top_Phase1Cat")

    val userFeature = userProifleLines.map(line => {
      UserFeature.jsonLineParser(line, topRelCat, topRelKw, topRelTopic,topRelEntity, topRelImageLabel, top_categories, top_keywords, top_topics, top_entities, top_image_labels, top_notifyEntities,top_Phase1Cat, categoryKey, keywordKey, topicKey,entityKey, imageLabelKey, notifyEntityKey)
    }).filter(_._1.length > 0)
    userFeature.partitionBy(new HashPartitioner(upConfig.getInt("partitions")))
  }

  /**
    *
    * @param getUserFeatureRDD Option[(dateString, UserProfile)]
    * @param batchEvent (uid, (contentId, batchId, act, value, eventFeature, contentFeature))
    * @return ((uid, contentId), (batchId, act, value, eventFeature, contentFeature, userFeature))
    */
  def joinWithBatchEvent(getUserFeatureRDD: () =>Option[(String, RDD[(String, String)])],
                         batchEvent: DStream[(String, (String, Long, String, Double, String, String))]): DStream[((String, String), (Long, String, Double, String, String, String))] = {
    batchEvent.transform(eventRDD => {
      val dateAndUP = getUserFeatureRDD().get
      val dateString = dateAndUP._1
      val up = dateAndUP._2
      logInfo(s"Now, use user profile of $dateString")
      eventRDD.leftOuterJoin(up).map(result =>
        ((result._1, result._2._1._1), (result._2._1._2, result._2._1._3, result._2._1._4, result._2._1._5, result._2._1._6, result._2._2.getOrElse("")))
      )
    })
  }

  /**
    *
    * @param upConfig Config
    * @param batchEvent (uid, (contentId, batchId, act, value, eventFeature, contentFeature))
    * @return ((uid, contentId), (batchId, act, value, eventFeature, contentFeature, userFeature))
    */
  def getUpFromUFS(upConfig: Config, batchEvent: DStream[(String, (String, Long, String, Double, String, String))]): DStream[((String, String), (Long, String, Double, String, String, String))] = {
    val topRelCat = Try(upConfig.getInt("rel_u_ncat")).getOrElse(200)
    val topRelKw = Try(upConfig.getInt("rel_u_nkey")).getOrElse(200)
    val topRelTopic = Try(upConfig.getInt("rel_u_ntopic")).getOrElse(200)
    val topRelEntity = Try(upConfig.getInt("rel_u_nentity")).getOrElse(200)
    val topRelPkg = Try(upConfig.getInt("rel_u_npkg")).getOrElse(15)
    val topRelNotifyEntity = Try(upConfig.getInt("rel_u_nnotify_entities")).getOrElse(200)
    val topRelSearchEntity = Try(upConfig.getInt("rel_u_nsearch_entity")).getOrElse(200)

    val topRelImageLabel = Try(upConfig.getInt("rel_u_nimage_label")).getOrElse(200)
    val top_categories = Try(upConfig.getInt("top_categories")).getOrElse(5)
    val top_keywords = Try(upConfig.getInt("top_keywords")).getOrElse(25)
    val top_topics = Try(upConfig.getInt("top_topics")).getOrElse(20)
    val top_entities = Try(upConfig.getInt("top_entities")).getOrElse(25)
    val top_notifyEntities = Try(upConfig.getInt("top_notify_entities")).getOrElse(25)

    val top_image_labels = Try(upConfig.getInt("top_image_labels")).getOrElse(20)

    val top_Phase1Cat = Try(upConfig.getInt("top_phase1_cat")).getOrElse(3)
    val top_packages = Try(upConfig.getInt("top_packages")).getOrElse(15)
    val top_searchEntities = Try(upConfig.getInt("top_search_entities")).getOrElse(25)

    val ufs_format = Try(upConfig.getConfig("ufs").getString("format")).getOrElse("pb")
    val ufs_version = Try(upConfig.getConfig("ufs").getInt("version")).getOrElse(6)
    val ufs_version_topic =  Try(upConfig.getConfig("ufs").getInt("versiontp")).getOrElse(6)

    val ufs_namespace = Try(upConfig.getConfig("ufs").getString("namespace")).getOrElse("i")
    val ufs_merge_ns = Try(upConfig.getConfig("ufs").getString("merge_ns")).getOrElse("d,tl")
    val ufs_value = Try(upConfig.getConfig("ufs").getString("value")).getOrElse("k,c,t,e,v").split(",").map(f => s"${f}_$ufs_version").mkString(",")
    val ufs_host = Try(upConfig.getConfig("ufs").getString("host")).getOrElse("http://internal-news-ufs-1653782386.eu-west-1.elb.amazonaws.com/")
    val ufs_path = Try(upConfig.getConfig("ufs").getString("path")).getOrElse("/t/")

    val categoryKey = Try(upConfig.getConfig("ufs").getString("category_key")).getOrElse("c") + s"_$ufs_version"
    val keywordKey = Try(upConfig.getConfig("ufs").getString("keyword_key")).getOrElse("k") + s"_$ufs_version"
    val topicKey = Try(upConfig.getConfig("ufs").getString("topic_key")).getOrElse("t") + s"_$ufs_version_topic"
   //val topicKey = Try(upConfig.getConfig("ufs").getString("topic_key")).getOrElse("t") + s"_$ufs_version"
    val entitykey = Try(upConfig.getConfig("ufs").getString("entity_key")).getOrElse("e") + s"_$ufs_version"

    val imageLabelKey = Try(upConfig.getConfig("ufs").getString("image_label_key")).getOrElse("i") + s"_$ufs_version"

    val notifyEntityKey = Try(upConfig.getConfig("ufs").getString("notify_entity_key")).getOrElse("notify_entities_1")
    val searchEntityKey = Try(upConfig.getConfig("ufs").getString("search_entity_key")).getOrElse("searchentities_1")
    val lenkey = Try(upConfig.getConfig("ufs").getString("len_key")).getOrElse("v") + s"_$ufs_version"
    val catLenkey = Try(upConfig.getConfig("ufs").getString("cat_len_tag")).getOrElse("cat_len")
    val kwLenkey = Try(upConfig.getConfig("ufs").getString("kw_len_tag")).getOrElse("kw_len")
    val topicLenkey = Try(upConfig.getConfig("ufs").getString("topic_len_tag")).getOrElse("topic_len")
    val entityLenkey = Try(upConfig.getConfig("ufs").getString("entity_len_tag")).getOrElse("entity_len")
    val pkgKey = Try(upConfig.getConfig("ufs").getString("package_key")).getOrElse("pkg_1")
    val pkgKeyV2 = Try(upConfig.getConfig("ufs").getString("package_key_v2")).getOrElse("pkg_2")

    val imageLabelLenKey = Try(upConfig.getConfig("ufs").getString("image_label_len_tag")).getOrElse("u_image_label_len")

    logInfo(s"User feature category key is $categoryKey\ttopRelCat is $topRelCat\ttop_categories is $top_categories")
    logInfo(s"User feature keyword key is $keywordKey\ttopRelKw is $topRelKw\ttop_keywords is $top_keywords")
    logInfo(s"User feature topic key is $topicKey\ttopRelTopic is $topRelTopic\ttop_topics is $top_topics")
    logInfo(s"User feature entity key is $entitykey\ttopRelEntity is $topRelEntity\ttop_entities is $top_entities")
    logInfo(s"User feature notifyEntity key is $notifyEntityKey\ttopRelNotifyEntity is $topRelNotifyEntity\ttop_notifyEntities is $top_notifyEntities")
    logInfo(s"User feature searchEntity key is $searchEntityKey\ttopRelSearchEntity is $topRelSearchEntity\ttop_searchEntities is $top_searchEntities")
    logInfo(s"User feaqture imagelabel key is $imageLabelKey\t topRelImageLabel is $top_image_labels")
    logInfo(s"User feature package key is $pkgKey\ttopRelPkg is $topRelPkg\ttop_packages is $top_packages")
    logInfo(s"User feature top_Phase1Cat is $top_Phase1Cat")

    batchEvent.mapPartitions(events =>
      events.map(event => {
        val builder = new URIBuilder()
        //println("builder="+builder)
        builder.setScheme("http")
        builder.setHost(ufs_host).setPath(ufs_path)
        builder.addParameter("androidid", event._1)
        builder.addParameter("format", ufs_format)
        builder.addParameter("ns", ufs_namespace)
        builder.addParameter("merge_ns",ufs_merge_ns)
        builder.addParameter("fields",s"$ufs_value,sc_$ufs_version,$pkgKey,$pkgKeyV2,$notifyEntityKey,$searchEntityKey,$topicKey")
        //println("add param,sethost,setpath complete")
        //val uri = builder.build()
        //println("uri="+uri)
        //val url = ufs_host + ufs_path + s"androidid=${event._1}" + s"&v=2&c=news&format=$ufs_format" + s"&ns=$ufs_namespace" + s"&merge_ns=d,s,v&fields=$ufs_value,sc_$ufs_version"
        val user = Utils.httpGetEntity(builder.build())
        val up = userFeature(user,topRelCat,topRelKw,topRelTopic,topRelEntity,topRelImageLabel,topRelPkg,topRelNotifyEntity,topRelSearchEntity,top_categories,top_keywords,top_topics,top_notifyEntities,top_searchEntities,top_entities,top_image_labels,top_Phase1Cat,top_packages, categoryKey,keywordKey,topicKey,entitykey,imageLabelKey,lenkey,catLenkey,kwLenkey,topicLenkey,entityLenkey,imageLabelLenKey,notifyEntityKey,searchEntityKey,pkgKey, pkgKeyV2)
        ((event._1, event._2._1), (event._2._2, event._2._3, event._2._4, event._2._5, event._2._6, up))
      })
    )
  }

  def userFeature(entity: Option[HttpEntity],
                  topRelCat: Int,
                  topRelKw: Int,
                  topRelTopic: Int,
                  topRelEntity: Int,
                  topRelImageLabel: Int,
                  topRelPkg: Int,
                  topRelNotifyEntity: Int,
                  topRelSearchEntity: Int,
                  topCategories: Int,
                  topKeywords: Int,
                  topTopics: Int,
                  topNotifyEntities: Int,
                  topSearchEntities: Int,
                  topEntities:Int,
                  topImageLabels: Int,
                  topPhase1Cat: Int,
                  topPkg: Int,
                  categoryKey: String,
                  keywordKey: String,
                  topicKey:String,
                  entityKey:String,
                  imageLabelKey: String,
                  lenkey:String,
                  catLenkey:String,
                  kwLenkey:String,
                  topicLenkey:String,
                  entityLenkey:String,
                  imageLabelLenKey: String,
                  notifyEntityKey:String,
                  searchEntityKey:String,
                  pkgKey:String,
                  pkgKeyV2:String): String = {
    if (entity.isEmpty) {
      ""
    } else {
      Try(UserProfile.parseFrom(entity.get.getContent)) match {
        case Success(user) =>
          val feature = EventFeature()
          user.getWeightedTagsList.toList.foreach(f => {
            val version = f.getVersion
            if (version.equals(categoryKey)) {
              val categoriesRel = getUfsFeature(f.getTagList.toList, topRelCat)
              val categorytop3 = topCatOrKw(categoriesRel, topPhase1Cat)
              val categorytop10 = topCatOrKw(categoriesRel, 10)
              val categorytop3single = crossCatTop(categorytop3)
              feature.addFeature("U_CATEGORY_REL", categoriesRel)
              feature.addFeature("U_CATEGORYTOP3",categorytop3)
              feature.addFeature("U_CATEGORYTOP10",categorytop10)
              feature.addFeature("U_CATEGORYTOP3SINGLE",categorytop3single)
              feature.addFeature("U_CATEGORY",topCatOrKw(categoriesRel, topCategories))
            } else if (version.equals(keywordKey)) {
              val keywordsRel = getUfsFeature(f.getTagList.toList, topRelKw)
              feature.addFeature("U_KEYWORD_REL", keywordsRel)
              feature.addFeature("U_KEYWORD",topCatOrKw(keywordsRel, topKeywords))
            } else if (version.equals(topicKey)) {
              val topicsRel = getUfsFeature(f.getTagList.toList, topRelTopic)
              feature.addFeature("U_TOPIC_REL", topicsRel)
              feature.addFeature("U_TOPIC",topCatOrKw(topicsRel, topTopics))
            } else if (version.equals(entityKey)){
              val entityRel = getUfsFeature(f.getTagList.toList, topRelEntity)
              feature.addFeature("U_ENTITY_REL",entityRel)
              feature.addFeature("U_ENTITY",topCatOrKw(entityRel, topEntities))
            } else if (version.equals(imageLabelKey)) {
              val imageLabelRel = getUfsFeature(f.getTagList.toList, topRelImageLabel)
              feature.addFeature("U_IMAGELABEL_REL", imageLabelRel)
              feature.addFeature("U_IMAGELABEL", topCatOrKw(imageLabelRel, topImageLabels))
            }
            else if (version.equals(lenkey)) {
              val lenMap = f.getTagList.toList.map(t => (t.getTag, t.getWeight.toInt)).toMap
              val catLen = lenMap.getOrElse(catLenkey,0).toString
              val kwLen = lenMap.getOrElse(kwLenkey,0).toString
              val topicLen = lenMap.getOrElse(topicLenkey,0).toString
              val entityLen = lenMap.getOrElse(entityLenkey,0).toString
              val imageLabelLen = lenMap.getOrElse(imageLabelLenKey,0).toString
              //val topicLen = lenMap.getOrElse(catLenkey,0)
              feature.addFeature("U_CATLEN",catLen)
              feature.addFeature("U_KWLEN", kwLen)
              feature.addFeature("U_TOPICLEN",topicLen)
              feature.addFeature("U_ENTITYLEN",entityLen)
              feature.addFeature("U_IMAGELABELLEN",imageLabelLen)
            } else if (version.equals(notifyEntityKey)) {
              val notifyEntityRel = getUfsFeature(f.getTagList.toList, topRelNotifyEntity)
              feature.addFeature("U_NOTIFYENTITYREL",notifyEntityRel)
              feature.addFeature("U_NOTIFYENTITY",topCatOrKw(notifyEntityRel, topNotifyEntities))
            } else if(version.equals(pkgKey)) {
              val pkgRel = getUfsFeature(f.getTagList.toList, topRelPkg)
              feature.addFeature("U_PACKAGE", topCatOrKw(pkgRel, topPkg))
            } else if(version.equals(pkgKeyV2)) {
              val pkgRelV2 = getUfsFeature(f.getTagList.toList, topRelPkg)
              feature.addFeature("U_TFIDFPACKGE", topCatOrKw(pkgRelV2, topPkg))
            } else if (version.equals(searchEntityKey)) {
              val searchEntityRel = getUfsFeature(f.getTagList.toList, topRelSearchEntity)
              feature.addFeature("U_SEARCHENTITYREL",searchEntityRel)
              feature.addFeature("U_SEARCHENTITY",topCatOrKw(searchEntityRel, topSearchEntities))
            }
          })
          val gender = if (user.hasGender) user.getGender else ""
          val age = if (user.hasAge) user.getAge else ""
          feature.addFeature("U_AGE",age.toString)
          feature.addFeature("U_GENDER",gender.toString)
          feature.genFeatureString()
        case Failure(ex) =>
          logInfo(ex.getMessage, ex)
          ""
      }
    }
  }

  def getUfsFeature(items: List[WeightedTag], top: Int, default: Float = 0.0f): String = {
    items.map(x => (x.getTag, if (x.hasWeight) x.getWeight else default))
      .sortWith(_._2 > _._2).take(top).map(f => f._1 + Utils.keyGap + f._2).mkString(Utils.pairGap)
  }

  def topCatOrKw(str: String, top: Int) = {
    /*val lh = Try(str.split(gap).length).getOrElse(0)
    Try(str.split(gap).take(if (lh < top) lh else top).mkString(gap)).getOrElse("")*/
    Try(str.split(Utils.pairGap)
      .map(f => {
        val t = f.split(Utils.keyGap)
        (t(0), Try(t(1).toDouble).getOrElse(0.0))
      }).sortWith(_._2 > _._2).take(top).map(f => f._1 + Utils.keyGap + f._2)
      .mkString(Utils.pairGap)
    ).getOrElse("")
  }

  def crossCatTop(str: String) = {
    Try(str.split(Utils.pairGap)
      .map(f => {
        val t = f.split(Utils.keyGap)
        (t(0), Try(t(1).toDouble).getOrElse(0.0))
      }).sortWith(_._2 > _._2).map(f => f._1 )
      .mkString("_")
    ).getOrElse("")
  }

  def extractStr(jvalue: JValue, fieldName: String, default: String = "") = {
    jvalue \ fieldName match {
      case JString(s) => if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString()
      case _ => default
    }
  }


  def catKwAssist(jsonItems: List[JValue], top: Int, default: Double): String = {
    def parse_item(item: JValue) = {
      val name = (item \ "name").asInstanceOf[JString].s
      val L1_weight = item \ "weight" match {
        case JDouble(x) =>
          x.toDouble
        case JInt(x) =>
          x.toDouble
        case JString(x) =>
          Try(x.toDouble).getOrElse(default)
        case _ =>
          default
      }
      //name + Utils.keyGap + L1_weight
      (name, L1_weight)
    }
    //jsonItems.map(x => parse_item(x)).take(top).mkString(Utils.pairGap)
    jsonItems.map(x => parse_item(x))
      .sortWith(_._2 > _._2).take(top).map(f => f._1 + Utils.keyGap + f._2).mkString(Utils.pairGap)
  }


  def getListFromJson(key: String ,line: String, top: Int) : String = {
    val sb = new StringBuilder
    var newLine = line.substring(line.indexOf("\"" + key + "\" : ") + 5 + key.length)
    newLine = newLine.substring(0, newLine.indexOf("]") + 1)
    var index = 0
    breakable {
      while (index < (if (top < 0) Integer.MAX_VALUE else top)) {
        if (newLine.indexOf("}") == -1) {
          break()
        }
        val name = getValueForKey("name", newLine)
        val weight = getValueForKey("weight", newLine).toDouble
        sb.append(name + Utils.keyGap + weight + Utils.pairGap)
        newLine = newLine.substring(newLine.indexOf("}") + 1)
        index += 1
      }
    }
    if (index > 0)
      sb.dropRight(1).toString()
    else
      sb.toString()
  }

  def getValueForKey(key: String, record: String) = {
    val startIndex = record.indexOf("\"" + key + "\" : " ) + 5 + key.length
    val commaIndex = record.indexOf(",", startIndex)
    val antiBracketIndex = record.indexOf("}", startIndex)
    val endIndex = if (commaIndex != -1 && antiBracketIndex != -1)  Math.min(record.indexOf(",", startIndex),
      record.indexOf("}", startIndex)) else if (commaIndex != -1) commaIndex else antiBracketIndex
    val mendedStartInedex =
      if (record.charAt(startIndex) == '\"' || record.charAt(startIndex) == '[')
        startIndex + 1
      else
        startIndex
    val mendedEndInedex =
      if (record.charAt(endIndex - 1) == '\"' || record.charAt(endIndex - 1) == ']')
        endIndex - 1
      else
        endIndex

    if (mendedStartInedex > mendedEndInedex)
      ""
    else
      record.substring(mendedStartInedex, mendedEndInedex)
  }

}
