package com.cmcm.news.monitor.falcon

import org.apache.http.client.methods.HttpPost
import org.apache.http.client.utils.HttpClientUtils
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.util.EntityUtils
import org.apache.spark.Logging

import scala.util.Try
import scala.util.Success
import scala.util.Failure

/**
 * Created by hanbin on 15/11/17.
 */
case class FalconSender(url: String) extends Logging {

  def sendRecord(falconRecord: FalconRecord) = {

    val post = new HttpPost(url)
    //val client = HttpClients.createDefault();
    val client = new DefaultHttpClient()
    //val client = HttpClientBuilder.create().build()
    val body = falconRecord.toJson()

    val postTry = Try({
      val stringEntity = new StringEntity(s"[$body]")
      post.setEntity(stringEntity)
      client.execute(post)
    })

//    logInfo(s"send metrics: $body")

    postTry match {
      case Success(response) => {
        val response = postTry.get
        val entity = response.getEntity
        val res = if (entity != null) {
          Try(EntityUtils.toString(entity, "UTF-8")).getOrElse("Can not parse response!")
        } else {
          "No respose"
        }
//        logInfo(s"Respose from $url : $res")

        //client.close()
        //response.close()
        HttpClientUtils.closeQuietly(client)
        HttpClientUtils.closeQuietly(response)

        if (res.contains("success"))
          true
        else
          false
      }
      case Failure(ex) => {
        //client.close()
        HttpClientUtils.closeQuietly(client)
//        logInfo(s"error when connecting to $url, the reason is ${ex.getMessage}")
        false
      }
    }

  }

}
