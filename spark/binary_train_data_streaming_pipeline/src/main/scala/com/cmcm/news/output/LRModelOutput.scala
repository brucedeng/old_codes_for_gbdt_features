package com.cmcm.news.output

import java.util.Properties

import kafka.producer.{KeyedMessage, Producer, ProducerConfig}
import org.apache.spark.streaming.dstream.DStream
import org.joda.time.DateTime

/**
  * Created by lilonghua on 16/8/24.
  */

class LRModelFileOutput(val outputFilePrefix: String, val window: Int) extends StreamingOutput[((Long,String), (String, String, String, Double, String))] {

  /**
    *
    * @param dStream ((batchId,pid), (uid, cid, label, dwell, featureMap))
    * @param appendedKey
    * @return
    */
  override def output(dStream: DStream[((Long,String), (String, String, String, Double, String))], appendedKey: String = ""): Option[DStream[Int]] = {
    val splitKey = appendedKey.split("_")
    if (splitKey.size > 1 && splitKey(0).equals("multilan")) {
      multilanOutput(dStream, splitKey(1))
    } else {
      normalOutput(dStream, appendedKey)
    }
  }

  def multilanOutput(dStream: DStream[((Long,String), (String, String, String, Double, String))], appendedKey: String = ""): Option[DStream[Int]] = {
    val countries = appendedKey.split(",")
    countries.foreach(countryTag => {
      dStream.foreachRDD((rdd, time) => {
        val datePrefix = new DateTime(time.milliseconds).toString("yyyyMMddHHmm")
          rdd.map(f => generateOutputString(f._2._1 + "\t" + f._2._2 + "\t" + f._2._3 + ":" + f._2._4.toString + "\t" + f._2._5,f._1._2))
          .saveAsTextFile(outputFilePrefix + "/" + countryTag + "/" + datePrefix.substring(0, 8) + "/" + datePrefix.substring(8,10) + "/" + datePrefix.substring(10))
      })
    })
    None
  }

  def normalOutput(dStream: DStream[((Long,String), (String, String, String, Double, String))], appendedKey: String = ""): Option[DStream[Int]] ={
    dStream.foreachRDD((rdd, time) => {
      val datePrefix = new DateTime(time.milliseconds).toString("yyyyMMddHHmm")
      if (appendedKey.nonEmpty) {
        rdd.map(f => generateOutputString(f._2._1 + "\t" + f._2._2 + "\t" + f._2._3 + ":" + f._2._4.toString + "\t" + f._2._5,appendedKey))
          .saveAsTextFile(outputFilePrefix + "/" + appendedKey + "/" + datePrefix.substring(0, 8) + "/" + datePrefix.substring(8,10) + "/" + datePrefix.substring(10))
      } else {
        rdd.map(f => generateOutputString(f._2._1 + "\t" + f._2._2 + "\t" + f._2._3 + ":" + f._2._4.toString + "\t" + f._2._5,appendedKey))
          .saveAsTextFile(outputFilePrefix + "/" + datePrefix.substring(0, 8) + "/" + datePrefix.substring(8,10) + "/" + datePrefix.substring(10))
      }
    })
    None
  }

  /**
    *
    * @param record (feature, batchId, view, click, ?)
    * @param appendedKey a tag
    * @return (batchId \t featureKey \t featureValue \t click \t view)
    */
  def generateOutputString(record: String, appendedKey: String): Any = {
    val outputString = record

    if (appendedKey.nonEmpty)
      outputString + "\t" + appendedKey
    else
      outputString
  }

}


/**
  *
  * @param kafkas (producer_name,broker,producerType,topic)
  */
class LRModelKafkaOutput(val kafkas: Map[String, (String, String, String)]) extends StreamingOutput[((Long,String), (String, String, String, Double, String))] {
  override def output(dStream: DStream[((Long,String), (String, String, String, Double, String))], appendedKey: String = ""): Option[DStream[Int]] = {
    val splitKey = appendedKey.split("_")
    if (splitKey.size > 1 && splitKey(0).equals("multilan")) {
      outputDStream(dStream, splitKey(1), true)
    } else {
      outputDStream(dStream, appendedKey, false)
    }
  }

  def outputDStream(dStream: DStream[((Long,String), (String, String, String, Double, String))],
                    appendedKey: String = "",
                    isMultiLan: Boolean = false): Option[DStream[Int]] = {
    dStream.foreachRDD(rdd => {
      rdd.foreachPartition(partition => {
        val producerList = getProducerList(kafkas)
        if (isMultiLan) {
          partition//.filter(record => record._5.length > 2 && appendedKey.indexOf(record._5.substring(0, 2)) >= 0)
            .foreach(f => {
              sendToKafka(f._2._1 + "\t" + f._2._2 + "\t" + f._2._3 + ":" + f._2._4.toString + "\t" + f._2._5, producerList, f._1._2)
            })
        } else {
          partition.foreach(f => sendToKafka(f._2._1 + "\t" + f._2._2 + "\t" + f._2._3 + ":" + f._2._4.toString + "\t" + f._2._5, producerList, appendedKey))
        }
        producerList.foreach(_._2._2.close())
      })
    })
    None
  }

  def sendToKafka(record: String,
                  producerList: Map[String, (String, Producer[String, String])],
                  appendKey: String): Unit = {
    producerList.get("default")
      .foreach(tuple => {
        val data = new KeyedMessage[String, String](tuple._1, generateOutputString(record, appendKey))
        tuple._2.send(data)
      })
  }

  /**
    *
    * @param kafkas config of kafkas (producer_name,(broker,producerType,topic))
    * @return Map of (producer_name, (topic, producer))
    */
  def getProducerList(kafkas: Map[String, (String, String, String)]): Map[String, (String, Producer[String, String])] = {
    kafkas.map(tuple => {
      val props = new Properties()
      props.put("metadata.broker.list", tuple._2._1)
      props.put("serializer.class", "kafka.serializer.StringEncoder")
      props.put("producer.type", tuple._2._2)
      val config = new ProducerConfig(props)
      val producer = new Producer[String, String](config)
      tuple._1 ->(tuple._2._3, producer)
    })
  }

  /**
    *
    * @param record (feature, batchId, view, click, ?)
    * @param appendedKey
    * @return (feature \t batchId \t view \t click \t appendedKey)
    */
  def generateOutputString(record: String, appendedKey: String = ""): String = {
    if (appendedKey.nonEmpty)
      record + "\t" + appendedKey
    else
      record
  }
}

class LRModelMixedOutput(val outputs: List[StreamingOutput[((Long,String), (String, String, String, Double, String))]]) extends StreamingOutput[((Long,String), (String, String, String, Double, String))] {
  override def output(dStream: DStream[((Long,String), (String, String, String, Double, String))], appendedKey: String = ""): Option[DStream[Int]] = {
    outputs.foreach(output => {
      output.output(dStream, appendedKey)
    })
    None
  }
}