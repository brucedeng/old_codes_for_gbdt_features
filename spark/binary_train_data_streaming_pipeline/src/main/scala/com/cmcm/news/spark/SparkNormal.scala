package com.cmcm.news.spark

import com.cmcm.news.component.Component
import com.cmcm.news.config.{ConfigUnit, Configurable}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by lilonghua on 16/9/9.
  */
object SparkNormal {
  val componentName = "spark_component"
  val configFileName = "spark.conf"
  val defaultAppName = "News_dl lr Spark"
}

/**
  * SparkComponent is used to init and manage spark env for other components.
  * 1, init or recover the spark env
  * 2, provide interface for other components to call the spark service
  */
trait SparkNormal extends Component { this: Configurable =>

  private var _sparkContext: SparkContext =_

  override def init(configDir: String) = {
    super.init(configDir)
    _sparkContext = initSparkComponent(configDir + "/" + SparkComponent.configFileName)
  }

  override def preStart() = {
    super.preStart()
  }

  def sparkContext: SparkContext = {
    _sparkContext
  }

  def sparkConfigUnit = {
    getConfigUnit(SparkComponent.componentName).get
  }

  private def createSparkContext(sparkConfigUnit: ConfigUnit): SparkContext = {
    logInfo("Initializing new spark context...")
    val sparkConf = new SparkConf().setAppName(sparkConfigUnit.getString("spark.app.name",
      SparkComponent.defaultAppName))
    if(sparkConfigUnit.getBoolean("spark.test",false)){
      sparkConf.setMaster(sparkConfigUnit.getString("spark.master","local[1]"))
    }

    new SparkContext(sparkConf)
  }

  def initSparkComponent(sparkConfigFile: String): SparkContext = {
    logInfo("Initializing spark component...")

    val sparkConfigUnit = loadComponentConfig(SparkComponent.componentName, sparkConfigFile)
    createSparkContext(sparkConfigUnit)
  }

}