package com.cmcm.news.analysis

import com.cmcm.news.component.NormalComponent
import com.cmcm.news.config.Configurable
import com.cmcm.news.spark.SparkNormal
import org.apache.spark.storage.StorageLevel

/**
  * Created by lilonghua on 16/9/9.
  */
trait LRFeatureDiff extends NormalComponent {
  this: Configurable with SparkNormal =>

  override def workflowSetup(): Unit = {

    val leftFeat = sparkContext.textFile(modelConfigUnit.getString("model.lr.input.leftFeat", NormalComponent.defaultTenFeatPath))
      .map(f => {
        val ft = f.split("\t")
        ((ft(0), ft(1)), ft(2))
      })
      .reduceByKey((f1,f2) => f1 + ";" + f2)
      .map(f => {
        val (po, ng) = f._2.split(";")
          .map(fs => {
            val s = fs.split(":")
            val p = if (s(0) == "1") 1 else 0
            val n = if (s(0) == "1") 0 else 1
            (p,n)
          }).reduce((s1,s2) => (s1._1 + s2._1, s1._2 + s2._2))
        (f._1,(po, ng, f._2))
      }).persist(StorageLevel.MEMORY_AND_DISK_SER)

    val rightFeat = sparkContext.textFile(modelConfigUnit.getString("model.lr.input.rightFeat", NormalComponent.defaultOneFeatPath))
      .map(f => {
        val ft = f.split("\t")
        ((ft(0), ft(1)), ft(2))
      })
      .reduceByKey((f1,f2) => f1 + ";" + f2)
      .map(f => {
        val (po, ng) = f._2.split(";")
          .map(fs => {
            val s = fs.split(":")
            val p = if (s(0) == "1") 1 else 0
            val n = if (s(0) == "1") 0 else 1
            (p,n)
        }).reduce((s1,s2) => (s1._1 + s2._1, s1._2 + s2._2))
        (f._1,(po, ng, f._2))
      //f._1._1 + "\t" + f._1._2 + "\t" + po.toString + "\t" + ng.toString
      }).persist(StorageLevel.MEMORY_AND_DISK_SER)

    val joinFeat = leftFeat.fullOuterJoin(rightFeat).persist(StorageLevel.MEMORY_AND_DISK_SER)

    joinFeat.filter(f => {
      val t = f._2._1 match {
        case Some(x) => true
        case _ => false
      }

      val o = f._2._2 match {
        case Some(x) => true
        case _ => false
      }

      t && o
    }).map(f => {
      f._1.toString() + "\t" + f._2._1.getOrElse(("","")).toString + "\t" + f._2._2.getOrElse((-1,-1,"")).toString
    }).saveAsTextFile(modelConfigUnit.getString("lmodel.r.out.join.all",NormalComponent.defaultModelOutputFilePrefix))

    joinFeat.filter(f => {
      val t = f._2._1 match {
        case Some(x) => true
        case _ => false
      }

      val o = f._2._2 match {
        case Some(x) => true
        case _ => false
      }

      t && !o
    }).map(f => {
      f._1.toString() + "\t" + f._2._1.getOrElse(("","")).toString
    }).saveAsTextFile(modelConfigUnit.getString("model.lr.out.join.left",NormalComponent.defaultModelOutputFilePrefix))

    joinFeat.filter(f => {
      val t = f._2._1 match {
        case Some(x) => true
        case _ => false
      }

      val o = f._2._2 match {
        case Some(x) => true
        case _ => false
      }

      !t && o
    }).map(f => {
      f._1.toString() + "\t" + f._2._2.getOrElse((-1,-1,"")).toString()
    }).saveAsTextFile(modelConfigUnit.getString("model.lr.out.join.right",NormalComponent.defaultModelOutputFilePrefix))

    leftFeat.filter(f => {
      f._2._1 > 0 && f._2._2 > 0
    }).map(f => {
      f._1.toString() + "\t" + f._2.toString()
    }).saveAsTextFile(modelConfigUnit.getString("model.lr.out.left.more",NormalComponent.defaultModelOutputFilePrefix))

    leftFeat.filter(f => {
      f._2._1 > 1 && f._2._2 == 0
    }).map(f => {
      f._1.toString() + "\t" + f._2.toString()
    }).saveAsTextFile(modelConfigUnit.getString("model.lr.out.left.positive",NormalComponent.defaultModelOutputFilePrefix))

    leftFeat.filter(f => {
      f._2._1 == 0 && f._2._2 > 1
    }).map(f => {
      f._1.toString() + "\t" + f._2.toString()
    }).saveAsTextFile(modelConfigUnit.getString("model.lr.out.left.negative",NormalComponent.defaultModelOutputFilePrefix))

    leftFeat.filter(f => {
      (f._2._1 == 0 && f._2._2 == 1) || (f._2._1 == 1 && f._2._2 == 0)
    }).map(f => {
      f._1.toString() + "\t" + f._2.toString()
    }).saveAsTextFile(modelConfigUnit.getString("model.lr.out.left.only",NormalComponent.defaultModelOutputFilePrefix))

    rightFeat.filter(f => {
      f._2._1 > 0 && f._2._2 > 0
    }).map(f => {
      f._1.toString() + "\t" + f._2.toString()
    }).saveAsTextFile(modelConfigUnit.getString("model.lr.out.right.more",NormalComponent.defaultModelOutputFilePrefix))

    rightFeat.filter(f => {
      f._2._1 > 1 && f._2._2 == 0
    }).map(f => {
      f._1.toString() + "\t" + f._2.toString()
    }).saveAsTextFile(modelConfigUnit.getString("model.lr.out.right.positive",NormalComponent.defaultModelOutputFilePrefix))

    rightFeat.filter(f => {
      f._2._1 == 0 && f._2._2 > 1
    }).map(f => {
      f._1.toString() + "\t" + f._2.toString()
    }).saveAsTextFile(modelConfigUnit.getString("model.lr.out.right.negative",NormalComponent.defaultModelOutputFilePrefix))

    rightFeat.filter(f => {
      (f._2._1 == 0 && f._2._2 == 1) || (f._2._1 == 1 && f._2._2 == 0)
    }).map(f => {
      f._1.toString() + "\t" + f._2.toString()
    }).saveAsTextFile(modelConfigUnit.getString("model.lr.out.right.only",NormalComponent.defaultModelOutputFilePrefix))
  }
}
