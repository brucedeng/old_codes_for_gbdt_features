package com.cmcm.news.util

import redis.clients.jedis.{HostAndPort, JedisCluster}

import collection.JavaConversions._

/**
  * Created by lilonghua on 16/9/27.
  */
class RedisSink (createRedis: () => JedisCluster) extends Serializable {

  lazy val redis = createRedis()

  def get(key: Array[Byte]) : Array[Byte] = { redis.get(key) }

}


object RedisSink {
  def apply(hosts: Set[HostAndPort]): RedisSink = {
    val f = () => {
      val redis = new JedisCluster(hosts)

      sys.addShutdownHook {
        redis.close()
      }

      redis
    }
    new RedisSink(f)
  }
}
