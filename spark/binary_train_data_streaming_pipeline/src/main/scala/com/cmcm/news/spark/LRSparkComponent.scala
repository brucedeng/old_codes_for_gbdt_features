package com.cmcm.news.spark

import com.cmcm.news.component.Component
import com.cmcm.news.config.{ConfigUnit, Configurable}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import scala.collection.immutable.HashMap

/**
  * Created by lilonghua on 2016/11/3.
  */
object LRSparkComponent extends Serializable {
  val componentName = "spark_component"
  val configFileName = "spark.conf"
  val defaultAppName = "News_dl lr Spark Streaming"
  val defaultRecoverMode = false
  val defaultCheckPoint = "/tmp/news_dl_lr_checkpoint/"
  val defaultBatchSeconds = 600
}

trait LRSparkComponent extends Component { this: Configurable =>

  private var _streamingContext: StreamingContext =_
  private var _isNewContext: Boolean =_

  override def init(configDir: String) = {
    super.init(configDir)
    val(streamingContext, isNewContext) = initSparkComponent(configDir + "/" + SparkComponent.configFileName)
    _streamingContext = streamingContext
    _isNewContext = isNewContext
  }

  override def preStart() = {
    super.preStart()
  }

  def streamingContext: StreamingContext = {
    _streamingContext
  }

  def isNewContext: Boolean = {
    _isNewContext
  }

  def sparkContext: SparkContext = {
    _streamingContext.sparkContext
  }

  def sparkConfigUnit = {
    getConfigUnit(SparkComponent.componentName).get
  }

  private def createNewSparkStreamingContext(sparkConfigUnit: ConfigUnit): StreamingContext = {
    logInfo("Initializing new spark streaming context...")
    val sparkConf = new SparkConf()
      .registerKryoClasses(Array(
        /*classOf[BaseFeature],
        classOf[EventFeature],
        classOf[UserFeature],
        classOf[ContentFeature],*/
        classOf[List[(String, Double)]],
        classOf[HashMap[String, Int]],
        classOf[HashMap[String, List[(String,Double)]]],
        classOf[HashMap[String, String]]))
      .set("spark.serializer","org.apache.spark.serializer.KryoSerializer")
      .setAppName(sparkConfigUnit.getString("spark.app.name", SparkComponent.defaultAppName))
    if(sparkConfigUnit.getBoolean("spark.test",false)){
      sparkConf.setMaster(sparkConfigUnit.getString("spark.master","local[1]"))
    }
    val batchSeconds = sparkConfigUnit.getInt("streaming.batch.seconds",
      SparkComponent.defaultBatchSeconds)
    val sparkContext = new SparkContext(sparkConf)
    val streamingContext = new StreamingContext(sparkContext, Seconds(batchSeconds))
    if (sparkConfigUnit.getBoolean("spark.app.checkpointEnable", true)) {
      streamingContext.checkpoint(sparkConfigUnit.getString("spark.app.checkpoint",
        SparkComponent.defaultCheckPoint))
    }
    streamingContext
  }

  def initSparkComponent(sparkConfigFile: String): (StreamingContext, Boolean) = {
    logInfo("Initializing spark component...")

    @volatile var isNewContext: Boolean = false
    val sparkConfigUnit = loadComponentConfig(SparkComponent.componentName, sparkConfigFile)
    val checkpoint = sparkConfigUnit.getString("spark.app.checkpoint",
      SparkComponent.defaultCheckPoint)
    val streamingContext = StreamingContext.getOrCreate(checkpoint,
      () => {
        isNewContext = true
        createNewSparkStreamingContext(sparkConfigUnit)
      })

    (streamingContext, isNewContext)
  }

}