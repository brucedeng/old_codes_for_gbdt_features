package com.cmcm.news.output

import org.apache.spark.Logging
import org.apache.spark.streaming.dstream.DStream

/**
  * Created by lilonghua on 16/8/24.
  */

trait StreamingOutput[T] extends Serializable with Logging {
  def output(dStream: DStream[T], appendedKey: String = ""): Option[DStream[Int]]
}