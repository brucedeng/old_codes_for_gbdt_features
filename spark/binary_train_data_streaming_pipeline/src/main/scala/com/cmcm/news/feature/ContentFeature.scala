package com.cmcm.news.feature

import com.cmcm.news.feature.Profile.{CategoryProfile, EntityProfile, KeywordProfile, TopicProfile}
import com.cmcm.news.util.Utils
import com.typesafe.config.Config
import org.apache.spark.Logging
import org.apache.spark.streaming.dstream.DStream
import redis.clients.jedis.Jedis

import collection.JavaConversions._
import scala.util.Try

/**
  * Created by lilonghua on 2016/11/22.
  */
object ContentFeature extends Logging {

  def getCategories(cc: List[CategoryProfile], topCategories: Int) = {
    cc.map(f => (f.getName, f.getL1Weight))
      .sortWith(_._2 > _._2).take(topCategories)
      .map(f => f._1 + Utils.keyGap + f._2)
      .mkString(Utils.pairGap)
  }

  def getKeywords(ck: List[KeywordProfile], topKeywords: Int) = {
    ck.map(f => (f.getName, f.getL1Weight))
      .sortWith(_._2 > _._2).take(topKeywords)
      .map(f => f._1 + Utils.keyGap + f._2)
      .mkString(Utils.pairGap)
  }

  def getTopics(ct: List[TopicProfile], topTopics: Int) = {
    ct.map(f => (f.getName, f.getL1Weight))
      .sortWith(_._2 > _._2).take(topTopics)
      .map(f => f._1 + Utils.keyGap + f._2)
      .mkString(Utils.pairGap)
  }

  def getEntities(ct: List[EntityProfile], topEntities: Int) = {
    ct.map(f => (f.getName, f.getL1Weight))
      .sortWith(_._2 > _._2).take(topEntities)
      .map(f => f._1 + Utils.keyGap + f._2)
      .mkString(Utils.pairGap)
  }

  def getImageLabels(ci: List[String]) = {
    ci.map(f => (f, 0.05))
      .map(f => f._1 + Utils.keyGap + f._2)
      .mkString(Utils.pairGap)
  }

  def getContentFeature(newsProfile: CmnewsNewsProfile.NewsProfile,
                        topRelCat: Int,
                        topRelKw: Int,
                        topRelTopic: Int,
                        topRelEntity: Int,
                        topRelTitleEntity: Int,
                        topRelDisplayEntity: Int,
                        topCategories: Int,
                        topKeywords: Int,
                        topTopics: Int,
                        topEntities: Int,
                        topTitleEntities: Int,
                        topDisplayEntities: Int,
                        categoryVersion: String,
                        keywordVersion: String,
                        topicVersion: String,
                        entityVersion: String,
                        titleEntityVersion: String,
                        displayEntityVersion: String) = {

    if (newsProfile != null) {
      val cc = if(categoryVersion.length <= 0){
        newsProfile.getCategoriesList.toList
      } else {
        var ccList = List[CategoryProfile]()
        for (category <- newsProfile.getVcategoriesList) {
          if (category.getVersion.equals(categoryVersion)) {
            ccList = category.getCategoriesList.toList
          }
        }
        ccList
      }
      val ccString = getCategories(cc, topCategories)
      val ccRel = getCategories(cc,topRelCat)

      val ck = if(keywordVersion.length <= 0){
        newsProfile.getKeywordsList.toList
      } else {
        var ckList = List[KeywordProfile]()
        for (keyword <- newsProfile.getVkeywordsList) {
          if (keyword.getVersion.equals(keywordVersion)) {
            ckList = keyword.getKeywordsList.toList
          }
        }
        ckList
      }
      val ckString = getKeywords(ck, topKeywords)
      val ckRel = getKeywords(ck, topRelKw)

      val ct = if(topicVersion.length <= 0){
        List[TopicProfile]()
      } else {
        var ctList = List[TopicProfile]()
        for (topic <- newsProfile.getVtopicsList) {
          if (topic.getVersion.equals(topicVersion)) {
            ctList = topic.getTopicsList.toList
          }
        }
        ctList
      }
      val ctString = getTopics(ct, topTopics)
      val ctRel = getTopics(ct, topRelTopic)


      val ce = if(entityVersion.length <= 0){
        List[EntityProfile]()
      } else {
        var ceList = List[EntityProfile]()
        for (entity <- newsProfile.getVentitiesList) {
          if (entity.getVersion.equals(entityVersion)) {
            ceList = entity.getEntitiesList.toList
          }
        }
        ceList
      }
      val ceString = getEntities(ce, topEntities)
      val ceRel = getEntities(ce, topRelEntity)

      val ci = newsProfile.getImageLabelsList()
      val ciString = getImageLabels(ci.toList)
      val ciRel = getImageLabels(ci.toList)

      // ---------------------by cxq---------------------------
      val ctitleEnt = if(titleEntityVersion.length <= 0){
        List[EntityProfile]()
      } else {
        var ctitleEntList = List[EntityProfile]()
        for (titleEnt <- newsProfile.getTitleEntitiesList) {
          if (titleEnt.getVersion.equals(titleEntityVersion)) {
            ctitleEntList = titleEnt.getEntitiesList.toList
          }
        }
        ctitleEntList
      }
      val ctitleEntString = getEntities(ctitleEnt, topTitleEntities)
      val ctitleEntRel = getEntities(ctitleEnt, topRelTitleEntity)
      val ctitleEntitiesLength = ctitleEnt.size


      val cdisplayEnt = if(displayEntityVersion.length <= 0){
        List[EntityProfile]()
      } else {
        var cdisplayEntList = List[EntityProfile]()
        for (displayEnt <- newsProfile.getDisplayEntitiesList) {
          if (displayEnt.getVersion.equals(titleEntityVersion)) {
            cdisplayEntList = displayEnt.getEntitiesList.toList
          }
        }
        cdisplayEntList
      }
      val cdisplayEntString = getEntities(cdisplayEnt, topDisplayEntities)
      val cdisplayEntRel = getEntities(cdisplayEnt, topRelDisplayEntity)
      val cdisplayEntitiesLength = cdisplayEnt.size
      // ---------------------by cxq---------------------------


      val publisher = newsProfile.getPublisher
      val publishTime = newsProfile.getPublishTime
      val groupId = newsProfile.getGroupId
      val titleMd5 = newsProfile.getTitleMd5
      //val updateTime = newsProfile.getUpdateTime
      val tier  = newsProfile.getTier
      val listImageCount = newsProfile.getListImageCount   //  by cxq
      val titleType = newsProfile.getTitleType   //  by cxq
      val titleLength = newsProfile.getTitle.length   //  by cxq
      val quanlity = if(newsProfile.hasQuality) newsProfile.getQuality else Utils.defaultNu.toFloat  //  by cxq

      val wordCount = if (newsProfile.hasWordCount) newsProfile.getWordCount else Utils.defaultNu
      val imageCount = if (newsProfile.hasImageCount) newsProfile.getImageCount else Utils.defaultNu
      val newsyScore = if (newsProfile.hasNewsyScore) newsProfile.getNewsyScore else Utils.defaultNu
      val mediaLevel = if (newsProfile.hasMediaLevel) newsProfile.getMediaLevel else Utils.defaultNu
      val commentCount =  if (newsProfile.hasCommentCount) newsProfile.getCommentCount else Utils.defaultNu
      val hasBigImg = if (newsProfile.hasHasBigImg) Try(newsProfile.getHasBigImg.toInt).getOrElse(0) else Utils.defaultNu

      val feature = EventFeature()
      feature.addFeature("D_KEYWORD", ckString)
      feature.addFeature("D_KEYWORD_REL", ckRel)
      feature.addFeature("D_CATEGORY", ccString)
      feature.addFeature("D_CATEGORY_REL", ccRel)
      feature.addFeature("D_TOPIC", ctString)
      feature.addFeature("D_TOPIC_REL", ctRel)
      feature.addFeature("D_ENTITY",ceString)
      feature.addFeature("D_ENTITY_REL",ceRel)
      feature.addFeature("D_PUBLISHER", publisher)
      feature.addFeature("C_PUBLISHTIME", publishTime.toString)
      feature.addFeature("D_GROUPID", groupId)
      feature.addFeature("D_TITLEMD5", titleMd5)
      feature.addFeature("D_TIER", tier)
      feature.addFeature("D_WORDCOUNT", wordCount.toString)
      feature.addFeature("D_IMAGECOUNT", imageCount.toString)
      feature.addFeature("D_IMAGELABEL", ciString)
      feature.addFeature("D_IMAGELABEL_REL", ciRel)
      feature.addFeature("D_NEWSYSCORE", newsyScore.toString)
      feature.addFeature("D_MEDIALEVEL", mediaLevel.toString)
      feature.addFeature("D_COMMENTCOUNT", commentCount.toString)
      feature.addFeature("D_HASBIGIMG", hasBigImg.toString)
      feature.addFeature("D_LISTIMAGECOUNT", listImageCount)   //  by cxq
      feature.addFeature("D_TITLETYPE", titleType)   //  by cxq
      feature.addFeature("D_TITLELENGTH", titleLength.toString)   //  by cxq
      feature.addFeature("D_QUALITY", quanlity.toString)   //  by cxq
      feature.addFeature("D_TITLEENTITIESCOUNT", ctitleEntitiesLength.toString)   //  by cxq
      feature.addFeature("D_DISPLAYENTITIESCOUNT", cdisplayEntitiesLength.toString)   //  by cxq
      feature.addFeature("D_TITLEENTITY",ctitleEntString)   //  by cxq
      feature.addFeature("D_TITLEENTITY_REL",ctitleEntRel)   //  by cxq
      feature.addFeature("D_DISPLAYENTITY",cdisplayEntString.toLowerCase)   //  by cxq
      feature.addFeature("D_DISPLAYENTITY_REL",cdisplayEntRel.toLowerCase)   //  by cxq

      feature.genFeatureString()
    } else {
      ""
    }
  }

  /**
    *
    * @param batchEvent (contentId, (uid, batchId, act, value, eventFeature))
    * @param contentFeatureConfig cf config
    * @return (uid, (cid, batchId, act, value, eventFeature, contentFeature))
    */
  def getContentFeatureFromRedis(batchEvent: DStream[(String, (String, Long, String, Double, String))], contentFeatureConfig: Config) = {
    //val host:String = contentFeatureConfig.getString("redis_host")
    //val port = contentFeatureConfig.getInt("redis_port")
    val topRelCat = Try(contentFeatureConfig.getInt("rel_d_ncat")).getOrElse(200)
    val topRelKw = Try(contentFeatureConfig.getInt("rel_d_nkey")).getOrElse(200)
    val topRelTopic = Try(contentFeatureConfig.getInt("rel_d_ntopic")).getOrElse(200)
    val topRelEntity = Try(contentFeatureConfig.getInt("rel_d_nentity")).getOrElse(200)
    val topRelTitleEntity = Try(contentFeatureConfig.getInt("rel_d_ntitleentity")).getOrElse(200)   // by cxq
    val topRelDisplayEntity = Try(contentFeatureConfig.getInt("rel_d_ndisplayentity")).getOrElse(200)   // by cxq

    val topCategories = Try(contentFeatureConfig.getInt("top_categories")).getOrElse(5)
    val topKeywords = Try(contentFeatureConfig.getInt("top_keywords")).getOrElse(15)
    val topTopics= Try(contentFeatureConfig.getInt("top_topics")).getOrElse(5)
    val topEntity= Try(contentFeatureConfig.getInt("top_entities")).getOrElse(10)
    val topTitleEntities= Try(contentFeatureConfig.getInt("top_titleentities")).getOrElse(10)   // by cxq
    val topDisplayEntities= Try(contentFeatureConfig.getInt("top_displayentities")).getOrElse(10)   // by cxq

    val categoryVersion = Try(contentFeatureConfig.getString("category_version")).getOrElse("v4")
    val keywordVersion = Try(contentFeatureConfig.getString("keyword_version")).getOrElse("v2")
    val topicVersion = Try(contentFeatureConfig.getString("topic_version")).getOrElse("v2")
    val entityVersion = Try(contentFeatureConfig.getString("entity_version")).getOrElse("v2")
    val titleEntityVersion = Try(contentFeatureConfig.getString("titleentity_version")).getOrElse("v1")   // by cxq
    val displayEntityVersion = Try(contentFeatureConfig.getString("displayentity_version")).getOrElse("v1")   // by cxq

    val serverVersion = Try(contentFeatureConfig.getString("server_version")).getOrElse("v2")

    serverVersion match {
      case "v2" =>
        val host = Try(contentFeatureConfig.getString("cluster_host")).getOrElse("http://10.2.91.77:11000/redis_proxy")
        val limit = Try(contentFeatureConfig.getInt("merge_limit")).getOrElse(1000)
        batchEvent.transform(rdd => {
          rdd.mapPartitions(events => {
            val eventList = events.toList
            logError(s"event's size is ${eventList.size}")
            val distinctCids = eventList.grouped(limit)
            val result = distinctCids.flatMap(listOfCid => {
              val cfList: Map[String, CmnewsNewsProfile.NewsProfile] = {
                Utils.getContentFeature(host, listOfCid.map(f => f._1))
              }
              logError(s"cfList size is ${cfList.size}, cf not empty is ${cfList.count(t=>t._1.nonEmpty)}")
              listOfCid.map(event => {
                val cf = cfList.getOrElse(event._1, null)
                if (cf != null) {
                  logError(s"cf is empty of size < 10")
                }
                (event._2._1, (event._1, event._2._2, event._2._3, event._2._4, event._2._5,
                  getContentFeature(cf, topRelCat, topRelKw, topRelTopic, topRelEntity, topRelTitleEntity, topRelDisplayEntity, topCategories, topKeywords, topTopics, topEntity, topTitleEntities, topDisplayEntities, categoryVersion, keywordVersion, topicVersion, entityVersion, titleEntityVersion, displayEntityVersion)))
              })
            })
            result
          })
        })

      case "v1" =>
        val host: String = contentFeatureConfig.getString("redis_host")
        val port = contentFeatureConfig.getInt("redis_port")
        val slaveOnly = Try(contentFeatureConfig.getBoolean("redis_slave_only")).getOrElse(true)
        val auth = contentFeatureConfig.getString("redis_password")
        batchEvent.transform(rdd => {
          rdd.mapPartitionsWithIndex((partitionNum, events) => {
            def getRedisSlaveClient(hostList: String): Array[Jedis] = {
              val redisHostList: Array[String] = if (hostList.split(",").length > 1) {
                hostList.split(",")
              } else {
                (hostList :: Nil).toArray
              }
              redisHostList.flatMap(eachHost => {
                val client = new Jedis(eachHost, port)
                val role = client.info("replication").split("\n").flatMap(line => {
                  val splits = line.split(":")
                  if (splits.length == 2) {
                    Some(splits(0), splits(1))
                  } else {
                    None
                  }
                }).toMap.getOrElse("role", "none")
                print(role)
                if (role.trim.equals("slave")) {
                  Some(client)
                } else {
                  None
                }
              })
            }

            def getRedisClient(host: String): String = {
              if (host.split(",").length > 1) {
                val redisHostList = host.split(",")
                redisHostList(partitionNum % redisHostList.length)
              } else {
                host
              }
            }

            var result: Iterator[(String, (String, Long, String, Double, String, String))] = Iterator()
            slaveOnly match {
              case true =>
                var redisClientList: Array[Jedis] = null
                try {
                  redisClientList = getRedisSlaveClient(host)
                  val redisClient = redisClientList(partitionNum % redisClientList.length)
                  if (auth.nonEmpty) redisClient.auth(auth)
                  result = events.map(event => {
                    val cf = Utils.getContent(redisClient.get(("n_" + event._1).getBytes()))
                    (event._2._1, (event._1, event._2._2, event._2._3, event._2._4, event._2._5,
                      getContentFeature(cf, topRelCat, topRelKw, topRelTopic, topRelEntity, topRelTitleEntity, topRelDisplayEntity, topCategories, topKeywords, topTopics, topEntity, topTitleEntities, topDisplayEntities, categoryVersion, keywordVersion, topicVersion, entityVersion, titleEntityVersion, displayEntityVersion)))
                  })
                } catch {
                  case e: Exception =>
                    log.error(s"Fail read ContentFeature from redis $host:$port")
                } finally {
                  redisClientList.foreach(_.disconnect())
                }
              case false =>
                var redisClient: Jedis = null
                val redisHost = getRedisClient(host)
                try {
                  redisClient = new Jedis(redisHost, port)
                  if (auth.nonEmpty) redisClient.auth(auth)
                  result = events.map(event => {
                    val cf = Utils.getContent(redisClient.get(("n_" + event._1).getBytes()))
                    (event._2._1, (event._1, event._2._2, event._2._3, event._2._4, event._2._5,
                      getContentFeature(cf, topRelCat, topRelKw, topRelTopic, topRelEntity, topRelTitleEntity, topRelDisplayEntity, topCategories, topKeywords, topTopics, topEntity, topTitleEntities, topDisplayEntities, categoryVersion, keywordVersion, topicVersion, entityVersion, titleEntityVersion, displayEntityVersion)))
                  })
                } catch {
                  case e: Exception =>
                    log.error(s"Fail read ContentFeature from redis $redisHost:$port")
                } finally {
                  redisClient.disconnect()
                }
            }
            result
          })
        })
    }

  }

}
