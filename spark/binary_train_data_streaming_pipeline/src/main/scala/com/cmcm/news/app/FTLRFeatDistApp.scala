package com.cmcm.news.app

import com.cmcm.news.analysis.FTRLFeatureDistribute
import com.cmcm.news.config.Configurable
import com.cmcm.news.spark.SparkNormal

/**
  * Created by lilonghua on 16/9/28.
  */
object FTLRFeatDistApp extends NormalApp
  with Configurable
  with SparkNormal
  with FTRLFeatureDistribute
