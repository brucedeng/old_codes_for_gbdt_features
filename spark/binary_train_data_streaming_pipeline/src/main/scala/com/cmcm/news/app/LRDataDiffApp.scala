package com.cmcm.news.app

import com.cmcm.news.config.Configurable
import com.cmcm.news.model.LRModelDataDiff
import com.cmcm.news.spark.SparkComponent

/**
  * Created by lilonghua on 2016/10/19.
  */
object LRDataDiffApp extends DLMonitorApp
  with Configurable
  with SparkComponent
  with LRModelDataDiff