package com.cmcm.news.component

import com.cmcm.news.config.{ConfigUnit, Configurable}
import com.cmcm.news.parse.Event
import com.cmcm.news.spark.SparkComponent
import com.cmcm.news.util.{ZKStringSerializer, ZKUtils}
import kafka.common.TopicAndPartition
import kafka.message.MessageAndMetadata
import kafka.serializer.StringDecoder
import org.I0Itec.zkclient.ZkClient
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.{LongWritable, Text}
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat
import org.apache.spark.Logging
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.kafka.{HasOffsetRanges, KafkaUtils, OffsetRange}

object EventComponent extends Logging {
  val componentName = "event_component"
  val configFileName = "event.conf"
  val defaultZkquorum = "10.3.6.123:2181"
  val defaultZkoffset = "/news/dl/lr/offset"
  val defaultTopicMap = "spart-topic"
  val defaultGroupId = "news_dl_lr_"
  val defaultReadThread = 4
  val defaultInputSource = "file"
  val defaultInputFileDir = "/tmp/news"
  val defaultInputPartitions = 20
  val defaultReceiverNumPerTopic = 2
  val defaultFetchSize = 3145728
  val defaultMode = "receiver"
  val defaultBroker = "10.10.17.45:9092,10.10.17.46:9092,10.10.14.25:9092"
  var offsetRanges = Array[OffsetRange]()

  def saveKafkaOffsetToZkp(mode: String, zkquorum: String, groupId: String, offsetPath: String)() = {
    if (mode.compareTo("direct") == 0) try {
      val zkClient = new ZkClient(zkquorum, 10000, 10000, ZKStringSerializer)
      val offsetData = offsetRanges.map(offsetRange => {
        offsetRange.topic + "\002" + offsetRange.partition + "\002" + offsetRange.untilOffset
      }).mkString("\001")

      logInfo("offset data : " + offsetData)
      ZKUtils.retry(3)(ZKUtils.updateAppOffsets(zkClient, offsetPath, offsetData))
      offsetRanges = Array[OffsetRange]()
      zkClient.close()
      logInfo("Finish writing offset to zk")
    } catch {
      case e: Throwable =>
        logInfo("update offsets to zkp failed, catch exception : \n" + e.getMessage)
    }
  }
}

/**
  * EventComponent is used to process the events.
  * 1, provide interfaces for other components to process the events
  * 2, According to the event.conf file, we can get the input events
  */
trait EventComponent extends Component {
  this: Configurable with SparkComponent =>

  override def init(configDir: String) = {
    super.init(configDir)
    initEventComponent(configDir + "/" + EventComponent.configFileName)
    Event.initEventParser(eventConfigUnit)
    Event.generateProductIdsBroadcastVar(sparkContext)
    Event.generateSourcesBroadcastVar(sparkContext)
    Event.generateApiVBroadcastVar(sparkContext)
    Event.generateActMappingBroadcastVar(sparkContext)
  }

  override def preStart() = {
    super.preStart()
  }

  def eventConfigUnit: ConfigUnit = {
    getConfigUnit(EventComponent.componentName).get
  }

  def getInputDstream(streamingContext: StreamingContext, eventConfigUnit: ConfigUnit) = {
    val inputSource = eventConfigUnit.getString("event.input.source", EventComponent.defaultInputSource)
    if (inputSource == "file") {
      val inputDir = eventConfigUnit.getString("event.input.file.dir", EventComponent.defaultInputFileDir)
      logInfo(s"init file input dstream from $inputDir")
      streamingContext.fileStream[LongWritable, Text, TextInputFormat](inputDir,
        (t: Path) => true, newFilesOnly = true)
        .map(_._2.toString)
    } else if (inputSource == "kafka") {
      getEventKafkaDstream(streamingContext, eventConfigUnit, sparkConfigUnit)
    } else {
      throw new IllegalStateException("Can't get input dstream from " + eventConfigUnit.getConfigName)
    }
  }

  def initEventComponent(eventConfigFile: String) = {
    logInfo("Initializing event component...")
    loadComponentConfig(EventComponent.componentName, eventConfigFile)
  }

  private def getEventKafkaDstream(ssc: StreamingContext, eventConfigUnit: ConfigUnit, sparkConfigUnit: ConfigUnit): DStream[(String)] = {
    val mode = eventConfigUnit.getString("event.input.kafka.mode", EventComponent.defaultMode)
    val zkquorum = eventConfigUnit.getString("event.input.kafka.zkquorum", EventComponent.defaultZkquorum)
    val topicmap = eventConfigUnit.getString("event.input.kafka.topic", EventComponent.defaultTopicMap)
    val inputPartitions = eventConfigUnit.getInt("event.input.partitions", EventComponent.defaultInputPartitions)
    val fetchSize = eventConfigUnit.getInt("event.input.kafka.fetchSize", EventComponent.defaultFetchSize)
    val beforeMinute = sparkConfigUnit.getInt("streaming.before.minute", 1)
    val afterMinute = sparkConfigUnit.getInt("streaming.after.minute", 1)
    val windowMinute = beforeMinute + 1 + afterMinute
    val batchSeconds = sparkConfigUnit.getInt("streaming.batch.seconds", 60)

    if (mode.compareTo("receiver") == 0) {
      // no use any more
      val groupId = eventConfigUnit.getString("event.input.kafka.groupid", EventComponent.defaultGroupId)
      val readthread = eventConfigUnit.getInt("event.input.kafka.readthread", EventComponent.defaultReadThread)
      val receiverNumPerTopic = eventConfigUnit.getInt("event.input.kafka.receiver_number_per_topic", EventComponent.defaultReceiverNumPerTopic)
      val topicMap = topicmap.split(",").map((_, readthread))
      val kafkaDStreams = topicMap.flatMap(f = (topic: (String, Int)) => {
        (1 to receiverNumPerTopic) map (_ => {
          val groupIdForTopic: String = groupId + topic._1
          val kafkaParams = Map[String, String](
            "zookeeper.connect" -> zkquorum,
            "group.id" -> groupIdForTopic,
            "zookeeper.session.timeout.ms" -> "10000",
            "rebalance.backoff.ms" -> "4000",
            "rebalance.max.retries" -> "10",
            "zookeeper.connection.timeout.ms" -> "10000")
          KafkaUtils.createStream[String, String, StringDecoder, StringDecoder](ssc, kafkaParams, Map(topic._1 -> topic._2 / receiverNumPerTopic), StorageLevel.MEMORY_AND_DISK_SER_2)
        })
      })
      val unionDStream = ssc.union(kafkaDStreams)
      //unionDStream.repartition(inputPartitions).map(line => {
      unionDStream.map(line => {
        line._2
      })
    } else if (mode.compareTo("direct") == 0) {
      val offsetPath = eventConfigUnit.getString("event.input.kafka.offset", EventComponent.defaultZkoffset)
      val broker = eventConfigUnit.getString("event.input.kafka.broker", EventComponent.defaultBroker)
      var partitionOffsets = scala.collection.mutable.Map[TopicAndPartition, Long]()
      try {
        val zkClient = new ZkClient(zkquorum, 10000, 10000, ZKStringSerializer)
        partitionOffsets = ZKUtils.retry(3)(ZKUtils.getTopicsPartitionAndOffsets(zkClient, offsetPath, topicmap.split(",").toSet))
        zkClient.close()
      } catch {
        case e: Throwable => logInfo("Connecting to zk to get offset ranges catch some exceptions:\n " + e.getMessage)
      }

      val kafkaParams = Map[String, String]("metadata.broker.list" -> broker, "fetch.message.max.bytes" -> fetchSize.toString)
      if (partitionOffsets.nonEmpty) {
        logInfo("has partition offsets in zkp")
        KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder, String](ssc, kafkaParams, partitionOffsets.toMap, (mmd: MessageAndMetadata[String, String]) => mmd.message())
          .transform(rdd => {
            EventComponent.offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
            rdd
          }).map((record: String) => record)
            .transform((rdd,time) => {
              rdd.map(data => {
                time.milliseconds.toString + '\t' +  data
              })
            })
          .window(Seconds(windowMinute * batchSeconds), Seconds(batchSeconds))

        //}).map((record: String) => record).repartition(inputPartitions)
      } else {
        logInfo("no partition offsets in zkp")
        KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](ssc, kafkaParams, topicmap.split(",").toSet)
          .transform(rdd => {
            EventComponent.offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
            rdd
          }).map(_._2).transform((rdd,time) => {
          rdd.map(data => {
            time.milliseconds.toString + '\t' +  data
          })
        }).window(Seconds(windowMinute * batchSeconds), Seconds(batchSeconds))
        //}).map(_._2).repartition(inputPartitions)
      }
    } else {
      println("unknown kafka mode")
      sys.exit(-1)
    }

  }

  def getKafkaOffsetSaver() = {
    val mode = eventConfigUnit.getString("event.input.kafka.mode", EventComponent.defaultMode)
    val zkquorum = eventConfigUnit.getString("event.input.kafka.zkquorum", EventComponent.defaultZkquorum)
    val groupId = eventConfigUnit.getString("event.input.kafka.groupid", EventComponent.defaultGroupId)
    val offsetPath = eventConfigUnit.getString("event.input.kafka.offset", EventComponent.defaultZkquorum)
    EventComponent.saveKafkaOffsetToZkp(mode, zkquorum, groupId, offsetPath) _
  }

}
