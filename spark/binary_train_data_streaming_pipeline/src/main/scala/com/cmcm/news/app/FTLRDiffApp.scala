package com.cmcm.news.app

import com.cmcm.news.analysis.FTLRFeatureDiff
import com.cmcm.news.config.Configurable
import com.cmcm.news.spark.SparkNormal

/**
  * Created by lilonghua on 16/9/22.
  */
object FTLRDiffApp extends NormalApp
  with Configurable
  with SparkNormal
  with FTLRFeatureDiff
