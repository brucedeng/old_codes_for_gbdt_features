package com.cmcm.news.util

import org.apache.spark.Logging

/**
 * Created by hanbin on 15/10/26.
 */
case object TaskLogger extends Logging {

  override def logInfo(msg: => String) = {
    super.logInfo(msg)
  }

}
