package com.cmcm.news.feature

import com.cmcm.news.util.Utils

/**
  * Created by hanbin on 16/1/7.
  */
object EventFeature {

  def apply(featureString: String) = {
    val eventFeature = new EventFeature()
    featureString.split(Utils.pairDelimiter).foreach(kvString => {
      val pair = kvString.split(Utils.keyValueDelimiter)
      if (pair.size == 2)
        eventFeature.addFeature(pair(0), pair(1))
    })
    eventFeature
  }

  def apply() = {
    new EventFeature()
  }

}

class EventFeature() {
  var features: Map[String, String] = Map()

  def addFeature(key: String, value: String) = {
    features = features.+((key, value))
    this
  }

  def genFeatureString() = {
    features.map(pair => {
      pair._1 + Utils.keyValueDelimiter + pair._2
    }).mkString(Utils.pairDelimiter)
  }

  def getFeatureValue(key: String) = {
    features.get(key)
  }

}
