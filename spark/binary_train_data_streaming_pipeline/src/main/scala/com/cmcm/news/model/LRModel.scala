package com.cmcm.news.model

import com.cmcm.news.component.{EventComponent, FeatureComponent}
import com.cmcm.news.config.Configurable
import com.cmcm.news.output.StreamingOutput
import com.cmcm.news.processor.LRModelProcessor
import com.cmcm.news.spark.SparkComponent
import org.apache.spark.storage.StorageLevel
import org.joda.time.DateTime

/**
  * Created by lilonghua on 16/8/24.
  */
trait LRModel extends LRModelProcessor {
  this: Configurable with SparkComponent with EventComponent with FeatureComponent  =>

  override def workflowSetup() = {
    val cfbModelOutput: StreamingOutput[((Long,String), (String, String, String, Double, String))] = getCfbModelOutput(modelConfigUnit)

    val debug = modelConfigUnit.getBoolean("model.debug", false)
    val debugPath = modelConfigUnit.getString("model.debugPath", "")

    val eventDStream = getEventDStream()
    val parsedDStream = generalParse(eventDStream)
    val labelDStream = eventLabel(parsedDStream)
    val eventAggregationDStream = eventAggregation(labelDStream)
    val featureStreamWithCP = joinIndiaCP(eventAggregationDStream)
    val featureStreamWithCPAndUP = joinUP(featureStreamWithCP)

    if (debug && debugPath.nonEmpty) {
      featureStreamWithCPAndUP.foreachRDD(r => {
        val datePrefix = new DateTime().toString("yyyyMMddHHmm")
        r.saveAsTextFile(debugPath + "/" + datePrefix.substring(0, 8) + "/" + datePrefix.substring(8,10) + "/" + datePrefix.substring(10))
      })
    }

    val lrbFeatureStream = generateWorldwideLRBFeatures(featureStreamWithCPAndUP)
    val part = modelConfigUnit.getInt("model.lr.partitions", 0)
    //save kafka offset
    val modelStream = (if (part > 1) cfbAggregation(lrbFeatureStream).transform(_.coalesce(part)) else cfbAggregation(lrbFeatureStream)).persist(StorageLevel.MEMORY_AND_DISK_SER)
    cfbModelOutput.output(modelStream, "")
    lrbMetricsOutput(eventDStream, parsedDStream, labelDStream, modelStream)
    modelStream.foreachRDD(rdd => rdd.unpersist())
  }
}