package com.cmcm.news.feature

import com.cmcm.news.config.ConfigUnit
import com.cmcm.news.util.Utils
import com.typesafe.config.Config
import org.apache.spark.streaming.dstream.DStream
import org.joda.time.{DateTime, DateTimeZone}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.Try

/**
  * Created by lilonghua on 16/8/30.
  */
object LRBFeature extends Serializable {

  /**
    *
    * @param fullFeature      ((uid, contentId), (batchId, label, dwell, eventFeature, contentFeature, userFeature))
    * @param cfbFeatureConfig config
    * @return ((batchId,pid), (uid, cid, label, dwell, featureMap))
    */
  def generateLRBFeatureDstream(fullFeature: DStream[((String, String), (Long, String, Double, String, String, String))],
                                cfbFeatureConfig: Config,
                                eventConfig: ConfigUnit): DStream[((Long,String), (String, String, String, Double, String))] = {
    fullFeature.filter(feat => {
      val contentFeature = EventFeature(feat._2._5)
      val wordCount = contentFeature.getFeatureValue("D_WORDCOUNT").getOrElse("0").toDouble
      val trunk = Try(cfbFeatureConfig.getDouble("D_WORDCOUNT")).getOrElse(10000.0d)
      wordCount < trunk
    }).map(feature => genBinaryStaticFeature(feature, cfbFeatureConfig, eventConfig))
  }

  /**
    *
    * @param matchFeatureConfig list of feature should be generated. "M_D_U_KEYWORD_KEYWORD_B90"
    * @param featureMap         (U_KEYWORD, [(CAT,1.0),(DOG,1.0)])
    * @param binaryFeatures         (batchId, act, value)
    */
  def getMatchBinaryFeatures(matchFeatureConfig: List[String],
                             featureMap: mutable.Map[String, List[(String, Double)]],
                             binaryFeatures: ListBuffer[(String, String)],
                             featJoin: String) = {
    matchFeatureConfig.foreach(feature => {
      val fields = feature.split("_")
      val preFeature = fields(1) + "_" + fields(3)
      val posFeature = fields(2) + "_" + fields(4)
      val featureId = fields(5)

      featureMap.getOrElse(preFeature,List()).foreach(value1 => {
        featureMap.getOrElse(posFeature,List()).foreach(value2 => {
          if (value1._1.equals(value2._1)) {
            binaryFeatures += ((featureId + featJoin + value1._1 + featJoin + value1._1, "%.3f".format(value1._2 * value2._2)))
          }
        })
      })
    })
  }

  /**
    *
    * @param crossFeatureConfig list of feature should be generated. "C_D_U_CATEGORY_CATEGORY_B50"
    * @param featureMap         (U_KEYWORD, [(CAT,1.0),(DOG,1.0)])
    * @param binaryFeatures         (batchId, act, value)
    */
  def getCrossBinaryFeatures(crossFeatureConfig: List[String],
                             featureMap: mutable.Map[String, List[(String, Double)]],
                             binaryFeatures: ListBuffer[(String, String)],
                             featJoin: String) = {

    crossFeatureConfig.foreach(feature => {
      var featureList = new ListBuffer[String]
      val fields = feature.split("_")
      var i = 1
      val catNameIndex = fields.length/2
      while(i <= ((fields.length-1)/2)){
        featureList += (fields(i) + "_" + fields(i+catNameIndex-1))
        i = i + 1
      }
      val featureId = fields(fields.length-1)
      getBinaryFeatureValueRecursive(featureMap, featureList.toList, featJoin).foreach(value => {
        binaryFeatures += ((featureId + featJoin + value._1, "%.3f".format(value._2)))
      })
    })
  }

  def getBinaryFeatureValueRecursive(featureMap:mutable.Map[String, List[(String, Double)]],
                                     featureList: List[String],
                                     featJoin: String) : List[(String, Double)] = {
    var featureValues = new ListBuffer[(String, Double)]
    if (featureList.size > 1) {
      val preFeatures = featureMap.getOrElse(featureList.head,List())
      val newFeatureList = featureList.drop(1)
      for (preFeature <- preFeatures) {
        for (posFeature <- getBinaryFeatureValueRecursive(featureMap, newFeatureList, featJoin)) {
          featureValues += ((preFeature._1 + featJoin + posFeature._1, preFeature._2 * posFeature._2))
        }
      }
    } else {
      for (preFeature <- featureMap.getOrElse(featureList.head,List())){
        featureValues += preFeature
      }
    }
    featureValues.toList
  }

  /**
    * @param featureConfig "U_UID_B1"
    * @param featureMap    (U_KEYWORD, [(CAT,1.0),(DOG,1.0)])
    * @param binaryFeatures         (batchId, act, value)
    * @param featJoin    "_"
    */
  def getSingleEdgeBinaryFeatures(featureConfig: List[String],
                                  featureMap: mutable.Map[String, List[(String, Double)]],
                                  binaryFeatures: ListBuffer[(String, String)],
                                  featJoin: String) = {
    featureConfig.foreach(feature => {
      val fields = feature.split("_")
      val featureName = fields(0) + "_" + fields(1)
      val featureId = fields(2)

      featureMap.getOrElse(featureName,List()).foreach(value => {
        binaryFeatures += ((featureId + featJoin + value._1, "%.3f".format(value._2)))
      })
    })
  }

  /**
    *
    * @param featureMap          ((batchId,pid), (uid, cid, label, dwell, featureMap))
    * @param cfbFeatureConfig config from feature.conf -> cfb
    * @return ((batchId,pid), (uid, cid, label, dwell, featureMap))
    */
  def generateLRBFeatures(featureMap: ((Long,String), (String, String, String, Double, mutable.HashMap[String, List[(String,Double)]])),
                          cfbFeatureConfig: Config): ((Long,String), (String, String, String, Double, String)) = {

    val binaryFeatures = new ListBuffer[(String, String)]

    val batchId: Long = featureMap._1._1
    val pid = featureMap._1._2
    val uid = featureMap._2._1
    val cid = featureMap._2._2
    val label: String = featureMap._2._3
    val dwell: Double = featureMap._2._4
    val featSplit = Try(cfbFeatureConfig.getString("feat_split")).getOrElse(" ")
    val featJoin = Try(cfbFeatureConfig.getString("feat_join")).getOrElse("_")
    val replaceSplit = Try(cfbFeatureConfig.getString("replace_split")).getOrElse("_")

    setFeatureConfig(cfbFeatureConfig)
      .filter(_._2.nonEmpty)
      .foreach(feature => {
        feature._1 match {
          case "uf" | "df" => getSingleEdgeBinaryFeatures(feature._2.toList, featureMap._2._5, binaryFeatures, featJoin)
          case "mf" => getMatchBinaryFeatures(feature._2.toList, featureMap._2._5, binaryFeatures, featJoin)
          case "cf" => getCrossBinaryFeatures(feature._2.toList, featureMap._2._5, binaryFeatures, featJoin)
          case _ => None
        }
      })

    //((batchId, pid), (uid, cid, label, dwell, Json.encode(binaryFeatures)))
    ((batchId, pid), (uid, cid, label, dwell, binaryFeatures.map(f => f._1.replace("\n",featSplit).replace(featSplit,replaceSplit)).map(f => if (f.contains(featJoin)) f else f + featJoin).mkString(featSplit)))
  }

  /**
    *
    * @param featureMap
    * @param binConf String
    */
  def binBinaryStaticFeature(featureMap: mutable.HashMap[String, List[(String,Double)]], binConf: String) = {
    val featureConf = binConf.split(",").map(feature_conf => {
      val feature = feature_conf.split(":")(0).split("_").slice(0,2).mkString("_")
      val conf = feature_conf.split(":")(1)
      feature -> conf
    }).toMap
    val featureConfCompatiable = collection.mutable.Map(featureConf.toSeq: _*)
    featureConfCompatiable("D_WORDCOUNT") = featureConfCompatiable.getOrElse("D_WORDCOUNT", "floor 0.1")
    featureConfCompatiable("UD_CATREL") = featureConfCompatiable.getOrElse("UD_CATREL", "floor 1000")
    featureConfCompatiable("UD_KWREL") = featureConfCompatiable.getOrElse("UD_KWREL", "floor 1000")
    for((feature, binMethod) <- featureConfCompatiable) {
      if(featureMap.contains(feature)){
        val originFeature = featureMap(feature)(0)._1
        var binFeature = ""
        //bin method: floor
        if(binMethod.startsWith("floor")){
          val floorCoef = binMethod.split(" ")(1).toFloat
          binFeature = "%.0f".format(math.floor(Try(originFeature.toFloat).getOrElse(0.0f) * floorCoef ))
        }
        //bin method: range
        else{
          val binRange = binMethod.split(" ").map(num => num.toDouble)
          val featureValStr = featureMap(feature)(0)._1
          if(featureValStr != "" && featureValStr.toDouble.toInt != Utils.defaultNu) {
            val l = binRange.toList.zipWithIndex.filter(x => x._1 >= featureValStr.toDouble)
            val featureValBinIndex = binRange.length - l.length
            binFeature = featureValBinIndex.toString
          }
        }
        featureMap(feature) = List((binFeature, featureMap(feature)(0)._2))
      }
    }
  }

  /**
    *
    * @param cfbFeatureConfig config from feature.conf -> cfb
    * @return Sample: CL_PV_U_KEYWORD
    */
  private def setFeatureConfig(cfbFeatureConfig: Config): Map[String, ListBuffer[String]] = {
    val featureConfigMap: Map[String, ListBuffer[String]] = Map(
      "uf" -> ListBuffer[String](),
      "df" -> ListBuffer[String](),
      "cf" -> ListBuffer[String](),
      "mf" -> ListBuffer[String]()
    )

    val coecConf = cfbFeatureConfig.getConfig("coec-feat")
    /*val typeC = coecConf.getString("c")
    val typeEC = coecConf.getString("ec")*/
    featureConfigMap.foreach(feature => {
      feature._2 ++= Try(coecConf.getString(feature._1))
        .getOrElse("")
        .split(",")
        .filter(_.length > 0)
    })

    featureConfigMap
  }

  /**
    *
    * @param feature      ((uid, cid), (batchId, label, dwell, eventFeature, contentFeature, userFeature))
    * @param cfbFeatureConfig config
    * @return ((batchId,pid), (uid, cid, label, dwell, featureMap))
    */
  def genBinaryStaticFeature(feature:((String, String), (Long, String, Double, String, String, String)), cfbFeatureConfig: Config, eventConfig: ConfigUnit) = {
    val featureMap = new mutable.HashMap[String, List[(String,Double)]]
    val eventFeature = EventFeature(feature._2._4)
    val contentFeature = EventFeature(feature._2._5)
    val userFeature = EventFeature(feature._2._6)
    val dCid = feature._1._2
    val uid = feature._1._1
    val binConf = Try(cfbFeatureConfig.getString("feat_bin")).getOrElse("D_WORDCOUNT_B26:floor 0.1,UD_CATREL_B35:floor 1000,UD_KWREL_B36:floor 1000")  //  by cxq get binConf
    val ts = eventFeature.getFeatureValue("SERVER_TIME").getOrElse("0").toLong
    val sTime = eventFeature.getFeatureValue("STIME").getOrElse("0").toLong
    val requestTime = eventFeature.getFeatureValue("REQUEST_TIME").getOrElse("0").toLong
    val psTime = eventFeature.getFeatureValue("PS_TIME").getOrElse("0").toLong
    val eventTime = eventFeature.getFeatureValue("EVENT_TIME").getOrElse("0").toLong
    val rid = eventFeature.getFeatureValue("RID").getOrElse("")
    val srid = eventFeature.getFeatureValue("SRID").getOrElse("")
    val pid = eventFeature.getFeatureValue("PID").getOrElse("11")
    val uCity = eventFeature.getFeatureValue("U_CITY").getOrElse("")
    val position = eventFeature.getFeatureValue("U_POSITION").getOrElse("")
    val net = eventFeature.getFeatureValue("U_NET").getOrElse("")
    val osVersion = eventFeature.getFeatureValue("U_OSVERSION").getOrElse("")
    val appVersion = eventFeature.getFeatureValue("U_APPVERSION").getOrElse("")
    val platform = eventFeature.getFeatureValue("U_PLATFORM").getOrElse("")
    val brand = eventFeature.getFeatureValue("U_BRAND").getOrElse("")
    val model = eventFeature.getFeatureValue("U_MODEL").getOrElse("")
    val channel = eventFeature.getFeatureValue("U_CHANNEL").getOrElse("")
    val isHot = eventFeature.getFeatureValue("U_ISHOT").getOrElse("")
    val appIv = eventFeature.getFeatureValue("U_APPIV").getOrElse("")
    val query_pkg = eventFeature.getFeatureValue("U_QUERYPACKAGE").getOrElse("").split(",").map(x => (x, 1.0))
    val dwell = feature._2._3
    val label = feature._2._2
    val lan = eventFeature.getFeatureValue("U_LAN").getOrElse("")
    val country = eventFeature.getFeatureValue("U_COUNTRY").getOrElse("")
    val src = eventFeature.getFeatureValue("D_SRC").getOrElse("")

    //gmp score and version
    val gmpLowerBound = eventConfig.getDouble("gmp_lower_bound", 0.0)
    val gmpUpperBound = eventConfig.getDouble("gmp_upper_bound", 1.0)
    val gmpInterval = eventConfig.getInt("gmp_interval", 200)
    val gmpScore = eventFeature.getFeatureValue("D_GMP_SCORE").getOrElse("0").toDouble
    val gmpVersion = eventFeature.getFeatureValue("D_GMP_VERSION").getOrElse("4")

    val lanRegion = lan + "_" + country
    val uRelCat = userFeature.getFeatureValue("U_CATEGORY_REL").getOrElse("")
    val uCat = userFeature.getFeatureValue("U_CATEGORY").getOrElse("")
    val uCatTop3 = userFeature.getFeatureValue("U_CATEGORYTOP3").getOrElse("")
    val uCatTop10 = userFeature.getFeatureValue("U_CATEGORYTOP10").getOrElse("")
    val uCatTop3S = userFeature.getFeatureValue("U_CATEGORYTOP3SINGLE").getOrElse("")
    val uRelKey = userFeature.getFeatureValue("U_KEYWORD_REL").getOrElse("")
    val uKey = userFeature.getFeatureValue("U_KEYWORD").getOrElse("")
    val uRelTopic = userFeature.getFeatureValue("U_TOPIC_REL").getOrElse("")
    val uTopic = userFeature.getFeatureValue("U_TOPIC").getOrElse("")
    val uRelEntity = userFeature.getFeatureValue("U_ENTITY_REL").getOrElse("")
    val uEntity = userFeature.getFeatureValue("U_ENTITY").getOrElse("")
    val uRelImageLabel = userFeature.getFeatureValue("U_IMAGELABEL_REL").getOrElse("")
    val uImageLabel = userFeature.getFeatureValue("U_IMAGELABEL").getOrElse("")

    val uRelNotifyEntity = userFeature.getFeatureValue("U_NOTIFYENTITYREL").getOrElse("")
    val uNotifyEntity = userFeature.getFeatureValue("U_NOTIFYENTITY").getOrElse("")
    val uRelSearchEntity = userFeature.getFeatureValue("U_SEARCHENTITYREL").getOrElse("")
    val uSearchEntity = userFeature.getFeatureValue("U_SEARCHENTITY").getOrElse("")

    val uGender = userFeature.getFeatureValue("U_GENDER").getOrElse("")
    val uAge = userFeature.getFeatureValue("U_AGE").getOrElse("")
    val uCatLen = userFeature.getFeatureValue("U_CATLEN").getOrElse("")
    val uKeyLen = userFeature.getFeatureValue("U_KWLEN").getOrElse("")
    val uEntityLen = userFeature.getFeatureValue("U_ENTITYLEN").getOrElse("")
    val uImageLabelLen = userFeature.getFeatureValue("U_IMAGELABELLEN").getOrElse("")

    val uPkg = userFeature.getFeatureValue("U_PACKAGE").getOrElse("")
    val uPkgV2 = userFeature.getFeatureValue("U_TFIDFPACKGE").getOrElse("")

    val uTopicLen = userFeature.getFeatureValue("U_TOPICLEN").getOrElse("")
    val dGroupid = contentFeature.getFeatureValue("D_GROUPID").getOrElse("")
    val dTier = contentFeature.getFeatureValue("D_TIER").getOrElse("")
    val dPublisher = contentFeature.getFeatureValue("D_PUBLISHER").getOrElse("")
    val dPublishTime = contentFeature.getFeatureValue("C_PUBLISHTIME").getOrElse("-1").toLong
    val dRelCat = contentFeature.getFeatureValue("D_CATEGORY_REL").getOrElse("")
    val dCat = contentFeature.getFeatureValue("D_CATEGORY").getOrElse("")
    val dRelKey = contentFeature.getFeatureValue("D_KEYWORD_REL").getOrElse("")
    val dKey = contentFeature.getFeatureValue("D_KEYWORD").getOrElse("")
    val dRelTopic = contentFeature.getFeatureValue("D_TOPIC_REL").getOrElse("")
    val dTopic = contentFeature.getFeatureValue("D_TOPIC").getOrElse("")

    val dRelEntity = contentFeature.getFeatureValue("D_ENTITY_REL").getOrElse("")
    val dEntity = contentFeature.getFeatureValue("D_ENTITY").getOrElse("")

    val dRelImageLabel = contentFeature.getFeatureValue("D_IMAGELABEL_REL").getOrElse("")
    val dImageLabel = contentFeature.getFeatureValue("D_IMAGELABEL").getOrElse("")

    val wordCount = Try(contentFeature.getFeatureValue("D_WORDCOUNT").getOrElse("").toInt).getOrElse(Utils.defaultNu)
    val imageCount = Try(contentFeature.getFeatureValue("D_IMAGECOUNT").getOrElse("").toInt).getOrElse(Utils.defaultNu)
    val newsyScore = Try(contentFeature.getFeatureValue("D_NEWSYSCORE").getOrElse("").toDouble).getOrElse(Utils.defaultNu.toDouble)
    val dTitlemd5 = contentFeature.getFeatureValue("D_TITLEMD5").getOrElse("")
    val mediaLevel = Try(contentFeature.getFeatureValue("D_MEDIALEVEL").getOrElse("").toInt).getOrElse(Utils.defaultNu)
    val commentCount = Try(contentFeature.getFeatureValue("D_COMMENTCOUNT").getOrElse("").toInt).getOrElse(Utils.defaultNu)
    val hasBigImg = Try(contentFeature.getFeatureValue("D_HASBIGIMG").getOrElse("").toInt).getOrElse(Utils.defaultNu)

    val dlistImageCount = contentFeature.getFeatureValue("D_LISTIMAGECOUNT").getOrElse("")
    val dtitleType = contentFeature.getFeatureValue("D_TITLETYPE").getOrElse("")
    val dtitleLen = contentFeature.getFeatureValue("D_TITLELENGTH").getOrElse("0")
    val dRelTitleEntity = contentFeature.getFeatureValue("D_TITLEENTITY_REL").getOrElse("")
    val dTitleEntity = contentFeature.getFeatureValue("D_TITLEENTITY").getOrElse("")
    val dRelDisplayEntity = contentFeature.getFeatureValue("D_DISPLAYENTITY_REL").getOrElse("")
    val dDisplayEntity = contentFeature.getFeatureValue("D_DISPLAYENTITY").getOrElse("")
    val dRelTitleEntityCount = contentFeature.getFeatureValue("D_TITLEENTITIESCOUNT").getOrElse("")
    val dRelDisplayEntityCount = contentFeature.getFeatureValue("D_DISPLAYENTITIESCOUNT").getOrElse("")
    val dquality = contentFeature.getFeatureValue("D_QUALITY").getOrElse("0")


    val catRel = genRelevance(uRelCat,dRelCat)
    val keyRel = genRelevance(uRelKey,dRelKey)
    val notifyRel = genRelevance(uRelNotifyEntity,dRelKey)
    val searchRel = genRelevance(uRelSearchEntity,dRelKey)
    val topicRel = genRelevance(uRelTopic,dRelTopic)
    val entityRel = genRelevance(uRelEntity,dRelEntity)
    val imageLabelRel = genRelevance(uRelImageLabel, dRelImageLabel)

    val titleentityRel = genRelevance(uRelEntity,dRelTitleEntity)
    val displayentityRel = genRelevance(uRelEntity,dRelDisplayEntity)


    //TODO: fix time and doc age feature consist bug
    //val curTime0 = if(psTime <=0) requestTime else psTime
    //val curTime = if(curTime0 <=0 ) ts else curTime0
    val curTime = ts
    val docAge = Try(math.floor((curTime - dPublishTime)/3600.0d).toLong).getOrElse(0l) match {
      case x:Long if x >= 0 => x
      case _ => 0
    }
    val timeTuple = if (curTime <= 0) {
      (-1,-1)
    } else {
      val dateD = new DateTime(curTime * 1000L).withZone(DateTimeZone.UTC)
      (dateD.getHourOfDay, dateD.getDayOfWeek%7)
    }
    val timeOfDay = timeTuple._1
    val dayOfWeek = timeTuple._2

    if(uCatLen.nonEmpty && uCatLen != "unknown") {
      if (uCatLen == "0") {
        featureMap.put("U_CATLEN", List(("-1",1.0)))
      } else {
        featureMap.put("U_CATLEN", List(("%.0f".format(math.floor(Try(uCatLen.toFloat).getOrElse(-1.0f) * 1)),1.0)))
      }
    }else{
      featureMap.put("U_CATLEN", List(("-1",1.0)))
    }
    if(uKeyLen.nonEmpty && uKeyLen != "unknown") {
      if (uKeyLen == "0") {
        featureMap.put("U_KWLEN", List(("0",1.0)))
      } else {
        featureMap.put("U_KWLEN", List(("%.0f".format(math.floor(Try(uKeyLen.toFloat).getOrElse(0.0f) * 0.1)),1.0)))
      }
    }else{
      featureMap.put("U_KWLEN", List(("0",1.0)))
    }
    if(uTopicLen.nonEmpty && uTopicLen != "unknown") {
      if (uTopicLen == "0") {
        featureMap.put("U_TOPICLEN", List(("0",1.0)))
      } else {
        featureMap.put("U_TOPICLEN", List(("%.0f".format(math.floor(Try(uTopicLen.toFloat).getOrElse(0.0f) * 0.1)),1.0)))
      }
    }else{
      featureMap.put("U_TOPICLEN", List(("0",1.0)))
    }


    if(uEntityLen.nonEmpty && uEntityLen != "unknown") {
      if (uEntityLen == "0") {
        featureMap.put("U_ENTITYLEN", List(("0",1.0)))
      } else {
        featureMap.put("U_ENTITYLEN", List(("%.0f".format(math.floor(Try(uEntityLen.toFloat).getOrElse(0.0f) * 0.1)),1.0)))
      }
    }else{
      featureMap.put("U_ENTITYLEN", List(("0",1.0)))
    }

    if (uImageLabelLen.nonEmpty && uImageLabelLen != "unknown") {
      if (uImageLabelLen == "0") {
        featureMap.put("U_IMAGELABELLEN", List(("0",1.0)))
      } else {
        featureMap.put("U_IMAGELABELLEN", List(("%.0f".format(math.floor(Try(uImageLabelLen.toFloat).getOrElse(0.0f) * 0.1)),1.0)))
      }
    }else{
      featureMap.put("U_IMAGELABELLEN", List(("0",1.0)))
    }
    
    if(wordCount != Utils.defaultNu) {
      featureMap.put("D_WORDCOUNT", List((wordCount.toString,1.0)))
    }else{
      featureMap.put("D_WORDCOUNT", List(("",1.0)))
    }
    if(imageCount != Utils.defaultNu) {
      featureMap.put("D_IMAGECOUNT", List(("%.0f".format(math.floor(Try(imageCount.toFloat).getOrElse(0.0f) * 1)),1.0)))
    }else{
      featureMap.put("D_IMAGECOUNT", List(("",1.0)))
    }
    if(catRel >= 0) {
      featureMap.put("UD_CATREL", List((catRel.toString,1.0)))
    }else{
      featureMap.put("UD_CATREL", List(("",1.0)))
    }
    if(keyRel >= 0) {
      featureMap.put("UD_KWREL", List((keyRel.toString,1.0)))
    }else{
      featureMap.put("UD_KWREL", List(("",1.0)))
    }
    if(topicRel >= 0) {
      featureMap.put("UD_TOPICREL", List(("%.0f".format(math.floor(topicRel * 1000)),1.0)))
    }else{
      featureMap.put("UD_TOPICREL", List(("",1.0)))
    }

    if(entityRel >= 0) {
      featureMap.put("UD_ENTITYREL", List(("%.0f".format(math.floor(entityRel * 1000)),1.0)))
    }else{
      featureMap.put("UD_ENTITYREL", List(("",1.0)))
    }

    if(titleentityRel >= 0) {
      featureMap.put("UD_TITLEENTITYREL", List((titleentityRel.toString,1.0)))
    }else{
      featureMap.put("UD_TITLEENTITYREL", List(("",1.0)))
    }
    if(displayentityRel >= 0) {
      featureMap.put("UD_DISPLAYENTITYREL", List((displayentityRel.toString,1.0)))
    }else{
      featureMap.put("UD_DISPLAYENTITYREL", List(("",1.0)))
    }

    if(notifyRel >= 0) {
      featureMap.put("UD_NOTIFYKWREL", List(("%.0f".format(math.floor(notifyRel * 1000)),1.0)))
    }else{
      featureMap.put("UD_NOTIFYKWREL", List(("",1.0)))
    }

    if(searchRel >= 0) {
      featureMap.put("UD_SEARCHKWREL", List(("%.0f".format(math.floor(searchRel * 1000)),1.0)))
    }else {
      featureMap.put("UD_SEARCHKWREL", List(("", 1.0)))
    }

    var score:Int = -1
    if (gmpLowerBound <= gmpScore && gmpScore <= gmpUpperBound) {
      score = math.floor(((gmpScore - gmpLowerBound) / (gmpUpperBound - gmpLowerBound)) * gmpInterval).toInt
    }
    val gmpVal: String = score.toString + "_" + gmpVersion.toString
    featureMap.put("D_GMPSCORE", List((gmpVal, 1.0)))

    if(imageLabelRel >= 0) {
      featureMap.put("UD_IMAGELABELREL", List((imageLabelRel.toString,1.0)))
    }else{
      featureMap.put("UD_IMAGELABELREL", List(("",1.0)))
    }

    if(docAge >= 0) {
      featureMap.put("D_DOCAGE", List((docAge.toString,1.0)))
    }else{
      featureMap.put("D_DOCAGE", List(("",1.0)))
    }
    if(timeOfDay >= 0) {
      featureMap.put("E_TIMEOFDAY", List((timeOfDay.toString,1.0)))
    }else{
      featureMap.put("E_TIMEOFDAY", List(("",1.0)))
    }
    if(dayOfWeek >= 0){
      featureMap.put("E_DAYOFWEEK", List((dayOfWeek.toString,1.0)))
    }else{
      featureMap.put("E_DAYOFWEEK", List(("",1.0)))
    }
    if(ts >= 0){
      featureMap.put("E_SERVERTIME", List((ts.toString,1.0)))
    }else{
      featureMap.put("E_SERVERTIME", List(("",1.0)))
    }
    if(sTime >= 0){
      featureMap.put("E_STIME", List((sTime.toString,1.0)))
    }else{
      featureMap.put("E_STIME", List(("",1.0)))
    }
    if(requestTime >= 0){
      featureMap.put("E_REQUESTTIME", List((requestTime.toString, 1.0)))
    }else{
      featureMap.put("E_REQUESTTIME", List(("",1.0)))
    }
    if(psTime >= 0){
      featureMap.put("E_PSTIME", List((psTime.toString, 1.0)))
    }else{
      featureMap.put("E_PSTIME", List(("", 1.0)))
    }
    if(eventTime >= 0){
      featureMap.put("E_EVENTTIME", List((eventTime.toString, 1.0)))
    }else{
      featureMap.put("E_EVENTTIME", List(("", 1.0)))
    }
    if(lanRegion.nonEmpty && lanRegion != "unknown") {
      featureMap.put("E_LANREGION",List((lanRegion.toLowerCase,1.0)))
    }else{
      featureMap.put("E_LANREGION", List(("",1.0)))
    }
    if(rid.nonEmpty && rid != "") {
      featureMap.put("E_RID", List((rid, 1.0)))
    } else {
      featureMap.put("E_RID", List(("", 1.0)))
    }
    if(srid.nonEmpty && srid != "") {
      featureMap.put("E_SRID", List((srid, 1.0)))
    } else {
      featureMap.put("E_SRID", List(("", 1.0)))
    }
    if(src.nonEmpty && src != "unknown") {
      featureMap.put("D_SRC", List((src, 1.0)))
    } else {
      featureMap.put("D_SRC", List(("", 1.0)))
    }
    if (uid.nonEmpty && uid != "unknown") {
      featureMap.put("U_UID", List((uid,1.0)))
    }else{
      featureMap.put("U_UID", List(("",1.0)))
    }
    if (uCity.nonEmpty && uCity != "unknown") {
      featureMap.put("U_CITY", List((uCity,1.0)))
    }else{
      featureMap.put("U_CITY", List(("",1.0)))
    }
    if (position.nonEmpty && position != "unknown") {
      featureMap.put("U_POSITION", List((position,1.0)))
    }else{
      featureMap.put("U_POSITION", List(("",1.0)))
    }
    if (uGender.nonEmpty && uGender != "unknown") {
      featureMap.put("U_GENDER", List((uGender,1.0)))
    }else{
      featureMap.put("U_GENDER", List(("",1.0)))
    }
    if (uAge.nonEmpty && uAge != "unknown") {
      featureMap.put("U_AGE", List((uAge,1.0)))
    }else{
      featureMap.put("U_AGE", List(("",1.0)))
    }
    if (net.nonEmpty && net != "unknown") {
      featureMap.put("U_NET", List((net,1.0)))
    }else{
      featureMap.put("U_NET", List(("",1.0)))
    }
    if (osVersion.nonEmpty && osVersion != "unknown") {
      featureMap.put("U_OSVERSION", List((osVersion,1.0)))
    }else{
      featureMap.put("U_OSVERSION", List(("",1.0)))
    }
    if (appVersion.nonEmpty && appVersion != "unknown") {
      featureMap.put("U_APPVERSION", List((appVersion,1.0)))
    }else{
      featureMap.put("U_APPVERSION", List(("",1.0)))
    }
    if (platform.nonEmpty && platform != "unknown") {
      featureMap.put("U_PLATFORM", List((platform,1.0)))
    }else{
      featureMap.put("U_PLATFORM", List(("",1.0)))
    }
    if (brand.nonEmpty && brand != "unknown") {
      featureMap.put("U_BRAND", List((brand,1.0)))
    }else{
      featureMap.put("U_BRAND", List(("",1.0)))
    }
    if (model.nonEmpty && model != "unknown") {
      featureMap.put("U_MODEL", List((model,1.0)))
    }else{
      featureMap.put("U_MODEL", List(("",1.0)))
    }
    if (channel.nonEmpty && channel != "unknown") {
      featureMap.put("U_CHANNEL", List((channel,1.0)))
    }else{
      featureMap.put("U_CHANNEL", List(("",1.0)))
    }
    if (isHot.nonEmpty && isHot != "unknown") {
      featureMap.put("U_ISHOT", List((isHot,1.0)))
    }else{
      featureMap.put("U_ISHOT", List(("0",1.0)))
    }
    if (appIv.nonEmpty && appIv != "unknown") {
      featureMap.put("U_APPIV", List((appIv,1.0)))
    }else{
      featureMap.put("U_APPIV", List(("0",1.0)))
    }
    if (uCat.nonEmpty && uCat != "unknown") {
      featureMap.put("U_CATEGORY", uCat.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("U_CATEGORY", List(("",1.0)))
    }

    if (uCatTop3.nonEmpty && uCatTop3 != "unknown") {
      featureMap.put("U_CATEGORYTOP3", uCatTop3.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("U_CATEGORYTOP3", List(("",1.0)))
    }

    if (uCatTop10.nonEmpty && uCatTop10 != "unknown") {
      featureMap.put("U_CATEGORYTOP10", uCatTop10.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("U_CATEGORYTOP10", List(("",1.0)))
    }

    if (uCatTop3S.nonEmpty &&  uCatTop3S != "unknown") {
      featureMap.put("U_CATEGORYTOP3SINGLE", List((uCatTop3S,1.0)))
    }else{
      featureMap.put("U_CATEGORYTOP3SINGLE", List(("",1.0)))
    }

    if (uKey.nonEmpty && uKey != "unknown") {
      featureMap.put("U_KEYWORD", uKey.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("U_KEYWORD", List(("",1.0)))
    }
    if (uTopic.nonEmpty && uTopic != "unknown") {
      featureMap.put("U_TOPIC", uTopic.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("U_TOPIC", List(("",1.0)))
    }


    if (uEntity.nonEmpty && uEntity != "unknown") {
      featureMap.put("U_ENTITY", uEntity.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("U_ENTITY", List(("",1.0)))
    }

    if (uImageLabel.nonEmpty && uImageLabel != "unknown") {
      featureMap.put("U_IMAGELABEL", uImageLabel.split(Utils.pairGap).map(record =>{
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2){
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        }else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("U_IMAGELABEL", List(("",1.0)))
    }
    
    if (uRelImageLabel.nonEmpty && uRelImageLabel != "unknown") {
      featureMap.put("U_RELIMAGELABEL", uRelImageLabel.split(Utils.pairGap).map(record =>{
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2){
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        }else{
        ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("U_RELIMAGELABEL", List(("",1.0)))
    }


    if(uPkg.nonEmpty && uPkg != "unknown") {
      featureMap.put("U_PACKAGE", uPkg.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("U_PACKAGE", List(("",1.0)))
    }

    if(uPkgV2.nonEmpty && uPkgV2 != "unknown") {
      featureMap.put("U_TFIDFPACKGE", uPkgV2.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("U_TFIDFPACKGE", List(("",1.0)))
    }

    if(query_pkg.nonEmpty) {

      featureMap.put("U_QUERYPACKAGE", query_pkg.toList)
    } else {
      featureMap.put("U_QUERYPACKAGE", List(("",1.0)))
    }



    if (uNotifyEntity.nonEmpty && uNotifyEntity != "unknown") {
      featureMap.put("U_NOTIFYENTITY", uNotifyEntity.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("U_NOTIFYENTITY", List(("",1.0)))
    }

    if (uSearchEntity.nonEmpty && uSearchEntity != "unknown") {
      featureMap.put("U_SEARCHENTITY", uSearchEntity.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("U_SEARCHENTITY", List(("",1.0)))
    }

    if (dGroupid.nonEmpty && dGroupid != "unknown") {
      featureMap.put("D_GROUPID", List((dGroupid,1.0)))
    }else{
      featureMap.put("D_GROUPID", List(("",1.0)))
    }
    if (dlistImageCount.nonEmpty && dlistImageCount != "unknown") {
      featureMap.put("D_LISTIMAGECOUNT", List((dlistImageCount,1.0)))
    }else{
      featureMap.put("D_LISTIMAGECOUNT", List(("",1.0)))
    } // by cxq
    if (dtitleType.nonEmpty && dtitleType != "unknown") {
      featureMap.put("D_TITLETYPE", List((dtitleType,1.0)))
    }else{
      featureMap.put("D_TITLETYPE", List(("",1.0)))
    } // by cxq
    if (dtitleLen.nonEmpty && dtitleLen != "unknown") {
      featureMap.put("D_TITLELENGTH", List((dtitleLen,1.0)))
    }else{
      featureMap.put("D_TITLELENGTH", List(("",1.0)))
    } // by cxq
    if (dRelTitleEntityCount.nonEmpty && dRelTitleEntityCount != "unknown") {
      featureMap.put("D_TITLEENTITIESCOUNT", List((dRelTitleEntityCount,1.0)))
    }else{
      featureMap.put("D_TITLEENTITIESCOUNT", List(("",1.0)))
    } // by cxq
    if (dRelDisplayEntityCount.nonEmpty && dRelDisplayEntityCount != "unknown") {
      featureMap.put("D_DISPLAYENTITIESCOUNT", List((dRelDisplayEntityCount,1.0)))
    }else{
      featureMap.put("D_DISPLAYENTITIESCOUNT", List(("",1.0)))
    } // by cxq
    if (dquality.nonEmpty && dquality != "unknown") {
      featureMap.put("D_QUALITY", List((dquality,1.0)))
    }else{
      featureMap.put("D_QUALITY", List(("",1.0)))
    } // by cxq
    if(newsyScore != Utils.defaultNu.toDouble) {
      featureMap.put("D_NEWSYSCORE", List((newsyScore.toString,1.0)))
    }else{
      featureMap.put("D_NEWSYSCORE", List(("",1.0)))
    }  // by cxq
    if (dTier != "" && dTier != "unknown") {
      featureMap.put("D_TIER", List((dTier,1.0)))
    }else{
      featureMap.put("D_TIER", List(("",1.0)))
    }
    if (dTitlemd5.nonEmpty && dTitlemd5 != "unknown") {
      featureMap.put("D_TITLEMD5", List((dTitlemd5,1.0)))
    }else {
      featureMap.put("D_TITLEMD5", List(("",1.0)))
    }
    if (dPublisher.nonEmpty && dPublisher != "unknown") {
      featureMap.put("D_PUBLISHER", List((dPublisher,1.0)))
    }else{
      featureMap.put("D_PUBLISHER", List(("",1.0)))
    }
    if (mediaLevel != Utils.defaultNu) {
      featureMap.put("D_MEDIALEVEL", List((mediaLevel.toString,1.0)))
    }else{
      featureMap.put("D_MEDIALEVEL", List(("",1.0)))
    }
    if (commentCount != Utils.defaultNu) {
      if (commentCount > 100)
        featureMap.put("D_COMMENTCOUNT", List(("5",1.0)))
      else if (commentCount > 50)
        featureMap.put("D_COMMENTCOUNT", List(("4",1.0)))
      else if (commentCount > 10)
        featureMap.put("D_COMMENTCOUNT", List(("3",1.0)))
      else if (commentCount > 0)
        featureMap.put("D_COMMENTCOUNT", List(("2",1.0)))
      else
        featureMap.put("D_COMMENTCOUNT", List(("1",1.0)))
    }else{
      featureMap.put("D_COMMENTCOUNT", List(("",1.0)))
    }
    if (hasBigImg != Utils.defaultNu) {
      featureMap.put("D_HASBIGIMG", List((hasBigImg.toString,1.0)))
    }else{
      featureMap.put("D_HASBIGIMG", List(("",1.0)))
    }
    if (dCat.nonEmpty && dCat != "unknown") {
      featureMap.put("D_CATEGORY", dCat.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0),nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("D_CATEGORY", List(("",1.0)))
    }
    if (dKey.nonEmpty && dKey != "unknown") {
      featureMap.put("D_KEYWORD", dKey.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("D_KEYWORD", List(("",1.0)))
    }
    if (dTopic.nonEmpty && dTopic != "unknown") {
      featureMap.put("D_TOPIC", dTopic.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("D_TOPIC", List(("",1.0)))
    }


    if (dEntity.nonEmpty && dEntity != "unknown") {
      featureMap.put("D_ENTITY", dEntity.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("D_ENTITY", List(("",1.0)))
    }

    if (dImageLabel.nonEmpty && dImageLabel != "unknown") {
      featureMap.put("D_IMAGELABEL", dImageLabel.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        }else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("D_IMAGELABEL", List(("",1.0)))
    }

    if (dCid.nonEmpty && dCid != "unknown") {
      featureMap.put("D_CONID", List((dCid,1.0)))
    } else {
      featureMap.put("D_CONID", List(("",1.0)))
    }
    if (dTitleEntity.nonEmpty && dTitleEntity != "unknown") {
      featureMap.put("D_TITLEENTITY", dTitleEntity.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("D_TITLEENTITY", List(("",1.0)))
    }

    if (dDisplayEntity.nonEmpty && dDisplayEntity != "unknown") {
      featureMap.put("D_DISPLAYENTITY", dDisplayEntity.split(Utils.pairGap).map(record => {
        val nameAndWeight = record.split(Utils.keyGap)
        if (nameAndWeight.size == 2) {
          (nameAndWeight(0), nameAndWeight(1).toDouble)
        } else{
          ("",1.0)
        }
      }).toList)
    }else{
      featureMap.put("D_DISPLAYENTITY", List(("",1.0)))
    }

    if(uKey.nonEmpty && dKey.nonEmpty && uKey != "unknown" && dKey != "unknown")
    {
      val ukeyname=uKey.split(Utils.pairGap).map(record=>{
                              val kv = record.split(Utils.keyGap)
                              if(kv.size==2)
                                kv(0)
                              else
                                ""}).toSet
      val dkeyname=dKey.split(Utils.pairGap).map(record=>{
                              val kv = record.split(Utils.keyGap)
                              if(kv.size==2)
                                kv(0)
                              else
                                  ""}).toSet
      val intersectiom=ukeyname.&(dkeyname).toList.filter(elem=>elem.nonEmpty).map(line=>(line,1.0))
      featureMap.put("UD_KEYWORD", intersectiom)
    }
    else
    {
      featureMap.put("UD_KEYWORD", List(("",1.0)))
    }

    binBinaryStaticFeature(featureMap, binConf)
    generateLRBFeatures(((feature._2._1, pid), (uid, dCid, label,dwell, featureMap)), cfbFeatureConfig)
  }

  def genRelevance(u_input:String,d_input:String):Double = {
    if (u_input == "" || u_input == "unknown" || d_input == "" || d_input == "unknown") {
      0.0
    } else {
      val u_input_dict = u_input.split(Utils.pairGap).map((line:String) => {
        val items = line.split(Utils.keyGap)
        val name = items(0)
        val value = items(1).toDouble
        (name, value)
      }).toMap

      val rel = d_input.split(Utils.pairGap).map((line:String) => {
        val items = line.split(Utils.keyGap)
        val name = items(0)
        val value = items(1).toDouble
        Try(u_input_dict(name)).getOrElse(0.0) * value
      }).sum
      if(rel.isNaN || rel.isInfinity) 0.0
      else rel
    }
  }

  /**
    *
    * @param baseData  ((batchId,pid), (uid, cid, label, dwell, featureMap))
    * @return ((batchId,pid), (uid, cid, label, dwell, featureMap))
    */
  def calculateWeight(baseData: DStream[((Long,String), (String, String, String, Double, String))]): DStream[((Long,String), (String, String, String, Double, String))] = {

    baseData.transform(rdd=> {
      val (spWt: Double, snWt: Double) = Try(rdd.map(x => {
        val label = x._2._3.toInt
        val wt = x._2._4
        val pos_wt = if (label == 1) wt else 0.0d
        val neg_wt = if (label == 1) 0.0d else 1.0d
        (pos_wt, neg_wt)
      }).reduce((x, y) => (x._1 + y._1, x._2 + y._2))).getOrElse((1d,0d))

      val ratio: Double = math.min(snWt / spWt, 10)

      rdd.map(x=>{
        val wt = x._2._4
        val weight: Double = if (x._2._3.toInt == 1) wt * ratio else 1.0d
        (x._1, (x._2._1, x._2._2, x._2._3, weight, x._2._5))})
    })
  }

  def main(args: Array[String]) {

    var featureList = new ListBuffer[String]
    val feature = "C_UD_U_U_KWREL_GENDER_CATEGORY_B214"
    val fields = feature.split("_")
    var i = 1
    val catNameIndex = fields.length/2
    while(i <= ((fields.length-1)/2)){
      featureList += (fields(i) + "_" + fields(i+catNameIndex-1))
      i = i + 1
    }
    val featureId = fields(fields.length-1)

    featureList.foreach(println(_))
    println("featureId =>" + featureId)

    val binConf = "D_WORDCOUNT_B26:floor 0.1,UD_CATREL_B35:0 0.02 0.04 0.20 0.32,UD_KWREL_B36:0 0.015 0.025 0.055 0.075 0.085"
    val featureMap =  new mutable.HashMap[String, List[(String,Double)]]
    featureMap.put("UD_CATREL", List(("0.1", 1.0)))
    featureMap.put("D_WORDCOUNT", List(("300", 1.0)))
    println(featureMap)
    binBinaryStaticFeature(featureMap, binConf)
    println(featureMap)

  }

}
