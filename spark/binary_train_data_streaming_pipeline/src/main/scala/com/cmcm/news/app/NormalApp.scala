package com.cmcm.news.app

import com.cmcm.news.component.NormalComponent
import com.cmcm.news.config.Configurable
import com.cmcm.news.spark.SparkNormal

import scala.collection.mutable

/**
  * Created by lilonghua on 16/9/9.
  */

trait NormalApp {
  this: Configurable with SparkNormal with NormalComponent =>
  case class Config(cp: String = "")

  def defineParser() = {
    val parser = new scopt.immutable.OptionParser[Config]("DL_NORMAL@DataHero", "0.1") {
      def options = mutable.Seq(
        opt("c", "config", "config folder") { (v: String, c: Config) => c.copy(cp = v) }
      )
    }
    parser
  }

  def initApp(configDir: String) = {
    init(configDir)
  }

  def start() = {

    logInfo("Prestarting...")
    preStart()
    logInfo("The App start to run.")

    workflowSetup()
  }

  def main(args: Array[String]) {
    val parser = defineParser()
    val configDir = parser.parse(args, Config()) match {
      case Some(config) => {
        val configDir = config.cp
        logInfo(s"DL App will load configures from $configDir")
        configDir
      }
      case None =>
        logWarning("DL App wouldn't load configures" + args.mkString("\t"))
        ""
    }
    initApp(configDir)
    start()
  }
}