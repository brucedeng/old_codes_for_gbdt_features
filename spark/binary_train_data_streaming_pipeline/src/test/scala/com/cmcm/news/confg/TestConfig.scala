package com.cmcm.news.confg

import com.cmcm.news.config.ConfigUnit
import org.scalatest.FunSuite

import scala.util.{Failure, Success, Try}

/**
  * Created by lilonghua on 2016/11/7.
  */
class TestConfig extends FunSuite {
  test("test config") {
    Try(ConfigUnit("feature_component", "conf/us/us_cm_v4_lr_v1/conf/feature.conf")) match {
      case Success(conf) => {
        val up = conf.config.getConfig("feature").getConfig("up").getString("input_dir")
        println(up)
      }
      case Failure(err) =>
        println(err.getMessage)
    }
  }
}
