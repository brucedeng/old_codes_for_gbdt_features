package com.cmcm.news.processor

import com.cmcm.news.feature.UserFeature
import org.scalatest.FunSuite

/**
  * Created by lilonghua on 2016/11/7.
  */
class TestUserFeature extends FunSuite {
  test("test UserFeature") {
    val up = """{"neg_decalre_categories":[],"gender":"","uid":"2257b13c54de9","version":"3","u_cat_len":17,"entities_v2":[{"weight":0.005251,"name":"दिल"},{"weight":0.005162,"name":"अनिल_कपूर"},{"weight":0.005147,"name":"पेनिस"},{"weight":0.005143,"name":"अमिताभ_बच्चन"},{"weight":0.005102,"name":"टोटके"},{"weight":0.005085,"name":"अपनाएं_ये"},{"weight":0.005069,"name":"रामदेव"}],"categories":[{"weight":0.144311,"name":"1000032"},{"weight":0.10994,"name":"1000404"},{"weight":0.087424,"name":"1000267"},{"weight":0.0867,"name":"1000300"},{"weight":0.081413,"name":"1000288"},{"weight":0.079055,"name":"1000374"},{"weight":0.06631,"name":"1000294"},{"weight":0.065878,"name":"1000540"},{"weight":0.050088,"name":"1000671"},{"weight":0.048885,"name":"1000296"},{"weight":0.038605,"name":"1000426"},{"weight":0.034381,"name":"1000963"},{"weight":0.033612,"name":"1000395"},{"weight":0.030131,"name":"1000667"},{"weight":0.019414,"name":"1000681"},{"weight":0.018073,"name":"1000121"},{"weight":0.005779,"name":"1000780"}],"neg_decalre_keywords":[],"aid":"2257b13c54de9","neg_decalre_keywords_hindi":[],"age":"","neg_decalre_categories_hindi":[],"u_kw_len":1058}"""
    val user = UserFeature.jsonLineParser(up,200,200,200,200,200,15,25,15,25,20,3,4,"categories","keywords","topics","entities_v2","image_label","notify_entities")
    println(user)
      }
}
