package com.ijinshan.cmcm.gbdt.runtime.model;

import org.apache.hadoop.util.hash.Hash;
import org.junit.Assert;
import org.mockito.Mockito;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by tanghuaidong on 8/5/15.
 */
public class GbdtModelTest extends Mockito {

    private GbdtModel gbdtModel;
    private String modelFile;
    private String featureFile;
    private String predFile;
    private String testDataFile;

    @BeforeClass
    public void init(){
        this.featureFile = GbdtModelTest.class.getResource("/feature.map").getPath();
        this.predFile = GbdtModelTest.class.getResource("/pred.txt").getPath();
        this.testDataFile = GbdtModelTest.class.getResource("/test_score_raw_data.txt").getPath();
        this.modelFile = GbdtModelTest.class.getResource("/test.model.dump").getPath();
        gbdtModel = new GbdtModel();
    }

    @Test
    public void loadTest() throws IOException, FileNotFoundException{
        gbdtModel.load(modelFile);
   //     gbdtModel.printTrees();
    }

    @Test
    public void scoringTest() throws IOException{
        gbdtModel.load(modelFile);
    //    gbdtModel.printTrees();
        List<Double> predScore = new ArrayList<Double>();
        List<Double> cmpScore = new ArrayList<Double>();
        List<String> featureName = new ArrayList<String>();

        BufferedReader br = new BufferedReader(new FileReader(featureFile));
        String line = null;
        try {
            while ((line = br.readLine()) != null){
                String[] slices = line.trim().split("\t");
                featureName.add(slices[1]);
            }
        }catch(IOException e){
            System.out.println("failed to read feature file");
        }

        br = new BufferedReader(new FileReader(predFile));
        try {
            while ((line = br.readLine()) != null){
                String[] slices = line.trim().split("\t");
                predScore.add(Double.parseDouble(slices[0]));
            }
        }catch(IOException e){
            System.out.println("failed to read feature file");
        }

        br = new BufferedReader(new FileReader(testDataFile));
        try {
            while ((line = br.readLine()) != null){
                Map<String, Double> tmpTestData = new HashMap<String, Double>();
                String[] slices = line.trim().split(" ");
                for(int i = 1; i < slices.length; i++){
                    String[] innerSlices = slices[i].trim().split(":");
                    tmpTestData.put(featureName.get(i-1), Double.parseDouble(innerSlices[1]));
                }
                cmpScore.add(gbdtModel.scoring(tmpTestData));
            }
        }catch(IOException e){
            System.out.println("failed to read feature file");
        }


        for(int i = 0; i < cmpScore.size(); i ++){
//            System.out.printf("expectedScore:%f\tactualScore:%f\n",predScore.get(i).doubleValue(), cmpScore.get(i).doubleValue());
        }
    }
}
