package com.ijinshan.cmcm.gbdt.runtime.model;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by mengchong on 1/19/16.
 */
public class MulticlassGbdtModelTest {
    private MulticlassGbdtModel gbdtModel;
    private String modelFile;
    private String featureFile;
    private String predFile;
    private String testDataFile;
    private int num_class = 6;

    @BeforeClass
    public void init(){
        this.featureFile = GbdtModelTest.class.getResource("/multiclass_feature.map").getPath();
        this.predFile = GbdtModelTest.class.getResource("/multiclass_pred.txt").getPath();
        this.testDataFile = GbdtModelTest.class.getResource("/multiclass_test_data.txt").getPath();
        this.modelFile = GbdtModelTest.class.getResource("/multiclass_model.dump").getPath();
        gbdtModel = new MulticlassGbdtModel(num_class);
    }

    @Test
    public void scoringTest() throws IOException{
        gbdtModel.load(modelFile);
        //    gbdtModel.printTrees();
        List<List<Double>> predScore = new ArrayList<List<Double>>();
        List<double[]> cmpScore = new ArrayList<double[]>();
        Map<Integer,String> featureName = new HashMap<Integer, String>();

        BufferedReader br = new BufferedReader(new FileReader(featureFile));
        String line = null;
        try {
            while ((line = br.readLine()) != null){
                String[] slices = line.trim().split("\t");
                featureName.put(Integer.parseInt(slices[0]), slices[1]);
            }
        }catch(IOException e){
            System.out.println("failed to read feature file");
        }

        br = new BufferedReader(new FileReader(predFile));
        try {
            int cnt = 0;
            List<Double> curRes = null;
            while ((line = br.readLine()) != null){
                String[] slices = line.trim().split("\t");
                if(cnt % num_class ==0 ){
                    if(curRes!=null) {
                        predScore.add(curRes);
                    }
                    curRes = new ArrayList<Double>(6);
                }
                curRes.add(Double.parseDouble(slices[0]));
                cnt ++;
            }
            if(curRes.size()!= 0 ) predScore.add(curRes);
        }catch(IOException e){
            System.out.println("failed to read pred file");
        }

        System.out.println(testDataFile);
        br = new BufferedReader(new FileReader(testDataFile));
        try {
            while ((line = br.readLine()) != null){
                Map<String, Double> tmpTestData = new HashMap<String, Double>();
                String[] slices = line.trim().split(" ");
                for(int i = 1; i < slices.length; i++){
                    String[] innerSlices = slices[i].trim().split(":");
                    tmpTestData.put(featureName.get(Integer.parseInt(innerSlices[0])), Double.parseDouble(innerSlices[1]));
                }
                cmpScore.add(gbdtModel.multiScoring(tmpTestData));
            }
        }catch(IOException e){
            System.out.println("failed to read data file");
        }


        for(int i = 0; i < cmpScore.size(); i ++){
            List<Double> baseline = predScore.get(i);
            double[] test = cmpScore.get(i);
            for(int j=0; j<baseline.size(); j++){
                Assert.assertEquals(baseline.get(j),test[j],0.0001);
            }
        }
    }
}
