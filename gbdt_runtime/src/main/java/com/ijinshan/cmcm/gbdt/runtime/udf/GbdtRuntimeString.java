package com.ijinshan.cmcm.gbdt.runtime.udf;

/**
 * Created by tanghuaidong on 5/8/15.
 */

import com.ijinshan.cmcm.gbdt.runtime.model.GbdtModel;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class GbdtRuntimeString extends EvalFunc<Double> {

    public GbdtRuntimeString(String modelFile, String featureMapFile) {
        if(null == modelFile){
            LOG.error("[gbdt runtime] model file is invalid.");
        }

        gbdtModel = new GbdtModel();
        gbdtModel.load(modelFile);
        featureList = new ArrayList<String>();

        if(null == featureMapFile){
            LOG.error("[gbdt runtime] feature map file is invalid");
        }

        try {
            BufferedReader br = new BufferedReader(new FileReader(featureMapFile));
            String line = null;
            try {
                while ((line = br.readLine()) != null) {
                    String[] slices = line.split("\t");
                    if (slices.length != 3) {
                        LOG.debug("[gbdt runtime] feature line is in valid: " + line);
                    }else{
                        this.featureList.add(slices[1]);
                    }
                }
            }catch(IOException e){
                LOG.error("[gbdt runtime] read line from feature map file is invalid");
            }
        }catch(FileNotFoundException e){
            LOG.error("[gbdt runtime] feature map file is invalid");
        }
    }

    @Override
    public Double exec(Tuple input) throws IOException {
        if(input == null || input.size() == 0)
            return -1.0;
        String featureString = (String)input.get(0);
        if(featureString == null)
            return -1.0;

        String[] featureValues = featureString.split("\t");
        if(featureValues.length != featureList.size()){
            return -1.0;
        }

        Map<String, Double> feamap = new HashMap<String, Double>();

        int index = 0;
        for(String feature: featureList){
            feamap.put(feature,Double.parseDouble(featureValues[index]));
            index++;
        }
        double response = 0;
        response = gbdtModel.scoring(feamap);
        return response;
    }


    private GbdtModel gbdtModel;
    private List<String> featureList;
    private static Logger LOG = LoggerFactory.getLogger(GbdtRuntimeMap.class);

}


