package com.ijinshan.cmcm.gbdt.runtime.udf;

/**
 * Created by tanghuaidong on 5/8/15.
 */

import com.ijinshan.cmcm.gbdt.runtime.model.GbdtModel;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;


public class GbdtRuntimeMap extends EvalFunc<Double> {

    public GbdtRuntimeMap(String modelFile) {
        if(null == modelFile){
            LOG.error("[gbdt runtime] model file is invalid.");
        }

        gbdtModel = new GbdtModel();
        gbdtModel.load(modelFile);
    }

    @Override
    public Double exec(Tuple input) throws IOException {
        if(input == null || input.size() == 0)
            return -1.0;
        Map<String, Double> feamap = (Map<String, Double>)input.get(0);
        if(feamap == null)
            return -1.0;

        double response = 0;
        response = gbdtModel.scoring(feamap);
        return response;
    }


    private GbdtModel gbdtModel;
    private static Logger LOG = LoggerFactory.getLogger(GbdtRuntimeMap.class);

}


