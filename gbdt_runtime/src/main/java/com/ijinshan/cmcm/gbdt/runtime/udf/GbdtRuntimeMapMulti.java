package com.ijinshan.cmcm.gbdt.runtime.udf;

import com.ijinshan.cmcm.gbdt.runtime.model.GbdtModel;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

/**
 * Created by mengchong on 5/11/15.
 */
public class GbdtRuntimeMapMulti extends EvalFunc<Double> {
    public GbdtRuntimeMapMulti(String modelPath) {
        if(null == modelPath){
            LOG.error("[gbdt runtime] model path is invalid.");
        }
        System.err.println("model path is " + modelPath);

        File folder = new File(modelPath);
        File[] listOfFiles = folder.listFiles();
        System.err.println();
        models = new HashMap<String, GbdtModel>();

        for (File file : listOfFiles) {
            String fname = file.getName();
            String model_file_path = file.getAbsolutePath();
            System.err.println("model file path is " + model_file_path);
            System.err.println("parsing model " + fname);
            if(fname==null || (!fname.endsWith(".dump"))){
                System.err.println("Error file extention. needed a .dump file. file name is " + fname);
                continue;
            }

            if (file.isFile()) {
                System.out.println("opening model file" + file.getName());
                String modelName = fname.replace(".dump","");
                GbdtModel oneModel = new GbdtModel();
                oneModel.load(model_file_path);
                models.put(modelName, oneModel);
            }
        }
        System.err.println("model path is " + modelPath);

    }

    @Override
    public Double exec(Tuple input) throws IOException {
        double result;
        if(input == null || input.size() == 0)
            return null;
        Map<String, Double> feamap = (Map<String, Double>)input.get(0);
        String modelName = (String)input.get(1);

        if(feamap == null)
            return null;
        if(modelName == null)
            return null;

        GbdtModel currentModel = models.get(modelName);
        if ( currentModel == null){
            return null;
        }

        result = currentModel.scoring(feamap);

        return result;
    }

    private Map<String,GbdtModel> models;
    private static Logger LOG = LoggerFactory.getLogger(GbdtRuntimeMap.class);
}
