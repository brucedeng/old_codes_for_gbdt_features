package com.ijinshan.cmcm.gbdt.runtime.util;

/**
 * Created by tanghuaidong on 7/5/15.
 */
public class TreeNode {

    private TreeNode leftNode;
    private TreeNode rightNode;
    private String feature_name;
    private double value;
    private boolean isMissingEqualYes;
    private int nodeId;

    public TreeNode(){
        super();
        this.leftNode = null;
        this.rightNode = null;
        this.feature_name = null;
        this.value = Double.MAX_VALUE;
        this.isMissingEqualYes = false;
    }

    public TreeNode getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(TreeNode leftNode) {
        this.leftNode = leftNode;
    }

    public TreeNode getRightNode() {
        return rightNode;
    }

    public void setRightNode(TreeNode rightNode) {
        this.rightNode = rightNode;
    }

    public String getFeature_name() {
        return feature_name;
    }

    public void setFeature_name(String feature_name) {
        this.feature_name = feature_name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public boolean isMissingEqualYes() {
        return isMissingEqualYes;
    }

    public void setIsMissingEqualYes(boolean isMissingEqualYes) {
        this.isMissingEqualYes = isMissingEqualYes;
    }

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

}
