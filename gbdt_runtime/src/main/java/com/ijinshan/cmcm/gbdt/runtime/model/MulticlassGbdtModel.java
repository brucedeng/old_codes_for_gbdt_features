package com.ijinshan.cmcm.gbdt.runtime.model;

import com.ijinshan.cmcm.gbdt.runtime.util.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mengchong on 1/19/16.
 */
public class MulticlassGbdtModel {
    private static Logger LOG = LoggerFactory.getLogger(MulticlassGbdtModel.class);
    private int num_class;
    public MulticlassGbdtModel(int num_class){
        this.trees = new ArrayList<TreeNode>();
        this.num_class = num_class;
    }

    public int load(String modelFileName){
        int loadCode = loadModel(modelFileName);
        if ( loadCode != 0) return loadCode;
        else {
            int model_trees = trees.size();
            if (model_trees % num_class != 0) {
                LOG.error(String.format("invalid number of trees. must be a multiple of %d",num_class));
                return -1;
            }
            else return 0;
        }
    }

    public double scoring(Map<String, Double> features){
        double sumScore = 0.0;
        if (null == features || features.size() <= 0) {
            return sumScore;
        }

        for(TreeNode tree: this.trees){
            sumScore += scoring(tree, features);
        }

        return 1.0/(1.0 + Math.exp(-sumScore));
    }

    public double[] multiScoring(Map<String, Double> features){
        double[] results = new double[num_class];
        double sumScore = 0.0;
        if (null == features || features.size() <= 0) {
            return null;
        }

        int round = trees.size()/num_class;

        for(int i=0;i<round;i++){
            for (int j=0;j<num_class; j++){
                results[j] += scoring(trees.get(i * num_class + j), features);
            }
        }

        double sum=0;
        for(int i=0;i<results.length;i++){
            results[i] = Math.exp(results[i]);
            sum += results[i];
        }
        for(int i=0;i<results.length;i++) results[i]=results[i] / sum;

        return results;
    }

    private static Pattern pattern = Pattern.compile("yes=(\\d+),no=(\\d+),missing=(\\d+)");

    public int loadModel(String modelFileName) {
        if(modelFileName.length() < 5 || !modelFileName.endsWith(".dump")){
            LOG.error("[gbdt] invalid file name: " + modelFileName);
            return -1;
        }

        try {
            BufferedReader br = new BufferedReader(new FileReader(modelFileName));
            String line = null;
            Map<Integer, TreeNode> nodeIdTreeMap = new HashMap<Integer, TreeNode>();
            try {
                while ((line = br.readLine()) != null){
                    line = line.trim();
                    if(line.isEmpty()){
                        continue;
                    }

                    if(line.startsWith("booster[")) {
                        nodeIdTreeMap.clear();
                        int ret = parse_tree(br, nodeIdTreeMap);
                        if (ret != 0) {
                            LOG.error("[gbdt] failed to parse tree, index = " + trees.size());
                            return -1;
                        }
                        if (nodeIdTreeMap.size() > 1) {
                            this.trees.add(nodeIdTreeMap.get(0));
                        } else {
                            LOG.error("[gbdt] failed to parse tree, index = " + trees.size());
                            return -1;
                        }
                    }else{
                        return -1;
                    }
                }
            }catch(IOException e){
                LOG.error("[gbdt] Failed to read line from model file");
                return -1;
            }
            return 0;
        }catch(FileNotFoundException e){
            LOG.error("[gbdt] File Not found");
            LOG.error(e.toString());
            return -1;
        }
    }

    public int parse_tree(BufferedReader br, Map<Integer, TreeNode> nodeIdTreeMap) {
        String line = null;
        try {
            line = br.readLine();
            line = line.trim();
        }catch(IOException e){
            LOG.error("[gbdt] failed to read line from model");
            return -1;
        }

        String[] slices = line.split(":");
        if(slices.length !=2){
            LOG.error("[gbdt] invalid line: " + line);
            return -1;
        }

        int nodeId = Integer.parseInt(slices[0]);
        String tmp = slices[1];

        //leaf
        if(tmp.startsWith("leaf=")){
            if(tmp.length() <= 5){
                LOG.error("[gbdt] invalid line: " + line);
                return -1;
            }
            TreeNode node = new TreeNode();
            nodeIdTreeMap.put(nodeId,node);
            String[] tmpSlices = tmp.split(",");
            node.setValue(Float.parseFloat(tmpSlices[0].split("=")[1]));
            node.setNodeId(nodeId);
            return 0;
        }
        slices = tmp.split(" ");
        if(slices.length != 2){
            LOG.error("[gbdt] invalid line: " + line);
            return -1;
        }

        String cond = slices[0];
        tmp = slices[1];

        if(cond.length() < 5){
            LOG.error("[gbdt] invalid line: " + line);
            return -1;
        }

        cond = cond.substring(1,cond.length()-1);

        slices = cond.split("<");
        if(slices.length != 2){
            LOG.error("[gbdt] invalid line: " + line);
            return -1;
        }

        String featureName = slices[0];
        Double val = Double.parseDouble(slices[1]);
        Matcher m = pattern.matcher(tmp);
        int yesId = -1;
        int noId = -1;
        int missingId = -1;
        if(m.find()) {
            yesId = Integer.parseInt(m.group(1));
            noId = Integer.parseInt(m.group(2));
            missingId = Integer.parseInt(m.group(3));
        }

        boolean isYesMissing = (yesId == missingId);

        //parse left sub tree
        int ret = parse_tree(br,nodeIdTreeMap);
        // parse right sub tree

        if(ret == 0){
            ret = parse_tree(br,nodeIdTreeMap);
        }

        if(ret == 0){
            TreeNode node = new TreeNode();
            nodeIdTreeMap.put(nodeId, node);

            node.setNodeId(nodeId);
            node.setFeature_name(featureName);
            node.setValue(val);
            node.setIsMissingEqualYes(isYesMissing);
            node.setLeftNode(nodeIdTreeMap.get(yesId));
            node.setRightNode(nodeIdTreeMap.get(noId));
            return 0;
        }
        return -1;
    }

    public double scoring(TreeNode tree, Map<String, Double>features){
        if (null == features || features.size() <= 0) {
            return 0.0;
        }
        TreeNode p = tree;
        while(null != p) {
            if (null == p.getLeftNode() && null == p.getRightNode()) {
                return p.getValue();
            } else {
                boolean isYesMissing = p.isMissingEqualYes();
                if ( null == features.get(p.getFeature_name()) ){
                    if(isYesMissing) p=p.getLeftNode();
                    else p=p.getRightNode();
                }else {
                    if ( features.get(p.getFeature_name()) < p.getValue()) {
                        p = p.getLeftNode();
                    } else {
                        p = p.getRightNode();
                    }
                }
            }
        }
        return 0.0;
    }

    public void printTrees(){
        if(null == trees || trees.size() < 1){
            System.out.println("invalid trees");
        }
        int count = 0;
        for(TreeNode tree : trees){
            System.out.printf("booster[%d]:\n", count);
            printTree(tree, 0);
            count++;
        }
    }

    public void printTree(TreeNode tree, int indent){
        String indentStr = getIndent(indent);
        TreeNode p = tree;
        if(p.getLeftNode() == null && p.getRightNode() == null){
            System.out.printf("%s%d:leaf=%f\n",indentStr,p.getNodeId(), p.getValue());
        }else{
            int nodeId = p.getNodeId();
            int yesId = p.getLeftNode().getNodeId();
            int noId = p.getRightNode().getNodeId();
            int missingId = p.isMissingEqualYes() ? yesId : noId;
            String featureName = p.getFeature_name();
            double val = p.getValue();
            System.out.printf("%s%d:[%s<%.8f] yes=%d,no=%d,missing=%d\n", indentStr,nodeId,featureName,val,yesId,noId,missingId);
        }

        if(p.getLeftNode() != null){
            printTree(p.getLeftNode(),indent + 1);
        }

        if(p.getRightNode() != null){
            printTree(p.getRightNode(), indent + 1);
        }

    }

    public String getIndent(int indent){
        String indentStr = "";
        for(int i = 0; i < indent; i++){
            indentStr += "\t";
        }
        return indentStr;
    }
    public List<TreeNode> trees;

}
