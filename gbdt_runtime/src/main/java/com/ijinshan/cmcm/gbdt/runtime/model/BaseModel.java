package com.ijinshan.cmcm.gbdt.runtime.model;

import com.ijinshan.cmcm.gbdt.runtime.util.TreeNode;

import java.io.BufferedReader;
import java.util.List;
import java.util.Map;

/**
 * Created by tangdong on 7/5/15.
 */
public interface BaseModel {
    int load(String modelFileName);
    int parse_tree(BufferedReader br, Map<Integer,TreeNode> nodeIdTreeMap);
    double scoring(Map<String, Double>features);
    double scoring(TreeNode tree, Map<String, Double>features);
}
