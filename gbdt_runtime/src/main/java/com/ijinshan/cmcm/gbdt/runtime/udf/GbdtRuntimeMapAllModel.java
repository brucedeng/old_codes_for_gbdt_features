package com.ijinshan.cmcm.gbdt.runtime.udf;

/**
 * Created by mengchong on 11/2/15.
 */

import com.ijinshan.cmcm.gbdt.runtime.model.GbdtModel;
import org.apache.commons.collections.Bag;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.*;
import org.apache.pig.impl.logicalLayer.schema.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mengchong on 5/11/15.
 */
public class GbdtRuntimeMapAllModel extends EvalFunc<DataBag> {
    public GbdtRuntimeMapAllModel(String modelPath) {
        if(null == modelPath){
            LOG.error("[gbdt runtime] model path is invalid.");
        }
        System.err.println("model path is " + modelPath);

        File folder = new File(modelPath);
        File[] listOfFiles = folder.listFiles();
        System.err.println();
        models = new HashMap<String, GbdtModel>();

        for (File file : listOfFiles) {
            String fname = file.getName();
            String model_file_path = file.getAbsolutePath();
            System.err.println("model file path is " + model_file_path);
            System.err.println("parsing model " + fname);
            if(fname==null || (!fname.endsWith(".dump"))){
                System.err.println("Error file extention. needed a .dump file. file name is " + fname);
                continue;
            }

            if (file.isFile()) {
                System.out.println("opening model file" + file.getName());
                String modelName = fname.replace(".dump","");
                GbdtModel oneModel = new GbdtModel();
                oneModel.load(model_file_path);
                models.put(modelName, oneModel);
            }
        }
        System.err.println("model path is " + modelPath);

    }

    @Override
    public DataBag exec(Tuple input) throws IOException {
        double result;
        DataBag output = mBagFactory.newDefaultBag();
        if(input == null || input.size() == 0)
            return null;
        Map<String, Double> feamap = (Map<String, Double>)input.get(0);

        if(feamap == null)
            return null;

        for(String modelName:models.keySet()){
            GbdtModel currentModel = models.get(modelName);
            if ( currentModel == null){
                return null;
            }

            result = currentModel.scoring(feamap);
            List curTuple = new ArrayList(2);
            curTuple.add(modelName);
            curTuple.add(result);
            output.add(mTupleFactory.newTuple(curTuple));
        }

        return output;
    }

    public Schema outputSchema(Schema input) {
        try{
            Schema bagSchema = new Schema();
            bagSchema.add(new Schema.FieldSchema("model_name", DataType.CHARARRAY));
            bagSchema.add(new Schema.FieldSchema("score", DataType.DOUBLE));

            return new Schema(new Schema.FieldSchema(getSchemaName(this.getClass().getName().toLowerCase(), input),
                    bagSchema, DataType.BAG));
        }catch (Exception e){
            return null;
        }
    }

    private Map<String,GbdtModel> models;
    private static Logger LOG = LoggerFactory.getLogger(GbdtRuntimeMap.class);
    TupleFactory mTupleFactory = TupleFactory.getInstance();
    BagFactory mBagFactory = BagFactory.getInstance();
}

