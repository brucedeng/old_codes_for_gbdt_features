package com.ijinshan.cmcm.gbdt.runtime;

import com.ijinshan.cmcm.gbdt.runtime.model.GbdtModel;
import net.sf.json.JSONObject;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mengchong on 6/14/16.
 */
public class cli {
    public static void main(String[] argv) throws IOException {
        if(argv.length < 2){
            System.err.println("Usage: [command] model.dump feature_file [featmap.txt(optional)]\n  feature file is '-' if read from stdin");
            System.exit(0);
        }
        String modelFile = argv[0];
        String feature = argv[1];

        // load model file
        GbdtModel gbdtModel = new GbdtModel();
        gbdtModel.load(modelFile);

        // load feature file
        BufferedReader br = null;
        if(feature.equals("-")) {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        else {
            try {
                InputStream fis=new FileInputStream(new File(feature));
                br = new BufferedReader(new InputStreamReader(fis));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }


        if(argv.length == 2) {
            String line = br.readLine();
            for (; line != null; line = br.readLine()) {
//                System.out.println(line);
                Map<String,Double> tmpObj = new HashMap<String, Double>();
                JSONObject obj = JSONObject.fromObject(line);
                for(Object key:obj.keySet()){
                    tmpObj.put(key.toString(),obj.getDouble(key.toString()));
                }
                System.out.println(gbdtModel.scoring(tmpObj));
            }
        }
    }
}
