package com.ijinshan.cmcm.gbdt.runtime.udf;

import com.ijinshan.cmcm.gbdt.runtime.model.GbdtModel;
import com.ijinshan.cmcm.gbdt.runtime.model.MulticlassGbdtModel;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.*;
import org.apache.pig.impl.logicalLayer.schema.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by mengchong on 1/19/16.
 */
public class GbdtRuntimeMultiClassfication extends EvalFunc<DataBag> {
    public GbdtRuntimeMultiClassfication(String modelFile, String numClass) {
        if(null == modelFile){
            LOG.error("[gbdt runtime] model file is invalid.");
        }
        this.numClass = Integer.parseInt( numClass);
        gbdtModel = new MulticlassGbdtModel(this.numClass);
        gbdtModel.load(modelFile);
    }

    @Override
    public DataBag exec(Tuple input) throws IOException {
        if(input == null || input.size() == 0)
            return null;
        Map<String, Double> feamap = (Map<String, Double>)input.get(0);
        if(feamap == null)
            return null;

        DataBag output = mBagFactory.newDefaultBag();
        double[] results = new double[numClass];
        results = gbdtModel.multiScoring(feamap);

        for(int idx=0; idx < results.length; idx ++) {
            List curTuple = new ArrayList(2);
            curTuple.add(idx);
            curTuple.add(results[idx]);
            output.add(mTupleFactory.newTuple(curTuple));
        }
        return output;
    }

    public Schema outputSchema(Schema input) {
        try{
            Schema bagSchema = new Schema();
            bagSchema.add(new Schema.FieldSchema("class", DataType.INTEGER));
            bagSchema.add(new Schema.FieldSchema("score", DataType.DOUBLE));

            return new Schema(new Schema.FieldSchema(getSchemaName(this.getClass().getName().toLowerCase(), input),
                    bagSchema, DataType.BAG));
        }catch (Exception e){
            return null;
        }
    }

    private int numClass;
    private MulticlassGbdtModel gbdtModel;
    TupleFactory mTupleFactory = TupleFactory.getInstance();
    BagFactory mBagFactory = BagFactory.getInstance();

    private static Logger LOG = LoggerFactory.getLogger(GbdtRuntimeMap.class);
}
