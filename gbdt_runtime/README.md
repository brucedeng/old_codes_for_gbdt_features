gbdt runtime
==================

Repository gbdt runtime udf for pig. Built for hadoop 2.6 and pig 0.14, please chang pom.xml if you need a different version of hadoop and pig.

** NOTE: This library can only load model files with extention ".dump" , and ignores all other files.**

## pig udf usage

### 1. single model prediction

**This runtime generates scores for logistic regression problems.**

To use single model prediction in pig UDF, you need to:

1. register the jar file
2. distribute cache the model dump file so than any mapreduce node can access it.
3. initialize runtime function with appropriate file path, directing to the dump file on hadoop nodes.
4. generate features. features should be stored in maps.


```
register '*.jar';
set mapred.create.symlink yes;
set mapred.cache.files hdfs://path/to/hdfs/model/file#model.dump;

define EVAL com.ijinshan.cmcm.gbdt.runtime.udf.GbdtRuntimeMap('model.dump');
-- do something here
-- feature_data: ... features:map[]
result = foreach feature_data generate ... , EVAL(features) as score;
```

### 2. multiple models prediction

**This runtime generates scores for logistic regression problems, and can run prediction for multiple models.**

Like the single model runtime usage, but you need to compress all files into a tar.gz file, and distribute cache the file.

first, create a tar.gz file offline.

```
bash $> cd path/to/the/dump/files
bash $> tar -zcvf models.tar.gz *.dump
bash $> hadoop fs -copyFromLocal models.tar.gz hdfs://path/to/hdfs/model/file
```

Then, add the following code to pig script.

```
register '*.jar';
set mapred.create.symlink yes;
set mapred.cache.archives hdfs://path/to/hdfs/model/file#models;
define EVAL com.ijinshan.cmcm.gbdt.runtime.udf.GbdtRuntimeMapMulti('models');
-- do something here
-- feature_data: ... features:map[], model_name:chararray
result = foreach feature_data generate ... , EVAL(features,model_name) as score;
-- score has schema score:double
```


### 3. multiclassfication model prediction
**This runtime generates scores for multiple classfication problems.**

**Supported by version 0.5.0 or later**

the multiclassfication model runtime is objective to generate scores for multi classfication problems. this model use softmax method, and finally select one of the multiple classes as final result. classes are counted start from 0.

Like the single model prediction, you need to upload model file to hdfs first, and then use distribute cache.

xgboost's model dump for multiclassfication problems arranges like this:

* if you have N iterations, and M classes, the total number of trees in dump file is ```N*M```
* The Kth tree is for class ```K mod M```, and iteration ```int(K/M)```.
* All trees with its tree number ```K mod M == J``` are for class J. All trees with its tree number ```K / M == H``` are for iteration H.

So, the runtime must split the trees into several different classes, and run prediction for each class. and then calculate the final score with softmax functon. 

```
register '*.jar';
set mapred.create.symlink yes;
set mapred.cache.files hdfs://path/to/hdfs/model/file#model.dump;

-- if we have 6 classes.
define EVAL com.ijinshan.cmcm.gbdt.runtime.udf.GbdtRuntimeMultiClassfication('model.dump','6');
-- do something here
-- feature_data: ... features:map[]
result = foreach feature_data generate ... , EVAL(features) as probs;
-- scores has schema scores:{(class:chararray,score:double)}
```

### 4. single model prediction local mode

**This runtime generates scores for logistic regression problems.**

copy the jar file to your machine, and run 

```
java -jar gbdt_runtime-0.5.0-jar-with-dependencies.jar model.dump feature_file.txt
```

feature_file.txt is json format features. one json object each line. 

Below is one example of feature_file.txt

```
{"KW_REL":"0","NEWSY_SCORE":"0.76665","PROD_ID_3":"1","PROD_ID_5":"0","TIME_OF_DAY":"11","U_AGE_1":"0","U_AGE_2":"0","U_GENDER":"0","WORD_COUNT":"1920"}
{"CAT_REL":"0","DAY_OF_WEEK":"2","DOC_AGE":"8","D_CATEGORY_MACRO_MAX_COEC":"0.150143","D_CATEGORY_MACRO_MEAN_COEC":"0.150143","D_CATEGORY_MAX_C":"601506","D_CATEGORY_MAX_EC":"3.96626e+06","D_CATEGORY_MEAN_C":"601506","D_CATEGORY_MEAN_EC":"3.96626e+06","D_CATEGORY_MICRO_MAX_COEC":"0.150143"}
```

If the second argunment is "-", the code reads data from stdin.
