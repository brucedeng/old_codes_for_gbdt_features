# model 更新记录

[原始 google doc 英语实验上线文档](https://docs.google.com/document/d/1XxGw16llicOQltsR8hheJ0Q4de_aBo5XVSzZ5uCT8sk/edit#)

[原始 google doc 印地语实验上线文档](https://docs.google.com/document/d/1SJOIOyCZOarVWRgK2fVzNLkWR0oF6MMFsUpF5E7f7ho/edit#)

### 数据说明
* rcv log: s3://iwarehouse/cmnow/news_rcv_log/20151129/*/*.rcv, (北京时间)每五分钟更新一次
* up:  s3://com.cmcm.instanews.usw2.prod/user_profile/V1/up_json/  UTC每天凌晨更新前一天profile
* gmp dump:  s3://com.cmcm.instanews.usw2.prod/nrt/gmp/ 每10分钟一份
* cfb dump: s3://com.cmcm.instanews.usw2.prod/nrt/cfb/    每10分钟一份
* hindi cfb dump: s3://com.cmcm.instanews.usw2.prod/nrt/cfb_multi_lan/cfb/hi  每10分钟一份


##### 数据清洗注意事项:

1. raw cfb 计算时不做去重操作,  category的配置是: U_NCAT=15 -p U_NKEY=20 -p D_NCAT=5 -p D_NKEY=10. 
2. 训练数据的event要做去重操作, 每个用户-文章只保留一条
3. 过滤条件为:pid=='11' and json#'scenario'#'level1_type' =='1' and json#'scenario'#'level1' == '1', 即, 只保留列表页文章的点击
4. dwelltime 做硬截断, 上限是600.
5. cp的web源问题已修复, web源的category可以加入训练样本中.
6. ctype区分新闻，专题页和视频. 由于ctype格式有所变化, 判断时需要把ctype转成int型再做比较. 新闻训练数据filter条件为:  ctype is not null and (int)(ctype) ==1
7. app_lan, 印地语为"hi_in" 或 "hi", 英语为"en_.*" 或 "en" 或空. 因为上报问题, 英语很多记为en_us, en_gb 等, 还有老版本的是空, 需要做特殊处理.

