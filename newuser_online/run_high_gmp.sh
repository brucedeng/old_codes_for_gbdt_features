#basic setting
lan='en'
country='US'

#gmp 
cur_date=`date +%Y%m%d`;
cur_time=`date +%Y%m%d%H%M`

ftr_start=`date -d "+3 day ago $cur_date" +%Y%m%d`;
ftr_start_s=`date -d "$ftr_start" +%s`;

ftr_end=$cur_date
agg_dates=$ftr_start
agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"
for ((; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done

#echo $agg_dates

cur_h=`date +%H`
cur_hour=`date -d "+1 hour ago $cur_h" +%H`
cur_hour_12hago=`date -d "+12 hour ago $cur_h" +%H`
cur_min=`date +%M`
cur_minute=`echo "$cur_min / 10 * 10" | bc | sed ':a;s/\(..\)/\1/;tb;s/^/0/;ta;:b'`
gmp_path="hdfs://mycluster/projects/news/nrt/dwell_gmp/dump/{${agg_dates}}/${cur_hour}${cur_minute}/*"
#gmp_path="hdfs://mycluster/projects/news/nrt/dwell_gmp/dump/{${agg_dates}}/${cur_hour}${cur_minute}/*"
#gmp_path="hdfs://mycluster/projects/news/nrt/india_group_gmp/dump/{${agg_dates}}/${cur_hour}${cur_minute}/*"
#echo  $cur_hour

#cp
input_cp_template="/projects/news/cpp/feeder/in_cp_dump/%s/*/*/*.data"
content_profile_path=`printf "$input_cp_template" "{$agg_dates}"`
country_lower=$(echo ${country} | tr '[A-Z]' '[a-z]')


#output
output_path="hdfs://mycluster/projects/news/model/new_user/high_gmp"

#pig parameters
queue=default
#experiment
#default
PIG_PARAM=" -Dmapred.job.queue.name=$queue"

cmd="pig $PIG_PARAM -p gmp_path='$gmp_path' -p ftr_start_s='$ftr_start_s' -p cp_dump='$content_profile_path' -p output_path='$output_path' -p lan='$lan' -p country='$country' -p country_lower='$country_lower' high_gmp.pig"
echo $cmd
eval $cmd > log/piglog/${cur_time}_pig.log 2>&1
#sleep 600

# #copy to local
mv data/shortterm.data data/shortterm.archive 
hadoop fs -cat "${output_path}/part-*" | head -n 100 > data/shortterm.data
#write to redis
/data/guozhenyuan/anaconda/bin/python write2redis.py > log/${cur_time}.log 2>&1 
