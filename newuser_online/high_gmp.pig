REGISTER 'lib/*.jar';
REGISTER 'lib/utils.py' USING jython as myfun;

raw_gmp = LOAD '$gmp_path' as (content_id:chararray, batchid:int, original_ec:double, original_c:double, ec:double, c:double, gmp:double, written:chararray, lan_coun_click:chararray, timestamp:double);

raw_gmp = FOREACH raw_gmp GENERATE 
  content_id, 
  original_ec, 
  original_c,
  original_c/original_ec as dwellgmp:double,
  myfun.parse_lan(lan_coun_click) as lan:chararray,
  myfun.parse_country(lan_coun_click) as country:chararray,
  myfun.parse_click(lan_coun_click) as click:int,
  written,
  timestamp;

raw_gmp = FOREACH  raw_gmp GENERATE  content_id..timestamp, click/original_ec as gmp;


filter_gmp = Filter raw_gmp by lan == '$lan' and country == '$country' and original_ec > 3000;

group_gmp =  FOREACH (group filter_gmp by content_id) {
  ord = order filter_gmp by timestamp DESC;
  top = limit ord 1;
  var = datafu.pig.stats.VAR($1.gmp);
  cnt = COUNT($1.gmp);

  GENERATE FLATTEN(top), var, cnt;
} 

group_gmp = Filter group_gmp by written == 'w' ;



raw_content = LOAD '$cp_dump' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);
raw_content = foreach raw_content generate  
  json#'item_id' as content_id:chararray,
  --json#'title_md5' as title_md5:chararray,
  json#'group_id' as groupid:chararray,
  --REPLACE(json#'publisher','(\r\n|\n|\t|\r)','') as publisher:chararray,
  --(float)json#'newsy_score' as newsy_score:float,
  json#'type' as type:chararray,
  --(int)(json#'source_type') as source_type:int,
  (long)json#'publish_time' as publish_time:long,
  --(long)json#'update_time' as update_time:long,
  --(int)json#'image_count' as image_count:int,
  --(int)json#'word_count' as word_count:int,
  json#'region' as region:chararray,
  json#'language' as language:chararray,
  json#'l2_categories' as categories:{t1:(y:map[])};
  --json#'entities' as keywords:{t1:(y:map[])};


raw_content_filter = FILTER raw_content by publish_time >= (long)'$ftr_start_s'  and ((type=='article' or type=='slideshow' or type=='photostory') and (region == '$country_lower' and language == '$lan')) ;
raw_content = FOREACH raw_content_filter GENERATE content_id, groupid, publish_time,categories as categories:{t1:(y:map[])};

raw_content_distinct = foreach (group raw_content by content_id) {
    
    r = order raw_content by publish_time DESC;
    l = limit r 1;
    generate flatten(l);

};

content_info = FOREACH raw_content_distinct {
  categories = FOREACH categories GENERATE
          (chararray)y#'name' AS name:chararray;
  
  categories = filter categories by name is not null;

  GENERATE
      content_id AS content_id,
      --title_md5 AS title_md5,
      groupid AS groupid,
      --publisher AS publisher,
      --update_time AS update_time,
      publish_time AS publish_time,
      flatten(categories) AS categories;
}

raw_cp_gmp_join = join group_gmp by top::content_id, content_info by content_id;

cp_gmp_join = FOREACH raw_cp_gmp_join GENERATE content_info::content_id, gmp, dwellgmp, click, original_ec, categories, publish_time, groupid;

res = order cp_gmp_join by dwellgmp DESC;


rmf $output_path
STORE res into '$output_path';


