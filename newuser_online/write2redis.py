import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import traceback
import redis
import time
global config
import json
import traceback
import socket
import logging
console_handler = logging.StreamHandler()
console_handler.setFormatter(logging.Formatter('%(asctime)s\t%(levelname)s\t%(message)s'))
logger = logging.getLogger(__name__)
logger.addHandler(console_handler)
logger.setLevel(logging.DEBUG)

def load_longterm_articles(file):
    data = []
    with open(file) as f:
        for line in f:
            newuser_lst = line.split()
            content_id = newuser_lst[1]
            score = newuser_lst[2]
            group_id = newuser_lst[0]
            content_dict = {'id':content_id,'score':score,'group_id':group_id}
            data.append(content_dict)
    return data

def load_shortterm_articles(file):
    data = []
    with open(file) as f:
        for line in f:
            newuser_lst = line.split()
            content_id = newuser_lst[0]
            score = newuser_lst[1]
            group_id = newuser_lst[7]
            content_dict = {'id':content_id,'score':score,'group_id':group_id}
            data.append(content_dict)
    return data

if __name__ == '__main__':
    key = 'nr17_us_en'
    longterm = load_longterm_articles('data/longterm.data')
    shortterm = load_shortterm_articles('data/shortterm.data')
    logger.info("loaded shortterm %s longterm %s" % (len(shortterm),len(longterm)))
    data = longterm + shortterm
    #print data[:5]
    redis_host = ['rank-list.6gvwka.ng.0001.euw1.cache.amazonaws.com','rank-list.mkjqd2.ng.0001.apse1.cache.amazonaws.com','rank-list.bgkplr.ng.0001.usw2.cache.amazonaws.com']
    redis_port = 6379
    db_writer=0
    for anyhost in redis_host:
            for i in xrange(3):
                try:
                    redis_writer = redis.Redis(host=anyhost,port=redis_port,db=db_writer)
                    redis_writer.set(key,json.dumps(data))
                    #print json.dumps(data)
                    logger.info("writing to %s %s %s" % (anyhost,key,len(data)))
                    break
                except:
                    traceback.print_exc()

