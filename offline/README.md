# explore列表

https://docs.google.com/document/d/1arAMR5BGjn8sfUlrcNGEMiRmsAFH8FPNNgpERzqTsu0/edit#

程序：/data/guozhenyuan/explorer

crontab: insta_explorer_cron

git: http://git.liebaopay.com/cmnews/explorer/

脚本：insta_explorer.sh

配置文件: conf

监控：http://falconus.liebaopay.com:8042/screen/1012

负责人: 郭震远, 孟崇

## 部署方法:
```{r, engine='bash', count_lines}
cd deploy

# 安装依赖
sudo sh dependency.sh

# 添加insta_explorer_cron到crontab
crontab -e
# !!! copy insta_explorer_cron to crontab file 

# 添加自动rotate日志功能
sudo cp logrotate_explorer /etc/logrotate.d/

# 根据exstat_hive中说明建立hive表查询文章explore状态（可选)
```



美西服务器地址文档
https://docs.google.com/spreadsheets/d/1lSs3uiGlXsXDGT1ZNi3UlV2IER3Ngae2RE0z_VQSZxU/edit#gid=1625939552