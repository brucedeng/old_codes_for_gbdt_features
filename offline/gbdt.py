#!/usr/bin/env python
# -*- coding:utf-8 -*-

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from util import Dict
import re
import math
import json


class GBDT(object):

    def __init__(self, filename):
        self.trees = []
        self.load(filename)

    def get_tree(self, lines):
        if lines:
            tree = Dict()
            ret = self.parse_tree(tree, lines)
            if ret:
                self.trees.append(tree)

    def load(self, filename):
        lines = []
        with open(filename) as fp:
            for line in fp:
                if line.startswith('booster['):
                    self.get_tree(lines)
                    lines = []
                else:
                    lines.append(line.strip())
        self.get_tree(lines)

    def parse_line(self, tree, line):
        res = re.match('^(\d+):leaf=(.*?),cover=(.*)$', line)
        #res = re.match('^(\d+):leaf=(.*?)$', line)
        if res:
            node = Dict({
                'id': int(res.groups()[0]),
                'leaf': True,
                'val': float(res.groups()[1]),
            })
            tree[node.id] = node
            return True

        res = re.match('^(\d+):\[(\w+)<(.*)\]\s*yes=(\d+),no=(\d+),missing=(\d+),gain=(.*?),cover=(.*?)$', line)
        #res = re.match('^(\d+):\[(\w+)<(.*)\]\s*yes=(\d+),no=(\d+),missing=(\d+)$', line)
        if res:
            node = Dict({
                'id': int(res.groups()[0]),
                'fea': res.groups()[1],
                'val': float(res.groups()[2]),
                'left': int(res.groups()[3]),
                'right': int(res.groups()[4]),
            })
            if res.groups()[5] == res.groups()[3]:
                node.missing = node.left
            elif res.groups()[5] == res.groups()[4]:
                node.missing = node.right
            else:
                print 'parse %s failed' % line
            tree[node.id] = node
            return True

        print 'parse %s failed' % line
        return False

    def parse_tree(self, tree, lines):
        ret = []
        for line in lines:
            ret.append(self.parse_line(tree, line))
        return all(ret)

    def _scoring(self, tree, features):
        p = tree[0]
        while True:
            if p.leaf:
        #        print "id: {} value: {}".format(p.id, p.val)
                return p.val

            if features.get(p.fea) == None:
                p = tree[p.missing]
            elif features.get(p.fea) < p.val:
                p = tree[p.left]
            else:
                p = tree[p.right]

    def scoring(self, features):
        sum_score = 0
        idx = 0
        for tree in self.trees:
            #print idx
            idx += 1
            sum_score += self._scoring(tree, features)

        #print sum_score
        final_score = 1.0 / (1.0 + math.exp(-sum_score))
        return final_score


if __name__ == '__main__':
    gbdt = GBDT('cmtab_ru_ru_p2_20160624.dump')
    data = json.load(file('fresh.json'))
    for item in data['data']:
        if not 'features' in item:
            continue
        features = json.loads(item['features']['ranking_features'])
        for key in features:
            features[key] = float(features[key])
        online_score = features['gbdt']
        del features['gbdt']
        print online_score, gbdt.scoring(features)
