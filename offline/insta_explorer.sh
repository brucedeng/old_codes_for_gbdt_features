#!/bin/bash
PYTHON2="/data/guozhenyuan/anaconda/bin/python"
#PYTHON2="/usr/bin/python2.7"
INSTANEWS_PY="instanews.py"
LOG_FILE_PATH="./logs/explorer.log"
MULTILAN_CONFIG_FILE_PATH="./conf/explore_multilan.cfg"
RUN_MODE="ONLINE"
if [ ! -z "$1" ]
  then
    RUN_MODE=$1
fi
cmd="${PYTHON2} ${INSTANEWS_PY} ${MULTILAN_CONFIG_FILE_PATH} ${RUN_MODE} >> ${LOG_FILE_PATH} 2>&1 &"
echo ${cmd}
eval ${cmd}


