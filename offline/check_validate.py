#!/usr/bin/env python
# -*- coding:utf-8 -*-

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from instanews import get_social_scores

ex_list = ['q2287fdd7JH_us', 'RL2982943Nu_us', '1529a935a2U_us']
#get_social_scores(ex_list)

if len(sys.argv) > 1:
    ex_list = [sys.argv[1], ]



from thrift_gen.scoring.ttypes import *
def get_quality_scores(client, ex_list):
    query = Query()
    query.request_id = 'ex'
    query.product_id = '3'
    params = {
        'predictor':'nr_en_us_content_20160830',
        'debug': 'true',
    }
    query.params = params

    scores = {}

    predict_req_count = 0
    doc_ids = []
    for doc_id in ex_list:
        doc_ids.append(doc_id)
        if len(doc_ids) >= 1000:
            query.doc_ids = doc_ids
            client.scoring(query, scores)
            predict_req_count += 1
            doc_ids = []

    if len(doc_ids) > 0:
        query.doc_ids = doc_ids
        client.validate(query, "/data1/wangzhen1/source/india_news_ranking/models/nr/en_us/nr_en_us_content_20160830.dump")
        predict_req_count += 1

    print('predict count=%d, scores count=%d' % (predict_req_count, len(scores)))
    return scores

from prediction_client import PredictionClient

prediction_host = 'internal-instanews-ps-1299829311.us-west-2.elb.amazonaws.com'
#prediction_host = '10.5.2.244'
prediction_port = 31000
prediction_predictor = 'nr_en_us_content_20160830'
client = PredictionClient(prediction_host, prediction_port)
g_quality_scores = get_quality_scores(client, ex_list)
# print g_quality_scores
