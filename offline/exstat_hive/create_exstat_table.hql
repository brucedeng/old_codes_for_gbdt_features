USE default;
DROP TABLE `exstat_article`;
CREATE EXTERNAL TABLE `exstat_article`(
  `content_id` string,
  `ts` bigint,
  `stats` string,
  `region` string,
  `lan` string,
  `index_flag` bigint,
  `isdel` int,
  `editor_level` int,
  `publish_time` bigint,
  `type` string,
  `sourcetype` int,
  `opencms_id` int,
  `md5` string,
  `publisher` string )
PARTITIONED BY(
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE;

DROP TABLE `exstat_video`;
CREATE EXTERNAL TABLE `exstat_video`(
  `content_id` string,
  `ts` bigint,
  `stats` string,
  `region` string,
  `lan` string,
  `index_flag` bigint,
  `isdel` int,
  `editor_level` int,
  `publish_time` bigint,
  `type` string,
  `sourcetype` int,
  `opencms_id` int,
  `md5` string,
  `publisher` string )
PARTITIONED BY(
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE;

