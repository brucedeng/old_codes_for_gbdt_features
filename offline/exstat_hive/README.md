# first change user to news_model:
sudo -u news_model bash

# create hive exstat sql_table
hive -f create_exstat_table.hql

# add crontab to upload file and add partition
### set by guozhenyuan for pour explorer stats in hive ###
* 5 * * * cd /data/guozhenyuan/explorer/exstat_hive && nohup sh -x upload_log_to_hdfs.sh >> run.log 2>&1 &