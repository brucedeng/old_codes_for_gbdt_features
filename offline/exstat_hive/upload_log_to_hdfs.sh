#!/usr/bin/env bash
if [ `whoami` != "news_model" ]; then
   echo "This script must be run as news_model"
   exit 1
fi
TIME_FORMAT="%Y%m%d"
TIME_INCREMENT="1 days"
END_TIME=`date +%Y%m%d`
START_TIME=`date -d "5 days ago ${END_TIME}" +%Y%m%d`
ARTICLE_EXSTAT_PATH=/data/guozhenyuan/explorer/logs
VIDEO_EXSTAT_PATH=/data/tongming/explorer/offline/logs

function add_part(){
type=$1
time=$2
hive -e "ALTER TABLE exstat_${type} add PARTITION ( dt='${time}' ) location 'hdfs://mycluster/projects/news/model/tmp/exstat/${type}/${time}'"
}

function upload_exstat_log(){
type=$1
CUR_TIME=${START_TIME}
while [[ `date -d "${CUR_TIME}" +%s`  -le `date -d "${END_TIME}" +%s` ]];do
    upload_path=/projects/news/model/tmp/exstat/${type}/${CUR_TIME}/exstat.log-${CUR_TIME}
    if $(hdfs dfs -test -f ${upload_path}) ; then
        echo "${upload_path} already exists. Skip."
    else
        log_file_name=exstat.log-${CUR_TIME}
        if [ -f ${log_file_name} ] ; then
            hdfs dfs -mkdir -p /projects/news/model/tmp/exstat/${type}/${CUR_TIME}/
            cmd="hdfs dfs -put ${log_file_name} /projects/news/model/tmp/exstat/${type}/${CUR_TIME}/"
            echo ${cmd}
            eval ${cmd}

            cmd="add_part ${type} ${CUR_TIME}"
            echo ${cmd}
            eval ${cmd}
        fi
    fi
    CUR_TIME=`date -d "${CUR_TIME}+${TIME_INCREMENT}" "+${TIME_FORMAT}"`
done;
}

cd ${ARTICLE_EXSTAT_PATH}
upload_exstat_log article
cd ${VIDEO_EXSTAT_PATH}
upload_exstat_log video

