#!/usr/bin/python
# encoding=utf-8
from thrift_gen.scoring import ScoringService
from thrift_gen.scoring.ttypes import *
from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
import traceback

import json
import time
import requests
from datetime import datetime
def getStampFromString(timestring, timepattern):
      return int(time.mktime(datetime.strptime(timestring, timepattern).timetuple()))
def get_docage(item_id):
    res = requests.get('http://instanews-cp-db-api-internet-1981402352.us-west-2.elb.amazonaws.com:8000/v2/content/{}'.format(item_id))
    publish_time = getStampFromString(json.loads(res.content)['publish_time'], '%Y-%m-%d %H:%M:%S')
    return (int(time.time()) - publish_time)/3600


class PredictionClient:
    def __init__(self, host, port):
        try:
            self.host = host
            self.port = port
            socket = TSocket.TSocket(host,int(port))
            self.transport = TTransport.TFramedTransport(socket)
            protocol = TBinaryProtocol.TBinaryProtocol(self.transport)
            self.client = ScoringService.Client(protocol)
            self.transport.open()
        except:
            self.client = None
            self.transport = None
            traceback.print_exc()

    def scoring(self,query, scores):
        result = self.client.scoring(query)

        for entity in result.documents:
            scores[entity.doc_id] = entity.score

    def validate(self,query, model, age_decay=-0.001):
        from gbdt import GBDT
        import math
        gbdt = GBDT(model)
        result = self.client.scoring(query)

        for entity in result.documents:
            features = entity.ranking_scores
            if not features:
                continue

            online_score = entity.score
            offline_score = gbdt.scoring(features)
            offline_score *= math.exp(get_docage(entity.doc_id) * age_decay)
            print entity.doc_id, online_score, offline_score, abs(online_score - offline_score) < math.pow(10, -4)


    def __del__(self):
        self.transport.close()

if __name__ == '__main__':
    host = '10.10.14.42'
    port = 31000
    client = PredictionClient(host, port)
    doc_ids = ['56974797', '57039168', '56996221', '57000437', '56997080', '57026435', '57003411', '56974783', '56995663', '56966723']
    #doc_ids = ['56974797']
    #doc_ids = ['57012614']
    #doc_ids = ['57012614', '56815698', '57014185', '57014185']
    query = Query()
    query.request_id = '123524352345'
    user_profile = UserProfile()
    user_profile.userid = 'test_user'
    user_profile.categories = {"sport": 1.0, "society": 1.0}
    user_profile.keywords = {u"郑州".encode('utf-8'): 1.0, u"长城".encode('utf-8'):1.0, u"北京".encode('utf-8'): 1.0}
    print user_profile.keywords
    user_profile.city = '704_zhengzhou'
    user_profile.cat_len = 1
    user_profile.kw_len = 2
    user_profile.age = 1
    user_profile.gender = 1
    query.user_profile = user_profile
    query.doc_ids = doc_ids
    query.product_id = '3'
    params = {
        'predictor':'pns',
    }
    query.params = params

    scores = {}
    client.scoring(query, scores)

    print scores
