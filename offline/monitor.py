#!/usr/bin/env python

# import requests
import time
import json
import traceback
import logging
import httplib

console_handler = logging.StreamHandler()
console_handler.setFormatter(logging.Formatter('%(asctime)s\t%(levelname)s\t%(message)s'))
logger = logging.getLogger(__name__)
logger.addHandler(console_handler)
logger.setLevel(logging.DEBUG)

def send_post(host,port,link,data):
    httpServ = httplib.HTTPConnection(host, port)
    httpServ.connect()
    httpServ.request('POST', link, data)

    response = httpServ.getresponse()
    if response.status == httplib.OK:
        return (response.read())
    else:
        return None


def write_falcon_raw(payload,retry=2,logger=logger):
    # print payload
    for i in range(retry):
        try:
            # r = requests.post("http://127.0.0.1:1988/v1/push", data=json.dumps(payload))
            r = send_post('127.0.0.1',1988,'/v1/push',json.dumps(payload))
            # print r
            if 'success' == r:
                return True
            else:
                logger.error("error writting falcon. return message is: " + r)
                continue
        except:
            err_msg = traceback.format_exc()
            logger.error(err_msg)
            continue
    return False


class monitor_server:
    def __init__(self,retry=2,logger=logger,ts=None,endpoint='',counterType='GAUGE', step=600):
        self.retry = retry
        self.logger = logger
        self.ts = ts
        self.endpoint = endpoint
        self.counterType = counterType
        self.step = step

    def write_falcon_with_tag(self,data):
        payload = []
        if self.ts is None:
            ts=int(time.time())
        else:
            ts=self.ts

        # print data

        for item in data:
            (metric,value,tag) = item[:3]
            payload.append({"endpoint":self.endpoint,"metric":metric,"timestamp":ts,"step":self.step,"value":value,"counterType":self.counterType,"step":self.step,"tags":tag})
        if write_falcon_raw(payload,self.retry,self.logger):
            self.logger.info("successfully wrote falcoun")
        else:
            self.logger.error("error writing falcoun")

if __name__=='__main__':
    ts = int(time.time())
    payload = [
    {
        "endpoint": "10.2.2.238",
        "metric": "test-metric",
        "timestamp": ts,
        "step": 60,
        "value": 1,
        "counterType": "GAUGE",
        "tags": "target=featureserver_apse1",
    },
    {
        "endpoint": "10.2.2.238",
        "metric": "test-metric2",
        "timestamp": ts,
        "step": 60,
        "value": 2,
        "counterType": "GAUGE",
        "tags": "target=featureserver_apse1",
    }
    ]

    falcon=falcon_server()
    falcon.write_falcon_with_tag([('test_metric',1,'target=featureserver_apse1')])

    # write_falcon(payload)
