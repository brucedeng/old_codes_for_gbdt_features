#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import redis
import logging
import logging.config
import urllib2
import json
import random
import time
import math
import os
import ConfigParser
import hashlib
from itertools import izip_longest
import monitor
import socket
import requests

from cmnews_news_profile_pb2 import NewsProfile

from thrift_gen.scoring.ttypes import *
from prediction_client import PredictionClient

# Get logger.
logging.config.fileConfig('./conf/logging.conf')
logger = logging.getLogger('main')
exlist_logger = logging.getLogger('exlist')
exstat_logger = logging.getLogger('exstat')

# Load common config to get content_redis, opencmsid_redis and output servers.
config = ConfigParser.ConfigParser()
explore_common_config_file = './conf/explore_common.cfg'
config.readfp(file(explore_common_config_file))

base_url = config.get('index', 'base_url')

content_redis = redis.StrictRedis(
    host=config.get('content_redis', 'host'),
    port=config.getint('content_redis', 'port'),
    password=config.get('content_redis', 'password'),
    db=config.getint('content_redis', 'db'))

opencmsid_redis = redis.StrictRedis(
    host=config.get('opencmsid_redis', 'host'),
    port=config.getint('opencmsid_redis', 'port'),
    password=config.get('opencmsid_redis', 'password'),
    db=config.getint('opencmsid_redis', 'db'))

exploration_redis = []
output_servers = config.get('exploration_redis', 'servers').split(',')
for output_server in output_servers:
    if output_server == '':
        continue
    columns = output_server.split(':')
    server = redis.StrictRedis(
        host=columns[0],
        port=columns[1],
        password=columns[2],
        db=int(columns[3]))
    exploration_redis.append(server)

g_predict_server = config.get('prediction_server', 'host')
g_predict_port = config.getint('prediction_server', 'port')

def get_all_docs():
    ids = []
    url = base_url + '?action=filter_docs&doc_age=24&index_flag=%s&language=%s&region=%s' % (
        g_index_flag, g_lan, g_region)
    logger.info('index url:%s' % url)
    req = urllib2.urlopen(url)
    if req is not None:
        res = req.read()
        if res is not None and res != '':
            obj = json.loads(res)
            if 'filter_docs' in obj:
                for id in obj['filter_docs']:
                    ids.append(id)
            elif 'all_docs' in obj:
                for id in obj['all_docs']:
                    ids.append(id)
    return ids


def get_all_gmp_docs():
    gmp_docs = []
    gmp_prefix = 'ng_'

    for key in content_redis.scan_iter(match=gmp_prefix + '*', count=20000):
        gmp_docs.append(key[3:])

    return gmp_docs


def doc_exstats_info(doc_id, stats, doc_fea=None):
    curr_ts = int(time.time())
    doc_stats = [doc_id, curr_ts, stats, g_lan, g_region, g_index_flag]
    if doc_fea is not None:
        doc_stats.extend([doc_fea.isdel, doc_fea.editor_level,
                         doc_fea.publish_time, doc_fea.type, doc_fea.sourcetype,
                         doc_fea.opencms_id, doc_fea.md5, doc_fea.publisher.encode('utf8')])
    else:
        doc_stats.extend(['']*8)
    return '\t'.join(map(str, doc_stats))


def gen_non_gmp_docs(all_docs, gmp_docs):
    non_gmp_docs = []
    gmp_docs_set = set(gmp_docs)

    for doc_id in all_docs:
        if doc_id not in gmp_docs_set:
            non_gmp_docs.append(doc_id)
        else:
            exstat_logger.info(doc_exstats_info(doc_id, stats="has_gmp"))

    return non_gmp_docs


def get_doc_feas(doc_ids, doc_feas, random_weight):
    def grouper(n, iterable, fill_value=None):
        """grouper(3, 'ABCDEFG', 'x') --> ABC DEF Gxx"""
        args = [iter(iterable)] * n
        return izip_longest(fillvalue=fill_value, *args)

    pipe = content_redis.pipeline(transaction=False)
    pipe_size = 20000
    global random_formatter
    m2 = hashlib.md5()

    for batch_doc_ids in grouper(pipe_size, doc_ids):
        for doc_id in batch_doc_ids:
            if doc_id is not None:
                pipe.get("n_" + doc_id)
        for (doc_id, raw_doc_feas) in zip(batch_doc_ids, pipe.execute()):
            if raw_doc_feas is not None and raw_doc_feas != '':
                p = NewsProfile()
                p.ParseFromString(raw_doc_feas)
                doc_feas[doc_id] = p
                m2.update(batch_doc_ids[0])
                random_weight[doc_id] = int(m2.hexdigest()[:8], 16)


def exlist_cmp(doc_1, doc_2):
    doc_fea1 = g_doc_feas[doc_1]
    doc_fea2 = g_doc_feas[doc_2]

    doc_explore_score_1 = 0
    doc_explore_score_2 = 0

    if doc_fea1.sourcetype in ['6', '7']:
        doc_explore_score_1 += 2
    if doc_fea2.sourcetype in ['6', '7']:
        doc_explore_score_2 += 2

    # article which has copyright should be in head
    if not doc_fea1.has_copyright:
        doc_explore_score_1 += 1
    if not doc_fea2.has_copyright:
        doc_explore_score_2 += 1

    if doc_fea1.media_level == 1:
        doc_explore_score_1 += 0.5
    if doc_fea2.media_level == 1:
        doc_explore_score_2 += 0.5

    #g_final_score[doc_1] = "cms: {} + copyright: {} = {}".format((g_doc_feas[doc_1].sourcetype in ['6', '7']) * 2, (not g_doc_feas[doc_1].has_copyright)*1, doc_explore_score_1)
    #g_final_score[doc_2] = "cms: {} + copyright: {} = {}".format((g_doc_feas[doc_2].sourcetype in ['6', '7']) * 2, (not g_doc_feas[doc_2].has_copyright)*1, doc_explore_score_2)

    if doc_explore_score_1 != doc_explore_score_2:
        return -cmp(doc_explore_score_1, doc_explore_score_2)
    elif g_random_weight[doc_1] != g_random_weight[doc_2]:
        return -cmp(g_random_weight[doc_1], g_random_weight[doc_2])
    elif doc_fea1.publish_time != doc_fea2.publish_time:
        return -cmp(doc_fea1.publish_time, doc_fea2.publish_time)
    else:
        return 0

def get_fea_and_score(item_id):
    quality_score = g_quality_scores.get(item_id, 0.0) \
        + g_social_scores.get(item_id, 0.0) \
        + (g_doc_feas[item_id].sourcetype in ['6', '7']) * 2 \
        + (not g_doc_feas[item_id].has_copyright) \
        + (g_doc_feas[item_id].media_level == 1) * 0.5
    #g_final_score[item_id] = "model: {} + social: {} + cms: {} + copyright: {} = {}".format(g_quality_scores.get(item_id, 0.0), g_social_scores.get(item_id, 0.0), (g_doc_feas[item_id].sourcetype in ['6', '7']) * 2, (not g_doc_feas[item_id].has_copyright)*1, quality_score)

    exstat_logger.info("itemid: {} quality_score: {} g_quality: {} social: {}".format(item_id, quality_score, g_quality_scores.get(item_id, 0), g_social_scores.get(item_id, 0)))
    return g_doc_feas[item_id], g_random_weight[item_id], quality_score

def quality_cmp(a, b):
    doc_fea1, w1, s1 = get_fea_and_score(a)
    doc_fea2, w2, s2 = get_fea_and_score(b)

    if s1 != s2:
        if s1 > s2:
            return -1
        else:
            return 1
    elif w1 != w2:
        if w1 > w2:
            return -1
        else:
            return 1
    elif doc_fea1.publish_time != doc_fea2.publish_time:
        if doc_fea1.publish_time > doc_fea2.publish_time:
            return -1
        else:
            return 1
    else:
        return 0

def sort_exploration_list(ex_list):
    list1 = sorted(ex_list, cmp=quality_cmp)
    return list1


def get_quality_scores(ex_list):
    predict_client = PredictionClient(g_predict_server, g_predict_port)
    query = Query()
    query.request_id = 'ex'
    query.product_id = '3'
    params = {
        'predictor':g_predictor,
    }
    query.params = params

    scores = {}

    predict_req_count = 0
    doc_ids = []
    for doc_id in ex_list:
        doc_ids.append(doc_id)
        if len(doc_ids) >= 1000:
            query.doc_ids = doc_ids
            predict_client.scoring(query, scores)
            predict_req_count += 1
            doc_ids = []

    if len(doc_ids) > 0:
        query.doc_ids = doc_ids
        predict_client.scoring(query, scores)
        predict_req_count += 1

    logger.info('predict count=%d, scores count=%d' % (predict_req_count, len(scores)))
    return scores

def get_social_scores(ex_list):
    sns_search = "http://instanews-cp-db-api-internet-1981402352.us-west-2.elb.amazonaws.com:8000/v2/sns/items/_search"

    scores = {}
    social_req_count = 0
    doc_ids = []
    try:
        for doc_id in ex_list:
            doc_ids.append({'item_id': doc_id})
            if len(doc_ids) >= 1000:
                post_data = {'items': doc_ids, 'fields': ['item_id', 'social_data']}
                res = requests.post(sns_search, json.dumps(post_data))
                js = json.loads(res.content)
                for item in js:
                    scores[item['item_id']] = get_social_score(item['social_data'])
                social_req_count += 1
                doc_ids = []

        if len(doc_ids) > 0:
            post_data = {'items': doc_ids, 'fields': ['item_id', 'social_data']}
            res = requests.post(sns_search, json.dumps(post_data))
            js = json.loads(res.content)
            for item in js:
                scores[item['item_id']] = get_social_score(item['social_data'])
            social_req_count += 1

        logger.info('social req count=%d, scores count=%d' % (social_req_count, len(scores)))
    except Exception, e:
        logger.error(str(e))

    return scores


def get_social_score(social_data):
    # get latest count
    latest_count_dict = None
    social_data_js = json.loads(social_data)
    for k, v in social_data_js.items():
        if not latest_count_dict or latest_count_dict['updated_time'] < v['updated_time']:
            latest_count_dict = v

    sum_score = latest_count_dict['comment_count'] * 1  + latest_count_dict['share_count'] * 5 + latest_count_dict['favorite_count'] * 3
    return 1.0 / (1.0 + math.exp(-(sum_score*1.0/100)))


# http://git.liebaopay.com/cmnews/rs_engine/blob/master/proto/cmnews_news_profile.proto
def gen_exploration_list(non_gmp_docs, doc_feas):
    ex_filter_list = []
    ex_sort_list = []
    ex_list = []
    filter_stats = {
        'no_fea': 0,
        'editor_level': 0,
        'isdel': 0,
        'publish_time': 0,
        'dedup': 0,
        'groupid': 0,
        'local': 0,
        'sourcetype': 0,
        'important_level': 0,
        'whitelist_or_regist_time': 0,
        'video': 0,
        'no_pic': 0,
    }

    opencmsid_whitelist = []
    opencmsid_whitelist_str = opencmsid_redis.get('news_opencmsid_whitelist')
    if opencmsid_whitelist_str is not None:
        opencmsid_whitelist = opencmsid_whitelist_str.split(',')

    logger.info('opencmsid whitelist=%s' % opencmsid_whitelist)

    curr_ts = time.time()

    md5_set = set()

    for doc_id in non_gmp_docs:
        if doc_id not in doc_feas:
            filter_stats['no_fea'] += 1
            exstat_logger.info(doc_exstats_info(doc_id, 'no_fea'))
            continue

        doc_fea = doc_feas[doc_id]

        # isdel = 0
        if doc_fea.isdel != 0:
            filter_stats['isdel'] += 1
            exstat_logger.info(doc_exstats_info(doc_id, 'isdel', doc_fea))
            continue

        # editor_level != -5
        if doc_fea.editor_level == -5:
            filter_stats['editor_level'] += 1
            exstat_logger.info(doc_exstats_info(doc_id, 'editor_level', doc_fea))
            continue

        if doc_fea.type == 'video':
            exstat_logger.info(doc_exstats_info(doc_id, 'video', doc_fea))
            filter_stats['video'] += 1
            continue

        # md5 dedup
        if doc_fea.md5 != '' and doc_fea.md5 in md5_set:
            filter_stats['dedup'] += 1
            exstat_logger.info(doc_exstats_info(doc_id, 'dedup', doc_fea))
            continue

        md5_set.add(doc_fea.md5)

        # filter doc with out pic
        if g_region == "us" and g_lan == "en":
            if doc_fea.image_md5 == "":
                exstat_logger.info(doc_exstats_info(doc_id, 'no_pic', doc_fea))
                filter_stats['no_pic'] += 1
                continue


        ex_filter_list.append(doc_id)


    if g_predictor:
        g_social_scores.update(get_social_scores(ex_filter_list))

    for doc_id in ex_filter_list:
        doc_fea = doc_feas[doc_id]

        if doc_fea.sourcetype == '6' or doc_fea.sourcetype == '7':
            # 7 days or whitelist
            if curr_ts - doc_fea.regist_time > 3600 * 24 * 7:
                if doc_fea.opencms_id not in opencmsid_whitelist:
                    filter_stats['whitelist_or_regist_time'] += 1
                    exstat_logger.info(doc_exstats_info(doc_id, 'whitelist_or_regist_time', doc_fea))
                    continue

            # publish time < 24h
            if curr_ts - doc_fea.publish_time > 3600 * 24:
                filter_stats['publish_time'] += 1
                exstat_logger.info(doc_exstats_info(doc_id, 'publish_time', doc_fea))
                continue
        elif doc_id in g_social_scores:
            # publish time < 12h
            if curr_ts - doc_fea.publish_time > 3600 * 12:
                filter_stats['publish_time'] += 1
                exstat_logger.info(doc_exstats_info(doc_id, 'publish_time', doc_fea))
                continue
        else:
            # publish time < 6h
            if curr_ts - doc_fea.publish_time > 3600 * 6:
                filter_stats['publish_time'] += 1
                exstat_logger.info(doc_exstats_info(doc_id, 'publish_time', doc_fea))
                continue

            # if g_region == 'in' and g_lan != 'hi' and doc_fea.important_level < 50:
            #     filter_stats['important_level'] += 1
            #     continue

            filter_stats['sourcetype'] += 1
            # continue

        ex_sort_list.append(doc_id)


    if g_predictor:
        g_quality_scores.update(get_quality_scores(ex_sort_list))
        ex_sort_list = sort_exploration_list(ex_sort_list)
    else:
        ex_sort_list.sort(cmp=exlist_cmp)


    groupid_map = {}
    for doc_id in ex_sort_list:
        doc_fea = doc_feas[doc_id]
        if g_region == "us" and g_lan == "en":
            # groupid dedup
            if doc_fea.group_id != '' and doc_fea.group_id in groupid_map:
                filter_stats['groupid'] += 1
                groupid_map[doc_fea.group_id].append(doc_id)
                continue

            if doc_fea.group_id != '':
                groupid_map[doc_fea.group_id] = [doc_id]

        exstat_logger.info(doc_exstats_info(doc_id, stats="explored"))
        ex_list.append(doc_id)

    logger.info('ex_list filter stats: %s, after filtered: %d' % (filter_stats, len(ex_list)))

    stats = {
        'opencms': 0,
        'other': 0
    }
    for doc_id in ex_list:
        doc_fea = g_doc_feas[doc_id]
        if doc_fea.sourcetype == '6' or doc_fea.sourcetype == '7':
            stats['opencms'] += 1
        else:
            stats['other'] += 1

    logger.info('total exploration_list: %d, samples: %s' % (len(ex_list), ex_list[:10]))
    logger.info('stats: %s' % stats)

    opencms_ratio = 0
    stats_total = stats['opencms'] + stats['other']
    if stats_total != 0:
        opencms_ratio = float(stats['opencms']) / float(stats_total)
    g_monitor_result.append(['gmp.exploration.opencms_ratio', str(opencms_ratio)])

    return ex_list[:5000]


def update_exploration_list(docs):
    copyright_cnt = 0
    ncid_stats = {}
    for doc_id in docs:
        copyright_cnt += g_doc_feas[doc_id].has_copyright

        doc_ncid = g_doc_feas[doc_id].ncid
        ncid_stats[doc_ncid] = ncid_stats.get(doc_ncid, 0) + 1

    exlist_logger.info('\tregion=%s\tlang=%s\tindex_flag=%s\tcnt=%d\tcopyright_cnt=%s\texlist=%s' % (
        g_region, g_lan, g_index_flag, len(docs), copyright_cnt, ','.join(docs)))
    #exlist_logger.info('\tregion=%s\tlang=%s\tindex_flag=%s\tcnt=%d\tcopyright_cnt=%s\texlist=\n%s' % (
    #    g_region, g_lan, g_index_flag, len(docs), copyright_cnt, '\n'.join(["{}\t{}\t{}\t{}".format(doc, g_final_score[doc], g_doc_feas[doc].title.encode("utf8"), g_doc_feas[doc].source_url) for doc in docs])))

    if g_run_mode == 'TEST':
        return

    for server_address in exploration_redis:
        logger.info('update redis: %s' % server_address)
        server_address.delete(exploration_key)
        if len(docs) > 0:
            server_address.rpush(exploration_key, *docs)
    logger.info('region=%s\tlang=%s\tindex_flag=%s\tcnt=%d. Update redis succeed.' % (
        g_region, g_lan, g_index_flag, len(docs)))


def write_monitor(output, region, lan, index_flag):
    if g_run_mode == 'TEST':
        return
    tag = 'region=%s,lan=%s,index_flag=%s' % (region, lan, index_flag)
    map(lambda x: x.append(tag), output)
    host = socket.gethostname()
    monitor_obj = monitor.monitor_server(endpoint=host, step=600)
    monitor_obj.write_falcon_with_tag(output)
    logger.info('Write monitor succeed. %s' % tag)


def is_running():
    # return False
    try:
        f = open(PID_FILE, 'r')
    except:
        return False

    pid = f.read()
    f.close()

    if pid == '':
        return False

    try:
        os.kill(int(pid), 0)
    except OSError:
        return False
    else:
        return True


if __name__ == "__main__":

    # Assure only one instance in running.
    PID_FILE = './run.pid'
    if is_running():
        logger.info('Another process is already running')
        sys.exit(-1)
    f = open(PID_FILE, 'w+')
    f.write(str(os.getpid()))
    f.close()

    # Get multilan config.
    config = ConfigParser.ConfigParser()
    if len(sys.argv) < 2:
        explore_multilan_config_file = './conf/explore_multilan.cfg'
    else:
        explore_multilan_config_file = sys.argv[1]
    g_run_mode = "ONLINE"
    if len(sys.argv) > 2:
        g_run_mode = sys.argv[2]

    config.readfp(file(explore_multilan_config_file))
    explore_multilan = config.sections()

    # First prepare gmp_docs for all language article.
    g_gmp_docs = get_all_gmp_docs()
    logger.info('Multilan explore program start')
    logger.info('Total gmp docs: %d, samples: %s' % (len(g_gmp_docs), g_gmp_docs[:10]))

    # Run explore split by region, lan and pid according to config file.
    for explore_singlelan in explore_multilan:
        g_lan = config.get(explore_singlelan, 'language')
        g_region = config.get(explore_singlelan, 'region')
        g_index_flag = config.get(explore_singlelan, 'index_flag')
        quality_p = config.getfloat(explore_singlelan, 'quality_p')
        exploration_key = config.get(explore_singlelan, 'explore_list_key')
        g_predictor = ''
        if config.has_option(explore_singlelan, 'predictor'):
            g_predictor = config.get(explore_singlelan, 'predictor')


        logger.info("Singlelan program started, lan=%s, region=%s, index_flag=%s" % (g_lan, g_region, g_index_flag))
        try:
            g_random_bin_size = int(config.get(explore_singlelan, 'random_bin_size'))
        except:
            g_random_bin_size = 0

        if g_random_bin_size > 0:
            def random_formatter(x):
                return float(int(x * g_random_bin_size)) / g_random_bin_size
        else:
            def random_formatter(x):
                return x

        g_doc_feas = {}
        g_quality_scores = {}
        g_social_scores = {}
        g_final_score = {}
        g_random_weight = {}
        g_monitor_result = []

        all_docs = get_all_docs()
        g_monitor_result.append(['gmp.exploration.index_total_docs_cnt', str(len(all_docs))])
        logger.info('total docs: %d, samples: %s' % (len(all_docs), all_docs[:10]))
        if len(all_docs) == 0:
            logger.info('Docs is empty, run next language..')
            continue

        non_gmp_docs = gen_non_gmp_docs(all_docs, g_gmp_docs)
        logger.info('Non gmp docs: %d, samples: %s' % (len(non_gmp_docs), non_gmp_docs[:10]))

        non_gmp_docs_threshold = 50000
        if len(non_gmp_docs) > non_gmp_docs_threshold:
            logger.info('Cap non gmp docs to ' + str(non_gmp_docs_threshold))
            del non_gmp_docs[0:(len(non_gmp_docs) - non_gmp_docs_threshold)]

        get_doc_feas(non_gmp_docs, g_doc_feas, g_random_weight)
        logger.info('Non gmp docs %d, with profile: %d' % (len(non_gmp_docs), len(g_doc_feas)))

        exploration_list = gen_exploration_list(non_gmp_docs, g_doc_feas)
        g_monitor_result.append(['gmp.exploration.explored_article_cnt', str(len(exploration_list))])
        update_exploration_list(exploration_list)
        write_monitor(g_monitor_result, region=g_region, lan=g_lan, index_flag=g_index_flag)
        logger.info("Singlelan program exited, lan=%s, region=%s, index_flag=%s" % (g_lan, g_region, g_index_flag))
    logger.info('Multilan program exited.')


