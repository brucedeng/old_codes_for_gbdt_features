#!/usr/bin/env python
# encoding=utf-8

import sys

import argparse
import datetime
import json
import threading
import traceback
import urllib2

global host
import socket
try:
    host=socket.gethostname()
except:
    host='can_not_detect'
import math

global score_name
score_name='score'

def calc_age_decay(cur_ts,decay_factor,item):
    x = item['fields']
    return item.get('score',0) * math.exp(decay_factor*(cur_ts - float(x['publish_time']))/3600)

def re_order_agedecay(cur_ts,decay_factor,content_list):
    new_list=[]
    for news in content_list:
        tmp_des = news['des']
        if tmp_des.find('src=0') < 0:
            continue
        news['decay_score'] = calc_age_decay(cur_ts,decay_factor,news)
        new_list.append(news)
    return sorted(new_list,key=lambda x: x['decay_score'], reverse=True )

def calc_age_inverse_pct(content_list,get_metric_func=lambda x: x[score_name],max_cnt=100):
    reverse_cnt=0
    total_cnt = 0
    # print len(content_list)
    end=min(len(content_list),max_cnt)
    for idx in range(end):
        news = content_list[idx]
        for idx2 in range(idx+1,end):
            # print get_metric_func(content_list[idx])
            if get_metric_func(content_list[idx]) < get_metric_func(content_list[idx2]):
                reverse_cnt += 1
            total_cnt += 1
    return float(reverse_cnt)/total_cnt

def calc_age_topn_inv(content_list,top=10,get_metric_func=lambda x: x[score_name],max_cnt=100):
    reverse_cnt=0
    total_cnt = 0
    end=min(len(content_list),max_cnt)
    # print len(content_list)
    for idx in range(top):
        for idx2 in range(idx+1,end):
            baseline = get_metric_func(content_list[idx])
            if baseline < get_metric_func(content_list[idx2]):
                reverse_cnt += 1
            total_cnt += 1
    return float(reverse_cnt)/total_cnt

if __name__ == '__main__':
    timestamp=int(datetime.datetime.now().strftime('%s'))
        
    print timestamp

    res = urllib2.urlopen('http://54.179.166.129/r/?uid=c01da9d986cc23c9&count=1000&pid=11&ncid=29&offset=0&cache=false&debug=true&exp=insta_007&prediction_age_decay=0&diversity=false')
    data = res.read()
    result1 = json.loads(data)
    res_map = {}
    for item in filter(lambda x: x.get('des','').find('src=0') > 0,result1['newsid']):
        res_map[item['id']] = item['score']

    decay_factor = -0.001
    print 'http://54.179.166.129/r/?uid=c01da9d986cc23c9&count=1000&pid=11&ncid=29&offset=0&cache=false&debug=true&exp=insta_007&prediction_age_decay={}&diversity=false'.format(decay_factor)
    res2 = urllib2.urlopen('http://54.179.166.129/r/?uid=c01da9d986cc23c9&count=1000&pid=11&ncid=29&offset=0&cache=false&debug=true&exp=insta_007&prediction_age_decay={}&diversity=false'.format(decay_factor))
    data2 = res2.read()
    result2 = json.loads(data2)

    data_map = map(lambda x: (x['id'],x['score'],int(x['fields']['publish_time'])),filter(lambda x: x.get('des','').find('src=0') > 0,result2['newsid']))

    diff_cnt = 0
    for i in range(len(data_map)):
        itemid = data_map[i][0]
        cur_score = data_map[i][1]
        publish_time = int(data_map[i][2])
        if itemid not in res_map:
            continue
        now = math.log(cur_score/res_map[itemid])*3600/(-0.001) + publish_time
        print str(data_map[i]) + '\t' + str(res_map[itemid]) + '\t' + str((timestamp-now)/3600)
        # if data_map[i] != data_reorder[i]:
        #     diff_cnt += 1
    # print diff_cnt
    sys.exit()


    i=0
    while i <= 0.001:
        data_reorder = re_order_agedecay(timestamp, -i, data['newsid'])
        score_inv = calc_age_topn_inv(data_reorder,1, lambda x: x.get(score_name,0))
        age_inv = calc_age_topn_inv(data_reorder,1, lambda x: int(x.get('fields',{}).get('publish_time',0)))
        print -i, score_inv, age_inv
        i += 0.0001
    sys.exit()

    test_data={
      "des": "rid=a332bb9b|src=0|ord=1",
      "fields": {
        "adult_score": "0",
        "categories": "{1000395:1, }",
        "content_id": "6705534",
        "create_time": "1451835929",
        "editor_level": "0",
        "feedback_gmp": "0.171723",
        "feedback_time": "1451880000",
        "flag": "0",
        "group_id": "295524",
        "image_count": "1",
        "important_level": "0",
        "index_flag": "0",
        "ip_status": "0",
        "is_del": "0",
        "keywords": "{foods:0.066556, avoiding:0.0742426, retention:0.0878505, you:0.115122, your:0.0870127, repetitions:0.112035, belly:0.129422, paunch:0.0744813, workout:0.118153, fat:0.135125, }",
        "main_category": "1000395",
        "ncid": "",
        "news_md5": "0cb57ef8",
        "newsy_score": "1",
        "ord": "1",
        "publish_time": "1451835929",
        "publisher": "Zee News",
        "ranking_scores": "{}",
        "req_id": "a332bb9b",
        "source": "india.com",
        "source_type": "0",
        "source_url": "http://zeenews.india.com/news/health/health-news/five-steps-seven-days-losing-that-belly-fat-is-a-snap_1841299.html",
        "tier": "",
        "type": "article",
        "update_time": "1451836124",
        "version": "9",
        "word_count": "2173"
      },
      "flag": 0,
      "id": "6705534",
      "score": 0.573924
    }
    print calc_age_decay(1451880693,-0.001,test_data)
    sys.exit()
