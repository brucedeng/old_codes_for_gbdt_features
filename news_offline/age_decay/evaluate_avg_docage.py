#!/usr/bin/env python
# encoding=utf-8

import sys

import argparse
import datetime
import json
import threading
import traceback
import urllib2

if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-u', '--url', action='store', dest='url', help='serving url.')
    arg_parser.add_argument('-r', '--range', action='store', dest='factor_range', help='factor range.')
    args = arg_parser.parse_args()

    data = timestamp = None

    timestamp=int(datetime.datetime.now().strftime('%s'))
    url=args.url
    response = urllib2.urlopen(url)
    data = json.loads(response.read())

    data['newsid'] = filter(lambda x: x.get('des','').find('src=0') > 0,data['newsid'])
    print len(data['newsid'])
    item_list = data['newsid']

    top_age_range = [1,5,10,20]

    range_idx = 0
    idx = 0
    sum_age = 0
    result = []
    for idx in range(len(item_list)):
        cur_age = timestamp - int(item_list[idx].get('fields',{}).get('publish_time',0))
        if idx >= top_age_range[range_idx]:
            result.append(sum_age/(idx+1.0)/3600)
            range_idx += 1
            if range_idx >= len(top_age_range):
                break
        sum_age += cur_age

    print result
