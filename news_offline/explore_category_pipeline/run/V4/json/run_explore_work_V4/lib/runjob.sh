#!/bin/bash
export LD_LIBRARY_PATH=$LDLIBRARY_PATH:/usr/lib/hadoop/lib/native

spark-submit \
	--class com.cmcm.cmnews.userexplore.app.UExpCatNewUserApp \
	--executor-memory 8G \
	--driver-memory 8G \
	--master yarn \
	--name UserExploreApp.aohongbo.V4.json \
	--queue deeplearning \
	--executor-cores 4\
	--num-executors 30 \
	$@
