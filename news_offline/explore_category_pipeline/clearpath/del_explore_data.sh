#!/bin/bash
DEL_DAYS=10

cnt=${DEL_DAYS}
while [[ $cnt -gt 5 ]]
do
	TIME=`date -d "+ ${cnt} days ago"  +%Y%m%d`
	YEAR=${TIME:0:4}
	MONTH=${TIME:4:2}
	DAY=${TIME:6:2}
	echo $TIME
	echo $YEAR
	echo $MONTH
	echo $DAY

	OUTPUT_USER_EXPLORE=hdfs:///projects/news/deeplearning/experiments/explore/aohongbo/V4/json/output/merged/${YEAR}${MONTH}${DAY}/
	OUTPUT_USER_EXPLORE_EN_US=hdfs:///projects/news/deeplearning/experiments/explore/aohongbo/V4/json/output/en_us/${YEAR}${MONTH}${DAY}/
	OUTPUT_USER_EXPLORE_EN_IN=hdfs:///projects/news/deeplearning/experiments/explore/aohongbo/V4/json/output/en_in/${YEAR}${MONTH}${DAY}/
	OUTPUT_USER_EXPLORE_HI_IN=hdfs:///projects/news/deeplearning/experiments/explore/aohongbo/V4/json/output/hi_in/${YEAR}${MONTH}${DAY}/
	OUTPUT_USER_EXPLORE_OTHER=hdfs:///projects/news/deeplearning/experiments/explore/aohongbo/V4/json/output/other/${YEAR}${MONTH}${DAY}/

	hdfs dfs -ls ${OUTPUT_USER_EXPLORE}
	if [[ $? -eq 0 ]];then
		hdfs dfs -rmr ${OUTPUT_USER_EXPLORE}
	fi

	hdfs dfs -ls ${OUTPUT_USER_EXPLORE_EN_US}
	if [[ $? -eq 0 ]];then
		hdfs dfs -rmr ${OUTPUT_USER_EXPLORE_EN_US}
	fi

	hdfs dfs -ls ${OUTPUT_USER_EXPLORE_EN_IN}
	if [[ $? -eq 0 ]];then
		hdfs dfs -rmr ${OUTPUT_USER_EXPLORE_EN_IN}
	fi

	hdfs dfs -ls ${OUTPUT_USER_EXPLORE_HI_IN}
	if [[ $? -eq 0 ]];then
		hdfs dfs -rmr ${OUTPUT_USER_EXPLORE_HI_IN}
	fi

	hdfs dfs -ls ${OUTPUT_USER_EXPLORE_OTHER}
	if [[ $? -eq 0 ]];then
		hdfs dfs -rmr ${OUTPUT_USER_EXPLORE_OTHER}
	fi

	echo ${OUTPUT_USER_EXPLORE}
	echo ${OUTPUT_USER_EXPLORE_EN_US}
	echo ${OUTPUT_USER_EXPLORE_EN_IN}
	echo ${OUTPUT_USER_EXPLORE_HI_IN}
	echo ${OUTPUT_USER_EXPLORE_OTHER}
	cnt=$[ $cnt - 1 ]
done




