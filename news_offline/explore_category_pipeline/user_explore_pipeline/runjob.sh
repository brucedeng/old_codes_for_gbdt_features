#!/bin/bash

chmod u+x  ./target/scala-2.10/*jar
cp ./target/scala-2.10/*jar ../run_explore_work_V4/lib/
cp ./target/scala-2.10/*jar ../run_explore_work_V4_gcm/lib/
PARA="task_parallelism 300 para_parallelism 300 num_of_keyword 200 num_of_cat 200 listsize 3 dropsize 5  kw_version oversea  cat_version oversea_l2 lx_norm yes totalcat 20  out_format json"
VIEW="/projects/news/data/raw_data/impression/20160910/*/*/*"
CLICK="/projects/news/data/raw_data/click/20160910/*/*/*"
CP="/projects/news/cpp/feeder/in_cp_dump/20160910/*/*/*"
UP_PATH_EN_US="hdfs:///projects/news/user_profile/data/V4/up_json/en_us/20160910/*"
UP_PATH_EN_IN="hdfs:///projects/news/user_profile/data/V4/up_json/en_in/20160910/*"
UP_PATH_HI_IN="hdfs:///projects/news/user_profile/data/V4/up_json/hi_in/20160910/*"
OUT_EN_US="/projects/news/deeplearning/experiments/aohongbo/userexplore/output/1/en_us/"
OUT_EN_IN="/projects/news/deeplearning/experiments/aohongbo/userexplore/output/1/en_in/"
OUT_HI_IN="/projects/news/deeplearning/experiments/aohongbo/userexplore/output/1/hi_in/"
OUT_OTHER="/projects/news/deeplearning/experiments/aohongbo/userexplore/output/1/other/"
OUT="/projects/news/deeplearning/experiments/aohongbo/userexplore/output/1/"


hdfs dfs -rmr $OUT


spark-submit \
	--class com.cmcm.cmnews.userexplore.app.UExpCatNewUserApp  \
	--executor-memory 8G \
  --driver-memory 8G \
  --master yarn \
	--name UserExploreApp.aohongbo_test \
	--queue default \
 --executor-cores 4\
 --num-executors 30\
	 ../run_explore_work_V4/lib/userexplore_2.10-1.0.jar ${PARA} view ${VIEW}  cp ${CP}  out ${OUT} up_en_us ${UP_PATH_EN_US} up_en_in ${UP_PATH_EN_IN} up_hi_in ${UP_PATH_HI_IN} click ${CLICK} out_en_us ${OUT_EN_US} out_en_in ${OUT_EN_IN} out_hi_in ${OUT_HI_IN} out_other ${OUT_OTHER}
   
