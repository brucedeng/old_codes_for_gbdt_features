name := "userexplore"

version := "1.0"

scalaVersion := "2.10.4"

libraryDependencies += "org.apache.spark" % "spark-core_2.10" % "1.3.1"
libraryDependencies += "org.json4s" %% "json4s-native" % "3.2.11"
libraryDependencies += "log4j" % "log4j" % "1.2.14"
