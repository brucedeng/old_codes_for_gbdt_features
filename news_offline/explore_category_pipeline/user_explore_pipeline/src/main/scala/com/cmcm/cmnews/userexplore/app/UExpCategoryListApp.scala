package com.cmcm.cmnews.userexplore.app

import com.cmcm.cmnews.userexplore.spark.UExpCategoryListContext
import com.cmcm.cmnews.userexplore.input.UExpCategoryListInput
import com.cmcm.cmnews.userexplore.compute.UExpCategoryListCompute
import com.cmcm.cmnews.userexplore.output.UExpCategoryListOutput


object  UExpCategoryListApp extends App
	with UExpCategoryListContext
	with UExpCategoryListInput
	with UExpCategoryListCompute
	with UExpCategoryListOutput


