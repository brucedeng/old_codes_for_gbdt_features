package com.cmcm.cmnews.userexplore.output

import com.cmcm.cmnews.userexplore.spark.ComputeContext

trait DataOutput{

  this:ComputeContext =>

  def output() {
  }

 }
