package com.cmcm.cmnews.userexplore.spark

import org.apache.spark.{SparkConf,SparkContext}
import scala.util.{Failure, Success, Try}

trait UExpCategoryListContext extends ComputeContext{

  import com.cmcm.cmnews.userexplore.util.{Parameters,CPProcessor,UPProcessor,EventProcessor}
  import Parameters._
  
  override def init():Unit={
    super.init()
    val spConf = new SparkConf()
      .set("spark.serializer","org.apache.spark.serializer.KryoSerializer")
      .set("spark.default.parallelism", Try(getPara(task_parallelism)).getOrElse("32"))
    sbc = new SparkContext(spConf)
  }


  def getRdd[T](key:String):T=getRddStore.get(key) match {
    case Some(rdd) => rdd.asInstanceOf[T]
    case _ => throw new RuntimeException(s"Error when getRDD")
  }

  def getPara(key:String):String=getParameterStore.get(key) match {
    case Some(para) => para
    case _ => throw new RuntimeException(s"Error when getPara")
  }

  def loadRdd(path:String,name:String) = {
    getParameterStore.get(path) match {
      case Some(p) =>
        val rdd = sbc.textFile(p.asInstanceOf[String])
        setRddStore(name,rdd)
      case _ =>
        throw new RuntimeException(s"Error when setRdd")
    }
  }
}

