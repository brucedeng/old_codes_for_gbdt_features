package com.cmcm.cmnews.userexplore.compute

import org.apache.spark.rdd.RDD
import com.cmcm.cmnews.userexplore.util.{Parameters,CPProcessor,UPProcessor,EventProcessor}
import com.cmcm.cmnews.userexplore.spark.UExpCategoryListContext

trait UExpCategoryListCompute extends Compute{

  this : UExpCategoryListContext =>
  import Parameters._

  override def compute()
  {
    super.compute()

    //获取原始数据集cp，up，click，view
    val cpRdd:RDD[String]         = getRdd[RDD[String]](rdd_cp)
    val upRdd:RDD[String]         = getRdd[RDD[String]](rdd_up)
    val eventViewRdd:RDD[String]  = getRdd[RDD[String]](rdd_event_view)
    val eventClickRdd:RDD[String] = getRdd[RDD[String]](rdd_event_click)

    //获取程序运行的参数，pid，并行度，推荐category的数量，热门category排除用户点击过的类别的数量
    val env                       = this.getParameterStore
    val pidSelector               = this.getPara(event_pid)
    val parallelism               = this.getPara(para_parallelism).toInt
    val categorysize              = this.getPara(listsize).toInt
    val ucategorydrop             = this.getPara(dropsize).toInt
    val languageRegion            = this.getPara(lan)

    //根据相应参数过滤cp，up，click，view，并解析相应的RDD
    val parsedCpRdd               = cpRdd.map{line => CPProcessor.parseLine(line,env)}.filter{line=>line._1.compareTo("unknown")!=0}
    val parsedUpRdd               = upRdd.map{line => UPProcessor.parseLine(line,env)}.filter{line=>line._1.compareTo("unknown")!=0}
    val parsedViewRdd             = filterEventPidActLan(eventViewRdd,event_view,pidSelector,languageRegion)
    val parsedClickRdd            = filterEventPidActLan(eventClickRdd,event_click,pidSelector,languageRegion)

    //记录各个category的view和click的数量
    val viewCategoryCnt           = eventCpCategoryCount(parsedViewRdd,parsedCpRdd)
    val clickCategoryCnt          = eventCpCategoryCount(parsedClickRdd,parsedCpRdd)

    //计算各个类别的CTR
    val clickDividView            = clickCategoryCnt.join(viewCategoryCnt,parallelism).map{line => (line._1,line._2._1 / line._2._2)}

    //计算每个用户已点击的类别，按照点击次数排序
    val perUserClickCategory      = upCategoryCount(parsedClickRdd,parsedCpRdd)

    //产生用户推荐category列表
    val broadcastedCtrCat         = getSparkContext.broadcast(clickDividView.collect()) 
    val recommendUserCat          = perUserClickCategory.map(line => {
                                    val userCat = line._2.split(pairDelimiter).map(elem=>{ val kv = elem.split(keyValueDelimiter)
                                    (kv(0),kv(1).toDouble)})
                                    (line._1,userCat)}).flatMap(line=>{
                                    val userCatSet = line._2.map(elem=>elem._1).toSet.take(ucategorydrop)
                                    val highCtrCatSet = broadcastedCtrCat.value.map(elem=>elem._1).toSet
                                    val diffCatSet = highCtrCatSet diff userCatSet
                                    diffCatSet.toArray.map(elem=>(elem,line._1))
                                    }).join(clickDividView,parallelism).map(line=>(line._2._1,(line._1,line._2._2))).groupByKey().map(line=>(line._1,line._2.toArray))
                                    .map(line => {(line._1, line._2.sortWith((elem1,elem2)=>{
                                      if(elem1._2 > elem2._2)
                                        true
                                      else
                                        false
                                    }).take(categorysize))})

     val hotCategory              = clickDividView.collect().sortWith((e1,e2)=>{if(e1._2 > e2._2) true else false}).take(categorysize)
     val broadcastHotcat          = getSparkContext.broadcast(hotCategory)
     val unContainedUser          = parsedUpRdd.map(line=>line._1).subtract(recommendUserCat.map(line=>line._1)).distinct().map(line=>(line,broadcastHotcat.value))
     val recommendList            = recommendUserCat.union(unContainedUser).distinct().map(line=>(line._1,line._2.map(elem=>(elem._1,elem._2.toString))))

     this.setRddStore(rdd_result,recommendList)



    ///////////////////Debug//////////////////////////                                
    //viewCategoryCnt.take(30).foreach(println)
    //println("************************<----------------------> view count = "+viewCategoryCnt.count())
    //clickCategoryCnt.take(30).foreach(println)
    //println("************************<----------------------> click count = "+clickCategoryCnt.count())
    //clickDividView.take(30).foreach(println)
    //println("************************<----------------------> clickDividView count = "+clickDividView.count())
    //perUserClickCategory.take(30).foreach(println)
    //println("************************<----------------------> perUserClickCategory count = "+perUserClickCategory.count())
    //recommendUserCat.take(30).map(line=>(line._1,line._2.map(elem=>elem._1+keyValueDelimiter+elem._2.toString))).map(line=>{(line._1,line._2.reduce{(elem1,elem2)=>elem1+pairDelimiter+elem2})}).foreach(println)
    //println("************************<----------------------> recommendUserCat count = "+ recommendUserCat.count())
    //recommendList.take(30).map(line=>(line._1,line._2.map(elem=>elem._1+keyValueDelimiter+elem._2.toString))).map(line=>{(line._1,line._2.reduce{(elem1,elem2)=>elem1+pairDelimiter+elem2})}).foreach(println)
    //println("************************<----------------------> recommendList count = "+ recommendList.count())
    //dist.foreach(println)
    //println("************************<----------------------> dist count = "+ dist.count())
  }

  def filterEventPidActLan(event:RDD[(String)],act:String,pid:String,lan:String):RDD[(String,(String,String))]={
    val env                       = this.getParameterStore
    event.map{line => EventProcessor.parseLine(line,env)}
    .filter{line=>line._1.compareTo("unknown")!=0}
    .filter{line=>line._2._2.split(fieldDelimiter)(2).compareTo(act)==0}
    .filter{line=>line._2._2.split(fieldDelimiter)(8).compareTo(pid)==0}
    .filter{line=>line._2._2.split(fieldDelimiter)(5).compareTo(lan)==0}
  }

 def eventCpCategoryCount(event:RDD[(String,(String,String))],cp:RDD[(String,(String,String))]):RDD[(String,Double)] = {

    val parallelism               = this.getPara(para_parallelism).toInt

    val joinRdd                   = event.map{line => (line._2._1,(line._1,line._2._2))}.join(cp,parallelism).map(line  => (line._1,line._2._2._1))
    val innerGrpSum               = joinRdd.map{line =>(line._1,line._2.split(pairDelimiter))}
                                    .map{line => (line._1,line._2.filter(elem=>elem.split(keyValueDelimiter).length >= 2))}
                                    .map{line => (line._1,line._2.map(elem => elem.split(keyValueDelimiter)(1).toDouble))}
                                    .map{line => (line._1,line._2.sum)}.distinct(parallelism)

    val categoryWeight            = joinRdd.flatMap{line => line._2.split(pairDelimiter)
                                    .map(elem => (line._1,elem))}.filter{line =>line._2.split(keyValueDelimiter).length >= 2}
                                    .map(line => { val kv:Array[String] = line._2.split(keyValueDelimiter)
                                    (line._1,(kv(0),kv(1).toDouble))})
                                  
    val normalizedRdd             = innerGrpSum.join(categoryWeight,parallelism).map(line =>{
                                    if(line._2._1>0.0)
                                      (line._2._2._1,line._2._2._2/line._2._1)
                                    else
                                      (line._2._2._1,0.0)})

    normalizedRdd.reduceByKey{(val1,val2)=>val1+val2}
 }

 def upCategoryCount(event:RDD[(String,(String,String))],cp:RDD[(String,(String,String))]):RDD[(String,String)] = {

    val parallelism               = this.getPara(para_parallelism).toInt
    val joinRdd                   = event.map{line=>(line._2._1,line._1)}.join(cp,parallelism).map{line=>(line._2._1,line._2._2._1)}////RDD[(uid,category)]

    val innerGrpSum               = joinRdd.map{line =>(line._1,line._2.split(pairDelimiter))}
                                    .map{line => (line._1,line._2.filter(elem=>elem.split(keyValueDelimiter).length >= 2))}
                                    .map{line => (line._1,line._2.map(elem => elem.split(keyValueDelimiter)(1).toDouble))}
                                    .map{line => (line._1,line._2.sum)}.reduceByKey((e1,e2)=>e1+e2)

    val categoryWeight            = joinRdd.flatMap{line => line._2.split(pairDelimiter)
                                    .map(elem => (line._1,elem))}.filter{line =>line._2.split(keyValueDelimiter).length >= 2}
                                    .map(line => { val kv:Array[String] = line._2.split(keyValueDelimiter)
                                    (line._1,(kv(0),kv(1).toDouble))}).map{line=>(line._1 + keyValueDelimiter + line._2._1,line._2._2)}
                                    .reduceByKey((e1,e2)=>e1+e2).map(line=>{
                                    val kv = line._1.split(keyValueDelimiter)
                                    (kv(0),(kv(1),line._2))})
                                  
    val normalizedRdd             = innerGrpSum.join(categoryWeight,parallelism).map(line => {
                                    val key = line._1
                                    val category = line._2._2._1
                                    var weight = 0.0
                                    if(line._2._1> 0)
                                      weight = line._2._2._2/line._2._1
                                    (key,category + keyValueDelimiter + weight.toString)
                                     }).groupByKey(parallelism).map{line=>(line._1,line._2.toArray.sortWith((elem1,elem2)=>
                                     {
                                       val sortedKeya = elem1.split(keyValueDelimiter)(1).toDouble
                                       val sortedKeyb = elem2.split(keyValueDelimiter)(1).toDouble
                                       if(sortedKeya > sortedKeyb)
                                        true
                                       else
                                        false
                                     }))}
    normalizedRdd.map{line => (line._1,line._2.reduce((value1,value2)=>value1+pairDelimiter+value2))}
  }
}
