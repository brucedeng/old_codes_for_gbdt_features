package com.cmcm.cmnews.userexplore.compute

import scala.collection.mutable.ArrayBuffer
import org.apache.spark.rdd.RDD
import com.cmcm.cmnews.userexplore.util.{Parameters,CPProcessor,UPProcessor,EventProcessor}
import com.cmcm.cmnews.userexplore.spark.UExpCategoryListContext

trait UExpCatNewUserCompute extends Compute{

  this : UExpCategoryListContext =>
  import Parameters._




  override def compute()
  {

    super.compute()

    //获取原始数据集cp，up，click，view
    val cpRdd:RDD[String]         = getRdd[RDD[String]](rdd_cp)
    //val upRdd:RDD[String]         = getRdd[RDD[String]](rdd_up)
    val eventViewRdd:RDD[String]  = getRdd[RDD[String]](rdd_event_view)
    val eventClickRdd:RDD[String] = getRdd[RDD[String]](rdd_event_click)

    //获取程序运行的参数，pid，并行度，推荐category的数量，热门category排除用户点击过的类别的数量
    val env                       = this.getParameterStore
    val parallelism               = this.getPara(para_parallelism).toInt
    val categorysize              = this.getPara(listsize).toInt
    val ucategorydrop             = this.getPara(dropsize).toInt


    //根据相应参数过滤cp，up，click，view，并解析相应的RDD
    val parsedCpRdd               = cpRdd.map{line => CPProcessor.parseLine(line,env)}.filter{line=>line._1.compareTo("unknown")!=0}
    val parsedViewRdd             = filterEventByAct(eventViewRdd,event_view)
    val parsedClickRdd            = filterEventByAct(eventClickRdd,event_click)
    val viewFilterByPid           = filterEventByPid(parsedViewRdd,selected_pid)
    val clickFilterByPid          = filterEventByPid(parsedClickRdd,selected_pid)
    val viewFilterByLan           = filterEventByLan(viewFilterByPid,selected_lan)
    val clickFilterByLan          = filterEventByLan(clickFilterByPid,selected_lan)


    //记录各个category的view和click的数量
    val viewCategoryCnt           = eventCpCategoryCount(viewFilterByLan,parsedCpRdd)
    val clickCategoryCnt          = eventCpCategoryCount(clickFilterByLan,parsedCpRdd)

    //计算各个类别的CTR
    val clickDividView            = clickCategoryCnt.join(viewCategoryCnt,parallelism).map{line => (line._1,line._2._1 / line._2._2)}.sortBy(line=>line._2,false,1)


    val upRddEnUs:RDD[String]     = getRdd[RDD[String]](rdd_up_en_us)
    val parsedEnUs                = upRddEnUs.map{line => UPProcessor.parseLine(line,env)}.filter{line=>line._1.compareTo("unknown")!=0}
    val parsedUnknownEnUs         = parsedEnUs.filter{line => line._2._1.compareTo("unknown")==0}
    val parsedKnownEnUs           = parsedEnUs.filter{line => line._2._1.compareTo("unknown")!=0}
    computeKnownCategoryUser(rdd_result_en_us,parsedKnownEnUs,clickDividView)
    computeUnknownCategoryUser(parsedUnknownEnUs,clickDividView)



    val upRddEnIn:RDD[String]    = getRdd[RDD[String]](rdd_up_en_in)
    val parsedEnIn               = upRddEnIn.map{line => UPProcessor.parseLine(line,env)}.filter{line=>line._1.compareTo("unknown")!=0}
    val parsedUnknownEnIn        = parsedEnIn.filter{line => line._2._1.compareTo("unknown")==0}
    val parsedKnownEnIn          = parsedEnIn.filter{line => line._2._1.compareTo("unknown")!=0}
    computeKnownCategoryUser(rdd_result_en_in,parsedKnownEnIn,clickDividView)

    val upRddHiIn:RDD[String]    = getRdd[RDD[String]](rdd_up_hi_in)
    val parsedHiIn               = upRddHiIn.map{line => UPProcessor.parseLine(line,env)}.filter{line=>line._1.compareTo("unknown")!=0}
    val parsedUnknownHiIn        = parsedHiIn.filter{line => line._2._1.compareTo("unknown")==0}
    val parsedKnownHiIn          = parsedHiIn.filter{line => line._2._1.compareTo("unknown")!=0}
    computeKnownCategoryUser(rdd_result_hi_in,parsedKnownHiIn,clickDividView)

  }


 
  def computeKnownCategoryUser(resultname:String,knownCatUser:RDD[(String,(String,String))],clickDividView:RDD[(String,Double)])
  {
    //获取程序运行的参数，pid，并行度，推荐category的数量，热门category排除用户点击过的类别的数量
    val parallelism               = this.getPara(para_parallelism).toInt
    val categorysize              = this.getPara(listsize).toInt
    val ucategorydrop             = this.getPara(dropsize).toInt
    val takeSize                  = this.getPara(totalcat).toInt

    val broadcastFullClkDivView   = getSparkContext.broadcast(clickDividView.collect())
    val knownUserRecommendCat     = knownCatUser.map(line=>(line._1,line._2._1))
                                                 .map(line=>(line._1,line._2.split(pairDelimiter)))
                                                 .map(line=>(line._1,line._2.map(elem=>{val kv = elem.split(keyValueDelimiter)
                                                 (kv(0),kv(1))})))
                                                 .map(line=>(line._1,line._2.sortWith((e1,e2)=>{
                                                 if(e1._2.toDouble > e2._2.toDouble)
                                                   true
                                                 else
                                                   false})))
                                                 .map(line=>(line._1,line._2.take(ucategorydrop)))
                                                 .flatMap(line=>{
                                                   val hotCat = broadcastFullClkDivView.value.map(elem=>elem._1).toSet
                                                   val uCat   = line._2.map(elem=>elem._1).toSet
                                                   val selCat = hotCat.diff(uCat)
                                                   selCat.toArray.map(elem=>(elem,line._1))})

     val RecommendCatForTagUser     = clickDividView.join(knownUserRecommendCat,parallelism).map(line=>(line._2._2,(line._1,line._2._1)))
                                                   .groupByKey().map(line=>(line._1,line._2.toArray))
                                                   .map(line=>(line._1,line._2.sortWith((elem1,elem2) =>{
                                                     if(elem1._2.toDouble > elem2._2.toDouble)
                                                        true
                                                      else
                                                        false}).take(takeSize)))
                                                   .flatMap(line=>{
                                                          val randomIdx = scala.collection.mutable.Set[Int]()
                                                          while(randomIdx.size<categorysize)
                                                          {
                                                              val e = scala.util.Random.nextInt().abs % takeSize
                                                              if(!randomIdx(e))
                                                                randomIdx += e
                                                          }
  
                                                          val selectedCat = ArrayBuffer[(String,Double)]()
                                                          for(e<-randomIdx)
                                                            selectedCat += line._2(e)
  
                                                          selectedCat.toArray.map(e=>(line._1,(e._1,e._2.toString)))}).distinct()
                                                    .groupByKey().map(line=>(line._1,line._2.toArray))
                                                    .map(line=>(line._1,line._2.sortWith((elem1,elem2) =>{
                                                          if(elem1._2.toDouble > elem2._2.toDouble)
                                                            true
                                                          else
                                                            false
                                                     })))

  this.setRddStore(resultname,RecommendCatForTagUser)
  }
   


  def computeUnknownCategoryUser(unknownCatUser:RDD[(String,(String,String))],clickDividView:RDD[(String,Double)])
  {
    //获取程序运行的参数，pid，并行度，推荐category的数量，热门category排除用户点击过的类别的数量
    val parallelism               = this.getPara(para_parallelism).toInt
    val categorysize              = this.getPara(listsize).toInt
    val ucategorydrop             = this.getPara(dropsize).toInt
    val takeSize                  = this.getPara(totalcat).toInt

    val clkDivViewTake            = clickDividView.take(takeSize)
    val broadcastClkDivView       = getSparkContext.broadcast(clkDivViewTake) 
    val unknownUserRecommendCat   = unknownCatUser.flatMap(line=>{
                                                    val randomIdx = scala.collection.mutable.Set[Int]()
                                                    while(randomIdx.size<categorysize)
                                                    {
                                                        val elem = scala.util.Random.nextInt().abs % takeSize
                                                        if(!randomIdx(elem))
                                                           randomIdx += elem
                                                    }

                                                    val selectedCat = ArrayBuffer[(String,Double)]()
                                                    for(elem<-randomIdx)
                                                       selectedCat += broadcastClkDivView.value(elem)

                                                    selectedCat.toArray.map(elem=>(line._1,(elem._1,elem._2.toString)))}).distinct()
                                                    .groupByKey().map(line=>(line._1,line._2.toArray))
                                                    .map(line=>(line._1,line._2.sortWith((elem1,elem2) =>{
                                                        if(elem1._2.toDouble > elem2._2.toDouble)
                                                          true
                                                        else
                                                          false
                                                     })))

  this.setRddStore(rdd_result_other,unknownUserRecommendCat)
    ///////////////////Debug//////////////////////////                                
    //viewCategoryCnt.take(30).foreach(println)
    //println("************************<----------------------> view count = "+viewCategoryCnt.count())
    //clickCategoryCnt.take(30).foreach(println)
    //println("************************<----------------------> click count = "+clickCategoryCnt.count())
    //clickDividView.take(50).foreach(println)
    //println("************************<----------------------> clickDividView count = "+clickDividView.count())
    //perUserClickCategory.take(30).foreach(println)
    //println("************************<----------------------> perUserClickCategory count = "+perUserClickCategory.count())
    //recommendUserCat.take(30).map(line=>(line._1,line._2.map(elem=>elem._1+keyValueDelimiter+elem._2.toString))).map(line=>{(line._1,line._2.reduce{(elem1,elem2)=>elem1+pairDelimiter+elem2})}).foreach(println)
    //println("************************<----------------------> recommendUserCat count = "+ recommendUserCat.count())
    //RecommendCatForTagUser.take(50).map(line=>(line._1,line._2.map(elem=>elem._1+keyValueDelimiter+elem._2.toString))).map(line=>{(line._1,line._2.reduce{(elem1,elem2)=>elem1+pairDelimiter+elem2})}).foreach(println)
    //println("************************<----------------------> recommendList count = "+ RecommendCatForTagUser.count())
    //dist.foreach(println)
    //println("************************<----------------------> dist count = "+ dist.count())
  }

  def filterEventByAct(event:RDD[(String)],act:String):RDD[(String,(String,String))]={
    val env                       = this.getParameterStore
    event.map{line => EventProcessor.parseLine(line,env)}
         .filter{line=>line._1.compareTo("unknown")!=0}
         .filter{line=>line._2._2.split(fieldDelimiter)(2).compareTo(act)==0}
  }

 def filterEventByPid(event:RDD[(String,(String,String))],pid:List[String])={
   event.filter(line=>{
                       val field = line._2._2.split(fieldDelimiter)(8)
                       var result = false
                       for(e <- pid)
                         result = result || (field.compareTo(e)==0)
                       result})
 }

 def filterEventByLan(event:RDD[(String,(String,String))],lan:List[String])={
   event.filter(line=>{
                       val field = line._2._2.split(fieldDelimiter)(5)
                       var result = false
                       for(e <- lan)
                         result = result || (field.compareTo(e)==0)
                       result})
 }

 def filterEventByLan(event:RDD[(String,(String,String))],lan:String)={
   event.filter(line=>line._2._2.split(fieldDelimiter)(5).compareTo(lan)==0)
 }


 def eventCpCategoryCount(event:RDD[(String,(String,String))],cp:RDD[(String,(String,String))]):RDD[(String,Double)] = {
       val parallelism               = this.getPara(para_parallelism).toInt
    
       val joinRdd                   = event.map{line => (line._2._1,(line._1,line._2._2))}.join(cp,parallelism).map(line  => (line._1,line._2._2._1))
       val innerGrpSum               = joinRdd.map{line =>(line._1,line._2.split(pairDelimiter))}
                                              .map{line => (line._1,line._2.filter(elem=>elem.split(keyValueDelimiter).length >= 2))}
                                              .map{line => (line._1,line._2.map(elem => elem.split(keyValueDelimiter)(1).toDouble))}
                                              .map{line => (line._1,line._2.sum)}.distinct(parallelism)
   
       val categoryWeight            = joinRdd.flatMap{line => line._2.split(pairDelimiter)
                                              .map(elem => (line._1,elem))}.filter{line =>line._2.split(keyValueDelimiter).length >= 2}
                                              .map(line => { val kv:Array[String] = line._2.split(keyValueDelimiter)
                                              (line._1,(kv(0),kv(1).toDouble))})
  
       val normalizedRdd             = innerGrpSum.join(categoryWeight,parallelism).map(line =>{
                                      if(line._2._1>0.0)
                                        (line._2._2._1,line._2._2._2/line._2._1)
                                      else
                                       (line._2._2._1,0.0)})
 
       normalizedRdd.reduceByKey{(val1,val2)=>val1+val2}
 }

}
