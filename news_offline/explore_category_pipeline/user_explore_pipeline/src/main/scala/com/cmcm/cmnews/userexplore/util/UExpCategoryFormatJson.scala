package com.cmcm.cmnews.userexplore.util

import org.apache.spark.rdd.RDD

object UExpCategoryFormatJson extends  Serializable{

   def postProcess(rdd:RDD[(String,Array[(String,String)])]):RDD[String]={
     rdd.map(line=>(line._1,line._2.map(e=>formatPair(e))))
        .map(line=>(line._1,line._2.reduce((x,y)=>x+","+y)))
        .map(line=>(line._1,"\"explore_category\""+":"+"["+line._2+"]"))
        .map(line=>"{"+"\"aid\""+" : "+parseElem(line._1)+","+line._2+"}")
   }
 
   def formatPair(value:(String,String)):String={
     val first = "\"name\""+" : " +parseElem(value._1)
     val second = "\"weight\""+" : " +parseElem(value._2.toDouble)
     val result ="{"+first+","+second+"}"
     result
   }
 
   def parseElem(value:Any):String={
     value match {
       case x:String => "\""+x+"\""
       case y:Double => y.toString
       case z:Any    => "None"
     }
   }
}
