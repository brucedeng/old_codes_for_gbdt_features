package com.cmcm.cmnews.userexplore.util

import org.apache.spark.rdd.RDD
import org.json4s.JsonAST._

import scala.util.{Failure, Success, Try}
import scala.util.control.Breaks._


object  UPProcessor extends Serializable {

  import Parameters._

  def parseLine(line: String, para: collection.mutable.Map[String, String]) = {
    val topRelCat = Try(para(num_of_keyword)).getOrElse("100").toInt

    Try(JsonUtil.convertToJValue(line)) match {
      case Success(jvalue) =>
        val uuid = extractStr(jvalue, "uid", "unknown")
        val categories = jvalue \ "categories" match {
          case JArray(cats) => catKwAssist(cats.toList, "", topRelCat, "unknown")
          case _ => "unknown"
        }
        val gender = extractStr(jvalue, "gender", "unknown")
        val age = extractStr(jvalue, "age", "unknown")
        val catLen = extractStr(jvalue, "u_cat_len", "0")
        val kwLen = extractStr(jvalue, "u_kw_len", "0")

        (uuid, (categories, gender + fieldDelimiter + age + fieldDelimiter + catLen + fieldDelimiter + kwLen))
      case Failure(ex) =>
        ("unknown", ("unknown",  "unknown" + fieldDelimiter + "unknown" + fieldDelimiter + "0" + fieldDelimiter + "0"))
    }
  }

  def catKwAssist(jsonItems: List[JValue], version: String, top: Int, default: String): String = {
    def parse_item(item: JValue) = {
      val name = (item \ "name").asInstanceOf[JString].s
      val L1_weight = item \ "weight" match {
        case JDouble(x) =>
          x.toDouble
        case JInt(x) =>
          x.toDouble
        case _ =>
          0.0
      }
      name + keyValueDelimiter + L1_weight
    }
    if(jsonItems.size>0)
      jsonItems.map(x => parse_item(x)).take(top).mkString(fieldDelimiter)
    else
      default
  }

  def extractStr(jvalue: JValue, fieldName: String, default: String = ""):String = {
    jvalue \ fieldName match {
      case JString(s) => if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString()
      case _ => default
    }
  }

  def getListFromJson(key: String, line: String, top: Int): String = {
    val sb = new StringBuilder
    var newLine = line.substring(line.indexOf("\"" + key + "\" : ") + 5 + key.length)
    newLine = newLine.substring(0, newLine.indexOf("]") + 1)
    var index = 0
    breakable {
      while (index < (if (top < 0) Integer.MAX_VALUE else top)) {
        if (newLine.indexOf("}") == -1) {
          break()
        }
        val name = getValueForKey("name", newLine)
        val weight = getValueForKey("weight", newLine).toDouble
        sb.append(name + keyValueDelimiter + weight + pairDelimiter)
        newLine = newLine.substring(newLine.indexOf("}") + 1)
        index += 1
      }
    }
    if (index > 0)
      sb.dropRight(1).toString()
    else
      sb.toString()
  }

  def getValueForKey(key: String, record: String) = {
    val startIndex = record.indexOf("\"" + key + "\" : ") + 5 + key.length
    val commaIndex = record.indexOf(",", startIndex)
    val antiBracketIndex = record.indexOf("}", startIndex)
    val endIndex = if (commaIndex != -1 && antiBracketIndex != -1) Math.min(record.indexOf(",", startIndex),
      record.indexOf("}", startIndex))
    else if (commaIndex != -1) commaIndex else antiBracketIndex
    val mendedStartInedex =
      if (record.charAt(startIndex) == '\"' || record.charAt(startIndex) == '[')
        startIndex + 1
      else
        startIndex
    val mendedEndInedex =
      if (record.charAt(endIndex - 1) == '\"' || record.charAt(endIndex - 1) == ']')
        endIndex - 1
      else
        endIndex

    if (mendedStartInedex > mendedEndInedex)
      ""
    else
      record.substring(mendedStartInedex, mendedEndInedex)
  }

}
