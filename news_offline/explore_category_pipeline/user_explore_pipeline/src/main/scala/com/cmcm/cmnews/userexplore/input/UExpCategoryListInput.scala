package com.cmcm.cmnews.userexplore.input

import com.cmcm.cmnews.userexplore.util.Parameters
import com.cmcm.cmnews.userexplore.spark.UExpCategoryListContext


trait UExpCategoryListInput extends DataInput {

  this:UExpCategoryListContext=>
  import Parameters._

  override def getInput()
  {
    super.getInput()
    loadRdd(para_event_view,rdd_event_view)
    loadRdd(para_event_click,rdd_event_click)
    loadRdd(para_cp,rdd_cp)
    //UP分多个版本
    loadRdd(para_up_en_us,rdd_up_en_us)
    loadRdd(para_up_en_in,rdd_up_en_in)
    loadRdd(para_up_hi_in,rdd_up_hi_in)
  }
}

