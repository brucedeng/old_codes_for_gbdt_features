package com.cmcm.cmnews.userexplore.output


import com.cmcm.cmnews.userexplore.util.UExpCategoryFormatTab
import com.cmcm.cmnews.userexplore.util.UExpCategoryFormatJson
import org.apache.spark.rdd.RDD
import com.cmcm.cmnews.userexplore.util.Parameters
import com.cmcm.cmnews.userexplore.spark.UExpCategoryListContext


trait UExpCategoryListOutput extends DataOutput {

  this:UExpCategoryListContext =>
  import Parameters._


  override def output()
  {
    super.output()

    val outpathenus = this.getPara(out_en_us)
    val outrddenus=this.getRdd[RDD[(String,Array[(String,String)])]](rdd_result_en_us)
    outputRdd(outrddenus,outpathenus)
    val addSuffixEnus = keyAddSuffix(outrddenus,"_en_us")

    val outpathenin = this.getPara(out_en_in)
    val outrddenin=this.getRdd[RDD[(String,Array[(String,String)])]](rdd_result_en_in)
    outputRdd(outrddenin,outpathenin)
    val addSuffixEnin = keyAddSuffix(outrddenin,"_en_in")

    val outpathhiin = this.getPara(out_hi_in)
    val outrddhiin=this.getRdd[RDD[(String,Array[(String,String)])]](rdd_result_hi_in)
    outputRdd(outrddhiin,outpathhiin)
    val addSuffixHiin = keyAddSuffix(outrddhiin,"_hi_in")

    val outpathother = this.getPara(out_other)
    val outrddother=this.getRdd[RDD[(String,Array[(String,String)])]](rdd_result_other)
    outputRdd(outrddother,outpathother)
    val addSuffixOther = keyAddSuffix(outrddother,"_other")

    val result=addSuffixEnus.union(addSuffixEnin).union(addSuffixHiin).union(addSuffixOther)
    val outpath = this.getPara(out)
    outputRdd(result,outpath)

  }

  def keyAddSuffix(rdd:RDD[(String,Array[(String,String)])],suffix:String):RDD[(String,Array[(String,String)])] =
  {
      rdd.map(line=>(line._1+pairDelimiter+suffix,line._2))
  }

  def outputRdd(rdd:RDD[(String,Array[(String,String)])],outpath:String)
  {

      if(this.getPara(out_format).compareTo("tsv")!=0 && this.getPara(out_format).compareTo("json")!=0)
        throw new RuntimeException("Bad output format.")

      val resultrdd = this.getPara(out_format) match {
        case "tsv"  => UExpCategoryFormatTab.postProcess(rdd).asInstanceOf[RDD[String]]
        case "json" => UExpCategoryFormatJson.postProcess(rdd).asInstanceOf[RDD[String]]
      }
      resultrdd.saveAsTextFile(outpath)
  }

}
