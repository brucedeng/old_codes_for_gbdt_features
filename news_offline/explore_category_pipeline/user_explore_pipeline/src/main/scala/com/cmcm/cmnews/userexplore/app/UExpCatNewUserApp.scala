package com.cmcm.cmnews.userexplore.app

import com.cmcm.cmnews.userexplore.spark.UExpCategoryListContext
import com.cmcm.cmnews.userexplore.input.UExpCategoryListInput
import com.cmcm.cmnews.userexplore.compute.UExpCatNewUserCompute
import com.cmcm.cmnews.userexplore.output.UExpCategoryListOutput


object  UExpCatNewUserApp extends App
	with UExpCategoryListContext
	with UExpCategoryListInput
	with UExpCatNewUserCompute
	with UExpCategoryListOutput


