package com.cmcm.cmnews.userexplore.app

import com.cmcm.cmnews.userexplore.spark.ComputeContext
import com.cmcm.cmnews.userexplore.input.DataInput
import com.cmcm.cmnews.userexplore.compute.Compute
import com.cmcm.cmnews.userexplore.output.DataOutput

trait App{

  this:ComputeContext
        with DataInput
        with Compute
        with DataOutput =>


  def initApp():Unit ={
   init() 
  }


  def processApp():Unit={
    getInput
    compute
    output
  }


  def main(args:Array[String]) = {
    var key = ""
    for(item <- args){
      if(key.isEmpty) 
        key = item
      else{
        parameterStore += (key->item)
        println(key+" -> "+item)
        key = ""
      }
    }
    initApp()
    processApp()
  }

}
