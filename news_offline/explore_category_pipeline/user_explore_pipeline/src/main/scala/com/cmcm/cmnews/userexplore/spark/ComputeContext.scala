package com.cmcm.cmnews.userexplore.spark

import org.apache.spark.SparkContext

trait ComputeContext {

  @transient protected var sbc:SparkContext = _
  protected val rddStore:scala.collection.mutable.Map[String,Any]=scala.collection.mutable.Map[String,Any]()
  protected val parameterStore:scala.collection.mutable.Map[String,String]=scala.collection.mutable.Map[String,String]()
  
	def init() = {
	}

  def getSparkContext:SparkContext = sbc
  def getParameterStore = parameterStore
  def getRddStore = rddStore

  def setRddStore(key:String,value:Any)=rddStore +=(key->value)
  def setParameterStore(key:String,value:String)=parameterStore +=(key->value)

}
