package com.cmcm.cmnews.userexplore.compute

import com.cmcm.cmnews.userexplore.spark.ComputeContext

trait Compute{
  this:ComputeContext =>
  
  def compute(){
  }

}
