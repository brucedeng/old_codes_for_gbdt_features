package com.cmcm.cmnews.userexplore.util

import org.apache.spark.rdd.RDD
import org.json4s.JsonAST._

import scala.util.{Failure, Success, Try}
import scala.util.control.Breaks._


object EventProcessor extends Serializable{

  import Parameters._

  def parseLine(line: String, para: collection.mutable.Map[String, String]) = {

    Try(JsonUtil.convertToJValue(line)) match {
      case Success(jvalue) =>
        val uid = extractStr(jvalue, "aid", "unknown")
        val lan = extractStr(jvalue, "app_lan", "en")
        val country = extractStr(jvalue, "country", "")
        val cid = extractStr(jvalue, "contentid", "unknown")
        val ip = extractStr(jvalue, "ip", "unknown")
        val ts = extractStr(jvalue, "servertime_sec", "0").toLong
        val pid = extractStr(jvalue, "pid", "unknown")
        val ctype = extractStr(jvalue, "ctype", "")
        val act = extractStr(jvalue, "act", "unknown")
        val city = extractStr(jvalue, "city", "unknown")
        val des = extractObjectStr(jvalue, "cpack", "des", "unknown")
        val dwelltime = extractObjectStr(jvalue, "ext", "dwelltime", "0").toInt
        val level1Type = extractObjectStr(jvalue, "scenario", "level1_type", "unknown")
        val level1 = extractObjectStr(jvalue, "scenario", "level1", "unknown")
        val lanRegion = lan + "_" + country

        val reqid = parseRid(des)
        var value = List(uid,ctype, act,level1,level1Type, lan,cid, ts, pid, city, reqid).mkString(fieldDelimiter)
        (uid,(cid, value))
      case Failure(ex) =>
        ("unknown",("unknown", List("unknown","0","0","unknown","unknown","unknown","unknown","0","unknown","unknown","unknown").mkString(fieldDelimiter)))
    }
  }

  def catKwAssist(jsonItems: List[JValue], version: String, top: Int, default: String): String = {
    def parse_item(item: JValue) = {
      val name = (item \ "name").asInstanceOf[JString].s
      val L1_weight = item \ "weight" match {
        case JDouble(x) =>
          x.toDouble
        case JInt(x) =>
          x.toDouble
        case _ =>
          0.0
      }
      name + keyValueDelimiter + L1_weight
    }
    jsonItems.map(x => parse_item(x)).take(top).mkString(fieldDelimiter)
  }

  def extractStr(jvalue: JValue, fieldName: String, default: String = ""):String = {
    jvalue \ fieldName match {
      case JString(s) => if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString()
      case _ => default
    }
  }

  def extractObjectStr(jvalue : JValue, objectName: String, fieldName:String, default:  String = "") = {
    jvalue \ objectName \ fieldName match {
      case JString(s) =>  if (s.trim.isEmpty) default else s
      case JInt(s) => s.toString()
      case JDouble(s) => s.toString()
      case _ => default
    }
  }

  def parseRid(des: String) = {
    val data = des.split("\\|")
    val dataMap = scala.collection.mutable.Map[String, String]()
    for (kv <- data) {
      val tArray = kv.split("=")
      if (2 == tArray.size ) {
        dataMap.update(tArray(0), tArray(1))
      }
    }
    dataMap.getOrElse("rid", "")
  }
                                      

  def getListFromJson(key: String, line: String, top: Int): String = {
    val sb = new StringBuilder
    var newLine = line.substring(line.indexOf("\"" + key + "\" : ") + 5 + key.length)
    newLine = newLine.substring(0, newLine.indexOf("]") + 1)
    var index = 0
    breakable {
      while (index < (if (top < 0) Integer.MAX_VALUE else top)) {
        if (newLine.indexOf("}") == -1) {
          break()
        }
        val name = getValueForKey("name", newLine)
        val weight = getValueForKey("weight", newLine).toDouble
        sb.append(name + keyValueDelimiter + weight + pairDelimiter)
        newLine = newLine.substring(newLine.indexOf("}") + 1)
        index += 1
      }
    }
    if (index > 0)
      sb.dropRight(1).toString()
    else
      sb.toString()
  }

  def getValueForKey(key: String, record: String) = {
    val startIndex = record.indexOf("\"" + key + "\" : ") + 5 + key.length
    val commaIndex = record.indexOf(",", startIndex)
    val antiBracketIndex = record.indexOf("}", startIndex)
    val endIndex = if (commaIndex != -1 && antiBracketIndex != -1) Math.min(record.indexOf(",", startIndex),
      record.indexOf("}", startIndex))
    else if (commaIndex != -1) commaIndex else antiBracketIndex
    val mendedStartInedex =
      if (record.charAt(startIndex) == '\"' || record.charAt(startIndex) == '[')
        startIndex + 1
      else
        startIndex
    val mendedEndInedex =
      if (record.charAt(endIndex - 1) == '\"' || record.charAt(endIndex - 1) == ']')
        endIndex - 1
      else
        endIndex

    if (mendedStartInedex > mendedEndInedex)
      ""
    else
      record.substring(mendedStartInedex, mendedEndInedex)
  }

}
