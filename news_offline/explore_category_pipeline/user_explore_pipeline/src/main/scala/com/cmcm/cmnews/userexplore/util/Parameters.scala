package com.cmcm.cmnews.userexplore.util

object Parameters{

  val para_parallelism             = "para_parallelism"
  val para_event_click             = "click"
  val para_event_view              = "view"
  val para_event_read              = "readtime"
  val para_cp                      = "cp"
  val para_up                      = "up"
  val para_up_en_us                = "up_en_us"
  val para_up_en_in                = "up_en_in"
  val para_up_hi_in                = "up_hi_in"

  val rdd_event_click              = "rdd_click"
  val rdd_event_view               = "rdd_view"
  val rdd_event_read               = "rdd_readtime"
  val rdd_cp                       = "rdd_cp"
  val rdd_up                       = "rdd_up"
  val rdd_up_en_us                 = "rdd_up_en_us"
  val rdd_up_en_in                 = "rdd_up_en_in"
  val rdd_up_hi_in                 = "rdd_up_hi_in"
  val rdd_up_other                 = "rdd_up_other"
  val rdd_result                   = "rdd_result"
  val rdd_result_en_us             = "rdd_result_en_us"
  val rdd_result_en_in             = "rdd_result_en_in"
  val rdd_result_hi_in             = "rdd_result_hi_in"
  val rdd_result_other             = "rdd_result_other"
  val out                          = "out"
  val out_en_us                    = "out_en_us"
  val out_en_in                    = "out_en_in"
  val out_hi_in                    = "out_hi_in"
  val out_other                    = "out_other"

  val num_of_keyword               = "num_of_keyword"
  val num_of_cat                   = "num_of_cat"
  val kw_version                   = "kw_version"
  val cat_version                  = "cat_version"
  val lx_norm                      = "lx_norm"

  val event_pid                    = "event_pid"
  val lan                          = "lan"

  val selected_pid                 = List("1","3","9","11","14","17")
  val selected_lan                 = List("en","hi")

  val fieldDelimiter               = "\003" 
  val keyValueDelimiter            = "\002"
  val pairDelimiter                = "\001"
  

  val event_view                   = "1"
  val event_click                  = "2"
  val task_parallelism             = "task_parallelism"
  val listsize                     = "listsize"
  val dropsize                     = "dropsize"

  val totalcat                     = "totalcat"
  val out_format                   = "out_format"
}
