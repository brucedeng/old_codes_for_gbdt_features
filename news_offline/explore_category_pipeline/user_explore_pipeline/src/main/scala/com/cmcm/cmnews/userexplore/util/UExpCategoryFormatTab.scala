package com.cmcm.cmnews.userexplore.util

import org.apache.spark.rdd.RDD

object UExpCategoryFormatTab extends  Serializable{

   def postProcess(rdd:RDD[(String,Array[(String,String)])]):RDD[String]={
     rdd.map(line=>(line._1,line._2.map(e=>formatPair(e))))
        .map(line=>(line._1,line._2.reduce((x,y)=>x+","+y)))
        .map(line=>line._1+"\t"+line._2)
   }
 
   def formatPair(value:(String,String)):String={
     parseElem(value._1)+":"+parseElem(value._2)
   }
 
   def parseElem(value:Any):String={
     value match {
       case x:String => x
       case y:Double => y.toString
       case z:Any    => "None"
     }
   }
}
