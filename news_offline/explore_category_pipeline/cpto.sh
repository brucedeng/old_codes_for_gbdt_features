#!/bin/bash
cp /data1/aohongbo/work_4/user_explore_pipeline/target/scala-2.10/*jar ./userexplore_2.10-1.0.jar

cp ./*jar   /data1/aohongbo/work_4/run/V4/json/run_explore_work_V4/lib/
cp ./*jar   /data1/aohongbo/work_4/run/V4/tsv/run_explore_work_V4/lib/
#cp ./*jar   /data1/aohongbo/work_4/run/V4_gcm/json/run_explore_work_V4_gcm/lib/
#cp ./*jar   /data1/aohongbo/work_4/run/V4_gcm/tsv/run_explore_work_V4_gcm/lib/
rm ./*jar

echo "Begin"
cd /data1/aohongbo/work_4/run/V4/json/run_explore_work_V4
cat coordinator.properties | sed '/start_time/d' |sed  '/end_time/i\start_time=2016-09-11T00:00Z' > tmp
rm coordinator.properties
mv tmp coordinator.properties
chmod 777 coordinator.properties
./oo.sh run
cd -

cd /data1/aohongbo/work_4/run/V4/tsv/run_explore_work_V4
cat coordinator.properties | sed '/start_time/d' |sed  '/end_time/i\start_time=2016-09-11T00:00Z' > tmp
rm coordinator.properties
mv tmp coordinator.properties
chmod 777 coordinator.properties
./oo.sh run 
cd -

#cd /data1/aohongbo/work_4/run/V4_gcm/json/run_explore_work_V4_gcm
#cat coordinator.properties | sed '/start_time/d' |sed  '/end_time/i\start_time=2016-09-09T00:00Z' > tmp
#rm coordinator.properties
#mv tmp coordinator.properties
#chmod 777 coordinator.properties
#./oo.sh run
#cd -

#cd /data1/aohongbo/work_4/run/V4_gcm/tsv/run_explore_work_V4_gcm
#cat coordinator.properties | sed '/start_time/d' |sed  '/end_time/i\start_time=2016-09-09T00:00Z' > tmp
#rm coordinator.properties
#mv tmp coordinator.properties
#chmod 777 coordinator.properties
#./oo.sh run
#cd - 
echo "End"
