REGISTER 'lib/*.jar';
REGISTER 'feature_log_util.py' USING jython AS myfunc;

set mapred.create.symlink yes;
set mapred.cache.files $FEATMAP_FILE#feature.map
set default_parallel 400;
set fs.s3a.connection.maximum 1000;

-- DEFINE rcvLoader com.ijinshan.cmcm.rcv.log.loader.rcvLogLoader('$ORI_COL','&', '=');
-- raw_rcv_log = LOAD '$INPUT_EVENT' USING rcvLoader AS ($TYPE_COL);

raw_rcv_log = LOAD '$INPUT_EVENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate
    json,
    (json#'app_lan' is null ? 'en' : ( json#'app_lan' MATCHES 'hi.*' ? 'hi' : 'en' )) as app_lan,
    json#'contentid' as content_id:chararray,
    json#'aid' as uid:chararray,
    json#'ip' as ip:chararray,
    json#'servertime_sec' as eventtime,
    json#'pid' as pid:chararray,
    json#'ctype' as ctype,
    (int)json#'ext'#'dwelltime' AS dwelltime:int,
    json#'act' as act,
    myfunc.parse_rid(json#'cpack') as rid:chararray;

-- raw_rcv_log = filter raw_rcv_log by pid == '11' and  ( ctype is not null and (ctype=='1' or ctype=='0x01' ) ) and eventtime >= myfunc.date2timestamp('$START_DATE_TIME',$WND,'%Y%m%d%H') and eventtime <= myfunc.date2timestamp('$END_DATE_TIME', $WND,'%Y%m%d%H') and act is not null and ( act == '1' or act == '2' or act=='3' or act=='4' ) and json#'scenario'#'level1_type' =='1' and json#'scenario'#'level1' == '1' and app_lan=='$app_lan';

raw_rcv_log = filter raw_rcv_log by pid == '11' and  ( ctype is not null and (ctype=='1' or ctype=='0x01' ) ) and act is not null and ( act == '1' or act == '2' or act=='3' or act=='4' ) and json#'scenario'#'level1_type' =='1' and json#'scenario'#'level1' == '1' and app_lan=='$app_lan';

click_view_data = FOREACH raw_rcv_log GENERATE
    content_id, uid, rid,
    -- myfunc.get_city_name(ip) as city_name,
    (long)(eventtime) as original_time,
    (long)(eventtime)/$WND * $WND as updatetime,
    pid as productid,
    ((dwelltime is null or dwelltime<0) ? 0 : dwelltime ) as dwelltime,
    (act=='1'? 'view':( act=='2'? 'click':( act=='4'? 'read': (act=='3'? 'listpagedwell':'unknown')))) as event_type;

log_click_view = FOREACH click_view_data GENERATE
      uid AS uid,
      content_id AS content_id,
      rid as rid,
      original_time as original_ts,
      updatetime AS ts,
      -- city_name AS city,
      event_type AS event_type,
      productid as productid,
      (event_type=='read' ? dwelltime:0) as dwelltime:int,
      (event_type=='listpagedwell' ? dwelltime:0 ) as listpagedwell:int,
      1.0 AS expression;

log_click_view = FILTER log_click_view BY (content_id matches '^\\d+$') and (ts != -1);

raw_event_group = GROUP log_click_view BY (uid, content_id, productid, rid); 

raw_event_out = FOREACH raw_event_group {
    ts = MIN( $1.ts );
    join_ts = ts/$WND * $WND - $WND;
    click_event = FILTER $1 BY (event_type == 'click');
    click_count = COUNT(click_event);
    label = (click_count > 0 ? 1: 0);
    dwelltime = SUM($1.dwelltime);
    listpagedwell = SUM($1.listpagedwell);
    -- city = MIN($1.city);
GENERATE
    $0.uid AS uid,
    $0.content_id AS content_id,
    $0.productid as productid,
    $0.rid as rid,
    ts AS ts,
    join_ts AS join_ts,
    -- city AS city,
    (dwelltime > 600 ? 600:dwelltime) as dwelltime,
    (listpagedwell > 600 ? 600:listpagedwell) as listpagedwell,
    label AS label;
}

-- rmf $DEBUG_OUTPUT/raw_event_out
-- store raw_event_out into '$DEBUG_OUTPUT/raw_event_out';

feature_log = LOAD '$INPUT_FEATURE_LOG' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

feature_log = FOREACH feature_log GENERATE 
    json#'req_id' as rid:chararray,
    json#'prod_id' as pid:chararray,
    json#'predictor_id' as predictor_id:chararray,
    json#'now' as ts:long,
    json#'doc_size' as doc_size:long,
    json#'uid' as uid:chararray,
    json#'up' as up:map[],
    FLATTEN(json#'docs') as features;



-- feature_log = LOAD '$INPUT_FEATURE_LOG' using PigStorage('^') as (raw:chararray);
-- rmf $DEBUG_OUTPUT/feature_log_raw
-- store feature_log into '$DEBUG_OUTPUT/feature_log_raw';
-- feature_log = FOREACH feature_log GENERATE myfunc.parse_json_featurelog(raw) as raw_tuple;

-- feature_log_valid = FILTER feature_log by raw_tuple is not null;

-- feature_log_data = FOREACH feature_log_valid GENERATE FLATTEN(raw_tuple) as (rid:chararray,pid:chararray,predictor_id:chararray,ts:long,doc_size:long,uid:chararray,up:map[],features:map[]);
-- feature_log = FOREACH feature_log_data GENERATE rid,pid,predictor_id,ts,doc_size,uid,up,
    -- FLATTEN(features) as features;

feature_log = FILTER feature_log by pid=='11';
-- feature_log = FOREACH feature_log GENERATE rid,pid,predictor_id,ts,doc_size,uid,up, myfunc.json_to_map(features) as features;

feature_log_flat = FOREACH feature_log GENERATE
    uid, features#'id' as content_id, pid, rid, predictor_id, ts, doc_size, up, features;

feature_log_dist = FOREACH ( GROUP feature_log_flat by (uid,content_id,pid,rid) ) {
    data_sorted = ORDER $1 BY ts;
    data_limit = LIMIT data_sorted 1;
    GENERATE FLATTEN(data_limit) as (uid, content_id, pid, rid, predictor_id, ts, doc_size, up, features);
}

feature_log_format = FOREACH feature_log_dist GENERATE 
    uid, content_id, pid, rid, predictor_id, ts, doc_size, up,
    myfunc.format_feature_log(features) as features;

-- rmf $DEBUG_OUTPUT/feature_log_format
-- store feature_log_format into '$DEBUG_OUTPUT/feature_log_format';
-- join feature log and events

data_jnd = join raw_event_out by (uid, content_id, productid,rid), feature_log_format by (uid, content_id, pid, rid);

events_with_feature = FOREACH data_jnd GENERATE
    raw_event_out::uid as uid,
    raw_event_out::content_id as content_id,
    raw_event_out::productid as productid,
    raw_event_out::rid as rid,
    raw_event_out::label as label,
    feature_log_format::features as features,
    raw_event_out::dwelltime as dwelltime,
    raw_event_out::listpagedwell as listpagedwell,
    feature_log_format::up as up,
    raw_event_out::ts as event_ts,
    feature_log_format::ts as server_ts,
    feature_log_format::predictor_id as predictor_id,
    feature_log_format::doc_size as doc_size;

-- rmf $DEBUG_OUTPUT/events_with_feature
-- store events_with_feature into '$DEBUG_OUTPUT/events_with_feature';

-- declare_interest = load '$INTEREST' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);
-- raw_interest = FOREACH declare_interest GENERATE json#'aid' as aid, json#'interest' as interest, json#'time' as time;

-- uniq_interest = FOREACH ( GROUP raw_interest by aid ) {
--     r = ORDER $1 BY  time;
--     l = LIMIT r 1;
--     GENERATE FLATTEN(l) as (aid, interests, time);
-- }

-- data_with_interest = FOREACH ( JOIN events_with_feature by uid left outer, uniq_interest by aid ) GENERATE
--     $PIG_COL,
--     FLATTEN(interest.get_category_matched(d_raw,interests)) as (declare_match, declare_cnt);

training_data = FOREACH events_with_feature GENERATE 
    uid, content_id, label, dwelltime, 
    myfunc.format_train_data(label,features, null) AS train_data,
    rid, event_ts, server_ts, predictor_id;

rmf $OUTPUT
store training_data into '$OUTPUT';
