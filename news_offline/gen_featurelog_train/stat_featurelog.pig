REGISTER 'lib/*.jar';
REGISTER 'feature_log_util.py' USING jython AS myfunc;

-- set mapred.create.symlink yes;
-- set mapred.cache.files $FEATMAP_FILE#feature.map
set default_parallel 400;
set fs.s3a.connection.maximum 1000;

feature_log = LOAD '$INPUT_FEATURE_LOG' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

feature_log = FOREACH feature_log GENERATE 
    json#'req_id' as rid:chararray,
    json#'prod_id' as pid:chararray,
    json#'predictor_id' as predictor_id:chararray,
    ( json#'predictor_id' matches 'hindi.*' ? 'hi' : 'en' ) as lan:chararray,
    json#'now' as ts:long,
    json#'doc_size' as doc_size:long,
    json#'uid' as uid:chararray,
    json#'up' as up:map[],
    FLATTEN(json#'docs') as features;


feature_log = FILTER feature_log by pid=='11' and lan=='$app_lan';
-- feature_log = FOREACH feature_log GENERATE rid,pid,predictor_id,ts,doc_size,uid,up, myfunc.json_to_map(features) as features;

feature_log_flat = FOREACH feature_log GENERATE
    uid, features#'id' as content_id, pid, rid, predictor_id, ts, doc_size, up, features;

feature_log_dist = FOREACH ( GROUP feature_log_flat by (uid,content_id,pid,rid) ) {
    data_sorted = ORDER $1 BY ts;
    data_limit = LIMIT data_sorted 1;
    GENERATE FLATTEN(data_limit) as (uid, content_id, pid, rid, predictor_id, ts, doc_size, up, features);
}

sample_id = foreach feature_log_dist generate uid, content_id;
sample_count = foreach (group sample_id ALL) generate COUNT(sample_id) as total_cnt;

feature_log_stat = FOREACH feature_log_dist GENERATE 
    FLATTEN(myfunc.stat_feature_log(features)) as (fname:chararray,cnt:int,v:float,v_square:float);

feature_log_grp = GROUP feature_log_stat by fname;

result = FOREACH feature_log_grp GENERATE $0 as fname, SUM($1.cnt) as cnt, SUM($1.v) as v, SUM($1.v_square) as v_square;

result_format = FOREACH result GENERATE fname, cnt, (float)cnt/sample_count.total_cnt as coverage, v/((float)cnt) as mean, SQRT((v_square-v*v/cnt)/cnt) as std;

result_format = ORDER result_format BY fname;
store result_format into '$OUTPUT';

-- feature_log_format = FOREACH feature_log_dist GENERATE 
--     uid, content_id, pid, rid, predictor_id, ts, doc_size, up,
--     myfunc.format_feature_log(features) as features;

-- -- rmf $DEBUG_OUTPUT/feature_log_format
-- -- store feature_log_format into '$DEBUG_OUTPUT/feature_log_format';
-- -- join feature log and events

-- data_jnd = join raw_event_out by (uid, content_id, productid,rid), feature_log_format by (uid, content_id, pid, rid);

-- events_with_feature = FOREACH data_jnd GENERATE
--     raw_event_out::uid as uid,
--     raw_event_out::content_id as content_id,
--     raw_event_out::productid as productid,
--     raw_event_out::rid as rid,
--     raw_event_out::label as label,
--     feature_log_format::features as features,
--     raw_event_out::dwelltime as dwelltime,
--     raw_event_out::listpagedwell as listpagedwell,
--     feature_log_format::up as up,
--     raw_event_out::ts as event_ts,
--     feature_log_format::ts as server_ts,
--     feature_log_format::predictor_id as predictor_id,
--     feature_log_format::doc_size as doc_size;

-- -- rmf $DEBUG_OUTPUT/events_with_feature
-- -- store events_with_feature into '$DEBUG_OUTPUT/events_with_feature';

-- -- declare_interest = load '$INTEREST' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);
-- -- raw_interest = FOREACH declare_interest GENERATE json#'aid' as aid, json#'interest' as interest, json#'time' as time;

-- -- uniq_interest = FOREACH ( GROUP raw_interest by aid ) {
-- --     r = ORDER $1 BY  time;
-- --     l = LIMIT r 1;
-- --     GENERATE FLATTEN(l) as (aid, interests, time);
-- -- }

-- -- data_with_interest = FOREACH ( JOIN events_with_feature by uid left outer, uniq_interest by aid ) GENERATE
-- --     $PIG_COL,
-- --     FLATTEN(interest.get_category_matched(d_raw,interests)) as (declare_match, declare_cnt);

-- training_data = FOREACH events_with_feature GENERATE 
--     uid, content_id, label, dwelltime, 
--     myfunc.format_train_data(label,features, null) AS train_data,
--     rid, event_ts, server_ts, predictor_id;

-- rmf $OUTPUT
-- store training_data into '$OUTPUT';
