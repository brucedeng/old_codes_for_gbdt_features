
queue_name=default

while getopts "s:e:l:q:" arg
do
    case $arg in
        s) #start date of events
        event_start=$OPTARG
        ;;
        e) #end date  of event
        event_end=$OPTARG
        ;;
        l) #run data
        app_lan=$OPTARG
        ;;
        q) # queue name
        queue_name=$OPTARG
        ;;
        ?)  #?
        echo "unkonw argument"
        exit 1
        ;;
    esac
done
                                                                                                                                                                                    
PIG_PARAM="-conf ./default-hadoop.xml -useHCatalog -Dmapred.max.map.failures.percent=10 ";
PIG_PARAM=" -useHCatalog -Dmapred.job.queue.name=$queue_name ";

echo $event_start
echo $event_end
cur_date=$event_start

# rm -f pig*.log

# config_file="hdfs://mycluster/projects/news/model/configs/config.json"
# hadoop fs -copyFromLocal -f config.json $config_file
# category_data="hdfs://mycluster/projects/news/model/configs/cat_tree.txt"
# hadoop fs -copyFromLocal -f lib/cat_tree.txt $category_data
# city_data="hdfs://mycluster/projects/news/model/configs/ip_city.data"
# hadoop fs -copyFromLocal -f lib/ip_city.data $city_data

featmap_file_hdfs="s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/$event_start-$event_end.txt"
hadoop fs -copyFromLocal -f featmap.txt $featmap_file_hdfs

while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
do
    # TOP_PARAM="-p U_NCAT=5 -p U_NKEY=15 -p D_NCAT=10 -p D_NKEY=25 -p WND=600"
    TOP_PARAM="-p U_NCAT=15 -p U_NKEY=20 -p D_NCAT=5 -p D_NKEY=10 -p WND=600"

    # input_event="s3://iwarehouse/cmnow/news_rcv_log/$cur_date/*/*.rcv" 
    input_event="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click,listpagedwelltime,readtime}/$cur_date/*/*"
    prev_date=`date -d "$cur_date -1 days" +%Y%m%d`
    feature_log="s3://com.cmcm.instanews.usw2.prod/model/feature_log/{$cur_date,$prev_date}/*/*"
    feature_log="s3://com.cmcm.instanews.usw2.prod/model/feature_log/$cur_date/*/*"
    # if [[ "$app_lan" == "hi" ]]; then
    #     input_user="/projects/news/model/hindi_up/$prev_date"
    # else
    #     input_user="s3://com.cmcm.instanews.usw2.prod/user_profile/V1/up_json/$prev_date"
    # fi

    # ftr_start=`date -d "+7 day ago $cur_date" +%Y%m%d`;
    # ftr_end=$cur_date;
    # agg_dates=$ftr_start
    # for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done

    # input_content_7d="s3://com.cmcm.instanews.usw2.prod/cpp/feeder/{$agg_dates}/*/*/*.data"
    # input_content_7d="/projects/news/cpp/feeder/in_cp_dump/{$agg_dates}/*/*/*.data"
    input_interest="s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall/$cur_date"

    output="s3://com.cmcm.instanews.usw2.prod/model/experiments/${app_lan}_featurelog_training_data/$cur_date";
    debug_output="s3://com.cmcm.instanews.usw2.prod/model/experiments/${app_lan}_featurelog_debug_data/$cur_date";

    # ftr_start=`date -d "+2 day ago $cur_date" +%Y%m%d`;
    # ftr_end=$cur_date;
    # agg_dates_cfb=$ftr_start
    # for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates_cfb="$agg_dates_cfb,$agg_day"; done
    # input_cfb="hdfs://mycluster/projects/news/model/${app_lan}_offline_cfb_raw/{$agg_dates_cfb}"

    # data_start_time="${cur_date}08"
    data_end_time=`date -d "$cur_date +1 days" +%Y%m%d`
    data_end_time="${data_end_time}00"

    data_start_time=${cur_date}00
    decay_factor=0.99
    n_decaywnd=2d

    cmd="pig $PIG_PARAM $TOP_PARAM -p app_lan=$app_lan -p INPUT_EVENT='$input_event' -p INPUT_FEATURE_LOG='$feature_log' -p DEBUG_OUTPUT='$debug_output' -p FEATMAP_FILE='$featmap_file_hdfs' -p OUTPUT='$output' -p featmap_file=$featmap_file_hdfs -p INTEREST=$input_interest generate_cmnews_featurelog.pig"
    echo $cmd
    eval $cmd;

    if [ $? != 0 ]; then echo "ERROR: Failed to run pipeline!"; exit -1; fi
    cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
done



