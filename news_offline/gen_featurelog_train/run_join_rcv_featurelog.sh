
queue_name=offline
expid='.*'
exp_tag="all"
while getopts "s:e:l:q:x:t:" arg
do
    case $arg in
        s) #start date of events
        event_start=$OPTARG
        ;;
        e) #end date  of event
        event_end=$OPTARG
        ;;
        l) #run data
        app_lan=$OPTARG
        ;;
        x) #exp
        expid=$OPTARG
        exp_tag=$expid
        ;;
        q) # queue name
        queue_name=$OPTARG
        ;;
        t) # queue name
        task=$OPTARG
        ;;
        ?)  #?
        echo "unkonw argument"
        exit 1
        ;;
    esac
done
                                                                                                                                                                                    
PIG_PARAM="-Dmapred.job.queue.name=$queue_name -Dmapred.max.map.failures.percent=5";

echo $event_start
echo $event_end
cur_date=$event_start

rm -f pig*.log

while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
do

    input_event="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click,listpagedwelltime,readtime}/$cur_date/*/*"
    feature_log="s3://com.cmcm.instanews.usw2.prod/model/feature_log/$cur_date/*/*"
    #input_event="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click,listpagedwelltime,readtime}/{$agg_dates}/12/*"
    #feature_log="s3://com.cmcm.instanews.usw2.prod/model/feature_log/{$agg_dates}/{11,12,13}/*"
    prev_date=`date -d "$cur_date -1 days" +%Y%m%d`
    #feature_log="s3://com.cmcm.instanews.usw2.prod/model/feature_log/{$cur_date,$prev_date}/*/*"

    input_interest="s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall/$cur_date"

    output="/projects/news/model/experiments/${app_lan}_${exp_tag}-${cur_date}";

    cmd="pig $PIG_PARAM $TOP_PARAM -p app_lan=$app_lan -p INPUT_EVENT='$input_event' -p INPUT_FEATURE_LOG='$feature_log' -p exp='$expid' -p OUTPUT='$output' join_rcv_featurelog.pig"
    echo $cmd
    eval $cmd;

    if [ $? != 0 ]; then echo "ERROR: Failed to run pipeline!"; exit -1; fi
    cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
done



