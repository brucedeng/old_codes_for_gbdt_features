#!/usr/bin/env python

import sys
import re

threshold = 20000000

if __name__=='__main__':
	file_regex = re.compile('.*/(\\d+/\\d+/)(\\d+\\.\\d+)\\.\\d+\\.\\d+\\.features\\.log\\.(\\d+)\\.gz')
	result = {}
	for line in sys.stdin:
		line = line.strip('\n')
		line = ' '.join(line.split())
		fields = line.split(' ')
		if len(fields) != 3:
			continue
		size=int(fields[0])
		path=fields[2]

		matched = file_regex.match(path)

		if matched is None:
			continue

		labels = matched.groups()
		ip = labels[1]
		ts = labels[0]
		key=ip + '\t' + ts
		if key not in result:
			result[key] = []

		result[key].append((size,path))

	for key,value in result.items():
		sum_size = sum([_k[0] for _k in value])
		cnt = len(value)
		if cnt <= 1:
			continue
		for item in value:
			other_avg = float(sum_size-item[0])/(cnt-1)
			if other_avg - item[0] > threshold:
				print item[1]
				# print value
