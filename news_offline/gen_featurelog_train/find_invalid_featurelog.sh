#!/bin/bash

dt=$1

tmpdir=`mktemp -d -t featurelog_stat.XXXXX`
echo $tmpdir
hadoop fs -du s3://com.cmcm.instanews.usw2.prod/model/feature_log/$dt/*/*/* > $tmpdir/stat.txt

cat $tmpdir/stat.txt | python find_invalid_part.py | tee $tmpdir/invalid.txt

while read path; do
	echo testing $path
	hadoop fs -copyToLocal $path $tmpdir/tmp_data.txt.gz
	gunzip $tmpdir/tmp_data.txt.gz
	if [[ $? -ne 0 ]]; then
		echo "deleteing $path"
		cmd="hadoop fs -rm $path"
		echo "$cmd"
		eval $cmd
	fi
	rm -f $tmpdir/tmp_data.txt*
done < $tmpdir/invalid.txt
