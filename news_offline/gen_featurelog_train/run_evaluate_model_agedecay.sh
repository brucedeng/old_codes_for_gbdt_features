
queue_name=default

while getopts "s:e:l:q:m:" arg
do
    case $arg in
        s) #start date of events
        event_start=$OPTARG
        ;;
        e) #end date  of event
        event_end=$OPTARG
        ;;
        l) #run data
        app_lan=$OPTARG
        ;;
        m) #run data
        model_file=$OPTARG
        ;;
        q) # queue name
        queue_name=$OPTARG
        ;;
        ?)  #?
        echo "unkonw argument"
        exit 1
        ;;
    esac
done
                                                                                                                                                                                    
PIG_PARAM="-conf ./default-hadoop.xml -useHCatalog ";
PIG_PARAM=" -useHCatalog -Dmapred.job.queue.name=$queue_name ";

echo $event_start
echo $event_end
cur_date=$event_start

# rm -f pig*.log

# config_file="hdfs://mycluster/projects/news/model/configs/config.json"
# hadoop fs -copyFromLocal -f config.json $config_file
# category_data="hdfs://mycluster/projects/news/model/configs/cat_tree.txt"
# hadoop fs -copyFromLocal -f lib/cat_tree.txt $category_data
# city_data="hdfs://mycluster/projects/news/model/configs/ip_city.data"
# hadoop fs -copyFromLocal -f lib/ip_city.data $city_data
model_file_name=`basename $model_file`
model_file_hdfs="s3://com.cmcm.instanews.usw2.prod/model/experiments/model_dump/$model_file_name"
hadoop fs -copyFromLocal -f $model_file $model_file_hdfs

if [[ $event_end -lt $event_start ]]; then
    ftr_start=$event_start;
    ftr_end=$event_end;
    agg_dates=$ftr_start
    for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done
else
    agg_dates=$event_end
fi

# input_event="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click,listpagedwelltime,readtime}/$agg_dates/*/*"
#     prev_date=`date -d "$cur_date -1 days" +%Y%m%d`
#     feature_log="s3://com.cmcm.instanews.usw2.prod/model/feature_log/{$cur_date,$prev_date}/*/*"
feature_log="s3://com.cmcm.instanews.usw2.prod/model/feature_log/$agg_dates/*/*"

output="s3://com.cmcm.instanews.usw2.prod/model/experiments/evaluate/${app_lan}_${model_file_name}_agedecay_$event_start-$event_end";

cmd="pig $PIG_PARAM -p app_lan=$app_lan -p INPUT_FEATURE_LOG='$feature_log' -p OUTPUT='$output' -p modelfiles=$model_file_hdfs evaluate_model_agedecay.pig"
echo $cmd
eval $cmd;

if [ $? != 0 ]; then echo "ERROR: Failed to run pipeline!"; exit -1; fi
