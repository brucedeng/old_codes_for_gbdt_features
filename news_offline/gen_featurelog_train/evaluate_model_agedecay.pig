REGISTER 'lib/*.jar';
REGISTER 'feature_log_util.py' USING jython AS myfunc;

set mapred.create.symlink yes;
-- set mapred.cache.files s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/20160321-20160327.txt#feature.map
set mapred.cache.archives $modelfiles#models
define EVAL com.ijinshan.cmcm.gbdt.runtime.udf.GbdtRuntimeMapAllModel('models');

-- set default_parallel 400;
set fs.s3a.connection.maximum 1000;

feature_log = LOAD '$INPUT_FEATURE_LOG' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);
feature_log = sample feature_log 0.01;

feature_log = FOREACH feature_log GENERATE 
    json#'req_id' as rid:chararray,
    json#'prod_id' as pid:chararray,
    json#'predictor_id' as predictor_id:chararray,
    ( json#'predictor_id' matches 'hindi.*' ? 'hi' : 'en' ) as lan:chararray,
    json#'now' as ts:long,
    json#'doc_size' as doc_size:long,
    json#'uid' as uid:chararray,
    json#'up' as up:map[],
    FLATTEN(json#'docs') as features;


feature_log = FILTER feature_log by pid=='11' and lan=='$app_lan';
-- feature_log = FOREACH feature_log GENERATE rid,pid,predictor_id,ts,doc_size,uid,up, myfunc.json_to_map(features) as features;

feature_log_flat = FOREACH feature_log GENERATE
    uid, features#'id' as content_id, pid, rid, predictor_id, ts, doc_size, up, features;

-- feature_log_flat = FOREACH ( GROUP feature_log_flat by (uid,content_id,pid,rid) ) {
--     data_sorted = ORDER $1 BY ts;
--     data_limit = LIMIT data_sorted 1;
--     GENERATE FLATTEN(data_limit) as (uid, content_id, pid, rid, predictor_id, ts, doc_size, up, features);
-- }

feature_log_map = FOREACH feature_log_flat GENERATE uid, content_id, pid, rid, predictor_id, ts, doc_size, up, 
    myfunc.format_feature_log(features) as features_map;

-- rmf $OUTPUT/raw_feature_data
-- store feature_log_map into '$OUTPUT/raw_feature_data';

feature_log_pred = FOREACH feature_log_map GENERATE uid, content_id, pid, rid, predictor_id, ts, doc_size, 
    FLATTEN(EVAL(features_map)) as (model_name:chararray,score:double), features_map#'DOC_AGE' as docage; --,myfunc.format_train_data(0,features_map,null) as train_data;

rmf $OUTPUT/raw_score_data
store feature_log_pred into '$OUTPUT/raw_score_data';

per_user_data = FOREACH ( GROUP feature_log_pred by (uid,rid,model_name) ) {
    raw_score = FOREACH $1 GENERATE score, docage;
    raw_score_ordered = ORDER raw_score BY score DESC;
    raw_score_ordered = LIMIT raw_score_ordered 100;
    GENERATE FLATTEN($0) as (uid,rid,model_name), FLATTEN(myfunc.eval_agedecay(raw_score_ordered,-0.006)) as (cnt,diff), myfunc.eval_age_score_diff(raw_score_ordered) as age_score_diff;
}

rmf $OUTPUT/per_user_data
store per_user_data into '$OUTPUT/per_user_data';

per_user_data_stat = FOREACH per_user_data GENERATE uid,rid,model_name,1.0 as user_cnt, cnt, cnt*cnt as cnt_square, diff, diff*diff as diff_square, age_score_diff;

result = FOREACH ( group per_user_data_stat by model_name ) GENERATE $0 as model_name,
    SUM($1.user_cnt) as user_cnt, SUM($1.age_score_diff) as age_score_diff,
    SUM($1.cnt) as cnt, SUM($1.cnt_square) as cnt_square,
    SUM($1.diff) as diff, SUM($1.diff_square) as diff_square;

result = FOREACH result GENERATE model_name, user_cnt, ((float)cnt)/user_cnt as mean_cnt, ((float)diff)/user_cnt as mean_diff, age_score_diff/user_cnt as age_score_diff, SQRT((cnt_square-cnt*cnt/user_cnt)/user_cnt) as std_cnt, 
    SQRT((diff_square-diff*diff/user_cnt)/user_cnt) as std_diff;

rmf $OUTPUT/total_result
store result into '$OUTPUT/total_result';





-- feature_log_grp = GROUP feature_log_stat by fname;

-- result = FOREACH feature_log_grp GENERATE $0 as fname, SUM($1.cnt) as cnt, SUM($1.v) as v, SUM($1.v_square) as v_square;

-- result_format = FOREACH result GENERATE fname, cnt, v/((float)cnt) as mean, SQRT((v_square-v*v/cnt)/cnt) as std;

-- result_format = ORDER result_format BY fname;
-- store result_format into '$OUTPUT';
