REGISTER '../gen_featurelog_train/lib/*.jar';
REGISTER '../sample_bias/on_grid/piggybank.jar'
REGISTER 'explore_util.py' USING jython AS myfunc;

%declare prev_date 20160416
%declare EX_LIST s3://com.cmcm.instanews.usw2.prod/model/experiments/tmp/explore_data/0417_$lan.txt 
set default_parallel 400;

raw_rcv_log = LOAD '$INPUT' USING org.apache.pig.piggybank.storage.PigStorageSchema(); 

rcv_grp = foreach (group raw_rcv_log by content_id){
  dates = foreach raw_rcv_log generate date_ts;
  valid_dates = filter dates by date_ts == '$target_date';
  prev_dates = filter dates by date_ts == '$prev_date';

  explore_ts = foreach raw_rcv_log generate ts as explore_ts;
  dates_uniq = distinct dates;

  generate flatten(raw_rcv_log), COUNT(prev_dates) as prev_flag, COUNT(valid_dates) as cur_flag, MIN(explore_ts) as min_explore_ts, MAX(explore_ts) as max_explore_ts, COUNT(dates_uniq) as explore_dates;
}

rcv_grp = foreach rcv_grp generate content_id, click_count, view_count, prev_flag, cur_flag, min_explore_ts, max_explore_ts, explore_dates;

explore_raw = LOAD '$EX_LIST' as (ts:chararray, id:chararray);
explore_list = foreach (group explore_raw by id){
    o = order explore_raw by ts;
    l = limit o 1;
    generate flatten(l);
};

rcv_valid = foreach (join rcv_grp by content_id, explore_list by id){
    generate 
        rcv_grp::raw_rcv_log::content_id as content_id,
        rcv_grp::raw_rcv_log::click_count as click_count,
        rcv_grp::raw_rcv_log::view_count as view_count,
        rcv_grp::prev_flag as prev_flag,
        rcv_grp::cur_flag as cur_flag,
        rcv_grp::min_explore_ts as min_explore_ts,
        rcv_grp::max_explore_ts as max_explore_ts,
        rcv_grp::explore_dates,
        explore_list::l::ts;
    };

--rcv_valid = filter rcv_grp by  prev_flag == 0 and cur_flag > 0;
rmf /tmp/chenkehan/explore
--store rcv_valid into '/tmp/chenkehan/explore/rcv_valid';

content_pv  = foreach (group rcv_valid by (content_id, min_explore_ts, max_explore_ts, explore_dates, ts)) generate flatten($0), SUM($1.click_count) as click_cnt, SUM($1.view_count) as view_cnt;

--content_pv = foreach content_pv generate content_id, ts, (max_explore_ts - min_explore_ts)/3600 as explore_interval, explore_dates, click_cnt, view_cnt;
content_pv = foreach content_pv generate content_id as content_id, ts as log_ts, min_explore_ts as min_explore_ts, click_cnt, view_cnt;
store content_pv into '/tmp/chenkehan/explore/content_pv_$lan'; 

--11647164        2435135 4.0     0.0     3.790809772083838       0.0     1461196800000
gmp_dump = load '$gmp_dump' as (content_id:chararray, batch_id:long, pv:float, click:float, decayed_pv:float, decayed_click:float, ts:long);

gmp_valid = filter gmp_dump by pv >= 100;

first_gmp = foreach (group gmp_valid by content_id){
        
        o = order gmp_valid by ts;
        l = limit o 1;
        generate flatten(l) as (content_id:chararray, batch_id:long, pv:float, click:float, decayed_pv:float, decayed_click:float, ts:long);
}

gmp_explore = foreach (join content_pv by content_id, first_gmp by content_id) 
    generate content_pv::content_id as content_id,
            content_pv::log_ts as log_ts,
            content_pv::min_explore_ts as min_explore_ts,
            first_gmp::ts/1000 as first_gmp_ts,
            first_gmp::pv as gmp_pv,
            first_gmp::decayed_click/first_gmp::decayed_pv as gmp;

gmp_explore_res = foreach gmp_explore generate content_id, log_ts, (first_gmp_ts - min_explore_ts) as gmp_delay, gmp_pv, gmp; 

store gmp_explore_res into '/tmp/chenkehan/explore/gmp_delay_$lan';



