# -*- coding:utf-8 -*-
import os
import datetime
import time
import math,sys

if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper


@outputSchema("t:tuple(rid:chararray, src:chararray, explore:int)")
def parse_des(cpack):
    try:
        if cpack is None:
            return ''
        if 'des' not in cpack:
            return ''
        str_data = cpack['des']
        data = str_data.split('|')
        data_map = {}
        for item in data:
            k,v = item.split('=')
            data_map[k] = v
        if 'src' in data_map:
            data_map['explore'] = (int(data_map['src']) / 16) % 2
        else:
            data_map['explore'] = 0
        return (data_map.get('rid',None), data_map['src'], data_map.get('explore'))
    except:
        print >> sys.stderr, cpack
        #raise Exception('Error src')
        return (None, None, None)


@outputSchema("date:chararray")
def to_date(ts):
    import datetime
    return datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d')

