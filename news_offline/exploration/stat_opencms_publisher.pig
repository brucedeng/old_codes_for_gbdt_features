REGISTER '../gen_featurelog_train/lib/*.jar'
REGISTER 'explore_util.py' USING jython AS myfunc;

set default_parallel 400;

raw_content = LOAD '$input' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_content = foreach raw_content generate  
  json#'item_id' as content_id:chararray,
  json#'title_md5' as title_md5:chararray,
  json#'group_id' as groupid:chararray,
  REPLACE(json#'publisher','(\r\n|\n|\t|\r)','') as publisher:chararray,
  json#'source_feed' as source_feed,
  json#'type' as type:chararray,
  (int)(json#'source_type') as source_type:int,
  myfunc.to_date((long)json#'publish_time') as publish_time:chararray,
  (long)json#'update_time' as update_time:long,
  (int)json#'editorial_importance_level' as editorial_importance_level:int,
  (int)json#'opencms_id' as opencms_id:int,
  (chararray)json#'is_servable' as is_servable:chararray,
  (chararray)json#'language' as language:chararray;

--raw_content_filter = FILTER raw_content by (type=='article' or type=='slideshow' or type=='photostory') and opencms_id is not null and publish_time=='$date';
raw_content_filter = FILTER raw_content by (type=='article' or type=='slideshow' or type=='photostory') and editorial_importance_level==50 and publish_time=='$date';

--raw_content_prj = foreach raw_content_filter generate content_id, groupid, publisher, is_servable, (is_servable == 'true'?1:0) as servable, language;

raw_content_prj = foreach raw_content_filter generate content_id, groupid, flatten(source_feed) as publisher, is_servable, (is_servable == 'true'?1:0) as servable, language;

raw_content_prj = distinct raw_content_prj;

res = foreach (group raw_content_prj by (publisher, language)){
        generate flatten($0) as (publisher, language), SUM($1.servable) as valid_count, COUNT($1) as total_cnt, 1 - (float)SUM($1.servable)/(float)COUNT($1) as invalid_rate;
}

rmf /tmp/chenkehan/publisher_res;
store res into '/tmp/chenkehan/publisher_res';
