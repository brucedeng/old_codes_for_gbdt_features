REGISTER '../gen_featurelog_train/lib/*.jar';
REGISTER '../sample_bias/on_grid/piggybank.jar'
REGISTER 'explore_util.py' USING jython AS myfunc;

set default_parallel 400;
set fs.s3a.connection.maximum 1000;

raw_rcv_log = LOAD '$INPUT_EVENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate
    json,
    (json#'app_lan' is null ? 'en' : ( json#'app_lan' MATCHES 'hi.*' ? 'hi' : 'en' )) as app_lan,
    json#'contentid' as content_id:chararray,
    json#'aid' as uid:chararray,
    json#'ip' as ip:chararray,
    json#'servertime_sec' as eventtime,
    json#'pid' as pid:chararray,
    json#'ctype' as ctype,
    (int)json#'ext'#'dwelltime' AS dwelltime:int,
    json#'act' as act,
    json#'upack'#'exp' as exp,
    flatten(myfunc.parse_des(json#'cpack')) as (rid, src, explore);


raw_rcv_log = filter raw_rcv_log by pid == '11' and  ( ctype is not null and (ctype=='1' or ctype=='0x01' ) ) and act is not null and ( act == '1' or act == '2' or act=='3' or act=='4' ) and json#'scenario'#'level1_type' =='1' and json#'scenario'#'level1' == '1' and app_lan=='$app_lan' and rid is not null;


click_view_data = FOREACH raw_rcv_log GENERATE
    content_id, uid,
    rid as rid,
    explore as explore,
    (long)(eventtime) as original_time,
    ((dwelltime is null or dwelltime<0) ? 0 : dwelltime ) as dwelltime,
    (act=='1'? 'view':( act=='2'? 'click':( act=='4'? 'read': (act=='3'? 'listpagedwell':'unknown')))) as event_type;

log_click_view = FOREACH click_view_data GENERATE
      uid AS uid,
      content_id AS content_id,
      rid AS rid,
      original_time as ts,
      explore as explore,
      myfunc.to_date(original_time) as date_ts, 
      event_type AS event_type,
      (event_type=='read' ? dwelltime:0) as dwelltime:int,
      (event_type=='listpagedwell' ? dwelltime:0 ) as listpagedwell:int,
      1.0 AS expression;

log_click_view = FILTER log_click_view BY (content_id matches '^\\d+$') and (ts != -1);

log_click_view = distinct log_click_view;

raw_event_group = GROUP log_click_view BY (uid, content_id, rid);

raw_event_out = FOREACH raw_event_group {
    date_ts = MIN($1.date_ts);
    click_event = FILTER $1 BY (event_type == 'click');
    click_count = COUNT(click_event);
    view_event = FILTER $1 BY (event_type == 'view');
    view_count = COUNT(view_event);
    ts = MIN( view_event.ts );
    
    
    label = (click_count > 0 ? 1: 0);
    dwelltime = SUM($1.dwelltime);
    listpagedwell = SUM($1.listpagedwell);
GENERATE
    $0.uid AS uid,
    $0.content_id AS content_id,
    ts AS ts,
    date_ts as date_ts,
    click_count as click_count,
    view_count as view_count,
    (SUM(view_event.explore) > 0? 'explore_pv':'exploit_pv') as explore_view_count,
    (dwelltime > 600 ? 600:dwelltime) as dwelltime;
}


explore_raw = LOAD '$EX_LIST' as (ts:chararray, id:chararray);
explore_list = foreach (group explore_raw by id){
    o = order explore_raw by ts;
    l = limit o 1;
    generate flatten(l);
};

event_explore = foreach (join raw_event_out by content_id LEFT, explore_list by id){
    generate raw_event_out::content_id as content_id,
            raw_event_out::click_count as click_count,
            raw_event_out::view_count as view_count,
            raw_event_out::explore_view_count as explore_view_count,
            raw_event_out::dwelltime as dwelltime,
            (explore_list::l::id is not null?'explore_item':'normal_item') as explore_item_flag;
}

--Explore candidate of this day or non-explore pv
--event_explore = filter event_explore by (explore_view_count == 'exploit_pv' or explore_item_flag == 'explore_item');

res = foreach (group event_explore by (explore_view_count, explore_item_flag)) {

    generate flatten($0), SUM($1.view_count) as pv, (float)SUM($1.click_count)/SUM($1.view_count) as ctr,  (float)SUM($1.dwelltime)/SUM($1.view_count) as dwell_pv;

} 

rmf $output
store res into '$output';





