
queue_name=offline
run_rcv='false'
run_analysis='false'

while getopts "s:e:l:q:d:t:" arg
do
    case $arg in
        s) #start date of events
        event_start=$OPTARG
        ;;
        e) #end date  of event
        event_end=$OPTARG
        ;;
        l) #run data
        app_lan=$OPTARG
        ;;
        q) # queue name
        queue_name=$OPTARG
        ;;
        d) # target date
        target_date=$OPTARG
        ;;
        t)
        task=$OPTARG
        ;;
        ?)  #?
        echo "unkonw argument"
        exit 1
        ;;
    esac
done
                                                                                                                                                                                    
PIG_PARAM="-Dmapred.job.queue.name=$queue_name";

echo $event_start
echo $event_end
cur_date=$event_start

rm -f pig*.log

if [[ "$task" == "run_rcv" ]]; then
    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do
        input_event="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click,listpagedwelltime,readtime}/$cur_date/*/*"
        #feature_log="s3://com.cmcm.instanews.usw2.prod/model/feature_log/{$cur_date,$prev_date}/*/*"
        output_path='/projects/news/model/tmp'
    
        cmd="pig $PIG_PARAM -p app_lan='$app_lan' -p INPUT_EVENT='$input_event' -p OUTPUT='$output_path/explore_pv_${app_lan}/$cur_date' stat_exploration_rcv.pig"
        echo $cmd
        eval $cmd;
    
        if [ $? != 0 ]; then echo "ERROR: Failed to run pipeline!"; exit -1; fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done
fi

echo $run_analysis
if [[ "$task" == "run_speed" ]]; then
    prev_date=`date -d "$target_date -1 days" +%Y%m%d`
    next_date=`date -d "$target_date +1 days" +%Y%m%d`
    
    if [[ "$app_lan" == "hi" ]]; then
        gmp_path='s3://com.cmcm.instanews.usw2.prod/nrt/gmp_hindi'
    elif [[ "$app_lan" == "en" ]]; then
        gmp_path='s3://com.cmcm.instanews.usw2.prod/nrt/gmp'
    fi

    output_path='/projects/news/model/tmp'
    cmd="pig $PIG_PARAM -p INPUT='$output_path/explore_pv_${app_lan}/{$prev_date,$target_date,$next_date}' -p lan='${app_lan}' -p target_date=$target_date -p gmp_dump='$gmp_path/{$target_date,$next_date}/*/*' analysis_explore_speed.pig"
    #cmd="pig $PIG_PARAM -p INPUT='$output_path/explore_pv_${app_lan}/{$target_date}' -p target_date=$target_date analysis_explore.pig"
    echo $cmd
    eval $cmd

fi

if [[ "$task" == "run_metric" ]]; then
    
    ex_list="s3://com.cmcm.instanews.usw2.prod/model/experiments/tmp/explore_data/0417_${app_lan}.txt"
    input_event="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click,listpagedwelltime,readtime}/$target_date/*/*"
    output_path='/projects/news/model/tmp'
    cmd="pig $PIG_PARAM -p INPUT_EVENT='$input_event' -p app_lan='${app_lan}' -p EX_LIST='$ex_list' -p output='$output_path/explore_metric' analysis_explore_metric.pig"

    echo $cmd
    eval $cmd

fi

