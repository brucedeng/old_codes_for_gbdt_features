REGISTER '../gen_featurelog_train/lib/*.jar';
REGISTER '../sample_bias/on_grid/piggybank.jar'
REGISTER 'explore_util.py' USING jython AS myfunc;

set default_parallel 400;
set fs.s3a.connection.maximum 1000;

raw_rcv_log = LOAD '$INPUT_EVENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate
    json,
    (json#'app_lan' is null ? 'en' : ( json#'app_lan' MATCHES 'hi.*' ? 'hi' : 'en' )) as app_lan,
    json#'contentid' as content_id:chararray,
    json#'aid' as uid:chararray,
    json#'ip' as ip:chararray,
    json#'servertime_sec' as eventtime,
    json#'pid' as pid:chararray,
    json#'ctype' as ctype,
    (int)json#'ext'#'dwelltime' AS dwelltime:int,
    json#'act' as act,
    json#'upack'#'exp' as exp,
    flatten(myfunc.parse_des(json#'cpack')) as (rid, src, explore);


raw_rcv_log = filter raw_rcv_log by pid == '11' and  ( ctype is not null and (ctype=='1' or ctype=='0x01' ) ) and act is not null and ( act == '1' or act == '2' or act=='3' or act=='4' ) and json#'scenario'#'level1_type' =='1' and json#'scenario'#'level1' == '1' and app_lan=='$app_lan' and explore==1;

click_view_data = FOREACH raw_rcv_log GENERATE
    content_id, uid,
    rid as rid,
    src as src,
    (long)(eventtime) as original_time,
    ((dwelltime is null or dwelltime<0) ? 0 : dwelltime ) as dwelltime,
    (act=='1'? 'view':( act=='2'? 'click':( act=='4'? 'read': (act=='3'? 'listpagedwell':'unknown')))) as event_type;

rmf /tmp/chenkehan/test;
store click_view_data into '/tmp/chenkehan/test'; 
quit;

log_click_view = FOREACH click_view_data GENERATE
      uid AS uid,
      content_id AS content_id,
      rid AS rid,
      original_time as ts,
      myfunc.to_date(original_time) as date_ts, 
      event_type AS event_type,
      (event_type=='read' ? dwelltime:0) as dwelltime:int,
      (event_type=='listpagedwell' ? dwelltime:0 ) as listpagedwell:int,
      1.0 AS expression;

log_click_view = FILTER log_click_view BY (content_id matches '^\\d+$') and (ts != -1);

log_click_view = distinct log_click_view;

raw_event_group = GROUP log_click_view BY (uid, content_id, rid);

raw_event_out = FOREACH raw_event_group {
    date_ts = MIN($1.date_ts);
    click_event = FILTER $1 BY (event_type == 'click');
    click_count = COUNT(click_event);
    view_event = FILTER $1 BY (event_type == 'view');
    view_count = COUNT(view_event);
    ts = MIN( view_event.ts );

    label = (click_count > 0 ? 1: 0);
    dwelltime = SUM($1.dwelltime);
    listpagedwell = SUM($1.listpagedwell);
GENERATE
    $0.uid AS uid,
    $0.content_id AS content_id,
    --$0.rid as rid,
    ts AS ts,
    date_ts as date_ts,
    click_count as click_count,
    view_count as view_count,
    (dwelltime > 600 ? 600:dwelltime) as dwelltime;
    --(listpagedwell > 600 ? 600:listpagedwell) as listpagedwell,
}

/*
raw_event_out_grp = foreach (group raw_event_out by content_id){
    dates = foreach raw_event_out generate date_ts;
    valid_dates = filter dates by date_ts == '$target_date';
    generate flatten(raw_event_out), COUNT(valid_dates) as isvalid;
}
raw_event_out_valid = filter raw_event_out_grp by isvalid > 0;
*/

rmf $OUTPUT;
store raw_event_out into '$OUTPUT' USING org.apache.pig.piggybank.storage.PigStorageSchema(); 

--ts_pv = foreach (group raw_event_out by ts) generate $0, SUM($1.click_count) + SUM($1.view_count) as cnt;
--store ts_pv into '$OUTPUT'; 

