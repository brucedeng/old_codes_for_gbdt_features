
import matplotlib.pyplot as plt
import numpy as np
import argparse
import config_util

hist_settings = {'bins': 30, 'normed': True, 'alpha': 0.3}
def draw_distributions(src_array, des_array, weights, fnames):
    plt.figure(figsize=(16, 12))
    columns = range(len(fnames))
    for id, column in enumerate(columns, 1):
        xlim = np.percentile(np.hstack(des_array[:,column]), [0.01, 99.99])
        #xlim = np.percentile(np.hstack(src_array[:,column]), [0.01, 99.99])
        plt.subplot(3,4, id)
        plt.hist(src_array[:,column], weights=weights, range=xlim, color='b',**hist_settings)
        plt.hist(des_array[:,column], range=xlim, color='orange',**hist_settings)
        plt.xlabel('%s' %(fnames[id-1]))

        plt.savefig('dist.png')

def load_from_file(filename):
    src_data = []
    lineno = 0
    with open(filename) as data_file:
        for line in data_file:
            skip_flag = False
            fmap = {}
            data_vec = []
            svm_data = line.split('\t')[-1]
            for t in svm_data.split(' ')[1:]:
                fid, fvalue = t.split(':')
                fmap[fid]=float(fvalue)
                if fid == '65' and float(fvalue) > 10000:
                    skip_flag = True

            if skip_flag:
                continue
            data_vec = [fmap.get(fid,0.0) for fid in reserved_fields]
            if lineno % 50000 == 0:
                print lineno
            src_data += data_vec
            lineno += 1

    src_array = np.array(src_data).reshape(lineno,len(reserved_fields))
    return src_array


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    #arg_parser.add_argument('-f', '--featmap', action='store', dest='featmap', default='featmap.txt', help='featmap path')
    #arg_parser.add_argument('-m', '--model-dump', action='store', dest='model', default='reweighter.model', help='model dump filename')
    arg_parser.add_argument('-w', '--weight-file', action='store', dest='weight_file', default=None, help='weight file for calibration')
    #arg_parser.add_argument('src', help='src data file')
    #arg_parser.add_argument('des', help='des data file')
    arg_parser.add_argument('cfg', help='config file')

    args = arg_parser.parse_args()
    config = config_util.parse_config(args.cfg);
    print config

    reserved_fields = sorted(config.get('calibrate_feature_id').split(','), key=lambda x:int(x))

    fid2name = {}
    with open(config.get('featmap','featmap.txt')) as featmap:
        for line in featmap:
            fid, fname, q = line.split('\t')
            fid2name[fid] = fname[0:15]


    src_data_file = config.get('src_data.test')
    des_data_file = config.get('target_data.test')

    src_array = load_from_file(src_data_file)
    des_array = load_from_file(des_data_file)
    fnames = [fid2name[fid] for fid in reserved_fields]


    draw_distributions(src_array, des_array, weights=np.ones(src_array[:,0].size), fnames=fnames)

    import pickle
    with open(config.get('model_dump','reweighter.model'),'r') as f:
        reweighter = pickle.load(f)

    if args.weight_file:
        print 'load weight file: %s' % args.weight_file
        with open(args.weight_file,'r') as w:
            gb_weights = [float(line) for line in w]
    else:
        print 'predict weight'
        gb_weights = reweighter.predict_weights(src_array)

    src_mean = np.mean(src_array, axis=0)
    print "original mean", src_mean
    weights = np.array(gb_weights).reshape(len(gb_weights),1)
    src_mean = np.dot(src_array.T, weights) / sum(gb_weights)
    print "weighted mean", src_mean
    des_mean = np.mean(des_array, axis=0)
    print "target mean", des_mean

    draw_distributions(src_array, des_array, weights=gb_weights, fnames=fnames)

    print "Done"
    plt.show()


