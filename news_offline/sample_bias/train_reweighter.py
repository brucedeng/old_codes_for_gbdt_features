
import matplotlib.pyplot as plt
import numpy as np
import argparse
import config_util


hist_settings = {'bins': 30, 'normed': True, 'alpha': 0.3}
def draw_distributions(src_array, des_array, weights, fnames):
    plt.figure(figsize=(16, 20))
    columns = range(len(fnames))
    for id, column in enumerate(columns, 1):
        xlim = np.percentile(np.hstack(des_array[:,column]), [0.01, 99.99])
        #xlim = np.percentile(np.hstack(src_array[:,column]), [0.01, 99.99])
        plt.subplot(5,4, id)
        plt.hist(src_array[:,column], weights=weights, range=xlim, color='b',**hist_settings)
        plt.hist(des_array[:,column], range=xlim, color='orange',**hist_settings)
        plt.xlabel('%s' %(fnames[id-1]))

        plt.savefig('dist.png')




def load_from_file(filename):
    src_data = []
    lineno = 0
    with open(filename) as data_file:
        for line in data_file:
            skip_flag = False
            fmap = {}
            data_vec = []
            svm_data = line.split('\t')[-1]
            for t in svm_data.split(' ')[1:]:
                fid, fvalue = t.split(':')
                fmap[fid]=float(fvalue)
                if fid == '65' and float(fvalue) > 10000:
                    skip_flag = True

            if skip_flag:
                continue
            data_vec = [fmap.get(fid,0.0) for fid in reserved_fields]
            if lineno % 50000 == 0:
                print lineno
            src_data += data_vec
            lineno += 1

    src_array = np.array(src_data).reshape(lineno,len(reserved_fields))
    return src_array


#reserved_fields = ['184','172','1','51','178','164','100','65','173','167']
#reserved_fields = ['1','51','100','65']
if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-t', '--train', action='store_true', dest='train', default=False, help='do training')
    #arg_parser.add_argument('-f', '--featmap', action='store', dest='featmap', default='featmap.txt', help='featmap path')
    #arg_parser.add_argument('-m', '--model-dump', action='store', dest='model', default='reweighter.model', help='model dump filename')
    #arg_parser.add_argument('src', help='src data file')
    #arg_parser.add_argument('src', help='src data file')
    arg_parser.add_argument('cfg', help='config file')

    args = arg_parser.parse_args()

    config = config_util.parse_config(args.cfg);
    print config
    #reserved_fields = sorted(reserved_fields, key=lambda x:int(x))
    fields = config.get('calibrate_feature_id').split(',')
    reserved_fields = sorted(fields, key=lambda x:int(x))

    fid2name = {}
    with open(config.get('featmap','featmap.txt')) as featmap:
        for line in featmap:
            fid, fname, q = line.split('\t')
            fname = '_'.join(filter(lambda x: len(x) >1 , fname.split('_')))
            fid2name[fid] = fname

    fnames = [fid2name[fid] for fid in reserved_fields]
    print fnames


    src_data_file = config.get('src_data.train')
    des_data_file = config.get('target_data.train')

    src_array = load_from_file(src_data_file)
    des_array = load_from_file(des_data_file)

    print "Draw distribution"
    draw_distributions(src_array, des_array, weights=np.ones(src_array[:,0].size), fnames=fnames)

    from hep_ml import reweight
    if args.train:
        reweighter = reweight.GBReweighter(n_estimators=150, learning_rate=0.1, max_depth=5, min_samples_leaf=100,                                  gb_args={'subsample': 0.5})

        print "Fit reweight model..."
        reweighter.fit(src_array, des_array)
        import pickle
        with open(config.get('model_dump','reweighter.model'),'w') as f:
            pickle.dump(reweighter,f)

        print "Predict reweight..."
        gb_weights = reweighter.predict_weights(src_array)

        print "Draw reweighted distribution"
        draw_distributions(src_array, des_array, weights=gb_weights, fnames=fnames)

    print "Done"
    plt.show()





