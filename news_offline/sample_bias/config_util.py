import ConfigParser
def parse_config(config_file):

    config_json = {}

    config = ConfigParser.ConfigParser()
    config.read(config_file)

    for k,v in config.items('Data'):
        if v:
            config_json[k] = v
        else:
            config_json[k] = None

    for k,v in config.items('Model'):
        if v:
            config_json[k] = v
        else:
            config_json[k] = None

    return config_json



