import matplotlib.pyplot as plt
import numpy as np
import corner
import argparse



def load_from_file(filename):
    src_data = []
    lineno = 0
    with open(filename) as data_file:
        for line in data_file:
            skip_flag = False
            fmap = {}
            data_vec = []
            svm_data = line.split('\t')[-1]
            for t in svm_data.split(' ')[1:]:
                fid, fvalue = t.split(':')
                fmap[fid]=float(fvalue)
                if fid == '65' and float(fvalue) > 10000:
                    skip_flag = True

            if skip_flag:
                continue
            data_vec = [fmap.get(fid,0.0) for fid in reserved_fields]
            if lineno % 50000 == 0:
                print lineno
            src_data += data_vec
            lineno += 1

    src_array = np.array(src_data).reshape(lineno,len(reserved_fields))
    return src_array


reserved_fields = ['184','172','1','51','178','164','100','65','173','167']
#reserved_fields = ['1','51','100','65']
if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-f', '--featmap', action='store', dest='featmap', default='featmap.txt', help='featmap path')
    arg_parser.add_argument('src', help='src data file')

    args = arg_parser.parse_args()

    fid2name = {}
    with open(args.featmap) as featmap:
        for line in featmap:
            fid, fname, q = line.split('\t')
            fname = '_'.join(map(lambda x: x[0:4] if len(x) > 4 else x, filter(lambda x: len(x) >1 , fname.split('_'))))
            fid2name[fid]=fname


    src_data_file = args.src

    src_array = load_from_file(src_data_file)
    fnames = [fid2name[fid] for fid in reserved_fields]

    #draw feature correlation
    fig = corner.corner(src_array, bins=20, smooth=0.85, labels=fnames)
    fig.savefig("corner.png")

    plt.show()








