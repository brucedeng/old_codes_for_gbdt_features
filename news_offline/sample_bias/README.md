# sample bias re-weight 

### 基于featurelog生成event和full query list的特征集合
```
news_offline/gen_featurelog_train/run_join_rcv_featurelog.sh 
```

### 训练reweight模型
```
train_reweighter.py -t config_file
-t 表示针对原始分布和目标分布训练拟合模型，不带-t则仅会绘制原始分布和目标分布的差异
```

### 评测reweight模型
```
eval_reweighter.py -w weight_file config_file 
调用训练好的模型预测原始分布的weight，验证对目标分布的校准。-w 可以指定目标分布对应的weight文件。
```

### 离线预测
```
predict_weight.py -i input_file -o outputfile config_file
不指定input_file则从stdin读取样本
```

### on-grid预测
```
run_predict_weight.sh 线上预测
```

### 分析
```
draw_dist.py 特征分布分析
```

### 模型评测带weight的AUC/PR
```
news_offline/gen_train_data/run_eval_model_weighted.sh
```