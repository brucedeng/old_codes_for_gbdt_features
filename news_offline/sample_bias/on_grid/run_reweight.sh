

python_hdfs_path=$1
feature_param=$2
data_index=$3

file_name=`basename $python_hdfs_path`

echo "copyting python runtime to local" 1>&2
hadoop fs -copyToLocal $python_hdfs_path 1>&2 

echo "extract file" 1>&2
tar -zxf $file_name 1>&2

echo "do predict" 1>&2
anaconda/bin/python predict_weight_grid.py -f $feature_param -i $data_index 

echo "done" 1>&2

