
queue_name=offline
while getopts "s:e:q:d:" arg
do
    case $arg in
        s) #start date of events
        event_start=$OPTARG
        ;;
        e) #end date  of event
        event_end=$OPTARG
        ;;
        q) # queue name
        queue_name=$OPTARG
        ;;
        d) # data path
        data=$OPTARG
        ;;
        ?)  #?
        echo "unkonw argument"
        exit 1
        ;;
    esac
done
                                                                                                                                                                                    
PIG_PARAM="-Dmapred.job.queue.name=$queue_name";

echo $event_start
echo $event_end
cur_date=$event_start

rm -f pig*.log

python_runtime="/tmp/chenkehan/anaconda.tar.gz"
data="/projects/news/model/training/run_training_data/hi_cfb_offline_formatted_data/split_events_20160321-20160327"
model="/tmp/chenkehan/bias_reweighter/hi_cfb_0407_reweight_noage.model"
reweight_feature="173,167,184,64,1,56,178,100,65,62,106,99,105,51,172"

model_name=`basename $model`;

input="$data"
output="$data-$model_name";

cmd="pig $PIG_PARAM -p INPUT='$input' -p PYTHON_RUNTIME='$python_runtime' -p REWEIGHTER_MODEL='$model' -p REWEIGHT_FEATURES="$reweight_feature" -p OUTPUT='$output' sample_reweight.pig"
echo $cmd
#eval $cmd;

output='/projects/news/model/training/run_training_data/hi_cfb_offline_formatted_data/split_events_20160321-20160327-0406_reweight'
train_out="$output/{0,1}_{0,1,2,3}"
test_out="$output/{0,1}_4"
format_output="$output-formatted"

cmd2="pig $PIG_PARAM -p train='$train_out' -p test='$test_out' -p OUTPUT='$format_output' format_reweight_xgboost_train_data.pig"
echo $cmd2
eval $cmd2;




