register *.jar

raw_training_data = LOAD '/projects/news/model/training/run_training_data/hi_cfb_offline_formatted_data/split_events_20160321-20160327' as (uid:chararray, content_id:chararray, label:int, dwelltime:float, train_data:chararray, ts:long, bin:chararray, weight:float);

raw_training_data = sample raw_training_data 0.1;

rmf /tmp/chenkehan/split_events_20160321-20160327_sampled
store raw_training_data into '/tmp/chenkehan/split_events_20160321-20160327_sampled';


