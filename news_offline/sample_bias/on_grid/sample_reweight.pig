register *.jar

set mapred.create.symlink yes;
set mapred.cache.files $REWEIGHTER_MODEL#reweighter.model

--DEFINE CMD `python predict_weight_grid.py -f $REWEIGHT_FEATURES` ship('predict_weight_grid.py');
DEFINE CMD `bash run_reweight.sh $PYTHON_RUNTIME $REWEIGHT_FEATURES 4` ship('predict_weight_grid.py','run_reweight.sh');

raw_training_data = LOAD '$INPUT' as (uid:chararray, content_id:chararray, label:int, dwelltime:float,
    train_data:chararray, ts:long, bin:chararray);

result = STREAM raw_training_data THROUGH CMD as (uid:chararray, content_id:chararray, label:int, dwelltime:float, train_data:chararray, ts:long, bin:chararray, weight:float);

rmf $OUTPUT
store result into '$OUTPUT' using org.apache.pig.piggybank.storage.MultiStorage('$OUTPUT','6','none','\t');


