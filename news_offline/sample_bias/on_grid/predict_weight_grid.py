
import sys
import argparse


def load_from_str(str_data,index):
    src_data = []
    lineno = 0
    for line in str_data:
        fmap = {}
        data_vec = []
        svm_data = line.split('\t')[index]
        for t in svm_data.split(' ')[1:]:
            fid, fvalue = t.split(':')
            fmap[fid]=float(fvalue)

        data_vec = [fmap.get(fid,0.0) for fid in reserved_fields]
        src_data += data_vec
        lineno += 1

    import numpy as np
    src_array = np.array(src_data).reshape(lineno,len(reserved_fields))
    return src_array



#reserved_fields = ['184','172','1','51','178','164','100','65','173','167']
#reserved_fields = ['1','51','100','65']
if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-f', '--reserved_fields', action='store', dest='fields', help='feature id for reweight')
    arg_parser.add_argument('-i', '--data_index', action='store', dest='index', help='data index')

    args = arg_parser.parse_args()

    reserved_fields = sorted(args.fields.split(','), key=lambda x:int(x))

    import pickle
    with open('reweighter.model','r') as f:
        reweighter = pickle.load(f)

    batch_size = 1000
    str_data = []
    for line in sys.stdin:
        str_data.append(line)
        if len(str_data) < 1000:
            pass
        else:
            data_array = load_from_str(str_data,int(args.index))
            gb_weights = reweighter.predict_weights(data_array)
            for line, weight in zip(str_data, gb_weights):
                print '\t'.join(line.rstrip().split('\t') + ["%f" % weight])
            str_data = []


    if len(str_data) > 0:
        data_array = load_from_str(str_data,int(args.index))
        gb_weights = reweighter.predict_weights(data_array)

        for line, weight in zip(str_data, gb_weights):
            print '\t'.join(line.rstrip().split('\t') + ["%f" % weight])





