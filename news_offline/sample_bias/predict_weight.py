
import numpy as np
import sys
import argparse
import config_util


def load_from_str(str_data):
    src_data = []
    lineno = 0
    for line in str_data:
        fmap = {}
        data_vec = []
        svm_data = line.split('\t')[-1]
        for t in svm_data.split(' ')[1:]:
            fid, fvalue = t.split(':')
            fmap[fid]=float(fvalue)
            # fid == '65' and float(fvalue) > 10000:
                #skip_flag = True

        data_vec = [fmap.get(fid,0.0) for fid in reserved_fields]
        src_data += data_vec
        lineno += 1

    src_array = np.array(src_data).reshape(lineno,len(reserved_fields))
    return src_array


def load_from_file(filename):
    src_data = []
    lineno = 0
    with open(filename) as data_file:
        for line in data_file:
            fmap = {}
            data_vec = []
            svm_data = line.split('\t')[-1]
            for t in svm_data.split(' ')[1:]:
                fid, fvalue = t.split(':')
                fmap[fid]=float(fvalue)
                #if fid == '65' and float(fvalue) > 10000:
                    #skip_flag = True

            data_vec = [fmap.get(fid,0.0) for fid in reserved_fields]

            print svm_data, data_vec
            if lineno % 50000 == 0:
                print lineno
            src_data += data_vec
            lineno += 1

    src_array = np.array(src_data).reshape(lineno,len(reserved_fields))
    return src_array


#reserved_fields = ['184','172','1','51','178','164','100','65','173','167']
#reserved_fields = ['1','51','100','65']
if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    #arg_parser.add_argument('-m', '--model-dump', action='store', dest='model', default='reweighter.model', help='model dump filename')
    arg_parser.add_argument('-i', '--input-file', action='store', dest='data_file', help='input data file')
    arg_parser.add_argument('-o', '--output-file', action='store', dest='output_file', default='weight.txt', help='weight file')
    arg_parser.add_argument('cfg', help='config file')

    args = arg_parser.parse_args()
    config = config_util.parse_config(args.cfg)
    print config

    reserved_fields = sorted(config.get('calibrate_feature_id').split(','), key=lambda x:int(x))


    import pickle
    with open(config.get('model_dump','reweighter.model'),'r') as f:
        reweighter = pickle.load(f)

    if args.data_file:
        src_data_file = args.data_file
        src_array = load_from_file(src_data_file)


        gb_weights = reweighter.predict_weights(src_array)

        with open(args.output_file,'w') as output_file:
            for weight in gb_weights:
                output_file.write('%f\n' % weight)

    else:
        batch_size = 1000
        str_data = []
        for line in sys.stdin:
            if len(str_data) < 1000:
                str_data.append(line)
            else:
                data_array = load_from_str(str_data)
                gb_weights = reweighter.predict_weights(data_array)
                for weight in gb_weights:
                    print '%f' % weight
                str_data = []

        if len(str_data) > 0:
            data_array = load_from_str(str_data)
            gb_weights = reweighter.predict_weights(data_array)
            for weight in gb_weights:
                print '%f' % weight





