REGISTER 'lib/*.jar';
REGISTER 'format_up.py' USING jython AS myfun_up;

set mapred.max.map.failures.percent 1;
set mapred.max.map.failures.percent 1;
-- set default_parallel 400;
set fs.s3a.connection.maximum 1000;
set mapred.create.symlink yes;

raw_up_data = LOAD '$UP_DATA' as (up_str:chararray);
up_data = FOREACH raw_up_data GENERATE FLATTEN(myfun_up.gen_up_id_age_gender(up_str)) as (uid,u_age,u_gender);


raw_featurelog_data =  LOAD '$INPUT_FEATURE_LOG' as (featurelog_str:chararray);
featurelog_data = FOREACH raw_featurelog_data GENERATE FLATTEN(myfun_up.gen_featurelog_age_gender_city(featurelog_str)) as (uid,f_age,f_gender,f_city);


featurelog_distinct = FILTER featurelog_data by uid != '';
-- featurelog_distinct = DISTINCT featurelog_filtered;
-- len_featurelog = FOREACH (GROUP featurelog_data ALL) GENERATE COUNT($1) as len_of_featurelog;
-- len_featurelog_filtered = FOREACH (GROUP featurelog_data ALL) GENERATE COUNT($1) as len_of_featurelog_filtered;

up_distinct = FILTER up_data by uid != '';
-- up_distinct = DISTINCT up_filtered;
-- len_up = FOREACH (GROUP up_data ALL) GENERATE COUNT($1) as len_of_up;
-- len_up_filtered = FOREACH (GROUP up_data ALL) GENERATE COUNT($1) as len_of_up_filtered;

data_jnd = join up_distinct by uid, featurelog_distinct by uid;

data_res = FOREACH data_jnd GENERATE (up_distinct::u_age == featurelog_distinct::f_age ? 0 :1 ) as miss_age, (up_distinct::u_gender == featurelog_distinct::f_gender ? 0 : 1) as miss_gender;

data_result = FOREACH (GROUP data_res ALL) GENERATE COUNT($1.miss_age), SUM($1.miss_age),SUM($1.miss_gender);

sample_case = limit data_jnd 1000;
store sample_case into '$OUTPUT_BAD_CASE';
store data_result into '$OUTPUT_UP';
