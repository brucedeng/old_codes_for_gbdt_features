# -*- coding:utf-8 -*-
import os
import datetime
import time
import math,sys
import hashlib

#import json
m = hashlib.md5()
global_channel_dict = {
        1:'B0',
        2:'B1',
        3:'B2',
        4:'B3',
        5:'A',
        9:'B9',
        100:'B100',
        101:'B101',
        102:'B102',
        10:'B10',
        50:'B50',
        20:'PR'
        }

if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper
    import json
else:
    import com.xhaus.jyson.JysonCodec as json

feature_seq = chr(2)

def ip2int(ip):
    if ip is None:
        return -1
    fields = ip.split('.')
    if len(fields) != 4:
        return -1
    result = 0
    for idx in fields:
        result = result * 256
        result = result + int(idx)
    return result

def get_ip_city_dict():
    if get_ip_city_dict.ip_city_dict is None:
        get_ip_city_dict.ip_city_dict = {}
        f = open('ip_city.data')
        for line in f:
            fields = line.strip().split('\t')
            if len(fields) < 6:
                continue
            ip_int_addr = ip2int(fields[0])
            end_ip_int_addr = ip2int(fields[1])
            ip_addr_key = ip_int_addr/65536*65536
            if ip_addr_key not in get_ip_city_dict.ip_city_dict:
                get_ip_city_dict.ip_city_dict[ip_addr_key] = []
            city_name = fields[4]
            get_ip_city_dict.ip_city_dict[ip_addr_key].append((ip_int_addr,end_ip_int_addr,city_name))
            # ip_address = '.'.join(fields[0].split('.')[0:3]) + '.0'
            # city_name = fields[4]
            # get_ip_city_dict.ip_city_dict[ip_addr_key] = city_name
        f.close()
    return get_ip_city_dict.ip_city_dict
get_ip_city_dict.ip_city_dict=None

def get_config_json_object():
    if get_config_json_object.json_obj is None:
        config_file = 'config.json'
        config_json_object = {}
        if not os.path.exists(config_file):
            print "plz provide valid configure file"

        f = open(config_file,'r')
        lines = f.readlines()
        str_out = ''
        for line in lines:
            str_out += line.strip()
        config_json_object = json.loads(str_out)
        get_config_json_object.json_obj = config_json_object
        #print >> sys.stderr, str(config_json_object)
        f.close()

    return get_config_json_object.json_obj
get_config_json_object.json_obj = None

@outputSchema("rid:chararray")
def parse_rid(cpack):
    if cpack is None:
        return ''
    if 'des' not in cpack:
        return ''
    str_data = cpack['des']
    data = str_data.split('|')
    data_map = {}
    for item in data:
        kv_list = item.split('=',1)
        if len(kv_list) == 2:
            data_map[kv_list[0]] = kv_list[1]
    if 'rid' in data_map:
        return data_map['rid']
    return ''

@outputSchema('md5str:chararray')
def get_md5_id(input):
    if input is None or len(input) <=0:
        return None
    md5_str = u'_'.join([unicode(x) for x in input])
    md5_str = md5_str.encode('utf-8')
    m.update(md5_str)
    return m.hexdigest()

@outputSchema('bin:chararray')
def get_random_bin(data,bin_size,seed):
    if data is None:
        return 'invalid'
    seed = str(seed)
    data = data + seed
    m2 = hashlib.md5()
    m2.update(data)
    res_md5 = m2.hexdigest()
    first_md5 = int(res_md5[:8],16)

    return str(first_md5 % bin_size)

@outputSchema("city_name:chararray")
def get_city_name(ip_address):
    if ip_address is None or ip_address == '':
        return 'unknown'

    ip_city_dict = get_ip_city_dict()
    ip_address_list = ip_address.strip().split(',')

    for ip_address in ip_address_list:
        # ip_address = '.'.join(ip_address.strip().split('.')[0:3]) + '.0'
        ip_addr_int = ip2int(ip_address)
        ip_addr_key = ip_addr_int/65536*65536

        if ip_addr_key in ip_city_dict:
            for ranges in ip_city_dict[ip_addr_key]:
                if ip_addr_int >= ranges[0] and ip_addr_int <= ranges[1]:
                    print >> sys.stderr, ip_address, ranges[2]
                    return ranges[2]
            # return ip_city_dict[ip_address]
    return 'unknown'

@outputSchema("flag:chararray")
def filter_expired(current_ts, prev_ts, expired_ts):
    if prev_ts is None or current_ts is None or expired_ts is None:
        return '0'
    if long(current_ts) - long(prev_ts) <= long(expired_ts):
        return '1'
    else:
        return '0'

@outputSchema("flag:chararray")
def filter_date(current_ts, prev_ts):
    if prev_ts is None or current_ts is None :
        return '0'
    if long(current_ts) - long(prev_ts) >= 0:
        return '1'
    else:
        return '0'

@outputSchema("flag:chararray")
def filter_flag(filter_field, flag):
    if filter_field is None:
        return '0'
    try:
        filter_field = str(filter_field)
        if filter_field == flag:
            return '1'
        else:
            return '0'
    except:
        return '0'

# preprocess log udf
@outputSchema("interval:int")
def get_interval(current_ts, prev_ts, wnd):
    if prev_ts is None or current_ts is None:
        return 0
    return (current_ts - prev_ts)/(wnd)

@outputSchema("power:double")
def power(base, n):
    if base is None or n is None:
        return None
    return float(base) ** float(n)

@outputSchema("date:chararray")
def substring(date_string, start, end):
    if date_string is None or date_string == '':
        return None
    if start >= end:
        return None
    return date_string[start:end]

@outputSchema("timestamp:long")
def date2timestamp(date_str, wnd, format_str='%Y%m%d%H%M'):
    if date_str is None  or format_str is None:
        return None
    try:
        date_str = str(date_str)
        timestamp = int(time.mktime(datetime.datetime.strptime(date_str, format_str).timetuple()))
        timestamp = timestamp/long(wnd) * long(wnd)
        return timestamp
    except:
        return None

@outputSchema("interval:int")
def diff_date(current_date, prev_date):
    if current_date is None or len(current_date) < 10 or prev_date is None or len(prev_date) < 10:
        return 0
    try:
        current_timestamp = int(time.mktime(datetime.datetime.strptime(current_date, '%Y%m%d%H%M').timetuple()))
        prev_timestamp = int(time.mktime(datetime.datetime.strptime(prev_date, '%Y%m%d%H%M').timetuple()))
        diff_timestamp = current_timestamp - prev_timestamp

        return diff_timestamp/600
    except:
        return 0


@outputSchema("date:chararray")
def date2date10min(date_string):
    if date_string is None or len(date_string) < 10:
        return None
    try:
        timestamp = int(time.mktime(datetime.datetime.strptime(date_string, '%Y-%m-%d %H:%M:%S').timetuple()))
        timestamp = (timestamp/600) * 600
        return datetime.datetime.fromtimestamp(timestamp).strftime('%Y%m%d%H%M')
    except:
        return None

@outputSchema("date:chararray")
def timestamp2date(timestamp):
    if timestamp is None or len(timestamp) < 10:
        return None
    try:
        timestamp = long(timestamp)
        return datetime.datetime.fromtimestamp(timestamp).strftime('%Y%m%d%H%M')
    except:
        return None

@outputSchema("date:chararray")
def timestamp2dateformat(timestamp,format_str='%Y%m%d%H'):
    try:
        timestamp = long(timestamp)
        return datetime.datetime.fromtimestamp(timestamp).strftime(format_str)
    except:
        return None

@outputSchema("hot:int")
def get_hot(rank_type):
    if rank_type is None or rank_type == '':
        return None
    try:
        rank_type_int = int(rank_type)
        hot_flag = rank_type_int &0x1
        return hot_flag
    except:
        return None

@outputSchema("channel:chararray")
def get_channel(rank_type):
    if rank_type is None or rank_type == '':
        return -1
    try:
        rank_type_int = int(rank_type)
        channel_flag = (rank_type_int >>1) & 0xff
        return global_channel_dict[channel_flag]
    except:
        return 'B102'

@outputSchema("b:{t:(content_id:chararray,cid:chararray,eventtime:chararray,requesttime:chararray,ranktypes:chararray)}")

def extract_view_fields(input_list):
    if input_list is None or len(input_list) <= 0 :
        return None
    item_length = len(input_list[0])

    out_bag = []
    try:
        for index in xrange(0,item_length):
            inner_list = []
            for item in input_list:
                if len(item) <= index:
                    inner_list.append(item[len(item) - 1][0].strip())
                else:
                    inner_list.append(item[index][0].strip())
            out_bag.append(tuple(inner_list))
    except:
        pass
    return out_bag

#static feature udf
@outputSchema("time_of_day:int")
def time_of_day(event_time):
    if event_time is None:
        return -1

    time_of_day  = time.localtime(event_time).tm_hour
    return time_of_day

@outputSchema("time_of_week:int")
def day_of_week(event_time):
    if event_time is None:
        return -1

    day_of_week = time.strftime("%w", time.localtime(event_time));
    return day_of_week
@outputSchema("rel:float")
def inner_product_bag(bag1, bag2, idx_name, idx_score):
    if bag1 is None or len(bag1) <= 0 or bag2 is None or len(bag2) <= 0:
        return 0

    bag1_dict = {}
    for entity in bag1:
        feature_name = entity[idx_name]
        feature_score = float(entity[idx_score])
        if feature_name not in bag1_dict:
            bag1_dict[feature_name] = feature_score

    final_score = 0.0
    for entity in bag2:
        feature_name = entity[idx_name]
        feature_score = float(entity[idx_score])
        if feature_name in bag1_dict:
            final_score += bag1_dict[feature_name] * feature_score

    return final_score

# cfb udf
@outputSchema("b:{t:(ts:long, content_id:chararray, click_count:double, view_count:double, gmp_score:double, prev_ts:long)}")
def gen_gmp(input_bag, base_timestamp, decay_factor, smooth_factor):
    if input_bag is None or len(input_bag) <= 0:
        return None
    if base_timestamp is None or base_timestamp <= 0:
        return None

    #pre process bag
    record_dict = {}
    content_id = None
    for record in input_bag:
        ts = long(record[0])
        content_id = record[1]
        click_count = record[2]
        view_count = record[3]
        prev_ts = long(record[4])
        record_dict[ts] = [click_count, view_count, prev_ts]

    out_bag = []
    timestamp_interval = range(base_timestamp, base_timestamp + 600*6*24, 600)
    prev_click = 0
    prev_view = 0
    prev_ts = 0
    prev_timestamp = base_timestamp - 600
    if prev_timestamp in record_dict:
        prev_click = record_dict[prev_timestamp][0]
        prev_view = record_dict[prev_timestamp][1]
        prev_ts = record_dict[prev_timestamp][2]

    for timestamp in timestamp_interval:
        total_click = 0
        total_view = 0
        if timestamp in record_dict:
            current_click = record_dict[timestamp][0]
            current_view = record_dict[timestamp][1]
            total_click = decay_factor * prev_click + current_click
            total_view = decay_factor * prev_view  + current_view
            prev_ts = timestamp
        else:
            total_click = decay_factor * prev_click
            total_view = decay_factor * prev_view

        gmp_score = (1.0*total_click)/(total_view + smooth_factor)
        out_bag.append(tuple([timestamp, content_id, total_click, total_view, gmp_score, prev_ts]))
        prev_click = total_click
        prev_view = total_view

    return out_bag

@outputSchema("b:{t:(ts:long, total_click_count:double, total_view_count:double)}")
def gen_agg_cfb(input_bag, current_timestamp, wnd, agg_wnd='2d', decay_factor=1.0):
     if input_bag is None or len(input_bag) <= 0:
        return None

     wnd = int(wnd)
     #the last window is 1 wnd before current ts
     current_timestamp = int(current_timestamp/wnd) * wnd - wnd

     period = agg_wnd[-1]
     num = int(agg_wnd[:-1])
     if period == 'h':
         interval = int(3600/wnd)
     elif period == 'd':
         interval = int(86400/wnd)
     else:
         interval = 1

     first_agg_ts = (current_timestamp - (num*interval-1)*wnd)
     filter_bag = [item for item in input_bag if int(item[0])>=first_agg_ts and int(item[0])<=current_timestamp]


     sort_raw = sorted(filter_bag, key=lambda x:x[0])
     print >> sys.stderr, str(sort_raw)

     sum_click = 0
     sum_view = 0
     last_ts = first_agg_ts
     for item in sort_raw:
         decay_wnd = (int(item[0]) - last_ts)/wnd
         factor = decay_factor**decay_wnd
         sum_click = sum_click*factor + float(item[1])
         sum_view = sum_view*factor + float(item[2])
         last_ts = int(item[0])

     last_wnd = (current_timestamp - last_ts)/wnd
     factor = decay_factor**last_wnd
     sum_click *= factor
     sum_view *= factor

     return (current_timestamp, sum_click, sum_view)

@outputSchema("b:{t:(ts:long, total_click_count:double, total_view_count:double, prev_ts:long)}")
def gen_cfb(input_bag, base_timestamp, current_timestamp, decay_factor, freq ='1d'):
    if input_bag is None or len(input_bag) <= 0:
        return None
    if base_timestamp is None or base_timestamp <= 0:
        return None

    period = freq[-1]
    num = freq[:-1]
    interval = 0
    if period == 'h':
        interval = 6
    elif period == 'd':
        interval = 144

    interval = int(num) * interval
    interval += 6

    valid_timestamp = current_timestamp - 600

    #pre process bag
    record_dict = {}
    for record in input_bag:
        ts = long(record[0])
        click_count = float(record[1])
        view_count = float(record[2])
        prev_ts = long(record[3])
        record_dict[ts] = [click_count, view_count, prev_ts]

    out_bag = []
    timestamp_interval = range(base_timestamp, base_timestamp + 600*interval, 600)
    prev_click = 0
    prev_view = 0
    prev_ts = 0
    prev_timestamp = base_timestamp - 600
    if prev_timestamp in record_dict:
        prev_click = record_dict[prev_timestamp][0]
        prev_view = record_dict[prev_timestamp][1]
        prev_ts = record_dict[prev_timestamp][2]

    for timestamp in timestamp_interval:
        total_click = 0
        total_view = 0
        if timestamp in record_dict:
            current_click = record_dict[timestamp][0]
            current_view = record_dict[timestamp][1]
            total_click = decay_factor * prev_click + current_click
            total_view = decay_factor * prev_view  + current_view
            prev_ts = timestamp
        else:
            total_click = decay_factor * prev_click
            total_view = decay_factor * prev_view

        if timestamp >= valid_timestamp:
            out_bag.append(tuple([timestamp, total_click, total_view, prev_ts]))
        prev_click = total_click
        prev_view = total_view

    return out_bag

@outputSchema("b:bag{t:tuple(feature_name:chararray, score:double)}")
def top(list,NReserved):
    if list is None:
        return None
    NReserved = int(NReserved)
    return list[:NReserved]

@outputSchema("b:bag{t:tuple(feature_name:chararray, score:double)}")
def normalize(list, fea_idx, score_idx, NReserved, style):
    if list is None:
        return None

    NReserved = int(NReserved)
    positive_list = filter(lambda elem: elem[score_idx] > 0.0, list)
    notnone_list = filter(lambda elem: elem[score_idx] is not None, positive_list)
    list = sorted(notnone_list, key=lambda elem:elem[score_idx], reverse=True)
    list = list[:NReserved]

#    if int(NReserved) < len(list):# if the NReserved position weight == NReserved -1 position weight, only keep the small id record
#        bounder_value = list[NReserved]
#        if bounder_value[1] >= list[NReserved-1][1] and long(bounder_value[0]) < long(list[NReserved-1][0]):
#            list[NReserved-1] = list[NReserved]
#    list = list[:NReserved]

    sum = 0;
    for item in list:
        if style == 'L0':
            sum = 1
        if style == 'L1':
            sum += abs(item[score_idx])
        elif style == 'L2':
            sum += item[score_idx] * item[score_idx]
        else:
            sum = 1

    if style == 'L2':
        sum = math.sqrt(sum)
    factor = 1.0
    if sum > 0:
        factor = 1.0 / sum

    outBag = []
    for item in list:
        tup = (item[fea_idx], item[score_idx]*factor)
        outBag.append(tup)
    return outBag

@outputSchema("b:bag{t:tuple(feature_type:chararray, feature_name:chararray, feauture_weight:double, feature_score:double)}")
def gen_feature(fields_score_map, feature_type):
    # print "gen_feature"
    if fields_score_map is None or len(fields_score_map) <= 0:
        return None

    feature_type = feature_type.lower()

    if True:
        feature_json_object = get_config_json_object()['feature_generation']
        if feature_type is None or feature_type =='' or (not feature_type in feature_json_object and feature_type != 'all'):
            print "plz provide valid feature_type"
            return None
        if feature_type != 'all':
            config_fields = feature_json_object[feature_type]
            out_bag = gen_feature_inner(fields_score_map, config_fields)
            if feature_type == 'match':
                return gen_match_feature(out_bag)
            return out_bag
        else:
            out_bag = []
            for json_item in feature_json_object:
                config_fields = feature_json_object[json_item]
                tmp_feature_bag = gen_feature_inner(fields_score_map, config_fields)
                if json_item == 'match':
                    out_bag.extend(gen_match_feature(tmp_feature_bag))
                else:
                    out_bag.extend(tmp_feature_bag)
	   	#print >> sys.stderr, config_fields, str(tmp_feature_bag)
	    #print >> sys.stderr, str(out_bag)

            return out_bag
    else:
        pass

def gen_match_feature(feature_bag):
    if feature_bag is None or len(feature_bag) <= 0:
        return []
    # feature_seq = chr(2)
    out_bag = []
    for entity in feature_bag:
        feature_name = entity[1]
        fields = feature_name.split(feature_seq)
        if fields[0] == fields[1]:
            out_bag.append(entity)
        else:
            continue
    return out_bag

def gen_feature_inner(fields_score_map,config_fields):
    outBag = []
    for feature_config in config_fields:
        label = feature_config["label"]
        fields = feature_config["fields"]
        score_name = feature_config["score"]
        if score_name not in fields_score_map:
            continue
        feature_score = float(fields_score_map[score_name])

        fields_list = []
        for field in fields:
            if field not in fields_score_map:
                break
            elif 'keyword' not in field and 'category' not in field:
                field_value = fields_score_map[field]
                if field_value is None:
                    field_value = ''
                fields_list.append([(field_value, 1.0, feature_score)])
            else:
                elements = fields_score_map[field]
                inner_list = []
                if elements is None:
                    continue
                for elem in elements:
                    inner_list.append((elem[0],float(elem[1]), feature_score))
                fields_list.append(inner_list)
        if len(fields_list) != len(fields):
            continue
        feature_name_list = []
        if len(fields_list) == 1:
            feature_name_list = fields_list[0]
        else:
            feature_name_list = combination_feature(fields_list)
        for entity in feature_name_list:
            outBag.append(tuple([label, entity[0], entity[1], entity[1] * entity[2]]))

    return outBag

def product(input_list):
    pools = input_list
    result = [[]]
    for pool in pools:
        result = [x+[y] for x in result for y in pool]
    for prod in result:
        yield tuple(prod)

def combination_feature(input_list):
    # feature_seq = chr(2)
    feature_out_tuples = product(input_list)
    out_features = []
    for feature in feature_out_tuples:
        feature_name = feature_seq.join([elem[0] for elem in feature])
        feature_weight = reduce(lambda x,y: x*y, [elem[1] for elem in feature],1)
        feature_score = min([elem[2] for elem in feature])
        out_features.append([feature_name, feature_weight, feature_score])
    return out_features

@outputSchema("out:chararray")
def concat_cfb_dump(key_type,key_name,pv,click,ts):
    seq1 = chr(1)
    seq2 = chr(2)
    return  key_type + seq1 + '%s\t%.2f\t%.2f\t%s' % (key_name,pv,click,str(ts))

@outputSchema("filter:chararray")
def exclude_feature_type(input_feature):
    if input_feature is None or input_feature != '':
        return '0'

    exclude_feature_types = get_config_json_object()['exclude_feature_type']
    exclude_feature_type_dict = {}
    for feature in exclude_feature_types:
        exclude_feature_type_dict[feature] = 1

    if input_feature in exclude_feature_type_dict:
        return '1'
    else:
        return '0'

@outputSchema("cfb_json:chararray")
def gen_cfb_json(cfb_bag):
    print "gen_cfb_json"
    if cfb_bag is None or len(cfb_bag) <= 0:
        return None

    cfb_dict = {}
    for feature_item in cfb_bag:
        feature_type = feature_item[0]
        feature_name = feature_item[1]
        feature_weight = feature_item[2]
        click_sum = feature_item[3]
        view_sum = feature_item[4]
        if 'keyword' not in feature_type.lower() and 'category' not in feature_type.lower():
            cfb_dict[feature_type] = [click_sum, view_sum]
        else:
            if feature_type in cfb_dict:
                feature_dict = cfb_dict[feature_type]
                feature_dict[feature_name] = [click_sum, view_sum, feature_weight]
                cfb_dict[feature_type] = feature_dict
            else:
                feature_dict = {}
                feature_dict[feature_name] = [click_sum, view_sum, feature_weight]
                cfb_dict[feature_type] = feature_dict

    return json.dumps(cfb_dict)

@outputSchema("data:chararray")
def gen_json_str(data):
    if data is None:
        return ""
    else:
        return json.dumps(data)

@outputSchema("categories:{t1:(y:map[])}")
def gen_empty_category_bag(source_type):
    return []

@outputSchema("data:{(itemid:chararray,score:float,gbdt_score:float,meta:chararray)}")
def flatten_doc_data(data):
    data_list = json.loads(data)
    result = []
    for item in data_list:
        itemid = item['id']
        score = item['score']
        gbdt_score = item['gbdt_score']
        result.append((itemid,score,gbdt_score,json.dumps(item)))
    return result
    # except:
    #     return []


if __name__ == '__main__':
    print get_random_bin('wedaaaaabbb23',5,'00')
    import sys
    sys.exit()
    ip_address = '49.15.86.0'
    print get_city_name(ip_address)
    print get_city_name('1.22.8.1')
    print get_city_name('1.22.8.30')
    print get_city_name('1.22.8.34')
    # print timestamp2dateformat(1448939957, '%Y%m%d%H%M')

