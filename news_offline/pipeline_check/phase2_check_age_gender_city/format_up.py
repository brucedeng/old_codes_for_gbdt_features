if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper
    import json
else:
    import com.xhaus.jyson.JysonCodec as json


@outputSchema('data:(uid:chararray,u_age:int,u_gender:int)')
def gen_up_id_age_gender(up_str):
    if up_str is None or len(up_str) <= 0:
        return ('',0)
    try:
        up_map = json.loads(up_str)
        aid = up_map.get('aid','')
        uid = up_map.get('uid',aid)
        u_age = up_map['age']
        u_gender = up_map['gender']
        return (str(uid),int(u_age),int(u_gender))
    except:
        return ('',0,0)

@outputSchema('data:(uid:chararray,f_age:int,f_gender:int,f_city:chararray)')
def gen_featurelog_age_gender_city(featurelog_str):
    if featurelog_str is None or len(featurelog_str) <= 0:
        return ('',0)
    try:
        featurelog_map = json.loads(featurelog_str)
        uid = featurelog_map['uid']
        f_age = featurelog_map['up']['age']
        f_gender = featurelog_map['up']['gender']
        f_city = featurelog_map['up']['city']
        return (str(uid),int(f_age),int(f_gender),str(f_city))
    except:
        return ('',0,0,'')
