# -*- coding:utf-8 -*-
import os
import datetime
import time
import math,sys

if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper
    import json
else:
    import com.xhaus.jyson.JysonCodec as json

#// non cfb feature start from 501
feature_logger_map = {"f1":"U_UID_EC","f2":"U_UID_C","f3":"U_UID_COEC","f4":"U_CITY_EC","f5":"U_CITY_C","f6":"U_CITY_COEC","f7":"U_CATEGORY_MEAN_EC","f8":"U_CATEGORY_MEAN_C","f9":"U_CATEGORY_MAX_EC","f10":"U_CATEGORY_MAX_C","f11":"U_CATEGORY_MICRO_MEAN_COEC","f12":"U_CATEGORY_MICRO_MAX_COEC","f13":"U_CATEGORY_MACRO_MEAN_COEC","f14":"U_CATEGORY_MACRO_MAX_COEC","f15":"U_KEYWORD_MEAN_EC","f16":"U_KEYWORD_MEAN_C","f17":"U_KEYWORD_MAX_EC","f18":"U_KEYWORD_MAX_C","f19":"U_KEYWORD_MICRO_MEAN_COEC","f20":"U_KEYWORD_MICRO_MAX_COEC","f21":"U_KEYWORD_MACRO_MEAN_COEC","f22":"U_KEYWORD_MACRO_MAX_COEC","f23":"D_CONID_EC","f24":"D_CONID_C","f25":"D_CONID_COEC","f26":"D_PUBLISHER_EC","f27":"D_PUBLISHER_C","f28":"D_PUBLISHER_COEC","f29":"D_CATEGORY_MEAN_EC","f30":"D_CATEGORY_MEAN_C","f31":"D_CATEGORY_MAX_EC","f32":"D_CATEGORY_MAX_C","f33":"D_CATEGORY_MICRO_MEAN_COEC","f34":"D_CATEGORY_MICRO_MAX_COEC","f35":"D_CATEGORY_MACRO_MEAN_COEC","f36":"D_CATEGORY_MACRO_MAX_COEC","f37":"C_D_U_CONID_CITY_EC","f38":"C_D_U_CONID_CITY_C","f39":"C_D_U_CONID_CITY_COEC","f40":"C_D_U_CONID_CATEGORY_MEAN_EC","f41":"C_D_U_CONID_CATEGORY_MEAN_C","f42":"C_D_U_CONID_CATEGORY_MAX_EC","f43":"C_D_U_CONID_CATEGORY_MAX_C","f44":"C_D_U_CONID_CATEGORY_MICRO_MEAN_COEC","f45":"C_D_U_CONID_CATEGORY_MICRO_MAX_COEC","f46":"C_D_U_CONID_CATEGORY_MACRO_MEAN_COEC","f47":"C_D_U_CONID_CATEGORY_MACRO_MAX_COEC","f48":"C_D_U_PUBLISHER_CITY_EC","f49":"C_D_U_PUBLISHER_CITY_C","f50":"C_D_U_PUBLISHER_CITY_COEC","f51":"C_D_U_PUBLISHER_AGE_EC","f52":"C_D_U_PUBLISHER_AGE_C","f53":"C_D_U_PUBLISHER_AGE_COEC","f54":"C_D_U_PUBLISHER_GENDER_EC","f55":"C_D_U_PUBLISHER_GENDER_C","f56":"C_D_U_PUBLISHER_GENDER_COEC","f57":"C_D_U_CATEGORY_CITY_MEAN_EC","f58":"C_D_U_CATEGORY_CITY_MEAN_C","f59":"C_D_U_CATEGORY_CITY_MAX_EC","f60":"C_D_U_CATEGORY_CITY_MAX_C","f61":"C_D_U_CATEGORY_CITY_MICRO_MEAN_COEC","f62":"C_D_U_CATEGORY_CITY_MICRO_MAX_COEC","f63":"C_D_U_CATEGORY_CITY_MACRO_MEAN_COEC","f64":"C_D_U_CATEGORY_CITY_MACRO_MAX_COEC","f65":"C_D_U_CATEGORY_AGE_MEAN_EC","f66":"C_D_U_CATEGORY_AGE_MEAN_C","f67":"C_D_U_CATEGORY_AGE_MAX_EC","f68":"C_D_U_CATEGORY_AGE_MAX_C","f69":"C_D_U_CATEGORY_AGE_MICRO_MEAN_COEC","f70":"C_D_U_CATEGORY_AGE_MICRO_MAX_COEC","f71":"C_D_U_CATEGORY_AGE_MACRO_MEAN_COEC","f72":"C_D_U_CATEGORY_AGE_MACRO_MAX_COEC","f73":"C_D_U_CATEGORY_GENDER_MEAN_EC","f74":"C_D_U_CATEGORY_GENDER_MEAN_C","f75":"C_D_U_CATEGORY_GENDER_MAX_EC","f76":"C_D_U_CATEGORY_GENDER_MAX_C","f77":"C_D_U_CATEGORY_GENDER_MICRO_MEAN_COEC","f78":"C_D_U_CATEGORY_GENDER_MICRO_MAX_COEC","f79":"C_D_U_CATEGORY_GENDER_MACRO_MEAN_COEC","f80":"C_D_U_CATEGORY_GENDER_MACRO_MAX_COEC","f81":"D_KEYWORD_MEAN_EC","f82":"D_KEYWORD_MEAN_C","f83":"D_KEYWORD_MAX_EC","f84":"D_KEYWORD_MAX_C","f85":"D_KEYWORD_MICRO_MEAN_COEC","f86":"D_KEYWORD_MICRO_MAX_COEC","f87":"D_KEYWORD_MACRO_MEAN_COEC","f88":"D_KEYWORD_MACRO_MAX_COEC","f89":"C_D_U_KEYWORD_CATEGORY_MEAN_EC","f90":"C_D_U_KEYWORD_CATEGORY_MEAN_C","f91":"C_D_U_KEYWORD_CATEGORY_MAX_EC","f92":"C_D_U_KEYWORD_CATEGORY_MAX_C","f93":"C_D_U_KEYWORD_CATEGORY_MICRO_MEAN_COEC","f94":"C_D_U_KEYWORD_CATEGORY_MICRO_MAX_COEC","f95":"C_D_U_KEYWORD_CATEGORY_MACRO_MEAN_COEC","f96":"C_D_U_KEYWORD_CATEGORY_MACRO_MAX_COEC","f97":"M_D_U_KEYWORD_KEYWORD_MEAN_EC","f98":"M_D_U_KEYWORD_KEYWORD_MEAN_C","f99":"M_D_U_KEYWORD_KEYWORD_MAX_EC","f100":"M_D_U_KEYWORD_KEYWORD_MAX_C","f101":"M_D_U_KEYWORD_KEYWORD_MICRO_MEAN_COEC","f102":"M_D_U_KEYWORD_KEYWORD_MICRO_MAX_COEC","f103":"M_D_U_KEYWORD_KEYWORD_MACRO_MEAN_COEC","f104":"M_D_U_KEYWORD_KEYWORD_MACRO_MAX_COEC","f501":"U_CAT_LEN","f502":"U_KW_LEN","f503":"U_AGE_1","f504":"U_AGE_2","f505":"U_GENDER","f506":"TIME_OF_DAY","f507":"DAY_OF_WEEK","f508":"PROD_ID_3","f509":"PROD_ID_5","f510":"WORD_COUNT","f511":"DOC_AGE","f512":"IMAGE_COUNT","f513":"NEWSY_SCORE","f514":"D_CAT_LEN","f515":"D_KW_LEN","f516":"CAT_REL","f517":"KW_REL","f105":"D_TITLEMD5_EC","f106":"D_TITLEMD5_C","f107":"D_TITLEMD5_COEC","f108":"C_D_U_TITLEMD5_CITY_EC","f109":"C_D_U_TITLEMD5_CITY_C","f110":"C_D_U_TITLEMD5_CITY_COEC","f111":"C_D_U_TITLEMD5_AGE_EC","f112":"C_D_U_TITLEMD5_AGE_C","f113":"C_D_U_TITLEMD5_AGE_COEC","f114":"C_D_U_TITLEMD5_GENDER_EC","f115":"C_D_U_TITLEMD5_GENDER_C","f116":"C_D_U_TITLEMD5_GENDER_COEC","f117":"C_D_U_TITLEMD5_CATEGORY_MEAN_EC","f118":"C_D_U_TITLEMD5_CATEGORY_MEAN_C","f119":"C_D_U_TITLEMD5_CATEGORY_MAX_EC","f120":"C_D_U_TITLEMD5_CATEGORY_MAX_C","f121":"C_D_U_TITLEMD5_CATEGORY_MICRO_MEAN_COEC","f122":"C_D_U_TITLEMD5_CATEGORY_MICRO_MAX_COEC","f123":"C_D_U_TITLEMD5_CATEGORY_MACRO_MEAN_COEC","f124":"C_D_U_TITLEMD5_CATEGORY_MACRO_MAX_COEC","f125":"D_GROUPID_EC","f126":"D_GROUPID_C","f127":"D_GROUPID_COEC","f128":"C_D_U_GROUIDTITLEMD5_CITY_EC","f129":"C_D_U_GROUPID_CITY_C","f130":"C_D_U_GROUPID_CITY_COEC","f131":"C_D_U_GROUPID_AGE_EC","f132":"C_D_U_GROUPID_AGE_C","f133":"C_D_U_GROUPID_AGE_COEC","f134":"C_D_U_GROUPID_GENDER_EC","f135":"C_D_U_GROUPID_GENDER_C","f136":"C_D_U_GROUPID_GENDER_COEC","f137":"C_D_U_GROUPID_CATEGORY_MEAN_EC","f138":"C_D_U_GROUPID_CATEGORY_MEAN_C","f139":"C_D_U_GROUPID_CATEGORY_MAX_EC","f140":"C_D_U_GROUPID_CATEGORY_MAX_C","f141":"C_D_U_GROUPID_CATEGORY_MICRO_MEAN_COEC","f142":"C_D_U_GROUPID_CATEGORY_MICRO_MAX_COEC","f143":"C_D_U_GROUPID_CATEGORY_MACRO_MEAN_COEC","f144":"C_D_U_GROUPID_CATEGORY_MACRO_MAX_COEC"}

@outputSchema("rid:chararray")
def parse_rid(cpack):
    if cpack is None:
        return ''
    if 'des' not in cpack:
        return ''
    str_data = cpack['des']
    data = str_data.split('|')
    data_map = {}
    for item in data:
        kv_list = item.split('=',1)
        if len(kv_list) == 2:
            data_map[kv_list[0]] = kv_list[1]
    if 'rid' in data_map:
        return data_map['rid']
    return ''

reserved_keys = set(["id","score","gbdt_score"])

@outputSchema("features:map[]")
def format_feature_log(features):
    if features is None:
        return None
    result = {}
    for k in features:
        if k in reserved_keys:
            continue
        if k not in feature_logger_map:
            continue
        result[feature_logger_map[k]] = float(features[k])

    return result

@outputSchema("fstat:{T:(fname:chararray,cnt:int,v:float,v_square:float)}")
def stat_feature_log(features):
    if features is None:
        return []
    result = []
    for k in features:
        if k in reserved_keys:
            continue
        if k not in feature_logger_map:
            continue
        v = float(features[k])
        if v > 0:
            cnt = 1
        else:
            cnt = 0
        result.append((feature_logger_map[k],cnt,v,v*v))
        # result[feature_logger_map[k]] = features[k]

    return result


def get_feature_lists():
    #load feature map file
    if get_feature_lists._global_feature_map is None:
        feature_map_file = 'feature.map'
        f3 = open(feature_map_file, 'r')
        feature_map = {}
        feature_list = []
        for line in f3:
            line = line.rstrip('\n')
            if line == '':
                continue
            fid, fname, ftype = line.split('\t')
            if fid.startswith('#'):
                fid = fid.replace('#','').strip()
            feature_map[fname] = fid
            feature_list.append(fname)
        get_feature_lists._global_feature_map = feature_map
        get_feature_lists._global_feature_list = feature_list
        f3.close()
    return get_feature_lists._global_feature_list, get_feature_lists._global_feature_map

get_feature_lists._global_feature_map = None
get_feature_lists._global_feature_list = None

@outputSchema("features:chararray")
def format_train_data(label, log_features, gen_features):
    out_put = []
    out_put.append(str(label))
    # try:
    #     log_features = json.loads(log_features)
    # except:
    #     print 'error loading log_features'
    #     print log_features
    #     log_features = None
    # print static_features, cfb_features
    if log_features is None:
        log_features = {}
    if gen_features is None:
        gen_features = {}

    feature_list, feature_map = get_feature_lists()
    for feature_name in feature_list:
        if feature_name in log_features:
            fvalue = log_features[feature_name]
        elif feature_name in gen_features:
            fvalue = gen_features[feature_name]
        else:
            continue

        if fvalue is None or ( type(fvalue) == type("") and fvalue==""):
            continue
        fid = feature_map[feature_name]
        out_put.append(fid + ":" + str(fvalue))

    return ' '.join(out_put)

@outputSchema("b:{t:(rid:chararray,pid:chararray,predictor_id:chararray,ts:long,doc_size:long,uid:chararray,up:map[],features:[])}")
def parse_json_featurelog(line):
    try:
        data_map = json.loads(line)
    except:
        print "error parsing json data."
        print line
        return [(None,None,None,None,None,None,None,None)]

    docs = data_map.get('docs')

    result = [(None,None,None,None,None,None,None,None)]
    if docs is not None:
        for item in docs:
            result.append((data_map.get('req_id',''),data_map.get('prod_id'),data_map.get('predictor_id'),data_map.get('now'),data_map.get('doc_size'),data_map.get('uid'),data_map.get('up'),item))

    return result
    # return (data_map.get('req_id',''),data_map.get('prod_id'),data_map.get('predictor_id'),data_map.get('now'),data_map.get('doc_size'),data_map.get('uid'),data_map.get('up'),docs)

@outputSchema("f:[]")
def json_to_map(data):
    try:
        result = json.loads(data)
        return result
    except:
        return {}

@outputSchema("flag:chararray")
def filter_expired(current_ts, prev_ts, expired_ts):
    if prev_ts is None or current_ts is None or expired_ts is None:
        return '0'
    if long(current_ts) - long(prev_ts) <= long(expired_ts):
        return '1'
    else:
        return '0'

@outputSchema("flag:chararray")
def filter_date(current_ts, prev_ts):
    if prev_ts is None or current_ts is None :
        return '0'
    if long(current_ts) - long(prev_ts) >= 0:
        return '1'
    else:
        return '0'

@outputSchema("flag:chararray")
def filter_flag(filter_field, flag):
    if filter_field is None:
        return '0'
    try:
        filter_field = str(filter_field)
        if filter_field == flag:
            return '1'
        else:
            return '0'
    except:
        return '0'

# preprocess log udf
@outputSchema("interval:int")
def get_interval(current_ts, prev_ts, wnd):
    if prev_ts is None or current_ts is None:
        return 0
    return (current_ts - prev_ts)/(wnd)

@outputSchema("power:double")
def power(base, n):
    if base is None or n is None:
        return None
    return float(base) ** float(n)

@outputSchema("date:chararray")
def substring(date_string, start, end):
    if date_string is None or date_string == '':
        return None
    if start >= end:
        return None
    return date_string[start:end]

@outputSchema("timestamp:long")
def date2timestamp(date_str, wnd, format_str='%Y%m%d%H%M'):
    if date_str is None  or format_str is None:
        return None
    try:
        date_str = str(date_str)
        timestamp = int(time.mktime(datetime.datetime.strptime(date_str, format_str).timetuple()))
        timestamp = timestamp/long(wnd) * long(wnd)
        return timestamp
    except:
        return None

@outputSchema("interval:int")
def diff_date(current_date, prev_date):
    if current_date is None or len(current_date) < 10 or prev_date is None or len(prev_date) < 10:
        return 0
    try:
        current_timestamp = int(time.mktime(datetime.datetime.strptime(current_date, '%Y%m%d%H%M').timetuple()))
        prev_timestamp = int(time.mktime(datetime.datetime.strptime(prev_date, '%Y%m%d%H%M').timetuple()))
        diff_timestamp = current_timestamp - prev_timestamp

        return diff_timestamp/600
    except:
        return 0


@outputSchema("date:chararray")
def date2date10min(date_string):
    if date_string is None or len(date_string) < 10:
        return None
    try:
        timestamp = int(time.mktime(datetime.datetime.strptime(date_string, '%Y-%m-%d %H:%M:%S').timetuple()))
        timestamp = (timestamp/600) * 600
        return datetime.datetime.fromtimestamp(timestamp).strftime('%Y%m%d%H%M')
    except:
        return None

@outputSchema("date:chararray")
def timestamp2date(timestamp):
    if timestamp is None or len(timestamp) < 10:
        return None
    try:
        timestamp = long(timestamp)
        return datetime.datetime.fromtimestamp(timestamp).strftime('%Y%m%d%H%M')
    except:
        return None

@outputSchema("date:chararray")
def timestamp2dateformat(timestamp,format_str='%Y%m%d%H'):
    try:
        timestamp = long(timestamp)
        return datetime.datetime.fromtimestamp(timestamp).strftime(format_str)
    except:
        return None

@outputSchema("hot:int")
def get_hot(rank_type):
    if rank_type is None or rank_type == '':
        return None
    try:
        rank_type_int = int(rank_type)
        hot_flag = rank_type_int &0x1
        return hot_flag
    except:
        return None

@outputSchema("channel:chararray")
def get_channel(rank_type):
    if rank_type is None or rank_type == '':
        return -1
    try:
        rank_type_int = int(rank_type)
        channel_flag = (rank_type_int >>1) & 0xff
        return global_channel_dict[channel_flag]
    except:
        return 'B102'

@outputSchema("b:{t:(content_id:chararray,cid:chararray,eventtime:chararray,requesttime:chararray,ranktypes:chararray)}")

def extract_view_fields(input_list):
    if input_list is None or len(input_list) <= 0 :
        return None
    item_length = len(input_list[0])

    out_bag = []
    try:
        for index in xrange(0,item_length):
            inner_list = []
            for item in input_list:
                if len(item) <= index:
                    inner_list.append(item[len(item) - 1][0].strip())
                else:
                    inner_list.append(item[index][0].strip())
            out_bag.append(tuple(inner_list))
    except:
        pass
    return out_bag

#static feature udf
@outputSchema("time_of_day:int")
def time_of_day(event_time):
    if event_time is None:
        return -1

    time_of_day  = time.localtime(event_time).tm_hour
    return time_of_day

@outputSchema("time_of_week:int")
def day_of_week(event_time):
    if event_time is None:
        return -1

    day_of_week = time.strftime("%w", time.localtime(event_time));
    return day_of_week
@outputSchema("rel:float")
def inner_product_bag(bag1, bag2, idx_name, idx_score):
    if bag1 is None or len(bag1) <= 0 or bag2 is None or len(bag2) <= 0:
        return 0

    bag1_dict = {}
    for entity in bag1:
        feature_name = entity[idx_name]
        feature_score = float(entity[idx_score])
        if feature_name not in bag1_dict:
            bag1_dict[feature_name] = feature_score

    final_score = 0.0
    for entity in bag2:
        feature_name = entity[idx_name]
        feature_score = float(entity[idx_score])
        if feature_name in bag1_dict:
            final_score += bag1_dict[feature_name] * feature_score

    return final_score

# cfb udf
@outputSchema("b:{t:(ts:long, content_id:chararray, click_count:double, view_count:double, gmp_score:double, prev_ts:long)}")
def gen_gmp(input_bag, base_timestamp, decay_factor, smooth_factor):
    if input_bag is None or len(input_bag) <= 0:
        return None
    if base_timestamp is None or base_timestamp <= 0:
        return None

    #pre process bag
    record_dict = {}
    content_id = None
    for record in input_bag:
        ts = long(record[0])
        content_id = record[1]
        click_count = record[2]
        view_count = record[3]
        prev_ts = long(record[4])
        record_dict[ts] = [click_count, view_count, prev_ts]

    out_bag = []
    timestamp_interval = range(base_timestamp, base_timestamp + 600*6*24, 600)
    prev_click = 0
    prev_view = 0
    prev_ts = 0
    prev_timestamp = base_timestamp - 600
    if prev_timestamp in record_dict:
        prev_click = record_dict[prev_timestamp][0]
        prev_view = record_dict[prev_timestamp][1]
        prev_ts = record_dict[prev_timestamp][2]

    for timestamp in timestamp_interval:
        total_click = 0
        total_view = 0
        if timestamp in record_dict:
            current_click = record_dict[timestamp][0]
            current_view = record_dict[timestamp][1]
            total_click = decay_factor * prev_click + current_click
            total_view = decay_factor * prev_view  + current_view
            prev_ts = timestamp
        else:
            total_click = decay_factor * prev_click
            total_view = decay_factor * prev_view

        gmp_score = (1.0*total_click)/(total_view + smooth_factor)
        out_bag.append(tuple([timestamp, content_id, total_click, total_view, gmp_score, prev_ts]))
        prev_click = total_click
        prev_view = total_view

    return out_bag

@outputSchema("b:{t:(ts:long, total_click_count:double, total_view_count:double)}")
def gen_agg_cfb(input_bag, current_timestamp, wnd, agg_wnd='2d', decay_factor=1.0):
     if input_bag is None or len(input_bag) <= 0:
        return None

     wnd = int(wnd)
     #the last window is 1 wnd before current ts
     current_timestamp = int(current_timestamp/wnd) * wnd - wnd

     period = agg_wnd[-1]
     num = int(agg_wnd[:-1])
     if period == 'h':
         interval = int(3600/wnd)
     elif period == 'd':
         interval = int(86400/wnd)
     else:
         interval = 1

     first_agg_ts = (current_timestamp - (num*interval-1)*wnd)
     filter_bag = [item for item in input_bag if int(item[0])>=first_agg_ts and int(item[0])<=current_timestamp]


     sort_raw = sorted(filter_bag, key=lambda x:x[0])
     print >> sys.stderr, str(sort_raw)

     sum_click = 0
     sum_view = 0
     last_ts = first_agg_ts
     for item in sort_raw:
         decay_wnd = (int(item[0]) - last_ts)/wnd
         factor = decay_factor**decay_wnd
         sum_click = sum_click*factor + float(item[1])
         sum_view = sum_view*factor + float(item[2])
         last_ts = int(item[0])

     last_wnd = (current_timestamp - last_ts)/wnd
     factor = decay_factor**last_wnd
     sum_click *= factor
     sum_view *= factor

     return (current_timestamp, sum_click, sum_view)

@outputSchema("b:{t:(ts:long, total_click_count:double, total_view_count:double, prev_ts:long)}")
def gen_cfb(input_bag, base_timestamp, current_timestamp, decay_factor, freq ='1d'):
    if input_bag is None or len(input_bag) <= 0:
        return None
    if base_timestamp is None or base_timestamp <= 0:
        return None

    period = freq[-1]
    num = freq[:-1]
    interval = 0
    if period == 'h':
        interval = 6
    elif period == 'd':
        interval = 144

    interval = int(num) * interval
    interval += 6

    valid_timestamp = current_timestamp - 600

    #pre process bag
    record_dict = {}
    for record in input_bag:
        ts = long(record[0])
        click_count = float(record[1])
        view_count = float(record[2])
        prev_ts = long(record[3])
        record_dict[ts] = [click_count, view_count, prev_ts]

    out_bag = []
    timestamp_interval = range(base_timestamp, base_timestamp + 600*interval, 600)
    prev_click = 0
    prev_view = 0
    prev_ts = 0
    prev_timestamp = base_timestamp - 600
    if prev_timestamp in record_dict:
        prev_click = record_dict[prev_timestamp][0]
        prev_view = record_dict[prev_timestamp][1]
        prev_ts = record_dict[prev_timestamp][2]

    for timestamp in timestamp_interval:
        total_click = 0
        total_view = 0
        if timestamp in record_dict:
            current_click = record_dict[timestamp][0]
            current_view = record_dict[timestamp][1]
            total_click = decay_factor * prev_click + current_click
            total_view = decay_factor * prev_view  + current_view
            prev_ts = timestamp
        else:
            total_click = decay_factor * prev_click
            total_view = decay_factor * prev_view

        if timestamp >= valid_timestamp:
            out_bag.append(tuple([timestamp, total_click, total_view, prev_ts]))
        prev_click = total_click
        prev_view = total_view

    return out_bag

@outputSchema("b:bag{t:tuple(feature_name:chararray, score:double)}")
def top(list,NReserved):
    if list is None:
        return None
    NReserved = int(NReserved)
    return list[:NReserved]

@outputSchema("b:bag{t:tuple(feature_name:chararray, score:double)}")
def normalize(list, fea_idx, score_idx, NReserved, style):
    if list is None:
        return None

    NReserved = int(NReserved)
    positive_list = filter(lambda elem: elem[score_idx] > 0.0, list)
    notnone_list = filter(lambda elem: elem[score_idx] is not None, positive_list)
    list = sorted(notnone_list, key=lambda elem:elem[score_idx], reverse=True)
    list = list[:NReserved]

#    if int(NReserved) < len(list):# if the NReserved position weight == NReserved -1 position weight, only keep the small id record
#        bounder_value = list[NReserved]
#        if bounder_value[1] >= list[NReserved-1][1] and long(bounder_value[0]) < long(list[NReserved-1][0]):
#            list[NReserved-1] = list[NReserved]
#    list = list[:NReserved]

    sum = 0;
    for item in list:
        if style == 'L0':
            sum = 1
        if style == 'L1':
            sum += abs(item[score_idx])
        elif style == 'L2':
            sum += item[score_idx] * item[score_idx]
        else:
            sum = 1

    if style == 'L2':
        sum = math.sqrt(sum)
    factor = 1.0
    if sum > 0:
        factor = 1.0 / sum

    outBag = []
    for item in list:
        tup = (item[fea_idx], item[score_idx]*factor)
        outBag.append(tup)
    return outBag

@outputSchema("b:bag{t:tuple(feature_type:chararray, feature_name:chararray, feauture_weight:double, feature_score:double)}")
def gen_feature(fields_score_map, feature_type):
    # print "gen_feature"
    if fields_score_map is None or len(fields_score_map) <= 0:
        return None

    feature_type = feature_type.lower()

    if True:
        feature_json_object = get_config_json_object()['feature_generation']
        if feature_type is None or feature_type =='' or (not feature_type in feature_json_object and feature_type != 'all'):
            print "plz provide valid feature_type"
            return None
        if feature_type != 'all':
            config_fields = feature_json_object[feature_type]
            out_bag = gen_feature_inner(fields_score_map, config_fields)
            if feature_type == 'match':
                return gen_match_feature(out_bag)
            return out_bag
        else:
            out_bag = []
            for json_item in feature_json_object:
                config_fields = feature_json_object[json_item]
                tmp_feature_bag = gen_feature_inner(fields_score_map, config_fields)
                if json_item == 'match':
                    out_bag.extend(gen_match_feature(tmp_feature_bag))
                else:
                    out_bag.extend(tmp_feature_bag)
	   	#print >> sys.stderr, config_fields, str(tmp_feature_bag)
	    #print >> sys.stderr, str(out_bag)

            return out_bag
    else:
        pass

def gen_match_feature(feature_bag):
    if feature_bag is None or len(feature_bag) <= 0:
        return []
    feature_seq = chr(2)
    out_bag = []
    for entity in feature_bag:
        feature_name = entity[1]
        fields = feature_name.split(feature_seq)
        if fields[0] == fields[1]:
            out_bag.append(entity)
        else:
            continue
    return out_bag

def gen_feature_inner(fields_score_map,config_fields):
    outBag = []
    for feature_config in config_fields:
        label = feature_config["label"]
        fields = feature_config["fields"]
        score_name = feature_config["score"]
        if score_name not in fields_score_map:
            continue
        feature_score = float(fields_score_map[score_name])

        fields_list = []
        for field in fields:
            if field not in fields_score_map:
                break
            elif 'keyword' not in field and 'category' not in field:
                field_value = fields_score_map[field]
                if field_value is None:
                    field_value = ''
                fields_list.append([(field_value, 1.0, feature_score)])
            else:
                elements = fields_score_map[field]
                inner_list = []
                if elements is None:
                    continue
                for elem in elements:
                    inner_list.append((elem[0],float(elem[1]), feature_score))
                fields_list.append(inner_list)
        if len(fields_list) != len(fields):
            continue
        feature_name_list = []
        if len(fields_list) == 1:
            feature_name_list = fields_list[0]
        else:
            feature_name_list = combination_feature(fields_list)
        for entity in feature_name_list:
            outBag.append(tuple([label, entity[0], entity[1], entity[1] * entity[2]]))

    return outBag

def product(input_list):
    pools = input_list
    result = [[]]
    for pool in pools:
        result = [x+[y] for x in result for y in pool]
    for prod in result:
        yield tuple(prod)

def combination_feature(input_list):
    feature_seq = chr(2)
    feature_out_tuples = product(input_list)
    out_features = []
    for feature in feature_out_tuples:
        feature_name = feature_seq.join([elem[0] for elem in feature])
        feature_weight = reduce(lambda x,y: x*y, [elem[1] for elem in feature],1)
        feature_score = min([elem[2] for elem in feature])
        out_features.append([feature_name, feature_weight, feature_score])
    return out_features

@outputSchema("out:chararray")
def concat_cfb_dump(key_type,key_name,pv,click,ts):
    seq1 = chr(1)
    seq2 = chr(2)
    return  key_type + seq1 + '%s\t%.2f\t%.2f\t%s' % (key_name,pv,click,str(ts))

@outputSchema("filter:chararray")
def exclude_feature_type(input_feature):
    if input_feature is None or input_feature != '':
        return '0'

    exclude_feature_types = get_config_json_object()['exclude_feature_type']
    exclude_feature_type_dict = {}
    for feature in exclude_feature_types:
        exclude_feature_type_dict[feature] = 1

    if input_feature in exclude_feature_type_dict:
        return '1'
    else:
        return '0'

@outputSchema("cfb_json:chararray")
def gen_cfb_json(cfb_bag):
    print "gen_cfb_json"
    if cfb_bag is None or len(cfb_bag) <= 0:
        return None

    cfb_dict = {}
    for feature_item in cfb_bag:
        feature_type = feature_item[0]
        feature_name = feature_item[1]
        feature_weight = feature_item[2]
        click_sum = feature_item[3]
        view_sum = feature_item[4]
        if 'keyword' not in feature_type.lower() and 'category' not in feature_type.lower():
            cfb_dict[feature_type] = [click_sum, view_sum]
        else:
            if feature_type in cfb_dict:
                feature_dict = cfb_dict[feature_type]
                feature_dict[feature_name] = [click_sum, view_sum, feature_weight]
                cfb_dict[feature_type] = feature_dict
            else:
                feature_dict = {}
                feature_dict[feature_name] = [click_sum, view_sum, feature_weight]
                cfb_dict[feature_type] = feature_dict

    return json.dumps(cfb_dict)

@outputSchema("data:chararray")
def gen_json_str(data):
    if data is None:
        return ""
    else:
        return json.dumps(data)

@outputSchema("categories:{t1:(y:map[])}")
def gen_empty_category_bag(source_type):
    return []

@outputSchema("data:{(itemid:chararray,score:float,gbdt_score:float,meta:chararray)}")
def flatten_doc_data(data):
    data_list = json.loads(data)
    result = []
    for item in data_list:
        itemid = item['id']
        score = item['score']
        gbdt_score = item['gbdt_score']
        result.append((itemid,score,gbdt_score,json.dumps(item)))
    return result
    # except:
    #     return []

import math
@outputSchema("data:{(cnt:int,ratio:double)}")
def eval_agedecay(data, decay_factor, window=12):
    if data is None:
        return []
    data = sorted(data, key=lambda x:x[0], reverse=True)
    idx = [i+2 for i in xrange(len(data))]
    decay_data = []
    for idx in xrange(len(data)):
        score = data[idx][0]
        age = data[idx][1]
        decay_age = int(age) / int(window)
        decayed_score = score * math.exp(decay_factor * decay_age)
        decay_data.append((idx+2,score,age,decayed_score))
    data_order = sorted(decay_data, key=lambda _x: _x[3], reverse=True)

    diff = 0.0
    overall_diff = 0.0
    for idx in xrange(len(data_order)-1):
        cur_idx = idx + 2
        for idx2 in xrange(idx,len(data_order)):
            if data_order[idx] > data_order[idx2]:
                diff += 1/float(min(cur_idx,data_order[idx2]))
            overall_diff += 1/float(cur_idx)
    diff_ratio = diff/overall_diff
    return [(len(decay_data),diff)]

@outputSchema("data:float")
def eval_age_score_diff(data):
    if data is None:
        return []
    data = sorted(data, key=lambda x:x[0], reverse=True)
    idx = [i+2 for i in xrange(len(data))]
    decay_data = []
    for idx in xrange(len(data)):
        score = data[idx][0]
        age = data[idx][1]
        decay_data.append((idx+2,score,age))
    data_order = sorted(decay_data, key=lambda _x: _x[2])

    diff = 0.0
    overall_diff = 0.0
    for idx in xrange(len(data_order)-1):
        cur_idx = idx + 2
        for idx2 in xrange(idx,len(data_order)):
            if data_order[idx] > data_order[idx2]:
                diff += 1/float(min(cur_idx,data_order[idx2]))
            overall_diff += 1/float(cur_idx)
    diff_ratio = diff/overall_diff
    # return [(len(decay_data),diff)]
    return diff

if __name__ == '__main__':
    # cpack={"des":"rid=4764d473|src=0|ord=21","ishot":1,"md5":"ce35a256"}
    # # print get_city_name(ip_address)
    # print parse_rid(cpack)

    test_data = [(0.575121, 0), (0.315087, 0), (0.769606, 28), (0.663911, 14), (0.619393, 7), (0.605951, 7), (0.597192, 12), (0.789484, 15), (0.788263, 21), (0.751729, 4), (0.637523, 2), (0.58226, 15), (0.717743, 19), (0.506002, 11), (0.771067, 19), (0.761952, 33), (0.703695, 0), (0.583218, 11), (0.581618, 22), (0.68679, 10), (0.488412, 18), (0.698008, 11), (0.693737, 14), (0.676402, 15), (0.548046, 8), (0.580491, 16), (0.673016, 19), (0.481069, 22), (0.657621, 5), (0.654056, 0), (0.657121, 17), (0.539093, 13), (0.560996, 8), (0.616917, 17), (0.467479, 9), (0.656144, 21), (0.644535, 15), (0.459491, 16), (0.530732, 21), (0.47377, 10), (0.62453, 0), (0.612112, 17), (0.627947, 20), (0.62871, 14), (0.513529, 17), (0.450891, 2), (0.531336, 18), (0.450404, 12), (0.587207, 14), (0.619804, 24), (0.601812, 19), (0.546827, 8), (0.503267, 12), (0.477676, 6), (0.595158, 10), (0.535307, 5), (0.609251, 18), (0.596666, 13), (0.448174, 12), (0.570293, 18), (0.449531, 8), (0.47107, 8), (0.562582, 20), (0.595103, 15), (0.587098, 18), (0.580039, 19), (0.551281, 19), (0.531903, 23), (0.583767, 8), (0.549999, 15), (0.447106, 11), (0.575564, 9), (0.574901, 13), (0.527351, 10), (0.526853, 20), (0.573627, 20), (0.5465, 15), (0.573246, 23), (0.444067, 12), (0.570468, 1), (0.523399, 17), (0.443441, 17), (0.566035, 22), (0.522856, 14), (0.570719, 10), (0.44286, 0), (0.564173, 18), (0.51687, 17), (0.516403, 14), (0.5637, 23), (0.508691, 23), (0.440879, 12), (0.559684, 13), (0.559684, 23), (0.507636, 17), (0.504328, 1), (0.555337, 0), (0.49325, 10), (0.437088, 20), (0.55921, 14)]
    test_data = sorted(test_data,key=lambda x:x[0], reverse=True)
    for i in range(10):
        ratio = -0.01 - i/500.0
        print ratio,eval_agedecay(test_data,ratio)

