REGISTER 'lib/*.jar';
REGISTER 'feature_log_util.py' USING jython AS myfunc;
REGISTER 'utils.py' USING jython AS myfunc_ip;

set mapred.create.symlink yes;
set default_parallel 1;
set fs.s3a.connection.maximum 1000;
set mapred.cache.files $IP_CITY

raw_rcv_log = LOAD '$INPUT_EVENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate
    json,
    json#'aid' as uid:chararray,
    json#'ip' as ip:chararray,
    json#'pid' as pid:chararray,
    json#'ctype' as ctype,
    json#'act' as act,
    myfunc.parse_rid(json#'cpack') as rid:chararray;


raw_rcv_log = filter raw_rcv_log by pid == '11' and  ( ctype is not null and (ctype=='1' or ctype=='0x01' ) ) and act is not null and ( act == '1' or act == '2' or act=='3' or act=='4' ) and json#'scenario'#'level1_type' =='1' and json#'scenario'#'level1' == '1';

log_click_view = FOREACH raw_rcv_log GENERATE uid, rid, myfunc_ip.get_city_name(ip) as city, pid as pid;

----------

feature_log = LOAD '$INPUT_FEATURE_LOG' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

feature_log = FOREACH feature_log GENERATE
    json#'req_id' as rid:chararray,
    json#'prod_id' as pid:chararray,
    json#'uid' as uid:chararray,
    json#'up'#'city' as city:chararray;

feature_log = FILTER feature_log by pid=='11';

data_jnd = join log_click_view by (uid, pid,rid), feature_log by (uid, pid, rid);

--res = FOREACH data_jnd GENERATE (log_click_view::city == feature_log::city ? 0 : 1) as miss_city,  log_click_view::uid,log_click_view::pid,log_click_view::rid,feature_log::uid,feature_log::pid,feature_log::rid;

res_miss = FOREACH data_jnd GENERATE (log_click_view::city == feature_log::city ? 0 : 1) as miss_city;
res_group = FOREACH (GROUP res_miss ALL) GENERATE COUNT($1.miss_city) as cnt, SUM($1.miss_city) as sum;
res = FOREACH res_group GENERATE cnt,sum,(double)sum/cnt;

store res into '$OUTPUT';
