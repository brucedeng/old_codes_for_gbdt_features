#!/usr/bin/env python
# %matplotlib inline
# from matplotlib import pyplot as plt
import sys
import argparse
import datetime
import os, urllib2, json
import copy
import traceback

def load_gmp(path, threshold=20.0):
    res = {}
    file = open(path)
    total_ec = 0
    total_c = 0

    for line in file:
        f = line.split('\t')
        item_id = str(f[0])
        original_ec = float(f[2])
        ec = float(f[4])
        c = float(f[5])

        if c >0 and ec >0 and original_ec > threshold and c <= ec:
            coec = c/ec
            res[item_id] = coec #(c,ec,coec)

        total_ec += ec
        total_c += c

    res['total'] = total_c/total_ec#(total_c,total_ec,total_c/total_ec)
    file.close()

    return res

def avg_list_step(data,step):
    length = len(data)
    length = length / step * step
    i = step
    result = []
    while i <= length:
        result.append(sum(data[i-step:i])/float(step))
        i += step
    return result

def get_rank_features(json_str):
    try:
        data = json.loads(json_str)
        return float(data['D_CONID_COEC'])
    except:
        return 0.0

def calc_rel(a,b):
    if a is None or b is None or len(a) == 0 or len(b) == 0:
        return 0.0
    result = 0.0
    for k,v in a.items():
        # print k,v
        result += float(v) * float(b.get(k,0.0))
    return result

def get_feature_lists(filename):
    #load feature map file
    feature_map_file = filename
    f3 = open(feature_map_file, 'r')
    feature_map = {}
    feature_list = []
    for line in f3:
        line = line.rstrip('\n')
        if line == '':
            continue
        fid, fname, ftype = line.split('\t')
        if fid.startswith('#'):
            fid = fid.replace('#','').strip()
        feature_map[fname] = fid
        feature_list.append(fname)
    f3.close()
    return feature_map

if __name__=='__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-u', '--url', action='store', dest='url', help='serving url.')
    
    args = arg_parser.parse_args()

    url = args.url
    response = urllib2.urlopen(url)
    data = json.loads(response.read())

    user_profile = data['z_debug']['userprofile']
    # user_profile = data['userprofile']
    u_categories = user_profile['categories']
    u_keywords = user_profile['keywords']

    items = data['newsid']
    # items = data['data']
    now = int(datetime.datetime.now().strftime('%s'))

    for item in items:
        try:
            fields = item['fields']
            item_id = item['id']
            publish_time = int(fields["publish_time"])
            des = item['des']
            ranking_features = fields["ranking_features"]
            doc_age = (now - publish_time)/3600.0
            # features = json.loads(fields.get('ranking_features','{}'))
            # score = features['gbdt']
            result = str(item_id) + '\t' + str(doc_age) + '\t' + des
            print ranking_features.strip()
        except:
            traceback.print_exc()
            pass

