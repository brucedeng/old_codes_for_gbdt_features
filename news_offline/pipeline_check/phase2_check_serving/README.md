# serving 结果和离线 model 检查

这个目录下的code是用来检查serving的脚本和离线打分code的结果是否一致, 并且分析最终排序的分数与位置的关系.

使用方法:

* dump 线上 serving 返回的 feature , 存储在文本文件里. dump的脚本提取出serving返回结果里的 ranking_features 字段并将其打印到终端.

```
python dump_online_feature.py -u 'http://10.5.1.74/r/?n=1&count=1000&uid=e4a81461da44bc7a&ncid=29&ip=10.118.97.130&pid=14&act=3&scenario=0x001d0101&lan=en_US&osv=5.1&appv=1.2.0&app_lan=en_US&ch=200001&pf=android&net=wifi&v=3&mnc=22&ctype=0x267&display=0x38f&mode=1&brand=Panasonic&mcc=404&nmnc=90&nmcc=404&model=ELUGA_I2&location=1&columnid=29&page=1&exp=nr_es_004&debug=true&cache=false' > test.txt
```

* 使用jar包进行离线打分. gbdt_runtime-0.5.0-jar-with-dependencies.jar 可以读取json格式的feature , 并打分. 输入有两个参数, 第一个参数是model的dump, 第二个参数是feature文件. 如果第二个参数是 "-", 则表示从stdin读取feature并逐行打分.

```
java -jar /data1/bin/gbdt_runtime-0.5.0-jar-with-dependencies.jar <model_file> <input_feature>
```

```
java -jar /data1/bin/gbdt_runtime-0.5.0-jar-with-dependencies.jar ~/india_news_ranking/models/nr/en_us/nr_en_us_p2_20160922_2h_up.dump test.txt
```

jar包在news_model 10.2.2.238 机器的/data1/bin/gbdt_runtime-0.5.0-jar-with-dependencies.jar 目录下. 生成jar包的代码在 gbdt_runtime/ , 用法参见 gbdt_runtime/README.md

此代码目的是为了:

1. 核对线上打分和离线打分结果是不是一致
2. 离线用同一组数据检查不同model的效果