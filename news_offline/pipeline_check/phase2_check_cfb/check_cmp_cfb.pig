online_cfb = LOAD  '$ONCFB' AS (ts:int,feature_type:chararray,feature_name:chararray,weight:double,score:double,lan:chararray);
offline_cfb = LOAD '$OFFCFB' AS (ts:int,feature_type:chararray,feature_name:chararray,weight:double,score:double,lan:chararray);

online_grp = FOREACH ( GROUP online_cfb by (feature_type,feature_name,lan)) GENERATE 
$0.feature_type as feature_type,
REPLACE($0.feature_name, '\u0001', '\u0002') as feature_name:chararray,
SUM($1.weight) as weight,
SUM($1.score) as score,
$0.lan as lan;

offline_grp = FOREACH ( GROUP offline_cfb by (feature_type,feature_name,lan)) GENERATE 
$0.feature_type as feature_type,
REPLACE($0.feature_name, '\u0001', '\u0002') as feature_name:chararray,
SUM($1.weight) as weight,
SUM($1.score) as score,
$0.lan as lan;

res_online_grp = FOREACH ( GROUP online_grp BY feature_type ) GENERATE $0 as feature_type, COUNT($1) as on_cnt;
res_offline_grp = FOREACH ( GROUP offline_grp BY feature_type ) GENERATE $0 as feature_type, COUNT($1) as off_cnt;

jnd_cfb = JOIN offline_grp BY (feature_type,feature_name), online_grp BY (feature_type,feature_name);
res_total = FOREACH (GROUP jnd_cfb BY offline_grp::feature_type) GENERATE $0 as feature_type, COUNT($1) as ttl_cnt;

res_jnd = JOIN res_online_grp BY feature_type, res_offline_grp BY feature_type, res_total BY feature_type;
res_cnt = FOREACH res_jnd GENERATE res_online_grp::feature_type as feature_type,  on_cnt, off_cnt, ttl_cnt, (double)ttl_cnt/(on_cnt < off_cnt?on_cnt:off_cnt) as rate_join;
res_cnt_order = ORDER res_cnt by rate_join;

cmp = FOREACH jnd_cfb GENERATE offline_grp::feature_type as feature_type , offline_grp::feature_name as feature_name, (ABS(offline_grp::score - online_grp::score)/(double)(offline_grp::score > online_grp::score ? (offline_grp::score == 0 ? 1: offline_grp::score) : (online_grp::score == 0? 1:online_grp::score)) < 0.1 ? 1 : 0 ) as accu_score;

res_accu = FOREACH (GROUP cmp BY feature_type) GENERATE $0 as feature_type, SUM($1.accu_score) as accu_score, COUNT($1.accu_score) as total, (double)SUM($1.accu_score)/COUNT($1.accu_score) as accu_score_p;
res_accu_order = ORDER res_accu BY accu_score_p;
--int_filtered = FILTER  jnd_cfb BY (int)(offline_grp::score/1) == offline_grp::score and (int)(online_grp::score/1) == online_grp::score;
--cmp_int = FOREACH int_filtered GENERATE ( offline_grp::weight == online_grp::weight ? 1 : 0 ) as accu_weight_int, ( offline_grp::score == online_grp::score ? 1 : 0) as accu_score_int;
--res_accu_int = FOREACH (GROUP cmp_int ALL ) GENERATE SUM($1.accu_weight_int) as accu_weight, SUM($1.accu_score_int) as accu_score, COUNT($1.accu_score_int) as total, (double)SUM($1.accu_weight_int)/COUNT($1.accu_weight_int) as accu_weight_p, (double)SUM($1.accu_score_int)/COUNT($1.accu_weight_int) as accu_score_p;

 
--jnd_city =  Filter jnd_cfb by offline_grp::feature_type == 'D_CATEGORY';
--online_city = FILTER online_grp by feature_type == 'D_CATEGORY';

--jnd_badcase = JOIN online_city by (feature_type,feature_name) LEFT, jnd_city by (offline_grp::feature_type,offline_grp::feature_name);
-- and offline_grp::feature_name != online_grp::feature_name;
res = UNION res_cnt_order,res_accu_order;
STORE res INTO '$OUTPUT';
--STORE jnd_badcase INTO '$OUTPUT_BAD';
