#!/bin/sh

bin_file=`readlink -f $0`
bin_dir=`dirname $bin_file`


export PS4='+${BASH_SOURCE}:${LINENO}:${FUNCNAME[0]}: '
# set -e

export AWS_CONFIG_FILE="/home/mengchong/.aws/config_content_india"
log_dir=./log
data_dir=/data/news_india_fallback_pipeline/data/
hist_data_dir=/data2/dongzhaochen/index1/hist_data/
log_filename=pipeline.log
interval_minutes="10"
check_interval_second=30
load_data_days=4
load_data_seconds=$((load_data_days*24*60*60))

echo "merging previous $load_data_days days"
cur_ts=`date +%s`

#hist_data_file=$hist_data_dir/"`date +%Y%m%d-%H%M`"
hist_data_file=$hist_data_dir/"content.data"
echo "merging data to $hist_data_file ""`date +%Y%m%d-%H%M`"
rm -f $hist_data_file
start_ts=$((cur_ts-load_data_seconds))
interval_seconds=$((interval_minutes*60))
for ((ts=start_ts; ts<=cur_ts; ts+=interval_seconds)); do
    minute=`date -d@$ts +%M`
    hour=`date -d@$ts +%H`
    day=`date -d@$ts +%Y%m%d`
    path_minute=`echo "$minute / $interval_minutes * $interval_minutes" | bc | sed ':a;s/\(..\)/\1/;tb;s/^/0/;ta;:b'`
    local_path="$data_dir/$day/$hour/$path_minute"
    if [[ -e $local_path/_SUCCESS ]]; then
        cat $local_path/*.data >> $hist_data_file
    fi
done
