#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import traceback
import argparse
import logging
import pandas
import json
import urllib2

console_handler = logging.StreamHandler()
console_handler.setFormatter(logging.Formatter('%(asctime)s\t%(levelname)s\t%(message)s'))
logger = logging.getLogger(__name__)
logger.addHandler(console_handler)
logger.setLevel(logging.DEBUG)
global loginfo
loginfo = []

import data_parser
import time
import local_utils
import data_parser
import ranking_utils
import math

def int_value(val):
    try:
        return int(val)
    except:
        return -1
def cal_kw(article_kw,user_kw):
    res = 0
    if not article_kw or not user_kw:
        return 0
    for a_kw in article_kw:
        for u_kw in user_kw:
            if a_kw['name'] == u_kw['name']:
                res += a_kw['L1_weight'] * u_kw['weight']
    return res

def cal_ctgy(article_ctgy,user_ctgy):
    res = 0
    if not article_ctgy or not user_ctgy:
        return 0
    for a_ctgy in article_ctgy:
        for u_ctgy in user_ctgy:
            if a_ctgy['name'] == u_ctgy['name']:
                res += a_ctgy['weight'] * u_ctgy['weight']
    return res


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-c','--cp',action='store',dest='cp',help='cp content path')
    arg_parser.add_argument('-g','--gmp',action='store',dest='gmp',help='gmp content path')
    #arg_parser.add_argument('-o','--output',action='store',dest='output',help='output file path')
    arg_parser.add_argument('-u','--url',action='store',dest='url',help='url of request')
    arg_parser.add_argument('-l','--lan',action='store',dest='lan',help='language:en or hi')
    args = arg_parser.parse_args()

    cp = args.cp
    gmp = args.gmp
    #output = args.output
    url = args.url
    lan = args.lan

    cp_content = data_parser.load_dedup_content(cp,loginfo)
    logger.info("cp_content:" + str(len(cp_content)))
    gmp_data = data_parser.load_gmp(gmp,loginfo,threshold=100.0)
    logger.info("gmp_data:" + str(len(gmp_data)))

    if lan == 'hi':
        config_json = {"content_lan":"hi","content_region":"in"}
    if lan == 'en':
        config_json = {"content_lan":"en","content_region":"in"}
    content_list,schema,gmp_info = data_parser.parse_content_dump(cp_content,config_json,loginfo,gmp=gmp_data);
    logger.info("valid content: "+ str(len(content_list)))

    # content_id_list = []
    # for item in content_list:
    #     content_id_list.append(item[0])

    #response = urllib2.urlopen(url)
    #index = json.loads(response.read())
    #index_id_list = index['filter_docs']

    #content_id_list.sort()
    #index_id_list.sort()

    #cnt_match = 0
    #i,j = 0,0
    # len_index, len_content = len(index_id_list), len(content_id_list)
    # while i < len_index and j <len_content:
    #     if index_id_list[i] > content_id_list[j]:
    #         j += 1
    #     elif index_id_list[i] < content_id_list[j]:
    #         i += 1
    #     else:
    #         j += 1
    #         i += 1
    #         cnt_match +=1

    # match_rate_on_index = float(cnt_match) / len_index
    # match_rate_on_content = float(cnt_match) / len_content
    # logger.info(str(lan)+" len_content , len_index: {}, {}".format(len_content,len_index))
    # logger.info(str(lan)+" cnt_match:{}".format(cnt_match))
    # logger.info(str(lan)+" match_rate_on_content , match_rate_on_index: {}, {}".format(match_rate_on_content,match_rate_on_index))


    ##find user profile that has keywords
    up_sample = []
    cnt_up = 1
    total_up = 5
    #lan_u = ''
    lan_u = lan
    with open('up.2016040300000') as f:
        for line in f:
            up = json.loads(line)
            if lan_u == 'hi':
                if up['keywords_hindi']:
                    up_sample.append(up)
                    cnt_up += 1
            elif lan_u == 'en':
                if up['keywords']:
                    up_sample.append(up)
                    cnt_up += 1
            else:
                up_sample.append(up)
                cnt_up += 1

            if cnt_up > total_up:
                break

    logger.info('get up {}'.format(len(up_sample)))
    channel_name = 'politics'
    url = url + '&ncid=1'
    if lan == 'en':
        url = url + '&lan=en_IN&app_lan=en_IN'
    if lan == 'hi':
        url = url + '&lan=hi_IN&app_lan=hi_IN'
    cnt_news = 1000
    url = url +'&count='+str(cnt_news)

    #channel_1 = politics<-channel_name
    #channel_2 = society<-channel_name
    #channel_3 = entertainment<-channel_name
    timestamp = int(time.time())
    if lan == 'en':
        age_factor = -0.06
        w1 = 1.0
        w2 = 0.5
        w3 = 0.15
    elif lan == 'hi':
        age_factor = -0.03
        w1 = 1.0
        w2 = 2.0
        w3 = 0.2

    for user in up_sample:
        channel_article_list = []
        if lan == 'hi':
            user_kw = user['keywords_hindi']
            user_ctgy = user['categories_hindi']
        if lan == 'en':
            user_kw = user['keywords']
            user_ctgy = user['categories']
        for article in content_list:
            if channel_name != '':
                if channel_name not in article[schema['channel_name']]:
                    continue
            score_kw = cal_kw(article[schema['entities']],user_kw)
            score_ctgy = cal_ctgy(article[schema['category']],user_ctgy)
            score_gmp = article[schema['pop_score']]
            score_phase1 = w1 * score_gmp + w2 * score_kw + w3 * score_ctgy

            #get age_decay
            publish_time = article[schema['publish_time']]
            age_hour = float(timestamp - int(publish_time))/3600.0
            age_hour = 0 if age_hour < 0 else age_hour
            score = score_phase1 * math.exp(age_factor*age_hour)
            debug_score = (age_hour,math.exp(age_factor*age_hour),score_gmp,score_ctgy,score_kw )
            channel_article_list.append([article[schema['item_id']],score_phase1,score,debug_score])



        url = url + '&uid='+str(user['uid'])
        response = urllib2.urlopen(url)
        index = json.loads(response.read())
        newsid = index['newsid']
        index_list = [ news['id'] for news in newsid ]
        phase1_features_list = [ news['fields']['phase1_features'] for news in newsid ]
        index_gmp_score_list = [ json.loads(any)['rank_score'] for any in phase1_features_list ]
        index_score_list = [news['score'] for news in newsid]
        index_debug_score_list = [(json.loads(any)['age_hour'],json.loads(any)['age_decay'],json.loads(any)['gmp'],json.loads(any)['cat_rel'],json.loads(any)['kw_rel']) for any in phase1_features_list ]

        offline_list = sorted(channel_article_list,key=lambda x:x[2],reverse=True)
        offline_list = offline_list[0:cnt_news]
        offline_index_list = [item[0] for item in offline_list]
        offline_gmp_score_list = [item[1] for item in offline_list]
        offline_score_list = [item[2] for item in offline_list]
        offline_debug_score_list = [item[3] for item in offline_list]
        #logger.info("offline_list id:{} gmp score :{} score:{}".format(offline_index_list,offline_gmp_score_list,offline_score_list))
        #logger.info('debug score age_hour,math.exp(age_factor*age_hour),score_gmp,score_ctgy,score_kw {}'.format(offline_debug_score_list))
        #logger.info("index_list id:{},  index gmp score: {}, score:{} ".format(index_list,index_gmp_score_list,index_score_list))
        #logger.info("idebug score {} ".format(index_debug_score_list))

        #logger.info('len_index_list, len_offline_list: {} ,{}'.format(len(index_list),len(offline_list)))
        cnt_match = 0
        cnt_match_gmp_score = 0
        cnt_match_score = 0

        #logger.info('len of index_list {}, len of offline_index_list {}'.format(len(index_list),len(offline_index_list)))
        for i in xrange(len(index_list)):
            for j in xrange(len(offline_index_list)):
                if index_list[i] == offline_index_list[j]:
                    cnt_match += 1
                    if abs(index_gmp_score_list[i] - offline_gmp_score_list[j])< 0.01:
                        cnt_match_gmp_score += 1
                    if abs(index_score_list[i] - offline_score_list[j]) < 0.01:
                        cnt_match_score += 1




        rank_match_rate = float(cnt_match) / cnt_news
        phase1_match_rate = float(cnt_match_gmp_score) / cnt_news
        phase1_age_decay_rate = float(cnt_match_score) /cnt_news
        #match_rate_list.append(match_rate)


        logger.info('for user id {}, total is {}, cnt_match is {},cnt_phase1_match is {}, cnt_phase1_age_decay_match is {}'.format(user['uid'],cnt_news, cnt_match, cnt_match_gmp_score,cnt_match_score))
        logger.info('rank match rate is {}, phase1 score match rate is {}, phase1 age decay match rate is {}'.format(rank_match_rate,phase1_match_rate,phase1_age_decay_rate))
        logger.info('---------------')
    #mean_match_rate = sum(match_rate_list)/len(match_rate_list)
    #logger.info('mean of match rate is {}'.format(mean_match_rate))
