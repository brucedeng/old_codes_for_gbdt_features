export PS4='+${BASH_SOURCE}:${LINENO}:${FUNCNAME[0]}: '
gmp_file=/data/hindi_news_india_fallback_pipeline/gmp
cur_day=`date -d "10 minutes ago" +%Y%m%d`
cur_hour=`date -d "10 minutes ago" +%H`
cur_min=`date -d "10 minutes ago" +%M`
file_check_times=20
local_path=/data2/dongzhaochen/index1/hist_data/
for((i=0;i<$file_check_times;i++))
do
    cur_minute=`echo "$cur_min / 10 * 10" | bc | sed ':a;s/\(..\)/\1/;tb;s/^/0/;ta;:b'`
    if [ -e "$gmp_file/$cur_day/$cur_hour/$cur_minute".data ]
    then
        echo "download the instant gmp""`date +%Y%m%d-%H%M`"
        cat "$gmp_file/$cur_day/$cur_hour/$cur_minute"".data" >> ${local_path}"/gmp.data"
        break
    else
        echo "gmp.data not ready, wait 30 secs, check ""$i"" times in total""$file_check_times"
        sleep 30
    fi

done
