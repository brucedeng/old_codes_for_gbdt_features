import sys,math
reload(sys)
sys.setdefaultencoding('utf-8')
import copy

def diversify(content,schema,wnd_size=4,candidate_wnd=20):
    sorted_content = copy.deepcopy(content)

    publisher_index = schema['publisher']
    group_id_index= schema['group_id']
    category_index = schema['category']
    score_index = schema['rank_score']

    result = []
    while len(sorted_content) > 0:
        #print result[:10]

        if len(result) < wnd_size:
            context = result
        elif wnd_size >0:
            context = result[-wnd_size:]
        else:
            context = []

        wnd_set = set()
        for item in context:
            wnd_set.add('p-' + str(item[publisher_index]))
            wnd_set.update(set(item[category_index]))
            if int(item[group_id_index]) > 0:
                wnd_set.add('g-' + str(item[group_id_index]))

        max_score = 0
        max_item_index = -1
        max_penalty = 1

        for i in range(min(candidate_wnd, len(sorted_content))):
            item = sorted_content[i]
            diversity_factor = 1.0
            if ('p-' + item[publisher_index]) in wnd_set:
                diversity_factor *= 0.5
            if ('g-' + item[group_id_index]) in wnd_set:
                diversity_factor *= 0.1
            if len(set(item[category_index]) & wnd_set) > 0:
                diversity_factor *= 0.8

            score = float(item[score_index]) * float(diversity_factor)
            #print wnd_set, diversity_factor, item

            if score > max_score:
                max_score = score
                max_penalty = diversity_factor
                max_item_index = i

        if max_item_index >= 0:
            item = sorted_content.pop(max_item_index)
            item.append((max_score,max_penalty))
            #print item
            #sys.stdin.readline()
            result.append(item)

    schema['diversity_info'] = score_index+1
    return result,schema


def channel_filter(content_list, schema, timestamp, channel_id, channel_conf, max_age_days=7, max_hot_days=1):
    ts_index = schema['publish_time']
    id_index = schema['item_id']
    pop_index = schema['pop_score']
    feature_index = schema['features']


    channel_list = []
    expired_list = []
    pop_list = []

    for c in content_list:
        #adhoc!
        #if c[feature_index]['source_type'] == '0' and str(channel_id) !='29':
            #continue

        if eval_conf(c, schema, channel_conf):
            if int(timestamp) - int(c[ts_index]) <= 86400*max_age_days:
                channel_list.append(copy.copy(c))
            else:
                expired_list.append(copy.copy(c))

            if int(timestamp) - int(c[ts_index]) <= 86400*max_hot_days and int(c[feature_index]['newsy_score']) >= 0.7:
                pop_list.append((c[id_index], float(c[pop_index])))

    hot_list = sorted(pop_list, key=lambda x:x[1], reverse=True)[0:3]
    hot_news_id = [t[0] for t in hot_list]

    return channel_list, expired_list, hot_news_id

def eval_conf(content, schema, channel_conf):
    if len(channel_conf) == 0:
        return True

    for cond in channel_conf:
        if cond.find('<-') > -1:
            value,key = cond.split('<-')
            index = schema[key]
            value_set = set(value.split('|'))
            if len(value_set & set(content[index]))==0:
                return False

        elif cond.find('==') > -1:
            value,key = cond.split('==')
            if key.startswith('features.'):
                key_list = key.split('.',1)
                # print content, schema
                if value != str(content[schema['features']].get(key_list[1])):
                    return False
            else:
                if value != str(content[schema[key]]):
                    return False

    return True

def get_channel_by_conf(content_list,schema,channel,timestamp,config_json,loginfo,rank_method='linear',explore_budget=-1):
    #pro_publisher = ['ndtv','toi','the economic times','india times','indiatimes','the hindu','india.com','cricbuzz','hindustantimes','the indian express','one india','sify','firstpost.com','engadget.com','digit.in','indiatoday']
    pro_publisher = []

    channel_list = []
    expired_list = []

    channel_id = channel[0]
    channel_conf = channel[1]

    channel_list, expired_list, hot_news_id = channel_filter(content_list,schema,timestamp,channel_id,channel_conf,max_age_days=4, max_hot_days=1)
    #print '\n'.join(str(c) for c in channel_list[:3])

    id_index = schema['item_id']
    feature_index = schema['features']

    if int(timestamp / 600) % 6 == 0:
        update_hot_flag = True
    else:
        update_hot_flag = False

    for item in channel_list:
        if rank_method == 'linear':
            age_hour, rank_score, pro_factor, groupid_factor = cal_rank_score_linear(item, schema, timestamp, pro_publisher)
            #item[feature_index]['groupid_factor'] = str(groupid_factor)
            #item[feature_index]['pro_factor'] = str(pro_factor)
        else:
            age_hour, rank_score = cal_rank_score_str(item, schema, timestamp, pro_publisher)

        item.append(age_hour)
        item.append(rank_score)
        if update_hot_flag and item[id_index] in hot_news_id:
            item[feature_index]['hot_news'] = '2'
        else:
            item[feature_index]['hot_news'] = '0'


    schema['doc_age'] = 10
    schema['rank_score'] = 11
    output_list = sorted(channel_list, key = lambda x:x[-1], reverse=True)

    if explore_budget > 0:
        output_list_adjusted = explore_throtlling(output_list, schema, budget=explore_budget,wnd=100);
    else:
        output_list_adjusted = output_list


    loginfo.append('[RANKING] Channel %s: %d valid docs, %d expired docs.' % (str(channel_id), len(output_list), len(expired_list)))
    loginfo.append('[RANKING] Update hot news: %s' % str(update_hot_flag))
    if update_hot_flag:
        loginfo.append('[RANKING] Channel %s hot news: %s' % (str(channel_id) , ','.join(hot_news_id)))

    return (output_list_adjusted,expired_list,schema)

def explore_throtlling(content_list, schema, budget, wnd):
    feature_index = schema['features']
    score_index = schema['rank_score']
    explore_list = []
    ranking_list = []

    for item in content_list:
        if item[feature_index]['explore'] == 'True':
            explore_list.append(item)
        else:
            ranking_list.append(item)

    output_list = []
    explore_cnt = 0
    #megre sort ranking list and explore list, control explore list by budget
    while len(ranking_list) > 0 and len(explore_list) > 0:
        if ranking_list[0][score_index] >= explore_list[0][score_index]:
            item = ranking_list.pop(0)
            output_list.append(item)
        else:
            if explore_cnt < budget:
                item = explore_list.pop(0)
                output_list.append(item)
                explore_cnt += 1
            else:
                item = ranking_list.pop(0)
                output_list.append(item)

        if len(output_list) % wnd == 0:
            explore_cnt = 0

    if len(ranking_list) == 0:
        output_list += explore_list
    elif len(explore_list) == 0:
        output_list += ranking_list

    return output_list


def cal_rank_score_str(item,schema,timestamp,pro_publisher):
    ts_index = schema['publish_time']
    publisher_index = schema['publisher']
    pop_index = schema['pop_score']
    feature_index = schema['features']

    age_hour = float(int(timestamp) - int(item[ts_index]))/3600.0
    raw_score = item[pop_index]*1000

    #!boost head publisher
    publisher = item[publisher_index].lower()
    if publisher in pro_publisher:
        pro_factor = 1.5
    else:
        pro_factor = 1.0

    #!age decay
    score = pro_factor * raw_score #* math.exp(-0.2*age_hour)

    #ad-hoc top id
    #if str(item[0]) == '4551993':
        #score *= 100

    if score < 1:
        pop_score = '1'
    elif int(score) >= 100000:
        pop_score = '99999'
    else:
        pop_score = "%05d" % int(score)

    if int(item[feature_index]['images_cnt']) >= 100:
        image_cnt = "99"
    else:
        image_cnt = "%02d" % int(item[feature_index]['images_cnt'])

    if int(item[feature_index]['keywords_count']) >= 100:
        keywords_cnt = "99"
    else:
        keywords_cnt = "%02d" % int(item[feature_index]['keywords_count'])

    if int(item[feature_index]['word_count']) >= 10000:
        word_cnt = "9999"
    else:
        word_cnt = "%04d" % int(item[feature_index]['word_count'])

    score = int(pop_score+image_cnt+keywords_cnt+word_cnt)

    return age_hour, score


def cal_rank_score_linear(item,schema,timestamp, pro_publisher):
    ts_index = schema['publish_time']
    publisher_index = schema['publisher']
    pop_index = schema['pop_score']
    feature_index = schema['features']

    images_cnt = float(item[feature_index]['images_cnt'])
    gmp = float(item[pop_index])
    head_image = float(item[feature_index]['head_image'])


    groupid_factor = 1

    f = (gmp, images_cnt, head_image)
    c = (0.2900, 0.0141, 0.0013)
    linear_score = sum(t[0]*t[1] for t in zip(f,c))*1000*groupid_factor

    age_hour = float(int(timestamp) - int(item[ts_index]))/3600.0
    age_hour = 0 if age_hour < 0 else age_hour

    #!boost head publisher
    publisher = item[publisher_index].lower()
    if publisher in pro_publisher:
        pro_factor = 1.5
    else:
        pro_factor = 1.0

    #!age decay
    score = pro_factor * linear_score * math.exp(-0.03*age_hour)


    return age_hour, score,pro_factor, groupid_factor
