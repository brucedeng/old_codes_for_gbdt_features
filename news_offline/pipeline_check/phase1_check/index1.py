#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import traceback
import argparse
import logging
import pandas
import json
import urllib2
console_handler = logging.StreamHandler()
console_handler.setFormatter(logging.Formatter('%(asctime)s\t%(levelname)s\t%(message)s'))
logger = logging.getLogger(__name__)
logger.addHandler(console_handler)
logger.setLevel(logging.DEBUG)
global loginfo
loginfo = []

import data_parser
import time
import local_utils
import data_parser
import ranking_utils

def int_value(val):
    try:
        return int(val)
    except:
        return -1

if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-c','--cp',action='store',dest='cp',help='cp content path')
    arg_parser.add_argument('-g','--gmp',action='store',dest='gmp',help='gmp content path')
    arg_parser.add_argument('-o','--output',action='store',dest='output',help='output file path')
    arg_parser.add_argument('-u','--url',action='store',dest='url',help='url of request')
    args = arg_parser.parse_args()

    cp = args.cp
    gmp = args.gmp
    output = args.output
    url = args.url

    cp_content = data_parser.load_dedup_content(cp,loginfo)
    logger.info("cp_content:" + str(len(cp_content)))
    gmp_data = data_parser.load_gmp(gmp,loginfo,threshold=20.0)
    logger.info("gmp_data:" + str(len(gmp_data)))

    config_json = {"content_lan":"hi","content_region":"in"}
    content_list,schema,gmp_info = data_parser.parse_content_dump(cp_content,config_json,loginfo,gmp=gmp_data);
    logger.info("valid content: "+ str(len(content_list)))

    content_id_list = []
    for item in content_list:
        content_id_list.append(item[0])

    response = urllib2.urlopen(url)
    index = json.loads(response.read())
    index_id_list = index['filter_docs']

    content_id_list.sort()
    index_id_list.sort()

    cnt_match = 0
    i,j = 0,0
    len_index, len_content = len(index_id_list), len(content_id_list)
    while i < len_index and j <len_content:
        if index_id_list[i] > content_id_list[j]:
            j += 1
        elif index_id_list[i] < content_id_list[j]:
            i += 1
        else:
            j += 1
            i += 1
            cnt_match +=1

    match_rate_on_index = float(cnt_match) / len_index
    match_rate_on_content = float(cnt_match) / len_content
    logger.info("len_content , len_index: {}, {}".format(len_content,len_index))
    logger.info("cnt_match:{}".format(cnt_match))
    logger.info("match_rate_on_content , match_rate_on_index: {}, {}".format(match_rate_on_content,match_rate_on_index))
