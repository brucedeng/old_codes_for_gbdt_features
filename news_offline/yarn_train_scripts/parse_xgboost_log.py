#!/usr/bin/env python
#-*-encoding:utf-8-*-

import sys
import re
import StringIO
import json
#from optparse import OptionParser
import argparse
import os
from distutils.dir_util import mkpath

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process xgboost log and find the best number of trees.')
    parser.add_argument("-t", "--threashold",
                        dest="test_threashold", default='0.001', type=float,
                        help="threashold to choose a good iteration size. if the test auc increase is smaller than the threashold,choose the previous tree as gbdt tree number")
    parser.add_argument("-s", "--step_size",
                        dest="step_size", default='10',type=int,
                        help="""step size of finding best tree number using test threashold""")
    parser.add_argument("-f", "--formatted",
                        action="store_false", dest="formatted", default=True,
                        help="print tsv format result with title and indicator.")
    parser.add_argument("-n","--label_name", dest="label_name", default="train:train,test:test",
            help="name for train auc title. format like 'train:train,test:test,otehr:click_install'")
    parser.add_argument("input_file",
                        default='-',
                        help="the input xgboost log file. default will read from stdin.")
    args = parser.parse_args()
    threashold = float(args.test_threashold)
    step_size = int(args.step_size)
    label_names=args.label_name.split(',')
    train_name='train'
    test_name='test'
    data_name=[]
    for item in label_names:
        k,v=item.split(':')
        if k == 'train':
            train_name=v
        elif k=='test':
            test_name=v
        elif k=='other':
            data_name=v
            data_name = data_name.split('.')

    input_file = args.input_file
    if input_file=='-':
        in_strm = sys.stdin
    else:
        in_strm = open(input_file)

    # match "[70]\ttest-auc:0.888071\ttrain-auc:0.918976"
    log_reg = re.compile("\\[([0-9]+)\\].*\\s+([a-zA-Z_]+-auc:([0-9.]+))")
    last_tree = None
    last_test = None
    last_train = None
    last_data = None
    cnt=0
    for line in in_strm:
        matched = log_reg.search(line)
        # print matched
        sys.stderr.write(str(cnt) + '\r')
        cnt +=1
        if matched:
            match_map={}
            rnd   = int(matched.group(1))
            all_data = matched.group(0)
            items=all_data.split('\t')
            for idx in range(1,len(items)):
                k,v = items[idx].split(':')
                k = k.replace('-auc','')
                match_map[k]=float(v)
            train_flag=True
            test=match_map.get(test_name)
            train=match_map.get(train_name)
            data = []
            for i in data_name:
                data.append(match_map.get(i))

            if last_tree is None:
                last_tree = rnd
                last_test = test
                last_train = train
                last_data = data
                last_line = all_data
                continue
            if rnd % step_size !=0:
                continue
            if test - last_test < threashold:
                if rnd < 40:
                    continue
                break
            last_tree = rnd
            last_test = test
            last_train = train
            last_data = data
            last_line = all_data

    if last_tree is None:
        sys.stderr.write('error parsing log file. did not find any test-auc or train-auc information\n')
        sys.exit(1)

    total_rnd = last_tree
    for line in in_strm:
        matched = log_reg.match(line)
        if matched:
            total_rnd   = int(matched.group(1))

    over_train_gap = total_rnd - last_tree

    if args.formatted:
        print "over trained:\t%d\tbest:\t"%over_train_gap + last_line #'[' + str(rnd) + ']\ttest-auc:' + str(test) + '\ttrain-auc:' + str(train)
    else:
        print str(last_tree) + '\t' +  str(last_test) + '\t' + str(last_train) + '\t' + '\t'.join([str(_k) for _k in last_data])


