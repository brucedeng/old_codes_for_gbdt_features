#!/usr/bin/env python

import sys
import re
from collections import deque

class treeNode:
	def __init__(self,leaf=False,yes=-1,no=-1,missing=-1,value='0.0',condition='',idx=-1):
		self.leaf=leaf
		self.yes=yes
		self.no=no
		self.missing=missing
		self.condition=condition
		self.idx=idx
		self.value=value
		pass

boost_reg = re.compile('booster\\[([0-9]+)\\]:')
node_reg = re.compile('\\s*([0-9]+):\\[([^<]+<[-0-9.e]+)\\] yes=([0-9]+),no=([0-9]+),missing=([0-9]+).*')
leaf_reg = re.compile('\\s*([0-9]+):leaf=([-0-9.e]+).*')
def load_model(filename):
	f = open(filename)
	booster_list=[]
	cur_tree = []
	for line in f:
		boost_match = boost_reg.match(line)
		if boost_match:
			cur_tree = []
			booster_list.append(cur_tree)
			continue
		node_match = node_reg.match(line)
		if node_match:
			details = node_match.groups()
			node_id = int(details[0])
			condition = details[1]
			left = int(details[2])
			right = int(details[3])
			missing = int(details[4])
			lack = node_id - len(cur_tree) + 1
			if lack > 0:
				cur_tree += [None] * lack
			cur_tree[node_id] = treeNode(False, yes=left, no=right, missing=missing,condition=condition,idx=node_id)
			continue

		leaf_match = leaf_reg.match(line)
		if leaf_match:
			details = leaf_match.groups()
			node_id = int(details[0])
			value = details[1]
			lack = node_id - len(cur_tree) + 1
			if lack > 0:
				cur_tree += [None] * lack
			cur_tree[node_id] = treeNode(True, value=value, idx=node_id)
			continue
		print line
		sys.exit()
		print 'error'
	for idx in xrange(len(booster_list)):
		tree_list = booster_list[idx]
		tree_node = build_tree(tree_list,0)
		booster_list[idx] = tree_node
	return booster_list

def build_tree(node_list,idx):
	root = node_list[idx]
	if root.leaf:
		return root
	yes_idx = root.yes
	no_idx = root.no
	missing_idx = root.missing
	root.yes = build_tree(node_list,yes_idx)
	root.no = build_tree(node_list,no_idx)
	if missing_idx == yes_idx:
		root.missing = 0
	else:
		root.missing = 1
	return root

def merge_models(m1,m2,split):
	length = max(len(m1),len(m2))
	result = []
	for i in xrange(length):
		if i >= len(m1):
			t1 = treeNode(True,'0.0')
		else:
			t1 = m1[i]
		if i >= len(m2):
			t2 = treeNode(True,'0.0')
		else:
			t2 = m2[i]
		merged_tree = treeNode(False, yes=t1, no=t2, missing=0,condition=split,idx=0)
		queue = deque([merged_tree])
		node_idx = 0
		while len(queue) > 0:
			node = queue[0]
			node.idx = node_idx
			node_idx += 1
			if not node.leaf:
				queue.append(node.yes)
				queue.append(node.no)
			queue.popleft()
		result.append(merged_tree)
	return result

def dump_tree(tree,prefix):
	if tree.leaf:
		print "%s%d:leaf=%s" % (prefix,tree.idx,tree.value)
	else:
		if tree.missing==0:
			missing = tree.yes
		else:
			missing = tree.no
		print "%s%d:[%s] yes=%d,no=%d,missing=%d" % (prefix,tree.idx,tree.condition, tree.yes.idx, tree.no.idx, missing.idx)
		prefix = prefix + '\t'
		dump_tree(tree.yes,prefix)
		dump_tree(tree.no,prefix)

def dump_model(model):
	boosteridx=0
	for idx in xrange(len(model)):
		print 'booster[%d]:' % idx
		dump_tree(model[idx],'')

if __name__=='__main__':

	model1 = load_model(sys.argv[1])
	model2 = load_model(sys.argv[2])
	split = sys.argv[3]
	result = merge_models(model1,model2,split)
	dump_model(result)
