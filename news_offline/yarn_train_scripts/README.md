# 分布式xgboost的用法

本目录包含使用分布式xgboost训练模型的方法. 分布式xgboost可以在美西2 cdh集群 10.2.2.238 的 /data1/mengchong/xgboost/demo/distributed-training 找到.

## 环境配置

分布式xgboost需要一些环境量:

```
export HADOOP_HOME="/usr/lib/hadoop"
```

设置此环境变量后, 可以运行原始的dmlc脚本提交yarn job, 但是如果使用本目录下的start_train.sh 来启动任务的话, 还需要设置环境变量指定xgboost和dmlc的目录

```
export XGBOOST_ROOT=/data1/mengchong/xgboost
```

## 使用方法

start_train.sh 使用的一个例子:

```
./start_train.sh -n 20160330-reweight -d hdfs://mycluster/projects/news/model/training/run_training_data/hi_cfb_offline_formatted_data/split_events_20160321-20160327-reweight_libsvm/train -t hdfs://mycluster/projects/news/model/training/run_training_data/hi_cfb_offline_formatted_data/split_events_20160321-20160327-reweight_libsvm/test -c global.conf -m featmap.txt
```
其中, -n 参数指定train模型的名字, 脚本会在hdfs上以该名字存储训练好的模型文件. -o 参数指定模型dump文件和log文件的输出路径. -d 指定训练文件, 训练文件是一个hdfs目录,目录名字必须是以hdfs://开头的, 否则xgboost将无法识别. 数据必须存储成libsvm格式, 格式是: 
```
label:weight id1:value1 id2:value2 ..
```

-t 指定测试文件的路径,格式与训练数据相同. -c 是xgboost的配置文件. -m 是feature map文件, 存储feature id和feature name的pair.

具体参数设置可以参考 -h

```
./start_train.sh -h
Usage: ./start_train.sh -n|--name [application_name] -c|--config [config_file] -m|--featmap [featmap_file]
Optional args: -s|--silent -r|--run_train [run_train_flag:true] -q|--queue [queue_name:experiment] -d|--data [train_data] -t|--test [test_data] -o|--output [output_dir:application_name] --min-iter [minimum_iteration:0] --dump-threshold [test_threshold:0.0005] --num-workers [num_workers:50] --worker-cores [worker_cores:4] --worker-memory [worker-memory:2g]
```

## 合并多个model的方法

如果需要分开重度用户和轻度用户, 分别训练然后合并model, 需要以下步骤

1. 使用不同的数据训练重度用户和轻度用户
2. 合并model

合并方法:

```
python merge_two_models.py left_branch_model.dump right_branch_model.dump <split_condition> > merged_model.dump
```

例子:

```
python merge_two_models.py left_branch_model.dump right_branch_model.dump 'U_KW_LEN<200' > merged_model.dump
```