#!/bin/bash

bin_path=$(readlink -f $0)
bin_path=$(dirname $bin_path)

set -e

function printhelp {
    echo "Usage: $0 -n|--name [application_name] -c|--config [config_file] -m|--featmap [featmap_file]"
    echo "Optional args: -s|--silent -r|--run_train [run_train_flag:true] -q|--queue [queue_name:experiment] -d|--data [train_data] -t|--test [test_data] -o|--output [output_dir:application_name] --min-iter [minimum_iteration:0] --dump-threshold [test_threshold:0.0005] --num-workers [num_workers:50] --worker-cores [worker_cores:4] --worker-memory [worker-memory:2g]"
}

config=global.conf
only_dump='false'
minimum_iteration=0
test_threshold=0.0005
num_workers=50
worker_cores=4
worker_memory=2g
run_train=true
queue=experiment
silent_mode=false

while [[ $# -gt 0 ]]
do
key="$1"
xgboost_configs=""
# echo $key

case $key in
    -h|--help)
    printhelp; exit ;;
# necessary configs
    -n|--name)
    run_name="$2"; shift ;;
    -c|--config)
    config_file="$2"; shift ;;
    -m|--featmap)
    featmap="$2"; shift ;;
# optional configs
    -s|--silent)
    silent_mode=true ;;
    -r|--run_train)
    run_train="$2"; shift ;;
    -q|--queue) # default experiment
    queue="$2"; shift ;;
    -d|--data) # default do not define
    train_data="$2"; shift ;;
    -t|--test) # default do not define
    test_data="$2"; shift ;;
    -o|--output) # default the same with run_name
    output="$2"; shift ;;
    --min-iter) # default 0
    minimum_iteration="$2"; shift ;;
    --dump-threshold) # default 0.0005
    test_threshold="$2"; shift ;;
    --num-workers) # default 100
    num_workers="$2"; shift ;;
    --worker-cores) # default 4
    worker_cores="$2"; shift ;;
    --worker-memory) # default 4
    worker_memory="$2"; shift ;;
    *)
    xgboost_configs="$xgboost_configs $key"
    ;;
    # break ;;
esac
shift
done

xgboost_path="$XGBOOST_ROOT"

function run_and_print {
    cmd="$1"
    output_log="$2"
    echo "$cmd" >> $output_log
    $cmd
}

if [[ "a$output" == "a" ]]; then
    output=$run_name
fi

mkdir -p $output

featmap_hdfs=hdfs://mycluster/projects/news/model/training/featmaps/$run_name.txt
featmap_hdfs_path=`dirname $featmap_hdfs`
hadoop fs -mkdir -p $featmap_hdfs_path
hadoop fs -copyFromLocal -f $featmap $featmap_hdfs
cp $featmap $output/featmap.txt
cp $config_file $output/global.conf
cd $output

log_file=train.log
command_log=commands.log
model_dir=hdfs://mycluster/projects/news/model/training/models/$run_name
hadoop fs -mkdir -p $model_dir
hadoop fs -chmod 777 $model_dir

cmd="$xgboost_path/dmlc-core/tracker/dmlc-submit --cluster=yarn --num-workers=$num_workers --jobname=$run_name --worker-cores=$worker_cores --worker-memory=$worker_memory --queue=$queue $xgboost_path/xgboost global.conf fmap=$featmap_hdfs model_dir=$model_dir $xgboost_configs"
if [[ a"$train_data" != "a" ]]; then
    cmd="$cmd data='$train_data'"
fi
if [[ a"$test_data" != "a" ]]; then
    cmd="$cmd eval[test]='$test_data'"
fi

if [[ $run_train == "true" ]]; then
    if [[ $silent_mode == "true" ]]; then
        echo "$cmd" >> $command_log;
        $cmd > $log_file 2>&1 
    else
        echo "$cmd" | tee -a $command_log;
        $cmd 2>&1 | tee $log_file #|| echo "finished training" &
    fi
fi
#wait
# xgboost_pid=$!
# monitor_xgboost_process $xgboost_pid $run_name.train.log $bin_path/parse_xgboost_log.py

echo "searching best iteration using threshold $test_threshold"
best_data=`python $bin_path/parse_xgboost_log.py -t $test_threshold -s 10 -n train:train,test:test -f $log_file`
num_tree=`echo "$best_data" | cut -f1`
if [[ num_tree -lt $minimum_iteration ]]; then
    num_tree=$minimum_iteration;
fi
echo "using num trees $num_tree"
model_name=`echo $num_tree|sed ':a;s/\(....\)/\1/;tb;s/^/0/;ta;:b' `

# mkdir -p models
# if [[ -e models/$run_name.$model_name.model ]]; then
#     rm -f models/$run_name.$model_name.model;
# fi
model_file=$model_name.model
if [[ -e $model_file ]]; then rm -f $model_file; fi
run_and_print "hadoop fs -copyToLocal $model_dir/$model_name.model $model_file" $command_log
run_and_print "$xgboost_path/xgboost global.conf task=dump fmap=featmap.txt name_dump=model.dump dump_stats=1 model_in=$model_file" $command_log
run_and_print "$xgboost_path/xgboost global.conf task=dump fmap=featmap.txt name_dump=model.online.dump model_in=$model_file" $command_log
echo "python $bin_path/featureImp.py model.dump > model.imp" | tee -a $command_log
python $bin_path/featureImp.py model.dump > model.imp

# rm -f $model_file
