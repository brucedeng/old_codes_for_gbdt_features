
import sys
import math
if __name__=='__main__':
	result = []
	neg_cnt = 0.0
	pos_cnt = 0.0
	for line in sys.stdin:
		line = line.rstrip('\n')
		fields = line.split('\t')
		
		# print line
		try:
			label = int(fields[0])
			dwell = float(fields[1])
		except:
			label = 0
			dwell = 0
		if label == 0:
			result.append(-1)
			neg_cnt += 1
		else:
			wt = math.log(dwell + 1)
			result.append(wt)
			pos_cnt += wt

	ratio = neg_cnt/pos_cnt

	for item in result:
		if item == -1:
			print 1
		else:
			print item * ratio

