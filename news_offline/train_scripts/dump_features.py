#!/usr/bin/env python
import numpy as np
# import xgboost as xgb
import sys
from sklearn.datasets import load_svmlight_file

data_file_name = sys.argv[1]
fields = sys.argv[2]

fields_list = []
for item in fields.split(','):
	try:
		field_idx = int(item)
		fields_list.append(field_idx-1)
	except:
		sys.out.write("can not parse data %s\n" % item)
		pass

data_matrix = load_svmlight_file(data_file_name)

x = data_matrix[0]
y = data_matrix[1]

for i in range(x.shape[0]):
	label = y[i]
	result = [y[i]]
	for idx in fields_list:
		result.append(x[i,idx])
	print '\t'.join([str(_k) for _k in result])
