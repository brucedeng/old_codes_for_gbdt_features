#!/bin/bash

bin_path=$(readlink -f $0)
bin_path=$(dirname $bin_path)

set -e

function printhelp {
    echo Usage $0 -p train,weight,stat,split_train,split_test -m '<featmap>' '<training_file>' '<testing_file>'
}
declare -A procs

featmap_file=featmap-v3.txt
PROC='all'
bias_ratio=1
train_gp=""

aws_raw_path=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_evaluation/20160109

while [[ $# -gt 0 ]]
do
key="$1"
echo $key

case $key in
    -h|--help)
    printhelp;
    exit
    ;;
    -r|--raw_test)
    raw_test="$2"
    shift
    ;;
    --aws)
    aws_raw_path="$2"
    shift
    ;;
    *)
    echo unknown option $key
    ;;
esac
shift
done

evaluation_data=gmp_evaluation_data.txt
cat $raw_test | cut -f1,2,3,4,5 | awk -F$'\t' 'BEGIN{OFS="\t"}{gmp=gensub(/.* 47:([^ ]+) .*/,"\\1","g",$5); print $1,$2,$3,gmp+0.0}' > $evaluation_data

aws_raw_path=`echo $aws_raw_path | sed 's/\/$//g'`
echo "uploading aws $aws_raw_path/$output"
aws s3 cp $evaluation_data $aws_raw_path/$output
