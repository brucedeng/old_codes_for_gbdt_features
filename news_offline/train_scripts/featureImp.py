import sys
def get_feature_imp(model_file):

    fmodel = open(model_file)

    fmap = {}

    for l in fmodel.readlines():


        l = l.strip()
        a = l.split(':')

        if len(a[1]) == 0:
            continue

        arr = a[1].split('[')
        if len(arr) == 1:
            continue
        fid = arr[1].split(']')[0]
        arr2 = fid.split('<')

        if len(arr2) == 1:
            fid=arr2[0]
            #print fid
        else:
            fid=arr2[0]

        #if fid not in fmap:
        #    fmap[fid] = 1
        #else:
        #    fmap[fid] += 1
        begin = l.find('gain') + 5
        end = l.find('cover') - 1
        gain = float(l[begin:end])

        if fid not in fmap:
            fmap[fid] = gain
        else:
            fmap[fid] += gain


    f_imp = [(k,int(fmap[k])) for k in fmap]

    f_imp = sorted(f_imp,key=lambda x:x[1],reverse=True)

    f_norm = [(x[0],float(x[1]*100) / float(f_imp[0][1])) for x in f_imp]

    return f_norm

if __name__ == '__main__':

    if len(sys.argv) < 2:
        exit(-1);

    model_file = sys.argv[1]

    f = get_feature_imp(model_file)

    for t in f:
        print "%s\t%f" % (t[0],t[1])


