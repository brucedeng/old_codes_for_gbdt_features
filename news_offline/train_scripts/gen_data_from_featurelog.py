#!/usr/bin/env python
# %matplotlib inline
from matplotlib import pyplot as plt
import sys
import argparse
import datetime
import os, urllib2, json

feature_name_map={"U_UID_EC":"f1",
"U_UID_C": "f2",
"U_UID_COEC": "f3",
"U_CITY_EC": "f4",
"U_CITY_C": "f5",
"U_CITY_COEC": "f6",
"U_CATEGORY_MEAN_EC": "f7",
"U_CATEGORY_MEAN_C": "f8",
"U_CATEGORY_MAX_EC": "f9",
"U_CATEGORY_MAX_C": "f10",
"U_CATEGORY_MICRO_MEAN_COEC": "f11",
"U_CATEGORY_MICRO_MAX_COEC": "f12",
"U_CATEGORY_MACRO_MEAN_COEC": "f13",
"U_CATEGORY_MACRO_MAX_COEC": "f14",
"U_KEYWORD_MEAN_EC": "f15",
"U_KEYWORD_MEAN_C": "f16",
"U_KEYWORD_MAX_EC": "f17",
"U_KEYWORD_MAX_C": "f18",
"U_KEYWORD_MICRO_MEAN_COEC": "f19",
"U_KEYWORD_MICRO_MAX_COEC": "f20",
"U_KEYWORD_MACRO_MEAN_COEC": "f21",
"U_KEYWORD_MACRO_MAX_COEC": "f22",
"D_CONID_EC": "f23",
"D_CONID_C": "f24",
"D_CONID_COEC": "f25",
"D_PUBLISHER_EC": "f26",
"D_PUBLISHER_C": "f27",
"D_PUBLISHER_COEC": "f28",
"D_CATEGORY_MEAN_EC": "f29",
"D_CATEGORY_MEAN_C": "f30",
"D_CATEGORY_MAX_EC": "f31",
"D_CATEGORY_MAX_C": "f32",
"D_CATEGORY_MICRO_MEAN_COEC": "f33",
"D_CATEGORY_MICRO_MAX_COEC": "f34",
"D_CATEGORY_MACRO_MEAN_COEC": "f35",
"D_CATEGORY_MACRO_MAX_COEC": "f36",
"C_D_U_CONID_CITY_EC": "f37",
"C_D_U_CONID_CITY_C": "f38",
"C_D_U_CONID_CITY_COEC": "f39",
"C_D_U_CONID_CATEGORY_MEAN_EC": "f40",
"C_D_U_CONID_CATEGORY_MEAN_C": "f41",
"C_D_U_CONID_CATEGORY_MAX_EC": "f42",
"C_D_U_CONID_CATEGORY_MAX_C": "f43",
"C_D_U_CONID_CATEGORY_MICRO_MEAN_COEC": "f44",
"C_D_U_CONID_CATEGORY_MICRO_MAX_COEC": "f45",
"C_D_U_CONID_CATEGORY_MACRO_MEAN_COEC": "f46",
"C_D_U_CONID_CATEGORY_MACRO_MAX_COEC": "f47",
"C_D_U_PUBLISHER_CITY_EC": "f48",
"C_D_U_PUBLISHER_CITY_C": "f49",
"C_D_U_PUBLISHER_CITY_COEC": "f50",
"C_D_U_PUBLISHER_AGE_EC": "f51",
"C_D_U_PUBLISHER_AGE_C": "f52",
"C_D_U_PUBLISHER_AGE_COEC": "f53",
"C_D_U_PUBLISHER_GENDER_EC": "f54",
"C_D_U_PUBLISHER_GENDER_C": "f55",
"C_D_U_PUBLISHER_GENDER_COEC": "f55",
"C_D_U_CATEGORY_CITY_MEAN_EC": "f57",
"C_D_U_CATEGORY_CITY_MEAN_C": "f58",
"C_D_U_CATEGORY_CITY_MAX_EC": "f59",
"C_D_U_CATEGORY_CITY_MAX_C": "f60",
"C_D_U_CATEGORY_CITY_MICRO_MEAN_COEC": "f61",
"C_D_U_CATEGORY_CITY_MICRO_MAX_COEC": "f62",
"C_D_U_CATEGORY_CITY_MACRO_MEAN_COEC": "f63",
"C_D_U_CATEGORY_CITY_MACRO_MAX_COEC": "f64",
"C_D_U_CATEGORY_AGE_MEAN_EC": "f65",
"C_D_U_CATEGORY_AGE_MEAN_C": "f66",
"C_D_U_CATEGORY_AGE_MAX_EC": "f67",
"C_D_U_CATEGORY_AGE_MAX_C": "f68",
"C_D_U_CATEGORY_AGE_MICRO_MEAN_COEC": "f69",
"C_D_U_CATEGORY_AGE_MICRO_MAX_COEC": "f70",
"C_D_U_CATEGORY_AGE_MACRO_MEAN_COEC": "f71",
"C_D_U_CATEGORY_AGE_MACRO_MAX_COEC": "f72",
"C_D_U_CATEGORY_GENDER_MEAN_EC": "f73",
"C_D_U_CATEGORY_GENDER_MEAN_C": "f74",
"C_D_U_CATEGORY_GENDER_MAX_EC": "f75",
"C_D_U_CATEGORY_GENDER_MAX_C": "f76",
"C_D_U_CATEGORY_GENDER_MICRO_MEAN_COEC": "f77",
"C_D_U_CATEGORY_GENDER_MICRO_MAX_COEC": "f78",
"C_D_U_CATEGORY_GENDER_MACRO_MEAN_COEC": "f79",
"C_D_U_CATEGORY_GENDER_MACRO_MAX_COEC": "f80",
"D_KEYWORD_MEAN_EC": "f81",
"D_KEYWORD_MEAN_C": "f82",
"D_KEYWORD_MAX_EC": "f83",
"D_KEYWORD_MAX_C": "f84",
"D_KEYWORD_MICRO_MEAN_COEC": "f85",
"D_KEYWORD_MICRO_MAX_COEC": "f86",
"D_KEYWORD_MACRO_MEAN_COEC": "f87",
"D_KEYWORD_MACRO_MAX_COEC": "f88",
"U_CAT_LEN": "f501",
"U_KW_LEN": "f502",
"U_AGE_1": "f503",
"U_AGE_2": "f504",
"U_GENDER": "f505",
"TIME_OF_DAY": "f506",
"DAY_OF_WEEK": "f507",
"PROD_ID_3": "f508",
"PROD_ID_5": "f509",
"WORD_COUNT": "f510",
"DOC_AGE": "f511",
"IMAGE_COUNT": "f512",
"NEWSY_SCORE": "f513",
"D_CAT_LEN": "f514",
"D_KW_LEN": "f515",
"CAT_REL": "f516",
"KW_REL": "f517"}

feature_name_map_inv = dict([(v,k) for (k,v) in feature_name_map.items()])

def get_feature_lists():
    #load feature map file
    if get_feature_lists._global_feature_map is None:
        feature_map_file = 'feature.map'
        f3 = open(feature_map_file, 'r')
        feature_map = {}
        feature_list = []
        for line in f3:
            line = line.rstrip('\n')
            if line == '':
                continue
            fid, fname, ftype = line.split('\t')
            if fid.startswith('#'):
                fid = fid.replace('#','').strip()
            feature_map[fname] = fid
            feature_list.append(fname)
        get_feature_lists._global_feature_map = feature_map
        get_feature_lists._global_feature_list = feature_list
        f3.close()
    return get_feature_lists._global_feature_list, get_feature_lists._global_feature_map

def avg_list_step(data,step):
    length = len(data)
    length = length / step * step
    i = step
    result = []
    while i <= length:
        result.append(sum(data[i-step:i])/float(step))
        i += step
    return result

if __name__=='__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-m', '--featmap', action='store', dest='featmap', help='featmap file')
    arg_parser.add_argument('-d', '--data', action='store', dest='data', help="data file. one json each line")
    arg_parser.add_argument('-o', '--output', action='store', dest='output', help="output feature file")
    arg_parser.add_argument('-s', '--output_score', action='store', dest='score', help="output score file")

    args = arg_parser.parse_args()
    score_file = args.score
    output_file = args.output
    featmap = args.featmap
    data_file = args.data

    if score_file is None:
        score_file = output_file + '.score'

    f3 = open(args.featmap, 'r')
    feature_map = {}
    # feature_list = []
    for line in f3:
        line = line.rstrip('\n')
        if line == '':
            continue
        fid, fname, ftype = line.split('\t')
        if fid.startswith('#'):
            fid = fid.replace('#','').strip()
        feature_map[fname] = int(fid)
        # feature_list.append(fname)
    f3.close()

    f = open(data_file)
    out_f = open(output_file,'w+')
    out_s = open(score_file, 'w+')
    ignore_set = set(['id','gbdt_score','score'])
    missing_set = set()
    for line in f:
        data = json.loads(line)
        aid = data['uid']

        for doc in data['docs']:
            content_id = doc['id']
            raw_score = doc['gbdt_score']
            aged_score = doc['score']
            out_s.write('\t'.join([str(_k) for _k in (aid,content_id,raw_score, aged_score)]))
            out_s.write('\n')
            feature_data = {}
            for fname in doc:
                if fname in ignore_set:
                    continue
                actual_name = feature_name_map_inv[fname]
                if actual_name not in feature_map:
                    missing_set.add(actual_name)
                else:
                    fid = feature_map[actual_name]
                    feature_data[fid] = doc[fname]
            out_f.write('0 ' + ' '.join([str(fid)+':'+str(feature_data[fid]) for fid in sorted(feature_data)]))
            out_f.write('\n')

    out_f.close()
    out_s.close()



