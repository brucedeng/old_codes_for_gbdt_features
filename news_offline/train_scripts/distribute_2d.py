#!/usr/bin/env python
#-*-encoding:utf-8-*-

import sys
import argparse

# def normalize_2d(data):

def checkerboard_table(data, fmt='{:.2f}', boundary = None):
    import matplotlib.pyplot as plt
    import numpy as np
    import pandas
    from matplotlib.table import Table
    import matplotlib.cm as cm

    fig, ax = plt.subplots()

    ax.set_axis_off()
    fig.set_size_inches(28.5, 20.5)
    tb = Table(ax, bbox=[0,0,1,1])

    nrows, ncols = data.shape
    width, height = 1.0 / ncols, 1.0 / nrows

    # Add cells
    for (i,j), val in np.ndenumerate(data):
        # Index either the first or second item of bkg_colors based on
        # a checker board pattern
        idx = [j % 2, (j + 1) % 2][i % 2]
        # color = bkg_colors[idx]
        color = str(val)
        if boundary is not None:
            v2 = (val - boundary[0] )/(boundary[1]-boundary[0])
            color = cm.hot(v2)
        else:
            color = cm.hot(v2)

        tb.add_cell(i, j, width, height, text=fmt.format(val), 
                    loc='center', facecolor=color)

    # Row Labels...
    for i, label in enumerate(data.index):
        tb.add_cell(i, -1, width, height, text=label, loc='right', 
                    edgecolor='none', facecolor='none')
    # Column Labels...
    for j, label in enumerate(data.columns):
        tb.add_cell(-1, j, width, height/2, text=label, loc='center', 
                           edgecolor='none', facecolor='none')
    ax.add_table(tb)
    return fig

if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--square', action='store_true', dest='square',default=False, help='whether the table is square or not.')
    arg_parser.add_argument('-p','--plot', action='store_true', dest='plot_figure',default=False, help='whether to plot or not.')
    arg_parser.add_argument('filename', help='file name of the input')
    args = arg_parser.parse_args()
    square_flag = args.square
    filename = args.filename
    plot_figure = args.plot_figure

    if filename == '-':
        input_file = sys.stdin
    else:
        input_file = open(filename)

    headers = []
    indexes = []
    header_map = {}
    index_map = {}
    data = []
    header_avg = {}
    index_avg = {}
    max_value = 0.0
    min_value = 1000000
    if square_flag:
        index_map = header_map
        indexes = headers

    for line in input_file:
        line = line.rstrip('\n')
        fields = line.split('\t')
        header = fields[0]
        index = fields[1]
        val = float(fields[2])
        if len(fields) >= 4:
            weight = float(fields[3])
        else:
            weight = 1.0

        if header not in header_map:
            header_map[header] = len(headers)
            headers.append(header)
        col_num = header_map[header]

        if not square_flag:
            if index not in index_map:
                index_map[index] = len(indexes)
                indexes.append(index)
            row_num = index_map[index]
        else:
            if index not in header_map:
                header_map[index] = len(headers)
                headers.append(index)
            row_num = header_map[index]

        if col_num not in header_avg:
            header_avg[col_num] = [0.0,0.0]
        header_avg[col_num][0] += val * weight
        header_avg[col_num][1] += weight

        if row_num not in index_avg:
            index_avg[row_num] = [0.0,0.0]
        index_avg[row_num][0] += val * weight
        index_avg[row_num][1] += weight

        for idx in xrange(len(data),row_num + 1):
            data.append([])
        # print data
        idx_length = len(data[row_num])
        if col_num + 1 > idx_length:
            data[row_num] += [0.0] * (col_num- idx_length + 1)
        data[row_num][col_num] = val

    for idx in xrange(len(data),len(indexes)):
        data.append([])

    target_length = len(headers)
    for row_idx in xrange(len(data)):
        line = data[row_idx]
        cur_length = len(line)
        for col_idx in xrange(len(line)):
            if data[row_idx][col_idx] > 0.0:
                data[row_idx][col_idx] = data[row_idx][col_idx] / ((header_avg[col_idx][0]/header_avg[col_idx][1]) * (index_avg[row_idx][0]/index_avg[row_idx][1]))
                val = data[row_idx][col_idx]
                if val > max_value:
                    max_value = val
                if val < min_value:
                    min_value = val
        if cur_length < target_length:
            line += [0.0] * (target_length-cur_length)
    # print data

    if plot_figure:
        import matplotlib.pyplot as plt
        import numpy as np
        import pandas
        from matplotlib.table import Table
        import matplotlib.cm as cm
        data = pandas.DataFrame(np.array(data), columns=[x[:7] for x in headers], index=[x[:7] for x in indexes])
                    # columns=['A','B','C','D','E','F','G','H'],index=['A','B','C','D','E','F','G','H'])
        # plt.figure()
        checkerboard_table(data, boundary=(min_value,max_value))
        plt.show()


    # print '\t' + '\t'.join(headers)
    # for idx in xrange(len(data)):
    #     print indexes[idx] + '\t' + '\t'.join([str(k) for k in data[idx]])

