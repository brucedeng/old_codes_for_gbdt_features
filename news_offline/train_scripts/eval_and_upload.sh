#!/bin/bash

bin_path=$(readlink -f $0)
bin_path=$(dirname $bin_path)

set -e

function printhelp {
    echo Usage $0 -p train,weight,stat,split_train,split_test -m '<featmap>' '<training_file>' '<testing_file>'
}
declare -A procs

featmap_file=featmap-v3.txt
PROC='all'
bias_ratio=1
train_gp=""

aws_raw_path=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_evaluation/20160109

while [[ $# -gt 0 ]]
do
key="$1"
echo $key

case $key in
    -h|--help)
    printhelp;
    exit
    ;;
    -r|--raw_test)
    raw_test="$2"
    shift
    ;;
    -n|--num_round)
    num_round="$2"
    shift
    ;;
    -o|--output)
    output="$2"
    shift
    ;;
    --aws)
    aws_raw_path="$2"
    shift
    ;;
    *)
    echo unknown option $key
    ;;
esac
shift
done

xgboost=xgboost_quick

echo "checking executable xgboost"
xgboost_exec=$(which $xgboost)
if [[ "a" == "a$xgboost_exec" ]];
then
    echo "no excutable xgboost found. exiting..."
    exit 1
else
    echo "xgboost is $xgboost_exec"
fi

raw_test=$(readlink -f $raw_test)
cd $output
model_name=`echo $num_round|sed ':a;s/\(....\)/\1/;tb;s/^/0/;ta;:b' `
echo "dumping for $model_name.model"
$xgboost global.conf use_buffer=0 task=dump model_in=models/$model_name.model fmap=featmap.txt name_dump=dump.txt dump_stats=1
echo "generated importance file dump.imp"
python $bin_path/featureImp.py dump.txt > dump.imp
$xgboost global.conf use_buffer=0 task=dump model_in=models/$model_name.model fmap=featmap.txt name_dump=dump_online.txt
echo "predicting..."
$xgboost global.conf task=pred  model_in=models/$model_name.model name_pred=pred.txt
cat $raw_test | cut -f1,2,3 | paste - pred.txt > eval_data.txt
aws_raw_path=`echo $aws_raw_path | sed 's/\/$//g'`
echo "uploading aws $aws_raw_path/$output"
aws s3 cp eval_data.txt $aws_raw_path/$output

find . -name '*.buffer' -print0 | xargs -0 rm -rf

