#!/usr/bin/env python
# %matplotlib inline
# from matplotlib import pyplot as plt
import sys
import argparse
import datetime
import os, urllib2, json
import copy

def load_gmp(path, threshold=20.0):
    res = {}
    file = open(path)
    total_ec = 0
    total_c = 0

    for line in file:
        f = line.split('\t')
        item_id = str(f[0])
        original_ec = float(f[2])
        ec = float(f[4])
        c = float(f[5])

        if c >0 and ec >0 and original_ec > threshold and c <= ec:
            coec = c/ec
            res[item_id] = coec #(c,ec,coec)

        total_ec += ec
        total_c += c

    res['total'] = total_c/total_ec#(total_c,total_ec,total_c/total_ec)
    file.close()

    return res

def avg_list_step(data,step):
    length = len(data)
    length = length / step * step
    i = step
    result = []
    while i <= length:
        result.append(sum(data[i-step:i])/float(step))
        i += step
    return result

def get_rank_features(json_str):
    try:
        data = json.loads(json_str)
        return float(data['D_CONID_COEC'])
    except:
        return 0.0

def calc_rel(a,b):
    if a is None or b is None or len(a) == 0 or len(b) == 0:
        return 0.0
    result = 0.0
    for k,v in a.items():
        # print k,v
        result += float(v) * float(b.get(k,0.0))
    return result

def get_feature_lists(filename):
    #load feature map file
    feature_map_file = filename
    f3 = open(feature_map_file, 'r')
    feature_map = {}
    feature_list = []
    for line in f3:
        line = line.rstrip('\n')
        if line == '':
            continue
        fid, fname, ftype = line.split('\t')
        if fid.startswith('#'):
            fid = fid.replace('#','').strip()
        feature_map[fname] = fid
        feature_list.append(fname)
    f3.close()
    return feature_map

if __name__=='__main__':
    arg_parser = argparse.ArgumentParser()
    # arg_parser.add_argument('-s', '--step', action='store', dest='step',type=int, default=1, help='step for average articles.')
    arg_parser.add_argument('-u', '--url', action='store', dest='url', help='serving url.')
    # arg_parser.add_argument('-m', '--featmap', action='store', dest='featmap', help='serving url.')

    # arg_parser.add_argument('-g', '--gmp', action='store', dest='gmp', help="gmp file. schema is: content_id,batch_id,original_ec,original_c,decay_ec,decay_c,timestamp ")

    args = arg_parser.parse_args()

    url = args.url
    # featmap_file = args.featmap
    response = urllib2.urlopen(url)
    data = json.loads(response.read())

    # feature_map = get_feature_lists(featmap_file)

    user_profile = data['z_debug']['userprofile']
    # user_profile = data['userprofile']
    u_categories = user_profile['categories']
    u_keywords = user_profile['keywords']

    items = data['newsid']
    # items = data['data']
    total_res = []

    for item in items:
        try:
            fields = item['fields']
            item_id = item['id']
            # score = item['score']
            features = json.loads(fields.get('ranking_features','{}'))
            score = features['gbdt']
            age = features['DOC_AGE']
            # result = str(item_id) + '\t' + str(score)
            # for k,v in features.items():
            #     if k in feature_map:
            #         result += " " + feature_map[k] + ':' + str(v)
            # print result
            total_res.append((float(score),int(age)))
        except:
            pass
        # c_keywords = json.loads(fields.get('keywords','{}'))
        # c_keywords = json.loads(fields.get('phase1_keywords','{}'))
        # c_categories = json.loads(fields.get('categories','{}'))
        # c_categories = json.loads(fields.get('phase1_categories','{}'))
        # if 'phase1_features' not in fields:
        #     continue
        # phase_1_features = json.loads(fields['phase1_features'])
        # if 'ranking_features' not in fields:
        #     continue
        # phase_2_features = json.loads(fields['ranking_features'])
        # if phase_1_features is None:
        #     phase_1_features = {}
        # if phase_2_features is None:
        #     phase_2_features = {}
        # print phase_1_features.get('kw_rel'),phase_2_features.get('KW_REL'),calc_rel(u_keywords,c_keywords), phase_1_features.get('cat_rel'),phase_2_features.get('CAT_REL'),calc_rel(u_categories,c_categories)

    print total_res
    # gmp_template = args.gmp
    # step = args.step

