#!/usr/bin/env python
#-*-encoding:utf-8-*-

import sys
import argparse

if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-i','--input_file', action='store', dest='input_file', help='input file')
    arg_parser.add_argument('-c','--column', action='store', dest='column',default=1, type=int, help='column id, starts from 1.')
    arg_parser.add_argument('filename', help='mapping file name')
    args = arg_parser.parse_args()
    filename = args.filename
    input_file = args.input_file
    column_id = args.column - 1

    f = open(filename)
    name_map = {}
    for line in f:
        line = line.rstrip('\n')
        fields = line.split('\t')
        name_map[fields[0]] = fields[1]

    if input_file is None:
        input_file = sys.stdin
    else:
        input_file = open(input_file)

    for line in input_file:
        line = line.rstrip('\n')
        fields = line.split('\t')
        fields[column_id] = name_map.get(fields[column_id],fields[column_id])
        print '\t'.join(fields)
