#!/bin/bash

bin_path=$(readlink -f $0)
bin_path=$(dirname $bin_path)

set -e

function printhelp {
    echo Usage $0 -p train,weight,stat,split_train,split_test -m '<featmap>' '<training_file>' '<testing_file>'
}
declare -A procs

featmap_file=featmap-v3.txt
PROC='all'
bias_ratio=1
train_gp=""

while [[ $# -gt 0 ]]
do
key="$1"
echo $key

case $key in
    -h|--help)
    printhelp;
    exit
    ;;
    -d|--data)
    data_file="$2"
    shift
    ;;
    -m|--featmap)
    featmap_file="$2"
    shift
    ;;
    -c|--config)
    config_file="$2"
    shift
    ;;
    -t|--test)
    test_file="$2"
    shift
    ;;
    -o|--output)
    output="$2"
    shift
    ;;
    -w|--weight)
    weight_file="$2"
    shift
    ;;
    *)
    echo unknown option $key
    ;;
esac
shift
done

if [[ "a$weight_file" == "a" ]]; then
    weight_file=${data_file}.weight
fi

rm -rf $output
mkdir -p $output

if [[ -e ${weight_file} ]]; then
    cp ${weight_file} $output/
else
    echo "WARN: weight file $weight_file not found"
fi

cp $config_file $output/global.conf
cp $featmap_file $output/featmap.txt

xgboost=xgboost_quick

echo "checking executable xgboost"
xgboost_exec=$(which $xgboost)
if [[ "a" == "a$xgboost_exec" ]];
then
    echo "no excutable xgboost found. exiting..."
    exit 1
else
    echo "xgboost is $xgboost_exec"
fi

data_file_abs=`readlink -f $data_file`
test_file_abs=`readlink -f $test_file`

cd $output
data_file_rel=`python -c "import os.path; print os.path.relpath('$data_file_abs', '.')"`
test_file_rel=`python -c "import os.path; print os.path.relpath('$test_file_abs', '.')"`
ln -s $data_file_rel training_data
ln -s $test_file_rel testing_data
ln -s $data_file_rel eval_training_data
mkdir models
$xgboost global.conf use_buffer=0 2> train.log
