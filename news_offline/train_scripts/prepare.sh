set -e

bin_path=$(readlink -f $0)
bin_path=$(dirname $bin_path)

# $bin_path/pull_aws_data.sh train_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiment/hi_cfb_training_data/2016010{4,5,6,7,8,9}
# $bin_path/pull_aws_data.sh test_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiment/hi_cfb_training_data/201601{10,11}
# $bin_path/pull_aws_data.sh featmap.txt s3://com.cmcm.instanews.usw2.prod/model/experiments/hi_cfb_training_featmap/20160105-20160111

# offline calculated cfb
# $bin_path/pull_aws_data.sh train_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_data/201602{06,07,08,09,10,11}
# $bin_path/pull_aws_data.sh test_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_data/201602{12,13}
# $bin_path/pull_aws_data.sh featmap.txt s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/20160206-20160213

# # nrt batch dedup raw cfb
# $bin_path/pull_aws_data.sh train_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/en_nrtcfb_dedup_training_data/201602{06,07,08,09,10,11}
# $bin_path/pull_aws_data.sh test_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/en_nrtcfb_dedup_training_data/201602{12,13}
# $bin_path/pull_aws_data.sh featmap.txt s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/20160206-20160213

# # nrt no batch dedup raw cfb
# $bin_path/pull_aws_data.sh train_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/en_nrtcfb_training_data/201602{06,07,08,09,10,11}
# $bin_path/pull_aws_data.sh test_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/en_nrtcfb_training_data/201602{12,13}
# $bin_path/pull_aws_data.sh featmap.txt s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/20160206-20160213

# # hindi featurelog training
# $bin_path/pull_aws_data.sh train_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/hi_featurelog_training_data/201602{06,07,08,09,10,11}
# $bin_path/pull_aws_data.sh test_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/hi_featurelog_training_data/201602{12,13}
# $bin_path/pull_aws_data.sh featmap.txt s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/20160206-20160213

# # hindi offline cfb
# $bin_path/pull_aws_data.sh train_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/hi_offlinecfb_training_data/201602{15,16,17,18,19}
# $bin_path/pull_aws_data.sh test_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/hi_offlinecfb_training_data/201602{20,21}

# # en offline cfb no dedup
# $bin_path/pull_aws_data.sh train_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/en_offlinecfb_training_data/201602{15,16,17,18,19}
# $bin_path/pull_aws_data.sh test_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/en_offlinecfb_training_data/201602{20,21}

# # en offline cfb no dedup
# $bin_path/pull_aws_data.sh train_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_data_offlinededup/201602{15,16,17,18,19}
# $bin_path/pull_aws_data.sh test_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_data_offlinededup/201602{20,21}

# # en nrt cfb no dedup
# $bin_path/pull_aws_data.sh train_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/en_nrtcfb_training_data/201602{15,16,17,18,19}
# $bin_path/pull_aws_data.sh test_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/en_nrtcfb_training_data/201602{20,21}

# # en nrt cfb with dedup
# $bin_path/pull_aws_data.sh train_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/en_nrtcfb_dedup_training_data/201602{15,16,17,18,19}
# $bin_path/pull_aws_data.sh test_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/en_nrtcfb_dedup_training_data/201602{20,21}

# # en offline cfb with no dedup
# $bin_path/pull_aws_data.sh train_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/en_offlinecfb_training_data_merged_cp/201602{20,21,22,23,24}
# $bin_path/pull_aws_data.sh test_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/en_offlinecfb_training_data_merged_cp/201602{25,26}

# en offline cfb with no dedup
# $bin_path/pull_aws_data.sh train_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_data_offlinededup2/201602{20,21,22,23,24}
# $bin_path/pull_aws_data.sh test_data_raw s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_data_offlinededup2/201602{25,26}

cat train_data_raw | cut -f5 > training_data &
cat test_data_raw | cut -f5 > testing_data &
cat test_data_raw | cut -f1,2,3,4 > test_data_label &
cat train_data_raw | cut -f3,4 | python $bin_path/calc_weight.py > training_data.weight

wait
exit
