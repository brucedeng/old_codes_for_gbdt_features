#!/usr/bin/env python
# %matplotlib inline
from matplotlib import pyplot as plt
import sys
import argparse
import datetime
import os, urllib2, json
import copy

def load_gmp(path, threshold=20.0):
    res = {}
    file = open(path)
    total_ec = 0
    total_c = 0

    for line in file:
        f = line.split('\t')
        item_id = str(f[0])
        original_ec = float(f[2])
        ec = float(f[4])
        c = float(f[5])

        if c >0 and ec >0 and original_ec > threshold and c <= ec:
            coec = c/ec
            res[item_id] = coec #(c,ec,coec)

        total_ec += ec
        total_c += c

    res['total'] = total_c/total_ec#(total_c,total_ec,total_c/total_ec)
    file.close()

    return res

def avg_list_step(data,step):
    length = len(data)
    length = length / step * step
    i = step
    result = []
    while i <= length:
        result.append(sum(data[i-step:i])/float(step))
        i += step
    return result

def get_rank_features(json_str):
    try:
        data = json.loads(json_str)
        #return float(data['D_CONID_COEC'])
        # return float(data['D_TITLEMD5_COEC'])
        # return float(data['CAT_REL'])
        return float(data['gbdt'])
    except:
        return 0.0

if __name__=='__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-s', '--step', action='store', dest='step',type=int, default=1, help='step for average articles.')
    arg_parser.add_argument('-u', '--url', action='store', dest='url', help='serving url.')
    arg_parser.add_argument('-g', '--gmp', action='store', dest='gmp', help="gmp file. schema is: content_id,batch_id,original_ec,original_c,decay_ec,decay_c,timestamp ")

    args = arg_parser.parse_args()
    gmp_template = args.gmp
    step = args.step

    data = timestamp = None
    timestamp=int(datetime.datetime.now().strftime('%s'))
    url=args.url
    response = urllib2.urlopen(url)
    data = json.loads(response.read())

    #data['newsid'] = filter(lambda x: x.get('des','').find('src=0') > 0,data['newsid'])
    print "num results: %d" % len(data['newsid'])
    item_list = data['newsid']

    top_age_range = [1,5,10,20,100,200]

    range_idx = 0
    idx = 0
    sum_age = 0
    result = []
    distribute_res = []
    age_distribute = {}

    for idx in range(len(item_list)):
        cur_age = timestamp - int(item_list[idx].get('fields',{}).get('publish_time',0))
        age_bin = int(cur_age/60/60) / 12 * 12

        if range_idx < len(top_age_range) and idx >= top_age_range[range_idx]:
            # print range_idx
            result.append(sum_age/float(idx)/3600)
            distribute_res.append(copy.copy(age_distribute))
            range_idx += 1

        sum_age += cur_age
        if age_bin not in age_distribute:
            age_distribute[age_bin] = 0
        age_distribute[age_bin] += 1

    # age_title = ['top 1', 'top 5', 'top 10', 'top 20' ]
    for idx in range(len(result)):
        print ("top %d" % top_age_range[idx]) + " average age: %f"%result[idx] + " ; age bin distribution: " + json.dumps(distribute_res[idx],sort_keys=True)

    print("total average age:" + str(sum_age/float(len(item_list))/3600))


    cur_ts = datetime.datetime.now()
    cur_ts = cur_ts.replace(minute=(cur_ts.minute/10*10))

    x_list = range(len(item_list))
    doc_age = [ (timestamp - int(_k.get('fields',{}).get('publish_time',0)))/3600.0 for _k in item_list]
    if gmp_template is not None:
        gmp_path = None
        for i in range(10):
            cur_ts = cur_ts - datetime.timedelta(minutes = 10 )
            gmp_path = cur_ts.strftime(gmp_template)
            if os.path.exists(gmp_path):
                break
        if gmp_path is None:
            print "error: can not find gmp file."
            sys.exit(2)
        gmp_map = load_gmp(gmp_path,1)
        print "loaded %d items from gmp file" % len(gmp_map)
        gmp_list = [ gmp_map.get(_k['id'],0.0) for _k in item_list ]
    else:
        gmp_list = [get_rank_features(_k['fields']['ranking_features']) for _k in item_list]
    score_list = [ _k['score'] for _k in item_list ]

    from scipy.stats.stats import pearsonr
    correlation = pearsonr(score_list, gmp_list)
    print 'correlation: %4f'%correlation[0]
    # print "pearson correlation between score and gmp is: %f"%correlation

    x_list = avg_list_step(x_list, step)
    doc_age = avg_list_step(doc_age,step)
    gmp_list = avg_list_step(gmp_list,step)

    plt.figure(1)
    plt.subplot(121)
    plt.scatter(x_list,doc_age)
    plt.xlabel("ranking index")
    plt.ylabel("doc_age(hour)")
    plt.title("ranking doc_age")

    plt.subplot(122)
    plt.scatter(x_list,gmp_list)
    plt.xlabel("ranking index")
    plt.ylabel("gmp")
    plt.title("ranking gmp")

    plt.show()

