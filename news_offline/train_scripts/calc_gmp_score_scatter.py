#!/usr/bin/env python
# %matplotlib inline
from matplotlib import pyplot as plt
import sys
import argparse

def load_gmp(path, threshold=20.0):
    res = {}
    file = open(path)
    total_ec = 0
    total_c = 0

    for line in file:
        f = line.split('\t')
        item_id = str(f[0])
        original_ec = float(f[2])
        ec = float(f[4])
        c = float(f[5])

        if c >0 and ec >0 and original_ec > threshold and c <= ec:
            coec = c/ec
            res[item_id] = (c,ec,coec)

        total_ec += ec
        total_c += c

    res['total'] = (total_c,total_ec,total_c/total_ec)
    file.close()

    return res

if __name__=='__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-u', '--user_list', action='store', dest='user_list', help='user whitelist to calculate gmp scatter.')
    arg_parser.add_argument('-t', '--title', action='store', dest='title', help='title of the plotted figure.')
    arg_parser.add_argument('score_file', help="score file. schema is: aid,content_id,label,score ")
    arg_parser.add_argument('gmp_file', help="gmp file. schema is: content_id,batch_id,original_ec,original_c,decay_ec,decay_c,timestamp ")
    args = arg_parser.parse_args()
    user_list = args.user_list
    score_file = args.score_file
    gmp_file = args.gmp_file

    gmp_map = load_gmp(gmp_file,100)

    new_user_set = set()
    use_new_user = False
    if user_list is not None:
        use_new_user = True
        f = open(user_list)
        for line in f:
            line = line.rstrip('\n')
            new_user_set.add(line)
        print "loaded %d user id list" % len(new_user_set)

    f = open(score_file)
    score_list = []
    gmp_list = []
    score_map = {}
    for line in f:
        line = line.rstrip('\n')
        fields = line.split('\t')
        aid = fields[0]
        content_id = fields[1]
        label = fields[2]
        score = float(fields[3])
        key = int(score*100)/100.0
        if content_id in gmp_map:
            if use_new_user and aid not in new_user_set:
                continue
            if key not in score_map:
                score_map[key] = []
            score_map[key].append(gmp_map[content_id][2])
            score_list.append(score)
            gmp_list.append(gmp_map[content_id][2])
    result = score_map.items()
    # print result[:10]
    x=[_k[0] for _k in result ]
    y = [sum(_k[1])/float(len(_k[1])) for _k in result ]
    # print y[:10]
    # print len(score_list)
    # plt.scatter(score_list, gmp_list)
    plt.scatter(x,y)
    plt.xlabel("score")
    plt.ylabel("gmp")
    if args.title is not None:
        plt.title(args.title)
    plt.show()

