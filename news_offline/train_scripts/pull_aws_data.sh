#!/bin/bash

tmp_file=`mktemp -d -t aws_file.XXXXXXX`

use_header=no
if [[ $1 == "-h" || $1 == "--header" ]]; then
    use_header=yes
    shift
fi

output=$1
shift
rm -f $output

got_header=no

total_line=0
while [[ $# -gt 0 ]]; do
    aws_file=$1
    if [[ $aws_file != */ ]]; then aws_file="$aws_file/"; fi

    for file in `aws s3 ls $aws_file | grep -v "Found" | awk '{print $4}'`
    do
        
        if [[ $file == .* ]]; then
            if [[ $use_header == "no" || $got_header=="yes" ]]; then
                continue
            else
                if [[ $file == ".header" ]]; then
                    got_header=yes
                else
                    continue
                fi
            fi
        fi
        echo downloading $file 1>&2
        aws s3 cp $aws_file$file $tmp_file/
        cat $tmp_file/$file >> $output
        linecnt=`cat $tmp_file/$file | wc -l`
        total_line=$(( total_line + linecnt ))
        echo downloaded $total_line lines 1>&2
        rm -f $tmp_file/$file
    done

    shift
done
