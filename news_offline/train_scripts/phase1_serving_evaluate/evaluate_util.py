#!/usr/bin/env python
# %matplotlib inline
import datetime
import urllib2, json


def get_phase1_features(json_str,fname):
    try:
        data = json.loads(json_str)
        v = float(data[fname])
        return v
    except:
        return 0.0


def get_rank_features(json_str):
    try:
        data = json.loads(json_str)
        coec = float(data['D_CONID_COEC'])
        ec = float(data['D_CONID_EC'])
        if ec > 20:
            return coec
        else:
            return 0.0
    except:
        return 0.0


baseline_itemid = {}

def eval_url(exp, url,uid):
    result_map = {}
    data = timestamp = None
    timestamp=int(datetime.datetime.now().strftime('%s'))

    url = url + '&exp=' + exp
    global baseline_itemid
    if exp not in baseline_itemid:
        print "evaluate baseline for exp: %s" % exp
        url_baseline = url + '&uid=aaa'
        response = urllib2.urlopen(url_baseline)
        data_baseline = json.loads(response.read())
        baseline = filter(lambda x : x['score'] < 2 and x['score'] > 0 and x['des'].find('src=8')>0, data_baseline['newsid'])
        baseline_itemid[exp] = set([t['id'] for t in baseline])

    url_p13n = url + '&uid=' + uid
    print uid
    #print url_p13n
    req = urllib2.Request(url_p13n)
    response = urllib2.urlopen(req)
    data = json.loads(response.read())

    if int(data['z_debug']['userprofile']['_origin_cg_len']) == 0 and int(data['z_debug']['userprofile']['_origin_kw_len']) == 0:
        return None, None

    item_list = filter(lambda x : x['score'] < 2 and x['score'] > 0 and x['des'].find('src=8')>0, data['newsid'])
    result_map['num_result'] = len(item_list)
    p13n_itemid = set([t['id'] for t in item_list])

    p13n_ratio = 1 - len(baseline_itemid[exp] & p13n_itemid) / float(len(p13n_itemid))
    result_map['ratio_p13n'] = p13n_ratio


    idx = 0
    sum_age = 0

    for idx in range(len(item_list)):
        cur_age = timestamp - int(item_list[idx].get('fields',{}).get('publish_time',0))
        sum_age += cur_age

    result_map['docage'] = sum_age/float(len(item_list))/3600

    #gmp_list = [get_rank_features(_k['fields']['ranking_features']) for _k in item_list]
    gmp_list = [float(_k['fields']['phase1_gmp']) for _k in item_list]

    p1_features = [json.loads(_k['fields'].get('phase1_features','')) for _k in item_list]
    cat_rel_list = [_k.get('cat_rel',0) for _k in p1_features]
    kw_rel_list = [_k.get('kw_rel',0) for _k in p1_features]

    #cat_rel_list = [get_phase1_features(_k['fields'].get('phase1_features',''),'cat_rel') for _k in item_list]
    #kw_rel_list = [get_phase1_features(_k['fields'].get('phase1_features',''),'kw_rel') for _k in item_list]

    gmp_nonzero = filter(lambda x:x>0, gmp_list)
    cat_nonzero = filter(lambda x:x>0, cat_rel_list)
    kw_nonzero = filter(lambda x:x>0, kw_rel_list)
    ratio = 1 - float(len(gmp_nonzero))/len(item_list)

    result_map['ratio_explore'] = ratio

    result_map['cat_match'] = float(len(cat_nonzero))/len(item_list)
    result_map['kw_match'] = float(len(kw_nonzero))/len(item_list)
    result_map['avg_cat'] = (sum(cat_nonzero))/len(cat_nonzero) if len(cat_nonzero) >0 else 0
    result_map['avg_kw'] = float(sum(kw_nonzero))/len(kw_nonzero) if len(kw_nonzero)>0 else 0
    result_map['avg_gmp'] = float(sum(gmp_nonzero))/len(gmp_nonzero) if len(gmp_nonzero)>0 else 0

    from scipy.stats.stats import pearsonr
    import math
    rank_index = range(len(item_list))
    correlation_gmp = pearsonr(gmp_list,rank_index)[0]
    result_map['cor_gmp']= 0 if math.isnan(correlation_gmp) else correlation_gmp
    correlation_cat = pearsonr(cat_rel_list,rank_index)[0]
    result_map['cor_cat']= 0 if math.isnan(correlation_cat) else correlation_cat
    correlation_kw = pearsonr(kw_rel_list,rank_index)[0]
    result_map['cor_kw']= 0 if math.isnan(correlation_kw) else correlation_kw



    score_list = [float(_k['score']) for _k in item_list]
    #score_list = [float(get_phase1_features(_k['fields'].get('ranking_features',''),'gbdt')) for _k in item_list]
    return score_list, result_map

if __name__ == '__main__':
    print "hello"

