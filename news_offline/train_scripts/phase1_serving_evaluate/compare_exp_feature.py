#!/usr/bin/env python
# %matplotlib inline
from matplotlib import pyplot as plt
import sys
import argparse
import datetime

import evaluate_util
from multiprocessing import Pool


def avg_list_step(data,step):
    length = len(data)
    length = length / step * step
    i = step
    result = []
    while i <= length:
        result.append(sum(data[i-step:i])/float(step))
        i += step
    return result


def add_list(base, des):
    if len(base) == 0:
        base = des
    else:
        base = base[0:len(des)]
        for i in range(len(base)):
            try:
                base[i] += des[i]
            except:
                print len(des), len(base), i

    return base



def do_line(data):
    exp, url, uid = data
    score_list, result_map = evaluate_util.eval_url(exp, url, uid)
    return exp, score_list, result_map


if __name__=='__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-c', '--result', action='store', dest='result_count',type=int, help='result count')
    arg_parser.add_argument('-u', '--url', action='store', dest='url', help='serving url.')
    arg_parser.add_argument('-e', '--exp', action='store', dest='exp', help='exp id')
    arg_parser.add_argument('-p', '--phase1-only', action='store_true', dest='phase1', default=False, help='phase1 only')

    args = arg_parser.parse_args()
    result_count = int(args.result_count)

    data = timestamp = None
    timestamp=int(datetime.datetime.now().strftime('%s'))
    url=args.url
    exps = args.exp.split(',')

    #url1 = url + '&exp=' + exps[0]
    #url2 = url + '&p1_gmp_weight=1&p1_kw_weight=0&p1_cat_weight=0&p1_cat_dec_weight=0&exp=' + exps[1]
    #print url2
    url += '&news_exploration_t=false&position_blending_t=false&insta_boost_newsy_score_t=false'
    url += '&count=%d' % result_count
    if args.phase1:
        url += '&declare_factor=1&diversity=false&insta_adult_filtering_t=false&prediction_ranking_t=false&news_exploration_t=false&insta_cms_news_t=false&insta_boost_newsy_score_t=false&adjust_score_t=false&candidates_pool_t_DATA0=insta_index_list_t:1000'

    featmap = ['D_TITLEMD5_COEC']
    result = {}
    header = []
    for exp in exps:
        url += '&exp=' + exp
        result[exp] = evaluate_util.eval_url_feature(url,featmap)
        header += [exp + ' score', exp + ' features']


    from prettytable import PrettyTable
    t = PrettyTable(header)
    for index in range(len(result[exps[0]])):
        row = []
        for exp in exps:
            row += list(result[exp][index])
        t.add_row(row)
    print t


