#!/usr/bin/env python
# %matplotlib inline
from matplotlib import pyplot as plt
import sys
import argparse
import datetime
import os, urllib2, json
import copy


def avg_list_step(data,step):
    length = len(data)
    length = length / step * step
    i = step
    result = []
    while i <= length:
        result.append(sum(data[i-step:i])/float(step))
        i += step
    return result

def get_rank_features(json_str):
    try:
        data = json.loads(json_str)
        coec = float(data['D_CONID_COEC'])
        ec = float(data['D_CONID_EC'])
        if ec > 100:
            return coec
        else:
            return 0.0
    except:
        return 0.0

if __name__=='__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-s', '--step', action='store', dest='step',type=int, default=1, help='step for average articles.')
    arg_parser.add_argument('-u', '--url', action='store', dest='url', help='serving url.')
    arg_parser.add_argument('-i', '--uid', action='store', dest='uid', help='uid')

    args = arg_parser.parse_args()
    step = args.step
    uid = args.uid

    data = timestamp = None
    timestamp=int(datetime.datetime.now().strftime('%s'))
    url=args.url

    url_baseline = url + '&uid=aaa'
    response = urllib2.urlopen(url_baseline)
    data_baseline = json.loads(response.read())
    #baseline = filter(lambda x: x.get('des','').find('src=8') > 0,data_baseline['newsid'])
    baseline = data_baseline['newsid'][1:]
    baseline_itemid = set([t['id'] for t in baseline])

    url_p13n = url + '&uid=' + uid
    response = urllib2.urlopen(url_p13n)
    data = json.loads(response.read())

    #data['newsid'] = filter(lambda x: x.get('des','').find('src=8') > 0,data['newsid'])
    data['newsid'] = data['newsid'][1:]
    print "num results: %d" % len(data['newsid'])
    item_list = data['newsid']
    p13n_itemid = set([t['id'] for t in item_list])
    #item_list = item_list[:100]

    p13n_ratio = 1 - len(baseline_itemid & p13n_itemid) / float(len(p13n_itemid))
    print "personalized ratio: %.2f" % p13n_ratio

    top_age_range = [1,5,10,20,100,200]

    range_idx = 0
    idx = 0
    sum_age = 0
    result = []
    distribute_res = []
    age_distribute = {}

    for idx in range(len(item_list)):
        cur_age = timestamp - int(item_list[idx].get('fields',{}).get('publish_time',0))
        #cur_age = timestamp - int(item_list[idx].get('fields',{}).get('phase1_publish_time',0))
        age_bin = int(cur_age/60/60) / 12 * 12

        if range_idx < len(top_age_range) and idx >= top_age_range[range_idx]:
            # print range_idx
            result.append(sum_age/float(idx)/3600)
            distribute_res.append(copy.copy(age_distribute))
            range_idx += 1

        sum_age += cur_age
        if age_bin not in age_distribute:
            age_distribute[age_bin] = 0
        age_distribute[age_bin] += 1



    # age_title = ['top 1', 'top 5', 'top 10', 'top 20' ]
    for idx in range(len(result)):
        print ("top %d" % top_age_range[idx]) + " average age: %f"%result[idx] + " ; age bin distribution: " + json.dumps(distribute_res[idx],sort_keys=True)
    print("total average age:" + str(sum_age/float(len(item_list))/3600))

    cur_ts = datetime.datetime.now()
    cur_ts = cur_ts.replace(minute=(cur_ts.minute/10*10))

    x_list = range(len(item_list))
    #doc_age = [ (timestamp - int(_k.get('fields',{}).get('phase1_publish_time',0)))/3600.0 for _k in item_list]
    doc_age = [ (timestamp - int(_k.get('fields',{}).get('publish_time',0)))/3600.0 for _k in item_list]

    gmp_list = [get_rank_features(_k['fields']['ranking_features']) for _k in item_list]
    #gmp_list2 = [float(_k['fields']['phase1_gmp']) for _k in item_list]

    score_list = [ _k['score'] for _k in item_list ]


    gmp_nonzero = filter(lambda x:x>0, gmp_list)
    ratio = 1 - float(len(gmp_nonzero))/len(gmp_list)
    print "explore_rate: %.4f" % ratio

    #gmp_nonzero = filter(lambda x:x>0, gmp_list2)
    #print 1 - float(len(gmp_nonzero))/len(gmp_list2)

    from scipy.stats.stats import pearsonr
    correlation = pearsonr(score_list, gmp_list)
    print 'correlation: %4f'%correlation[0]
    # print "pearson correlation between score and gmp is: %f"%correlation

    x_list = avg_list_step(x_list, step)
    doc_age = avg_list_step(doc_age,step)
    gmp_list = avg_list_step(gmp_list,step)

    plt.figure(1)
    plt.subplot(121)
    plt.scatter(x_list,doc_age)
    plt.xlabel("ranking index")
    plt.ylabel("doc_age(hour)")
    plt.title("ranking doc_age")

    plt.subplot(122)
    plt.scatter(x_list,gmp_list)
    plt.xlabel("ranking index")
    plt.ylabel("gmp")
    plt.title("ranking gmp")

    plt.show()

