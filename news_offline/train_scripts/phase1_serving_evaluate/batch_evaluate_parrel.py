#!/usr/bin/env python
# %matplotlib inline
from matplotlib import pyplot as plt
import sys
import argparse
import datetime

import evaluate_util
from multiprocessing import Pool


def avg_list_step(data,step):
    length = len(data)
    length = length / step * step
    i = step
    result = []
    while i <= length:
        result.append(sum(data[i-step:i])/float(step))
        i += step
    return result


def add_list(base, des):
    if len(base) == 0:
        base = des
    else:
        base = base[0:len(des)]
        for i in range(len(base)):
            try:
                base[i] += des[i]
            except:
                print len(des), len(base), i

    return base



def do_line(data):
    exp, url, uid = data
    score_list, result_map = evaluate_util.eval_url(exp, url, uid)
    return exp, score_list, result_map


if __name__=='__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-c', '--result', action='store', dest='result_count',type=int, help='result count')
    arg_parser.add_argument('-u', '--url', action='store', dest='url', help='serving url.')
    arg_parser.add_argument('-f', '--file', action='store', dest='filename', help='uid_file')
    arg_parser.add_argument('-e', '--exp', action='store', dest='exp', help='exp id')
    arg_parser.add_argument('-n', '--cnt', action='store', dest='cnt', help='user cnt')
    arg_parser.add_argument('-p', '--phase1-only', action='store_true', dest='phase1', default=False, help='phase1 only')

    args = arg_parser.parse_args()
    cnt = int(args.cnt)
    result_count = int(args.result_count)
    step = result_count/10

    data = timestamp = None
    timestamp=int(datetime.datetime.now().strftime('%s'))
    url=args.url
    exps = args.exp.split(',')

    #url1 = url + '&exp=' + exps[0]
    #url2 = url + '&p1_gmp_weight=1&p1_kw_weight=0&p1_cat_weight=0&p1_cat_dec_weight=0&exp=' + exps[1]
    #print url2
    url += '&count=%d' % result_count
    if args.phase1:
        url += 'declare_factor=1&diversity=false&insta_adult_filtering_t=false&prediction_ranking_t=false&news_exploration_t=false&insta_cms_news_t=false&insta_boost_newsy_score_t=false&adjust_score_t=false&candidates_pool_t_DATA0=insta_index_list_t:1000'

    base = {}
    result_map_sum = {}
    user_cnt = 0
    uid_file = open(args.filename)

    num_workers = 10
    pool = Pool(num_workers)

    url_list = []
    for line in uid_file:
        uid = line.split('\t')[0]

        for exp in exps:
            #url_exp = url + '&exp=' + exp
            url_list.append((exp, url, uid))
    uid_file.close()

    url_list = url_list[0:cnt*len(exps)]
    import random
    random.shuffle(url_list)
    results = pool.map(do_line, url_list, num_workers)
    user_cnt = 0
    for exp, score_list, result_map in results:
        if score_list == None or result_map == None:
            continue
        #print uid, exp
        if not exp in base:
            base[exp] = []

        #print score_list
        base[exp] = add_list(base[exp], score_list)

        for key in result_map:
            if exp+'@'+key not in result_map_sum:
                result_map_sum[exp+'@'+key] = result_map[key]
            else:
                result_map_sum[exp+'@'+key] += result_map[key]
        user_cnt += 1

    user_cnt = user_cnt/len(exps)

    avg_score = {}
    avg_score_step = {}
    for exp in exps:
        data = base[exp][1:]
        avg_score[exp] = [float(t)/float(user_cnt) for t in data]
        avg_score_step[exp] = avg_list_step(avg_score[exp],step)

    avg_result_map = {}
    for key in result_map_sum:
        avg_result_map[key] = result_map_sum[key]/user_cnt

    table_result = {}
    header = []
    for key in sorted(avg_result_map.keys()):
        #print key, avg_result_map[key]
        exp, fname = key.split('@')
        if fname not in header:
            header.append(fname)
        if exp not in table_result:
            table_result[exp] = ['%.4g' % (avg_result_map[key])]
        else:
            table_result[exp].append('%.4g' % (avg_result_map[key]))


    from prettytable import PrettyTable
    t = PrettyTable(['Exp'] + header)
    for exp in sorted(table_result.keys()):
        t.add_row([exp]+table_result[exp])
    print t


    from itertools import cycle
    cycol = cycle('bgrcmk').next

    for key in sorted(avg_score_step.keys()):
        l = avg_score_step[key]
        print key, str(["%.4g" % t for t in l])

    #plt.figure(2)
    #plt.subplot(121)
    #for exp in exps:
    #    plt.scatter(range(len(avg_score_step[exp])), avg_score_step[exp], color=cycol(), label=exp)
    #plt.legend(loc='lower right')
    #plt.xlabel("ranking index")
    #plt.ylabel("avg_score")
    #plt.title("ranking avg_score")
    #plt.show()

