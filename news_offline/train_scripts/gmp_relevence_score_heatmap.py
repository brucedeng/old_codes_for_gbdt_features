#!/usr/bin/env python

import sys
import csv

import numpy as np

def regulize_idx(idx, bin_size):
    if idx < 0:
        return 0
    if idx >= bin_size-1:
        idx = bin_size-1
    return idx

def plot_data(x_list, y_list, z_list, bin_size=100, x_range=(0,1), y_range=(0,1), xlabel='x', ylabel='y'):
    import matplotlib.pyplot as plt
    
    x_list = np.array(x_list)
    y_list = np.array(y_list)
    x_mean = np.mean(x_list)
    x_std = np.std(x_list)
    x_range=(x_mean-3*x_std, x_mean+3*x_std)

    y_mean = np.mean(y_list)
    y_std = np.std(y_list)
    y_range=(y_mean-3*y_std, y_mean+3*y_std)

    m = np.zeros((bin_size, bin_size))
    n = np.zeros((bin_size, bin_size))
    x_step = (x_range[1] - x_range[0])/float(bin_size)
    y_step = (y_range[1] - y_range[0])/float(bin_size)

    for idx in xrange(len(x_list)):
        x = x_list[idx]
        y = y_list[idx]
        z = z_list[idx]

        x_bin = regulize_idx((x-x_range[0])/x_step,bin_size)
        y_bin = regulize_idx((y-y_range[0])/y_step,bin_size)
        n[x_bin,y_bin] += 1
        m[x_bin,y_bin] += z
        # print x_bin,y_bin
        # print n[x_bin,y_bin]
        # sys.exit()

    n_div = 1.0 * n
    for v in np.nditer(n_div,op_flags=['readwrite']):
        if v <= 0:
            v[...] = 1 + v

    avg = np.divide(m,n_div)

    plt.figure(2)
    plt.subplot(121)
    plt.imshow(n, cmap='hot', interpolation='nearest')
    plt.title('count heatmap')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    plt.subplot(122)
    plt.imshow(avg, cmap='hot', interpolation='nearest')
    plt.title('value heatmap')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.show()

if __name__=="__main__":

    feature_file_name=sys.argv[1]
    gmp_list = []
    rel_list = []
    score_list = []
    with open(feature_file_name) as f:
        for line in f:
            line=line.replace('\n','')
            line = line.split('\t')
            gmp_list.append(float(line[1]))
            rel_list.append(float(line[2]))
            score_list.append(float(line[4]))
    plot_data(gmp_list,rel_list,score_list)
