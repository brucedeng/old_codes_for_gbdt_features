#!/bin/bash

bin_path=$(readlink -f $0)
bin_path=$(dirname $bin_path)

set -e

function printhelp {
    echo Usage $0 -p train,weight,stat,split_train,split_test -m '<featmap>' '<training_file>' '<testing_file>'
}
declare -A procs

featmap_file=featmap-v3.txt
PROC='all'
bias_ratio=1
train_gp=""

aws_raw_path=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_evaluation/20160109
output=models
while [[ $# -gt 0 ]]
do
key="$1"
echo $key

case $key in
    -h|--help)
    printhelp;
    exit
    ;;
    -n|--num_round)
    num_round="$2"
    shift
    ;;
    -o|--output)
    output="$2"
    shift
    ;;
    *)
    echo unknown option $key
    ;;
esac
shift
done

xgboost=xgboost_quick
output_path=$(readlink -f $output)
mkdir -p $output_path

model_name=`echo $num_round|sed ':a;s/\(....\)/\1/;tb;s/^/0/;ta;:b' `
for f in * ; do
    if [[ -e $f/models ]]; then
        pushd $f
        $xgboost global.conf use_buffer=0 task=dump model_in=models/$model_name.model fmap=featmap.txt name_dump=dump.txt dump_stats=1
        echo "generated importance file dump.imp"
        python $bin_path/featureImp.py dump.txt > dump.imp
        $xgboost global.conf use_buffer=0 task=dump model_in=models/$model_name.model fmap=featmap.txt name_dump=dump_online.txt
        cp dump_online.txt $output_path/$f.dump
        cp dump.imp $output_path/$f.imp
        popd
    fi
done
