#!/bin/bash

featurelog=$1
mkdir feature_log_eval
python gen_data_from_featurelog.py -m featmap.txt -d $featurelog -o feature_log_eval/testing_data
cp global.conf feature_log_eval
cd feature_log_eval
xgboost_quick global.conf task=pred model_in=models
