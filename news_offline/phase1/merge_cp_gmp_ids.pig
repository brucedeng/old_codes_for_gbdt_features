REGISTER '../gen_train_data/lib/*.jar';


--rmf $output_path
--STORE cp_ids into '$output_path';

raw_content = LOAD '$cp_dump' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_content = foreach raw_content generate  
  json#'item_id' as content_id:chararray,
  --json#'title_md5' as title_md5:chararray,
  --json#'group_id' as groupid:chararray,
  --REPLACE(json#'publisher','(\r\n|\n|\t|\r)','') as publisher:chararray,
  --(float)json#'newsy_score' as newsy_score:float,
  json#'type' as type:chararray,
  --(int)(json#'source_type') as source_type:int,
  (long)json#'publish_time' as publish_time:long,
  --(long)json#'update_time' as update_time:long,
  --(int)json#'image_count' as image_count:int,
  --(int)json#'word_count' as word_count:int,
  json#'region' as region:chararray,
  json#'language' as language:chararray,
  json#'l2_categories' as categories:{t1:(y:map[])},
  json#'entities' as keywords:{t1:(y:map[])};

raw_content_filter = FILTER raw_content by ((type=='article' or type=='slideshow' or type=='photostory') and (region == '$country' and language == '$lan')) ;
raw_content = FOREACH raw_content_filter GENERATE content_id, publish_time,categories as categories:{t1:(y:map[])},keywords as keywords;

raw_content_distinct = foreach (group raw_content by content_id) {
    
    r = order raw_content by publish_time DESC;
    l = limit r 1;
    generate flatten(l);

};

content_info = FOREACH raw_content_distinct {
  categories = FOREACH categories GENERATE
          (chararray)y#'name' AS name:chararray,
          (double)y#'weight' AS weight:double;
  keywords = FOREACH keywords GENERATE
          (chararray)y#'name' AS name:chararray,
          (double)y#'L1_weight' AS weight:double;
  
  categories = filter categories by name is not null and weight is not null;
  keywords = filter keywords by name is not null and weight is not null;

  GENERATE
      content_id AS content_id,
      --title_md5 AS title_md5,
      --groupid AS groupid,
      --publisher AS publisher,
      --update_time AS update_time,
      publish_time AS publish_time,
      categories AS categories,
      keywords AS keywords;
}

cp_ids = LOAD '$cp_ids' as ids:chararray;

ids_cp_join = join content_info by content_id,  cp_ids by ids;

gmp = LOAD '$gmp_path' as (content_id:chararray, timestamp:int, original_ec:double, original_c:double, ec:double, c:double);
gmp_filter = filter gmp by (original_ec >= 50 and c > 0 and ec > 0 and c < ec);
gmp_coec = FOREACH gmp_filter GENERATE content_id, (double)c / (double)ec as gmp_coec;

ids_cp_gmp_join = join gmp_coec by content_id, ids_cp_join by content_id;

ids_cp_gmp = FOREACH ids_cp_gmp_join GENERATE 
  ids_cp_join::content_info::content_id as content_id,
  publish_time as publish_time,
  gmp_coec as gmp_coec,
  categories as categories,
  keywords as keywords;

rmf $output_path
STORE ids_cp_gmp into '$output_path' using JsonStorage();



