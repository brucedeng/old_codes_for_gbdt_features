#!/bin/sh

#basic setting
country=in
lan=hi
up_hdfs="/projects/news/user_profile/data/V4/up_json/${lan}_${country}"
#up_hdfs="/projects/news/user_profile/data/V4/up_json/others"

# cp_ids
export PATH=/data/guozhenyuan/anaconda/bin:$PATH
rm -rf cp_ids.data
python parse_cp_ids.py -c $country
hdfs_cp_ids='hdfs://mycluster/projects/news/model/'
hadoop fs -copyFromLocal -f cp_ids.data $hdfs_cp_ids
echo "cp_ids successfully dumped into $hdfs_cp_ids"

#cp
cur_date=`date +%Y%m%d`
ftr_start=`date -d "+4 day ago $cur_date" +%Y%m%d`;
agg_dates=$ftr_start
for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $cur_date; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done
input_cp_template="/projects/news/cpp/feeder/in_cp_dump/%s/*/*/*.data"
content_profile_path=`printf "$input_cp_template" "{$agg_dates}"`

#up
up_date=`date -d "+2 day ago" +%Y%m%d`
random=${RANDOM:0:2}
cmd_up="hadoop fs -cat ${up_hdfs}/${up_date}/"part-m-000${random}.gz" | zcat | grep '\"u_cat_len\" : [1-9]' | head -n 100 > up_sample_100.data"
echo $cmd_up
eval $cmd_up
#gmp 
cur_day=`date +%Y%m%d`
cur_hour=`date +%H`
cur_min=`date +%M`
cur_minute=`echo "$cur_min / 10 * 10" | bc | sed ':a;s/\(..\)/\1/;tb;s/^/0/;ta;:b'`
gmp_path="hdfs://mycluster/projects/news/nrt/india_group_gmp/dump/${cur_day}/${cur_hour}${cur_minute}/*"
#gmp_path="/projects/news/nrt/dwell_gmp/dump/${cur_day}/${cur_hour}${cur_minute}/*"

#output
output_path="hdfs://mycluster/projects/news/model/phase1/${cur_day}${cur_hour}${cur_minute}_cp_gmp"

#pig parameters
queue=experiment
PIG_PARAM=" -Dmapred.job.queue.name=$queue"

cmd="pig $PIG_PARAM -p gmp_path='$gmp_path' -p cp_dump='$content_profile_path' -p cp_ids='${hdfs_cp_ids}cp_ids.data' -p output_path='$output_path' -p lan='$lan' -p country='$country' merge_cp_gmp_ids.pig"
echo $cmd
eval $cmd

hadoop fs -cat "${output_path}/part-*" > cp_gmp_ids.data
echo "successfully dump cp_gmp_ids.data into local file"