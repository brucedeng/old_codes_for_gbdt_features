REGISTER '../gen_train_data/lib/*.jar';
REGISTER '../gen_train_data/lib/utils_multilan.py' USING jython AS myfunc;

raw_rcv_log = LOAD '$event_view' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);
raw_rcv_log = foreach raw_rcv_log generate
    json,
    json#'country' as country,
    json#'contentid' as content_id,
	json#'aid' as uid:chararray,
    --((long)json#'servertime_sec'/ 600 + 1 ) * 600 * 1000 as timestamp,
    json#'pid' as pid,
    json#'ctype' as ctype,
    json#'act' as act,
    json#'app_lan' as app_lan,
    myfunc.parse_src(json#'cpack') as src:chararray,
    myfunc.parse_rid(json#'cpack') as rid:chararray,
    myfunc.parse_exp(json#'upack') as exp:chararray,
    myfunc.parse_newuser_old(json#'upack') as newuser:chararray,
    --myfunc.parse_newuser(json#'upack') as newuser:chararray,
    myfunc.parse_firsttimeuser(json#'upack') as firsttimeuser:chararray;

raw_rcv_log = Filter raw_rcv_log by (newuser == 'true' or newuser == '1') and app_lan == '$lan' and act is not null and pid=='$pid' and country == '$country' and ( ctype is null or ctype=='' or ctype=='1' or ctype=='0x01' ) and act == '1' and (json#'scenario'#'level1' == '1' or (pid=='11' and json#'scenario'#'level1'=='10')) and json#'scenario'#'level1_type' =='1';

newuser_log = FOREACH raw_rcv_log GENERATE 
uid, 
(newuser == 'true' ? 1: 0) as newuser,
(firsttimeuser == 'true'? 1:0)  as firsttimeuser;

newuser_log_distinct = FOREACH ( Group newuser_log by uid ) {
    -- r = order newuser_log by newuser;
    top = limit $1 1;
    GENERATE flatten(top);
}

--res_Count = FOREACH (group newuser_log_distinct ALL) GENERATE SUM($1.newuser), SUM($1.firsttimeuser);

rmf $output
STORE newuser_log_distinct into '$output';

