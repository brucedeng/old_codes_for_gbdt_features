#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys,time,math
import urllib2
import json
import logging
from prediction_client import *
from scipy.stats.stats import pearsonr   
from scipy.stats.stats import spearmanr
from scipy.stats import rankdata
from prettytable import PrettyTable
import argparse

def int_value(val):
    try:
        return int(val)
    except:
        return -1

def cal_kw(article_kw,user_kw):
    res = 0
    if not article_kw or not user_kw:
        return 0
    for a_kw in article_kw:
        for u_kw in user_kw:
            if a_kw['name'] == u_kw['name']:
                res += a_kw['weight'] * u_kw['weight']
    return res

def cal_ctgy(article_ctgy,user_ctgy):
    res = 0
    if not article_ctgy or not user_ctgy:
        return 0
    for a_ctgy in article_ctgy:
        for u_ctgy in user_ctgy:
            if a_ctgy['name'] == u_ctgy['name']:
                res += a_ctgy['weight'] * u_ctgy['weight']
    return res

def phase2_score(cp_id_list,up_id,user_ctgy,user_kw,u_city,u_gender,u_age,u_cat_len,u_kw_len,pid,predictor_id,host='10.5.1.227',C_TOPN=15,K_TOPN=15):

	port = '31000'
	client = PredictionClient(host, port)
	doc_ids = cp_id_list
	query = Query()
	query.request_id = 'test'
	user_profile = UserProfile()
	user_profile.userid = up_id

	u_categories_format = {}
	for any in user_ctgy:
		u_categories_format[any['name']] = any['weight']
		if len(u_categories_format) > C_TOPN:
			break
	user_profile.categories = u_categories_format

	u_keywords_format = {}
	for any in user_kw:
		u_keywords_format[any['name'].encode("utf-8")] = any['weight']	
		if len(u_keywords_format) > K_TOPN:
			break
	user_profile.keywords = u_keywords_format

	user_profile.city = u_city
	user_profile.gender = 0 if u_gender == "" else int(u_gender)
	user_profile.age = 0 if u_age == "" else int(u_age)
	user_profile.cat_len = -1 if u_cat_len == 0 else int(u_cat_len)
	user_profile.kw_len = -1 if u_kw_len == 0 else int(u_kw_len)
	query.user_profile = user_profile
	query.doc_ids = doc_ids 
	query.product_id = str(pid)
	params = {'predictor': predictor_id}
	query.params = params
	scores = {}
	try:
		client.scoring(query, scores)
	except:
		client = PredictionClient('10.5.1.227', port)
		client.scoring(query, scores)

	return scores

def load_cp_gmp_ids(path,id_list,cp_dict,cp_selected_num):
	file = open(path)
	doc_count = 0 
	bad_head = 2
	for line in file:

		if doc_count < bad_head:
			doc_count += 1
			continue

		cp_json = json.loads(line)
		contend_id = str(cp_json['content_id'])
		publish_time = int(cp_json['publish_time'])
		gmp = float(cp_json['gmp_coec'])
		categories = cp_json['categories']
		keywords = cp_json['keywords']
		tmp_list = [publish_time,gmp,categories,keywords]
		cp_dict[contend_id] = tmp_list
		id_list.append(contend_id)
		doc_count += 1
		if cp_selected_num > 0 and ((doc_count-1-bad_head) > cp_selected_num):
			break
	file.close()
	return 

def load_up(path):
	up_list=[]
	file = open(up_file)
	for line in file:
		up_list.append(line)
	file.close()
	return up_list


if __name__ == '__main__':

	#logging
	console_handler = logging.StreamHandler()
	console_handler.setFormatter(logging.Formatter('%(asctime)s\t%(levelname)s\t%(message)s'))
	logger = logging.getLogger(__name__)
	logger.addHandler(console_handler)
	logger.setLevel(logging.DEBUG)

	#configs
	url_doc_id = 'http://10.5.1.193:8080/?action=filter_docs&doc_age=2&index_flag=2048'
	up_file = 'up_sample_100.data'
	pid = 11
	predictor_id = 'insta_news_hi_p2_upv4_20160716'
	host_list = ['10.5.1.227','10.5.1.225','10.5.2.85','10.5.2.86','10.5.1.27','10.5.1.28','10.5.2.33','10.5.2.36']
	doc_index_list = ['10.5.1.194','10.5.1.193','10.5.1.159']
	phase1_doc_num = 1000
	up_selected_num = 20
	cp_selected_num = 6000 # 0 means unlimited
	timestamp = int(time.time())

	#parameters
	arg_parser = argparse.ArgumentParser()
	arg_parser.add_argument('-c','--categories',action='store',dest='w_cat',help='categories')
	arg_parser.add_argument('-k','--keywords',action='store',dest='w_kw',help='keywords')
	arg_parser.add_argument('-g','--gmp',action='store',dest='w_gmp',help='gmp')
	arg_parser.add_argument('-d','--decayfactor',action='store',dest='w_decayfactor',help='decayfactor')
	arg_parser.add_argument('-p','--para',action='store',dest='para_name',help='para')
	arg_parser.add_argument('-s','--start',action='store',dest='start',help='para')
	arg_parser.add_argument('-e','--end',action='store',dest='end',help='para')
	arg_parser.add_argument('-t','--step',action='store',dest='step',help='para')

	args = arg_parser.parse_args()
	w_cat = float(args.w_cat)
	w_kw = float(args.w_kw)
	w_gmp = float(args.w_gmp)
	w_decayfactor = float(args.w_decayfactor)
	para_name = args.para_name
	para_val = float(args.start)
	para_max = float(args.end)
	para_step = float(args.step)

	#cp_gmp_ids
	id_list = []
	cp_dict = {}
	cp_path = 'cp_gmp_ids.data'
	load_cp_gmp_ids(cp_path,id_list,cp_dict,cp_selected_num)

	#up
	up_list = load_up(up_file)

	#setup table 
	t = PrettyTable(['gmp', 'keyword','category','age_factor','recall_rate','pearson_r','spearman_r'])

	#scoring
	while para_val <= para_max:
		
		#set parameter
		if para_name == "g":
			w_gmp = para_val
		elif para_name == "c":
			w_cat = para_val
		elif para_name == "k":
			w_kw = para_val
		elif para_name == "d":
			w_decayfactor = para_val
		logger.info("PARAMETERS: GMP %s KW %s CAT %s AGE_DECAY %s" % (w_gmp,w_kw,w_cat,w_decayfactor))	

		# for each user
		up_result = {'recall_rate':[],'pearson_r':[],'spearman_r':[]}
		up_count = 0
		for up_json in up_list[0:up_selected_num]:
			up = json.loads(up_json)
			user_kw = up['keywords']
			user_ctgy = up['categories']
			up_id = up['uid']

			score_1_dict = {}
			doc_count = 0
			#phase1 scoring
			#for each article
			for contend_id in cp_dict:

				#docage decay
				publish_time = cp_dict[contend_id][0]
				age_hour = float(timestamp - int(publish_time))/3600.0
				age_hour = 0 if age_hour < 0 else age_hour				

				# scoring
				score_gmp = cp_dict[contend_id][1]
				score_ctgy = cal_ctgy(cp_dict[contend_id][2],user_ctgy)
				score_kw = cal_kw(cp_dict[contend_id][3],user_kw)

				score_phase1 = w_gmp * score_gmp + w_kw * score_kw + w_cat * score_ctgy	
				score_1 = score_phase1 * math.exp(w_decayfactor*age_hour)
				score_1_dict[contend_id]=score_1

				doc_count += 1
				if doc_count % 5000 == 0:
					logger.info("Phase 1 : %d documents scoring completed" % doc_count)


			logger.info("Phase 1 : %d documents scoring completed" % doc_count)

			#phase2 scoring
			host_ind = int(up_count % len(host_list))
			score_2_dict = phase2_score(id_list,up_id,user_ctgy,user_kw,up.get('city',""),up.get('gender',0),up.get('age',0),up.get('u_cat_len',-1),up.get('u_kw_len',-1),pid,predictor_id,host_list[host_ind])
			logger.info("Phase 2 : %d documents scoring completed once" % doc_count)

			#pearsonr
			pearsonr_p1p2 = pearsonr(score_1_dict.values(),score_2_dict.values())
			#spearmanr
			score_1_rank = rankdata(score_1_dict.values())
			score_2_rank = rankdata(score_2_dict.values())
			spearmanr_p1p2 = spearmanr(score_1_rank,score_2_rank)
			#recall rate
			score_1_top = [x[0] for x in sorted(enumerate(score_1_dict.values()),key=lambda y:y[1],reverse=True)]
			score_2_top = [x[0] for x in sorted(enumerate(score_2_dict.values()),key=lambda y:y[1],reverse=True)]
			recall = len(set(score_1_top[0:phase1_doc_num])&set(score_2_top[0:phase1_doc_num]))

			up_result['recall_rate'].append(float(recall)/phase1_doc_num)
			up_result['pearson_r'].append(pearsonr_p1p2[0])
			up_result['spearman_r'].append(spearmanr_p1p2[0])

			logger.info("recall_rate: {},pearson correlation: {},spearman correlation: {} ".format(float(recall)/phase1_doc_num,pearsonr_p1p2[0],spearmanr_p1p2[0]))

			up_count += 1
		if up_count % 10 == 0:
			logger.info("Phase 1&2 scoring completed for {} users".format(up_count))

		recall_rate_mean = sum(up_result['recall_rate'])/float(len(up_result['recall_rate']))
		pearson_r_mean = sum(up_result['pearson_r'])/float(len(up_result['pearson_r']))
		spearman_r_mean = sum(up_result['spearman_r'])/float(len(up_result['spearman_r']))

		t.add_row([w_gmp,w_kw,w_cat,w_decayfactor,recall_rate_mean,pearson_r_mean,spearman_r_mean])

		para_val += para_step
		

	print t


