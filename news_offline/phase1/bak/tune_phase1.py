#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys,time,math
import urllib2
import json
import logging
from prediction_client import *
from scipy.stats.stats import pearsonr   
from scipy.stats.stats import spearmanr
from prettytable import PrettyTable

def int_value(val):
    try:
        return int(val)
    except:
        return -1

def cal_kw(article_kw,user_kw):
    res = 0
    if not article_kw or not user_kw:
        return 0
    for a_kw in article_kw.keys():
        for u_kw in user_kw:
            if a_kw == u_kw['name']:
                res += article_kw[a_kw] * u_kw['weight']
    return res

def cal_ctgy(article_ctgy,user_ctgy):
    res = 0
    if not article_ctgy or not user_ctgy:
        return 0
    for a_ctgy in article_ctgy.keys():
        for u_ctgy in user_ctgy:
            if a_ctgy == u_ctgy['name']:
                res += article_ctgy[a_ctgy] * u_ctgy['weight']
    return res

def phase2_score(cp_id_list,up_id,user_ctgy,user_kw,u_city,u_gender,u_age,u_cat_len,u_kw_len,pid,predictor_id,host='10.5.1.227',C_TOPN=15,K_TOPN=15):

	port = '31000'
	client = PredictionClient(host, port)
	doc_ids = cp_id_list
	query = Query()
	query.request_id = 'test'
	user_profile = UserProfile()
	user_profile.userid = up_id

	u_categories_format = {}
	for any in user_ctgy:
		u_categories_format[any['name']] = any['weight']
		if len(u_categories_format) > C_TOPN:
			break
	user_profile.categories = u_categories_format

	u_keywords_format = {}
	for any in user_kw:
		u_keywords_format[any['name'].encode("utf-8")] = any['weight']	
		if len(u_keywords_format) > K_TOPN:
			break
	user_profile.keywords = u_keywords_format

	user_profile.city = u_city
	user_profile.gender = 0 if u_gender == "" else int(u_gender)
	user_profile.age = 0 if u_age == "" else int(u_age)
	user_profile.cat_len = -1 if u_cat_len == 0 else int(u_cat_len)
	user_profile.kw_len = -1 if u_kw_len == 0 else int(u_kw_len)
	query.user_profile = user_profile
	query.doc_ids = doc_ids 
	query.product_id = str(pid)
	params = {'predictor': predictor_id}
	query.params = params

	scores = {}
	client.scoring(query, scores)

	return scores


if __name__ == '__main__':
	
	#configs
	url_doc_id = 'http://10.5.1.193:8080/?action=filter_docs&doc_age=2&index_flag=2048'
	up_file = 'hindi_up_sample.data'
	pid = 11
	predictor_id = 'insta_news_hi_p2_upv4_20160716'
	host_list = ['10.5.1.227','10.5.1.225','10.5.2.85','10.5.2.86','10.5.1.27','10.5.1.28','10.5.2.33','10.5.2.36']
	doc_index_list = ['10.5.1.194','10.5.1.193','10.5.1.159']
	timestamp = int(time.time())
	phase1_num = 60
	country = 'in'
	lan = 'hi'
	resp_cp_id = urllib2.urlopen(url_doc_id)
	cp_list_raw = json.loads(resp_cp_id.read())['filter_docs']
	cp_list = filter(lambda x : x[-2:] == country,cp_list_raw)


	console_handler = logging.StreamHandler()
	console_handler.setFormatter(logging.Formatter('%(asctime)s\t%(levelname)s\t%(message)s'))
	logger = logging.getLogger(__name__)
	logger.addHandler(console_handler)
	logger.setLevel(logging.DEBUG)

	
	#para
	para_name = 'keyword'
	age_factor = -0.03
	w1 = 1.0
	w2 = 12.0
	w3 = 1.0

	para_step = 2
	para_max = 17
	para_val = 13

	t = PrettyTable(['gmp', 'keyword','category','age_factor','recall_rate','pearson_r','spearman_r'])

	while para_val <= para_max:
		
		if para_name == "gmp":
			w1 = para_val
		elif para_name == "keyword":
			w2 = para_val
		elif para_name == "category":
			w3 = para_val
		elif para_name == "age_factor":
			age_factor = para_val
		
		logger.info("PARAMETERS: GMP {} KW {} CAT {} AGE_DECAY {}".format(w1,w2,w3,age_factor))	
		#up cp
		up_list = []
		with open(up_file) as f:
			for line in f:
				up_list.append(line)

		

		up_result = {'recall_rate':[],'pearson_r':[],'spearman_r':[]}
		up_num = 0
		for up_json in up_list[0:10]:
			up = json.loads(up_json)
			user_kw = up['keywords']
			user_ctgy = up['categories']
			up_id = up['uid']

			cp_id_list = []
			score_1_dict = {}
			doc_num = 0
			for cp in cp_list[0:200]:
				doc_ind = int(doc_num % len(doc_index_list))
				url_doc_detail = 'http://'+ doc_index_list[doc_ind] +':8080/?action=doc&id='
				try:
					resp_cp_detail = urllib2.urlopen(url_doc_detail + cp)
					cp_detail = json.loads(resp_cp_detail.read())
					cp_detail_lan = cp_detail['doc']['fields']['language']
				except:
					continue

				if cp_detail_lan != lan:
					continue
				cp_id = cp_detail['doc']['id']
				cp_id_list.append(cp_id)

				#docage 
				publish_time = cp_detail['doc']['publish_time']
				age_hour = float(timestamp - int(publish_time))/3600.0
				age_hour = 0 if age_hour < 0 else age_hour

				#phase1 scoring
				score_kw = cal_kw(cp_detail['doc']['keywords'],user_kw)
				score_ctgy = cal_ctgy(cp_detail['doc']['categories'],user_ctgy)
				score_gmp = cp_detail['doc']['gmp']
				score_phase1 = w1 * score_gmp + w2 * score_kw + w3 * score_ctgy	
				score_1 = score_phase1 * math.exp(age_factor*age_hour)
				score_1_dict[cp]=score_1
				doc_num += 1
				if doc_num % 5 == 0:
					logger.info("Phase 1 : {} documents scoring completed".format(doc_num))

			if len(cp_id_list) == 0:
				continue

			#phase2 scoring
			host_ind = int(up_num % len(host_list))
			score_2_dict = phase2_score(cp_id_list,up_id,user_ctgy,user_kw,up.get('city',""),up.get('gender',0),up.get('age',0),up.get('u_cat_len',-1),up.get('u_kw_len',-1),pid,predictor_id,host_list[host_ind])

			#pearsonr
			pearsonr_p1p2 = pearsonr(score_1_dict.values(),score_2_dict.values())
			#spearmanr
			score_1_rank = [x[0] for x in sorted(enumerate(score_1_dict.values()),key=lambda y:y[1])]
			score_2_rank = [x[0] for x in sorted(enumerate(score_2_dict.values()),key=lambda y:y[1])]
			spearmanr_p1p2 = spearmanr(score_1_rank,score_2_rank)
			#recall rate
			recall = len(set(score_1_rank[0:phase1_num])&set(score_2_rank[0:phase1_num]))

			up_result['recall_rate'].append(float(recall)/phase1_num)
			up_result['pearson_r'].append(pearsonr_p1p2[0])
			up_result['spearman_r'].append(spearmanr_p1p2[0])

			logger.info("recall_rate: {},pearson correlation: {},spearman correlation: {} ".format(float(recall)/phase1_num,pearsonr_p1p2[0],spearmanr_p1p2[0]))

		up_num += 1
		if up_num % 3 == 0:
			logger.info("Phase 1&2 scoring completed for {} users".format(up_num))

		recall_rate_mean = sum(up_result['recall_rate'])/float(len(up_result['recall_rate']))
		pearson_r_mean = sum(up_result['pearson_r'])/float(len(up_result['pearson_r']))
		spearman_r_mean = sum(up_result['spearman_r'])/float(len(up_result['spearman_r']))
		
		t.add_row([w1,w2,w3,age_factor,recall_rate_mean,pearson_r_mean,spearman_r_mean])

		para_val += para_step

		logger.info("NEXT STEP OF PARAMETER")

	print t


