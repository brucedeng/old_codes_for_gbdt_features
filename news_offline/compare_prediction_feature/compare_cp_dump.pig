REGISTER 'lib/*.jar';
REGISTER 'compare_json.py' USING jython AS myfunc;

%default cp_dump 's3://com.cmcm.instanews.usw2.prod/cpp/feeder/{20160210,20160211,20160212}/*/*'
%default test_cp_dump 's3://com.cmcm.instanews.usw2.prod/cpp/feeder/staging/{20160210,20160211,20160212}/*/*'

define dedupe_cp ( cp_data ) RETURNS deduped {
    $deduped = FOREACH ( GROUP $cp_data by content_id ) {
        r = ORDER $1 BY update_time desc;
        l = LIMIT r 1;
        GENERATE FLATTEN(l) as (content_id,update_time,json);
    }
}

-- pre process content
raw_content = LOAD '$cp_dump' USING com.twitter.elephantbird.pig.load.JsonLoader('') as (json:map[]);

cp_2 = load '$test_cp_dump' USING com.twitter.elephantbird.pig.load.JsonLoader('') as (json:map[]);

raw_content = foreach raw_content generate  
  json#'item_id' as content_id:chararray,
  json#'discovery_time' as update_time:long,
  json as json;

-- raw_content = FILTER raw_content by update_time >= 1454284800 and update_time <=1454371200;

raw_content = dedupe_cp(raw_content);

cp_2 = FOREACH cp_2 GENERATE
  json#'item_id' as content_id:chararray,
  json#'discovery_time' as update_time:long,
  json as json;

-- cp_2 = FILTER cp_2 by update_time >= 1454284800 and update_time <=1454371200;

cp_2 = dedupe_cp(cp_2);

jnd = JOIN raw_content by content_id full outer, cp_2 by content_id;

data_cmp = FOREACH jnd GENERATE raw_content::content_id as content_id, cp_2::content_id as cpid2, raw_content::json as json1, cp_2::json as json2;

data_cmp_id = FOREACH data_cmp GENERATE content_id, cpid2;
rmf /tmp/mengchong/compare_cp_id
store data_cmp_id into '/tmp/mengchong/compare_cp_id';

-- rmf /tmp/mengchong/compare_cp
-- store data_cmp into '/tmp/mengchong/compare_cp';

result_comp = FOREACH data_cmp GENERATE content_id, cpid2, myfunc.compare_map(json1,json2) as comp_result;
rmf /tmp/mengchong/compare_result
store result_comp into '/tmp/mengchong/compare_result';
