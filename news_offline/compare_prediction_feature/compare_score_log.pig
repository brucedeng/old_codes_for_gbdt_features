REGISTER '*.jar';
REGISTER 'utils.py' USING jython AS myfunc;
REGISTER 'cfb_agg.py' USING jython AS cfb;

%DECLARE ORI_COL "eventtime, act, aid, contentid, appv,brand, ccode,ip, net, model, pf, pid, v"
%DECLARE TYPE_COL "eventtime:long, act:chararray, aid:chararray, contentid:chararray, appv:chararray , brand:chararray, ccode:chararray, ip:chararray, net:chararray, model:chararray, pf:chararray, pid:chararray, v:chararray"

set mapred.create.symlink yes;
set mapred.cache.files $CONFIG#config.json

-- DEFINE rcvLoader com.ijinshan.cmcm.rcv.log.loader.rcvLogLoader('$ORI_COL','&', '=');
-- raw_rcv_log = LOAD '$INPUT_EVENT' USING rcvLoader AS ($TYPE_COL);
server_log = LOAD '$INPUT_PREDICTION' USING com.twitter.elephantbird.pig.load.JsonLoader() as (json:map[]);

server_log = FOREACH server_log GENERATE
    json#'predictor_id' as predictor_id:chararray,
    json#'now' as ts:long,
    json#'doc_size' as doc_size:int,
    json#'uid' as uid:chararray,
    json#'up' as up:chararray,
    json#'docs' as docs:chararray;

server_log_flat = FOREACH server_log GENERATE 
    predictor_id, ts , doc_size, uid, up, FLATTEN(myfunc.flatten_doc_data(docs)) as (item_id,doc);

-- pre process content
raw_content = LOAD '$INPUT_CONTENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_content = foreach raw_content generate  
  json#'item_id' as content_id:chararray,
  json#'publisher' as publisher:chararray,
  json#'type' as type:chararray,
  (long)json#'publish_time' as publish_time:long,
  (long)json#'update_time' as update_time:long,
  myfunc.gen_json_str(json) as d_raw:chararray,
  json#'categories' as categories:{t1:(y:map[])},
  json#'entities' as keywords:{t1:(y:map[])};

content_info = FOREACH raw_content {
  categories = FOREACH categories GENERATE
          (chararray)y#'name' AS name:chararray,
          (double)y#'weight' AS weight:double;
  keywords = FOREACH keywords GENERATE
          (chararray)y#'name' AS name:chararray,
          (double)y#'L1_weight' AS weight:double;
  
  categories = filter categories by name is not null and weight is not null;
  keywords = filter keywords by name is not null and weight is not null;

  GENERATE
        content_id AS content_id,
        publisher AS publisher,
        update_time AS update_time,
        categories AS categories,
        keywords AS keywords,
        -- newsy_score AS newsy_score,
        -- word_count AS word_count,
        -- image_count AS image_count,
        d_raw AS d_raw,
        type as type,
        publish_time as publish_time;
}

-- normalized categories and keywords
content_info_norm = FOREACH content_info GENERATE
      content_id AS content_id,
      publisher AS publisher,
      update_time AS update_time,
      -- newsy_score AS newsy_score,
      -- word_count AS word_count,
      -- image_count AS image_count,
      d_raw AS d_raw,
      type as type,
      publish_time as publish_time,
      myfunc.normalize(categories,0,1,$D_NCAT,'L1') AS categories,
      myfunc.top(keywords, $D_NKEY) AS keywords;

content_distinct = FOREACH ( GROUP content_info_norm by content_id ) GENERATE 
    $0 as content_id, 
    $1 as content_info;


-- rmf tmp/content_info_norm;
-- store content_info_norm into 'tmp/content_info_norm';

-- pre process user profile
raw_user_profile = LOAD '$INPUT_USER' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_user_profile = FOREACH raw_user_profile GENERATE
     json#'uid' AS uid:chararray,
     (chararray)json#'age' AS age:chararray,
     (chararray)json#'gender' AS gender:chararray,
     json#'categories' AS categories:{t1:(y:map[])},
     json#'keywords' AS keywords:{t1:(y:map[])},
     myfunc.gen_json_str(json) as u_raw:chararray;

describe raw_user_profile;

user_profile_info = FOREACH raw_user_profile {
  categories = FOREACH categories GENERATE
          (chararray)y#'name' AS name:chararray,
          (double)y#'weight' AS weight:double;
  keywords = FOREACH keywords GENERATE
          (chararray)y#'name' AS name:chararray,
          (double)y#'weight' AS weight:double;
  GENERATE
      uid AS uid,
      age AS age,
      gender AS gender,
      categories AS categories,
      keywords AS keywords,
      u_raw AS u_raw;
}

-- normalized categories and keywords
user_profile_info_norm = FOREACH user_profile_info GENERATE
      uid AS uid,
      age AS age,
      gender AS gender,
      myfunc.top(categories,$U_NCAT) AS categories,
      myfunc.top(keywords, $U_NKEY) AS keywords,
      u_raw as u_raw;

--rmf tmp/user_profile_info_norm;
--store user_profile_info_norm into 'tmp/user_profile_info_norm';

log_content_info_join = JOIN raw_event_out_withid BY (uid) LEFT,
                             user_profile_info_norm BY (uid) PARALLEL 50;

log_content_info_for = FOREACH  log_content_info_join GENERATE
      raw_event_out_withid::content_id AS content_id,
      raw_event_out_withid::productid AS productid,
      raw_event_out_withid::ts AS ts,
      raw_event_out_withid::join_ts AS join_ts,
      raw_event_out_withid::uid AS uid,
      raw_event_out_withid::label AS label,
      raw_event_out_withid::city AS city,
      user_profile_info_norm::u_raw AS u_raw,
      user_profile_info_norm::age AS age,
      user_profile_info_norm::gender AS gender,
      user_profile_info_norm::categories AS u_categories,
      user_profile_info_norm::keywords AS u_keywords;

log_content_user_info_join = JOIN log_content_info_for BY (content_id),
                                  content_info_norm BY (content_id) USING 'replicated';

log_content_user_info = FOREACH log_content_user_info_join GENERATE
        log_content_info_for::content_id AS content_id,
        log_content_info_for::productid AS productid,
        log_content_info_for::ts AS ts,
        log_content_info_for::join_ts AS join_ts,
        log_content_info_for::uid AS uid,
        log_content_info_for::label AS label,
        log_content_info_for::city AS city,
        log_content_info_for::age AS u_age,
        log_content_info_for::gender AS u_gender,
        u_categories AS u_categories,
        u_keywords AS u_keywords,
        content_info_norm::update_time AS cp_update_time,
        content_info_norm::d_raw AS d_raw,
        log_content_info_for::u_raw AS u_raw,
        -- content_info_norm::newsy_score AS cp_newsy_score,
        -- content_info_norm::word_count AS word_count,
        -- content_info_norm::image_count AS image_count,
        content_info_norm::type as cp_type,
        content_info_norm::publish_time as publish_time,
        content_info_norm::publisher AS publisher,
        content_info_norm::categories AS d_categories,
        content_info_norm::keywords AS d_keywords;

log_content_user_info = FOREACH log_content_user_info {
    id_tuple = TOTUPLE(uid,content_id,ts,productid,city,label);
    id = myfunc.get_md5_id(id_tuple);
    GENERATE id AS id, content_id .. d_keywords;
}

log_content_user_info_feature = FOREACH log_content_user_info {
    fields_map = TOMAP('d_content_id',content_id,'d_publisher',publisher, 'd_category',d_categories,'d_keyword',d_keywords,'u_uid',uid,'u_city',city, 'u_category',u_categories,'u_keyword',u_keywords,'u_age',u_age, 'u_gender', u_gender,  'score',0);
    GENERATE
        id AS id,
        ts AS ts,
        join_ts as join_ts,
        myfunc.gen_feature(fields_map,'all') AS  feature_bag:{t:(feature_type:chararray, feature_name:chararray, feature_weight:double,feature_score:double)};
}

event_user_content_profile_out = FOREACH log_content_user_info_feature GENERATE
    id AS id,
    (chararray)ts AS ts:chararray,
    (chararray)join_ts as join_ts:chararray,
    FLATTEN(feature_bag) AS (feature_type:chararray, feature_name:chararray, feature_weight:double, feature_score:double);

rmf $OUTPUT/event_user_content_profile_out
store event_user_content_profile_out into '$OUTPUT/event_user_content_profile_out';
-- -- rmf $OUTPUT/event_user_content_profile_out
-- -- store event_user_content_profile_out into'$OUTPUT/event_user_content_profile_out';
-- add cfb feature and join with all events
raw_cfb = LOAD '$INPUT_CFB' AS (
  ts : long,
  feature_type : chararray,
  feature_name : chararray,
  click_sum : double,
  view_sum : double
);

raw_cfb_group = GROUP raw_cfb BY (ts, feature_type, feature_name) PARALLEL 50;

raw_cfb = FOREACH raw_cfb_group GENERATE
    FLATTEN(group) AS (ts, feature_type, feature_name),
    SUM($1.click_sum) AS click_sum,
    SUM($1.view_sum) AS view_sum;

raw_cfb_filter = FILTER raw_cfb BY ts > 0; -- (myfunc.exclude_feature_type(feature_type) == '0');

cfb_group = GROUP raw_cfb_filter BY (feature_type, feature_name)  PARALLEL 30;

cfb_out = FOREACH cfb_group { 
    records = FOREACH $1 GENERATE
            ts AS ts,
            click_sum AS click_sum,
            view_sum AS view_sum,
            ts AS orig_ts;

    records_order = ORDER records BY ts ASC;
    GENERATE FLATTEN($0) as (feature_type,feature_name), records_order;
}

-- rmf /user/mengchong/cfb_test_output
-- store cfb_out into '/user/mengchong/cfb_test_output';

cfb_out_parsed = foreach cfb_out GENERATE feature_type, feature_name, cfb.gen_cfb_new(records_order,myfunc.date2timestamp('$START_DATE_TIME',$WND,'%Y%m%d%H')-$WND, myfunc.date2timestamp('$END_DATE_TIME', $WND,'%Y%m%d%H'), $DECAY_FACTOR, '$N_DECAYWND') as cfb_out_bag;
cfb_out_parsed_flat = FOREACH cfb_out_parsed GENERATE feature_type, feature_name, FLATTEN(cfb_out_bag) AS (ts:long,total_click_sum:double,total_view_sum:double,prev_ts:long);

-- store cfb_out_parsed_flat into '/user/mengchong/cfb_test_output_parsed';

-- join cfb end
cfb_out_parsed_flat = FILTER cfb_out_parsed_flat BY (total_view_sum > 0.0 or total_click_sum > 0.0);

cfb_out = STREAM  cfb_out_parsed_flat THROUGH `cat` AS ( 
    feature_type:chararray,
    feature_name:chararray,
    ts:chararray,
    total_click_sum:double,
    total_view_sum:double,
    prev_ts:long
);

-- cfb_out = load '$OUTPUT/cfb_agg' as (feature_type:chararray, feature_name:chararray, ts:chararray, total_click_sum:double, total_view_sum:double, prev_ts:long);
-- rmf $OUTPUT/cfb_agg
-- store cfb_out into '$OUTPUT/cfb_agg';

event_user_content_profile_cfb_join = JOIN event_user_content_profile_out BY (join_ts, feature_type, feature_name) LEFT,
                                          cfb_out BY (ts, feature_type, feature_name) PARALLEL 100;

event_user_content_profile_cfb = FOREACH  event_user_content_profile_cfb_join GENERATE
            id AS id,
            event_user_content_profile_out::feature_type AS feature_type,
            event_user_content_profile_out::feature_name AS feature_name,
            feature_weight AS feature_weight,
            cfb_out::total_click_sum AS click_sum,
            cfb_out::total_view_sum AS view_sum;

-- rmf $OUTPUT/event_user_content_profile_cfb
-- store event_user_content_profile_cfb into '$OUTPUT/event_user_content_profile_cfb';

event_user_content_profile_cfb_group = GROUP event_user_content_profile_cfb BY (id) PARALLEL 50;

event_cfb_json = FOREACH event_user_content_profile_cfb_group {
    cfb_bag = FOREACH $1 GENERATE
        feature_type AS feature_type,
        feature_name AS feature_name,
        feature_weight AS feature_weight,
        click_sum AS click_sum,
        view_sum AS view_sum;
    cfb_bag = FILTER cfb_bag by feature_type is not null and feature_name is not null;

    cfb_json = myfunc.gen_cfb_json(cfb_bag);
    GENERATE
    group  AS id,
    cfb_json AS cfb_json;
}

-- rmf $OUTPUT/event_cfb_json
-- store event_cfb_json into '$OUTPUT/event_cfb_json';

event_user_content_profile_cfb_json_join = JOIN log_content_user_info BY (id) LEFT,
                                                event_cfb_json BY (id) PARALLEL 100;

event_user_content_profile_cfb_json = FOREACH event_user_content_profile_cfb_json_join GENERATE 
        log_content_user_info::id as id,
        uid AS uid,
        content_id AS content_id,
        ts AS ts,
        label AS label,
        city AS city,
        -- u_categories AS u_categories,
        -- u_keywords AS u_keywords,
        log_content_user_info::publisher AS publisher,
        -- log_content_user_info::d_categories AS d_categories,
        -- log_content_user_info::d_keywords AS d_keywords,
        log_content_user_info::cp_update_time AS cp_update_time,
        log_content_user_info::d_raw AS d_raw,
        log_content_user_info::u_raw as u_raw,
        -- log_content_user_info::cp_newsy_score AS cp_newsy_score,
        -- log_content_user_info::word_count AS word_count,
        -- log_content_user_info::image_count AS image_count,
        log_content_user_info::cp_type as cp_type,
        log_content_user_info::publish_time as publish_time,
        log_content_user_info::u_age as u_age,
        log_content_user_info::u_gender as u_gender,
        cfb_json AS cfb_json;

rmf $OUTPUT/events_with_cfb_features
STORE event_user_content_profile_cfb_json INTO '$OUTPUT/events_with_cfb_features';

