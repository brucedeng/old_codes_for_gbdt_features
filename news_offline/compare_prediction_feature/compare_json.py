# -*- coding:utf-8 -*-
import os
import datetime
import time
import math,sys
import hashlib

if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper
    import json
else:
    import com.xhaus.jyson.JysonCodec as json

key_black_list = set(['feature_scores','update_time','newsy_score'])

@outputSchema('md5str:chararray')
def compare_map(map1,map2):
    if map1 is None:
        return '{"empty":1}'
    if map2 is None:
        return '{"empty":2}'

    result = {"empty":0,"1":[],"2":[],"diff":[],'value1':{},'value2':{}}
    for keys in sorted(map1):
        if keys in key_black_list:
            continue
        if keys not in map2:
            if map1[keys] != '' and map1[keys] is not None and len(map1[keys]) > 0:
                result["1"].append(keys)
        elif map2[keys] != map1[keys]:
            result['diff'].append(keys)
            result['value1'][keys] = map1[keys]
            result['value2'][keys] = map2[keys]

    for keys in sorted(map2):
        if keys in key_black_list:
            continue
        if keys not in map1:
            if map2[keys] != '' and map2[keys] is not None and len(map2[keys]) > 0:
                result['2'].append(keys)

    return json.dumps(result,sort_keys=True)

if __name__ == '__main__':
    ip_address = '49.15.86.0'
    # print get_city_name(ip_address)
    print timestamp2dateformat(1448939957, '%Y%m%d%H%M')

