#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys,time,math
import urllib2
import json
import logging
from prediction_client import *
from scipy.stats.stats import pearsonr   
from scipy.stats.stats import spearmanr
from scipy.stats import rankdata
from prettytable import PrettyTable
import argparse
from multiprocessing import Pool


def myany(lst):
	for any in lst:
		if any !=0 :
			return True
	return False

def cal_kw(article_kw,user_kw):
    res = 0
    if not article_kw or not user_kw:
        return 0
    for a_kw in article_kw:
        for u_kw in user_kw:
            if a_kw['name'] == u_kw['name']:
                res += a_kw['weight'] * u_kw['weight']
    return res

def cal_ctgy(article_ctgy,user_ctgy):
    res = 0
    if not article_ctgy or not user_ctgy:
        return 0
    for a_ctgy in article_ctgy:
        for u_ctgy in user_ctgy:
            if a_ctgy['name'] == u_ctgy['name']:
                res += a_ctgy['weight'] * u_ctgy['weight']
    return res

def phase2_score(cp_id_list,up_id,user_ctgy,user_kw,u_city,u_gender,u_age,u_cat_len,u_kw_len,pid,predictor_id,host='10.5.1.227'):

	port = '31000'
	client = PredictionClient(host, port)
	doc_ids = cp_id_list
	query = Query()
	query.request_id = 'test'
	user_profile = UserProfile()
	user_profile.userid = up_id

	u_categories_format = {}
	for any in user_ctgy:
		u_categories_format[any['name']] = float(any['weight'])

	user_profile.categories = u_categories_format

	u_keywords_format = {}
	for any in user_kw:
		u_keywords_format[any['name'].encode("utf-8")] = float(any['weight'])	

	user_profile.keywords = u_keywords_format

	user_profile.city = u_city
	user_profile.gender = 0 if u_gender == "" else int(u_gender)
	user_profile.age = 0 if u_age == "" else int(u_age)
	user_profile.cat_len = -1 if u_cat_len == 0 else int(u_cat_len)
	user_profile.kw_len = -1 if u_kw_len == 0 else int(u_kw_len)
	query.user_profile = user_profile
	query.doc_ids = doc_ids 
	query.product_id = str(pid)
	params = {'predictor': predictor_id,'debug' : "true"}
	query.params = params
	scores={}
	try:
		client.scoringonly(query, scores)
	except:
		client = PredictionClient('10.5.1.227', port)
		client.scoringonly(query, scores)

	return scores

def load_cp_gmp_ids(path,id_list,cp_dict,cp_selected_num):
	file = open(path)
	doc_count = 0 
	for line in file:
		cp_json = json.loads(line)
		contend_id = str(cp_json['content_id'])
		publish_time = int(cp_json['publish_time'])
		gmp = float(cp_json['gmp_coec'])
		categories = cp_json['categories']
		keywords = cp_json['keywords']
		tmp_list = [publish_time,gmp,categories,keywords]
		cp_dict[contend_id] = tmp_list
		id_list.append(contend_id)
		doc_count += 1
		if cp_selected_num > 0 and (doc_count >= cp_selected_num):
			break
	file.close()
	return 

def load_up(path,user_num):
	up_list=[]
	load_up_count = 1
	file = open(up_file)
	for line in file:
		try:
			up = json.loads(line)
			user_kw = up['keywords']
			user_ctgy = up['categories']
			up_id = up['uid']
		except:
			continue
		if load_up_count > user_num:
			break
		up_list.append(line)
		load_up_count +=1
	file.close()
	return up_list

def tuple2list(list,postion):
	return [any[postion] for any in list]

def doc_scoring( up_json,cp_dict,up_count,w_gmp,w_kw,w_cat,w_decayfactor):
	
	up = json.loads(up_json)
	user_kw = up['keywords']
	user_ctgy = up['categories']
	up_id = up['uid']


	#phase2 scoring
	host_ind = int(up_count % len(host_list))
	score_2_dict = phase2_score(id_list,up_id,user_ctgy,user_kw,up['city'],up['gender'],up['age'],up['u_cat_len'],up['u_kw_len'],pid,predictor_id,host_list[host_ind])
	score_2_list_nodecay = [(k,v) for k,v in score_2_dict.items() ]

	#phase1 original score list 
	score_1_list = []
	doc_count = 0
	
	logger.info("Input Documents : %d, Valid Documents : %d " % (len(id_list),len(score_2_list_nodecay)))
	logger.info("Phase 2 : %d documents scoring completed for user %s" % (len(score_2_list_nodecay),up_count))



	#for each article
	score_2_list = []
	for contend_id, score_p2 in score_2_list_nodecay:

		#docage decay
		timestamp = int(time.time())
		publish_time = cp_dict[contend_id][0]
		age_hour = float(timestamp - int(publish_time))/3600.0
		age_hour = 0 if age_hour < 0 else age_hour				

		#phase2 scoring decay
		age_hour_num = int(age_hour / 12) * 12
		score_2_list.append((contend_id,score_p2 * math.exp(age_hour_num*phase2_decayfactor)))


		#phase1 scoring
		score_gmp = cp_dict[contend_id][1]
		score_ctgy = cal_ctgy(cp_dict[contend_id][2],user_ctgy)
		score_kw = cal_kw(cp_dict[contend_id][3],user_kw)

		score_phase1 = w_gmp * score_gmp + w_kw * score_kw + w_cat * score_ctgy	
		score_1 = score_phase1 * math.exp(w_decayfactor*age_hour)
		score_1_list.append((contend_id,score_1))

		doc_count += 1
		#if doc_count % 5000 == 0:
		#	logger.info("Phase 1 : %d documents scoring completed for user %s" % (doc_count,up_count))

	logger.info("Phase 1 - Original_Phase1 : %d documents scoring completed for user %s" % (doc_count,up_count))

	#pearsonr
	# score_1_values = tuple2list(score_1_list,1)
	# score_2_values = tuple2list(score_2_list,1)

	#pearsonr_p1p2 = pearsonr(score_1_values,score_2_values)
	#spearmanr
	#score_1_rank = rankdata(score_1_values)
	#score_2_rank = rankdata(score_2_values)
	#spearmanr_p1p2 = spearmanr(score_1_rank,score_2_rank)
	
	#recall rate
	score_1_values = tuple2list(score_1_list,1)
	score_2_values = tuple2list(score_2_list,1)
	score_1_top = [x[0] for x in sorted(enumerate(score_1_values),key=lambda y:y[1],reverse=True)][0:Original_Phase1_num]
	score_2_top = [x[0] for x in sorted(enumerate(score_2_values),key=lambda y:y[1],reverse=True)][0:phase2_doc_num]
	recall_original_phase1 = len(set(score_1_top)&set(score_2_top))

	
	recall_dict = {}
	

	
	logger.info("CALCULATATION COMPLETED FOR ONE USER")
	#logger.info("recall_rate: {},pearson correlation: {},spearman correlation: {} ".format(float(recall)/phase2_doc_num,pearsonr_p1p2[0],spearmanr_p1p2[0]))
	
	recall_dict['Original_Phase1'] = recall_original_phase1

	print recall_dict
	return recall_dict
	#return (float(recall)/phase2_doc_num,pearsonr_p1p2[0],spearmanr_p1p2[0])

def do_line(process_arg_list):
	up_json,cp_dict,up_count,w_gmp,w_kw,w_cat,w_decayfactor = process_arg_list
	return doc_scoring(up_json,cp_dict,up_count,w_gmp,w_kw,w_cat,w_decayfactor)

if __name__ == '__main__':

	#logging
	console_handler = logging.StreamHandler()
	console_handler.setFormatter(logging.Formatter('%(asctime)s\t%(levelname)s\t%(message)s'))
	logger = logging.getLogger(__name__)
	logger.addHandler(console_handler)
	logger.setLevel(logging.DEBUG)

	#configs
	up_file = 'up_sample_100.data'
	#pid = 11
	#predictor_id = 'insta_news_hi_p2_upv4_20160716'
	host_list = ['10.5.1.227','10.5.1.225','10.5.2.85','10.5.2.86','10.5.1.27','10.5.1.28','10.5.2.33','10.5.2.36']
	doc_index_list = ['10.5.1.194','10.5.1.193','10.5.1.159']
	#multi_thread = False
	# phase1_doc_num = 1000
	# up_selected_num = 3
	# cp_selected_num = 2000 # 0 means unlimited

	#parameters
	arg_parser = argparse.ArgumentParser()
	arg_parser.add_argument('-c','--categories',action='store',dest='w_cat',help='categories')
	arg_parser.add_argument('-k','--keywords',action='store',dest='w_kw',help='keywords')
	arg_parser.add_argument('-g','--gmp',action='store',dest='w_gmp',help='gmp')
	arg_parser.add_argument('-d','--decayfactor',action='store',dest='w_decayfactor',help='decayfactor')
	arg_parser.add_argument('-z','--phase2_decayfactor',action='store',dest='phase2_decayfactor',help='phase2_decayfactor')
	arg_parser.add_argument('-p','--para',action='store',dest='para_name',help='para_name')
	arg_parser.add_argument('-s','--start',action='store',dest='start',help='start')
	arg_parser.add_argument('-e','--end',action='store',dest='end',help='end')
	arg_parser.add_argument('-t','--step',action='store',dest='step',help='step')
	arg_parser.add_argument('-u','--user_num',action='store',dest='user_num',help='user_num')
	arg_parser.add_argument('-w','--doc_num',action='store',dest='doc_num',help='doc_num')
	arg_parser.add_argument('-v','--phase1_trunc',action='store',dest='phase1_trunc',help='phase1_trunc')
	arg_parser.add_argument('-b','--phase2_trunc',action='store',dest='phase2_trunc',help='phase2_trunc')
	arg_parser.add_argument('-m','--multi_thread',action='store_true',dest='multi_thread',default=False,help='multi_thread')
	arg_parser.add_argument('-i','--predictor_id',action='store',dest='predictor_id',help='predictor_id')
	arg_parser.add_argument('-n','--pid',action='store',dest='pid',help='pid')


	args = arg_parser.parse_args()
	w_cat = float(args.w_cat)
	w_kw = float(args.w_kw)
	w_gmp = float(args.w_gmp)
	w_decayfactor = float(args.w_decayfactor)
	phase2_decayfactor = float(args.phase2_decayfactor)
	para_name = args.para_name
	para_val = float(args.start)
	para_max = float(args.end)
	para_step = float(args.step)
	user_num = int(args.user_num)
	doc_num = int(args.doc_num)
	phase1_doc_num = int(args.phase1_trunc)
	phase2_doc_num = int(args.phase2_trunc)
	multi_thread = args.multi_thread
	predictor_id = str(args.predictor_id)
	pid = int(args.pid) 
	

	Original_Phase1_num = phase1_doc_num
	
	cp_selected_num = doc_num
	up_selected_num = user_num

	#cp_gmp_ids
	id_list = []
	cp_dict = {}
	cp_path = 'cp_gmp_ids.data'
	load_cp_gmp_ids(cp_path,id_list,cp_dict,cp_selected_num)

	#up
	up_list = load_up(up_file,user_num)

	#setup table 
	table = PrettyTable()
	
	table_phase1_recall = "Phase1_Recall(top%d)" % Original_Phase1_num
	table_head = ['Phase2_Top', table_phase1_recall]
	table_head += ['phase1_decay','phase2_decay','-----------------','GMP','Keywords','category']
	table.add_column('ITEM',table_head)
	#scoring
	while para_val <= para_max:
		
		#set parameter
		if para_name == "g":
			w_gmp = para_val
		elif para_name == "c":
			w_cat = para_val
		elif para_name == "k":
			w_kw = para_val
		elif para_name == "d":
			w_decayfactor = para_val
		elif para_name == "d2":
			phase2_decayfactor = para_val
		logger.info("PARAMETERS: GMP %s KW %s CAT %s P1_AGE_DECAY %s P2_AGE_DECAY %s" % (w_gmp,w_kw,w_cat,w_decayfactor,phase2_decayfactor))	

		# for each user
		up_count = 0

		if multi_thread == True:
			
			num_workers = 10
			pool = Pool(num_workers)

			up_result = []
			for up_json in up_list:
				up_count += 1
				up_result.append(pool.apply_async(doc_scoring,(up_json,cp_dict,up_count,w_gmp,w_kw,w_cat,w_decayfactor)))

			pool.close()
			pool.join()
		else:
			up_result =[]
			for up_json in up_list:
				up_count += 1
				up_result.append(doc_scoring(up_json,cp_dict,up_count,w_gmp,w_kw,w_cat,w_decayfactor))

		mean_res = {}
		for any in up_result:
			if any is not None:
				if multi_thread == True:
					any_res = any.get()
				else:
					any_res = any

				
				for k,v in any_res.items():
					if k in mean_res:
						mean_res[k] += v 
					else:
						mean_res[k] = v

		for k,v in mean_res.items():
			mean_res[k] = v/float(up_count)

		print mean_res

		value_to_add = [phase2_doc_num,mean_res['Original_Phase1']]
		value_to_add += [w_decayfactor,phase2_decayfactor, '--------',w_gmp,w_kw,w_cat]

		table.add_column('VALUE',value_to_add)

		para_val += para_step
	
	#new user
	# para_val = float(args.start)
	# while para_val <= para_max:
	# 	#set parameter
	# 	if para_name == "g":
	# 		w_gmp = para_val
	# 	elif para_name == "c":
	# 		w_cat = para_val
	# 	elif para_name == "k":
	# 		w_kw = para_val
	# 	elif para_name == "d":
	# 		w_decayfactor = para_val

	# 	logger.info("PARAMETERS: GMP %s KW %s CAT %s AGE_DECAY %s" % (w_gmp,w_kw,w_cat,w_decayfactor))	
	# 	up_new = '{"aid" : "test_new", "uid" : "test_new", "version" : "304", "gender" : "", "age" : "2", "u_cat_len" : 0, "u_kw_len" : 0,  "categories" : [], "keywords" : [], "prefer_hindi" : "", "new_user" : "1", "city" : "", "neg_categories" : ""}'
	# 	up_new_result = doc_scoring(selected_feature,up_new,cp_dict,1,w_gmp,w_kw,w_cat,w_decayfactor)
	# 	table.add_row(['new',w_gmp,w_kw,w_cat,w_decayfactor,up_new_result[0],up_new_result[1],up_new_result[2]])


	# 	para_val += para_step

	print table
