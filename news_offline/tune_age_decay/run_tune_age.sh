#product setting
pid=14
predictor_id='nr_en_us_p2_20160818_alluser_no_aid' #'en_us_exp3'

#default parameter vale
cat_weight=0.15
kw_weight=0.5
gmp_weight=1
decay_factor=-0.06 #-0.045
phase2_decayfactor=-0.007
#parameter to adjust
para='d2' #c k g d 
start=-0.01
end=-0.01
step=0.5

#doc and user number
doc_num=0 #0 means unlimited
user_num=10
phase1_trunc=1000
phase2_trunc=100
#selected_feature_num="{Original_Phase1:500,DNN_C_D_U_TITLEMD5_CATEGORY_MACRO_MEAN_COEC:500,DNN_C_D_U_CATEGORY_UID_MICRO_MEAN_COEC:500}"
#,DNN_C_D_U_CATEGORY_UID_MICRO_MEAN_COEC:100,DNN_D_TITLEMD5_COEC:100}"
#selected_feature: you can put any feature here, just combine them with star key *
#selected_feature="C_D_U_TITLEMD5_CATEGORY_MICRO_MEAN_COEC*C_D_U_CATEGORY_UID_MICRO_MEAN_COEC*D_TITLEMD5_COEC"

export PATH=/data/guozhenyuan/anaconda/bin:$PATH
#python phase12_scoring.py -c $cat_weight -k $kw_weight -g $gmp_weight -d $decay_factor -p $para -s $start -e $end -t $step
python tune_age.py -m -n $pid -i $predictor_id -c $cat_weight -k $kw_weight -g $gmp_weight -d $decay_factor -p $para -s $start -e $end -t $step -u $user_num -w $doc_num -v $phase1_trunc -b $phase2_trunc -z $phase2_decayfactor