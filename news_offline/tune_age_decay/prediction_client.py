#!/usr/bin/env python
# encoding=utf-8
from thrift_gen.scoring import ScoringService
from thrift_gen.scoring.ttypes import *
from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
import traceback
import sys 
reload(sys)  
sys.setdefaultencoding('utf-8') 
class PredictionClient:
    def __init__(self, host, port):
        try:
            self.host = host
            self.port = port
            socket = TSocket.TSocket(host,int(port))
            self.transport = TTransport.TFramedTransport(socket)
            protocol = TBinaryProtocol.TBinaryProtocol(self.transport)
            self.client = ScoringService.Client(protocol)
            self.transport.open()
        except:
            self.client = None
            self.transport = None
            traceback.print_exc()

    def scoring(self,query, scores, selected_feature):

        result = self.client.scoring(query)
        for entity in result.documents:
            scores[entity.doc_id] = [entity.score]
            for feature in selected_feature:
                scores[entity.doc_id].append(entity.ranking_scores.get(feature,0))

    def scoringonly(self,query, scores):

        result = self.client.scoring(query)
        for entity in result.documents:
            scores[entity.doc_id] = entity.score
                
    def __del__(self):
        self.transport.close()

if __name__ == '__main__':
    host = '10.5.1.227'
    port = '31000'
    client = PredictionClient(host, port)
    doc_ids = ['001f53057F7_in']
    query = Query()
    query.request_id = 'xxxxxxxx'
    user_profile = UserProfile()
    user_profile.userid = '6a59c406877d7'
    user_profile.categories = {'1000606':1.0}
    user_profile.keywords = {"आज_का_दिन_कैसा_रहेगा":0.0285681,"एक_कहानी_यह_भी":0.0219415,"कन्या":0.0220887,"कृपा":0.128068,"जीवन_साथी":0.0403487,"दोस्ती":0.0290098,"धन":0.0240031,"पेट":0.0254756,"पैसा":0.023414,"भगवान_विष्णु":0.212293,"मन":0.027979,"मनोकामना":0.101531,"महाभारत":0.0322495,"राजनीति":0.0260647,"राशिफल":0.138864,"विवाह":0.0257702,"वृषभ":0.0214997,"संघर्ष":0.0235613,"समय":0.0244448,"सिंह":0.022825}
    user_profile.city = ''
    user_profile.gender = 0
    user_profile.age = 0
    user_profile.cat_len = -1
    user_profile.kw_len = 2
    query.user_profile = user_profile
    query.doc_ids = doc_ids 
    query.product_id = '11'
    params = {
        'predictor' : "hindi_exp14",
        'debug' : "true"
    }
    query.params = params

    scores = {}
    client.scoring(query, scores)

    print scores
