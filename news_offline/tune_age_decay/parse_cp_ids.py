import json
import urllib2
import argparse


if __name__ == '__main__':

	arg_parser = argparse.ArgumentParser()
	arg_parser.add_argument('-c','--country',action='store',dest='country',help='country')
	arg_parser.add_argument('-p','--pid_binary',action='store',dest='pid_binary',help='pid_binary')

	args = arg_parser.parse_args()
	country = args.country
	pid_binary = args.pid_binary

	url_doc_id = 'http://10.5.1.193:8080/?action=filter_docs&doc_age=96&index_flag='
	url_doc_id += str(pid_binary)

	resp_cp_id = urllib2.urlopen(url_doc_id)
	cp_list_raw = json.loads(resp_cp_id.read())['filter_docs']

	with open('cp_ids.data','w') as f:
		for item in cp_list_raw:
			if item[-2:] == country:
				f.write("{}\n".format(item))

	print "cp_ids successfully dumped into local file"