### gen_train_data用于生成phase2训练xgboost模型的training data.

主入口为 run_gen_training_data_all.sh 
配置文件为 configs/configure_hi_all-20160321.sh.
前者起job, 后者为参数配置

run_gen_training_data_all.sh 分为如下部分（在文件开头有开关，可以选择性执行如下一步或几步)：

1.generate_cmnews_cfb_raw_data.pig

    up + cp + rcv(其中包含click和pv的event) => 过去4天的raw_cfb，
    raw_cfb结构如下：ts,feature_type,feature_name_app_lan,click_sum(一个batch中click的和,i.e.10 mins),view_sum.

2.generate_cmnews_merged_cfb.pig

    2.1 up + cp + rcv =>（rid,uid,contnet_id,pid，...(other features)...feature_type,feature_name,click_sum,view_sum）

    2.2 4天raw_cfb(from Step 1) --> age_decay---> 1天的age_decaged raw_cfb

    2.3 (2.1) + (2.2) => merged cfb

3.get_raw_train_data.pig
    merged cfb(from Step 2) + declare interest => raw_train_data
    raw_train_data 结构如下： 
    
    id相关：           uid,contentid,label,dwelltime,ts
        
    static feature:   doc_age,time_day,day_week,declare_rel,u_gender,u_age,kw_rel,cat_rel ....
        
    cfb feature:      各类feature Click,PV, COEC....


4.split_training_data.pig
    
    use udf.get_random_bin to split train data

5.format_xgboost_train_data.pig
    
    format train_data into libsvm format

### 以上默认language=en, 若需生成hindi训练数据,需提前处理up:
run_gen_hindi_up.sh



### 评测模型：
run_gen_eval_models.sh <train data config> <model.dump> <*4>
如：./run_gen_eval_models.sh configure_hi_all-20160424-20160502.sh modeld4.tar.gz *4