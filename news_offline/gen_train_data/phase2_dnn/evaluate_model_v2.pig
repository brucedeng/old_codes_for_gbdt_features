REGISTER '../lib/*.jar';
REGISTER '../lib/feature_udf.py' USING jython AS myudf;
REGISTER '../lib/coke_udf.py' using jython as coke_udf;
-- DEFINE PROC_ALL_METRIC `get_all_metric.py` SHIP('./lib/get_all_metric.py');

--set mapred.cache.archives $modelfiles#models
--define EVAL com.ijinshan.cmcm.gbdt.runtime.udf.GbdtRuntimeMapAllModel('models');
--set mapred.cache.files $featmap_file#feature.map;

DEFINE calc_evaluate ( data, join_key ) RETURNS metrics {
    event_score = foreach $data generate model_name, uid, iid, (action == 1?'c':'v') as action, score;

    G = group event_score by (model_name, uid)  parallel 40;

    user_group = foreach G {
        c = filter event_score by action == 'c';
        o = order event_score by score DESC;
        l = limit o 3;
        top_item = foreach l generate uid, iid;
        generate FLATTEN(group) as (model_name,uid), event_score, COUNT(c) as pos_num, COUNT(event_score) as total_cnt, top_item;
    };
    /*per user*/
    G = filter user_group by pos_num >0;
    UserScores = foreach G generate model_name, uid, 
            (double)coke_udf.metric_mrr(event_score.(action, score)) as score_mrr,
            (double)coke_udf.metric_map(event_score.(action, score)) as score_map,
            (double)coke_udf.metric_auc(event_score.(action, score)) as score_auc;
    Gall  = group UserScores by model_name;
    O_peruser  = foreach Gall generate $0 as model_name, 'all' as key, AVG(UserScores.score_mrr) as score_mrr, AVG(UserScores.score_map) as score_map, AVG(UserScores.score_auc) as score_auc;
    /*overall*/
    -- event_score = foreach event_score generate
    --     model_name,
    --     score,
    --     1 as view:int,
    --     (action=='c'?1:0) as click;

    -- G = group event_score ALL;

    -- sorted_score_action = foreach G {
    --     D = order $1 by score desc;
    --     generate
    --     flatten(D);
    -- };

    -- O = stream sorted_score_action through PROC_ALL_METRIC as (current_view, current_click, view_click_mrr, view_click_map, view_click_roc_area, view_click_pr_area, view_click_total_logLoss);

    -- O_overall = foreach O generate 'all' as key,current_view, current_click, view_click_roc_area, view_click_pr_area, view_click_total_logLoss;


    /*Diversity metric*/
    user_group = filter user_group by total_cnt >= 10;
    rec_items = foreach user_group generate model_name, flatten(top_item) as (uid, iid);

    uids = foreach rec_items generate uid;
    d_uids = distinct uids;
    total_user = foreach (group d_uids all) generate COUNT(d_uids) as user_cnt;

    iids = foreach rec_items generate model_name,iid;
    d_iids = distinct iids;
    total_item = foreach (group d_iids by model_name) generate $0 as model_name,COUNT(d_iids) as item_cnt;

    rec_count = foreach (group rec_items by (model_name,iid) PARALLEL 100) {
        rec_uid = foreach rec_items generate uid;
        d = distinct rec_uid;
        generate FLATTEN($0) as (model_name,iid), COUNT(d) as rec_cnt, total_user.user_cnt as cnt;
    };

    rec_jnd = join rec_count by model_name, total_item by model_name;

    rec_metric = FOREACH rec_jnd GENERATE rec_count::model_name as model_name, flatten(coke_udf.diversity_metric(rec_count::rec_cnt,rec_count::cnt,total_item::item_cnt)) as (entropy_div, gini_div, herfindahl_div);

    O_diversity = foreach (group rec_metric by model_name) generate $0 as model_name, 'all' as key, SUM($1.entropy_div) as entropy_div, 1 - SUM($1.herfindahl_div) as herfindahl_div;

    $metrics = FOREACH ( JOIN O_peruser by (model_name,key), O_diversity by (model_name,key) ) GENERATE
        O_peruser::model_name as model_name,
        '$join_key' as key,
 
        O_peruser::score_mrr as score_mrr,
        O_peruser::score_map as score_map,
        O_peruser::score_auc as score_auc,
        O_diversity::entropy_div as entropy_div,
        O_diversity::herfindahl_div as herfindahl_div;
}

raw_training_data = LOAD '$INPUT' as (uid:chararray, content_id:chararray, label:int, score:float);

eval_data = FOREACH raw_training_data GENERATE uid, content_id as iid, label as action, score, '1' as model_name, 100 as u_kw_len;

-- store eval_data into '/projects/news/model/tmp/model_pred_data' using org.apache.pig.piggybank.storage.MultiStorage('/projects/news/model/tmp/model_pred_data','4','none','\t');

-- result = Stream data THROUGH SCORING as (uid:chararray, iid:chararray, action:int, score:float);

overall_metric = calc_evaluate(eval_data, 'all');

rmf $output_metric;
store overall_metric into '$output_metric';
