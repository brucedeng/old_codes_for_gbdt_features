REGISTER '../lib/*.jar';
REGISTER '../lib/feature_udf.py' USING jython AS myudf;
REGISTER '../lib/coke_udf.py' using jython as coke_udf;
-- DEFINE PROC_ALL_METRIC `get_all_metric.py` SHIP('./lib/get_all_metric.py');

set mapred.cache.archives $modelfiles#models
define EVAL com.ijinshan.cmcm.gbdt.runtime.udf.GbdtRuntimeMapAllModel('models');
set mapred.cache.files $featmap_file#feature.map;

raw_training_data = LOAD '$INPUT' as (uid:chararray, content_id:chararray, label:int, dwelltime:float,
    train_data:chararray, ts:long);

raw_training_data_eval = FOREACH raw_training_data GENERATE uid, content_id, label, dwelltime,
    myudf.parse_libsvm(train_data) as train_data;

eval_data = FOREACH raw_training_data_eval GENERATE uid,content_id, label, dwelltime, train_data#'U_KW_LEN' as u_kw_len, FLATTEN(EVAL(train_data)) as (model_name, score);

result = FOREACH eval_data GENERATE label as action, score as score;

rmf $output_metric;
store result into '$output_metric';
