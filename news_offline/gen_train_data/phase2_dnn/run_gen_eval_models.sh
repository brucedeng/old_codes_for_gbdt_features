
train_config_file=$1
source $train_config_file

if [[ $# -le 3 ]]; then
    echo  "usage: $0 config_file model_file"
fi

model_file="$2"
if [[ "a$model_file" == "a" ]]; then
    echo "usage: $0 config_file model_file"
    exit 1
fi

test_file_bin="$3"

echo $event_start
echo $event_end

set -e
# test if all needed variables are assigned.
test_var="${PIG_PARAM?} ${TOP_PARAM?}"

test_var="${output_s3_path?}"

ftr_start=$event_start
ftr_end=$event_end
model_filename=`basename $model_file`

model_dump=${output_root?}/models_dump/$event_start-$event_end-$model_filename
hadoop fs -mkdir -p ${output_root?}/models_dump
#hadoop fs -copyFromLocal $model_file $model_dump

data_path="${output_s3_path}/split_events_$ftr_start-$ftr_end"
output_path="${data_path}.eval"

cmd="pig $PIG_PARAM -p INPUT='$data_path/$test_file_bin' -p output_metric='$output_path' -p modelfiles=$model_dump -p featmap_file=$featmap_file_hdfs evaluate_model.pig"
echo "$cmd";
eval $cmd
