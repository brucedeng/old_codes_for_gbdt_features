# basic settings
#event_start=20160704;
#event_start=20160620;
#event_end=20160714;
train_sample_rate=1.0;
app_lan=hi
batch_dedup=true
cfb_ignore_date=20160101

# run processes
run_raw_cfb=true
run_merge_cfb=true
#run_featmap=true
#run_training_data=true
#run_split_data=true
#run_format_data=true

# pig params
queue=deeplearning
#queue=offline
PIG_PARAM=" -Dmapred.job.queue.name=$queue";
TOP_PARAM="-p U_NCAT=5 -p U_NKEY=25 -p D_NCAT=5 -p D_NKEY=15 -p WND=600"

# cached files
local_config=config_dnn.json
local_city=../lib/ip_city.data
local_category=../lib/cat_tree.txt
config_file="hdfs://mycluster/projects/news/deeplearning/model/configs/config_dnn.json"
city_file="hdfs://mycluster/projects/news/deeplearning/model/configs/ip_city_dummy.data"
category_data="hdfs://mycluster/projects/news/deeplearning/model/configs/cat_tree.txt"

# data pathes
if [[ "$app_lan" == "hi" ]]; then
    input_user_root="/projects/news/deeplearning/model/hindi_up/"
else
    input_user_root="/projects/news/deeplearning/data/V3/up_json"
fi
input_interest_root="s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall"
input_cp_template="/projects/news/deeplearning/model/experiments/merged_cp_multiple_days/%s"
kw_split=10000000
# output path
output_root="/projects/news/deeplearning/model/training"
# raw_cfb_template="$output_root/raw_cfb/${app_lan}_offline_cfb_raw_new/%s"
raw_cfb_template="$output_root/raw_cfb/${app_lan}_offline_cfb_test/%s.bz2"
merged_cfb_root="$output_root/merged_cfb/${app_lan}_feature_aggcfb_test"
#output_s3_path="$output_root/experiments/${app_lan}_offlinecfb_training_data_test";
output_s3_path="s3://com.cmcm.instanews.usw2.prod/user_profile/model/experiments/${app_lan}_offlinecfb_training_data_test";
output_s3_path_dfs="$output_root/experiments/${app_lan}_offlinecfb_training_data_test";
featmap_path=s3://com.cmcm.instanews.usw2.prod/user_profile/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}
featmap_file_hdfs=s3://com.cmcm.instanews.usw2.prod/user_profile/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}.txt
