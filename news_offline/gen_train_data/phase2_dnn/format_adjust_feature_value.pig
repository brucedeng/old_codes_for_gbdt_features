REGISTER '../lib/*.jar';
REGISTER '../lib/feature_udf.py' USING jython AS myudf;
 
-- set default_parallel 500;

raw_training_data = LOAD '$INPUT' as (line:chararray);

train_data_weight = FOREACH raw_training_data GENERATE myudf.adjust_feature_value(line) as line;

train_data_weight = Filter train_data_weight by line != '';

--rmf $OUTPUT
--store train_data_weight into '$OUTPUT';

rmf $OUTPUT_S3
store train_data_weight into '$OUTPUT_S3';
