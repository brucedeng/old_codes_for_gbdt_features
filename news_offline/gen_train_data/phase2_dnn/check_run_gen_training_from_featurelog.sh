###configs begin
#join_rcv_featurelog="true"
#run_split_data="true"
run_format_data="true"
#sample_type="test"
#run_dnn_data="true"
#####join_rcv_featurelog
event_start=20160621
event_end=20160623
#event_end=20160614
#queue_name=offline
queue_name=experiment
expid='*'
####generate splited data
kw_split=-1;  #keyword number split sample sample bin
train_sample_rate=1.0;
app_lan=hi
PIG_PARAM=" -Dmapred.job.queue.name=$queue_name";
TOP_PARAM="-p U_NCAT=5 -p U_NKEY=25 -p D_NCAT=5 -p D_NKEY=15 -p WND=600"
cfb_pid1=11
cfb_pid2=null
local_config=config_less.json
local_category=../lib/cat_tree.txt
#config_file="hdfs://mycluster/projects/news/model/configs/config_less.json"
#category_data="hdfs://mycluster/projects/news/model/configs/cat_tree.txt"

# data pathes
#output_s3_path="/projects/news/model/experiments/dengkun/training_data_from_featurelog";
output_s3_path="s3://com.cmcm.instanews.usw2.prod/model/dengkun/dnn/training_data_from_featurelog_gz";
output_dnn_s3_path="s3://com.cmcm.instanews.usw2.prod/model/dengkun/tmp/training_data_from_featurelog_gz";
#featmap_path=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}
featmap_file_hdfs=s3://com.cmcm.instanews.usw2.prod/model/experiments/dengkun/en_cfb_training_featmap/${event_start}-${event_end}.txt

####configs end

if [[ "$join_rcv_featurelog" == "true" ]]; then
    PIG_PARAM="-Dmapred.job.queue.name=$queue_name -Dmapred.max.map.failures.percent=5";


    echo $event_start
    echo $event_end
    cur_date=$event_start
    echo $cur_date

    rm -f pig*.log

    hadoop fs -copyFromLocal featmap.txt $featmap_file_hdfs
    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do

        input_event="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click,listpagedwelltime,readtime}/$cur_date/*/*"
        feature_log="s3://com.cmcm.instanews.usw2.prod/model/feature_log/$cur_date/*/*"
        #input_event="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click,listpagedwelltime,readtime}/$cur_date/12/10"
        #feature_log="s3://com.cmcm.instanews.usw2.prod/model/feature_log/$cur_date/12/*"
        prev_date=`date -d "$cur_date -1 days" +%Y%m%d`
        #feature_log="s3://com.cmcm.instanews.usw2.prod/model/feature_log/{$cur_date,$prev_date}/*/*"

       # input_interest="s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall/$cur_date"
       # input_interest="s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall/20160603"
        output="${output_s3_path}/${cur_date}.gz";

        cmd="pig $PIG_PARAM $TOP_PARAM -p featmap_file_hdfs=$featmap_file_hdfs -p cfb_pid1=$cfb_pid1 -p cfb_pid2=$cfb_pid2 -p app_lan=$app_lan -p INPUT_EVENT='$input_event' -p INPUT_FEATURE_LOG='$feature_log' -p exp='$expid' -p OUTPUT='$output' join_rcv_featurelog.pig"
        echo $cmd
        eval $cmd;

        if [ $? != 0 ]; then echo "ERROR: Failed to run pipeline!"; exit -1; fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done

fi


set -e
# test if all needed variables are assigned.
test_var="${PIG_PARAM?} ${TOP_PARAM?}"

if [[ "$run_split_data" == "true" ]]; then
    test_var="${output_s3_path?} ${kw_split?}"

    ftr_start=$event_start
    ftr_end=$event_end
    agg_dates=$ftr_start
    for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done

    input_path="${output_s3_path}/{$agg_dates}.gz"
    output_path="${output_s3_path}/split_events_$ftr_start-$ftr_end"
    if [[ a"$split_seed" == "a" ]];then
        split_seed=$RANDOM
    fi
    echo "spliting using seed $split_seed"

    cmd="pig $PIG_PARAM -p SEED='$split_seed' -p KW_SPLIT='$kw_split' -p INPUT='$input_path' -p OUTPUT='$output_path' split_training_data.pig"
    echo "$cmd";
    eval $cmd
fi

if [[ "$run_format_data" == "true" ]]; then
    test_var="${output_s3_path?} ${event_end?} ${event_start?}"

    echo "formatting for all users"
    train_path="$output_s3_path/split_events_$event_start-$event_end/*_{0,1,2,3}"
    test_path="$output_s3_path/split_events_$event_start-$event_end/*_4"
    output_path="$output_s3_path/split_events_$event_start-$event_end-libsvm-test"
    null_path=/projects/news/model/experiments/dengkun/training_data_from_featurelog/20130620
    if [[ "$sample_type" == "train" ]]; then
        train_path="$output_s3_path/split_events_$event_start-$event_end"
        test_path=$null_path
        output_path="$output_s3_path/events_$event_start-$event_end-libsvm"
    elif [[ "$sample_type" == "test" ]]; then
        train_path=$null_path
        test_path="$output_s3_path/split_events_$event_start-$event_end"
        output_path="$output_s3_path/events_$event_start-$event_end-libsvm"
    else
        echo "split data for train and test"
    fi
    output_cnn_test_path="$output_dnn_s3_path/events_$event_start-$event_end-libsvm-adjust-with-uid-iid/"
    cmd="pig $PIG_PARAM -p train='$train_path' -p test='$test_path' -p OUTPUT='${output_path}.check' -p OUTPUT_DNN='${output_cnn_test_path}.check' test_format_xgboost_train_data.pig"
    echo "$cmd";
    eval $cmd
fi


if [[ "$run_dnn_data" == "true" ]]; then
    echo "adjust feature value for dnn train"
    input_data="$output_s3_path/events_$event_start-$event_end-libsvm/$sample_type"
    output_data_s3="$output_dnn_s3_path/events_$event_start-$event_end-libsvm/$sample_type.gz"
    output_data="$output_s3_path/events_$event_start-$event_end-libsvm-adjust/$sample_type"
    cmd="pig $PIG_PARAM -p INPUT='$input_data' -p OUTPUT='$output_data' -p OUTPUT_S3='$output_data_s3' format_adjust_feature_value.pig"
    echo $cmd
    eval $cmd
fi
