
set -e

queue=deeplearning
#queue=offline
PIG_PARAM=" -Dmapred.job.queue.name=$queue";

s3_path=s3://com.cmcm.instanews.usw2.prod/user_profile/model/experiments/hi_training_data_from_featurelog_newformat/tmp

data_path="${s3_path}/output_score_file_withuidcid"
output_path="${s3_path}/metric_score"

cmd="pig $PIG_PARAM -p INPUT='$data_path' -p output_metric='$output_path'  evaluate_model_v2.pig"
echo "$cmd";
eval $cmd
