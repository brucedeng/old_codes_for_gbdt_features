#!/bin/bash
source ./daily_training_from_featurelog.conf
join_rcv_featurelog="true"
gen_singleday_tr_te_data="true"
rmr_history_data="true"
#run_split_data="true"
#run_format_data="true"
#sample_type="test"
#run_dnn_data="true"
#####join_rcv_featurelog
prenum=$1
if [ -z $prenum ]; then
    prenum=1
fi
event_start=`date -d "$prenum days ago" +%Y%m%d`
event_end=$event_start
echo "start day: "$event_start"  end day: "$event_end

#event_end=20160614
#queue_name=offline
queue_name=deeplearning
expid='*'
####generate splited data
kw_split=-1;  #keyword number split sample sample bin
train_sample_rate=1.0;
app_lan=hi
PIG_PARAM=" -Dmapred.job.queue.name=$queue_name";
TOP_PARAM="-p U_NCAT=5 -p U_NKEY=25 -p D_NCAT=5 -p D_NKEY=15 -p WND=600"
cfb_pid1=11
cfb_pid2=null
local_config=config_less.json
local_category=../lib/cat_tree.txt
job_name='dengkun_hi_dnn_train_data'
#config_file="hdfs://mycluster/projects/news/model/configs/config_less.json"
#category_data="hdfs://mycluster/projects/news/model/configs/cat_tree.txt"

# data pathes
#output_s3_path="/projects/news/model/experiments/dengkun/training_data_from_featurelog";
output_s3_path="/projects/news/deeplearning/model/experiments/dengkun/training_data_from_featurelog";
#output_s3_path="s3://com.cmcm.instanews.usw2.prod/model/dengkun/dnn/training_data_from_featurelog_gz";
#output_s3_path="s3://com.cmcm.instanews.usw2.prod/user_profile/dengkun/dnn/training_data_from_featurelog_gz";
#output_dnn_s3_path="s3://com.cmcm.instanews.usw2.prod/model/dengkun/tmp/training_data_from_featurelog_gz";
output_dnn_path="/projects/news/deeplearning/model/experiments/hi_training_data_from_featurelog";
output_dnn_s3_path="s3://com.cmcm.instanews.usw2.prod/user_profile/model/experiments/hi_training_data_from_featurelog_newformat";
#featmap_path=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}
featmap_file_hdfs=s3://com.cmcm.instanews.usw2.prod/user_profile/experiments/dengkun/en_cfb_training_featmap/${event_start}-${event_end}.txt

###check input log

fun_check_log() {
    input_flag=$1
    ret=1
    try_time=0
    while [ $ret -ne 0 ]
    do
        if [ $try_time -ge 30 ];then
            sh -x $SEND_MESSAGE_DIR/send_message.sh $telephone "701" "$input_flag not ready"
            try_time=0
            ##exit -1
        fi
        hadoop fs -ls $input_flag
        ret=$?
        if [ $ret -ne 0 ]
        then
            sleep 180
        fi
        let ++try_time
    done
}

####configs end

if [[ "$join_rcv_featurelog" == "true" ]]; then
    PIG_PARAM="-Dmapred.job.queue.name=$queue_name -Dmapred.max.map.failures.percent=5";


    echo $event_start
    echo $event_end
    cur_date=$event_start
    echo $cur_date

    rm -f pig*.log

    hadoop fs -copyFromLocal featmap.txt $featmap_file_hdfs
    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do

        input_event="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click,listpagedwelltime,readtime}/$cur_date/*/*"
        feature_log="s3://com.cmcm.instanews.usw2.prod/model/feature_log/$cur_date/*/*"
        #feature_log="s3://com.cmcm.instanews.usw2.prod/model/feature_log_v2/hi_in/$cur_date/*/*"
        fun_check_log s3://com.cmcm.instanews.usw2.prod/data/raw_data/impression/$cur_date/23/50/_SUCCESS
        fun_check_log s3://com.cmcm.instanews.usw2.prod/data/raw_data/click/$cur_date/23/50/_SUCCESS
        #fun_check_log s3://com.cmcm.instanews.usw2.prod/data/raw_data/listpagedwelltime/$cur_date/23/50/_SUCCESS
        fun_check_log s3://com.cmcm.instanews.usw2.prod/data/raw_data/readtime/$cur_date/23/50/_SUCCESS
        fun_check_log s3://com.cmcm.instanews.usw2.prod/model/feature_log/$cur_date/23/55/
        #input_event="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click,listpagedwelltime,readtime}/$cur_date/12/10"
        #feature_log="s3://com.cmcm.instanews.usw2.prod/model/feature_log/$cur_date/12/*"
        prev_date=`date -d "$cur_date -1 days" +%Y%m%d`
        #feature_log="s3://com.cmcm.instanews.usw2.prod/model/feature_log/{$cur_date,$prev_date}/*/*"

       # input_interest="s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall/$cur_date"
       # input_interest="s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall/20160603"
        #output="${output_s3_path}/${cur_date}.gz";
        output="${output_s3_path}/${cur_date}";
        monitor_label="${output_dnn_s3_path}/${cur_date}/monitor_label_before_join"

        cmd="pig $PIG_PARAM $TOP_PARAM -p job_name='${job_name}_join_rcv_featurelog.pig' -p featmap_file_hdfs=$featmap_file_hdfs -p cfb_pid1=$cfb_pid1 -p cfb_pid2=$cfb_pid2 -p app_lan=$app_lan -p INPUT_EVENT='$input_event' -p INPUT_FEATURE_LOG='$feature_log' -p exp='$expid' -p monitor_label='$monitor_label' -p OUTPUT='$output' join_rcv_featurelog.pig"
        echo $cmd
        eval $cmd;

        if [ $? != 0 ]; then 
            echo "ERROR: Failed to run pipeline!";
            sh -x $SEND_MESSAGE_DIR/send_message.sh $telephone "701" "ERROR: join feature job failed. day $cur_date"
            exit -1;
        fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done

fi



if [[ "$gen_singleday_tr_te_data" == "true" ]]; then
    PIG_PARAM="-Dmapred.job.queue.name=$queue_name -Dmapred.max.map.failures.percent=5";

    echo $event_start
    echo $event_end
    cur_date=$event_start
    echo $cur_date

    rm -f pig*.log

    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do

        input_event="${output_s3_path}/${cur_date}";
        output="${output_dnn_path}/${cur_date}/train.gz";
        output_s3="${output_dnn_s3_path}/${cur_date}/train.gz";

        cmd="pig $PIG_PARAM $TOP_PARAM -p job_name='${job_name}_singleday_training_data.pig' -p INPUT='$input_event' -p OUTPUT='$output' singleday_training_data.pig"
        echo $cmd
        eval $cmd;
        hadoop fs -rmr $output/_SUCCESS
        if [ $? != 0 ]; then
             echo "ERROR: Failed to run pipeline! generate singleday training data failed"; 
             sh -x $SEND_MESSAGE_DIR/send_message.sh $telephone "701" "ERROR: generate singleday featurelog training data failed. day: $cur_date."
             exit -1;
        fi

        cmd="hadoop fs -rmr -skipTrash $output_s3"
        echo $cmd
        eval $cmd;

        cmd="hadoop distcp  -m 40 -bandwidth 15 $output $output_s3"
        echo $cmd
        eval $cmd;
        if [ $? != 0 ]; then
            echo "ERROR: Failed to cp training data to s3!";
            sh -x $SEND_MESSAGE_DIR/send_message.sh $telephone "701" "ERROR: copy featurelog training data to s3 failed day: $cur_date."
            exit -1;
        fi
        hadoop fs -touchz $output/_SUCCESS
        hadoop fs -touchz $output_s3/_SUCCESS

        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done

fi


if [[ "$rmr_history_data" == "true" ]]; then
    history_day=`date -d "15 days ago $event_start" +%Y%m%d`
    echo $history_day
    if [[ a"${history_day}" != "a" ]]; then
        hdfs_history_data=${output_s3_path}/${history_day}
        hadoop fs -rmr $hdfs_history_data

        hdfs_history_data=${output_dnn_path}/${history_day}
        hadoop fs -rmr ${hdfs_history_data}
    fi

fi

#set -e
## test if all needed variables are assigned.
#test_var="${PIG_PARAM?} ${TOP_PARAM?}"
#
#if [[ "$run_split_data" == "true" ]]; then
#    test_var="${output_s3_path?} ${kw_split?}"
#
#    ftr_start=$event_start
#    ftr_end=$event_end
#    agg_dates=$ftr_start
#    for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done
#
#    #input_path="${output_s3_path}/{$agg_dates}.gz"
#    input_path="${output_s3_path}/{$agg_dates}"
#    output_path="${output_s3_path}/split_events_$ftr_start-$ftr_end"
#    if [[ a"$split_seed" == "a" ]];then
#        split_seed=$RANDOM
#    fi
#    echo "spliting using seed $split_seed"
#
#    cmd="pig $PIG_PARAM -p SEED='$split_seed' -p KW_SPLIT='$kw_split' -p INPUT='$input_path' -p OUTPUT='$output_path' split_training_data.pig"
#    echo "$cmd";
#    eval $cmd
#fi
#
#if [[ "$run_format_data" == "true" ]]; then
#    test_var="${output_s3_path?} ${event_end?} ${event_start?}"
#
#    echo "formatting for all users"
#    train_path="$output_s3_path/split_events_$event_start-$event_end/*_{0,1,2,3}"
#    test_path="$output_s3_path/split_events_$event_start-$event_end/*_4"
#    output_path="$output_s3_path/split_events_$event_start-$event_end-libsvm-test"
#    null_path=/projects/news/deeplearning/experiments/dengkun/training_data_from_featurelog/20130620
#    if [[ "$sample_type" == "train" ]]; then
#        train_path="$output_s3_path/split_events_$event_start-$event_end"
#        test_path=$null_path
#        output_path="$output_s3_path/events_$event_start-$event_end-libsvm"
#    elif [[ "$sample_type" == "test" ]]; then
#        train_path=$null_path
#        test_path="$output_s3_path/split_events_$event_start-$event_end"
#        output_path="$output_s3_path/events_$event_start-$event_end-libsvm"
#    else
#        echo "split data for train and test"
#    fi
#    output_cnn_test_path="$output_dnn_s3_path/events_$event_start-$event_end-libsvm-adjust-with-uid-iid/"
#    cmd="pig $PIG_PARAM -p train='$train_path' -p test='$test_path' -p OUTPUT='$output_path' -p OUTPUT_DNN='$output_cnn_test_path' format_xgboost_train_data.pig"
#    echo "$cmd";
#    eval $cmd
#fi
#
#
#if [[ "$run_dnn_data" == "true" ]]; then
#    echo "adjust feature value for dnn train"
#    input_data="$output_s3_path/events_$event_start-$event_end-libsvm/$sample_type"
#    output_data_s3="$output_dnn_s3_path/events_$event_start-$event_end-libsvm/$sample_type.gz"
#    output_data="$output_s3_path/events_$event_start-$event_end-libsvm-adjust/$sample_type"
#    cmd="pig $PIG_PARAM -p INPUT='$input_data' -p OUTPUT='$output_data' -p OUTPUT_S3='$output_data_s3' format_adjust_feature_value.pig"
#    echo $cmd
#    eval $cmd
#fi
