
%default input1 /projects/news/model/training/en_in_train_data_20160601-20160722/libsvm2/daily_data/20160728
%default input2 /projects/news/model/experiments/training_data_from_featurelog/test_en_in
%default output /tmp/news_model/compare_trainng_data_featurelog2

data1 = load '$input1' as (uid:chararray,conid:chararray,label:chararray, dwell:float, train_data:chararray);
data2 = load '$input2' as (uid:chararray,conid:chararray,label:chararray, dwell:float, train_data:chararray,ts:long, rid:chararray);

jnd = FOREACH ( JOIN data1 by (uid,conid) , data2 by (uid,conid) ) GENERATE 
    (data1::uid is not null? data1::uid : data2::uid) as uid,
    (data1::conid is not null? data1::conid : data2::conid) as conid,
    data1::label as label1,
    data2::label as label2,
    data1::dwell as dwell1,
    data2::dwell as dwell2,
    data1::train_data as train1,
    data2::train_data as train2,
    data2::ts as ts,
    data2::rid as rid;

store jnd into '$output';
