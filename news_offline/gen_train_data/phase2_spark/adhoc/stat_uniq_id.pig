event_input = load '/projects/news/model/training/en_us_train_data_20160801_20160814/formatted_cfb_events/20160802' as (uniq_id:chararray,events:chararray);

event_grp = GROUP event_input by uniq_id;

store event_grp into '/tmp/news_model/uniq_id_stat';