import sys
if __name__=='__main__':

	input_file = sys.argv[1]
	event_list = []
	event_map = {}
	start_ts = 14697465540000
	end_ts=0

	with open(input_file) as f:
		for line in f:
			fields = line.split('\t')
			ts = long(fields[7])
			act = 1
			if fields[0].find('act#1') >=0:
				act = 1
			elif fields[0].find('act#2') >=0:
				act = 2
			ts_event = ts/600*600
			if ts_event < start_ts:
				start_ts = ts_event
			elif ts_event > end_ts:
				end_ts = ts_event
			event_map[ts_event] = event_map.get(ts_event,[0.0,0.0])
			if act == 1:
				event_map[ts_event][0]+=1
			if act == 2:
				event_map[ts_event][1]+=1

	agg = None
	for ts in xrange(start_ts,end_ts+600,600):
		if ts in event_map:
			if agg is None:
				agg = event_map[ts] + [ts]
				
			else:
				tmp_agg = event_map[ts]
				decay = 0.995 ** ((ts - agg[2])/600)
				agg[0] = agg[0]*decay + tmp_agg[0]
				agg[1] = agg[1]*decay + tmp_agg[1]
				agg[2] = ts
			print '\t'.join([str(k) for k in agg])

