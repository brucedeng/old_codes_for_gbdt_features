
-- %default input1 /projects/news/model/training/merged_cfb/hi_feature_aggcfb_test/20160629/
-- %default input2 /projects/news/model/training/merged_cfb/hi_feature_aggcfb_new_user_up_v4/20160629/events_with_cfb_features
%default input1 /projects/news/model/training/en_in_train_data_20160601-20160722/merged_cfb/20160728/
%default input2 /projects/news/model/training/en_in_train_data_20160601-20160722/merged_cfb2/20160728/
%default output /tmp/news_model/compare_merge_cfb_data

-- %DECLARE PIG_COL_TYPE "id:chararray,uid:chararray,content_id:chararray,ts:long,label:chararray,city:chararray,publisher:chararray,cp_update_time:chararray,d_raw:chararray,u_raw:chararray,cp_type:chararray,publish_time:chararray,u_age:chararray,u_gender:chararray,cfb_json:chararray,dwelltime:float,listpagedwell:float,declare_match:int,declare_cnt:int"
-- %DECLARE PIG_COL "id,uid,content_id,ts,label,city,publisher,cp_update_time,d_raw,u_raw,cp_type,publish_time,u_age,u_gender,cfb_json,dwelltime,listpagedwell,declare_match,declare_cnt"
%DECLARE PIG_COL_TYPE "id:chararray,uid:chararray,content_id:chararray,ts:long,dwell:float,pid:int,region:chararray,dummy:float,label:chararray,city:chararray,cat_len:int,kw_len:int,cat_rel:float,kw_rel:float,word_count:int,image_count:int, newsy_score:float,docage:float,time_of_day:float,day_of_week:float,cfb_json:chararray"
%DECLARE PIG_COL "id,uid,content_id,ts,dwell,pid,region,dummy,label,city,cat_len,kw_len,cat_rel,kw_rel,word_count,image_count, newsy_score,docage,time_of_day,day_of_week,cfb_json"


-- data1 = load '$input1' as (fields:chararray,cfb:chararray);
data1 = load '$input1' as (${PIG_COL_TYPE});
data2 = load '$input2' as (${PIG_COL_TYPE});

-- data1 = FOREACH data1 GENERATE FLATTEN(STRSPLIT(fields,'\u0001',5)) as (id:chararray,uid:chararray,content_id:chararray,join_ts:chararray), cfb;
data1 = FOREACH data1 GENERATE uid, content_id, ts, cfb_json;
data2 = FOREACH data2 GENERATE uid, content_id, ts, cfb_json;
jnd = JOIN data1 by (uid,content_id) full outer, data2 by (uid,content_id);

rmf $output
store jnd into '$output';
