REGISTER '../../lib/*.jar';
REGISTER '../../lib/utils.py' USING jython AS myfunc;

raw_rcv_log = LOAD '$event' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate
    json,
    (json#'app_lan' is null ? 'en' : ( json#'app_lan' MATCHES 'hi.*' ? 'hi' : json#'app_lan' )) as app_lan,
    (json#'country' is null? '' : (chararray)(json#'country')) as country:chararray,
    json#'contentid' as content_id,
    json#'aid' as uid,
    json#'ip' as ip,
    json#'servertime_sec' as eventtime,
    json#'pid' as pid,
    json#'ctype' as ctype,
    json#'act' as act,
    json#'city' as city,
    myfunc.parse_rid(json#'cpack') as rid:chararray,
    (int)json#'ext'#'dwelltime' AS dwelltime:int;

raw_rcv_log = FOREACH raw_rcv_log GENERATE json,app_lan,country, CONCAT(app_lan, CONCAT('_', country)) as lan_region, content_id, uid, ip, eventtime, pid, ctype, act, city, rid, dwelltime;

raw_rcv_log = filter raw_rcv_log by act is not null and pid=='$pid' and ( ctype is null or ctype=='' or ctype=='1' or ctype=='0x01' ) and ( act == '1' or act == '2' or act == '4') and ( json#'scenario'#'level1_type' =='1' or json#'scenario'#'level1_type' =='10' ) and json#'scenario'#'level1' == '1' and INDEXOF('$lan_region',LOWER(lan_region),0) >=0;

rcv_filtered = FILTER raw_rcv_log by content_id == 'Nf1b87416LP_in';
rcv_filtered = ORDER rcv_filtered BY eventtime;
store rcv_filtered into '/tmp/news_model/rcv_filtered';
