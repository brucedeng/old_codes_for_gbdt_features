
%default input1 /projects/news/model/training/en_us_train_data_20160807_20160813_nr_en_us_aid_position/libsvm/daily_data/20160813
%default input2 /projects/news/model/training/en_us_train_data_20160801_20160814/libsvm/daily_data/20160813
%default output /tmp/news_model/compare_trainng_data

data1 = load '$input1' as (uid:chararray,conid:chararray,label:chararray, dwell:float, train_data:chararray);
data2 = load '$input2' as (uid:chararray,conid:chararray,label:chararray, dwell:float, train_data:chararray);

jnd = FOREACH ( JOIN data1 by (uid,conid) full outer, data2 by (uid,conid) ) GENERATE 
    (data1::uid is not null? data1::uid : data2::uid) as uid,
    (data1::conid is not null? data1::conid : data2::conid) as conid,
    data1::label as label1,
    data2::label as label2,
    data1::dwell as dwell1,
    data2::dwell as dwell2,
    data1::train_data as train1,
    data2::train_data as train2;

rmf $output
store jnd into '$output';