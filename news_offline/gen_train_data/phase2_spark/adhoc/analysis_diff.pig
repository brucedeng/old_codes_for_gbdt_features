a = load '/tmp/news_model/compare_spark_raw_cfb/diff/' as (ts:chararray, ftype:chararray, fname:chararray, diffclk:float, diffpv:float, clk1:float,pv1:float, clk2:float, pv2:float);

filtered_event = filter a by diffclk<-0.001 or diffclk>0.001 or diffpv < -0.001 or diffpv > 0.001;

event_sorted = order filtered_event by diffpv;
store event_sorted into '/tmp/news_model/diff_analysis';

