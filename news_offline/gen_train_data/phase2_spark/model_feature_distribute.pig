
DEFINE STAT `cut -f5 | python distribute_features.py '$fid'` ship('../lib/distribute_features.py');

raw_training_data = load '$input' as (uid:chararray, content_id:chararray, label:int, dwelltime:float, train_data:chararray);

training_data_format = FOREACH raw_training_data GENERATE train_data;

training_stat = STREAM training_data_format THROUGH STAT as (fvalue:float,count:int);
result_grp = FOREACH ( GROUP training_stat by fvalue ) GENERATE $0 as fvalue, SUM($1.count) as count;
result_ord = ORDER result_grp BY fvalue;

rmf $output
store result_ord into '$output';
