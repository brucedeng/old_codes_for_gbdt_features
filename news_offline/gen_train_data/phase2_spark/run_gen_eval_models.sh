
featmap_file_hdfs=$1

if [[ $# -le 3 ]]; then
    echo  "usage: $0 featmap_hdfs model_hdfs data_hdfs"
fi

set -e
# test if all needed variables are assigned.

# model_filename=`basename $model_file`

model_dump="$2"

data_path="$3"
output_path=`echo "${data_path}.eval" | sed 's/[{}]//g' | tr ',' '_'`

PIG_PARAM="-Dmapreduce.job.queuename=experiment"
cmd="pig $PIG_PARAM -p INPUT='$data_path' -p output_metric='$output_path' -p modelfiles=$model_dump -p featmap_file=$featmap_file_hdfs evaluate_model.pig"
echo "$cmd";
eval $cmd
