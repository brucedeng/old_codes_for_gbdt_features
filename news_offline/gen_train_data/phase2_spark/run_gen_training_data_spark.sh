
train_config_file=$1
source $train_config_file

echo $event_start
echo $event_end

set -e
# test if all needed variables are assigned.
test_var="${PIG_PARAM?} ${TOP_PARAM?} ${APP_NAME?}"

if [[ "$run_merge_cp" == "true" ]]; then
    # test if all needed variables are assigned.
    test_var="${input_cp_daily_template?} ${merged_cp_path?}"

    cur_date=`date -d "+7 day ago $event_start" +%Y%m%d`;

    ftr_start=$cur_date
    ftr_end=$event_end;
    agg_dates=$ftr_start
    for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done
    input_contents=`printf "$input_cp_daily_template" "{$agg_dates}"`

    output="/projects/news/model/experiments/merged_cp_multiple_days/$event_start-$event_end"
    cmd="pig $PIG_PARAM -p job_name=${APP_NAME}_merge_cp -p cp_dump='$input_contents' -p output='$merged_cp_path' gen_merged_cp.pig"
    echo "$cmd"
    eval $cmd
    if [ $? != 0 ]; then echo "ERROR: Failed to run merge cp!"; exit -1; fi
fi

if [[ "$run_format_raw_cfb" == "true" ]]; then
    # test if all needed variables are assigned.
    test_var="${format_rawcfb_event?} ${lan_region?} ${cfb_filter_condition?}"

    cur_date=`date -d "+3 day ago $event_start" +%Y%m%d`;

    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do
        if [[ "a$cfb_ignore_date" != "a" ]]; then
            if [[ $cfb_ignore_date -gt $cur_date ]]; then
                cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
                continue
            fi
        fi

        input_event="/projects/news/data/raw_data/{impression,click,comment,completeness,dislike,praise,readtime,search,share}/$cur_date/*/*"
        output=`printf "$format_rawcfb_event" "$cur_date"`

        method=" -p METHOD=gen_raw_cfb "

        cmd="pig $PIG_PARAM $TOP_PARAM $method -p job_name=${APP_NAME}_raw_cfb_event -p event='$input_event' -p output='$output' -p pid=$product_id -p lan_region=${lan_region} -p FILTER_CONDITION=$cfb_filter_condition generate_format_event_data.pig"
        echo $cmd
        eval $cmd;

        if [ $? != 0 ]; then echo "ERROR: Failed to run format raw cfb!"; exit -1; fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done
fi

if [[ "$run_raw_cfb" == "true" ]]; then
    # test if all needed variables are assigned.
    test_var="${format_rawcfb_event?} ${RAW_SPARK_PARAM?} ${input_user_root?} ${merged_cp_path?} ${raw_cfb_template?}"

    cur_date=`date -d "+3 day ago $event_start" +%Y%m%d`;

    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do
        if [[ "a$cfb_ignore_date" != "a" ]]; then
            if [[ $cfb_ignore_date -gt $cur_date ]]; then
                cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
                continue
            fi
        fi

        # input_event="/projects/news/data/raw_data/{impression,click}/$cur_date/*/*"
        input_format_data=`printf "$format_rawcfb_event" "$cur_date"`

        up_date=`date -d "+1 day ago $cur_date" +%Y%m%d`;
        input_user="$input_user_root/$up_date"
        ftr_start=`date -d "+4 day ago $cur_date" +%Y%m%d`;
        ftr_end=$cur_date;
        if [[ "a$cp_first_date" != "a" ]]; then
            if [[ $cp_first_date -gt $cur_date ]]; then
                ftr_end=$cp_first_date;
            fi
        fi
        agg_dates=$ftr_start
        for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done
        input_contents=`printf "$merged_cp_path" "{$agg_dates}"`

        output=`printf "$raw_cfb_template" "$cur_date"`
        hadoop fs -rm -r $output || echo "$output does not exist"
        date_time="`date -d $cur_date '+%Y-%m-%dT%H:%MZ'`"

        cmd="spark-submit $RAW_SPARK_PARAM --conf spark.app.name=${APP_NAME}_raw_cfb --class com.cmcm.cmnews.model.app.CFBApp content_xfb_pipline-1.0-SNAPSHOT.jar $SPARK_TOP_PARAM event_input '$input_format_data' content_input '$input_contents' user_input '$input_user' xfb_output '$output' date_time '$date_time' "
        echo "$cmd"
        eval $cmd;

        if [ $? != 0 ]; then echo "ERROR: Failed to run raw cfb!"; exit -1; fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done
fi

if [[ "$run_format_train_event" == "true" ]]; then
    # test if all needed variables are assigned.
    test_var="${format_train_event?} ${lan_region?}"

    cur_date=$event_start

    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do
        if [[ "a$cfb_ignore_date" != "a" ]]; then
            if [[ $cfb_ignore_date -gt $cur_date ]]; then
                cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
                continue
            fi
        fi

        input_event="/projects/news/data/raw_data/{impression,click,readtime,comment,dislike,praise,share}/$cur_date/*/*"
        output=`printf "$format_train_event" "$cur_date"`

        method=" -p METHOD=gen_event "

        cmd="pig $PIG_PARAM $TOP_PARAM $method -p job_name=${APP_NAME}_agg_cfb_event -p event='$input_event' -p output='$output' -p pid=$product_id -p lan_region=${lan_region} -p FILTER_CONDITION=filter_train_event generate_format_event_data.pig"
        echo $cmd
        eval $cmd;

        if [ $? != 0 ]; then echo "ERROR: Failed to run format train event!"; exit -1; fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done
fi

if [[ "$run_train_data" == "true" ]]; then
    # test if all needed variables are assigned.
    test_var="${raw_cfb_template?} ${TRAIN_SPARK_PARAM?} ${input_user_root?} ${merged_cp_path?} ${format_train_event?}"

    # cur_date=`date -d "+2 day ago $event_start" +%Y%m%d`;
    cur_date=$event_start

    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do
        if [[ "a$cfb_ignore_date" != "a" ]]; then
            if [[ $cfb_ignore_date -gt $cur_date ]]; then
                cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
                continue
            fi
        fi

        input_format_data=`printf "$format_train_event" "$cur_date"`

        ftr_start=`date -d "+3 day ago $cur_date" +%Y%m%d`;
        ftr_end=$cur_date;
        agg_dates=$ftr_start
        for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done
        input_raw_cfb=`printf "$raw_cfb_template" "{$agg_dates}"`

        up_date=`date -d "+1 day ago $cur_date" +%Y%m%d`;
        input_user="$input_user_root/$up_date"
        ftr_start=`date -d "+4 day ago $cur_date" +%Y%m%d`;
        ftr_end=$cur_date;
        agg_dates=$ftr_start
        for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done
        input_contents=`printf "$merged_cp_path" "{$agg_dates}"`

        output=$merged_cfb_root/$cur_date
        hadoop fs -rm -r $output || echo "$output does not exist"

        date_time="`date -d "+1 days $cur_date" '+%Y-%m-%dT%H:%MZ'`"
        prev_date_time="`date -d "$cur_date" '+%Y-%m-%dT%H:%MZ'`"

        cmd="spark-submit $TRAIN_SPARK_PARAM --conf spark.app.name=${APP_NAME}_agg_cfb --class com.cmcm.cmnews.model.app.CFBTrainDataApp content_xfb_train_data_pipeline-1.0-SNAPSHOT.jar $SPARK_TOP_PARAM event_input '$input_format_data' content_input '$input_contents' user_input '$input_user' xfb_input '$input_raw_cfb' train_output '$output' date_time '$date_time' prev_data_time '$prev_date_time' "
        echo "$cmd"
        eval $cmd;

        if [ $? != 0 ]; then echo "ERROR: Failed to run merge cfb!"; exit -1; fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done
fi

if [[ "$run_gen_libsvm" == "true" ]]; then
    # test if all needed variables are assigned.
    test_var="${raw_cfb_template?} ${TRAIN_SPARK_PARAM?} ${input_user_root?} ${merged_cp_path?} ${format_train_event?} ${train_libsvm_root?} "

    # cur_date=`date -d "+2 day ago $event_start" +%Y%m%d`;
    cur_date=$event_start

    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do
        input_merge_cfb=$merged_cfb_root/$cur_date
        output=$train_libsvm_root/daily_data/$cur_date
        hadoop fs -rm -r $output || echo "$output does not exist"

        date_time="`date -d "+1 days $cur_date" '+%Y-%m-%dT%H:%MZ'`"
        prev_date_time="`date -d "$cur_date" '+%Y-%m-%dT%H:%MZ'`"

        cmd="spark-submit $FORMAT_SPARK_PARAM --conf spark.app.name=${APP_NAME}_format_data --class com.cmcm.cmnews.model.app.FormatLibsvm content_xfb_train_data_pipeline-1.0-SNAPSHOT.jar $SPARK_TOP_PARAM merge_cfb_input '$input_merge_cfb' libsvm_output '$output' featmap_file featmap.txt "
        echo "$cmd"
        eval $cmd;

        if [ $? != 0 ]; then echo "ERROR: Failed to run gen libsvm!"; exit -1; fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done
fi

if [[ "$run_split_data" == "true" ]]; then
    test_var="${train_libsvm_root?} ${kw_split?}"

    ftr_start=$event_start
    ftr_end=$event_end
    agg_dates=$ftr_start
    for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done

    input_path="${train_libsvm_root}/daily_data/{$agg_dates}"
    output_path="${train_libsvm_root}/split_events_$ftr_start-$ftr_end"
    if [[ a"$split_seed" == "a" ]];then
        split_seed=$RANDOM
    fi
    echo "spliting using seed $split_seed"

    cmd="pig $PIG_PARAM -p SEED='$split_seed' -p job_name=${APP_NAME}_split_data -p KW_SPLIT='$kw_split' -p INPUT='$input_path' -p OUTPUT='$output_path' split_training_data.pig"
    echo "$cmd";
    eval $cmd
    if [ $? != 0 ]; then echo "ERROR: Failed to run split data!"; exit -1; fi
fi

if [[ "$run_format_data" == "true" ]]; then
    test_var="${train_libsvm_root?} ${event_end?} ${event_start?}"

    echo "formatting for all users"
    train_path="$train_libsvm_root/split_events_$event_start-$event_end/*_{0,1,2,3}"
    test_path="$train_libsvm_root/split_events_$event_start-$event_end/*_4"
    output_path="$train_libsvm_root/split_events_$event_start-$event_end-libsvm"
    cmd="pig $PIG_PARAM -p job_name=${APP_NAME}_format_train -p train='$train_path' -p test='$test_path' -p OUTPUT='$output_path' format_xgboost_train_data.pig"
    echo "$cmd";
    eval $cmd

    # echo "formatting for light users"
    # train_path="$train_libsvm_root/split_events_$event_start-$event_end/0_{0,1,2,3}"
    # test_path="$train_libsvm_root/split_events_$event_start-$event_end/0_4"
    # output_path="$train_libsvm_root/split_events_$event_start-$event_end-libsvm_lightuser"
    # cmd="pig $PIG_PARAM -p job_name=${APP_NAME}_format_train -p train='$train_path' -p test='$test_path' -p OUTPUT='$output_path' format_xgboost_train_data.pig"
    # echo "$cmd";
    # eval $cmd

    # echo "formatting for heavy users"
    # train_path="$train_libsvm_root/split_events_$event_start-$event_end/1_{0,1,2,3}"
    # test_path="$train_libsvm_root/split_events_$event_start-$event_end/1_4"
    # output_path="$train_libsvm_root/split_events_$event_start-$event_end-libsvm_heavyuser"
    # cmd="pig $PIG_PARAM -p job_name=${APP_NAME}_format_train -p train='$train_path' -p test='$test_path' -p OUTPUT='$output_path' format_xgboost_train_data.pig"
    # echo "$cmd";
    # eval $cmd
fi

if [[ "$run_format_data_by_day" == "true" ]]; then
    test_var="${train_libsvm_root?} ${event_end?} ${event_start?} ${test_start?}"

    echo "formatting for training testing"
    ftr_start=$event_start
    ftr_end=$test_start
    agg_dates=$ftr_start
    for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day < $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done
    train_path="${train_libsvm_root}/daily_data/{$agg_dates}"

    ftr_start=$test_start
    ftr_end=$event_end
    agg_dates=$ftr_start
    for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done
    test_path="${train_libsvm_root}/daily_data/{$agg_dates}"

    output_path="$train_libsvm_root/split_events_$event_start-$event_end-libsvm"
    cmd="pig $PIG_PARAM -p job_name=${APP_NAME}_format_train -p train='$train_path' -p METHOD=gen_train_weighted -p OUTPUT='$output_path/train' format_xgboost_train_data.pig"
    echo "$cmd";
    eval $cmd
    cmd="pig $PIG_PARAM -p job_name=${APP_NAME}_format_train -p train='$test_path' -p METHOD=gen_train_weighted -p OUTPUT='$output_path/test' format_xgboost_train_data.pig"
    echo "$cmd";
    eval $cmd
fi
