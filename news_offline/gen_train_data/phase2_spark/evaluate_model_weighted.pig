REGISTER '../lib/*.jar';
REGISTER '../lib/feature_udf.py' USING jython AS myudf;
REGISTER '../lib/coke_udf.py' using jython as coke_udf;
DEFINE PROC_ALL_METRIC `python get_mutli_model_metric.py` SHIP('../lib/get_mutli_model_metric.py');

set mapred.cache.archives $modelfiles#models
define EVAL com.ijinshan.cmcm.gbdt.runtime.udf.GbdtRuntimeMapAllModel('models');
set mapred.cache.files $featmap_file#feature.map;

DEFINE calc_evaluate ( data, join_key ) RETURNS metrics {
    event_score = foreach $data generate model_name, uid, iid, (action == 1?'c':'v') as action, score, 1 as weight;

    G = group event_score by (model_name, uid)  parallel 40;
    user_group = foreach G {
        c = filter event_score by action == 'c';
        o = order event_score by score DESC;
        l = limit o 3;
        top_item = foreach l generate uid, iid;
        generate FLATTEN(group) as (model_name,uid), event_score, COUNT(c) as pos_num, COUNT(event_score) as total_cnt, top_item;
    };

    /*per user*/
    G = filter user_group by pos_num >0;
    UserScores = foreach G generate model_name, uid, 
            (double)coke_udf.metric_wmrr(event_score.(action, score, weight)) as score_mrr,
            (double)coke_udf.metric_wmap(event_score.(action, score, weight)) as score_map,
            (double)coke_udf.metric_wauc(event_score.(action, score, weight)) as score_auc;
    Gall  = group UserScores by model_name;
    O_peruser  = foreach Gall generate $0 as model_name, 'all' as key, AVG(UserScores.score_mrr) as score_mrr, AVG(UserScores.score_map) as score_map, AVG(UserScores.score_auc) as score_auc;
   
    /*Diversity metric*/
    user_group = filter user_group by total_cnt >= 10;
    rec_items = foreach user_group generate model_name, flatten(top_item) as (uid, iid);

    uids = foreach rec_items generate uid;
    d_uids = distinct uids;
    total_user = foreach (group d_uids all) generate COUNT(d_uids) as user_cnt;

    iids = foreach rec_items generate model_name,iid;
    d_iids = distinct iids;
    total_item = foreach (group d_iids by model_name) generate $0 as model_name,COUNT(d_iids) as item_cnt;

    rec_count = foreach (group rec_items by (model_name,iid) PARALLEL 100) {
        rec_uid = foreach rec_items generate uid;
        d = distinct rec_uid;
        generate FLATTEN($0) as (model_name,iid), COUNT(d) as rec_cnt, total_user.user_cnt as cnt;
    };

    rec_jnd = join rec_count by model_name, total_item by model_name;

    rec_metric = FOREACH rec_jnd GENERATE rec_count::model_name as model_name, flatten(coke_udf.diversity_metric(rec_count::rec_cnt,rec_count::cnt,total_item::item_cnt)) as (entropy_div, gini_div, herfindahl_div);

    O_diversity = foreach (group rec_metric by model_name) generate $0 as model_name, 'all' as key, SUM($1.entropy_div) as entropy_div, 1 - SUM($1.herfindahl_div) as herfindahl_div;

    /*overall*/
    event_score_all = foreach event_score generate
         model_name,
         weight as view:float,
         (action=='c'?weight:0) as click:float,
         (int)(score*10000.0) as bin,
         weight;

     G = foreach (group event_score_all by (model_name, bin) PARALLEL 200){
            
            generate flatten(group) as (model_name, bin), SUM(event_score_all.view) as view, SUM(event_score_all.click) as click, 1 as weight; 
         
         };

    G_all = group G all;

    sorted_score_action = foreach G_all {
         D = order $1 by bin desc;
         generate
         flatten(D);
    };
    sorted_score_action = foreach sorted_score_action generate model_name, view, click, bin, weight;  

    --rmf /tmp/chenkehan/sorted_score_action;
    store sorted_score_action into '/tmp/chenkehan/sorted_score_action';

    O = stream sorted_score_action through PROC_ALL_METRIC as (model_name, current_view, current_click, view_click_roc_area, view_click_pr_area, view_click_total_logLoss);

    O_overall = foreach O generate model_name, '$join_key' as key, current_view, current_click, view_click_roc_area, view_click_pr_area, view_click_total_logLoss;
    --store O_overall into '/tmp/chenkehan/overall';


    $metrics = FOREACH ( JOIN O_overall by (model_name, key), O_peruser by (model_name,key), O_diversity by (model_name,key) ) GENERATE
        O_peruser::model_name as model_name,
        '$join_key' as key,
        O_overall::view_click_roc_area as overall_roc,
        O_overall::view_click_pr_area as overall_pr,
        O_peruser::score_mrr as score_mrr,
        O_peruser::score_map as score_map,
        O_peruser::score_auc as score_auc,
        O_diversity::entropy_div as entropy_div,
        O_diversity::herfindahl_div as herfindahl_div;
}

raw_training_data = LOAD '$INPUT' as (uid:chararray, content_id:chararray, label:int, dwelltime:float,
    train_data:chararray, ts:long, bin:chararray, weight:float);

raw_training_data_eval = FOREACH raw_training_data GENERATE uid, content_id, label, dwelltime,
    myudf.parse_libsvm(train_data) as train_data, weight;

eval_data = FOREACH raw_training_data_eval GENERATE uid,content_id, label, dwelltime, train_data#'U_KW_LEN' as u_kw_len, FLATTEN(EVAL(train_data)) as (model_name, score), weight;

eval_data = FOREACH eval_data GENERATE uid, content_id as iid, label as action, score as score, weight, model_name,  u_kw_len;

--rmf /tmp/chenkehan/model_pred_data;
--store eval_data into '/tmp/chenkehan/model_pred_data' using org.apache.pig.piggybank.storage.MultiStorage('/projects/news/model/tmp/model_pred_data','5','none','\t');

-- result = Stream data THROUGH SCORING as (uid:chararray, iid:chararray, action:int, score:float);
overall_metric = calc_evaluate(eval_data, 'all');
result = order overall_metric by key, model_name;
/*
new_user = FILTER eval_data by u_kw_len is null or u_kw_len <=0;
new_user_metric = calc_evaluate(new_user,'new_user');

light_user = FILTER eval_data by u_kw_len is not null and u_kw_len <=300;
light_user_metric = calc_evaluate(light_user,'light_user');

heavy_user = FILTER eval_data by u_kw_len is not null and u_kw_len > 300;
heavy_user_metric = calc_evaluate(heavy_user,'heavy_user');

result = UNION overall_metric,new_user_metric, light_user_metric, heavy_user_metric;
result = order result by key, model_name;
*/

rmf $output_metric;
store result into '$output_metric';



