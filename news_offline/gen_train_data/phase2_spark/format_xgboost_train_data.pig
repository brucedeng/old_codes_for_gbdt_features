REGISTER '../lib/*.jar';
REGISTER '../lib/feature_udf.py' USING jython AS myudf;
 
-- set default_parallel 500;
set job.name '$job_name';

DEFINE gen_train_weighted (events) RETURNS weighted_data {
    train_data_weight = FOREACH $events GENERATE uid,content_id, label, LOG(dwelltime+1) as wt, train_data;

    train_data_with_weight = FOREACH train_data_weight GENERATE uid, content_id,label, (label ==1? wt : 1 ) as weight, train_data as train_data;

    $weighted_data = FOREACH train_data_with_weight GENERATE myudf.add_libsvm_weight(train_data,weight) as train_data;
}

DEFINE gen_train_no_weight (events) RETURNS no_weight_data {
    $no_weight_data = FOREACH $events GENERATE train_data;
}

raw_training_data = LOAD '$train' as (uid:chararray, content_id:chararray, label:int, dwelltime:float,
    train_data:chararray, ts:long, bin:chararray);

libsvm_data = $METHOD(raw_training_data);

rmf $OUTPUT
store libsvm_data into '$OUTPUT';
