# basic settings
event_start=20160629;
event_end=20160705;
train_sample_rate=1.0;
app_lan=hi
product_id=11

# cfb_ignore_date=20160629

APP_NAME=mengchong

# run processes
# run_merge_cp=true
# run_format_raw_cfb=true
# run_raw_cfb=true
# run_format_train_event=true
# run_train_data=true
# run_gen_libsvm=true
# run_split_data=true
run_format_data=true

# pig params
queue=experiment
# queue=offline

config_file=cfb_features.conf
PIG_PARAM=" -Dmapreduce.job.queuename=$queue ";
# RAW_SPARK_PARAM=" --conf spark.rpc.askTimeout=300 --conf spark.task.maxFailures=100 --conf spark.yarn.executor.memoryOverhead=1024 --conf spark.yarn.max.executor.failures=200 --conf spark.memory.storageFraction=0.6 --master yarn --deploy-mode cluster --driver-memory 1g --executor-memory 2g --executor-cores 3 --num-executors 30 --queue $queue "
RAW_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 6g --num-executors 30 --queue $queue "
TRAIN_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 10g --num-executors 60 --queue $queue "
FORMAT_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 2g --num-executors 60 --queue $queue "
TOP_PARAM=" -p WND=600 "

SPARK_TOP_PARAM=" parallelism 800 pid 11 wnd 600 u_ncat 5 u_nkey 25 d_ncat 5 d_nkey 15 cat_version oversea_l2 kw_version oversea rel_u_ncat 200 rel_u_nkey 200 rel_d_ncat 200 rel_d_nkey 200 decay_factor 0.995 decay_wind 2d top_num 1000 random_num 1000 config_file $config_file l1_norm yes "

# data pathes
if [[ "$app_lan" == "hi" ]]; then
    input_user_root="/projects/news/model/hindi_up_v4"
else
    input_user_root="/projects/news/user_profile/data/V3/up_json"
fi

input_cp_daily_template="/projects/news/cpp/feeder/in_cp_dump/%s/*/*"
merged_cp_path="/projects/news/model/experiments/merged_cp_multiple_days/20160624-20160705"
kw_split=0
split_seed=1000

# input_event_template="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click}/$cur_date/*/*"

# output path
output_root="/projects/news/model/training"

format_rawcfb_event="$output_root/formatted_events/raw_cfb_20160624-20160705/%s"
format_train_event="$output_root/formatted_events/train_data_20160624-20160705/%s"
raw_cfb_template="$output_root/raw_cfb/${app_lan}_offline_cfb_test/%s"
merged_cfb_root="$output_root/merged_cfb/${app_lan}_feature_aggcfb_test"
train_libsvm_root="$output_root/libsvm/${app_lan}_training_data"

# raw_cfb_template="$output_root/raw_cfb/${app_lan}_offline_cfb_raw_new/%s"

featmap_path=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}
featmap_file_hdfs=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}.txt
