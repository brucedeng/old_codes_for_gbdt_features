# basic settings
event_start=20160713;
event_end=20160716;
test_start=20160716;
train_sample_rate=1.0;
app_lan=hi
product_id=11
lan_region=hi_in

# cfb_ignore_date=20160721

APP_NAME=mengchong

# run processes
# run_merge_cp=true
# run_format_raw_cfb=true
run_raw_cfb=true
# run_format_train_event=true
# run_train_data=true
# run_gen_libsvm=true
# run_split_data=true
# run_format_data=true
# run_format_data_by_day=true

# pig params
queue=experiment
# queue=offline

config_file=cfb_features.conf
PIG_PARAM=" -Dmapreduce.job.queuename=$queue ";

RAW_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 6g --num-executors 30 --queue $queue "
TRAIN_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 10g --num-executors 80 --queue $queue "
FORMAT_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 2g --num-executors 60 --queue $queue "
TOP_PARAM=" -p WND=600 "

SPARK_TOP_PARAM=" parallelism 800 wnd 600 u_ncat 5 u_nkey 25 d_ncat 5 d_nkey 15 cat_version oversea_l2 kw_version oversea rel_u_ncat 200 rel_u_nkey 200 rel_d_ncat 200 rel_d_nkey 200 decay_factor 0.995 decay_wind 2d top_num 1000 random_num 1000 config_file $config_file l1_norm yes multi_lan no"

input_user_root="/projects/news/user_profile/data/V4/up_json/hi_in"

input_cp_daily_template="/projects/news/cpp/feeder/in_cp_dump/%s/*/*"
merged_cp_path="/projects/news/model/experiments/merged_cp_multiple_days/${event_start}-20160727"
kw_split=0
split_seed=1000

# input_event_template="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click}/$cur_date/*/*"

output_lan=`echo $lan_region | tr ',' '-'`
# output path
output_root="/projects/news/model/training/${output_lan}_train_data_20160601-20160722"

format_rawcfb_event="$output_root/formatted_rawcfb_events/%s"
format_train_event="$output_root/formatted_cfb_events/%s"
raw_cfb_template="$output_root/raw_cfb/%s"
merged_cfb_root="$output_root/merged_cfb"
train_libsvm_root="$output_root/libsvm"

# raw_cfb_template="$output_root/raw_cfb/${app_lan}_offline_cfb_raw_new/%s"

# featmap_path=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}
featmap_file_hdfs=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}.txt
