
%default input hdfs://mycluster/projects/news/model/training/hi_train_data_20160615-20160715/libsvm/split_events_20160615-20160715-libsvm/train

a = load '$input' as (data:chararray);

b = STREAM a THROUGH `cut -f 1 -d ' ' | tr ':' '\t'` as (label:chararray,weight:float);

event_grp = FOREACH ( GROUP b by label ) GENERATE $0 as label,
    COUNT($1) as cnt,
    SUM($1.weight) as wt;

store event_grp into '/tmp/news_model/statistic_weight';
