package monoid

import scala.collection.immutable.HashMap

trait Monoid[A] {
  def op(a1: A, a2: A): A
  def zero: A
}

object Monoid {
  def mapMergeMonoid[K, V](V: Monoid[V]): Monoid[HashMap[K, V]] = new Monoid[HashMap[K, V]] {
    def zero: HashMap[K, V] = HashMap.empty
    def op(a: HashMap[K, V], b: HashMap[K, V]): HashMap[K, V] = {
      val c = a.map {
        case (k, v) => (k, V.op(v, b.get(k) getOrElse V.zero))
      }
      b ++ c
    }
  }

  def floatAddition: Monoid[Float] = new Monoid[Float] {
    def zero: Float = 0.0f
    def op(a: Float, b: Float): Float = a + b
  }
}
