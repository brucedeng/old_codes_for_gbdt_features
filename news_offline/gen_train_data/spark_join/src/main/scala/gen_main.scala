package data_gen

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{SQLContext, Row}

import scala.collection.immutable._

object Main {
  val argRegex = "--([^=\\s]*)=([^=\\s]*)".r
  def parserArgs(args: Array[String]): HashMap[String, String] =
    args.foldLeft(HashMap.empty[String, String])((y, x) => x match {
      case argRegex(key, value) => y + (key -> value)
      case other => throw new Error(s"illegal input $other")
    })


  def main(args: Array[String]) {
    val argMaps                 = parserArgs(args)
    println(argMaps.toString)
    val featMapFileHdfs         = argMaps("FeatMapFileHDFS")
    val cfbPid1                 = argMaps("CfbPid1")
    val cfbPid2                 = argMaps("CfbPid2")
    val paramAppLan             = argMaps("AppLang")
    val inputEvent              = argMaps("InputEvent")
    val inputFeatureLog         = argMaps("featureLog")
    val exp                     = argMaps("Exp")
    val output                  = argMaps("Output")

    val reserved_keys_path      = argMaps("reserved_keys")
    val feature_logger_map_path = argMaps("feature_logger_map")

    val conf = new SparkConf().setAppName("gen feature data")
    val sc   = new SparkContext(conf)
    val hql  = new HiveContext(sc)

    val reserved_keys      = reserved_keys_path.split(",").toSet
    val maps: Array[String] = sc.textFile(feature_logger_map_path).collect
    val feature_logger_map = Util.parseMap(Util.hmapRegex)(maps(0))
    val featureMaps        =
      sc.textFile(featMapFileHdfs)
        .collect
        .foldLeft(HashMap.empty[String, Int])((y, x) => {
          val terms = x.split("\\s")
          y + (terms(1) -> terms(0).toInt)
        })

    dat_gen.featmap_file_hdfs = featMapFileHdfs
    dat_gen.cfb_pid1          = cfbPid1
    dat_gen.cfb_pid2          = cfbPid2
    dat_gen.param_app_lan     = paramAppLan 
    dat_gen.input_event       = inputEvent
    dat_gen.input_feature_log = inputFeatureLog
    dat_gen.exp               = exp
    dat_gen.output            = output

    val text = sc.textFile(inputEvent)

    def genRCVFeature(text: RDD[String]):
        HashMap[dat_gen.STupleKey, dat_gen.STupleVal] = {
      val xraw_rcv_log = Util.time("raw_rcv_log") { dat_gen.raw_rcv_log(sc, hql, text) }
      val xlog_click_view = Util.time("log_click_view") { dat_gen.log_click_view(hql, xraw_rcv_log) }
      Util.time("raw_event_ap") { dat_gen.raw_event_map(hql, xlog_click_view) }
    }

    val fLog = sc.textFile(inputFeatureLog)

    def genFeatureLog(fLog: RDD[String]): DataFrame = {
      val xfeature_log = Util.time("feature_log") { dat_gen.feature_log(hql, sc, fLog) }
      Util.time("feature_log_dist") { dat_gen.feature_log_dist(hql)(reserved_keys, feature_logger_map)(xfeature_log) }
    }

    def MergeAll(rcvF: HashMap[dat_gen.STupleKey, dat_gen.STupleVal], fLogF: DataFrame): Unit = {
      Util.time("merge all") {
        dat_gen.fastJoinAll(sc)(featureMaps)(rcvF, fLogF).map(_.mkString(","))
             .coalesce(1000, shuffle=true).saveAsTextFile(output)
      }
    }

    MergeAll(genRCVFeature(text), genFeatureLog(fLog))
    sc.stop()
  }
}

object Test {
  val argRegex = "--([^=\\s]*)=([^=\\s]*)".r
  def parserArgs(args: Array[String]): HashMap[String, String] =
    args.foldLeft(HashMap.empty[String, String])((y, x) => x match {
      case argRegex(key, value) => y + (key -> value)
      case other => throw new Error(s"illegal input $other")
    })


  def main(args: Array[String]) {
    val argMaps                 = parserArgs(args)
    println(argMaps.toString)
    val featMapFileHdfs         = argMaps("FeatMapFileHDFS")
    val cfbPid1                 = argMaps("CfbPid1")
    val cfbPid2                 = argMaps("CfbPid2")
    val paramAppLan             = argMaps("AppLang")
    val inputEvent              = argMaps("InputEvent")
    val inputFeatureLog         = argMaps("featureLog")
    val exp                     = argMaps("Exp")
    val output                  = argMaps("Output")
    val debugPath               = argMaps("debugPath")

    val reserved_keys_path      = argMaps("reserved_keys")
    val feature_logger_map_path = argMaps("feature_logger_map")

    val conf = new SparkConf().setAppName("gen feature data")
    val sc   = new SparkContext(conf)
    val hql  = new HiveContext(sc)

    val reserved_keys      = reserved_keys_path.split(",").toSet
    val maps: Array[String] = sc.textFile(feature_logger_map_path).collect
    val feature_logger_map = Util.parseMap(Util.hmapRegex)(maps(0))
    val featureMaps        =
      sc.textFile(featMapFileHdfs)
        .collect
        .foldLeft(HashMap.empty[String, Int])((y, x) => {
          val terms = x.split("\\s")
          y + (terms(1) -> terms(0).toInt)
        })

    // println(s"feature maps: ${featureMaps.toString}")

    dat_gen.featmap_file_hdfs = featMapFileHdfs
    dat_gen.cfb_pid1          = cfbPid1
    dat_gen.cfb_pid2          = cfbPid2
    dat_gen.param_app_lan     = paramAppLan 
    dat_gen.input_event       = inputEvent
    dat_gen.input_feature_log = inputFeatureLog
    dat_gen.exp               = exp
    dat_gen.output            = output

    val text = sc.textFile(inputEvent)

    implicit def toDebug(data: RDD[Row]): Debug =
      Debug(data)

    case class Debug(data: RDD[Row]) {
      def saveTo(path: String): Unit =
        data.map(_.mkString(", ")).saveAsTextFile(path)

      def count: Long = data.count

      def and(data2: RDD[Row]): RDD[Row] =
        data union data2
    }

    def genRCVFeature(text: RDD[String]):
        HashMap[dat_gen.STupleKey, dat_gen.STupleVal] = {
      val xraw_rcv_log = Util.time("raw_rcv_log") { dat_gen.raw_rcv_log(sc, hql, text) }
      val xlog_click_view = Util.time("log_click_view") { dat_gen.log_click_view(hql, xraw_rcv_log) }
      Util.time("raw_event_ap") { dat_gen.raw_event_map(hql, xlog_click_view) }
    }


    val mfLog = sc.textFile(inputFeatureLog)

    def genFeatureLog(fLog: RDD[String]): DataFrame = {
      val xfeature_log = Util.time("feature_log") { dat_gen.feature_log(hql, sc, fLog) }
      xfeature_log saveTo (debugPath + "xfeature_log/")
      Util.time("feature_log_dist") { dat_gen.feature_log_dist(hql)(reserved_keys, feature_logger_map)(xfeature_log) }
    }

    genFeatureLog(mfLog).rdd saveTo (debugPath + "feature_log/")

    /*

    def MergeAll(rcvF: HashMap[dat_gen.STupleKey, dat_gen.STupleVal], fLogF: DataFrame): Unit = {
      Util.time("merge all") {
        dat_gen.fastJoinAll(sc)(featureMaps)(rcvF, fLogF).map(_.mkString(","))
             .coalesce(1000, shuffle=true).saveAsTextFile(output)
      }
    }

    MergeAll(genRCVFeature(text), genFeatureLog(fLog))
    */
    sc.stop()
  }
}


