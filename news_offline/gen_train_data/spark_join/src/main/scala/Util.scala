package data_gen

import scala.collection.immutable._
import scala.util.matching.Regex


trait SerializeMap {
  type From
  type To
  def serializeMap(m: HashMap[From, To]): String = {
    "{" + m.map({case (key: From, value: To) => key.toString + ":" + value.toString})
      .mkString(",") + "}"
  }

  def deSerializeMap(str: String): HashMap[From, To]
}

object StringStringMap extends SerializeMap {
  type From = String
  type To = String

  def deSerializeMap(str: String): HashMap[From, To] = {
    val line = str.drop(1).dropRight(1)
    line.split(",").foldLeft(HashMap.empty[String, String])((y, x) => {
      val terms = x.split(":")
      y + (terms(0) -> terms(1))
    })
  }
}

object StringIntMap extends SerializeMap {
  type From = String
  type To = Int

  def deSerializeMap(str: String): HashMap[From, To] = {
    val line = str.drop(1).dropRight(1)
    line.split(",").foldLeft(HashMap.empty[String, Int])((y, x) => {
      val terms = x.split(":")
      y + (terms(0) -> terms(1).toInt)
    }) 
  }
}

object StringFloatMap extends SerializeMap {
  type From = String
  type To = Float
  def deSerializeMap(str: String): HashMap[From, To] = {
    val line = str.drop(1).dropRight(1)
    line.split(",").foldLeft(HashMap.empty[String, Float])((y, x) => {
      val terms = x.split(":")
      y + (terms(0) -> terms(1).toFloat)
    }) 
  }
}

object SerialTest {

  def main(arg: Array[String]) {
    val m = HashMap("a" -> 1, "b" -> 2)
    require(StringIntMap.deSerializeMap(StringIntMap.serializeMap(m))("a") == 1,
      s"""
      |-- ${m.toString}
      |-- ${StringIntMap.serializeMap(m)},
      |-- ${StringIntMap.deSerializeMap(StringIntMap.serializeMap(m)).toString}
        """)
    val m1 = HashMap("a" -> "asdf", "b" -> "asdf")
    require(StringStringMap.deSerializeMap(StringStringMap.serializeMap(m1))("a") == 1,
      s"""
      |-- ${m1.toString}
      |-- ${StringStringMap.serializeMap(m1)},
      |-- ${StringStringMap.deSerializeMap(StringStringMap.serializeMap(m1)).toString}
      """)
    val m2 = HashMap("a" -> 1f, "b" -> 2f)
    require(StringFloatMap.deSerializeMap(StringFloatMap.serializeMap(m2))("a") == 1,
      s"""
      |-- ${m2.toString}
      |-- ${StringFloatMap.serializeMap(m2)},
      |-- ${StringFloatMap.deSerializeMap(StringFloatMap.serializeMap(m2)).toString}
      """)
  }
}

object Util {
  lazy val hmapRegex: Regex = "\"([^\":\\s,]*)\":\"([^\":\\s,]*)\"".r
  def parseMap(mapRegex: Regex)(str: String): HashMap[String, String] = {
    mapRegex.findAllIn(str)
      .toArray
      .foldLeft(HashMap.empty[String, String])((y, x) => x match {
        case mapRegex(key, value) => y + (key -> value)
        case other                => y
      })
  }

  def time[R](tag: String)(block: => R): R = {  
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val t1 = System.nanoTime()
    println(s"$tag Elapsed time: " + (t1 - t0) + "ns")
    result
  }
}
