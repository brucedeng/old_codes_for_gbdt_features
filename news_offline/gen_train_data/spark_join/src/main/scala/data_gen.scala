package data_gen

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

import org.apache.spark.rdd.RDD

import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.sql.{SQLContext, Row}

import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.jackson.Serialization.{write, read}

import java.sql.Timestamp
import scala.collection.immutable._


object dat_gen {
  def map2HashMap[A, B](m: scala.collection.Map[A, B]): HashMap[A, B] =
    m.foldLeft(HashMap.empty[A, B])(_ + _)
  def map2StringHashMap[A, B](m: scala.collection.Map[A, B]): HashMap[A, String] =
    m.foldLeft(HashMap.empty[A, String])((y, x) => y + (x._1 -> x._2.toString))

  implicit val formats = DefaultFormats
  var featmap_file_hdfs = ""
  var cfb_pid1 = ""
  var cfb_pid2 = ""
  var param_app_lan = ""
  var input_event = ""
  var input_feature_log = ""
  var exp = ""
  var output = ""

  val equalRegex = "([^=|]*)=([^=|]*)".r
  def parserRid(str_data: String): String = {
    val terms = str_data.split("\\|")
    terms.foldLeft("")((y, x) => x match {
      case equalRegex("rid", value) => value
      case other => y
    })
  }

  lazy val rcvStruct = new StructType(
    Array(
      StructField("app_lan", StringType, true),
      StructField("contentid", StringType, true),
      StructField("uid", StringType, true),
      StructField("ip", StringType, true),
      StructField("servertime_sec", LongType, true),
      StructField("productpid", StringType, true),
      StructField("ctype", StringType, true),
      StructField("dwelltime", IntegerType, true),
      StructField("act", StringType, true),
      StructField("exp", StringType, true),
      StructField("cpack", StringType, true)
    )
  )

  def liftM[T](default: T)(work: => T): T = try {
      val res = work
      res
  } catch { case e: Exception => default }

  def raw_rcv_log(sc: SparkContext, hql: HiveContext, text: RDD[String]): RDD[Row] = {
    println(s"raw_rcv_log text: ${text.count.toString}")
    val result = text.flatMap(elem => {
      try {
        val jObj = parse(elem)
        val app_lan = jObj \ "app_lan"
        //val app_lan = if (app_lan_tmp == JNothing) "en" else { if ((app_lan_tmp.extract[String]) startsWith "hi") "hi" else "en" }
        val contentid = liftM("") { (jObj \ "contentid").extract[String] }
        val aid = liftM("") { (jObj \ "aid").extract[String] }
        val ip = liftM("") { (jObj \ "ip").extract[String] }
        val servertime_sec = liftM(0L) { (jObj \ "servertime_sec").extract[String].toLong }
        val pid = liftM("") { (jObj \ "pid").extract[String] }
        val ctype = liftM("") { (jObj \ "ctype").extract[String] }
        val dwelltime = liftM(0L) { (jObj \ "ext" \ "dwelltime").extract[String].toLong }
        val act = liftM("") { (jObj \ "act").extract[String] }
        val exp = liftM("") { (jObj \ "upack" \ "exp").extract[String] }
        val rid_tmp = liftM("") { (jObj \ "cpack" \ "des").extract[String] }
        val rid = parserRid(rid_tmp)

        val level1_type = liftM("") { (jObj \ "scenario" \ "level1_type").extract[String] }
        val level1 = liftM("") { (jObj \ "scenario" \ "level1").extract[String] }

        if ((pid == cfb_pid1 || pid == cfb_pid2) &&
            (ctype == "1" || ctype == "0x01") &&
            (act == "1" || act == "2" || act == "3" || act == "4")) {
           // (act == "1" || act == "2" || act == "3" || act == "4") && 
           // (app_lan == param_app_lan ) &&
           // (level1_type == "1" || level1_type == "10") &&
           // (level1 == "1")) {
          Array(Row(app_lan, contentid, aid,
            ip,servertime_sec, pid, ctype, dwelltime,
            act, exp, rid))
        } else {
          Array.empty[Row]
        }
      } catch {
        case exception => {
          throw new Error(s"raw_rcv_log, exception: ${exception.toString}")
          Array.empty[Row]
        }
      }
    })
    result
    // hql.createDataFrame(result, rcvStruct)
  }

  lazy val dfLogClickView  = new StructType(
    Array(
      StructField("uid", StringType, true),
      StructField("contentid", StringType, true),
      StructField("rid", StringType, true),
      StructField("ts", LongType, true),
      StructField("event_type", StringType, true),
      StructField("productid", StringType, true),
      StructField("dwelltime", IntegerType, true),
      StructField("listpagedwell", IntegerType, true),
      StructField("click", IntegerType, true)
    )
  )

  def log_click_view(hql: HiveContext, data: RDD[Row]): DataFrame = {
    val rdd_row = data.flatMap {
      case Row(app_lan: String, contentid: String, uid: String,
        ip: String, eventtime: Long, productid: String, ctype: String,
        dwelltime: Long, act: String, exp, rid) => {
        val event_type = act match {
          case "1" => "view"
          case "2" => "click"
          case "3" => "listpagedwell"
          case "4" => "read"
          case  _  => "unknown"
        }
        val dtime: Int = if ((event_type == "read") && (dwelltime > 0)) dwelltime.toInt else 0
        val listpagedwell: Int = if ((event_type == "listpagedwell") && (dwelltime > 0)) dwelltime.toInt else 0
        val click: Int = if (event_type == "click") 1 else 0

        if (eventtime != -1L)
          Array(Row(uid, contentid, rid, eventtime,
            event_type, productid, dtime, listpagedwell, click))
        else {
          Array.empty[Row]
        }
      }
    }
    // createDataFrame(RDD[Row], StructType): DataFrame
    hql.createDataFrame(rdd_row, dfLogClickView)
  }

  // ts dwelltime slpwell label
  // type STupleVal = (Long, Int, Int, Int)
  // uid contentid productid rid
  // type STupleKey = (String, String, String, String)

  def raw_event_map(hql: HiveContext, lcv_data: DataFrame): HashMap[STupleKey, STupleVal] = {
    import hql.implicits._

    val grouped_data = lcv_data.groupBy("uid", "contentid", "productid", "rid")
      .agg($"uid",
        $"contentid",
        $"productid",
        $"rid",
        min("ts") as "min_ts",
        sum("dwelltime") as "sum_dwelltime",
        sum("listpagedwell") as "sum_listpagedwell",
        max("click") as "label")

    val res = grouped_data.rdd.flatMap {
      /*
      case Row(uid: String,
        contentid: String,
        productid: String,
        rid: String,
        ts: Long,
        dwelltime: Long,
        slpdwell: Long,
        label: Long) => {
          val dtime: Int = math.min(dwelltime, 600L).toInt
          val listpagedwell: Int = math.min(slpdwell, 600L).toInt
          Array(((uid, contentid, productid, rid), (ts.toLong, dtime, listpagedwell, label)))
        }
        */
      case row: Row => {
        val uid = row.getString(0)
        val contentid = row.getString(1)
        val productid = row.getString(2)
        val rid = row.getString(3)
        val ts = row.getLong(4)
        val dwelltime = math.min(row.getLong(5), 600L).toInt
        val listpagedwell = row.getLong(6).toInt
        val label = row.getInt(7)
        val dtime = math.min(dwelltime, 600)
        Array(((uid, contentid, productid, rid), (ts.toLong, dtime, listpagedwell, label)))
      }
    }.collectAsMap

    println(s"raw_event_group: res map: ${res.toString}")
     
    map2HashMap(res)
  }



  def raw_event_group(hql: HiveContext, lcv_data: DataFrame): DataFrame = {
    import hql.implicits._

    val grouped_data = lcv_data.groupBy("uid", "contentid", "productid", "rid")
      .agg($"uid", $"contentid", $"productid", $"rid",
        min("ts") as "ts", sum("dwelltime") as "dwelltime",
        sum("listpagedwell") as "listpagedwell", max("click") as "label")
      
    val structType = grouped_data.schema

    val res = grouped_data.rdd.flatMap {
      case Row(uid: String, contentid: String, productid: String,
        rid: String, ts: Long, dwelltime: Int,
        slpdwell: Int, label: Int) => {
        val dtime: Int = math.min(dwelltime, 600)
        val listpagedwell: Int = math.min(slpdwell, 600)
        Array(Row(uid, contentid, productid, rid, ts, dtime, listpagedwell, label))
      }
    }

    hql.createDataFrame(res, structType)
  }

  // ---------------
  //
  // feature log
  // ---------------
  //

  def format_feature_log(reserved_keys: Set[String],
    feature_logger_map: HashMap[String, String])(jFeature: JObject): HashMap[String, Float] = jFeature match {
      case JObject(lst: List[JField]) =>
        lst.foldLeft(HashMap.empty[String, Float])((y, x) => x match {
          case (key, value) => {
            if (reserved_keys contains key) y
            else if (! (feature_logger_map contains key)) y
            else y + (feature_logger_map(key) -> value.extract[Float])
          }
        })
    }

  def feature_log(hql: HiveContext, sc: SparkContext, fLog: RDD[String]): RDD[Row] = {
    val res = fLog.flatMap {
      case line => {
        val json: JValue = parse(line)
        val rid = liftM("") { (json \ "req_id").extract[String] }
        val pid = liftM("") { (json \ "prod_id").extract[String] }
        val predictor_id = liftM("") { (json \ "predictor_id").extract[String] }
        val ts = liftM(0L) { (json \ "now").extract[Long] }
        val doc_size = liftM(0L) { (json \ "doc_size").extract[Long] }
        val uid =  liftM("") { (json \ "doc_size").extract[String] }
        // val up: String = StringStringMap.serializeMap(map2StringHashMap((json \ "up").extract[Map[String, String]]))
        val up: String = liftM("") { compact(render(json \ "up")) }
        val features: JValue = json \ "docs"
        if (
          //(predictor_id matches "^hindi_.*$") &&
          (pid == cfb_pid1 || pid == cfb_pid2)) {
            features match {
              case JArray(a) => a.flatMap {
                case feat: JObject => {
                  // uid, contentid, pid, rid, predictor_id, ts, doc_size, up, feat
                  val contentid = (feat \ "id").extract[String]
                  Array(Row(uid, contentid,
                    pid, rid, predictor_id, ts, doc_size, up, write(feat)))
                }
                case other => throw new Error(s"$other")
              }
              case other => throw new Error(s"$other")
            }
        } else {
          Array.empty[Row]
        }
      }
    }
    hql.createDataFrame(res, fStructType)
    res
  }

  lazy val fStructType = new StructType(
    Array(
      StructField("uid",          StringType,  true),
      StructField("contentid",    StringType,  true),
      StructField("productid",    StringType,  true),
      StructField("rid",          StringType,  true),
      StructField("predictor_id", StringType,  true),
      StructField("ts",           IntegerType, true),
      StructField("doc_size",     LongType,    true),
      StructField("up",           StringType,  true),
      StructField("feat",         StringType,  true)
    )
  )


  def events_with_feature(
    sc: SparkContext,
    hql: HiveContext,
    reserved_keys: Set[String],
    feature_logger_map: HashMap[String, String],
    fMaps: HashMap[String, Int]
    )(rEventMap: HashMap[STupleKey, STupleVal],
    fLog: RDD[Row]): RDD[Row] = {
    val broadMap = sc.broadcast(rEventMap).value
    val res =
      fLog.flatMap {
      case Row(uid: String, contentid: String, productid: String,
        rid: String, predictor_id: String, ts: Int, doc_size: Long,
        up: String, feature: String) => {
          if (broadMap contains (uid, contentid, productid, rid)) {
            broadMap((uid, contentid, productid, rid)) match {
              case (event_ts: Long, dwelltime: Int, slpdwell: Int, label: Int) =>
                Array(Row(uid, contentid, label, feature,
                  dwelltime, slpdwell, up, doc_size, rid, event_ts, productid))
            }
          } else Array.empty[Row]
        }
    }.map {
      case Row(uid, contentid, label: Int, feature: String,
        dwelltime, slpdwell, up, doc_size, rid, event_ts: Long, productid) =>
        ((uid, contentid, productid, rid), (label, feature, dwelltime, slpdwell, up, doc_size, event_ts: Long))
    }.reduceByKey((x, y) => {
      if (x._7 < y._7) y else x
    }).map {
      case ((uid, contentid, productid, rid), (label: Int, feature: String, dwelltime, slpdwell, up, doc_size, event_ts: Long)) =>
        (uid, contentid, label,
          StringFloatMap.serializeMap(format_feature_log(reserved_keys, feature_logger_map)(read[JObject](feature))),
          dwelltime, event_ts: Long)
    }.flatMap {
      case (uid, contentid, label: Int, feature: String, dwelltime, event_ts) => {
        val train_data = format_train_data(fMaps)(label, feature)
        if (! train_data.isEmpty)
          Array(Row(uid, contentid, label, dwelltime, event_ts))
        else Array.empty[Row]
      }
    }
    res
  }

  def feature_log_dist(hql: HiveContext)(reserved_keys: Set[String],
    feature_logger_map: HashMap[String, String])(rdd: RDD[Row]): DataFrame = {
    val wraped_rdd = rdd.map({
      case Row(uid, contentid, pid, rid, predictor_id, ts: Long, doc_size, up, feat) =>
        ((uid, contentid, pid, rid), (predictor_id, ts, doc_size, up, feat))
    })

    val data_row = wraped_rdd.reduceByKey((x, y) => {
      if (x._2 > y._2) x
      else y
    }).map({
      case ((uid, contentid, pid, rid),
        (predictor_id, ts, doc_size, up, feat: String)) =>
        Row(uid, contentid, pid, rid,
          predictor_id, ts.toInt, doc_size, up,
          StringFloatMap.serializeMap(format_feature_log(reserved_keys, feature_logger_map)(read[JObject](feat))))
    })

    hql.createDataFrame(data_row, fStructType)
  }

  // DataFrame Join
  // 
  //
  
  def format_train_data(fMaps: HashMap[String, Int])(label: Int, feat: String): String = {
    val features = read[HashMap[String, Float]](feat)
    val f = features.flatMap {
      case (name, value) =>
        if (fMaps contains name) Array(s"${fMaps(name).toString}:${name}")
        else Array.empty[String]
    }.mkString(" ")
    label.toString + " " + f
  }

  lazy val trainType = new StructType(
    Array(
      StructField("uid", StringType, true),
      StructField("contentid", StringType, true),
      StructField("label", IntegerType, true),
      StructField("dwelltime", IntegerType, true),
      StructField("train_data", StringType, true),
      StructField("ts", IntegerType, true)
    )
  )

  // need feature maps 
  // feature log (large, GB)
  def joinAll(hql: HiveContext)(fMaps: HashMap[String, Int])(rEvent: DataFrame, fLog: DataFrame): DataFrame = {
    val jRes = rEvent.join(fLog,
      rEvent("uid") <=> fLog("uid") &&
      rEvent("contentid") <=> fLog("contentid") &&
      rEvent("productid") <=> fLog("productid") &&
      rEvent("rid") <=> fLog("rid"),
      "inner")
      .agg(rEvent("uid"), rEvent("contentid"), rEvent("label"),
        fLog("feat"), rEvent("dwelltime"), rEvent("listpagedwell"),
        fLog("up"), fLog("doc_size"), rEvent("rid"), rEvent("ts")
        )

    val res = jRes.rdd.map {
      case Row(uid, contentid, label: Int, feat: String, dtime: Int, lpdl, up, docsize, rid, ts) =>
        Row(uid, contentid, label, dtime, format_train_data(fMaps)(label, feat), ts)
    }
    hql.createDataFrame(res, trainType)
  }
  // ts dwelltime slpwell label
  type STupleVal = (Long, Int, Int, Int)
  // uid contentid productid rid
  type STupleKey = (String, String, String, String)

  def fastJoinAll(sc: SparkContext)(fMaps: HashMap[String, Int])(rEventMap: HashMap[STupleKey, STupleVal], fLog: DataFrame): RDD[Row] = {
    val broadEventMap: HashMap[STupleKey, STupleVal] = sc.broadcast(rEventMap).value
    fLog.rdd.flatMap {
      case Row(uid: String, contentid: String, pid: String, rid: String,
        predictor_id, tss: Long, doc_size, up, feature: String) => {
          if (broadEventMap.contains((uid, contentid, pid, rid))) {
            broadEventMap((uid, contentid, pid, rid)) match {
              case (ts: Long, dwelltime: Int, slpdwell: Int, label: Int) =>
                Array(Row(uid, contentid, label, dwelltime,
                format_train_data(fMaps)(label, feature), ts))
            }
          } else Array.empty[Row]
        }
      case other => throw new Error(s"no such element: $other")
    }
  }
}
