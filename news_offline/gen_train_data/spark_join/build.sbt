name := "spark_sql"

version := "1.0"

scalaVersion := "2.10.4"

autoScalaLibrary := false

// libraryDependencies += "org.apache.spark" %% "spark-core" % "1.3.0" % "provided"
libraryDependencies ++= Seq(
        "org.slf4j" % "slf4j-api"       % "1.7.7",
        "org.slf4j" % "jcl-over-slf4j"  % "1.7.7"
        ).map(_.force())

libraryDependencies ~= { _.map(_.exclude("org.slf4j", "slf4j-jdk14")) }
libraryDependencies += "org.apache.spark" %% "spark-core" % "1.3.0" % "provided"

libraryDependencies += "org.apache.spark" %% "spark-mllib" % "1.3.0" % "provided"

libraryDependencies += "it.unimi.dsi" % "fastutil" % "6.5.15"

libraryDependencies += "com.facebook.jcommon" % "util" % "0.1.14"

libraryDependencies += "org.apache.spark" %% "spark-sql"  % "1.3.0"

libraryDependencies += "org.apache.spark" % "spark-hive_2.10" % "1.3.0"

// libraryDependencies += "com.github.tototoshi" % "play-json4s-native_2.10" % "0.4.2"

// libraryDependencies += "com.github.tototoshi" % "play-json4s-jackson_2.10" % "0.4.2"

libraryDependencies += "org.json4s" % "json4s-native_2.10" % "3.1.0"
libraryDependencies += "org.json4s" % "json4s-jackson_2.10" % "3.1.0"

libraryDependencies += "com.typesafe.scala-logging" % "scala-logging-slf4j_2.10" % "2.1.2"

libraryDependencies += "log4j" % "log4j" % "1.2.14"
