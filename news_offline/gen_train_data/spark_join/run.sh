source common.conf

hadoop fs -rmr -skipTrash $Output
aws s3 rm $tmpPath --recursive

echo $SparkConfig $Other \

spark-submit \
  $Other \
  $SparkConfig \
  --class 'data_gen.Test' \
  $JAR_PATH \
  --InputEvent=$InputEvent \
  --featureLog=$featureLog \
  --tag="zhangyule_join_feature_log" \
  --Exp=$Exp \
  --CfbPid1=$CfbPid1 \
  --CfbPid2=$CfbPid2 \
  --AppLang=$AppLang \
  --debugPath=$debugPath \
  --output_s3_path=$output_s3_path \
  --Output=$Output \
  --reserved_keys=$reserved_keys \
  --feature_logger_map=$feature_logger_map \
  --FeatMapFileHDFS=$FeatMapFileHDFS \
