# basic settings
event_start='2016-09-14 02:00';
event_end='2016-09-20 00:00';

test_start='2016-09-19 02:00';
train_sample_rate=1.0;
app_lan=en
cfb_pid='(14)|(17)'
product_id='14'
lan_region=en_us

log_dir=logs/nr_en_us_20160919
cfb_ignore_date='2016-09-18 06:00'
PARALLEL=3
APP_NAME=mc_EN_US_NR

# run processes
# run_merge_cp=true
# run_format_raw_cfb=true
# run_raw_cfb=true
# run_format_train_event=true
# run_train_data=true
# run_gen_libsvm=true
run_format_data_by_day=true

# pig params
queue=experiment
# queue=offline

config_file=cfb_features.conf
PIG_PARAM=" -Dmapreduce.job.queuename=$queue ";

RAW_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 2g --num-executors 30 --queue $queue "
TRAIN_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 6g --num-executors 30 --queue $queue --conf spark.memory.storageFraction=0.5 --conf spark.yarn.executor.memoryOverhead=1024"
FORMAT_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 1g --num-executors 30 --queue $queue "
TOP_PARAM=" -p WND=600 "

SPARK_TOP_PARAM=" parallelism 300 wnd 600 u_ncat 5 u_nkey 25 d_ncat 5 d_nkey 15 cat_version oversea_l2 kw_version oversea rel_u_ncat 200 rel_u_nkey 200 rel_d_ncat 200 rel_d_nkey 200 decay_factor 0.995 decay_wind 2d top_num 400 random_num 1000 config_file $config_file l1_norm yes multi_lan no"

cfb_filter_condition=filter_cfb_event
# input_user_root="/projects/news/user_profile/data/V4/up_json/en_us"
overall_user_template="/projects/news/user_profile/contents_user_profile/up_v4_json_hourly/%Y%m%d/%H"
insc_user_template="/projects/news/user_profile/contents_user_profile/up_v4_incremental_json_hourly/%Y%m%d/%H"

input_cp_daily_template="/projects/news/cpp/feeder/in_cp_dump/%s/*/*"
output_lan=`echo $lan_region | tr ',' '-'`
event_start_path=`echo $event_start | sed 's/[,: ]//g'`
event_end_path=`echo $event_end | sed 's/[,: ]//g'`
merged_cp_path="/projects/news/model/experiments/merged_cp_multiple_days/${output_lan}-${event_start_path}-${event_end_path}"
kw_split=0
split_seed=1000

# output path
output_root="/projects/news/model/training/${output_lan}_train_data_${event_start_path}-${event_end_path}"

format_rawcfb_event="$output_root/formatted_rawcfb_events/%s"
format_train_event="$output_root/formatted_cfb_events/%s"
raw_cfb_template="$output_root/raw_cfb/%s"
merged_cfb_root="$output_root/merged_cfb"
train_libsvm_root="$output_root/libsvm"

# raw_cfb_template="$output_root/raw_cfb/${app_lan}_offline_cfb_raw_new/%s"

