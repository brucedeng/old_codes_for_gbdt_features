# basic settings
event_start=20160619;
event_end=20160619;
train_sample_rate=1.0;
app_lan=hi
product_id=11

cfb_ignore_date=20160619

APP_NAME=test_spark_train

# run processes
# run_format_raw_cfb=true
# run_raw_cfb=true
# run_format_train_event=true
run_train_data=true

# pig params
queue=experiment
# queue=offline
PIG_PARAM=" -Dmapred.job.queue.name=$queue";
# RAW_SPARK_PARAM=" --conf spark.rpc.askTimeout=300 --conf spark.task.maxFailures=100 --conf spark.yarn.executor.memoryOverhead=1024 --conf spark.yarn.max.executor.failures=200 --conf spark.memory.storageFraction=0.6 --master yarn --deploy-mode cluster --driver-memory 1g --executor-memory 2g --executor-cores 3 --num-executors 30 --queue $queue "
RAW_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 6g --num-executors 30 --queue $queue "
TRAIN_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 10g --num-executors 100 --queue $queue --conf spark.memory.storageFraction=0.3"
TOP_PARAM=" -p WND=600 "

SPARK_TOP_PARAM=" parallelism 800 pid 11 wnd 600 u_ncat 5 u_nkey 25 d_ncat 5 d_nkey 15 cat_version oversea kw_version oversea rel_u_ncat 200 rel_u_nkey 200 rel_d_ncat 200 rel_d_nkey 200 decay_factor 0.995 decay_wind 2d top_num 1000 random_num 1000 "

# data pathes
if [[ "$app_lan" == "hi" ]]; then
    input_user_root="/projects/news/model/hindi_up"
else
    input_user_root="/projects/news/user_profile/data/V3/up_json"
fi
# input_cp_template="/projects/news/cpp/feeder/in_cp_dump/%s/*/*/*.data"
#input_cp_template="/projects/news/model/experiments/merged_cp_multiple_days/20160321-previous"
input_interest_root="s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall"
input_cp_template="/projects/news/model/experiments/merged_cp_multiple_days/20160608-20160619"
kw_split=10000000

input_event_template="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click}/$cur_date/*/*"

# output path
output_root="/projects/news/model/training"

format_rawcfb_event="$output_root/formatted_events/raw_cfb_$event_start-$event_end/%s"
format_train_event="$output_root/formatted_events/train_data_$event_start-$event_end/%s"
raw_cfb_template="$output_root/raw_cfb/${app_lan}_offline_cfb_test/%s"
merged_cfb_root="$output_root/merged_cfb/${app_lan}_feature_aggcfb_test"

# raw_cfb_template="$output_root/raw_cfb/${app_lan}_offline_cfb_raw_new/%s"

output_s3_path="s3://com.cmcm.instanews.usw2.prod/model/experiments/${app_lan}_offlinecfb_training_data_test";
featmap_path=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}
featmap_file_hdfs=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}.txt
