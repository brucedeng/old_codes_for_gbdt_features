
train_config_file=$1
source $train_config_file

echo $event_start
echo $event_end

set -e
# test if all needed variables are assigned.
test_var="${PIG_PARAM?} ${TOP_PARAM?} ${APP_NAME?}"

if [[ "$run_merge_cp" == "true" ]]; then
    # test if all needed variables are assigned.
    test_var="${input_cp_daily_template?} ${merged_cp_path?}"

    cur_date=`date -d "+4 day ago $event_start" +%Y%m%d`;

    ftr_start=$cur_date
    ftr_end=$event_end;
    agg_dates=$ftr_start
    for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done
    input_contents=`printf "$input_cp_daily_template" "{$agg_dates}"`

    output="/projects/news/model/experiments/merged_cp_multiple_days/$event_start-$event_end"
    cmd="pig $PIG_PARAM -p job_name=${APP_NAME}_merge_cp -p cp_dump='$input_contents' -p output='$merged_cp_path' gen_merged_cp.pig"
    echo "$cmd"
    eval $cmd
    if [ $? != 0 ]; then echo "ERROR: Failed to run merge cp!"; exit -1; fi
fi

if [[ "$run_format_train_event" == "true" ]]; then
    # test if all needed variables are assigned.
    test_var="${format_train_event?} ${lan_region?}"

    cur_date=$event_start

    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do
        if [[ "a$cfb_ignore_date" != "a" ]]; then
            if [[ $cfb_ignore_date -gt $cur_date ]]; then
                cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
                continue
            fi
        fi

        input_event="/projects/news/data/raw_data/{impression,click,readtime}/$cur_date/*/*"
        output=`printf "$format_train_event" "$cur_date"`

        method=" -p METHOD=gen_event "

        cmd="pig $PIG_PARAM $TOP_PARAM $method -p job_name=${APP_NAME}_agg_cfb_event -p event='$input_event' -p output='$output' -p pid=$product_id -p lan_region=${lan_region} generate_format_event_data.pig"
        echo $cmd
        eval $cmd;

        if [ $? != 0 ]; then echo "ERROR: Failed to run format train event!"; exit -1; fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done
fi

if [[ "$run_train_data" == "true" ]]; then
    # test if all needed variables are assigned.
    test_var="${raw_cfb_template?} ${TRAIN_SPARK_PARAM?} ${input_user_root?} ${merged_cp_path?} ${format_train_event?}"

    # cur_date=`date -d "+2 day ago $event_start" +%Y%m%d`;
    cur_date=$event_start

    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do
        if [[ "a$cfb_ignore_date" != "a" ]]; then
            if [[ $cfb_ignore_date -gt $cur_date ]]; then
                cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
                continue
            fi
        fi

        input_format_data=`printf "$format_train_event" "$cur_date"`

        ftr_start=`date -d "+2 day ago $cur_date" +%Y%m%d`;
        ftr_end=$cur_date;
        agg_dates=$ftr_start
        for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done
        input_raw_cfb=`printf "$raw_cfb_template" "{$agg_dates}"`

        up_date=`date -d "+1 day ago $cur_date" +%Y%m%d`;
        input_user="$input_user_root/$up_date"
        ftr_start=`date -d "+4 day ago $cur_date" +%Y%m%d`;
        ftr_end=$cur_date;
        agg_dates=$ftr_start
        for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done
        input_contents=`printf "$merged_cp_path" "{$agg_dates}"`

        output=$merged_cfb_root/$cur_date
        hadoop fs -rm -r $output || echo "$output does not exist"

        date_time="`date -d "+1 days $cur_date" '+%Y-%m-%dT%H:%MZ'`"
        prev_date_time="`date -d "$cur_date" '+%Y-%m-%dT%H:%MZ'`"

        cmd="spark-submit $TRAIN_SPARK_PARAM --conf spark.app.name=${APP_NAME}_gen_binaryfeatures --class com.cmcm.cmnews.model.app.BinaryTrainDataApp content_xfb_train_data_pipeline-1.0-SNAPSHOT.jar $SPARK_TOP_PARAM event_input '$input_format_data' content_input '$input_contents' user_input '$input_user' xfb_input '$input_raw_cfb' train_output '$output' date_time '$date_time' prev_data_time '$prev_date_time' "
        echo "$cmd"
        eval $cmd;

        if [ $? != 0 ]; then echo "ERROR: Failed to run merge cfb!"; exit -1; fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done
fi

