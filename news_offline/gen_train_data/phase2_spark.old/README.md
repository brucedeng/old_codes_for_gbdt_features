# spark training data pipeline

## 运行环境

pipeline里所有的spark程序都需要使用spark 1.6运行. hadoop launcher 上的路径是: 

10.2.2.238:/home/mengchong/news_model/home/spark/spark-1.6.0-runtime/bin/spark-submit

spark code:

```
spark/content_xfb_pipline
spark/content_xfb_train_data_pipeline
```

## 天级训练脚本
### 使用方法

程序的入口是 run_gen_training_data_spark.sh, 它流程化地生成训练数据. 

生成训练数据的步骤包括:

- run_merge_cp : 预处理, 合并训练数据用到的所有cp数据
- run_format_raw_cfb : 格式化raw cfb数据. batch 去重
- run_raw_cfb : 生成raw cfb 数据, spark
- run_format_train_event : 格式化训练数据, 全量数据去重
- run_train_data : 生成merge cfb, spark
- run_gen_libsvm : 生成libsvm格式的数据, spark
- run_split_data : random split 数据
- run_format_data : 格式化成训练数据, 添加weight. 使用random split的4份作训练, 1份作测试
- run_format_data_by_day : 按照 test_start 区分. test_start 以前的(不包含)作为训练数据, 以后的作为测试数据. 和run_format_data 是不同的split方式, 两者只需要选择一种.

脚本里的其它参数的说明如下

| 参数 | 说明 | 示例 | 
| ---- | ------- | ------ |
| event_start | 起始日期 | 20160615 |
| event_end | 结束日期   | 20160715 |
| app_lan | 语言  | hi |
| product_id  | pid  | 11 |
| lan_region  | language region whitelist | hi_in,de_de |
| cfb_ignore_date | 生成cfb的起始时间, 该参数用于数据跑一半失败, 重跑时跳过已生成的数据 | 20160709 |
| queue | 运行的队列 | experiment | 
| PIG_PARAM | pig 运行参数 |  |
| RAW_SPARK_PARAM | spark 生成raw cfb 的参数. 不需要使用太多内存, 资源较少 | | 
| TRAIN_SPARK_PARAM | spark 生成merge cfb 的参数, 一般情况下需要10g的 executor memory, node不要超过60 | | 
| FORMAT_SPARK_PARAM | spark 格式化libsvm的参数, 没有shuffer操作, 申请很少的内存, 节点可以适当增加 | | 
| TOP_PARAM | pig 的固定参数  | -p WND=600 | 
| SPARK_TOP_PARAM | spark的固定参数, 包括window key number等 | | 
| input_user_root | 输入up的路径 | | 
| input_cp_daily_template | 原始cp dump的路径模板. 脚本中使用printf 把日期添加到路径模板中. | | 
| merged_cp_path | cp merge并去重后的路径 | |
| kw_split | 区分新老用户的kw_len 阈值 | 0 |
| split_seed | random split 的随机种子 | 1000 |
| output_root | 总输出路径 | |
| format_rawcfb_event | raw cfb spark的输入 | |
| format_train_event | 给spark用的训练数据去重后的log | | 
| raw_cfb_template | 输入或输出的raw cfb 模板 | |
| merged_cfb_root | 输出的merge cfb 目录 | |
| train_libsvm_root | 输出的libsvm 目录 | | 
| test_start  | 区分train和test的日期. 只用于run_format_data_by_day | 20160709 |

### 配置文件示例

```
# basic settings
event_start=20160615;
event_end=20160715;
train_sample_rate=1.0;
app_lan=hi
product_id=11
lan_region=hi_in

cfb_ignore_date=20160601

APP_NAME=mengchong

# run processes
run_merge_cp=true
run_format_raw_cfb=true
run_raw_cfb=true
run_format_train_event=true
run_train_data=true
run_gen_libsvm=true
run_split_data=true
run_format_data=true

# pig params
queue=experiment

config_file=cfb_features.conf
PIG_PARAM=" -Dmapreduce.job.queuename=$queue ";

RAW_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 6g --num-executors 30 --queue $queue "
TRAIN_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 10g --num-executors 60 --queue $queue "
FORMAT_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 2g --num-executors 60 --queue $queue "
TOP_PARAM=" -p WND=600 "

SPARK_TOP_PARAM=" parallelism 800 wnd 600 u_ncat 5 u_nkey 25 d_ncat 5 d_nkey 15 cat_version oversea_l2 kw_version oversea rel_u_ncat 200 rel_u_nkey 200 rel_d_ncat 200 rel_d_nkey 200 decay_factor 0.995 decay_wind 2d top_num 1000 random_num 1000 config_file $config_file l1_norm yes multi_lan no "

input_user_root="/projects/news/user_profile/data/V4/up_json/hi_in"

input_cp_daily_template="/projects/news/cpp/feeder/in_cp_dump/%s/*/*"
merged_cp_path="/projects/news/model/experiments/merged_cp_multiple_days/${event_start}-${event_end}"
kw_split=0
split_seed=1000

# output path
output_root="/projects/news/model/training/${app_lan}_train_data_20160615-20160715"

format_rawcfb_event="$output_root/formatted_rawcfb_events/%s"
format_train_event="$output_root/formatted_cfb_events/%s"
raw_cfb_template="$output_root/raw_cfb/%s"
merged_cfb_root="$output_root/merged_cfb"
train_libsvm_root="$output_root/libsvm"

```

## 2小时级并行训练脚本
### 使用方法
程序的入口是 run_gen_training_data_hourly_parallel.sh, 它流程化地生成训练数据. 

生成训练数据的步骤包括:

- run_merge_cp : 预处理, 合并训练数据用到的所有cp数据
- run_format_raw_cfb : 格式化raw cfb数据. batch 去重
- run_raw_cfb : 生成raw cfb 数据, spark
- run_format_train_event : 格式化训练数据, 全量数据去重
- run_train_data : 生成merge cfb, spark
- run_gen_libsvm : 生成libsvm格式的数据, spark
- run_format_data_by_day : 按照 test_start 区分. test_start 以前的(不包含)作为训练数据, 以后的作为测试数据. 和run_format_data 是不同的split方式, 两者只需要选择一种.

脚本里的其它参数的说明如下

| 参数 | 说明 | 示例 | 
| ---- | ------- | ------ |
| event_start | 起始日期时间 | '2016-09-14 02:00' |
| event_end | 结束日期时间   | '2016-09-20 00:00' |
| PARALLEL | 并行度. 同时运行几个job **(new)** | 3 |
| log_dir | log 及运行状态的存储路径**(new)** | logs/nr_en_us_20160920 |
| app_lan | 语言  | en |
| cfb_pid  | cfb event 的 pid,正则表达式**(new)**  | '(14)&#124;(17)' |
| product_id  | 训练pid,正则表达式**(new)**  | '14' |
| lan_region  | language region whitelist | en_us,de_de |
| cfb_ignore_date | 生成cfb的起始时间, 该参数用于数据跑一半失败, 重跑时跳过已生成的数据 | '2016-09-17 00:00' |
| queue | 运行的队列 | experiment | 
| PIG_PARAM | pig 运行参数 |  |
| RAW_SPARK_PARAM | spark 生成raw cfb 的参数. 不需要使用太多内存, 资源较少 | | 
| TRAIN_SPARK_PARAM | spark 生成merge cfb 的参数, 一般情况下需要10g的 executor memory, node不要超过60 | | 
| FORMAT_SPARK_PARAM | spark 格式化libsvm的参数, 没有shuffer操作, 申请很少的内存, 节点可以适当增加 | | 
| TOP_PARAM | pig 的固定参数  | -p WND=600 | 
| SPARK_TOP_PARAM | spark的固定参数, 包括window key number等 | | 
| input_user_root | 输入up的路径 | | 
| input_cp_daily_template | 原始cp dump的路径模板. 脚本中使用printf 把日期添加到路径模板中. | | 
| merged_cp_path | cp merge并去重后的路径 | |
| kw_split | 区分新老用户的kw_len 阈值 | 0 |
| split_seed | random split 的随机种子 | 1000 |
| output_root | 总输出路径 | |
| format_rawcfb_event | raw cfb spark的输入 | |
| format_train_event | 给spark用的训练数据去重后的log | | 
| raw_cfb_template | 输入或输出的raw cfb 模板 | |
| merged_cfb_root | 输出的merge cfb 目录 | |
| train_libsvm_root | 输出的libsvm 目录 | | 
| test_start  | 区分train和test的日期. 只用于run_format_data_by_day | '2016-09-19 00:00' |

### 说明
小时级的up是每2小时一份, 分为两种方式存储, 一种是全量的up,每天会保存一份22点的全量数据. 另一种存储是增量存储, 只保存在当前时间窗口内有过pv或click行为的用户的up.

全量up存储目录为 
```
/projects/news/user_profile/contents_user_profile/up_v4_json_hourly/%Y%m%d/%H
```

增量up存储目录为
```
/projects/news/user_profile/contents_user_profile/up_v4_incremental_json_hourly/%Y%m%d/%H
```

增量up每份数据大概延迟1.5小时左右, 因此训练数据里使用的up是join的2个batch前的up.
离线训练时join up的逻辑是:

1. 取前一天的全量up
2. 取前一天22点后到当前时间4小时以前的增量up
3. 将前两份数据join在一起, 然后按照aid去重, 保留时间戳较晚的一份.


例如: 

| 训练数据的时间 | 全量up 时间 | 增量up的时间 |
| -------------| ---------- | ---------- |
| 2016-09-22 00:00 | 2016-09-20 22:00 | 2016-09-21 00:00~20:00 |
| 2016-09-22 02:00 | 2016-09-21 22:00 | 无 |
| 2016-09-22 08:00 | 2016-09-21 22:00 | 2016-09-22 00:00~04:00 |

训练数据和UP数据里, 时间戳是用的时间窗口的起始时间来标记的, 例如, 
2016-09-22 08:00 代表的是8:00 ~ 9:59 这两个小时的数据.

小时级的训练数据和天级的训练逻辑区别是:

1. 数据周期改为2小时
2. up需要load 增量加全量的up, 然后做去重. 因此, spark代码里需要加上up的去重, 并添加了一个参数  dedup_up yes
3. 串行运行小时级的训练数据很慢, 应该并行去跑, 同一时刻跑多份数据.

代码:
run_gen_training_data_hourly.sh 串行运行, 7天数据大约要运行24+小时
run_gen_training_data_hourly_parallel.sh 并行运行脚本. 同时提交多个job, 并维护job间的依赖关系.

run_gen_training_data_hourly_parallel.sh中:
1. start_ts到end_ts决定训练数据的起始和结束时间
2. first_ts决定raw_cfb的计算时间，一般早于start_ts三天，除raw_cfb之外的步骤会跳过这段时间的计算。
3. 循环通过log/command_log_ts.running来控制并行的进程数。
4. 通过raw_cfb.$input_ts.done来控制raw cfb的执行依赖，每一份raw cfb需要再前一份数据执行完成之后提交job。

### 查看运行日志
并行运行的脚本, 日志都保存在 log_dir 配置的路径中, 每一个pig或spark job会保存一个log文件. 文件分为三种:

| 文件格式 | 说明 | 示例 | 
| ---- | ------- | ------ |
| [ts]*.log | 单个任务运行的日志 | 20160918_08-format_merge_cfb.log |
| batch_[ts].log | 每个时间batch的运行日志, 主要包含了每个pig或spark的命令 | batch_20160919_16.log |
| *.running | 正在运行的程序的pid文件. 通过计算running文件的个数来控制并行数. 运行完成后会删除running文件 | 20160919_02.running |

另外, 脚本会将基础的信息打印在终端上. 

**注意: 如果重启脚本, 必须手动将.running文件删除**

### 配置文件示例

```
# basic settings
event_start='2016-09-14 02:00';
event_end='2016-09-20 00:00';

test_start='2016-09-19 02:00';
train_sample_rate=1.0;
app_lan=en
cfb_pid='(14)|(17)'
product_id='^14$'
lan_region=en_us

log_dir=logs/nr_en_us_20160919
cfb_ignore_date='2016-09-18 06:00'
PARALLEL=3
APP_NAME=mc_EN_US_NR

# run processes
run_merge_cp=true
run_format_raw_cfb=true
run_raw_cfb=true
run_format_train_event=true
run_train_data=true
run_gen_libsvm=true
run_format_data_by_day=true

# pig params
queue=experiment

config_file=cfb_features.conf
PIG_PARAM=" -Dmapreduce.job.queuename=$queue ";

RAW_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 2g --num-executors 30 --queue $queue "
TRAIN_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 6g --num-executors 30 --queue $queue --conf spark.memory.storageFraction=0.5 --conf spark.yarn.executor.memoryOverhead=1024"
FORMAT_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 1g --num-executors 30 --queue $queue "
TOP_PARAM=" -p WND=600 "

SPARK_TOP_PARAM=" parallelism 300 wnd 600 u_ncat 5 u_nkey 25 d_ncat 5 d_nkey 15 cat_version oversea_l2 kw_version oversea rel_u_ncat 200 rel_u_nkey 200 rel_d_ncat 200 rel_d_nkey 200 decay_factor 0.995 decay_wind 2d top_num 400 random_num 1000 config_file $config_file l1_norm yes multi_lan no"

cfb_filter_condition=filter_cfb_event

overall_user_template="/projects/news/user_profile/contents_user_profile/up_v4_json_hourly/%Y%m%d/%H"
insc_user_template="/projects/news/user_profile/contents_user_profile/up_v4_incremental_json_hourly/%Y%m%d/%H"

input_cp_daily_template="/projects/news/cpp/feeder/in_cp_dump/%s/*/*"
output_lan=`echo $lan_region | tr ',' '-'`
event_start_path=`echo $event_start | sed 's/[,: ]//g'`
event_end_path=`echo $event_end | sed 's/[,: ]//g'`
merged_cp_path="/projects/news/model/experiments/merged_cp_multiple_days/${output_lan}-${event_start_path}-${event_end_path}"
kw_split=0
split_seed=1000

# output path
output_root="/projects/news/model/training/${output_lan}_train_data_${event_start_path}-${event_end_path}"

format_rawcfb_event="$output_root/formatted_rawcfb_events/%s"
format_train_event="$output_root/formatted_cfb_events/%s"
raw_cfb_template="$output_root/raw_cfb/%s"
merged_cfb_root="$output_root/merged_cfb"
train_libsvm_root="$output_root/libsvm"

```