REGISTER '../lib/*.jar';
REGISTER '../lib/utils.py' USING jython AS myfunc;

-- set mapred.create.symlink yes;
-- set mapred.cache.files $CONFIG#config.json,$CITY_DATA#ip_city.data

-- set default_parallel 400;
set job.name '$job_name'

DEFINE gen_raw_cfb (click_views) RETURNS log_click_view {
    log_click_view_tmp = FILTER $click_views BY ts != -1 and ( event_type=='view' or event_type=='click');
    dedup_content = FOREACH ( GROUP log_click_view_tmp by (uid, content_id, join_ts, event_type, app_lan) ) {
        result = order $1 by ts;
        data_limit = limit result 1;
        GENERATE FLATTEN(data_limit) as (uid, content_id, rid, ts, join_ts, pid, city, event_type, dwelltime, expression, app_lan, lan_region);
    }
    $log_click_view = FOREACH dedup_content GENERATE ts, content_id, uid, lan_region as lan_region, event_type, city, pid, expression;
}

DEFINE gen_event (click_views) RETURNS log_click_view {
    -- log_click_view_tmp = FILTER $click_views BY ts != -1;

    -- log_click_view = FOREACH ( GROUP log_click_view_tmp by (uid, content_id, pid, rid) ) {
    --     result = order $1 by ts;
    --     data_limit = limit result 1;
    --     GENERATE FLATTEN(data_limit) as (uid, content_id, rid, ts, join_ts, pid, city, event_type, dwelltime, expression, app_lan);
    -- }
    log_click_view_tmp = STREAM $click_views THROUGH `cat` as (uid:chararray, content_id:chararray, rid:chararray, ts:long, join_ts:chararray, pid:chararray, city:chararray, event_type:chararray, dwelltime:int, expression:float, app_lan:chararray, lan_region:chararray);

    raw_event_group = GROUP log_click_view_tmp BY (uid, content_id, pid, rid); 

    raw_event_out = FOREACH raw_event_group {
        ts = MIN( $1.ts );
        join_ts = ts/$WND * $WND - $WND;
        click_event = FILTER $1 BY (event_type == 'click');
        click_count = COUNT(click_event);
        label = (click_count > 0 ? 1: 0);
        dwelltime = SUM($1.dwelltime);
        city = MIN($1.city);
        lan_region = MAX($1.lan_region);
    GENERATE
        $0.uid AS uid,
        $0.content_id AS content_id,
        $0.pid as pid,
        $0.rid AS rid,
        ts AS ts,
        join_ts AS join_ts,
        city AS city,
        (dwelltime > 600 ? 600:dwelltime) as dwelltime,
        label AS label,
        lan_region as lan_region;
    }

    $log_click_view = FOREACH raw_event_out {
        id_tuple = TOTUPLE((chararray)uid,(chararray)content_id,(chararray)ts,(chararray)pid,(chararray)label,(chararray)rid);
        id = myfunc.get_md5_id(id_tuple);
        GENERATE id as id, uid,lan_region as lan_region, content_id, ts, join_ts,'' as field6, pid, city, dwelltime, label, rid;
    }
}

DEFINE filter_train_event (events) RETURNS filtered_events {
    $filtered_events = filter $events by act is not null and pid MATCHES '$pid' and ( ctype is null or ctype=='' or ctype=='1' or ctype=='0x01' ) and ( act == '1' or act == '2' or act == '4' or act == '5' or act == '6' or act == '7' or act == '8' or act == '9' or act == '14' or act=='101') and ( json#'scenario'#'level1_type' =='1' or json#'scenario'#'level1_type' =='10' ) and json#'scenario'#'level1' == '1' and INDEXOF('$lan_region',LOWER(lan_region),0) >=0;
}

DEFINE filter_cfb_event (events) RETURNS filtered_events {
    $filtered_events = filter $events by act is not null and pid MATCHES '$pid' and ( ctype is null or ctype=='' or ctype=='1' or ctype=='0x01' ) and ( act == '1' or act == '2' or act == '4' or act == '5' or act == '6' or act == '7' or act == '8' or act == '9' or act == '14' or act=='101') and (act=='101' or ( ((json#'scenario'#'level1_type' =='1' or json#'scenario'#'level1_type' =='10' ) and json#'scenario'#'level1' == '1' )  or (json#'scenario'#'level1_type' =='13' and json#'scenario'#'level1' == '13')  or (json#'scenario'#'level1_type' =='14' and json#'scenario'#'level1' == '14')  or (json#'scenario'#'level1_type' =='15' and json#'scenario'#'level1' == '15'))) and INDEXOF('$lan_region',LOWER(lan_region),0) >=0;
}

raw_rcv_log = LOAD '$event' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate
    json,
    (json#'app_lan' is null ? 'en' : ( json#'app_lan' MATCHES 'hi.*' ? 'hi' : json#'app_lan' )) as app_lan,
    (json#'country' is null? '' : (chararray)(json#'country')) as country:chararray,
    json#'contentid' as content_id,
    json#'aid' as uid,
    json#'ip' as ip,
    json#'servertime_sec' as eventtime,
    json#'pid' as pid,
    json#'ctype' as ctype,
    json#'act' as act,
    json#'city' as city,
    myfunc.parse_rid(json#'cpack') as rid:chararray,
    (int)json#'ext'#'dwelltime' AS dwelltime:int;

raw_rcv_log = FOREACH raw_rcv_log GENERATE json,app_lan,country, CONCAT(app_lan, CONCAT('_', country)) as lan_region, content_id, uid, ip, eventtime, pid, ctype, act, city, rid, dwelltime;

raw_rcv_log = $FILTER_CONDITION(raw_rcv_log);
-- raw_rcv_log = filter raw_rcv_log by act is not null and pid MATCHES '$pid' and ( ctype is null or ctype=='' or ctype=='1' or ctype=='0x01' ) and ( act == '1' or act == '2' or act == '4') and ( json#'scenario'#'level1_type' =='1' or json#'scenario'#'level1_type' =='10' ) and json#'scenario'#'level1' == '1' and INDEXOF('$lan_region',LOWER(lan_region),0) >=0 ;

click_view_data = FOREACH raw_rcv_log GENERATE
    content_id, uid, rid, app_lan, pid, lan_region,
    city,
    dwelltime,
    (long)(eventtime) as ts,
    (long)(eventtime)/$WND * $WND as join_ts,
    (act=='1'? 'view':( act=='2'? 'click':( act=='4'? 'read':'unknown'))) as event_type;

log_click_view = FOREACH click_view_data GENERATE
      uid AS uid,
      content_id AS content_id,
      rid AS rid,
      ts as ts,
      join_ts AS join_ts,
      pid as pid,
      city AS city,
      event_type AS event_type,
      (event_type=='read' ? dwelltime:0) as dwelltime:int,
      1.0 AS expression,
      app_lan,
      lan_region;

-- store log_click_view into '/tmp/news_model/log_click_view';
log_click_view = $METHOD(log_click_view);

rmf $output
store log_click_view into '$output';
