

raw_training_data = LOAD '$OUTPUT/train' as (data:chararray);

binarized_data = STREAM raw_training_data THROUGH `sed 's/ 64:[^0][0-9]* / 64:1 /g' | sed 's/ 62:[^0][0-9]* / 62:1 /g'` as (data:chararray);

store binarized_data into '$OUTPUT/train_kwlen';
