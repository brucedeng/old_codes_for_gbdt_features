REGISTER '../lib/*.jar';
REGISTER '../lib/feature_udf.py' USING jython AS myudf;
 
-- set default_parallel 500;
set job.name '$job_name';

DEFINE gen_train_weighted (events) RETURNS weighted_data {
    train_data_weight = FOREACH $events GENERATE uid,content_id, label, LOG(dwelltime+1) as wt, train_data;

    train_data_with_weight = FOREACH train_data_weight GENERATE uid, content_id,label, (label ==1? wt : 1 ) as weight, train_data as train_data;

    $weighted_data = FOREACH train_data_with_weight GENERATE myudf.add_libsvm_weight(train_data,weight) as train_data;
}

DEFINE gen_train_share_weighted (events) RETURNS weighted_data {
    train_data_weight = FOREACH $events GENERATE uid,content_id, label, LOG(dwelltime+1) as wt, train_data, ( events MATCHES '(.*:)?9(:.*)?' ? 1:0 ) as share:int;

    train_data_share_wt = FOREACH train_data_weight GENERATE uid,content_id, wt, train_data, (share==1? $factor : 1) as share_factor, (share==1?1:label) as label;

    train_data_with_weight = FOREACH train_data_share_wt GENERATE uid, content_id,label, (label ==1? wt*share_factor : 1 ) as weight, train_data as train_data;

    $weighted_data = FOREACH train_data_with_weight GENERATE myudf.add_libsvm_weight(train_data,weight,label) as train_data;
}

DEFINE gen_train_catrel_weighted (events) RETURNS weighted_data {
    train_data_weight = FOREACH $events GENERATE uid,content_id, label, (LOG(dwelltime+1)) as wt:float, train_data, myudf.get_cat_rel(train_data) as cat_rel;
    train_data_weight = STREAM train_data_weight THROUGH `cat` as (uid:chararray,content_id:chararray, label:int, wt:float, train_data:chararray, cat_rel:float);
    -- store train_data_weight into '/tmp/news_model/train_data_weight';

    train_data_share_wt = FOREACH train_data_weight GENERATE uid,content_id, label, wt, train_data, (1 + $factor * cat_rel) as cat_wt;

    train_data_with_weight = FOREACH train_data_share_wt GENERATE uid, content_id,label, (label ==1? wt*cat_wt : 1 ) as weight, train_data as train_data;

    $weighted_data = FOREACH train_data_with_weight GENERATE myudf.add_libsvm_weight(train_data,weight) as train_data;
}

DEFINE gen_train_no_weight (events) RETURNS no_weight_data {
    $no_weight_data = FOREACH $events GENERATE train_data;
}

raw_training_data = LOAD '$train' as (uid:chararray, content_id:chararray, label:int, dwelltime:float,
    train_data:chararray, uniq_id:long, req_id:chararray, pid:chararray, join_ts:long, events:chararray);

libsvm_data = $METHOD(raw_training_data);

rmf $OUTPUT
store libsvm_data into '$OUTPUT';
