
train_config_file=$1
source $train_config_file

echo $event_start
echo $event_end

export PS4='+${BASH_SOURCE}:${LINENO}:${FUNCNAME[0]}: '
set -e
# test if all needed variables are assigned.
test_var="${PIG_PARAM?} ${TOP_PARAM?} ${APP_NAME?} ${PARALLEL?} ${log_dir?}"

mkdir -p $log_dir
# tmp_dir=`mktemp -d -t run_train_data.XXXXX`
# echo "tmp dir is $tmp_dir"
export PARALLEL
# export tmp_dir

start_ts=`date -d "$event_start" +%s`
end_ts=`date -d "$event_end" +%s`

if [[ "$run_merge_cp" == "true" ]]; then
    # test if all needed variables are assigned.
    test_var="${input_cp_daily_template?} ${merged_cp_path?}"

    # cur_date=`date -d@$start_ts +%Y%m%d`
    cur_date=`date -d@$((start_ts-7*86400)) +%Y%m%d`;

    ftr_start=$cur_date
    ftr_end=`date -d@$end_ts +%Y%m%d`;
    agg_dates=$ftr_start
    for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done
    input_contents=`printf "$input_cp_daily_template" "{$agg_dates}"`

    cmd="pig $PIG_PARAM -p job_name=${APP_NAME}_merge_cp -p cp_dump='$input_contents' -p output='$merged_cp_path' gen_merged_cp.pig"
    echo "$cmd"
    eval $cmd > $log_dir/merge_cp.log 2>&1
    if [ $? != 0 ]; then echo "ERROR: Failed to run merge cp!"; exit -1; fi
fi

cfb_ignore_ts=0
if [[ "a$cfb_ignore_date" != "a" ]]; then
    cfb_ignore_ts=`date -d "$cfb_ignore_date" +%s`;
fi

function get_hourly_up_path {
    cur_ts="$1"
    overall_up="$2"
    incremental_up="$3"

    target_up_ts=$((cur_ts-2*7200))
    tmp_ts=$((target_up_ts-79200))
    overall_ts=$(( tmp_ts / 86400 * 86400 + 79200 ))

    output_path=`date -d@$overall_ts "+$overall_up"`

    for (( tmp_ts=target_up_ts; tmp_ts>overall_ts; tmp_ts-=7200)); do
        tmp_hourly_path=`date -d@$tmp_ts "+$incremental_up"`
        output_path="${output_path},${tmp_hourly_path}"
    done
    
    echo $output_path
}

function wait_for_file {
    file_name=$1
    echo "waiting for file $file_name"
    while [[ ! -e $file_name ]]; do
        sleep 5
    done
}

# run previous 3 days raw cfb
first_ts=$((start_ts-3*86400)) # 3 days ago
prev_ts=$((start_ts-7200)) # 4hours ago
# run for training data

function run_one_batch {
    ts=$1

    cur_run_log=`date -d@$ts +%Y%m%d_%H`

    if [[ "$run_format_raw_cfb" == "true" ]]; then
        # test if all needed variables are assigned.
        test_var="${format_rawcfb_event?} ${lan_region?} ${cfb_pid?}"

        hour_path=`date -d@$ts +%Y%m%d/%H`
        input_event_root="/projects/news/data/raw_data/{impression,click}"
        input_path="$input_event_root/$hour_path/*"
        hour_path=`date -d@$((ts+3600)) +%Y%m%d/%H`
        input_path="$input_path,$input_event_root/$hour_path/*"

        output_ts=`date -d@$ts +%Y%m%d_%H`
        output=`printf "$format_rawcfb_event" "$output_ts"`

        method=" -p METHOD=gen_raw_cfb "

        cmd="pig $PIG_PARAM $TOP_PARAM $method -p job_name=${APP_NAME}_raw_cfb_event_$cur_run_log -p FILTER_CONDITION=filter_cfb_event -p event='$input_path' -p output='$output' -p pid='$cfb_pid' -p lan_region=${lan_region}     generate_format_event_data.pig"
        echo "$cmd"
        eval $cmd > $log_dir/$cur_run_log-1_format_raw_cfb.log 2>&1
    fi

    if [[ "$run_raw_cfb" == "true" ]]; then
        # test if all needed variables are assigned.
        test_var="${format_rawcfb_event?} ${RAW_SPARK_PARAM?} ${overall_user_template?} ${insc_user_template?} ${merged_cp_path?} ${raw_cfb_template?}"

        input_ts=`date -d@$ts +%Y%m%d_%H`
        input_format_data=`printf "$format_rawcfb_event" "$input_ts"`

        needed_ts=`date -d@$((ts-7200)) +%Y%m%d_%H`
        if [[ $ts -gt $first_ts && $ts -gt $cfb_ignore_ts ]]; then
            echo "waiting for file $log_dir/raw_cfb.$needed_ts.done"
            wait_for_file $log_dir/raw_cfb.$needed_ts.done
        fi

        output=`printf "$raw_cfb_template" "$input_ts"`
        hadoop fs -rm -r $output || echo "$output does not exist"
        date_time="`date -d@$ts '+%Y-%m-%dT%H:%MZ'`"

        input_contents="$merged_cp_path"

        input_user="`get_hourly_up_path $ts "$overall_user_template" "$insc_user_template"`"

        cmd="spark-submit $RAW_SPARK_PARAM --conf spark.app.name=${APP_NAME}_raw_cfb_$cur_run_log --class com.cmcm.cmnews.model.app.CFBApp content_xfb_pipline-1.0-SNAPSHOT.jar $SPARK_TOP_PARAM event_input '$input_format_data' content_input '$input_contents' user_input '$input_user' xfb_output '$output' date_time '$date_time' dedup_up yes"
        echo "$cmd"
        eval $cmd > $log_dir/$cur_run_log-2_raw_cfb.log 2>&1

        if [ $? != 0 ]; then echo "ERROR: Failed to run raw cfb!"; exit -1; fi
        touch $log_dir/raw_cfb.$input_ts.done
    fi

    if [[ "$run_format_train_event" == "true" && $ts -ge $start_ts ]]; then
        # test if all needed variables are assigned.
        test_var="${format_train_event?} ${lan_region?} ${product_id?}"

        hour_path=`date -d@$ts +%Y%m%d/%H`
        input_event_root="/projects/news/data/raw_data/{impression,click,readtime}"
        input_path="$input_event_root/$hour_path/*"
        hour_path=`date -d@$((ts+3600)) +%Y%m%d/%H`
        input_event="$input_path,$input_event_root/$hour_path/*"

        output_ts=`date -d@$ts +%Y%m%d_%H`
        output=`printf "$format_train_event" "$output_ts"`

        method=" -p METHOD=gen_event "

        cmd="pig $PIG_PARAM $TOP_PARAM $method -p job_name=${APP_NAME}_agg_cfb_event_$cur_run_log -p FILTER_CONDITION=filter_train_event -p event='$input_event' -p output='$output' -p pid='$product_id' -p lan_region=${lan_region} generate_format_event_data.pig"
        echo "$cmd"
        eval $cmd > $log_dir/$cur_run_log-3_format_merge_cfb.log 2>&1
        if [ $? != 0 ]; then echo "ERROR: Failed to run format train event!"; exit -1; fi
    fi

    if [[ "$run_train_data" == "true" && $ts -ge $start_ts ]]; then
        # test if all needed variables are assigned.
        test_var="${raw_cfb_template?} ${TRAIN_SPARK_PARAM?} ${overall_user_template?} ${insc_user_template?} ${merged_cp_path?} ${format_train_event?}"

        input_ts=`date -d@$ts +%Y%m%d_%H`

        input_format_data=`printf "$format_train_event" "$input_ts"`

        start_ts=$((ts-3*86400))

        agg_dates="$input_ts"
        for ((tmp_ts=start_ts; tmp_ts<ts; tmp_ts+=7200)); do
            tmp_ts_str=`date -d@$tmp_ts +%Y%m%d_%H`
            agg_dates="$agg_dates,$tmp_ts_str"
        done
        input_raw_cfb=`printf "$raw_cfb_template" "{$agg_dates}"`

        input_user="`get_hourly_up_path $ts "$overall_user_template" "$insc_user_template"`"

        input_contents="$merged_cp_path"

        output=$merged_cfb_root/$input_ts
        hadoop fs -rm -r $output || echo "$output does not exist"

        date_time="`date -d@$(($ts+7200)) '+%Y-%m-%dT%H:%MZ'`"
        prev_date_time="`date -d@$ts '+%Y-%m-%dT%H:%MZ'`"

        cmd="spark-submit $TRAIN_SPARK_PARAM --conf spark.app.name=${APP_NAME}_agg_cfb_$cur_run_log --class com.cmcm.cmnews.model.app.CFBTrainDataApp content_xfb_train_data_pipeline-1.0-SNAPSHOT.jar $SPARK_TOP_PARAM event_input '$input_format_data' content_input '$input_contents' user_input '$input_user' xfb_input '$input_raw_cfb' train_output '$output' date_time '$date_time' prev_data_time '$prev_date_time' dedup_up yes"
        echo "$cmd"
        eval $cmd > $log_dir/$cur_run_log-4_merge_cfb.log 2>&1
        if [ $? != 0 ]; then echo "ERROR: Failed to run merge cfb!"; exit -1; fi
            cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    fi

    if [[ "$run_gen_libsvm" == "true" && $ts -ge $start_ts ]]; then
        # test if all needed variables are assigned.
        test_var="${raw_cfb_template?} ${TRAIN_SPARK_PARAM?} ${merged_cp_path?} ${format_train_event?} ${train_libsvm_root?} "

        input_ts=`date -d@$ts +%Y%m%d_%H`

        input_merge_cfb=$merged_cfb_root/$input_ts
        output=$train_libsvm_root/daily_data/$input_ts
        hadoop fs -rm -r $output || echo "$output does not exist"

        cmd="spark-submit $FORMAT_SPARK_PARAM --conf spark.app.name=${APP_NAME}_format_data_$cur_run_log --class com.cmcm.cmnews.model.app.FormatLibsvm content_xfb_train_data_pipeline-1.0-SNAPSHOT.jar $SPARK_TOP_PARAM merge_cfb_input '$input_merge_cfb' libsvm_output '$output' featmap_file featmap.txt "
        echo "$cmd"
        eval $cmd > $log_dir/$cur_run_log-5_libsvm.log 2>&1
        if [ $? != 0 ]; then echo "ERROR: Failed to run gen libsvm!"; exit -1; fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    fi

    command_log_ts=`date -d@$ts +%Y%m%d_%H` 
    rm -f $log_dir/$command_log_ts.running
}

if [[ "$run_format_raw_cfb" == true || "$run_raw_cfb" == true || "$run_format_train_event" == true || "$run_train_data" == true || "$run_gen_libsvm" == true ]]; then
    for ((tmp_ts=first_ts; tmp_ts<=end_ts; tmp_ts+=7200)); do
        if [[ $tmp_ts -lt $cfb_ignore_ts ]]; then
            continue;
        fi

        process_cnt=`ls -l $log_dir/*.running | wc -l`
        echo "running processes $process_cnt"

        while [[ process_cnt -ge $PARALLEL ]]; do
            sleep 5
            process_cnt=`ls -l $log_dir/*.running | wc -l`
        done

        command_log_ts=`date -d@$tmp_ts +%Y%m%d_%H` 
        echo "start running for $command_log_ts"
        run_one_batch $tmp_ts > $log_dir/batch_$command_log_ts.log 2>&1 &
        echo $! > $log_dir/$command_log_ts.running
    done
fi

wait

if [[ "$run_format_data_by_day" == "true" ]]; then
    test_var="${train_libsvm_root?} ${end_ts?} ${start_ts?} ${test_start?}"

    merge_feature_ts=""
    test_ts=`date -d "$test_start" +%s`
    for ((tmp_ts=start_ts; tmp_ts<test_ts; tmp_ts+=7200)); do
        tmp_folder=`date -d@$tmp_ts +%Y%m%d_%H`
        if [[ "$merge_feature_ts" == "" ]]; then
            merge_feature_ts="$tmp_folder";
        else
            merge_feature_ts="$merge_feature_ts,$tmp_folder";
        fi
    done

    echo "formatting for training testing"
    train_path="${train_libsvm_root}/daily_data/{$merge_feature_ts}"

    merge_feature_ts=""
    for ((tmp_ts=test_ts; tmp_ts<=end_ts; tmp_ts+=7200)); do
        tmp_folder=`date -d@$tmp_ts +%Y%m%d_%H`
        if [[ "$merge_feature_ts" == "" ]]; then
            merge_feature_ts="$tmp_folder";
        else
            merge_feature_ts="$merge_feature_ts,$tmp_folder";
        fi
    done
    test_path="${train_libsvm_root}/daily_data/{$merge_feature_ts}"

    output_path="$train_libsvm_root/training_data"
    cmd="pig $PIG_PARAM -p job_name=${APP_NAME}_format_train -p train='$train_path' -p test='$test_path' -p OUTPUT='$output_path' format_xgboost_train_data.pig"
    echo "$cmd";
    eval $cmd
fi

