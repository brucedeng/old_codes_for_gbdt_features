
%default input1 /projects/news/model/training/merged_cfb/hi_feature_aggcfb_test/20160629/
%default input2 /projects/news/model/training/merged_cfb/hi_feature_aggcfb_new_user_up_v4/20160629/events_with_cfb_features
%default output /tmp/news_model/compare_merge_cfb_data

%DECLARE PIG_COL_TYPE "id:chararray,uid:chararray,content_id:chararray,ts:long,label:chararray,city:chararray,publisher:chararray,cp_update_time:chararray,d_raw:chararray,u_raw:chararray,cp_type:chararray,publish_time:chararray,u_age:chararray,u_gender:chararray,cfb_json:chararray,dwelltime:float,listpagedwell:float,declare_match:int,declare_cnt:int"
%DECLARE PIG_COL "id,uid,content_id,ts,label,city,publisher,cp_update_time,d_raw,u_raw,cp_type,publish_time,u_age,u_gender,cfb_json,dwelltime,listpagedwell,declare_match,declare_cnt"


data1 = load '$input1' as (fields:chararray,cfb:chararray);
data2 = load '$input2' as (${PIG_COL_TYPE});

data1 = FOREACH data1 GENERATE FLATTEN(STRSPLIT(fields,'\u0001',5)) as (id:chararray,uid:chararray,content_id:chararray,join_ts:chararray), cfb;
data2 = FOREACH data2 GENERATE uid, content_id, ts, cfb_json;
jnd = JOIN data1 by (uid,content_id) full outer, data2 by (uid,content_id);

store jnd into '$output';