-- REGISTER '../*.jar';
-- REGISTER 'ts_util.py' USING jython AS timefunc;

rmf $output
-- set default_parallel 400;

raw_cfb = LOAD '$NEW_CFB' AS (
    pid:chararray,
    ts : long,
    feature_type : chararray,
    feature_name : chararray,
    click_sum : double,
    view_sum : double
);
raw_cfb = FOREACH raw_cfb GENERATE pid,ts,feature_type, REPLACE(feature_name, '\u0001', '\u0002') as feature_name:chararray, click_sum, view_sum;

raw_cfb_old = load '$OLD_CFB' as (
    ts : long,
    feature_type : chararray,
    feature_name : chararray,
    click_sum : double,
    view_sum : double
);

jnd = join raw_cfb by (ts,feature_type, feature_name) full outer, raw_cfb_old by (ts,feature_type, feature_name);

result = FOREACH jnd GENERATE
    (raw_cfb::ts is null? raw_cfb_old::ts : raw_cfb::ts ) as ts,
    (raw_cfb::feature_type is null? raw_cfb_old::feature_type : raw_cfb::feature_type ) as feature_type,
    (raw_cfb::feature_name is null? raw_cfb_old::feature_name : raw_cfb::feature_name ) as feature_name,
    raw_cfb::click_sum as offline_click,
    raw_cfb::view_sum as offline_view,
    raw_cfb_old::click_sum as nrt_click,
    raw_cfb_old::view_sum as nrt_view;

result = order result by feature_type, feature_name;

-- rmf $output/full_jnd
store result into '$output/full_jnd';

-- result = load '$output/full_jnd' as (feature_type:chararray, feature_name:chararray, click1:float, pv1:float, click2:float, pv2:float);

empty1 = FILTER result by offline_click is null and offline_view is null;
empty1_grp = FOREACH ( GROUP empty1 by feature_type ) GENERATE $0 as feature_type, COUNT($1) as cnt;
store empty1_grp into '$output/empty1';

empty2 = FILTER result by nrt_click is null and nrt_view is null;
empty2_grp = FOREACH ( GROUP empty2 by feature_type ) GENERATE $0 as feature_type, COUNT($1) as cnt;
store empty2_grp into '$output/empty2';

diff_events = filter result by offline_click is not null and offline_view is not null and nrt_click is not null and nrt_view is not null;
event_diff = FOREACH diff_events GENERATE ts, feature_type, feature_name,offline_click-nrt_click as click_diff, offline_view - nrt_view as view_diff, offline_click, offline_view, nrt_click, nrt_view;

store event_diff into '$output/diff';
