REGISTER '../lib/*.jar';
REGISTER '../lib/feature_log_util.py' USING jython AS featurelog_utils;
REGISTER '../lib/feature_udf.py' USING jython AS myudf;

set mapred.create.symlink yes;
set mapred.cache.files $featmap_file#feature.map;
set default_parallel 200;
set fs.s3a.connection.maximum 1000;

set mapred.max.map.failures.percent 5;

--set mapred.min.split.size 134217728;
--set mapred.max.split.size ;
--set pig.maxcombinedsplitsize 26843545600;

%DECLARE PIG_COL_TYPE "id:chararray,rid:chararray,uid:chararray,content_id:chararray,ts:long,label:int,city:chararray,publisher:chararray,      cp_update_time:chararray,d_raw:chararray,u_raw:chararray,u_app_inferred_cat:{t:(catid:chararray,weight:float)},cp_type:chararray,publish_time:chararray,u_age:chararray,u_gender:chararray,cfb_json:chararray,dwelltime:float,listpagedwell:float"


raw_rcv_log = LOAD '$INPUT_EVENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate
    json,
    (json#'app_lan' is null ? 'en' : ( json#'app_lan' MATCHES 'hi.*' ? 'hi' : 'en' )) as app_lan,
    json#'contentid' as content_id:chararray,
    json#'aid' as uid:chararray,
    json#'ip' as ip:chararray,
    json#'servertime_sec' as eventtime,
    json#'pid' as pid:chararray,
    json#'ctype' as ctype,
    (int)json#'ext'#'dwelltime' AS dwelltime:int,
    json#'act' as act,
    json#'upack'#'exp' as exp,
    featurelog_utils.parse_rid(json#'cpack') as rid:chararray;

-- raw_rcv_log = filter raw_rcv_log by pid == '11' and  ( ctype is not null and (ctype=='1' or ctype=='0x01' ) ) and eventtime >= myfunc.date2timestamp('$START_DATE_TIME',$WND,'%Y%m%d%H') and eventtime <= myfunc.date2timestamp('$END_DATE_TIME', $WND,'%Y%m%d%H') and act is not null and ( act == '1' or act == '2' or act=='3' or act=='4' ) and json#'scenario'#'level1_type' =='1' and json#'scenario'#'level1' == '1' and app_lan=='$app_lan';

raw_rcv_log = filter raw_rcv_log by pid == '11' and  ( ctype is not null and (ctype=='1' or ctype=='0x01' ) ) and act is not null and ( act == '1' or act == '2' or act=='3' or act=='4' ) and json#'scenario'#'level1_type' =='1' and json#'scenario'#'level1' == '1' and app_lan=='$app_lan';

click_view_data = FOREACH raw_rcv_log GENERATE
    content_id, uid, rid,
    -- myfunc.get_city_name(ip) as city_name,
    (long)(eventtime) as original_time,
    --(long)(eventtime)/$WND * $WND as updatetime,
    pid as productid,
    ((dwelltime is null or dwelltime<0) ? 0 : dwelltime ) as dwelltime,
    (act=='1'? 'view':( act=='2'? 'click':( act=='4'? 'read': (act=='3'? 'listpagedwell':'unknown')))) as event_type;

log_click_view = FOREACH click_view_data GENERATE
      uid AS uid,
      content_id AS content_id,
      rid as rid,
      original_time as ts,
      --updatetime AS ts,
      -- city_name AS city,
      event_type AS event_type,
      productid as productid,
      (event_type=='read' ? dwelltime:0) as dwelltime:int,
      (event_type=='listpagedwell' ? dwelltime:0 ) as listpagedwell:int,
      1.0 AS expression;

log_click_view = FILTER log_click_view BY (content_id matches '^\\d+$') and (ts != -1);

raw_event_group = GROUP log_click_view BY (uid, content_id, productid, rid); 

raw_event_out = FOREACH raw_event_group {
    ts = MIN( $1.ts );
    --join_ts = ts/$WND * $WND - $WND;
    click_event = FILTER $1 BY (event_type == 'click');
    click_count = COUNT(click_event);
    label = (click_count > 0 ? 1: 0);
    dwelltime = SUM($1.dwelltime);
    listpagedwell = SUM($1.listpagedwell);
    -- city = MIN($1.city);
GENERATE
    $0.uid AS uid,
    $0.content_id AS content_id,
    $0.productid as productid,
    $0.rid as rid,
    ts AS ts,
    -- city AS city,
    (dwelltime > 600 ? 600:dwelltime) as dwelltime,
    (listpagedwell > 600 ? 600:listpagedwell) as listpagedwell,
    label AS label;
}

--rmf /tmp/chenkehan/raw_event_out
--store raw_event_out into '/tmp/chenkehan/raw_event_out';

feature_log = LOAD '$INPUT_FEATURE_LOG' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

feature_log = FOREACH feature_log GENERATE 
    json#'req_id' as rid:chararray,
    json#'prod_id' as pid:chararray,
    json#'predictor_id' as predictor_id:chararray,
    json#'now' as ts:long,
    json#'doc_size' as doc_size:long,
    json#'uid' as uid:chararray,
    json#'up' as up:map[],
    FLATTEN(json#'docs') as features;

feature_log_flat = FOREACH feature_log GENERATE
    uid, features#'id' as content_id, pid, rid, predictor_id, ts, doc_size, up, features;

feature_log_dist = FOREACH ( GROUP feature_log_flat by (uid,content_id,pid,rid) ) {
    data_sorted = ORDER $1 BY ts;
    data_limit = LIMIT data_sorted 1;
    GENERATE FLATTEN(data_limit) as (uid, content_id, pid, rid, predictor_id, ts, doc_size, up, features);
}

feature_log_format = FOREACH feature_log_dist GENERATE 
    uid, content_id, pid, rid, 
    featurelog_utils.format_feature_log(features) as features;

--rmf /tmp/chenkehan/feature_log_format
--store feature_log_format into '/tmp/chenkehan/feature_log_format';

--set pig.splitCombination false;
-- join feature log and events
data_jnd = join raw_event_out by (uid, content_id, productid,rid), feature_log_format by (uid, content_id, pid, rid);

sample_featurelog = FOREACH data_jnd GENERATE
    raw_event_out::uid as uid,
    raw_event_out::content_id as content_id,
    raw_event_out::label as label,
    feature_log_format::features as features,
    raw_event_out::dwelltime as dwelltime,
    raw_event_out::rid as rid,
    raw_event_out::ts as event_ts;


-- load offline feature
raw = LOAD '$input_offline_cfb' AS (
  ${PIG_COL_TYPE}
);

sample_offline = FOREACH raw {
    doc_age = myudf.gen_doc_age(ts, publish_time);
    time_of_day = myudf.gen_time_of_day(ts);
    day_of_week = myudf.gen_day_of_week(ts);
    --static_features = myudf.gen_static_features(u_raw, d_raw, TOMAP('DOC_AGE',doc_age,'TIME_OF_DAY',time_of_day,'DAY_OF_WEEK',day_of_week), u_app_inferred_cat, null);
    static_features = myudf.gen_app_rel(u_app_inferred_cat, d_raw);

    cfb_features = myudf.parse_and_calc_cfb_feature(cfb_json);
    GENERATE
        uid, content_id, label, rid, featurelog_utils.merge_features(static_features,cfb_features) as offline_features;
        --static_features, u_app_inferred_cat, d_raw;
}

--rmf /tmp/chenkehan/test_sample_offline;
--store sample_offline into '/tmp/chenkehan/test_sample_offline';
--sample_offline = load '/tmp/chenkehan/test_sample_offline' as  (uid:chararray, content_id:chararray, label:int, rid:chararray, offline_features:map[]);

training_data = foreach (join sample_featurelog by (uid,content_id,rid,label), sample_offline by (uid,content_id,rid,label) PARALLEL 400) generate 
    sample_featurelog::uid as uid,
    sample_featurelog::content_id as content_id,
    sample_featurelog::label as label,
    sample_featurelog::dwelltime as dwelltime,
    featurelog_utils.format_train_data(sample_offline::label, sample_offline::offline_features, sample_featurelog::features) as train_data, 
    sample_featurelog::event_ts AS ts;

rmf $output;
store training_data into '$output';

