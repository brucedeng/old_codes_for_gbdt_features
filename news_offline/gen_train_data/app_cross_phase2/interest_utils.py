# -*- coding:utf-8 -*-
import os
import datetime
import time
import math
import hashlib

if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper
    import json
else:
    import com.xhaus.jyson.JysonCodec as json

@outputSchema("b:{t:(s:chararray)}")
def explode_str(input_str,delim=','):
    if input_str is None or input_str=='':
        return []

    fields = input_str.split(delim)
    return [(_k.strip(),) for _k in fields ]

def get_category_tree():
    if get_category_tree.tree is None:
        tree_file = open('cat_tree.txt')
        get_category_tree.tree = {}
        for line in tree_file:
            line = line.rstrip('\n')
            fields = line.split('\t')
            curid = fields[0]
            rootid = fields[1]
            get_category_tree.tree[curid] = rootid
    return get_category_tree.tree
get_category_tree.tree = None



#needed_interests = set('1000849,6000007,1000001,1000661,1000123,1000031,1000931,1000076'.split(','))
needed_interests = set('10000000,10000001,10000007,10000008,10000009,1000001,10000010,1000031,1000111,1000123,1000267,1000288,1000298,1000374,1000395,1000560,1000620,1000637,1000661,1000721,1000742,1000780,1000931,1000992,1001039,1001091,5000000,6000007,6000012'.split(','))

@outputSchema('cat:chararray')
def get_l1_category(catid):
    cat_tree = get_category_tree()
    if catid is None or catid not in cat_tree:
        return None
    else:
        curid = catid
        while curid not in needed_interests and curid in cat_tree:
            curid = cat_tree[curid]
        if curid not in needed_interests:
            return None
        else:
            return curid

@outputSchema('(groupid:chararray, appname:chararray)')
def split_cat_cat_name(featurename):
    fields = featurename.split('')
    groupid = fields[0]
    appname = fields[1]
    return (groupid,appname)




@outputSchema('cat:chararray')
def get_l1_category_withoutfilter(catid):
    cat_tree = get_category_tree()
    if catid is None or catid not in cat_tree:
        return catid
    else:
        curid = catid
        while curid != '1000849' and curid in cat_tree:
            if cat_tree[curid] == "root":
                break
            curid = cat_tree[curid]
        return curid

def match_declare(catid):
    cat_tree = get_category_tree()
    if catid is None or catid not in cat_tree:
        return None
    else:
        curid = catid
        while curid not in needed_interests and curid in cat_tree:
            curid = cat_tree[curid]
        if curid not in needed_interests:
            return None
        else:
            return curid

@outputSchema('(matched:int,cnt:int)')
def get_category_matched(d_raw, interests):
    if d_raw is None or interests is None or interests == '':
        return (0,0)

    max_cat = ''
    try:
        content=json.loads(d_raw)
        categories=content['categories']
        max_wt = 0
        for item in categories:
            item_map = item[0]
            wt = float(item_map.get('weight',0.0))
            if wt > max_wt:
                max_cat = item_map["name"]
                max_wt = wt
    # print max_cat
    except:
        print d_raw
    declared = match_declare(max_cat)
    interests = set([_k.strip() for _k in interests.split(',')])
    if declared is not None and declared in interests:
        return (max_wt,len(interests))
    else:
        return (0, len(interests))

@outputSchema('(matched:int,cnt:int,declare_cat:int)')
def get_category_matched_features(d_raw, interests):
    # if d_raw is None or interests is None or interests == '':
    #     return (0,0)

    max_cat = ''
    declare_cat = 0
    try:
        content=json.loads(d_raw)
        categories=content['categories']
        max_wt = 0
        for item in categories:
            item_map = item[0]
            wt = float(item_map.get('weight',0.0))
            if wt > max_wt:
                max_cat = item_map["name"]
                max_wt = wt
    # print max_cat
    except:
        print d_raw

    declared = match_declare(max_cat)
    declare_cat = 0 if declared is None else 1

    if interests is None or interests == '':
        return (0, 0, declare_cat)

    interests = set([_k.strip() for _k in interests.split(',')])

    if declared is not None and declared in interests:
        return (1,len(interests),declare_cat)
    else:
        return (0, len(interests),declare_cat)


needed_interests_list= sorted(needed_interests)
@outputSchema("b:{t:(s:chararray)}")
def cube_interest_str(input_str,delim=','):
    if input_str is None:
        input_str=''
    result = []

    fields = input_str.split(delim)
    field_set = set([_k.strip() for _k in fields ])
    for curid in needed_interests:
        if curid in field_set:
            result.append((curid,))
        else:
            result.append(('!' + curid , ))
    return result

@outputSchema("s:chararray")
def get_exp_id(input_str):
    try:
        data = json.loads(input_str)
        result = data.get('exp')
        return result
    except:
        return ''

@outputSchema("(entropy:float,cnt:int)")
def calc_entropy(cat_bag):
    cat_map = {}
    total_cnt = 0
    for item in cat_bag:
        cur_cat = item[0]
        if cur_cat is None or len(cur_cat) == 0:
            continue
            # category = ''
        else:
            category = cur_cat[0][0]
        if category not in cat_map:
            cat_map[category] = 0.0
        cat_map[category] += 1
        total_cnt += 1

    result = 0.0
    if total_cnt == 0:
        return result
    for key,value in cat_map.items():
        prob = value/total_cnt
        result -= prob * math.log(prob,2)

    return (result,total_cnt)

@outputSchema("(uniq_kwd_cnt:int,kwd_cnt:int,article_cnt:int)")
def calc_kwd_cnt(kwd_bag):
    article_cnt = len(kwd_bag)
    uniq_kwd_set = set()
    kwd_cnt = 0
    for item in kwd_bag:
        # print item
        cur_kwd = item[0]
        if cur_kwd is not None and len(cur_kwd) > 0:
            # print cur_kwd
            for kwd_tuple in cur_kwd:
                # print kwd_tuple
                uniq_kwd_set.add(kwd_tuple[0])
                kwd_cnt += 1
    return (int(len(uniq_kwd_set)), kwd_cnt, article_cnt )

def get_category_conf():
    if get_category_conf.tree is None:
        tree_file = open('cat_conf.txt')
        get_category_conf.tree = {}
        for line in tree_file:
            line = line.rstrip('\n')
            fields = line.split('\t')
            curid = fields[0]
            rootid = fields[1].strip('/')
            get_category_conf.tree[curid] = rootid
    return get_category_conf.tree
get_category_conf.tree = None

def map_category_to_name(cat_id):
    cat_conf = get_category_conf()
    if cat_id in cat_conf:
        return cat_conf[cat_id]
    else:
        return cat_id

@outputSchema("cat:chararray")
def get_category_names(cat_bag):
    if cat_bag is None:
        return ''
    result = None
    for item in cat_bag:
        cat_id = item[0]
        print cat_id
        if result is None:
            result = str(map_category_to_name(cat_id))
        else:
            result += (',' + str(map_category_to_name(cat_id)))
    return result

@outputSchema("name:chararray")
def get_l1_category_name(cat_id):
    if cat_id is None:
        return ''
    result = map_category_to_name(cat_id)
    fields = result.split('/')
    return fields[0]

@outputSchema("t:(f1:chararray,f2:chararray)")
def split_chararray_ctrlb(original_char):
    result = original_char.split(chr(2),1)
    return tuple(result)

if __name__=='__main__':
    print get_category_names([('1000031',),('1000001',)])
    print get_category_matched('{"categories":[[{"weight":"0.715739","name":"1000007"}],[{"weight":"0.166629","name":"1000931"}],[{"weight":"0.050253","name":"1000963"}],[{"weight":"0.040931","name":"1000288"}],[{"weight":"0.026449","name":"1000076"}]],"keywords":[[{"weight":"0.026345","name":"india"}],[{"weight":"0.023282","name":"pyramid"}],[{"weight":"0.022751","name":"bizarre"}],[{"weight":"0.022261","name":"movie"}],[{"weight":"0.019228","name":"baahubali"}],[{"weight":"0.015442","name":"ntr"}],[{"weight":"0.014011","name":"gopichand"}],[{"weight":"0.013362","name":"celebration"}],[{"weight":"0.012203","name":"shah rukh khan"}],[{"weight":"0.011712","name":"double"}],[{"weight":"0.011685","name":"couple"}],[{"weight":"0.01155","name":"puri jagannadh"}],[{"weight":"0.011365","name":"tamannaah"}],[{"weight":"0.010761","name":"rohit sharma"}],[{"weight":"0.01073","name":"amy jackson"}]]}',"1000001,6000007")
    print calc_entropy([([('1',),('2',)],),([('2',)],),([('1',)],)])
    print cube_interest_str('1000001,1000661,6000007')
    print cube_interest_str('1000849')
    print get_l1_category_withoutfilter('1000781')
    # print get_l1_category('1000829')
    # print get_l1_category('1000839')
