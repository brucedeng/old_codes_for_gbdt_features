--%DEFAULT SAMPLE_RATIO 0.02
%DEFAULT WEIGHT_INDEX 3
%DEFAULT SAMPLE_RATIO 1
%DEFAULT LABEL_INDEX 5

set mapred.create.symlink yes;
set mapred.cache.files $featmap_file#feature.map,$CAT_TREE#cat_tree.txt;

%DECLARE PIG_COL_TYPE "id:chararray,rid:chararray,uid:chararray,content_id:chararray,ts:long,label:chararray,city:chararray,publisher:chararray,cp_update_time:chararray,d_raw:chararray,u_raw:chararray,u_app_inferred_cat:{t:(catid:charrarray,weight:float)},cp_type:chararray,publish_time:chararray,u_age:chararray,u_gender:chararray,cfb_json:chararray,dwelltime:float,listpagedwell:float,declare_match:int,declare_cnt:int"
%DECLARE PIG_COL "id,rid,uid,content_id,ts,label,city,publisher,cp_update_time,d_raw,u_raw,u_app_inferred_cat,cp_type,publish_time,u_age,u_gender,cfb_json,dwelltime,listpagedwell,declare_match,declare_cnt"

REGISTER '../lib/*.jar';
REGISTER '../lib/feature_udf.py' USING jython AS myudf;
--REGISTER '../lib/interest_utils.py' USING jython AS interest;
--DEFINE HEADER `python append_header.py` ship('append_header.py');
 
DEFINE featmap(raw_training_data) RETURNS output_data {
    featmap_out = FOREACH $raw_training_data GENERATE FLATTEN(myudf.gen_train_featmap(static_features, cfb_features)) as fname;
    featmap_dist = DISTINCT featmap_out;
    $output_data = FOREACH ( GROUP featmap_dist ALL ) {
        data = ORDER $1 BY fname;
        GENERATE FLATTEN(myudf.format_featmap(data)) as (fid,fname,ftype);
    }
}

DEFINE gen_data(raw_training_data) RETURNS output_data {
    
    $output_data = FOREACH $raw_training_data GENERATE
        uid, content_id, label, dwelltime,
        myudf.format_train_data(label,static_features, cfb_features) AS train_data,
        ts as ts;
        -- static_features as static_features, 
        -- cfb_features as cfb_features;
}

DEFINE gen_data_tsv(raw_training_data) RETURNS output_data {
    
    $output_data = FOREACH $raw_training_data GENERATE
        uid, 
        content_id, 
        label,
        myudf.format_train_data_tsv(label,static_features, cfb_features) AS train_data,
        static_features as static_features, 
        cfb_features as cfb_features;
    
    --output_seq = foreach (group out ALL) generate flatten(out);
    --$output_data = Stream output_seq THROUGH HEADER;
}

-- DEFINE MY_SAMPLE `python sample_negative_train_data.py $SAMPLE_RATIO $LABEL_INDEX $WEIGHT_INDEX` ship('sample_negative_train_data.py');

rmf $output
raw = LOAD '$input' AS (
  ${PIG_COL_TYPE}
);

-- data_sample = STREAM raw THROUGH MY_SAMPLE as (label:int, nsample, sample_weight, ${PIG_COL_TYPE}); 

data_sample_train = FOREACH raw {
    doc_age = myudf.gen_doc_age(ts, publish_time);
    time_of_day = myudf.gen_time_of_day(ts);
    day_of_week = myudf.gen_day_of_week(ts);
    static_features = myudf.gen_static_features_withapp(u_raw, u_app_inferred_cat, d_raw, TOMAP('DOC_AGE',doc_age,'TIME_OF_DAY',time_of_day,'DAY_OF_WEEK',day_of_week), null);

    cfb_features = myudf.parse_and_calc_cfb_feature(cfb_json);
    GENERATE
        uid, content_id, label, dwelltime, static_features as static_features, cfb_features as cfb_features, ts;
}

-- rmf $output/data_sample_train
-- STORE data_sample_train into '$output/data_sample_train';

output_data = $METHOD(data_sample_train);

store output_data INTO '$output';

