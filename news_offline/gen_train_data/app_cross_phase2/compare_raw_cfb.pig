
%DECLARE cfb_app '/projects/news/model/tmp/app-training/raw_cfb/en_offline_cfb_raw/20160508/'
%DECLARE cfb_old '/projects/news/model/tmp/old-training/raw_cfb/en_offline_cfb_raw/20160508/'

raw_cfb_old = LOAD '$cfb_old' AS (
    ts : long,
    feature_type : chararray,
    feature_name : chararray,
    click_sum : double,
    view_sum : double
);

raw_cfb_app = LOAD '$cfb_app' AS (
    ts : long,
    feature_type : chararray,
    feature_name : chararray,
    click_sum : double,
    view_sum : double
);

res = foreach (join raw_cfb_old by (ts, feature_type, feature_name) LEFT, raw_cfb_app by (ts, feature_type, feature_name)){

    generate raw_cfb_old::ts as ts,
            raw_cfb_old::feature_type as feature_type,
            raw_cfb_old::feature_name as feature_name,
            raw_cfb_old::click_sum as click_sum,
            raw_cfb_old::view_sum as view_sum,
            raw_cfb_app::click_sum as click_sum_app,
            raw_cfb_app::view_sum as view_sum_app;
};

rmf /tmp/chenkehan/cfb_compare_res;
store res into '/tmp/chenkehan/cfb_compare_res';


