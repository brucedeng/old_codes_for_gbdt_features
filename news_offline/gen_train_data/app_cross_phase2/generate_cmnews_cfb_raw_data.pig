REGISTER '../lib/*.jar';
REGISTER '../lib/utils.py' USING jython AS myfunc;

%DECLARE ORI_COL "eventtime, act, aid, contentid, appv,brand, ccode, ip, net, model, pf, pid, v"

%DECLARE TYPE_COL "eventtime:long, act:chararray, aid:chararray, contentid:chararray, appv:chararray , brand:chararray, ccode:chararray,ip:chararray, net:chararray, model:chararray, pf:chararray, pid:chararray, v:chararray"

set mapred.create.symlink yes;
set mapred.cache.files $CONFIG#config.json,$CITY_DATA#ip_city.data
--%default install_app_dump '/user/news_model/tmp/indian_news/test/install/20160420';
-- DEFINE rcvLoader com.ijinshan.cmcm.rcv.log.loader.rcvLogLoader('$ORI_COL','&', '=');
set default_parallel 400;

DEFINE dedup_content (click_views) RETURNS log_click_view {
    log_click_view_tmp = FILTER $click_views BY (content_id matches '^\\d+$') and (ts != -1);
    $log_click_view = FOREACH ( GROUP log_click_view_tmp by (uid, content_id, ts, event_type, app_lan) ) {
        result = order $1 by city;
        data_limit = limit result 1;
        GENERATE FLATTEN(data_limit) as (uid, content_id, ts, city, event_type,expression, app_lan);
    }
}

DEFINE no_dedup_content (click_views) RETURNS log_click_view {
    $log_click_view = FILTER $click_views BY (content_id matches '^\\d+$') and (ts != -1);
}

raw_rcv_log = LOAD '$event' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate
    json,
    (json#'app_lan' is null ? 'en' : ( json#'app_lan' MATCHES 'hi.*' ? 'hi' : 'en' )) as app_lan,
    json#'contentid' as content_id,
    json#'aid' as uid,
    json#'ip' as ip,
    json#'servertime_sec' as eventtime,
    json#'pid' as pid,
    json#'ctype' as ctype,
    json#'act' as act;

raw_rcv_log = filter raw_rcv_log by act is not null and pid=='11' and ( ctype is null or ctype=='' or ctype=='1' or ctype=='0x01' ) and ( act == '1' or act == '2' ) and json#'scenario'#'level1_type' =='1' and json#'scenario'#'level1' == '1' and app_lan=='$app_lan';

click_view_data = FOREACH raw_rcv_log GENERATE
    content_id, uid, 
    myfunc.get_city_name(ip) as city_name,
    (long)(eventtime/$WND) * $WND as updatetime,
    (act=='1'? 'view':'click') as event_type,
    app_lan;

log_click_view = FOREACH click_view_data GENERATE
      uid AS uid,
      content_id AS content_id,
      updatetime AS ts,
      city_name AS city,
      event_type AS event_type,
      1.0 AS expression,
      app_lan as app_lan;

-- log_click_view = FILTER log_click_view BY (content_id matches '^\\d+$') and (ts != -1);

-- log_click_view = FOREACH ( GROUP log_click_view by (uid, content_id, ts, event_type, app_lan) ) {
--     result = order $1 by city;
--     data_limit = limit result 1;
--     GENERATE FLATTEN(data_limit) as (uid, content_id, ts, city, event_type,expression, app_lan);
-- }

log_click_view = $DEDUP (log_click_view);
-- pre process content
raw_content = LOAD '$cp_dump' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_content = foreach raw_content generate  
  json#'item_id' as content_id:chararray,
  json#'title_md5' as title_md5:chararray,
  json#'group_id' as groupid:chararray,
  REPLACE(json#'publisher','(\r\n|\n|\t|\r)','') as publisher:chararray,
  (float)json#'newsy_score' as newsy_score:float,
  json#'type' as type:chararray,
  (int)(json#'source_type') as source_type:int,
  (long)json#'publish_time' as publish_time:long,
  (long)json#'update_time' as update_time:long,
  (int)json#'image_count' as image_count:int,
  (int)json#'word_count' as word_count:int,
  json#'categories' as categories:{t1:(y:map[])},
  json#'entities' as keywords:{t1:(y:map[])};

raw_content_filter = FILTER raw_content by (type=='article' or type=='slideshow' or type=='photostory');
raw_content = FOREACH raw_content_filter GENERATE content_id,title_md5, groupid, publisher, newsy_score, type, publish_time, update_time, image_count, word_count,
    categories as categories:{t1:(y:map[])},
    keywords as keywords;

raw_content_distinct = foreach (group raw_content by content_id) {
    
    r = order raw_content by update_time DESC;
    l = limit r 1;
    generate flatten(l);

};

content_info = FOREACH raw_content_distinct {
  categories = FOREACH categories GENERATE
          (chararray)y#'name' AS name:chararray,
          (double)y#'weight' AS weight:double;
  keywords = FOREACH keywords GENERATE
          (chararray)y#'name' AS name:chararray,
          (double)y#'L1_weight' AS weight:double;
  
  categories = filter categories by name is not null and weight is not null;
  keywords = filter keywords by name is not null and weight is not null;

  GENERATE
      content_id AS content_id,
      title_md5 AS title_md5,
      groupid AS groupid,
      publisher AS publisher,
      update_time AS update_time,
      categories AS categories,
      keywords AS keywords;
}

-- normalized categories and keywords
content_info_norm = FOREACH content_info GENERATE
      content_id AS content_id,
      title_md5 AS title_md5,
      groupid AS groupid,
      publisher AS publisher,
      myfunc.normalize(categories,0,1,$D_NCAT,'L1') AS categories,
      myfunc.top(keywords, $D_NKEY) AS keywords;

-- rmf tmp/content_info_norm;
-- store content_info_norm into 'tmp/content_info_norm';

-- pre process user profile
raw_user_profile = LOAD '$up_dump' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_user_profile = FOREACH raw_user_profile GENERATE
     json#'uid' AS uid:chararray,
     (chararray)json#'age' AS age:chararray,
     (chararray)json#'gender' AS gender:chararray,
     json#'categories' AS categories:{t1:(y:map[])},
     json#'keywords' AS keywords:{t1:(y:map[])};

describe raw_user_profile;

user_profile_info = FOREACH raw_user_profile {
  categories = FOREACH categories GENERATE
          (chararray)y#'name' AS name:chararray,
          (double)y#'weight' AS weight:double;
  keywords = FOREACH keywords GENERATE
          (chararray)y#'name' AS name:chararray,
          (double)y#'weight' AS weight:double;
  GENERATE
      uid AS uid,
      age AS age,
      gender AS gender,
      categories AS categories,
      keywords AS keywords;
}

-- normalized categories and keywords
user_profile_info_norm = FOREACH user_profile_info GENERATE
      uid AS uid,
      age AS age,
      gender AS gender,
      myfunc.top(categories,$U_NCAT) AS categories,
      myfunc.top(keywords, $U_NKEY) AS keywords;

-- rmf tmp/user_profile_info_norm;
-- store user_profile_info_norm into 'tmp/user_profile_info_norm';

log_content_info_join = JOIN log_click_view BY (uid) LEFT,
                             user_profile_info_norm BY (uid);

log_content_info_for = FOREACH  log_content_info_join GENERATE
      log_click_view::content_id AS content_id,
      log_click_view::ts AS ts,
      log_click_view::uid AS uid,
      log_click_view::event_type AS event_type,
      log_click_view::expression AS expression,
      log_click_view::city AS city,
      log_click_view::app_lan as app_lan,
      user_profile_info_norm::age AS u_age,
      user_profile_info_norm::gender AS u_gender,
      user_profile_info_norm::categories AS u_categories,
      user_profile_info_norm::keywords AS u_keywords;


-- rmf /tmp/mengchong/raw_cfb/log_content_info_for_end;
-- store log_content_info_for into '/tmp/mengchong/raw_cfb/log_content_info_for_end';


raw_u_app_profile = LOAD '$install_app_dump' as (uid:chararray,app_cat:bag{t:(catid:chararray,weight:float)},inferred_cat:bag{t:(catid:chararray,weight:float)});

log_content_info_join = JOIN log_content_info_for BY (uid) left,
                             raw_u_app_profile by (uid);

log_content_info_for = FOREACH  log_content_info_join GENERATE     
      log_content_info_for::content_id AS content_id,
      log_content_info_for::ts AS ts,
      log_content_info_for::uid AS uid,
      log_content_info_for::event_type AS event_type,
      log_content_info_for::expression AS expression,
      log_content_info_for::city AS city,
      log_content_info_for::app_lan as app_lan,
      log_content_info_for::u_age AS u_age,
      log_content_info_for::u_gender AS u_gender,
      log_content_info_for::u_categories AS u_categories,
      log_content_info_for::u_keywords AS u_keywords,
      raw_u_app_profile::app_cat as u_app_cat,
      raw_u_app_profile::inferred_cat as u_inferred_cat;

-- rmf /tmp/mengchong/raw_cfb/log_content_info_for;
 -- store log_content_info_for into '/tmp/mengchong/raw_cfb/log_content_info_for';

cid_grp = GROUP log_content_info_for by content_id;
cid_cnt = FOREACH cid_grp GENERATE $0 as content_id, COUNT($1) as cnt;
cid_cnt = ORDER cid_cnt BY cnt desc;
-- store cid_cnt into 'tmp/cid_cnt';


log_content_user_info_join = JOIN log_content_info_for BY (content_id),
                                  content_info_norm BY (content_id) PARALLEL 400; -- USING 'replicated';

log_content_user_info = FOREACH log_content_user_info_join GENERATE
           log_content_info_for::content_id AS content_id,
           content_info_norm::title_md5 AS title_md5,
           content_info_norm::groupid AS groupid,
           log_content_info_for::ts AS ts,
           log_content_info_for::uid AS uid,
           log_content_info_for::event_type AS event_type,
           log_content_info_for::expression AS expression,
           log_content_info_for::city AS city,
           log_content_info_for::app_lan as app_lan,
           u_age AS u_age,
           u_gender AS u_gender,
           u_categories AS u_categories,
           u_keywords AS u_keywords,
           u_app_cat As u_app_cat,
           --u_inferred_cat AS u_inferred_cat,
           content_info_norm::publisher AS publisher,
           content_info_norm::categories AS d_categories,
           content_info_norm::keywords AS d_keywords;

-- rmf tmp/log_content_user_info;
-- store log_content_user_info into 'tmp/log_content_user_info';

log_content_user_info_feature = FOREACH log_content_user_info {
  fields_map = TOMAP('d_title_md5',title_md5,'d_groupid',groupid,'d_content_id',content_id,'d_publisher',publisher, 'd_category',d_categories,'d_keyword',d_keywords,'u_uid',uid,'u_city',city, 'u_category',u_categories,'u_keyword',u_keywords, 'u_age',u_age, 'u_gender', u_gender,'u_app_cat',u_app_cat,  'score',expression);
  GENERATE
      ts AS ts,
      event_type AS event_type,
      app_lan as app_lan,
      myfunc.gen_feature(fields_map,'all') AS  feature_bag:{t:(feature_type:chararray, feature_name:chararray, feature_weight:double,feature_score:double)};
    }

-- rmf tmp/log_content_user_info_feature;
-- store log_content_user_info_feature into 'tmp/log_content_user_info_feature';

content_user_features_flatten = FOREACH log_content_user_info_feature GENERATE
    ts AS ts,
    event_type AS event_type,
    app_lan as app_lan,
    FLATTEN(feature_bag) AS (feature_type, feature_name, feature_weight,feature_score);

content_user_features_group = GROUP content_user_features_flatten BY (ts, feature_type, feature_name, app_lan);

content_user_features = FOREACH content_user_features_group {
  click_event = FILTER $1 BY (event_type == 'click');
  view_event = FILTER $1 BY (event_type == 'view');
  click_sum = SUM(click_event.feature_score);
  click_sum = (click_sum is NULL ? 0 : click_sum);
  view_sum = SUM(view_event.feature_score);
  view_sum = (view_sum is NULL ? 0 : view_sum);
GENERATE
  FLATTEN(group) AS (ts, feature_type, feature_name, app_lan),
  click_sum AS click_sum,
  view_sum AS view_sum;
}

content_user_features = FOREACH content_user_features GENERATE ts, feature_type, feature_name, click_sum, view_sum, app_lan;
rmf $output
STORE content_user_features INTO '$output';

