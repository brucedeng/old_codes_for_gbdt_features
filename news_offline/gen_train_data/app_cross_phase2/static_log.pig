REGISTER '../lib/*.jar';
--REGISTER 'utils_new.py' USING jython AS myfunc;
register  '../lib/interest_utils.py' USING jython AS myfunc;
%default CAT_TREE 'hdfs://mycluster/projects/news/model/cfb/configs/en_cat_tree.txt';
set mapred.cache.files $CAT_TREE#cat_tree.txt;

%default input '/projects/news/model/cfb/training/raw_cfb/en_offline_cfb_raw_20160426/{20160423,20160424}/*';

--%default input '/projects/news/model/cfb/training/raw_cfb/en_offline_cfb_raw_20160426/20160423/p*';

raw_cfb = LOAD '$input' AS (
    ts : long,
    feature_type : chararray,
    feature_name : chararray,
    click_sum : double,
    view_sum : double
);
raw_cfb = filter raw_cfb by feature_type=='C_D_U_CATEGORY_APPCAT';
raw_cfb = group raw_cfb by feature_name;
raw_data = foreach raw_cfb generate group as feature_name, SUM($1.click_sum) as click,SUM($1.view_sum) as view ;

raw_data = foreach raw_data generate  feature_name,FLATTEN(myfunc.split_cat_cat_name(feature_name)) as (groupid,app), click, view;
raw_data = foreach raw_data generate  feature_name,groupid, app, click,view,(double)(click/view) as ctr;
raw_data = foreach raw_data generate feature_name,myfunc.get_l1_category(groupid) as L1_type,groupid, app, click,view,ctr;
raw_data = filter raw_data by view>100;

raw_data_base = group raw_data by L1_type ;
raw_data_base = foreach  raw_data_base generate group as type,SUM($1.click) as click,SUM($1.view) as view ;
raw_data_base = foreach raw_data_base generate type, click, view ,(double)(click/view) as ctr;

raw_data_end = foreach (JOIN  raw_data by L1_type, raw_data_base by type) generate 
    raw_data::L1_type  as type,
    raw_data::app as app,
    raw_data::view  as view,
    raw_data::click as click,
    raw_data::ctr as ctr,
    raw_data_base::ctr as ctr_base;



--raw_data = limit raw_data 10;
--dump  raw_data ;
--raw_data = order raw_data by  ctr desc ;
rmf /tmp/dulimei/tt_all_data;
store raw_data_end into '/tmp/dulimei/tt_all_data';

