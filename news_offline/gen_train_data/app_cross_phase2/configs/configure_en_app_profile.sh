
# basic settings
event_start=20160524;
event_end=20160527;
##train_sample_rate=1.0;
app_lan=en
# cfb_ignore_date=20160317
batch_dedup=true

# run processes
run_raw_cfb=false
run_merge_cfb=false

#1. run_training_data use offline cfb as features
#2. run_merge_training_data use both featurelog and offline cfb as 
run_training_data=false
run_merge_featurelog_data=true

run_split_data=false
run_featmap=false
run_format_data=false

# pig params
queue=experiment
#queue=offline
PIG_PARAM=" -Dmapred.job.queue.name=$queue";
TOP_PARAM="-p U_NCAT=5 -p U_NKEY=25 -p D_NCAT=5 -p D_NKEY=15 -p WND=600"

# cached files
local_config=config_new_train.json
local_city=../lib/ip_city.data
local_category=../lib/cat_tree.txt
#time='20160426'

config_file="hdfs://mycluster/projects/news/model/cfb/configs/en_config.json"
city_file="hdfs://mycluster/projects/news/model/cfb/configs/en_ip_city.data"
category_data="hdfs://mycluster/projects/news/model/cfb/configs/en_cat_tree.txt"

# data pathes
#kw_split=200

kw_split=50
if [[ "$app_lan" == "hi" ]]; then
    input_user_root="/projects/news/model/hindi_up"
else
    input_user_root="/projects/news/user_profile/data/V3/up_json"
fi
input_cp_template="/projects/news/cpp/feeder/in_cp_dump/%s/*/*/*.data"
#input_cp_template="/projects/news/model/experiments/merged_cp_multiple_days/20160321-previous"
input_interest_root="s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall"
input_user_app_root="hdfs://mycluster/projects/news/model/training/app_install_profile"

# output path
output_root="/projects/news/model/cfb/training"
raw_cfb_template="$output_root/raw_cfb/app_profile/${app_lan}_offline_cfb_raw/%s"
merged_cfb_root="$output_root/merged_cfb/app_profile/${app_lan}_raw_features_joined_with_cfb"
output_s3_path="s3://com.cmcm.instanews.usw2.prod/model/experiments/app_profile/${app_lan}_cfb_training_data_offline";
featmap_path=s3://com.cmcm.instanews.usw2.prod/model/experiments/app_profile/en_cfb_training_featmap/${event_start}-${event_end}
featmap_file_hdfs=s3://com.cmcm.instanews.usw2.prod/model/experiments/app_profile/en_cfb_training_featmap/${event_start}-${event_end}.txt


