# basic settings
event_start=20160627;
event_end=20160627;
pid=1;
country_lanuage="ru_RU,es_MX";
#PIG_USER=
#candidates:en_IN,en_US,en_UK,en_CA,hi_IN,ta_IN,fr_FR,es_ES,pt_BR,es_MX,de_DE,it_IT,ru_RU,id_ID,zh_TW

# run processes
run_raw_cfb=true
run_merge_cfb=true
run_training_data=true
run_split_data=true
run_format_data=true
# pig params
queue=offline

# data pathes
# up
OLD_IFS="$IFS";Other_mark=False;UP_path="";IFS=",";arr=($country_lanuage);IFS="$OLD_IFS"
for s in ${arr[@]}
do
    if [[ $s = 'ru_RU' ]]||[[ $s = 'en_US' ]]||[[ ${s#*_} = "IN" ]]; then
        UP_path="$UP_path,$s"
    elif [[ $Other_mark = 'False' ]]; then
        UP_path="$UP_path,others"
        Other_mark=True
    fi
done
up_lower=$(echo ${UP_path#*,} | tr '[A-Z]' '[a-z]')
input_user_root="/projects/news/user_profile/data/V4/up_json/{$up_lower}"
#cp
input_cp_template="/projects/news/cpp/feeder/in_cp_dump/%s/*/*/*.data"

# output path
app_lan_output=$(echo $country_lanuage | sed 's/,/_/g')
output_root="/projects/news/model/training_data/${event_start}_${event_end}_${app_lan_output}_${pid}"
raw_cfb_template="${output_root}/raw_cfb/%s"
merged_cfb_root="${output_root}/merged_cfb"
output_path="${output_root}/training_data_offline";
featmap_path="${output_root}"
featmap_file_hdfs="${output_root}.txt"


#something you barely need to edit 
tmp_file="/tmp/${PIG_USER}"
PIG_PARAM=" -Dmapred.job.queue.name=$queue -Dpig.temp.dir=$tmp_file";
cnty_lan_bool=true #add country_lanuage on feature or not
TOP_PARAM="-p U_NCAT=5 -p U_NKEY=25 -p D_NCAT=5 -p D_NKEY=15 -p WND=600"
balance_ratio=10
kw_split=200
batch_dedup=true
train_sample_rate=1.0
local_config=config.json
local_category=../lib/cat_tree.txt
config_file="hdfs://mycluster/projects/news/model/configs/config.json"
category_data="hdfs://mycluster/projects/news/model/configs/cat_tree.txt"
CNTY_LAN=$(echo $country_lanuage | sed 's/_//g' | sed 's/,/_/g' )