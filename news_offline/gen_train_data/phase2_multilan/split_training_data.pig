REGISTER '../lib/*.jar';
REGISTER '../lib/utils_multilan.py' USING jython AS myudf;
REGISTER '../lib/split_user.py' USING jython AS split_user;
set output.compression.enabled true;
set output.compression.codec org.apache.hadoop.io.compress.GzipCodec;
%DEFAULT SEED 34
%DEFAULT SPLIT_COUNT 5
set job.name '$job_name';

-- set default_parallel 500;

raw_training_data = LOAD '$INPUT' as (uid:chararray, content_id:chararray, label:int, dwelltime:float,
    train_data:chararray, ts:long);

train_data_with_bin = FOREACH raw_training_data GENERATE uid, content_id, label, dwelltime, train_data, ts,
            myudf.get_random_bin(uid,$SPLIT_COUNT,'$SEED') as bin, ((chararray)split_user.split_user_by_kwlen(train_data,$KW_SPLIT)) as usertype, split_user.get_word_count(train_data) as word_count;

train_data_with_bin = FILTER train_data_with_bin by word_count < 10000;

train_data_with_bin = FOREACH train_data_with_bin GENERATE uid, content_id, label, dwelltime, train_data, ts,
 CONCAT(usertype, CONCAT('_', bin)) as bin;

data_ordered = ORDER train_data_with_bin BY bin, uid, ts;

rmf $OUTPUT
store data_ordered into '$OUTPUT' using org.apache.pig.piggybank.storage.MultiStorage('$OUTPUT','6','none','\t');
