
train_config_file=$1
source $train_config_file

echo $event_start
echo $event_end

set -e
# test if all needed variables are assigned.
test_var="${PIG_PARAM?} ${TOP_PARAM?}"

if [[ "$run_raw_cfb" == "true" ]]; then
    # test if all needed variables are assigned.
    test_var="${local_config?} ${config_file?} ${input_user_root?} ${input_cp_template?} ${raw_cfb_template?} ${batch_dedup?}"

    cur_date=`date -d "+2 day ago $event_start" +%Y%m%d`;



    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do
        if [[ "a$cfb_ignore_date" != "a" ]]; then
            if [[ $cfb_ignore_date -gt $cur_date ]]; then
                cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
                continue
            fi
        fi
        # input_event="s3://iwarehouse/cmnow/news_rcv_log/$cur_date/*/*.rcv" 
        input_event="/projects/news/data/raw_data/{impression,click}/$cur_date/*/*"
        up_date=`date -d "+1 day ago $cur_date" +%Y%m%d`;

        input_user="$input_user_root/$up_date"


        ftr_start=`date -d "+4 day ago $cur_date" +%Y%m%d`;
        ftr_end=$cur_date;
        if [[ "a$cp_first_date" != "a" ]]; then
            if [[ $cp_first_date -gt $cur_date ]]; then
                ftr_end=$cp_first_date;
            fi
        fi
        agg_dates=$ftr_start
        for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done

        input_contents=`printf "$input_cp_template" "{$agg_dates}"`

        output=`printf "$raw_cfb_template" "$cur_date"`

        dedup=" -p DEDUP=no_dedup_content "
        if [[ "$batch_dedup" == true ]]; then
            dedup=" -p DEDUP=dedup_content "
        fi
        job_name=${PIG_USER}_generate_cmnews_cfb_raw_data.pig
        cmd="pig $PIG_PARAM $TOP_PARAM $dedup -p job_name=$job_name -p CONFIG=$config_file -p event='$input_event' -p cp_dump='$input_contents' -p up_dump='$input_user' -p output='$output' -p pid=$pid -p CNTY_LAN='$CNTY_LAN' -p cnty_lan_bool=$cnty_lan_bool generate_cmnews_cfb_raw_data.pig"
        echo $cmd
        eval $cmd;

        if [ $? != 0 ]; then echo "ERROR: Failed to run pipeline!"; exit -1; fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done
fi

if [[ "$run_merge_cfb" == "true" ]]; then
    # test if all needed variables are assigned.
    test_var="${local_config?} ${local_category?} ${config_file?}  ${category_data?} ${input_user_root?} ${input_cp_template?} ${raw_cfb_template?} ${merged_cfb_root?} "

    cur_date=$event_start

    hadoop fs -copyFromLocal -f ${local_config?} $config_file
    hadoop fs -copyFromLocal -f ${local_category?} $category_data

    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do
        if [[ "a$cfb_ignore_date" != "a" ]]; then
            if [[ $cfb_ignore_date -gt $cur_date ]]; then
                cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
                continue
            fi
        fi
        # input_event="s3://iwarehouse/cmnow/news_rcv_log/$cur_date/*/*.rcv" 
        input_event="/projects/news/data/raw_data/{impression,click,listpagedwelltime,readtime}/$cur_date/*/*"

        up_date=`date -d "+1 day ago $cur_date" +%Y%m%d`;
        input_user="$input_user_root/$up_date"

        ftr_start=`date -d "+4 day ago $cur_date" +%Y%m%d`;
        ftr_end=$cur_date;
        if [[ "a$cp_first_date" != "a" ]]; then
            if [[ $cp_first_date -gt $cur_date ]]; then
                ftr_end=$cp_first_date;
            fi
        fi
        agg_dates=$ftr_start
        for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done

        input_contents=`printf "$input_cp_template" "{$agg_dates}"`

        output="$merged_cfb_root/$cur_date"

        ftr_start=`date -d "+2 day ago $cur_date" +%Y%m%d`;
        ftr_end=$cur_date;
        agg_dates_cfb=$ftr_start
        for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates_cfb="$agg_dates_cfb,$agg_day"; done
        input_cfb=`printf "$raw_cfb_template" "{$agg_dates_cfb}"`

        # data_start_time="${cur_date}08"
        data_end_time=`date -d "$cur_date +1 days" +%Y%m%d`
        data_end_time="${data_end_time}00"

        data_start_time=${cur_date}00
        decay_factor=0.995
        n_decaywnd=2d
        job_name=${PIG_USER}_generate_cmnews_merged_cfb.pig
        cmd="pig $PIG_PARAM $TOP_PARAM -p job_name=$job_name -p CONFIG=$config_file -p CAT_TREE=$category_data  -p INPUT_EVENT='$input_event' -p INPUT_CONTENT='$input_contents' -p INPUT_USER='$input_user' -p INPUT_CFB='$input_cfb' -p OUTPUT='$output' -p START_DATE_TIME=$data_start_time -p END_DATE_TIME=$data_end_time -p DECAY_FACTOR=$decay_factor -p N_DECAYWND=$n_decaywnd  -p pid=$pid -p CNTY_LAN='$CNTY_LAN' -p cnty_lan_bool=$cnty_lan_bool generate_cmnews_merged_cfb.pig"
        echo $cmd
        eval $cmd;

        if [ $? != 0 ]; then echo "ERROR: Failed to run pipeline!"; exit -1; fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done
fi




if [[ "$run_training_data" == "true" ]]; then
    # test if all needed variables are assigned.
    test_var="${merged_cfb_root?} ${featmap_path?} ${featmap_file_hdfs?} ${local_category?} ${category_data?} ${output_path?}"

    hadoop fs -rm -skipTrash $featmap_file_hdfs || echo "file not exists"
    hadoop fs -copyFromLocal featmap.txt $featmap_file_hdfs
    if [ $? != 0 ]; then echo "Failed to put feature list onto hdfs!"; exit -1; fi

    hadoop fs -copyFromLocal -f ${local_category?} $category_data

    cur_date=$event_start;
    while [ $cur_date -le $event_end ];
    do
        if [[ "a$cfb_ignore_date" != "a" ]]; then
            if [[ $cfb_ignore_date -gt $cur_date ]]; then
                cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
                continue
            fi
        fi

        pre_date=`date -d "-1 day $cur_date" +%Y%m%d`;
        input_path=$merged_cfb_root/$cur_date/events_with_cfb_features
        output="${output_path}/$cur_date";

        job_name=${PIG_USER}_get_raw_train_data.pig

        cmd="pig $PIG_PARAM -p job_name=$job_name -p input='$input_path' -p output='$output' -p SAMPLE_RATIO=$train_sample_rate  -p CAT_TREE=$category_data -p METHOD=gen_data -p featmap_file=$featmap_file_hdfs get_raw_train_data.pig";
        echo $cmd;
        eval $cmd;

        if [ $? != 0 ]; then echo "Failed!"; exit -1; fi
        cur_date=`date -d "1 day $cur_date" +%Y%m%d`;
    done

    echo "Done";
 fi

if [[ "$run_split_data" == "true" ]]; then
    test_var="${output_path?} ${kw_split?}"

    ftr_start=$event_start
    ftr_end=$event_end
    agg_dates=$ftr_start
    for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done

    input_path="${output_path}/{$agg_dates}"
    output_path_split="${output_path}/split_events_$ftr_start-$ftr_end"
    if [[ a"$split_seed" == "a" ]];then
        split_seed=$RANDOM
    fi
    echo "spliting using seed $split_seed"
    job_name=${PIG_USER}_split_training_data.pig

    cmd="pig $PIG_PARAM -p job_name=$job_name -p SEED='$split_seed' -p KW_SPLIT='$kw_split' -p INPUT='$input_path' -p OUTPUT='$output_path_split' split_training_data.pig"
    echo "$cmd";
    eval $cmd
fi

if [[ "$run_format_data" == "true" ]]; then
    test_var="${output_path?} ${event_end?} ${event_start?}"

    echo "formatting for all users"
    train_path="$output_path/split_events_$event_start-$event_end/*_{0,1,2,3}"
    test_path="$output_path/split_events_$event_start-$event_end/*_4"
    output_path_libsvm="$output_path/split_events_$event_start-$event_end-libsvm"
    job_name=${PIG_USER}_format_xgboost_train_data.pig
    cmd="pig $PIG_PARAM  -p job_name=$job_name -p train='$train_path' -p test='$test_path' -p OUTPUT='$output_path_libsvm' -p balance_ratio='$balance_ratio' format_xgboost_train_data.pig"
    echo "$cmd";
    eval $cmd

    echo "formatting for light users"
    train_path="$output_path/split_events_$event_start-$event_end/0_{0,1,2,3}"
    test_path="$output_path/split_events_$event_start-$event_end/0_4"
    output_path_libsvm_l="$output_path/split_events_$event_start-$event_end-libsvm_lightuser"
    job_name=${PIG_USER}_format_xgboost_train_data_lightuser.pig
    cmd="pig $PIG_PARAM  -p job_name=$job_name -p train='$train_path' -p test='$test_path' -p OUTPUT='$output_path_libsvm_l' -p balance_ratio='$balance_ratio' format_xgboost_train_data.pig"
    echo "$cmd";
    eval $cmd

    echo "formatting for heavy users"
    train_path="$output_path/split_events_$event_start-$event_end/1_{0,1,2,3}"
    test_path="$output_path/split_events_$event_start-$event_end/1_4"
    output_path_libsvm_h="$output_path/split_events_$event_start-$event_end-libsvm_heavyuser"
    job_name=${PIG_USER}_format_xgboost_train_data_heavyuser.pig
    cmd="pig $PIG_PARAM -p job_name=$job_name -p train='$train_path' -p test='$test_path' -p OUTPUT='$output_path_libsvm_h' -p balance_ratio='$balance_ratio' format_xgboost_train_data.pig"
    echo "$cmd";
    eval $cmd
fi

