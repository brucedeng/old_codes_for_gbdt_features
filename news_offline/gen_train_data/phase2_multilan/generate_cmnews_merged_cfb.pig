REGISTER '../lib/*.jar';
REGISTER '../lib/utils_multilan.py' USING jython AS myfunc;
REGISTER '../lib/cfb_agg.py' USING jython AS cfb;
-- REGISTER 'interest_utils.py' USING jython AS interest;

%DECLARE ORI_COL "eventtime, act, aid, contentid, appv,brand, ccode,ip, net, model, pf, pid, v"
%DECLARE TYPE_COL "eventtime:long, act:chararray, aid:chararray, contentid:chararray, appv:chararray , brand:chararray, ccode:chararray, ip:chararray, net:chararray, model:chararray, pf:chararray, pid:chararray, v:chararray"

set output.compression.enabled true;
set output.compression.codec org.apache.hadoop.io.compress.GzipCodec;
set job.name '$job_name';

set mapred.create.symlink yes;
set mapred.cache.files $CONFIG#config.json;

-- ,$CAT_TREE#cat_tree.txt
set default_parallel 500;

-- DEFINE rcvLoader com.ijinshan.cmcm.rcv.log.loader.rcvLogLoader('$ORI_COL','&', '=');
-- raw_rcv_log = LOAD '$INPUT_EVENT' USING rcvLoader AS ($TYPE_COL);

raw_rcv_log = LOAD '$INPUT_EVENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate
    json,
    myfunc.concatfuntion(json#'country', json#'app_lan') as cnty_lan:chararray,
    json#'contentid' as content_id:chararray,
    json#'aid' as uid:chararray,
    json#'city' as city:chararray,
    json#'servertime_sec' as eventtime,
    json#'pid' as pid:chararray,
    json#'ctype' as ctype,
    (int)json#'ext'#'dwelltime' AS dwelltime:int,
    json#'act' as act,
    myfunc.parse_rid(json#'cpack') as rid:chararray;

raw_rcv_log = filter raw_rcv_log by pid == '$pid' and  ( ctype is not null and (ctype=='1' or ctype=='0x01' ) ) and myfunc.ismatch(cnty_lan,'$CNTY_LAN') and eventtime >= myfunc.date2timestamp('$START_DATE_TIME',$WND,'%Y%m%d%H') and eventtime <= myfunc.date2timestamp('$END_DATE_TIME', $WND,'%Y%m%d%H') and act is not null and ( act == '1' or act == '2' or act=='3' or act=='4' ) and (json#'scenario'#'level1' == '1' or (pid=='11' and json#'scenario'#'level1'=='10')) and json#'scenario'#'level1_type' =='1';

click_view_data = FOREACH raw_rcv_log GENERATE
    content_id, uid, rid,
    city as city_name,
    (long)(eventtime) as original_time,
    (long)(eventtime)/$WND * $WND as updatetime,
    pid as productid,
    ((dwelltime is null or dwelltime<0) ? 0 : dwelltime ) as dwelltime,
    (act=='1'? 'view':( act=='2'? 'click':( act=='4'? 'read': (act=='3'? 'listpagedwell':'unknown')))) as event_type,
    cnty_lan as cnty_lan;

log_click_view = FOREACH click_view_data GENERATE
      uid AS uid,
      content_id AS content_id,
      rid AS rid,
      original_time as original_ts,
      updatetime AS ts,
      city_name AS city,
      event_type AS event_type,
      productid as productid,
      (event_type=='read' ? dwelltime:0) as dwelltime:int,
      (event_type=='listpagedwell' ? dwelltime:0 ) as listpagedwell:int,
      1.0 AS expression,
      cnty_lan as cnty_lan;



log_click_view = FILTER log_click_view BY (ts != -1) and rid is not null;

raw_event_group = GROUP log_click_view BY (uid, content_id, productid, rid); 



raw_event_out = FOREACH raw_event_group {
    ts = MIN($1.ts);
    join_ts = ts/$WND * $WND - $WND;
    click_event = FILTER $1 BY (event_type == 'click');
    click_count = COUNT(click_event);
    label = (click_count > 0 ? 1: 0);
    dwelltime = SUM($1.dwelltime);
    listpagedwell = SUM($1.listpagedwell);
    city =  MAX($1.city);
    cnty_lan = MAX($1.cnty_lan);
GENERATE
    $0.uid AS uid,
    $0.content_id AS content_id,
    $0.productid as productid,
    $0.rid AS rid,
    ts AS ts,
    join_ts AS join_ts,
    city AS city,
    (dwelltime > 600 ? 600:dwelltime) as dwelltime,
    (listpagedwell > 600 ? 600:listpagedwell) as listpagedwell,
    label AS label,
    cnty_lan AS cnty_lan;
}

--rmf $OUTPUT/events_with_cfb_features
--STORE raw_event_out INTO '$OUTPUT/events_with_cfb_features';

raw_event_out_withid = FOREACH raw_event_out {
    id_tuple = TOTUPLE(uid,content_id,ts,productid,label,rid);
    id = myfunc.get_md5_id(id_tuple);
    GENERATE id as id, uid, content_id, productid, rid, ts, join_ts, city, cnty_lan, dwelltime, listpagedwell, label;
}

-- pre process content
raw_content = LOAD '$INPUT_CONTENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_content = foreach raw_content generate  
  json#'item_id' as content_id:chararray,
  json#'title_md5' as title_md5:chararray,
  json#'group_id' as groupid:chararray,
  REPLACE(json#'publisher','(\r\n|\n|\t|\r)','') as publisher:chararray,
  -- (float)json#'newsy_score' as newsy_score:float,
  json#'type' as type:chararray,
  (long)json#'publish_time' as publish_time:long,
  (long)json#'update_time' as update_time:long,
  (int)json#'source_type' as source_type:int,
  -- (int)json#'image_count' as image_count:int,
  -- (int)json#'word_count' as word_count:int,
  myfunc.gen_json_str(json) as d_raw:chararray,
  json#'l2_categories' as categories:{t1:(y:map[])},
  json#'entities' as keywords:{t1:(y:map[])};

raw_content_filter = FILTER raw_content by (type=='article' or type=='slideshow' or type=='photostory');
raw_content = FOREACH raw_content_filter GENERATE content_id,title_md5, groupid, publisher, type, publish_time, update_time, d_raw,
    categories as categories:{t1:(y:map[])},
    keywords as keywords;

raw_content_distinct = foreach (group raw_content by content_id PARALLEL 30) {
    
	r = order raw_content by update_time DESC;
	l = limit r 1;
	generate flatten(l);

};

content_info = FOREACH raw_content_distinct {
  categories = FOREACH categories GENERATE
          (chararray)y#'name' AS name:chararray,
          (double)y#'weight' AS weight:double;
  keywords = FOREACH keywords GENERATE
          (chararray)y#'name' AS name:chararray,
          (double)y#'L1_weight' AS weight:double;
  
  categories = filter categories by name is not null and weight is not null;
  keywords = filter keywords by name is not null and weight is not null;

  GENERATE
        content_id AS content_id,
        title_md5 AS title_md5,
        groupid AS groupid,
        publisher AS publisher,
        update_time AS update_time,
        categories AS categories,
        keywords AS keywords,
        d_raw AS d_raw,
        type as type,
        publish_time as publish_time;
}

-- normalized categories and keywords
content_info_norm = FOREACH content_info GENERATE
      content_id AS content_id,
      title_md5 AS title_md5,
      groupid AS groupid,
      publisher AS publisher,
      update_time AS update_time,
      d_raw AS d_raw,
      type as type,
      publish_time as publish_time,
      myfunc.normalize(categories,0,1,$D_NCAT,'L1') AS categories,
      myfunc.top(keywords, $D_NKEY) AS keywords;

-- rmf tmp/content_info_norm;
-- store content_info_norm into 'tmp/content_info_norm';

-- pre process user profile
raw_user_profile = LOAD '$INPUT_USER' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_user_profile = FOREACH raw_user_profile GENERATE
     json#'uid' AS uid:chararray,
     (chararray)json#'age' AS age:chararray,
     (chararray)json#'gender' AS gender:chararray,
     json#'categories' AS categories:{t1:(y:map[])},
     json#'keywords' AS keywords:{t1:(y:map[])},
     myfunc.gen_json_str(json) as u_raw:chararray;

describe raw_user_profile;

user_profile_info = FOREACH raw_user_profile {
  categories = FOREACH categories GENERATE
          (chararray)y#'name' AS name:chararray,
          (double)y#'weight' AS weight:double;
  keywords = FOREACH keywords GENERATE
          (chararray)y#'name' AS name:chararray,
          (double)y#'weight' AS weight:double;
  GENERATE
      uid AS uid,
      age AS age,
      gender AS gender,
      categories AS categories,
      keywords AS keywords,
      u_raw AS u_raw;
}

-- normalized categories and keywords
user_profile_info_norm = FOREACH user_profile_info GENERATE
      uid AS uid,
      age AS age,
      gender AS gender,
      myfunc.top(categories,$U_NCAT) AS categories,
      myfunc.top(keywords, $U_NKEY) AS keywords,
      u_raw as u_raw;

--rmf tmp/user_profile_info_norm;
--store user_profile_info_norm into 'tmp/user_profile_info_norm';

log_content_info_join = JOIN raw_event_out_withid BY (uid) LEFT,
                             user_profile_info_norm BY (uid);

log_content_info_for = FOREACH  log_content_info_join GENERATE
      raw_event_out_withid::content_id AS content_id,
      raw_event_out_withid::productid AS productid,
      raw_event_out_withid::rid AS rid,
      raw_event_out_withid::ts AS ts,
      raw_event_out_withid::join_ts AS join_ts,
      raw_event_out_withid::uid AS uid,
      raw_event_out_withid::dwelltime as dwelltime,
      raw_event_out_withid::listpagedwell as listpagedwell,
      raw_event_out_withid::label AS label,
      raw_event_out_withid::city AS city,
      raw_event_out_withid::cnty_lan AS cnty_lan,
      user_profile_info_norm::u_raw AS u_raw,
      user_profile_info_norm::age AS age,
      user_profile_info_norm::gender AS gender,
      user_profile_info_norm::categories AS u_categories,
      user_profile_info_norm::keywords AS u_keywords;

log_content_user_info_join = JOIN log_content_info_for BY (content_id),
                                  content_info_norm BY (content_id);

log_content_user_info = FOREACH log_content_user_info_join GENERATE
        log_content_info_for::content_id AS content_id,
        content_info_norm::title_md5 AS title_md5,
        content_info_norm::groupid AS groupid,
        log_content_info_for::productid AS productid,
        log_content_info_for::rid AS rid,
        log_content_info_for::ts AS ts,
        log_content_info_for::join_ts AS join_ts,
        log_content_info_for::uid AS uid,
        log_content_info_for::dwelltime as dwelltime,
        log_content_info_for::listpagedwell as listpagedwell,
        log_content_info_for::label AS label,
        log_content_info_for::city AS city,
        log_content_info_for::cnty_lan AS cnty_lan,
        log_content_info_for::age AS u_age,
        log_content_info_for::gender AS u_gender,
        u_categories AS u_categories,
        u_keywords AS u_keywords,
        content_info_norm::update_time AS cp_update_time,
        content_info_norm::d_raw AS d_raw,
        log_content_info_for::u_raw AS u_raw,
        content_info_norm::type as cp_type,
        content_info_norm::publish_time as publish_time,
        content_info_norm::publisher AS publisher,
        content_info_norm::categories AS d_categories,
        content_info_norm::keywords AS d_keywords;

log_content_user_info = FOREACH log_content_user_info {
    id_tuple = TOTUPLE(uid,content_id,ts,productid,label,rid);
    id = myfunc.get_md5_id(id_tuple);
    GENERATE id AS id, content_id .. d_keywords;
}

log_content_user_info_feature = FOREACH log_content_user_info {
    fields_map = TOMAP('d_title_md5',title_md5,'d_groupid',groupid,'d_content_id',content_id,'d_publisher',publisher, 'd_category',d_categories,'d_keyword',d_keywords,'u_uid',uid,'u_city',city, 'u_category',u_categories,'u_keyword',u_keywords,'u_age',u_age, 'u_gender', u_gender, 'score',0);
    GENERATE
        id AS id,
        ts AS ts,
        join_ts as join_ts,
        myfunc.gen_feature(fields_map,'all',cnty_lan,'$cnty_lan_bool') AS  feature_bag:{t:(feature_type:chararray, feature_name:chararray, feature_weight:double,feature_score:double)};
}

event_user_content_profile_out = FOREACH log_content_user_info_feature GENERATE
    id AS id,
    (chararray)ts AS ts:chararray,
    (chararray)join_ts as join_ts:chararray,
    FLATTEN(feature_bag) AS (feature_type:chararray, feature_name:chararray, feature_weight:double, feature_score:double);

-- rmf $OUTPUT/event_user_content_profile_out
-- store event_user_content_profile_out into '$OUTPUT/event_user_content_profile_out';
-- -- rmf $OUTPUT/event_user_content_profile_out
-- -- store event_user_content_profile_out into'$OUTPUT/event_user_content_profile_out';
-- add cfb feature and join with all events

-- nrt_cfb = load '$DUMP_CFB' as (
--     features : chararray,
--     ts : long,
--     view_sum : double,
--     click_sum : double
-- );
-- raw_cfb = FOREACH nrt_cfb GENERATE (ts/600*600)-600 as ts, FLATTEN(STRSPLIT(features,'\u0001',2)) as (feature_type:chararray, feature_name:chararray), click_sum, view_sum;

-- load offline cfb
raw_cfb = LOAD '$INPUT_CFB' AS (
    ts : long,
    feature_type : chararray,
    feature_name : chararray,
    click_sum : double,
    view_sum : double
);

raw_cfb = FOREACH raw_cfb GENERATE ts,feature_type, REPLACE(feature_name, '\u0001', '\u0002') as feature_name:chararray, click_sum, view_sum;

raw_cfb_group = GROUP raw_cfb BY (ts, feature_type, feature_name);

raw_cfb = FOREACH raw_cfb_group GENERATE
    FLATTEN(group) AS (ts, feature_type, feature_name),
    SUM($1.click_sum) AS click_sum,
    SUM($1.view_sum) AS view_sum;

raw_cfb_filter = FILTER raw_cfb BY ts > 0; -- (myfunc.exclude_feature_type(feature_type) == '0');

cfb_group = GROUP raw_cfb_filter BY (feature_type, feature_name);

cfb_out = FOREACH cfb_group { 
    records = FOREACH $1 GENERATE
            ts AS ts,
            click_sum AS click_sum,
            view_sum AS view_sum,
            ts AS orig_ts;

    records_order = ORDER records BY ts ASC;
    GENERATE FLATTEN($0) as (feature_type,feature_name), records_order;
}

-- rmf /user/mengchong/cfb_test_output
-- store cfb_out into '/user/mengchong/cfb_test_output';

cfb_out_parsed = foreach cfb_out GENERATE feature_type, feature_name, cfb.gen_cfb_new(records_order,myfunc.date2timestamp('$START_DATE_TIME',$WND,'%Y%m%d%H')-$WND, myfunc.date2timestamp('$END_DATE_TIME', $WND,'%Y%m%d%H'), $DECAY_FACTOR, '$N_DECAYWND') as cfb_out_bag;
cfb_out_parsed_flat = FOREACH cfb_out_parsed GENERATE feature_type, feature_name, FLATTEN(cfb_out_bag) AS (ts:long,total_click_sum:double,total_view_sum:double,prev_ts:long);

-- store cfb_out_parsed_flat into '/user/mengchong/cfb_test_output_parsed';

-- join cfb end
cfb_out_parsed_flat = FILTER cfb_out_parsed_flat BY (total_view_sum > 0.0 or total_click_sum > 0.0);

cfb_out = STREAM  cfb_out_parsed_flat THROUGH `cat` AS ( 
    feature_type:chararray,
    feature_name:chararray,
    ts:chararray,
    total_click_sum:double,
    total_view_sum:double,
    prev_ts:long
);

-- cfb_out = load '$OUTPUT/cfb_agg' as (feature_type:chararray, feature_name:chararray, ts:chararray, total_click_sum:double, total_view_sum:double, prev_ts:long);
-- rmf $OUTPUT/cfb_agg
-- store cfb_out into '$OUTPUT/cfb_agg';

event_user_content_profile_cfb_join = JOIN event_user_content_profile_out BY (join_ts, feature_type, feature_name) LEFT,
                                          cfb_out BY (ts, feature_type, feature_name);

event_user_content_profile_cfb = FOREACH  event_user_content_profile_cfb_join GENERATE
            id AS id,
            event_user_content_profile_out::feature_type AS feature_type,
            event_user_content_profile_out::feature_name AS feature_name,
            feature_weight AS feature_weight,
            cfb_out::total_click_sum AS click_sum,
            cfb_out::total_view_sum AS view_sum;

-- rmf $OUTPUT/event_user_content_profile_cfb
-- store event_user_content_profile_cfb into '$OUTPUT/event_user_content_profile_cfb';

event_user_content_profile_cfb_group = GROUP event_user_content_profile_cfb BY (id);

event_cfb_json = FOREACH event_user_content_profile_cfb_group {
    cfb_bag = FOREACH $1 GENERATE
        feature_type AS feature_type,
        feature_name AS feature_name,
        feature_weight AS feature_weight,
        click_sum AS click_sum,
        view_sum AS view_sum;
    cfb_bag = FILTER cfb_bag by feature_type is not null and feature_name is not null;

    cfb_json = myfunc.gen_cfb_json(cfb_bag);
    GENERATE
    group  AS id,
    cfb_json AS cfb_json;
}

-- rmf $OUTPUT/event_cfb_json
-- store event_cfb_json into '$OUTPUT/event_cfb_json';

event_user_content_profile_cfb_json_join = JOIN log_content_user_info BY (id) LEFT,
                                                event_cfb_json BY (id);

event_user_content_profile_cfb_json = FOREACH event_user_content_profile_cfb_json_join GENERATE 
    log_content_user_info::id as id,
    uid AS uid,
    content_id AS content_id,
    ts AS ts,
    label AS label,
    city AS city,
    -- u_categories AS u_categories,
    -- u_keywords AS u_keywords,
    log_content_user_info::publisher AS publisher,
    -- log_content_user_info::d_categories AS d_categories,
    -- log_content_user_info::d_keywords AS d_keywords,
    log_content_user_info::cp_update_time AS cp_update_time,
    log_content_user_info::d_raw AS d_raw,
    log_content_user_info::u_raw as u_raw,
    -- log_content_user_info::cp_newsy_score AS cp_newsy_score,
    -- log_content_user_info::word_count AS word_count,
    -- log_content_user_info::image_count AS image_count,
    log_content_user_info::cp_type as cp_type,
    log_content_user_info::publish_time as publish_time,
    log_content_user_info::u_age as u_age,
    log_content_user_info::u_gender as u_gender,
    cfb_json AS cfb_json,
    log_content_user_info::dwelltime as dwelltime,
    log_content_user_info::listpagedwell as listpagedwell;

rmf $OUTPUT/events_with_cfb_features
STORE event_user_content_profile_cfb_json INTO '$OUTPUT/events_with_cfb_features';

