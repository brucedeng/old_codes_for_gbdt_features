相较于phase2，有以下改变:

 1.可以同时选择一个或多个国家(过滤逻辑有变化：除印度外国家，如RU, 当app_lan非ru时不会被过滤掉，之前版本会过滤掉)

 2.可以选择给feature加后缀country_lan或不加（指定configs中的cnty_lan_bool）

 3.可以选择pid （指定pid）
 
 4.可以选择 修正正负样本比例的阈值 （指定balance_ratio）

 5.使用upv4

 6.使用cpv2（不需要提前手动转换）

 7.ipcity映射取上游结果

 8.去掉declare

 9.rcv event更换为hdfs

 10.清理udf中debug print log

 11.使用gzip存储

 12.生成merge_cfb过程中rid若为空将会被过滤掉
 
 13.指定tmp文件路径 (/tmp/$PIG_USER), pig job name 增加 $PIG_USER (在configure_template中指定)

目前 instanews_hindi 使用upv3需要手动改up路径