#!/bin/bash

UP_BASE_PATH=$1
prev_date_time=$2


cur_ts=`date -d "$prev_date_time" +%s`
up_ts=$((cur_ts - 4*3600))
up_hour_ts=$((up_ts / 3600 / 2 * 2 * 3600))
up_dt=`date -d@$up_hour_ts +%Y%m%d/%H`

echo "INPUT_UP=${UP_BASE_PATH}/$up_dt"

