set -x

export LD_LIBRARY_PATH=$LDLIBRARY_PATH:/usr/lib/hadoop/lib/native

spark-submit \
--class com.cmcm.cmnews.model.app.BinaryTrainDataApp \
--master yarn-cluster \
--driver-memory 2g \
--executor-memory 3g \
--executor-cores 4 \
--queue experiment \
--num-executors 20 \
--conf spark.app.name=gen_binaryfeatures_1h_chenkehan $@

