REGISTER 'pig*.jar';
REGISTER 'utils.py' USING jython AS myfunc;

-- set mapred.create.symlink yes;
-- set mapred.cache.files $CONFIG#config.json,$CITY_DATA#ip_city.data

-- set default_parallel 400;
set job.name '$job_name'


RAW_LOG = LOAD '${event_imp},${event_clk},${event_rtm}' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

RAW_CLICK_NE_LOG = LOAD '$event_clk_ne' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

--view rtm clk data
RAW_DATA_GEN = FOREACH RAW_LOG GENERATE
    json,
    (json#'app_lan' is null ? 'en' : ( json#'app_lan' MATCHES 'hi.*' ? 'hi' : json#'app_lan' )) as app_lan:chararray,
    (json#'country' is null? '' : (chararray)(json#'country')) as country:chararray,
    json#'contentid' as content_id:chararray,
    json#'aid' as uid:chararray,
    json#'ip' as ip:chararray,
    json#'servertime_sec' as eventtime,
    json#'pid' as pid:chararray,
    json#'ctype' as ctype:chararray,
    json#'act' as act:chararray,
    json#'city' as city:chararray,
    myfunc.parse_rid(json#'cpack') as rid:chararray,
    (int)json#'ext'#'dwelltime' AS dwelltime:int;

RAW_DATA_FM = FOREACH RAW_DATA_GEN GENERATE json,app_lan,country, CONCAT(app_lan, CONCAT('_', country)) as lan_region:chararray, content_id, uid, ip, (long)(eventtime) as ts, pid, ctype, act, city, rid, dwelltime;

RAW_DATA_FILTER = FILTER RAW_DATA_FM BY act is not null and pid=='$pid' and ( ctype is null or ctype=='' or ctype=='1' or ctype=='0x01' ) and ( act == '1' or act == '2' or act == '4') and ( json#'scenario'#'level1_type' =='1' or json#'scenario'#'level1_type' =='10' ) and json#'scenario'#'level1' == '1' and INDEXOF('$lan_region',LOWER(lan_region),0) >=0;

RAW_DATA = FOREACH (GROUP RAW_DATA_FILTER BY (uid, content_id, pid, rid, act)) {
        ts = MIN(RAW_DATA_FILTER.ts);
        join_ts = ts/$WND * $WND - $WND;
        city = MIN(RAW_DATA_FILTER.city);
        lan_region = MAX(RAW_DATA_FILTER.lan_region);
        dwell = MAX(RAW_DATA_FILTER.dwelltime);
        rtm = ($0.act == '4' ? (dwell > 600 ? 600 : dwell) : 0);
        b = ($0.act == '2' ? 1 : 0);
        GENERATE FLATTEN(group) AS (uid, content_id, pid, rid, act), ts AS ts, join_ts AS join_ts, city AS city, rtm AS dwelltime, b AS label, lan_region AS lan_region;
};

--click negative data
RAW_CLICK_NE_DATA_GEN = FOREACH RAW_CLICK_NE_LOG GENERATE
    json,
    (json#'app_lan' is null ? 'en' : ( json#'app_lan' MATCHES 'hi.*' ? 'hi' : json#'app_lan' )) as app_lan:chararray,
    (json#'country' is null? '' : (chararray)(json#'country')) as country:chararray,
    json#'contentid' as content_id:chararray,
    json#'aid' as uid:chararray,
    json#'ip' as ip:chararray,
    json#'servertime_sec' as eventtime,
    json#'pid' as pid:chararray,
    json#'ctype' as ctype:chararray,
    json#'act' as act:chararray,
    json#'city' as city:chararray,
    myfunc.parse_rid(json#'cpack') as rid:chararray,
    (int)json#'ext'#'dwelltime' AS dwelltime:int;

RAW_CLICK_NE_DATA_FM = FOREACH RAW_CLICK_NE_DATA_GEN GENERATE json,app_lan,country, CONCAT(app_lan, CONCAT('_', country)) as lan_region:chararray, content_id, uid, ip, (long)(eventtime) as ts, pid, ctype, act, city, rid, dwelltime;

RAW_CLICK_NE_DATA_FILTER = FILTER RAW_CLICK_NE_DATA_FM BY act is not null and pid=='$pid' and ( ctype is null or ctype=='' or ctype=='1' or ctype=='0x01' ) and act == '2' and ( json#'scenario'#'level1_type' =='1' or json#'scenario'#'level1_type' =='10' ) and json#'scenario'#'level1' == '1' and INDEXOF('$lan_region',LOWER(lan_region),0) >=0;

RAW_CLICK_NE_DATA = FOREACH (GROUP RAW_CLICK_NE_DATA_FILTER BY (uid, content_id, pid, rid)) {
        ts = MIN(RAW_CLICK_NE_DATA_FILTER.ts);
        join_ts = ts/$WND * $WND - $WND;
        city = MIN(RAW_CLICK_NE_DATA_FILTER.city);
        lan_region = MAX(RAW_CLICK_NE_DATA_FILTER.lan_region);
        GENERATE FLATTEN(group) AS (uid, content_id, pid, rid), ts AS ts, join_ts AS join_ts, city AS city, 0 AS dwelltime, 0 AS label, lan_region AS lan_region;
};

--splite

SPLIT RAW_DATA INTO
    RAW_VIEW_DATA IF act == '1',
    RAW_CLICK_DATA IF act == '2',
    RAW_RTM_DATA IF act == '4';


--click postive data


--rtm data


--negative label

NEGATIVE_DATA = JOIN RAW_VIEW_DATA BY ($0, $1, $2, $3) LEFT,  RAW_CLICK_NE_DATA BY ($0, $1, $2, $3);

NEGATIVE_DATA_FILTER = FILTER NEGATIVE_DATA BY RAW_CLICK_NE_DATA::uid IS NULL;

NEGATIVE_DATA_LABEL = FOREACH NEGATIVE_DATA_FILTER GENERATE
        RAW_VIEW_DATA::uid AS uid,
        RAW_VIEW_DATA::content_id AS content_id,
        RAW_VIEW_DATA::pid AS pid,
        RAW_VIEW_DATA::rid AS rid,
        RAW_VIEW_DATA::ts AS ts,
        RAW_VIEW_DATA::join_ts AS join_ts,
        RAW_VIEW_DATA::city AS city,
        RAW_VIEW_DATA::dwelltime AS dwelltime,
        RAW_VIEW_DATA::label AS label,
        RAW_VIEW_DATA::lan_region AS lan_region;

--positive label

POSITIVE_DATA = JOIN RAW_CLICK_DATA BY ($0, $1, $2, $3) LEFT,  RAW_RTM_DATA BY ($0, $1, $2, $3);

POSITIVE_DATA_LABEL = FOREACH POSITIVE_DATA GENERATE
        RAW_CLICK_DATA::uid AS uid,
        RAW_CLICK_DATA::content_id AS content_id,
        RAW_CLICK_DATA::pid as pid,
        RAW_CLICK_DATA::rid AS rid,
        RAW_CLICK_DATA::ts AS ts,
        RAW_CLICK_DATA::join_ts AS join_ts,
        RAW_CLICK_DATA::city AS city,
        (RAW_RTM_DATA::dwelltime IS NULL ? 70 : RAW_RTM_DATA::dwelltime) AS dwelltime,
        RAW_CLICK_DATA::label AS label,
        RAW_CLICK_DATA::lan_region AS lan_region;

--sample label

LABEL_DATA = UNION NEGATIVE_DATA_LABEL, POSITIVE_DATA_LABEL;

LABEL_DATA_OUT = FOREACH LABEL_DATA {
        id_tuple = TOTUPLE(uid,content_id,(chararray)ts,pid,label,rid);
        id = myfunc.get_md5_id(id_tuple);
        GENERATE id as id, uid,lan_region as lan_region, content_id, ts, join_ts,'' as field6, pid, city, dwelltime, label, rid;
};

rmf $output
store LABEL_DATA_OUT into '$output';
