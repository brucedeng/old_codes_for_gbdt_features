set -x

export LD_LIBRARY_PATH=$LDLIBRARY_PATH:/usr/lib/hadoop/lib/native

spark-submit \
--class com.cmcm.cmnews.model.app.UserExploreApp \
--master yarn-cluster \
--driver-memory 1g \
--executor-memory 2g \
--executor-cores 3 \
--queue fasttrack \
--num-executors 25 \
--conf spark.default.parallelism=300 \
--conf spark.app.name=filter_up_explore_data_2hour_cm_us_lilonghua $@


