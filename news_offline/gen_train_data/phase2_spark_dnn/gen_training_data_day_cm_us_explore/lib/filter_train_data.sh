set -x

export LD_LIBRARY_PATH=$LDLIBRARY_PATH:/usr/lib/hadoop/lib/native

spark-submit \
--class com.cmcm.cmnews.model.app.FTRLDataFilterApp \
--master yarn-cluster \
--conf spark.core.connection.ack.wait.timeout=300 \
--conf spark.akka.timeout=300 \
--conf spark.network.timeout=300 \
--driver-memory 2g \
--executor-memory 4g \
--executor-cores 2 \
--queue experiment \
--num-executors 30 \
--conf spark.app.name=filter_day_us_cm_lilonghua_explore $@