set -x

export LD_LIBRARY_PATH=$LDLIBRARY_PATH:/usr/lib/hadoop/lib/native

spark-submit \
--class com.cmcm.cmnews.model.app.BinaryTrainDataNewApp \
--master yarn-cluster \
--conf spark.yarn.executor.memoryOverhead=1024 \
--conf spark.core.connection.ack.wait.timeout=300 \
--conf spark.rdd.compress=true \
--conf spark.akka.timeout=300 \
--conf spark.network.timeout=300 \
--driver-memory 5g \
--executor-memory 7g \
--executor-cores 4 \
--queue experiment \
--num-executors 60 \
--conf spark.app.name=gen_binaryfeatures_day_us_cm_lilonghua_explore $@


