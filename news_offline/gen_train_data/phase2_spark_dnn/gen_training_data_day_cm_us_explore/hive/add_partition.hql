set mapred.job.queue.name=default;
set fs.s3.canned.acl=BucketOwnerFullControl;
 
use ${OUTPUT_DATABASE};
ALTER TABLE ${OUTPUT_TABLE} ADD IF NOT EXISTS PARTITION (dt='${DAY}',src='install') LOCATION '${DAY}/install.bz2';
ALTER TABLE ${OUTPUT_TABLE} ADD IF NOT EXISTS PARTITION (dt='${DAY}',src='uninstall') LOCATION '${DAY}/uninstall.bz2';
ALTER TABLE ${OUTPUT_TABLE} ADD IF NOT EXISTS PARTITION (dt='${DAY}',src='run') LOCATION '${DAY}/run.bz2';
ALTER TABLE ${OUTPUT_TABLE} ADD IF NOT EXISTS PARTITION (dt='${DAY}',src='install_status') LOCATION '${DAY}/install_status.bz2';

