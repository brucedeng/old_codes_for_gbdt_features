set mapred.job.queue.name=default;
set fs.s3.canned.acl=BucketOwnerFullControl;
 
use ${OUTPUT_DATABASE};
ALTER TABLE ${OUTPUT_TABLE} DROP IF EXISTS PARTITION (dt='${DAY}',src='install');
ALTER TABLE ${OUTPUT_TABLE} DROP IF EXISTS PARTITION (dt='${DAY}',src='uninstall');
ALTER TABLE ${OUTPUT_TABLE} DROP IF EXISTS PARTITION (dt='${DAY}',src='run');

