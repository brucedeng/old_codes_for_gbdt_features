set -x

export LD_LIBRARY_PATH=$LDLIBRARY_PATH:/usr/lib/hadoop/lib/native

hadoop distcp -Dmapreduce.job.queuename=deeplearning $@
