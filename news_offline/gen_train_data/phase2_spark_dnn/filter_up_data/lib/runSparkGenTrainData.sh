set -x

export LD_LIBRARY_PATH=$LDLIBRARY_PATH:/usr/lib/hadoop/lib/native

spark-submit \
--class com.cmcm.cmnews.model.app.UPFilterApp \
--master yarn-cluster \
--driver-memory 1g \
--executor-memory 2g \
--executor-cores 2 \
--queue deeplearning \
--num-executors 20 \
--conf spark.app.name=filter_up_data_day_lilonghua $@


