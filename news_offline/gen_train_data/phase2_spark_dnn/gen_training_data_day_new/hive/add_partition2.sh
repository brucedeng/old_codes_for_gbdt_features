DAY=20160214

hive -e"
set mapred.job.queue.name=default;
set fs.s3.canned.acl=BucketOwnerFullControl;
 
use targeting_pipeline_v4;
ALTER TABLE up_features_flatten_daily ADD IF NOT EXISTS PARTITION (dt='${DAY}',src='install') LOCATION '${DAY}/install.bz2';
ALTER TABLE up_features_flatten_daily ADD IF NOT EXISTS PARTITION (dt='${DAY}',src='uninstall') LOCATION '${DAY}/uninstall.bz2';
ALTER TABLE up_features_flatten_daily ADD IF NOT EXISTS PARTITION (dt='${DAY}',src='run') LOCATION '${DAY}/run.bz2';
ALTER TABLE up_features_flatten_daily ADD IF NOT EXISTS PARTITION (dt='${DAY}',src='install_status') LOCATION '${DAY}/install_status.bz2';
ALTER TABLE up_features_flatten_daily ADD IF NOT EXISTS PARTITION (dt='${DAY}',src='show') LOCATION '${DAY}/show.bz2';
ALTER TABLE up_features_flatten_daily ADD IF NOT EXISTS PARTITION (dt='${DAY}',src='click') LOCATION '${DAY}/click.bz2';
ALTER TABLE up_features_flatten_daily ADD IF NOT EXISTS PARTITION (dt='${DAY}',src='ad_install') LOCATION '${DAY}/ad_install.bz2';

"
