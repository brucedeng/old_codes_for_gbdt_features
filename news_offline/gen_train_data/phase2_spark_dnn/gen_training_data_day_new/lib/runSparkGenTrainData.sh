set -x

export LD_LIBRARY_PATH=$LDLIBRARY_PATH:/usr/lib/hadoop/lib/native

spark-submit \
--class com.cmcm.cmnews.model.app.BinaryTrainDataApp \
--master yarn-cluster \
--driver-memory 5g \
--executor-memory 5g \
--executor-cores 4 \
--queue deeplearning_online \
--num-executors 50 \
--conf spark.app.name=hi_gen_binaryfeatures_day_lilonghua $@


