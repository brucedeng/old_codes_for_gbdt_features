# basic settings
event_start=20160724;
event_end=20160731;
train_sample_rate=1.0;
app_lan=hi
product_id=11
lan_region=hi_,hi_in

APP_NAME=duanquansheng

# run processes
run_merge_cp=true
run_format_train_event=true
run_train_data=true

# pig params
queue=deeplearning

config_file=cfb_features_dnn.conf
PIG_PARAM=" -Dmapreduce.job.queuename=$queue ";
TRAIN_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 3g --num-executors 60 --queue $queue "
TOP_PARAM=" -p WND=600 "

SPARK_TOP_PARAM=" parallelism 500 wnd 600 u_ncat 5 u_nkey 25 d_ncat 5 d_nkey 15 cat_version oversea_l2 kw_version oversea rel_u_ncat 200 rel_u_nkey 200 rel_d_ncat 200 rel_d_nkey 200 decay_factor 0.995 decay_wind 2d top_num 1000 random_num 1000 config_file $config_file l1_norm yes multi_lan no"

input_user_root="/projects/news/user_profile/data/V4/up_json/hi_in"
input_cp_daily_template="/projects/news/cpp/feeder/in_cp_dump/%s/*/*"
merged_cp_path="/projects/news/deeplearning/model/experiments/merged_cp_multiple_days/${event_start}_${event_end}"
kw_split=0
split_seed=1000

# input_event_template="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click}/$cur_date/*/*"

output_lan=`echo $lan_region | tr ',' '-'`
# output path
output_root="/projects/news/deeplearning/model/training/${app_lan}_train_data_spark"
format_train_event="$output_root/formatted_cfb_events/%s"
merged_cfb_root="$output_root/merged_cfb"

