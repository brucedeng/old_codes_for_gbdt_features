set -x

export LD_LIBRARY_PATH=$LDLIBRARY_PATH:/usr/lib/hadoop/lib/native

./spark-home/bin/spark-submit \
--class com.cmcm.cmnews.model.app.BinaryTrainDataNewApp \
--master yarn \
--deploy-mode cluster \
--conf spark.yarn.executor.memoryOverhead=1024 \
--conf spark.rdd.compress=true \
--conf spark.rpc.askTimeout=300 \
--conf spark.task.maxFailures=50 \
--driver-memory 5g \
--executor-memory 5g \
--executor-cores 4 \
--queue experiment \
--num-executors 30 \
--conf spark.app.name=gen_binaryfeatures_hourly_us_cm_app $@


