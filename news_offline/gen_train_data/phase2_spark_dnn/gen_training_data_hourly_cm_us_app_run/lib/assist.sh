#!/usr/bin/env bash
set -x

four_hour_gap[0]=2
four_hour_gap[1]=6
four_hour_gap[2]=10
four_hour_gap[3]=14
four_hour_gap[4]=18
four_hour_gap[5]=22

two_hour_gap[0]=0
two_hour_gap[1]=2
two_hour_gap[2]=4
two_hour_gap[3]=6
two_hour_gap[4]=8
two_hour_gap[5]=10
two_hour_gap[6]=12
two_hour_gap[7]=14
two_hour_gap[8]=16
two_hour_gap[9]=18
two_hour_gap[10]=20
two_hour_gap[11]=22

hourly_str=""
function twoHourGap()
{
  hour=$((10#$1%24))
  h=$hour
  for ((i=${#two_hour_gap[@]}-1;i>=0;i--))
  {
    if (($hour >= ${two_hour_gap[$i]}));then
      h=${two_hour_gap[$i]}
      hourly_str="$h"
      break
    fi 
  }
  h_str=$h
  if (($h < 10));then
    h_str='0'$h
  fi
  if [ -z "$hourly_str" ];then
    return 127
  fi
  return 0
}

function fourHourGap()
{
  hour=$((10#$1%24))
  h=$hour
  for ((i=${#four_hour_gap[@]}-1;i>=0;i--))
  {
    if (($hour >= ${four_hour_gap[$i]}));then
      h=${four_hour_gap[$i]}
      hourly_str="$h"
      break
    fi 
  }
  h_str=$h
  if (($h < 10));then
    h_str='0'$h
  fi
  if [ -z "$hourly_str" ];then
    return 127
  fi
  return 0
}

function filterCP()
{
  cp_path=$1
  current_time=$2
  year_month_day=${current_time:0:8}
  OLD_IFS="$IFS"
  IFS=","
  arr=($cp_path)
  IFS="$OLD_IFS"
  result=""
  for p in ${arr[@]}
  do
    if [[ $p =~ "$year_month_day" ]];then
      result=$result","$p
    fi
  done
  echo $result
}

base_date="2016-11-10 04:00:00"
base_time=`date -d "$base_date" +%s`
current_time=$1
root_path=$2
done_flag=$3
cp_path=$4
cp_path=`filterCP $cp_path $current_time`
year_str=${current_time:0:4}
month_str=${current_time:4:2}
day_str=${current_time:6:2}
hour_str=${current_time:8:2}
last_str=`date --date="$year_str-$month_str-$day_str $hour_str 4 hours ago" +"%Y%m%d%H"`
last_date=`date --date="$year_str-$month_str-$day_str $hour_str 4 hours ago" +"%Y-%m-%d %H:00:00"`
last_time=`date -d "$last_date" +%s`
ymd_str=${last_str:0:8}
h_str=${last_str:8:2}

if [ $last_time -gt $base_time ];then
  twoHourGap $h_str
  if [[ $? -ne 0 ]];then
    ymd_str=`date --date="$last_date 1 days ago" +"%Y%m%d"`
    index=${#two_hour_gap[@]}-1
    hourly_str=${two_hour_gap[$index]} 
  fi
else
  fourHourGap $h_str
  if [[ $? -ne 0 ]];then
    ymd_str=`date --date="$last_date 1 days ago" +"%Y%m%d"`
    index=${#four_hour_gap[@]}-1
    hourly_str=${four_hour_gap[$index]} 
  fi
fi
if (($hourly_str < 10));then
  hourly_str='0'$hourly_str
fi
path=$root_path'/'$ymd_str'/'$hourly_str'/'$done_flag
output_path=$root_path'/'$ymd_str'/'$hourly_str
echo "Try to find $path"
hadoop fs -ls $path >/dev/null 2>&1
while [[ $? -ne 0 ]];do
  echo "Can't find $path"
  sleep 10m
  hadoop fs -ls $path >/dev/null 2>&1
done
echo "Find path: $path"
echo "path=$output_path"
echo "cp_today=$cp_path"
