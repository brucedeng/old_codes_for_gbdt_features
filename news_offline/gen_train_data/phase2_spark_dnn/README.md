# spark training data pipeline

## 运行环境

pipeline里所有的spark程序都需要使用spark 1.6运行. hadoop launcher 上的路径是: 

10.2.2.238:/home/mengchong/news_model/home/spark/spark-1.6.0-runtime/bin/spark-submit

spark code:

```
spark/content_xfb_pipline
spark/content_xfb_train_data_pipeline
```

## 使用方法

程序的入口是 run_gen_training_data_spark.sh, 它流程化地生成训练数据. 

生成训练数据的步骤包括:

- run_merge_cp : 预处理, 合并训练数据用到的所有cp数据
- run_format_raw_cfb : 格式化raw cfb数据. batch 去重
- run_raw_cfb : 生成raw cfb 数据, spark
- run_format_train_event : 格式化训练数据, 全量数据去重
- run_train_data : 生成merge cfb, spark
- run_gen_libsvm : 生成libsvm格式的数据, spark
- run_split_data : random split 数据
- run_format_data : 格式化成训练数据, 添加weight. 使用random split的4份作训练, 1份作测试
- run_format_data_by_day : 按照 test_start 区分. test_start 以前的(不包含)作为训练数据, 以后的作为测试数据. 和run_format_data 是不同的split方式, 两者只需要选择一种.

脚本里的其它参数的说明如下

| 参数 | 说明 | 示例 | 
| ---- | ------- | ------ |
| event_start | 起始日期 | 20160615 |
| event_end | 结束日期   | 20160715 |
| app_lan | 语言  | hi |
| product_id  | pid  | 11 |
| lan_region  | language region whitelist | hi_in,de_de |
| cfb_ignore_date | 生成cfb的起始时间, 该参数用于数据跑一半失败, 重跑时跳过已生成的数据 | 20160709 |
| queue | 运行的队列 | experiment | 
| PIG_PARAM | pig 运行参数 |  |
| RAW_SPARK_PARAM | spark 生成raw cfb 的参数. 不需要使用太多内存, 资源较少 | | 
| TRAIN_SPARK_PARAM | spark 生成merge cfb 的参数, 一般情况下需要10g的 executor memory, node不要超过60 | | 
| FORMAT_SPARK_PARAM | spark 格式化libsvm的参数, 没有shuffer操作, 申请很少的内存, 节点可以适当增加 | | 
| TOP_PARAM | pig 的固定参数  | -p WND=600 | 
| SPARK_TOP_PARAM | spark的固定参数, 包括window key number等 | | 
| input_user_root | 输入up的路径 | | 
| input_cp_daily_template | 原始cp dump的路径模板. 脚本中使用printf 把日期添加到路径模板中. | | 
| merged_cp_path | cp merge并去重后的路径 | |
| kw_spkit | 区分新老用户的kw_len 阈值 | 0 |
| split_seed | random split 的随机种子 | 1000 |
| output_root | 总输入路径 | |
| format_rawcfb_event | raw cfb spark的输入 | |
| format_train_event | 给spark用的训练数据去重后的log | | 
| raw_cfb_template | 输入或输了的raw cfb 模板 | |
| merged_cfb_root | 输出的merge cfb 目录 | |
| train_libsvm_root | 输出的libsvm 目录 | | 
| test_start  | 区分train和test的日期. 只用于run_format_data_by_day | 20160709 |

## 配置文件示例

```
# basic settings
event_start=20160615;
event_end=20160715;
train_sample_rate=1.0;
app_lan=hi
product_id=11
lan_region=hi_in

cfb_ignore_date=20160601

APP_NAME=mengchong

# run processes
run_merge_cp=true
run_format_raw_cfb=true
run_raw_cfb=true
run_format_train_event=true
run_train_data=true
run_gen_libsvm=true
run_split_data=true
run_format_data=true

# pig params
queue=experiment

config_file=cfb_features.conf
PIG_PARAM=" -Dmapreduce.job.queuename=$queue ";

RAW_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 6g --num-executors 30 --queue $queue "
TRAIN_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 10g --num-executors 60 --queue $queue "
FORMAT_SPARK_PARAM=" --master yarn-client --executor-cores 4 --executor-memory 2g --num-executors 60 --queue $queue "
TOP_PARAM=" -p WND=600 "

SPARK_TOP_PARAM=" parallelism 800 wnd 600 u_ncat 5 u_nkey 25 d_ncat 5 d_nkey 15 cat_version oversea_l2 kw_version oversea rel_u_ncat 200 rel_u_nkey 200 rel_d_ncat 200 rel_d_nkey 200 decay_factor 0.995 decay_wind 2d top_num 1000 random_num 1000 config_file $config_file l1_norm yes multi_lan no "

input_user_root="/projects/news/user_profile/data/V4/up_json/hi_in"

input_cp_daily_template="/projects/news/cpp/feeder/in_cp_dump/%s/*/*"
merged_cp_path="/projects/news/model/experiments/merged_cp_multiple_days/${event_start}-${event_end}"
kw_split=0
split_seed=1000

# output path
output_root="/projects/news/model/training/${app_lan}_train_data_20160615-20160715"

format_rawcfb_event="$output_root/formatted_rawcfb_events/%s"
format_train_event="$output_root/formatted_cfb_events/%s"
raw_cfb_template="$output_root/raw_cfb/%s"
merged_cfb_root="$output_root/merged_cfb"
train_libsvm_root="$output_root/libsvm"

```
