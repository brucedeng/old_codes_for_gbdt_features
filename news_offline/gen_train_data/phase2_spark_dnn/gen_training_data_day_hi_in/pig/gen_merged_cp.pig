REGISTER 'pig*.jar';
REGISTER 'hindi_up.py' USING jython AS myfunc;

-- set default_parallel 100;
set job.name '$job_name'
-- pre process content
-- raw_content = LOAD '$cp_dump' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_content = LOAD '$cp_dump' as (cp_str:chararray);

cp_data = FOREACH raw_content GENERATE FLATTEN(myfunc.gen_cp_id_uptime(cp_str)) as (content_id, update_time), cp_str;

raw_content_distinct = FOREACH ( GROUP cp_data by content_id ) {
    r = ORDER cp_data BY update_time DESC;
    l = LIMIT r 1;
    GENERATE FLATTEN(l) as (content_id, update_time, cp_str);
}

raw_cp_out = FOREACH raw_content_distinct GENERATE cp_str;

store raw_cp_out into '$output';
