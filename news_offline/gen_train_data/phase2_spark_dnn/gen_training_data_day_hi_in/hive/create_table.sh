#!/bin/sh
set -x
export HADOOP_HOME=/home/hadoop/
source ~/.bash_profile

#create table
hive -e "
use targeting_pipeline_v4;
CREATE EXTERNAL TABLE IF NOT EXISTS up_features_flatten_daily(
ftype                 	string,
fname                	string,
fvalue             	double,
aid               	string
  )
PARTITIONED BY ( 
dt                      string,
src                     string
  )
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://com.cmcm.bigdata.dh/targeting_pipeline_v4/up_features_flatten_daily';
"

hive -e "
use targeting_pipeline_v4;
CREATE EXTERNAL TABLE IF NOT EXISTS up_app_actionlist_daily(
aid                 	string,
pkgname                	string,
num             	double,
type               	string
  )
PARTITIONED BY ( 
dt                      string
  )
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://com.cmcm.bigdata.dh/targeting_pipeline_v4/up_app_actionlist_daily';
"


