#!/usr/bin/env python
import sys

for line in sys.stdin:
    data = line.rstrip().split('\t')[2]
    f = data.split(' ')

    label = f[0].split(':')[0]

    s = ' '.join([label] +  f[1:])
    print s
