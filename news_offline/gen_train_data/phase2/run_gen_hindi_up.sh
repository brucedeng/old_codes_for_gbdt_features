
while getopts "s:e:l:" arg
do
    case $arg in
        s) #start date of events
        event_start=$OPTARG
        ;;
        e) #end date  of event
        event_end=$OPTARG
        ;;
        l) #run data
        app_lan=$OPTARG
        ;;
        ?)  #?
        echo "unkonw argument"
        exit 1
        ;;
    esac
done
                                                                                                                                                                                    
#PIG_PARAM="-conf ./default-hadoop.xml -useHCatalog ";
#PIG_PARAM=" -Dmapred.job.queue.name=offline";
PIG_PARAM=" -Dmapred.job.queue.name=default ";

echo $event_start
echo $event_end
cur_date=$event_start

rm -f pig*.log
set -e

# if [ "$run_raw_cfb" = "true" ]; then
    # config_file="hdfs://mycluster/projects/news/model/configs/config_nrt.json"
    # city_file="hdfs://mycluster/projects/news/model/configs/ip_city.data"
    # hadoop fs -copyFromLocal -f config.json $config_file
    # hadoop fs -copyFromLocal -f ip_city.data $city_file

    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do
        input_user="s3://com.cmcm.instanews.usw2.prod/user_profile/V1/up_json/$cur_date"
        output_up="/projects/news/model/hindi_up/$cur_date"

        cmd="pig $PIG_PARAM -p up_dump=$input_user -p output=$output_up gen_hindi_up_data.pig"
        echo $cmd
        eval $cmd;

        if [ $? != 0 ]; then echo "ERROR: Failed to run pipeline!"; exit -1; fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done
# fi
