REGISTER '../lib/*.jar';
REGISTER '../lib/hindi_up.py' USING jython AS myfunc;

raw_user_profile = LOAD '$up_dump' as (up_str:chararray);

hindi_user_profile = FOREACH raw_user_profile GENERATE myfunc.format_hindi_up(up_str) as up_str;

rmf $output
store hindi_user_profile into '$output';
