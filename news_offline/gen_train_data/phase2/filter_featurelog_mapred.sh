
INPUT=$1
OUTPUT=$2

term=$3

hadoop fs -rmr $2

hadoop jar \
    /usr/lib/hadoop-mapreduce/hadoop-streaming-2.6.0-cdh5.4.8.jar \
    -Dmapred.reduce.tasks=100 \
    -Dmapred.job.queue.name=experiment \
    -input "$INPUT" \
    -output "$OUTPUT" \
    -mapper "grep $3" \
    -reducer "cat"  



