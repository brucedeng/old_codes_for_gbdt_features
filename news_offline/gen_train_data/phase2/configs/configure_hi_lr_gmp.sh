
# basic settings
event_start=20160402;
event_end=20160412;
train_sample_rate=1.0;
app_lan=hi
batch_dedup=false
cfb_ignore_date=20160320
#split_seed=2345

# run processes
run_raw_cfb=false
run_merge_cfb=false
run_training_data=true
run_split_data=true
run_featmap=false
run_merge_events=true

# pig params
queue=offline
PIG_PARAM=" -Dmapred.job.queue.name=$queue";
TOP_PARAM="-p U_NCAT=5 -p U_NKEY=25 -p D_NCAT=5 -p D_NKEY=15 -p WND=600"

# cached files
local_config=config_less.json
local_city=lib/ip_city.data
local_category=lib/cat_tree.txt
config_file="hdfs://mycluster/projects/news/model/configs/config_less.json"
city_file="hdfs://mycluster/projects/news/model/configs/ip_city.data"
category_data="hdfs://mycluster/projects/news/model/configs/cat_tree.txt"

# data pathes
if [[ "$app_lan" == "hi" ]]; then
    input_user_root="/projects/news/model/hindi_up"
    input_gmp_template="s3://com.cmcm.instanews.usw2.prod/nrt/gmp_hindi/%s/*"
else
    input_user_root="s3://com.cmcm.instanews.usw2.prod/user_profile/V1/up_json"
    input_gmp_template="s3://com.cmcm.instanews.usw2.prod/nrt/gmp/%s/*"
fi

input_cp_template="/projects/news/cpp/feeder/in_cp_dump/%s/*/*/*.data"
# input_cp_template="/projects/news/model/experiments/merged_cp_multiple_days/20160321-previous"
input_interest_root="s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall"

# output path
output_root="hdfs://mycluster/projects/news/model/training"
# raw_cfb_template="$output_root/raw_cfb/${app_lan}_offline_cfb_raw_new/%s"
raw_cfb_template="$output_root/raw_cfb/${app_lan}_offline_cfb_raw_old/%s"
merged_cfb_root="$output_root/merged_cfb/${app_lan}_feature_aggcfb_new"
# output_s3_path="s3://com.cmcm.instanews.usw2.prod/model/experiments/${app_lan}_offlinecfb_md5_training_data_new";
output_s3_path="$output_root/run_training_data/${app_lan}_cfb_offline_formatted_data"

raw_input_user_root=hdfs://mycluster/projects/news/model/up_data/V3/${app_lan}_up_json
joined_events_root="$output_root/merged_events/${app_lan}_raw_events_features_gmp"

featmap_path=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}
featmap_file_hdfs=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}.txt

