###configs begin
join_rcv_featurelog="true"
run_split_data="true"
run_format_data="true"
#####join_rcv_featurelog
event_start=20160622
event_end=20160628
queue_name=experiment
expid=cm_us_006
####generate splited data
kw_split=200;
train_sample_rate=1.0;
app_lan=en
PIG_PARAM=" -Dmapred.job.queue.name=$queue_name";
TOP_PARAM="-p U_NCAT=5 -p U_NKEY=25 -p D_NCAT=5 -p D_NKEY=15 -p WND=600"
cfb_pid1=1
cfb_pid2=n
#local_config=config_less.json
#local_category=../lib/cat_tree.txt
#config_file="hdfs://mycluster/projects/news/model/configs/config_less.json"
#category_data="hdfs://mycluster/projects/news/model/configs/cat_tree.txt"

# data pathes
output_root="/projects/news/model/experiments/training_data_from_featurelog/"
output_path="/projects/news/model/experiments/training_data_from_featurelog/";
output_s3_path="/projects/news/model/experiments/training_data_from_featurelog/";
featmap_path=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}
featmap_file_hdfs=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}.txt

