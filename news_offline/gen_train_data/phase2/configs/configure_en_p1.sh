
# basic settings
event_start=20160314;
event_end=20160320;
train_sample_rate=1.0;
app_lan=en

# run processes
run_raw_cfb=false
run_merge_cfb=false
run_training_data=true
run_featmap=false

# pig params
queue=experiment
PIG_PARAM=" -Dmapred.job.queue.name=$queue";

# cached files
category_data="hdfs://mycluster/projects/news/model/configs/en_cat_tree.txt"

# data pathes
input_interest_root="s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall"

# output path
output_root="/projects/news/model/training"
merged_cfb_root="$output_root/merged_cfb/${app_lan}_raw_features_joined_with_dedup_cfb"
output_path="/projects/news/model/experiments/${app_lan}_phase1_training_data";
featmap_path=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}
featmap_file_hdfs=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}.txt

