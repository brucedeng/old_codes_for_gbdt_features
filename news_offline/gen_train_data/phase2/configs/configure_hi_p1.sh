
# basic settings
event_start=20160315;
event_end=20160321;
app_lan=hi

# run processes
run_training_data=true

# pig params
queue=experiment
PIG_PARAM=" -Dmapred.job.queue.name=$queue";

# cached files
category_data="hdfs://mycluster/projects/news/model/configs/cat_tree.txt"

# data pathes
input_interest_root="s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall"

# output path
output_root="/projects/news/model/training"
output_path="/projects/news/model/experiments/${app_lan}_phase1_training_data";
merged_cfb_root="$output_root/merged_cfb/${app_lan}_feature_aggcfb_new"
featmap_file_hdfs=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}.txt


