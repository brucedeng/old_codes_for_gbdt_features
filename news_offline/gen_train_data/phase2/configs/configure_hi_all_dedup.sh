
# basic settings
event_start=20160412;
event_end=20160419;
train_sample_rate=1.0;
app_lan=hi
batch_dedup=true
cfb_ignore_date=20160412
split_seed=2345

# run processes
run_raw_cfb=true
run_merge_cfb=true
run_training_data=true
run_split_data=true
run_format_data=true
run_featmap=false

# pig params
queue=experiment
PIG_PARAM=" -Dmapred.job.queue.name=$queue";
TOP_PARAM="-p U_NCAT=5 -p U_NKEY=25 -p D_NCAT=5 -p D_NKEY=15 -p WND=600"

# cached files
local_config=config.json
local_city=lib/ip_city.data
local_category=lib/cat_tree.txt
config_file="hdfs://mycluster/projects/news/model/configs/config.json"
city_file="hdfs://mycluster/projects/news/model/configs/ip_city.data"
category_data="hdfs://mycluster/projects/news/model/configs/cat_tree.txt"
kw_split=200

# data pathes
if [[ "$app_lan" == "hi" ]]; then
    input_user_root="/projects/news/model/hindi_up"
else
    input_user_root="s3://com.cmcm.instanews.usw2.prod/user_profile/V1/up_json"
fi
input_cp_template="/projects/news/cpp/feeder/in_cp_dump/%s/*/*/*.data"
# input_cp_template="/projects/news/model/experiments/merged_cp_multiple_days/20160321-previous"
input_interest_root="s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall"

# output path
output_root="hdfs://mycluster/projects/news/model/training"
# raw_cfb_template="$output_root/raw_cfb/${app_lan}_offline_cfb_raw_new/%s"
raw_cfb_template="$output_root/raw_cfb/${app_lan}_offline_cfb_raw_dedup/%s"
merged_cfb_root="$output_root/merged_cfb/${app_lan}_feature_aggcfb_dedup"
# output_s3_path="s3://com.cmcm.instanews.usw2.prod/model/experiments/${app_lan}_offlinecfb_md5_training_data_new";
output_s3_path="$output_root/run_training_data/${app_lan}_cfb_offline_formatted_dedup"
featmap_path=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}
featmap_file_hdfs=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}.txt

