# basic settings
event_start=20160619;
event_end=20160619;
train_sample_rate=1.0;
app_lan=hi
batch_dedup=true
cfb_ignore_date=20160320

# run processes
# run_raw_cfb=true
run_merge_cfb=true
# run_training_data=true
# run_split_data=true
# run_featmap=true
# run_format_data=true

# pig params
queue=experiment
#queue=offline
PIG_PARAM=" -Dmapred.job.queue.name=$queue";
TOP_PARAM="-p U_NCAT=5 -p U_NKEY=25 -p D_NCAT=5 -p D_NKEY=15 -p WND=600"

# cached files
local_config=config_less.json
local_city=../lib/ip_city.data
local_category=../lib/cat_tree.txt
config_file="hdfs://mycluster/projects/news/model/configs/config_less.json"
city_file="hdfs://mycluster/projects/news/model/configs/ip_city_dummy.data"
# category_data="hdfs://mycluster/projects/news/model/configs/cat_tree.txt"

# data pathes
if [[ "$app_lan" == "hi" ]]; then
    input_user_root="/projects/news/model/hindi_up"
else
    input_user_root="/projects/news/user_profile/data/V3/up_json"
fi
# input_cp_template="/projects/news/cpp/feeder/in_cp_dump/%s/*/*/*.data"
#input_cp_template="/projects/news/model/experiments/merged_cp_multiple_days/20160321-previous"
input_interest_root="s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall"
input_cp_template="/projects/news/model/experiments/merged_cp_multiple_days/20160608-20160619"
kw_split=10000000
# output path
output_root="/projects/news/model/training"
# raw_cfb_template="$output_root/raw_cfb/${app_lan}_offline_cfb_raw_new/%s"
raw_cfb_template="$output_root/raw_cfb/${app_lan}_offline_cfb_test/%s"
merged_cfb_root="$output_root/merged_cfb/${app_lan}_feature_aggcfb_test"
output_s3_path="s3://com.cmcm.instanews.usw2.prod/model/experiments/${app_lan}_offlinecfb_training_data_test";
featmap_path=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}
featmap_file_hdfs=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}.txt
