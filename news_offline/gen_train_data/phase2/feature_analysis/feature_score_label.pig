
REGISTER '/home/chenkehan/news_model/home/india_news_ranking/news_offline/gen_train_data/lib/*.jar';
REGISTER '/home/chenkehan/news_model/home/india_news_ranking/news_offline/gen_train_data/lib/feature_udf.py' USING jython AS myudf;

set mapred.create.symlink yes;
set mapred.cache.files s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/20160612-20160613.txt#feature.map;


data = load '/tmp/chenkehan/events_with_feature' as (label:int, gbdt:double, feature:double);

data_bin = foreach data generate label, gbdt, (feature is null? -1 : (int)(feature*1000)) as feature_bin;

res = foreach (group data_bin by feature_bin PARALLEL 200) generate group, (double)SUM($1.label)/COUNT($1) as ctr, AVG($1.gbdt) as avg_score, COUNT($1) as cnt;

rmf /tmp/chenkehan/res;
--store res into '/tmp/chenkehan/res';


raw_training_data = LOAD 'hdfs://mycluster/projects/news/model/training/us_20160604_20160610_cfb_1_n_train_1/ready_to_gbdt/training_data_offline/split_events_20160604-20160610' as (uid:chararray, content_id:chararray, label:int, dwelltime:float, train_data:chararray, ts:long);

raw_training_data_eval = FOREACH raw_training_data GENERATE label, myudf.parse_libsvm(train_data) as train_data;

data = foreach raw_training_data_eval generate (double)train_data#'D_TITLEMD5_COEC' as feature, label;

data_bin = foreach  data generate (feature is null? -1 : (int)(feature*1000)) as feature_bin, label;

res = foreach (group data_bin by feature_bin PARALLEL 200) generate group, (double)SUM($1.label)/COUNT($1) as ctr, COUNT($1) as cnt;

store res into '/tmp/chenkehan/res';


