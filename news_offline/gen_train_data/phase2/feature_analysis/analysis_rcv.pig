REGISTER '../../lib/*.jar';
REGISTER '../../lib/utils.py' USING jython AS myfunc;

set mapred.create.symlink yes;
set mapred.cache.files s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/20160612-20160613.txt#feature.map;
set default_parallel 200;
set fs.s3a.connection.maximum 1000;

raw_rcv_log = LOAD '$INPUT_EVENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate
    json,
    (json#'app_lan' is null ? 'en' : ( json#'app_lan' MATCHES 'hi.*' ? 'hi' : 'en' )) as app_lan,
    json#'contentid' as content_id:chararray,
    json#'aid' as aid:chararray,
    --json#'ip' as ip:chararray,
    --json#'servertime_sec' as eventtime,
    json#'pid' as pid:chararray,
    json#'ctype' as ctype,
    --(int)json#'ext'#'dwelltime' AS dwelltime:int,
    json#'act' as act,
    json#'upack'#'exp' as exp,
    json#'mcc' as mcc,
    myfunc.parse_rid(json#'cpack') as rid:chararray;

raw_rcv_log = filter raw_rcv_log by pid == '1' and  ( ctype is not null and (ctype=='1' or ctype=='0x01' ) ) and act is not null and ( act == '1' or act == '2' ) and json#'scenario'#'level1_type' =='1' and json#'scenario'#'level1' == '1' and exp matches '.*us.*' ;

raw_rcv_log = foreach raw_rcv_log generate aid, myfunc.last_char(aid) as last_char, content_id, act, rid, mcc;
raw_rcv_log = distinct raw_rcv_log;

res = foreach (group raw_rcv_log by mcc) generate group, COUNT($1) as cnt;

rmf /tmp/chenkehan/res;
store res into '/tmp/chenkehan/res';


