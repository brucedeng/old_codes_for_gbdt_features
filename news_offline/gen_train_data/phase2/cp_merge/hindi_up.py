# -*- coding:utf-8 -*-
import os
import datetime
import time
import math,sys
import random

if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper
    import json
else:
    import com.xhaus.jyson.JysonCodec as json

@outputSchema('up_json:chararray')
def format_hindi_up(up_str):
    if up_str is None or len(up_str) <=0:
        return up_str
    try:
        up_map = json.loads(up_str)
        up_map['u_cat_len'] = up_map.get('u_cat_len_hindi',0)
        up_map['u_kw_len'] = up_map.get('u_kw_len_hindi',0)
        up_map['categories'] = up_map.get('categories_hindi','[]')
        up_map['keywords'] = up_map.get('keywords_hindi','[]')
        del(up_map['u_cat_len_hindi'])
        del(up_map['u_kw_len_hindi'])
        del(up_map['categories_hindi'])
        del(up_map['keywords_hindi'])
        return json.dumps(up_map)
    except:
        print "error formating up."
        return up_str


@outputSchema('up_json:chararray')
def format_category(up_str):
    try:
        up_map=json.loads(up_str)
        up_map['categories']=up_map.get('l2_categories',[])
        return json.dumps(up_map)
    except:
        print "error formating up."
        return up_str


@outputSchema('data:(id:chararray,updatetime:long)')
def gen_cp_id_uptime(cp_str):
    if cp_str is None or len(cp_str) <=0:
        return ('',0)
    try:
        cp_map = json.loads(cp_str)
        item_id = cp_map['item_id']
        updatetime = cp_map['update_time']
        return (item_id,updatetime)
    except:
        print "error parsing cp."
        return ('',0)

@outputSchema('data:(id:chararray,updatetime:long,type:chararray)')
def gen_cp_id_uptime_type(cp_str):
    if cp_str is None or len(cp_str) <=0:
        return ('',0)
    try:
        cp_map = json.loads(cp_str)
        item_id = cp_map['item_id']
        updatetime = cp_map['update_time']
        tp = cp_map.get('type','')
        return (item_id,updatetime,tp)
    except:
        print "error parsing cp."
        return ('',0,'')

@outputSchema('data:(id:chararray,u_cat_len:int,u_kw_len:int)')
def gen_up_id_catlen(up_str):
    if up_str is None or len(up_str) <=0:
        return ('',0)
    try:
        up_map = json.loads(up_str)
        item_id = up_map['uid']
        cat_len = up_map['u_cat_len']
        kw_len = up_map['u_kw_len']
        return (item_id,cat_len,kw_len)
    except:
        print "error parsing cp."
        print up_str
        return ('',0,0)

@outputSchema('data:{T:(id:chararray,u_cat_len:int,u_kw_len:int)}')
def random_sample(input_bag,count):
    if input_bag is None or len(input_bag) <=0:
        return []
    try:
        result = random.sample(input_bag, count)
        return result
    except:
        print "error sampling cp."
        return ('',0,0)


if __name__ == '__main__':
    print format_hindi_up('{"aid:" : "7defce83f4a7", "uid" : "7defce83f4a7", "version" : "1", "join_flag" : 0, "gender" : "2", "age" : "2", "new_user" : "", "u_cat_len" : 0, "u_kw_len" : 0, "categories" : [], "keywords" : [], "u_cat_len_hindi" : 0, "u_kw_len_hindi" : 0, "categories_hindi" : ["aa"], "keywords_hindi" : [],"lasttime": ""}')

