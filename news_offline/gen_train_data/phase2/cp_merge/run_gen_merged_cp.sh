
queue=default
while getopts "s:e:q:" arg
do
    case $arg in
        s) #start date of events
        event_start=$OPTARG
        ;;
        e) #end date  of event
        event_end=$OPTARG
        ;;
        q) #run data
        queue=$OPTARG
        ;;
        ?)  #?
        echo "unkonw argument"
        exit 1
        ;;
    esac
done
                                                                                                                                                                                    
#PIG_PARAM="-conf ./default-hadoop.xml -useHCatalog ";
PIG_PARAM=" -Dmapred.job.queue.name=$queue";

echo $event_start
echo $event_end
cur_date=$event_start

set -e

ftr_start=$event_start
ftr_end=$event_end;
agg_dates=$ftr_start
for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done

input_contents="/projects/news/cpp/feeder/in_cp_dump/{$agg_dates}/*/*/*.data"
output="/projects/news/user_profile/model/experiments/merged_cp_multiple_days/${event_start}-${event_end}"
cmd="pig $PIG_PARAM -p cp_dump='$input_contents' -p output='$output' gen_merged_cp_v1.pig"
echo "$cmd"
eval $cmd



