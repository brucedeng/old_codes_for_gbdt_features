###configs begin
join_rcv_featurelog="true"
run_split_data="true"
run_format_data="true"
event_start=20160625
event_end=20160629
queue_name=experiment
expid=cm_ru_001
country=RU
predictor_id=ru_ru_exp
cfb_pid=1

#generate splited data
kw_split=200;
train_sample_rate=1.0;
PIG_PARAM=" -Dmapred.job.queue.name=$queue_name";
TOP_PARAM="-p U_NCAT=5 -p U_NKEY=25 -p D_NCAT=5 -p D_NKEY=15 -p WND=600"
local_config=config_less.json
local_category=../lib/cat_tree.txt
config_file="hdfs://mycluster/projects/news/model/configs/config_less.json"
category_data="hdfs://mycluster/projects/news/model/configs/cat_tree.txt"

# data pathes
output_s3_path="/projects/news/model/experiments/training_data_from_featurelog/${expid}_${predictor_id}_${country}";
featmap_path=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}
featmap_file_hdfs=s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/${event_start}-${event_end}.txt

###configs end

if [[ "$join_rcv_featurelog" == "true" ]]; then
    PIG_PARAM="-Dmapred.job.queue.name=$queue_name -Dmapred.max.map.failures.percent=5";


    echo $event_start
    echo $event_end
    cur_date=$event_start

    rm -f pig*.log

    hadoop fs -copyFromLocal featmap.txt $featmap_file_hdfs
    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do

        input_event="/projects/news/data/raw_data/{impression,click,listpagedwelltime,readtime}/$cur_date/*/*"
        feature_log="s3://com.cmcm.instanews.usw2.prod/model/feature_log/$cur_date/*/*"
        prev_date=`date -d "$cur_date -1 days" +%Y%m%d`

        output="${output_s3_path}/${cur_date}";

        cmd="pig $PIG_PARAM $TOP_PARAM -p featmap_file_hdfs=$featmap_file_hdfs -p cfb_pid=$cfb_pid -p country=$country -p INPUT_EVENT='$input_event' -p INPUT_FEATURE_LOG='$feature_log' -p exp='$expid' -p predictor_id='$predictor_id' -p OUTPUT='$output' join_rcv_featurelog.pig"
        eval $cmd;

        if [ $? != 0 ]; then echo "ERROR: Failed to run pipeline!"; exit -1; fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done

fi


set -e
# test if all needed variables are assigned.
test_var="${PIG_PARAM?} ${TOP_PARAM?}"

if [[ "$run_split_data" == "true" ]]; then
    test_var="${output_s3_path?} ${kw_split?}"

    ftr_start=$event_start
    ftr_end=$event_end
    agg_dates=$ftr_start
    for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done

    input_path="${output_s3_path}/{$agg_dates}"
    output_path="${output_s3_path}/split_events_$ftr_start-$ftr_end"
    if [[ a"$split_seed" == "a" ]];then
        split_seed=$RANDOM
    fi
    echo "spliting using seed $split_seed"

    cmd="pig $PIG_PARAM -p SEED='$split_seed' -p KW_SPLIT='$kw_split' -p INPUT='$input_path' -p OUTPUT='$output_path' split_training_data.pig"
    echo "$cmd";
    eval $cmd
fi

if [[ "$run_format_data" == "true" ]]; then
    test_var="${output_s3_path?} ${event_end?} ${event_start?}"

    echo "formatting for all users"
    train_path="$output_s3_path/split_events_$event_start-$event_end/*_{0,1,2,3}"
    test_path="$output_s3_path/split_events_$event_start-$event_end/*_4"
    output_path="$output_s3_path/split_events_$event_start-$event_end-libsvm"
    cmd="pig $PIG_PARAM -p train='$train_path' -p test='$test_path' -p OUTPUT='$output_path' format_xgboost_train_data.pig"
    echo "$cmd";
    eval $cmd

fi
