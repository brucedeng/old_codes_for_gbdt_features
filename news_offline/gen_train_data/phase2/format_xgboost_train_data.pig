REGISTER '../lib/*.jar';
REGISTER '../lib/feature_udf.py' USING jython AS myudf;
 
-- set default_parallel 500;
set job.name '$job_name';

raw_training_data = LOAD '$train' as (uid:chararray, content_id:chararray, label:int, dwelltime:float,
    train_data:chararray, ts:long, bin:chararray);

train_data_weight = FOREACH raw_training_data GENERATE uid,content_id, label, LOG(dwelltime+1) as wt, train_data;

-- train_data_balance = FOREACH train_data_weight GENERATE label, wt, (label == 1? wt:0.0) as pos_wt, (label == 1? 0.0:1.0) as neg_wt;
-- balance_rate = FOREACH ( GROUP train_data_balance all ) GENERATE 
--     (SUM($1.neg_wt)/SUM($1.pos_wt)<10 ? SUM($1.neg_wt)/SUM($1.pos_wt):10) as ratio;

-- train_data_with_weight = FOREACH train_data_weight GENERATE uid, content_id,label, (label ==1? wt*balance_rate.ratio : 1 ) as weight, train_data as train_data;
train_data_with_weight = FOREACH train_data_weight GENERATE uid, content_id,label, (label ==1? wt : 1 ) as weight, train_data as train_data;

libsvm_data = FOREACH train_data_with_weight GENERATE myudf.add_libsvm_weight(train_data,weight) as train_data;

rmf $OUTPUT
store libsvm_data into '$OUTPUT/train';

raw_training_data = LOAD '$test' as (uid:chararray, content_id:chararray, label:int, dwelltime:float,
    train_data:chararray, ts:long, bin:chararray);

libsvm_data = FOREACH raw_training_data GENERATE train_data;

store libsvm_data into '$OUTPUT/test';
