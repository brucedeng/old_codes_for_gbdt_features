

eval_data=$1

if [[ $# -le 3 ]]; then
    echo  "usage: $0 eval_data model_file"
fi

model_file="$2"
if [[ "a$model_file" == "a" ]]; then
    echo "usage: $0 eval_data model_file"
    exit 1
fi

test_file_bin="$3"

set -e
# test if all needed variables are assigned.
model_filename=`basename $model_file`

model_dump=/tmp/chenkehan/models_dump/$model_filename
hadoop fs -mkdir -p /tmp/chenkehan/models_dump
hadoop fs -copyFromLocal -f $model_file $model_dump

data_path="$eval_data"
output_path="${data_path}.eval"

PIG_PARAM="-Dmapred.job.queue.name=experiment"

featmap_file_hdfs="s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/20160321-20160327.txt"

cmd="pig $PIG_PARAM -p INPUT='$data_path/{*_4}' -p output_metric='$output_path' -p modelfiles=$model_dump -p featmap_file=$featmap_file_hdfs evaluate_model_weighted.pig"

echo "$cmd";
eval $cmd
