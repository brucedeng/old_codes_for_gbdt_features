
INPUT=$1
OUTPUT=$2

hadoop fs -rmr $2

hadoop jar \
    /usr/lib/hadoop-mapreduce/hadoop-streaming-2.6.0-cdh5.4.8.jar \
    -Dmapred.reduce.tasks=1 \
    -Dmapred.job.queue.name=experiment \
    -input "$INPUT" \
    -output "$OUTPUT" \
    -mapper "parse_libsvm.py" \
    -reducer "cat"  \
    -file "parse_libsvm.py"



