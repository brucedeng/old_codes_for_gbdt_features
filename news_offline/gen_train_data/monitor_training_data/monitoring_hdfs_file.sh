#!/bin/bash
SEND_MESSAGE_DIR=/data1/dengkun/send_message_bin
telephone=15652050090

fun_check_hdfsdate() {
    input_flag=$1
    ret=1
    try_time=0
    while [ $ret -ne 0 ]
    do
        if [ $try_time -ge 30 ];then
            sh -x $SEND_MESSAGE_DIR/send_message.sh $telephone "701" "$input_flag not ready"
            try_time=0
            ##exit -1
        fi
        hadoop fs -ls $input_flag
        ret=$?
        if [ $ret -ne 0 ]
        then
            sleep 180
        fi
        let ++try_time
    done
}

