#!/bin/bash
source ./monitoring_hdfs_file.sh
mkdir data
mkdir src_data
prenum=$1
day=`date -d "$prenum days ago" +%Y%m%d`
queue=deeplearning
#queue=offline
PIG_PARAM=" -Dmapred.job.queue.name=$queue";

rm -f pig_*

s3_path=s3://com.cmcm.instanews.usw2.prod/user_profile/model/experiments/hi_training_data_from_featurelog_newformat/$day

input_path="${s3_path}/train.gz"
output_label="${s3_path}/label_monitor"
output_feature="${s3_path}/feature_monitor"
fun_check_hdfsdate "$input_path/_SUCCESS"

cmd="pig $PIG_PARAM -p INPUT='$input_path' -p OUTPUT_LABEL='$output_label' -p OUTPUT_FEATURE='$output_feature'  statistic_feature.pig"
echo "$cmd";
eval $cmd

if [ $? != 0 ] 
then
    message="ERROR: monitor dnn training log statistics hadoop job failed"
    echo "$message"
    sh -x $SEND_MESSAGE_DIR/send_message.sh $telephone "701" "$message"
    exit -1
fi

###download data
###label format: label label_num label_mean_weight
hadoop fs -getmerge $output_label src_data/label.$day
###feature data format: feature feature_num feature_mean feature_variance
hadoop fs -getmerge $output_feature src_data/feature.$day

###file handle
awk -F'\t' '{print "'$day'\t"$0; num+=$2; weight += $2*$3}END{print "'$day'\tall\t"num"\t"weight/num}' src_data/label.$day > data/label.$day

awk -F'\t' 'NR==FNR{hash[$1]=$2}NR>FNR{
    if($2>max) max=$2
    num[hash[$1]]+=$2
    mean[hash[$1]]+=$3
    var[hash[$1]]+=$4
        }END{for(i in num) print "'$day'\t"i"\t"num[i]"\t"num[i]/max"\t"mean[i]"\t"var[i]
    }' featmap.txt  src_data/feature.$day |sort -rnk 3 > data/feature.$day

###merge files
days1ago=`date -d"1 days ago $day" +%Y%m%d`
days7ago=`date -d"7 days ago $day" +%Y%m%d`
rm -f label.txt
cat data/label.{$day,${days1ago},${days7ago}} > label.txt
rm -f feature.txt
cat data/feature.{$day,${days1ago},${days7ago}} > feature.txt

python send_email.py label.txt feature.txt
