REGISTER '../lib/*.jar';
REGISTER '../lib/utils.py' USING jython AS myudf;

--set job.name '$job_name';


-- set default_parallel 500;

raw_training_data = LOAD '$INPUT' as (uid:chararray, content_id:chararray, train_data:chararray);
raw_label_weight = FOREACH raw_training_data GENERATE FLATTEN(myudf.parse_label_weight(train_data)) as (label:chararray, weight:double);
raw_label_weight = FOREACH raw_label_weight GENERATE label, (double)weight as weight;
sum_label_weight = FOREACH (group raw_label_weight by label) GENERATE
    $0 as label,
    COUNT($1) as num,
    AVG(raw_label_weight.weight) as avg_weight;
rmf $OUTPUT_LABEL
store sum_label_weight into '$OUTPUT_LABEL';

raw_feature_weight = FOREACH raw_training_data GENERATE flatten(myudf.parse_feature_weight(train_data)) as (feature:chararray, weight:double, weight_square:double);
raw_feature_weight = FOREACH raw_feature_weight GENERATE
    feature,
    (double)weight as weight,
    (double)weight_square as weight_square;

sum_feature_weight = FOREACH (GROUP raw_feature_weight BY feature ) GENERATE
    $0 as feature,
    COUNT($1) as num,
    AVG(raw_feature_weight.weight) as mean,
    AVG(raw_feature_weight.weight_square) as mean_ws;
feature_num_mean_square = FOREACH sum_feature_weight GENERATE
    feature,
    num,
    mean,
    mean_ws - mean * mean as variance;

rmf $OUTPUT_FEATURE
store feature_num_mean_square into '$OUTPUT_FEATURE';
