--%DEFAULT SAMPLE_RATIO 0.02
%DEFAULT WEIGHT_INDEX 3
%DEFAULT SAMPLE_RATIO 1
%DEFAULT LABEL_INDEX 5

set mapred.create.symlink yes;
set mapred.cache.files $featmap_file#feature.map,$CAT_TREE#cat_tree.txt;

%DECLARE PIG_COL_TYPE "id:chararray,uid:chararray,content_id:chararray,ts:long,label:chararray,city:chararray,publisher:chararray,cp_update_time:chararray,d_raw:chararray,u_raw:chararray,cp_type:chararray,publish_time:chararray,u_age:chararray,u_gender:chararray,cfb_json:chararray,dwelltime:float,listpagedwell:float,declare_match:int,declare_cnt:int"
%DECLARE PIG_COL "id,uid,content_id,ts,label,city,publisher,cp_update_time,d_raw,u_raw,cp_type,publish_time,u_age,u_gender,cfb_json,dwelltime,listpagedwell,declare_match,declare_cnt"

REGISTER 'lib/*.jar';
REGISTER 'feature_udf.py' USING jython AS myudf;
REGISTER 'interest_utils.py' USING jython AS interest;
--DEFINE HEADER `python append_header.py` ship('append_header.py');
 
DEFINE featmap(raw_training_data) RETURNS output_data {
    featmap_out = FOREACH $raw_training_data GENERATE FLATTEN(myudf.gen_train_featmap(static_features, cfb_features)) as fname;
    featmap_dist = DISTINCT featmap_out;
    $output_data = FOREACH ( GROUP featmap_dist ALL ) {
        data = ORDER $1 BY fname;
        GENERATE FLATTEN(myudf.format_featmap(data)) as (fid,fname,ftype);
    }
}

DEFINE gen_data(raw_training_data) RETURNS output_data {
    
    $output_data = FOREACH $raw_training_data GENERATE
            uid, content_id, label, dwelltime,
            myudf.format_train_data(label,static_features, cfb_features) AS train_data;
            -- static_features as static_features, 
            -- cfb_features as cfb_features;
}

DEFINE gen_data_tsv(raw_training_data) RETURNS output_data {
    
    $output_data = FOREACH $raw_training_data GENERATE
            uid, 
            content_id, 
	        label,
            myudf.format_train_data_tsv(label,static_features, cfb_features) AS train_data,
            static_features as static_features, 
            cfb_features as cfb_features;
    
    --output_seq = foreach (group out ALL) generate flatten(out);
    --$output_data = Stream output_seq THROUGH HEADER;
}

DEFINE MY_SAMPLE `python sample_negative_train_data.py $SAMPLE_RATIO $LABEL_INDEX $WEIGHT_INDEX` ship('sample_negative_train_data.py');

rmf $output
raw = LOAD '$input' AS (
  ${PIG_COL_TYPE}
);

grp = group raw by uid PARALLEL 400;
grp = sample grp 0.1;
raw = foreach grp generate flatten(raw);

declare_interest = load '$INTEREST' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);
raw_interest = FOREACH declare_interest GENERATE json#'aid' as aid, json#'interest' as interest, json#'time' as time;

uniq_interest = FOREACH ( GROUP raw_interest by aid ) {
    r = ORDER $1 BY  time;
    l = LIMIT r 1;
    GENERATE FLATTEN(l) as (aid, interests, time);
}

data_with_interest = FOREACH ( JOIN raw by uid left outer, uniq_interest by aid) GENERATE
    $PIG_COL,
    FLATTEN(interest.get_category_matched(d_raw,interests)) as (declare_match, declare_cnt);

-- data_sample = STREAM raw THROUGH MY_SAMPLE as (label:int, nsample, sample_weight, ${PIG_COL_TYPE}); 

data_sample_train = FOREACH data_with_interest {
    doc_age = myudf.gen_doc_age(ts, publish_time);
    static_features = myudf.gen_phase1_features(u_raw, d_raw, TOMAP('DOC_AGE',doc_age,'DECLARE_REL',declare_match), null);
    --new_user_flag = myudf.is_new_user(u_raw)
    cfb_features = myudf.parse_and_get_gmp(cfb_json,20);
    GENERATE
        uid, content_id, label, dwelltime, static_features as static_features, cfb_features as cfb_features, 0 as new_user_flag;
}

-- rmf $output/data_sample_train
-- STORE data_sample_train into '$output/data_sample_train';

data_sample_train = filter data_sample_train by new_user_flag == 0;

output_data = $METHOD(data_sample_train);

output_data = foreach output_data generate uid, content_id, label, train_data;

rmf $output;
store output_data INTO '$output';

