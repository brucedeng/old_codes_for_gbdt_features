
train_config_file=$1
source $train_config_file

echo $event_start
echo $event_end

set -e
# test if all needed variables are assigned.
test_var="${PIG_PARAM?}"


if [[ "$run_training_data" == "true" ]]; then
    # test if all needed variables are assigned.
    test_var="${merged_cfb_root?} ${input_interest_root?} ${featmap_file_hdfs?}"

    #hadoop fs -rm -skipTrash $featmap_file_hdfs
    #hadoop fs -copyFromLocal featmap.txt $featmap_file_hdfs
    #if [ $? != 0 ]; then echo "Failed to put feature list onto hdfs!"; exit -1; fi

    ftr_start=$event_start
    ftr_end=$event_end
    agg_dates=$ftr_start
    for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do                      agg_dates="$agg_dates,$agg_day"; done

    input_path="$merged_cfb_root/{$agg_dates}/events_with_cfb_features"

    interest_date=`date -d "+1 days $event_end" +%Y%m%d`;
    input_interest="$input_interest_root/$interest_date"
          
    output="${output_path}/$event_start-$event_end";

    cmd="pig $PIG_PARAM -p input='$input_path' -p output='$output' -p METHOD=gen_data -p INTEREST=$input_interest -p CAT_TREE=$category_data -p featmap_file=$featmap_file_hdfs get_phase1_train_data.pig";
    echo $cmd;
    eval $cmd;

    if [ $? != 0 ]; then echo "Failed!"; exit -1; fi
    cur_date=`date -d "1 day $cur_date" +%Y%m%d`;

    echo "Done";
 fi
