
if [ $# -lt 1 ];
then
    echo "No configure file!";
    exit -1;
fi

source ./$1;
if [ $? != 0 ]; then echo "Get parameters failed!"; exit -1; fi

start=$run_start;
end=$run_end;
sample_rate=$train_sample_rate;
output_root=$output_root

set -e
# whether to calculate fmap or not
gen_fmap=$gen_fmap

category_data="hdfs://mycluster/projects/news/model/configs/cat_tree.txt"
hadoop fs -copyFromLocal -f lib/cat_tree.txt $category_data
#non_cfb_feat_file=$noncfb_feature;
#noncfb_feat_num=`cat noncfb_feature.list | awk -F"\t" '{print NF}'`;

if [[ "a$featmap_path" == "a" ]]; then
    #output_fmap="s3://com.cmcm.instanews.usw2.prod/user/mengchong/cfb_training_featmap/$start-$end";
    output_fmap="s3://com.cmcm.instanews.usw2.prod/model/experiments/${app_lan}_cfb_training_featmap/$start-$end"
else
    output_fmap="$featmap_path";
fi

if [[ "a$featmap_file_hdfs" == "a" ]]; then
    #featmap_file_hdfs="s3://com.cmcm.instanews.usw2.prod/user/mengchong/cfb_training_featmap/$start-$end.txt";
    featmap_file_hdfs="s3://com.cmcm.instanews.usw2.prod/model/experiments/${app_lan}_cfb_training_featmap/$start-$end.txt"
fi

if hadoop fs -test -e $featmap_file_hdfs; then
    echo $featmap_file_hdfs exists
    if [[ -e featmap.txt ]]; then
        echo "copying local featmap.txt to hdfs"
        hadoop fs -copyFromLocal -f featmap.txt $featmap_file_hdfs
    fi
else
    basefile=`dirname $featmap_file_hdfs`
    hadoop fs -mkdir -p $basefile
    if [[ -e featmap.txt ]]; then
        echo "copying local featmap.txt to hdfs"
        hadoop fs -copyFromLocal -f featmap.txt $featmap_file_hdfs
    else
        hadoop fs -touchz $featmap_file_hdfs
    fi
fi

pig_param=" -Dmapred.job.queue.name=experiment -p SAMPLE_RATIO=$sample_rate -p CAT_TREE=$category_data -p INTEREST=$input_interest ";

if [[ "$gen_fmap" == "yes" ]]; then
    ftr_start=$start
    ftr_end=$end
    agg_dates=$ftr_start
    for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done

    pre_date=`date -d "-1 day $cur_date" +%Y%m%d`;
    input_path="$input_root/{${agg_dates}}/events_with_cfb_features"

    cmd="pig $pig_param -p input='$input_path' -p output='$output_fmap' -p METHOD='featmap' -p featmap_file=$featmap_file_hdfs get_raw_train_data.pig";
    echo $cmd;
    eval $cmd;

    if [ $? != 0 ]; then echo "Failed!"; exit -1; fi

    hadoop fs -cat $output_fmap/p* > featmap.txt
fi

hadoop fs -rm -skipTrash $featmap_file_hdfs
hadoop fs -copyFromLocal featmap.txt $featmap_file_hdfs

if [ $? != 0 ]; then echo "Failed to put feature list onto hdfs!"; exit -1; fi


cur_date=$start;
while [ $cur_date -le $end ];
do
    # if [ $cur_date -lt 20160207 ]; then
    #     cur_date=`date -d "1 day $cur_date" +%Y%m%d`;
    #     continue
    # fi
    pre_date=`date -d "-1 day $cur_date" +%Y%m%d`;
    input_path=$input_root/$cur_date/events_with_cfb_features
    output="${output_root}/$cur_date";
    cmd="pig $pig_param -p input='$input_path' -p output='$output' -p METHOD=gen_data -p featmap_file=$featmap_file_hdfs get_raw_train_data.pig";
    echo $cmd;
    eval $cmd;

    if [ $? != 0 ]; then echo "Failed!"; exit -1; fi
    cur_date=`date -d "1 day $cur_date" +%Y%m%d`;
done

echo "Done";
