
if [ $# -lt 1 ];
then
    echo "No configure file!";
    exit -1;
fi

source ./$1;
if [ $? != 0 ]; then echo "Get parameters failed!"; exit -1; fi

start=$run_start;
end=$run_end;
sample_rate=$train_sample_rate;

set -e
# whether to calculate fmap or not
gen_fmap=$gen_fmap

cfb_feat_file=$feature;
#non_cfb_feat_file=$noncfb_feature;
#noncfb_feat_num=`cat noncfb_feature.list | awk -F"\t" '{print NF}'`;

if [[ "a$featmap_path" == "a" ]]; then
    output_fmap="s3://com.cmcm.instanews.usw2.prod/mengchong/cfb_training_featmap/$start-$end";
else
    output_fmap="$featmap_path";
fi

if [[ "a$featmap_file_hdfs" == "a" ]]; then
    featmap_file_hdfs="s3://com.cmcm.instanews.usw2.prod/mengchong/cfb_training_featmap/$start-$end.txt";
fi

if hadoop fs -text $featmap_file_hdfs; then
    echo $featmap_file_hdfs exists
else
    hadoop fs -touchz $featmap_file_hdfs
fi

if [[ "$gen_fmap" == "yes" ]]; then
    ftr_start=$start
    ftr_end=$end
    agg_dates=$ftr_start
    for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done

    pre_date=`date -d "-1 day $cur_date" +%Y%m%d`;
    input_path="$input_root/{${agg_dates}}/events_with_cfb_features"

    cmd="pig $pig_param -p input='$input_path' -p output='$output_fmap' -p METHOD='featmap' -p featmap_file=$featmap_file_hdfs get_raw_train_data.pig";
    echo $cmd;
    eval $cmd;

    if [ $? != 0 ]; then echo "Failed!"; exit -1; fi

    hadoop fs -cat $output_fmap/p* > featmap.txt
    hadoop fs -rm -skipTrash $featmap_file_hdfs
fi

#hadoop fs -copyFromLocal featmap.txt $featmap_file_hdfs

if [ $? != 0 ]; then echo "Failed to put feature list onto hdfs!"; exit -1; fi

pig_param=" -p SAMPLE_RATIO=$sample_rate";

cur_date=$start;
while [ $cur_date -le $end ];
do
    pre_date=`date -d "-1 day $cur_date" +%Y%m%d`;
<<<<<<< HEAD
    input_path=/tmp/mengchong/cfb_agg/offline_cfb_raw/$cur_date/events_with_cfb_features
    #output="s3://com.cmcm.instanews.usw2.prod/mengchong/cfb_training_data/$cur_date";
    output="s3://com.cmcm.instanews.usw2.prod/mengchong/linear_training_data/$cur_date";
    cmd="pig $pig_param -p input='$input_path' -p output='$output' -p METHOD=gen_data_tsv -p featmap_file=$featmap_file_hdfs get_raw_train_data.pig";
=======
    input_path=$input_root/$cur_date/events_with_cfb_features
    output="s3://com.cmcm.instanews.usw2.prod/mengchong/cfb_training_data/$cur_date";
    cmd="pig $pig_param -p input='$input_path' -p output='$output' -p METHOD=gen_data -p featmap_file=$featmap_file_hdfs get_raw_train_data.pig";
>>>>>>> c7a7d763b82bb829657750aecba4609bcca97673
    echo $cmd;
    eval $cmd;

    if [ $? != 0 ]; then echo "Failed!"; exit -1; fi
    cur_date=`date -d "1 day $cur_date" +%Y%m%d`;
done

echo "Done";
