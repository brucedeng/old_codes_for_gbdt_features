

while getopts "s:e:m:" arg
do
    case $arg in
        s) #start date of events
        event_start=$OPTARG
        ;;
        e) #end date  of event
        event_end=$OPTARG
        ;;
        m)
        model_file=$OPTARG
        ;;
        ?)  #?
        echo "unkonw argument"
        exit 1
        ;;
    esac
done

set -e

start=$run_start;
end=$run_end;
sample_rate=$train_sample_rate;

set -e
# whether to calculate fmap or not
gen_fmap=$gen_fmap

category_data="hdfs://mycluster/projects/news/model/configs/cat_tree.txt"
hadoop fs -copyFromLocal -f cat_tree.txt $category_data

model_file_name=`basename $model_file`
model_file_hdfs="hdfs://mycluster/projects/news/model/configs/$model_file_name"
hadoop fs -copyFromLocal -f $model_file $model_file_hdfs

input_interest=s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall/$event_end

pig_param=" -Dmapred.job.queue.name=offline -p CAT_TREE=$category_data -p INTEREST=$input_interest ";

ftr_start=$event_start;
ftr_end=$event_end;
agg_dates_event=$ftr_start
for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates_event="$agg_dates_event,$agg_day"; done
input_root="/projects/news/model/experiments/features/en_raw_features_joined_with_nrtcfb"
input_path="$input_root/{${agg_dates_event}}/events_with_cfb_features"
output="s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_evaluation/20160110/$model_file_name-$event_start-$event_end"

cmd="pig $pig_param -p input='$input_path' -p output='$output' -p model_file=$model_file_hdfs model_scoring.pig";
echo $cmd;
eval $cmd;

echo "Done";
