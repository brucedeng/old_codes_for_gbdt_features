# -*- coding:utf-8 -*-
import os
import datetime
import time
import math,sys
import hashlib

if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper
    import json
else:
    import com.xhaus.jyson.JysonCodec as json

feature_seq = chr(2)

def getStopWords():
    if getStopWords.stop_words is None:
        try:
            stop_words = set()
            IN = open('stop_words_file', 'r')
            line = IN.readline()
            while line:
                stop_words.add(line.rstrip('\r\n'))
                line = IN.readline()
            IN.close()
            print "loaded %d stop words " % len(stop_words)
            getStopWords.stop_words = stop_words
            return getStopWords.stop_words
        except:
            print >> sys.stderr, traceback.print_exc()
    else:
        return getStopWords.stop_words
getStopWords.stop_words = None

@outputSchema("data:{T:(name:chararray,weight:double)}")
def merge_cat_weights(cats,cutoff,user_cutoff):
    if cats is None:
        return []
    result = {}
    sum_cnt = 0
    for cat_bag in cats:
        for cat_tuple in cat_bag[0]:
            name = cat_tuple[0]
            weight = cat_tuple[1]
            sum_cnt += 1
            if name not in result:
                result[name] = weight
            else:
                result[name] += weight
    if sum_cnt < user_cutoff:
        return []
    else:
        return sorted(result.items(), key = lambda x : x[1], reverse=True)[:cutoff]

@outputSchema("data:{T:(name:chararray,weight:double)}")
def merge_kw_weights(kws,cutoff,user_cutoff):
    if kws is None:
        return []
    stop_words = getStopWords()
    result = {}
    weight_sum = []
    for kw_bag in kws:
        for kw_tuple in kw_bag[0]:
            name = kw_tuple[0]
            weight = kw_tuple[1]
            if weight is None:
                continue
            if name in stop_words:
                continue
            if name not in result:
                result[name] = weight
            else:
                result[name] += weight
    return sorted(result.items(), key = lambda x : x[1], reverse=True)[:cutoff]

@outputSchema("data:{T:(name:chararray,weight:double)}")
def l1_norm(cats,norm_factor):
    if cats is None:
        return []
    sum_w = sum([x[1] for x in cats])
    if sum_w <= 0:
        return []

    sum_w += norm_factor
    return [(x[0],x[1]/sum_w) for x in cats]

@outputSchema("data:{T:(name:chararray,weight:double)}")
def gen_new_weight(cats, weight):
    if cats is None:
        return []
    if weight is None:
        weight = 1.0
    if weight > 600:
        weight = 600.0
    return [(x[0],weight * x[1]) for x in cats if x[1] is not None]

if __name__ == '__main__':
    print merge_cat_weights([([('1245',0.2),('1234',1.2)],),([('1245',0.2),('1234',0.2)],)],200,11)
    # print merge_kw_weights([([('1245',0.2),('1234',1.2)],),([('a',0.2),('1234',0.2)],)],200,11)
    print l1_norm([('1234', 1.4), ('1245', 0.2)],0)

