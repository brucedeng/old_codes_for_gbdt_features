
REGISTER '../lib/*.jar'
REGISTER 'merge_up.py' USING jython AS udf;

-- %default WEIGHTS 1
%default USER_CUTOFF 3

set mapred.create.symlink yes;
set mapred.cache.files /projects/news/user_profile/data/V1/stop_words.txt#stop_words_file;

rmf $output/en_weight_raw
rmf $output/hi_weight_raw
rmf $output/en_normalized
rmf $output/hi_normalized

clk_input = LOAD '$input' as ( aid:chararray, newsid:chararray, categories:bag{(name:chararray,weight:double)}, keywords:bag{(name:chararray,weight:double)}, video_flag:int, app_lan:chararray, time:chararray, dwelltime:double );

clk_input_filter = FILTER clk_input by video_flag==0;

up_dwell = FOREACH clk_input_filter {
    GENERATE aid, newsid, udf.gen_new_weight(categories,$WEIGHTS) as categories, udf.gen_new_weight(keywords,$WEIGHTS) as keywords,video_flag,app_lan, time, dwelltime;
}

up_grp = FOREACH ( GROUP up_dwell by (aid,app_lan) PARALLEL 1000 ) 
  GENERATE FLATTEN($0) as (aid, app_lan),
    udf.merge_cat_weights($1.categories,$U_CAT_CUT,$USER_CUTOFF) as categories,
    udf.merge_kw_weights($1.keywords,$U_KW_CUT,$USER_CUTOFF) as keywords;

en_events = FILTER up_grp by app_lan == 'en';
store en_events into '$output/en_weight_raw' USING parquet.pig.ParquetStorer;
hi_events = FILTER up_grp by app_lan == 'hi';
store hi_events into '$output/hi_weight_raw' USING parquet.pig.ParquetStorer;

up_l1_norm = FOREACH up_grp GENERATE aid, app_lan,
    udf.l1_norm(categories,$NORM_FACTOR) as categories,
    udf.l1_norm(keywords,0) as keywords;

cat_flat = FOREACH up_l1_norm GENERATE aid, app_lan, FLATTEN(categories) as (name, weight);
cat_flat = FOREACH cat_flat GENERATE aid, app_lan, name, weight, weight*weight as weight_square;

cat_avg = FOREACH ( GROUP cat_flat by (name,app_lan) ) GENERATE 
    FLATTEN($0) as (name,app_lan),
    (double)(COUNT($1)) as cnt,
    SUM($1.weight) as sum_v,
    SUM($1.weight_square) as sum_square;

cat_avg = FOREACH cat_avg GENERATE name, app_lan, sum_v/cnt as avg, SQRT((sum_square-sum_v*sum_v/cnt)/cnt) as std;

cat_normalized = FOREACH ( JOIN cat_flat by (name,app_lan), cat_avg by (name,app_lan) using 'replicated' ) GENERATE 
    cat_flat::aid as aid,
    cat_flat::app_lan as app_lan,
    cat_flat::name as name,
    cat_flat::weight as raw_weight,
    ((cat_flat::weight - cat_avg::avg) / cat_avg::std) as weight;

cat_user = FOREACH ( GROUP cat_normalized by (aid,app_lan)) {
	norm_categories = FOREACH $1 GENERATE name,weight;
    GENERATE FLATTEN($0) as (aid,app_lan), norm_categories as categories;
}


kw_flat = FOREACH up_l1_norm GENERATE aid, app_lan, FLATTEN(keywords) as (name, weight);
kw_flat = FOREACH kw_flat GENERATE aid, app_lan, name, weight, weight*weight as weight_square;

kw_avg = FOREACH ( GROUP kw_flat by (name,app_lan) ) GENERATE 
    FLATTEN($0) as (name,app_lan),
    (double)(COUNT($1)) as cnt,
    SUM($1.weight) as sum_v,
    SUM($1.weight_square) as sum_square;

kw_avg = FOREACH kw_avg GENERATE name, app_lan, sum_v/cnt as avg, SQRT((sum_square-sum_v*sum_v/cnt)/cnt) as std;

kw_normalized = FOREACH ( JOIN kw_flat by (name,app_lan), kw_avg by (name,app_lan) using 'replicated' ) GENERATE 
    kw_flat::aid as aid,
    kw_flat::app_lan as app_lan,
    kw_flat::name as name,
    kw_flat::weight as raw_weight,
    ((kw_flat::weight - kw_avg::avg) / kw_avg::std) as weight;

kw_user = FOREACH ( GROUP kw_normalized by (aid,app_lan)) {
    norm_keywords = FOREACH $1 GENERATE name,weight;
    GENERATE FLATTEN($0) as (aid,app_lan), norm_keywords as keywords;
}

overall_data = FOREACH ( JOIN cat_user by (aid, app_lan) full outer, kw_user by (aid, app_lan)) GENERATE 
    (cat_user::aid is not null ? cat_user::aid : kw_user::aid) as aid,
    (cat_user::app_lan is not null ? cat_user::app_lan : kw_user::app_lan )  as app_lan,
    cat_user::categories as categories,
    kw_user::keywords as keywords;

en_events = FILTER overall_data by app_lan == 'en';
store en_events into '$output/en_normalized'  USING parquet.pig.ParquetStorer;
hi_events = FILTER overall_data by app_lan == 'hi';
store hi_events into '$output/hi_normalized'  USING parquet.pig.ParquetStorer;
