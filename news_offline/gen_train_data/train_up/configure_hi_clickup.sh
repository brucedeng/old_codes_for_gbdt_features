# basic settings
event_start=20160430
event_end=20160504

# control process
run_merge_up=true

# up control
use_dwell_feature=false
category_cutoff=200
keyword_cutoff=200
user_click_cutoff=3
normalize_factor=300

# pig params
queue=experiment
PIG_PARAM=" -Dmapred.job.queue.name=$queue";
TOP_PARAM="-p U_NCAT=5 -p U_NKEY=25 -p D_NCAT=5 -p D_NKEY=15 -p WND=600"

# data pathes
input_raw_up=/projects/news/user_profile/data/V2/join_clk_cp
output_merge_up=hdfs://mycluster/projects/news/model/training/up_click
