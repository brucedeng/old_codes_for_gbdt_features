
train_config_file=$1
source $train_config_file

echo $event_start
echo $event_end

set -e
# test if all needed variables are assigned.
test_var="${PIG_PARAM?} ${TOP_PARAM?}"

if [[ "$run_merge_up" == "true" ]]; then
    # test if all needed variables are assigned.
    test_var="${input_raw_up?} ${output_merge_up?} ${normalize_factor?} ${category_cutoff?} ${keyword_cutoff?} ${user_click_cutoff?} "

    cur_date=$event_start

    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do
        ftr_start=`date -d "+30 day ago $cur_date" +%Y%m%d`;
        ftr_end=$cur_date;
        agg_dates=$ftr_start
        for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done

        input_up="$input_raw_up/{$agg_dates}"

        if [[ $use_dwell_feature == "true" ]]; then
            params="-p WEIGHTS=dwelltime"
        else
            params="-p WEIGHTS=1"
        fi

        output="$output_merge_up/$cur_date"

        cmd="pig $PIG_PARAM $params -p input='$input_up' -p output='$output' -p U_CAT_CUT=$category_cutoff -p U_KW_CUT=$keyword_cutoff -p USER_CUTOFF=$user_click_cutoff -p NORM_FACTOR=$normalize_factor merge_30_days_raw_up.pig"
        echo "$cmd"
        eval $cmd;

        if [ $? != 0 ]; then echo "ERROR: Failed to run pipeline!"; exit -1; fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done
fi
