
# basic settings
event_start=20160424;
event_end=20160504;
train_sample_rate=1.0;
app_lan=hi
batch_dedup=false
cfb_ignore_date=20160320
#split_seed=2345

# run processes
run_raw_cfb=false
run_merge_cfb=false
run_training_data=true
run_split_data=true
run_featmap=false
run_merge_events=true

# pig params
queue=offline
PIG_PARAM=" -Dmapred.job.queue.name=$queue";
TOP_PARAM="-p U_NCAT=5 -p U_NKEY=25 -p D_NCAT=5 -p D_NKEY=15 -p WND=600"

# data pathes
if [[ "$app_lan" == "hi" ]]; then
    input_gmp_template="hdfs://mycluster/projects/news/nrt/india_hindi_gmp/dump/%s/*"
else
    input_gmp_template="hdfs://mycluster/projects/news/nrt/india_en_gmp/dump/%s/*"
fi

input_cp_template="/projects/news/cpp/feeder/in_cp_dump/%s/*/*/*.data"
input_interest_root="s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall"

# output path
output_root="hdfs://mycluster/projects/news/model/training"

raw_input_user_template=hdfs://mycluster/projects/news/model/training/up_click/%s/hi_weight_raw

joined_events_root="$output_root/merged_events/${app_lan}_features_raw_click"
