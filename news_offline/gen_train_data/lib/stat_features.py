#!/usr/bin/env python
#-*-encoding:utf-8-*-

import sys
#from optparse import OptionParser
# import argparse
import os

def get_feature_map(feature_map_file):
    #load feature map file
    f3 = open(feature_map_file, 'r')
    feature_map = {}
    feature_list = []
    for line in f3:
        line = line.rstrip('\n')
        if line == '':
            continue
        fid, fname, ftype = line.split('\t')
        if fid.startswith('#'):
            fid = fid.replace('#','').strip()
        feature_map[fid] = fname
        feature_list.append((fid,fname))
    f3.close()
    return (feature_map,feature_list)

if __name__ == "__main__":

    # parser = argparse.ArgumentParser(description='calculate statistical values for features.')
    # parser.add_argument("-m", "--featmap",
    #                     dest="featmap", default=None,
    #                     help="""featmap file""")
    # parser.add_argument("input_files", # nargs='*',
    #                     help="the input data files to stat.")
    # args = parser.parse_args()

    featmap_file = "featmap.txt"#args.featmap
    # input_files = args.input_files

    # load featmap
    feature_map = {}
    if featmap_file is not None:
        (feature_map,feature_list) = get_feature_map(featmap_file)

    feature_stats = []
    f_cnt = 0
    # for data_file in input_files:
    # data_file = input_files
    # f = open(data_file)
    for line in sys.stdin:
        line=line.replace('\n','')
        label_data = line.split(' ')
        try:
            label = int(label_data[0])
        except:
            continue

        for kv in label_data[1:]:
            kv_list = kv.split(':')
            if len(kv_list) < 2:
                continue
            fid = int(kv_list[0])
            fval = float(kv_list[1])
            if len(feature_stats) <= fid:
                for k in range(fid-len(feature_stats)+1):
                    feature_stats.append([0,0.0,0.0,0.0,0.0,0.0])

            feature_stats[fid][0] += 1
            feature_stats[fid][1] += fval
            feature_stats[fid][2] += fval*fval
            feature_stats[fid][3] += label*fval
            feature_stats[fid][4] += label
            feature_stats[fid][5] += label*label

    for idx in range(len(feature_stats)):
        fid = str(idx)
        values = feature_stats[idx]
        fname = feature_map.get(fid,fid)
        print fid + '\t' + fname + '\t' + '\t'.join([str(_k) for _k in values ])
        # print fname + '\tcount\t' + str(values[0])
        # if values[0] == 0:
        #     values[0] = 1
        # print fname + '\tmean\t' + str(values[1]/values[0])
        # print fname + '\tstd\t' + str((values[2] - values[1] ** 2 / values[0])/values[0])






