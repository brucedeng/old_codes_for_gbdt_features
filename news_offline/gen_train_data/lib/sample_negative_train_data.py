#!/usr/bin/python

import sys
import random

sample_rate = float( sys.argv[1] )
label_index = int( sys.argv[2] )
weight_index = int( sys.argv[3] )
pre_sample_rate = 1.0
neg_weight_factor = 1/ (sample_rate*pre_sample_rate)

for line in sys.stdin:
    line = line.strip('\n')
    data = line.split('\t')
    label = int( data[label_index])
    weight = float(data[weight_index])
    pv = 0
    clk = 0
    if label == 0:
        pv = 1
    else:
        clk = 1

    if clk > 0:
        label = 1
        pos_weight = weight
        print str(label) + '\t' + str(clk) + '\t' + str(pos_weight) + '\t' + line
        continue

    rand = random.random()
    if rand < sample_rate:
        neg_weight = weight
        print str(label) + '\t' + str(pv) + '\t' + str(weight*neg_weight_factor) + '\t' + line
    else:
        continue
