# -*- coding:utf-8 -*-
import os
import datetime
import time
import math,sys
import random

if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper
    # import json
else:
    # import com.xhaus.jyson.JysonCodec as json
    pass

import re
kwlen_regex = re.compile(' 64:([0-9]+) ')
wordcount_regex = re.compile(' 65:([0-9]+) ')

def get_feature_value(data_str, regex):
    if data_str is None or len(data_str) <=0:
        return None

    res = regex.search(data_str)
    if res is None:
        return None
    res_grp = res.groups()
    try:
        return float(res_grp[0])
    except:
        return None

@outputSchema('split:int')
def split_user_by_kwlen_473(data_str):
    kw_value = get_feature_value(data_str,kwlen_regex)

    if kw_value is None:
        return 0
    if kw_value <= 473:
        return 0
    else:
        return 1

@outputSchema('split:int')
def split_user_by_kwlen(data_str,threshold=-1):
    kw_value = get_feature_value(data_str,kwlen_regex)

    if kw_value is None:
        return 0
    if kw_value <= threshold:
        return 0
    else:
        return 1

@outputSchema('wordcount:int')
def get_word_count(data_str):
    wc = get_feature_value(data_str,wordcount_regex)
    if wc is None:
        wc = 0.0
    return int(wc)

if __name__ == '__main__':
    print split_user_by_kwlen_473('1 1:2 3:4 64:500 9:3')

