# -*- coding:utf-8 -*-
import os
import datetime
import time
import math,sys
import random

if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper
    import json
else:
    import com.xhaus.jyson.JysonCodec as json

@outputSchema('up_json:chararray')
def format_hindi_up(up_str):
    if up_str is None or len(up_str) <=0:
        return up_str
    try:
        up_map = json.loads(up_str)
        up_map['u_cat_len'] = up_map.get('u_cat_len_hindi',0)
        up_map['u_kw_len'] = up_map.get('u_kw_len_hindi',0)
        up_map['categories'] = up_map.get('categories_hindi','[]')
        up_map['keywords'] = up_map.get('keywords_hindi','[]')
        del(up_map['u_cat_len_hindi'])
        del(up_map['u_kw_len_hindi'])
        del(up_map['categories_hindi'])
        del(up_map['keywords_hindi'])
        return json.dumps(up_map)
    except:
        print "error formating up."
        return up_str


@outputSchema('up_json:chararray')
def format_category(up_str):
    try:
        up_map=json.loads(up_str)
        up_map['categories']=up_map.get('l2_categories',[])
        return json.dumps(up_map)
    except:
        print "error formating up."
        return up_str


@outputSchema('data:(id:chararray,updatetime:long)')
def gen_cp_id_uptime(cp_str):
    if cp_str is None or len(cp_str) <=0:
        return ('',0)
    try:
        cp_map = json.loads(cp_str)
        item_id = cp_map['item_id']
        updatetime = cp_map['update_time']
        return (item_id,updatetime)
    except:
        print "error parsing cp."
        return ('',0)

@outputSchema('data:(id:chararray,updatetime:long,type:chararray)')
def gen_cp_id_uptime_type(cp_str):
    if cp_str is None or len(cp_str) <=0:
        return ('',0)
    try:
        cp_map = json.loads(cp_str)
        item_id = cp_map['item_id']
        updatetime = cp_map['update_time']
        tp = cp_map.get('type','')
        return (item_id,updatetime,tp)
    except:
        print "error parsing cp."
        return ('',0,'')

@outputSchema('data:(id:chararray,u_cat_len:int,u_kw_len:int)')
def gen_up_id_catlen(up_str):
    if up_str is None or len(up_str) <=0:
        return ('',0)
    try:
        up_map = json.loads(up_str)
        item_id = up_map['uid']
        cat_len = up_map['u_cat_len']
        kw_len = up_map['u_kw_len']
        return (item_id,cat_len,kw_len)
    except:
        print "error parsing cp."
        print up_str
        return ('',0,0)

@outputSchema('data:(id:chararray,update_time:long,type:chararray,cat:{T:(cat_id:chararray,weight:double)},kw:{T:(keyword:chararray,weight:double)})')
def gen_cp_cat_kw(cp_str):
    if cp_str is None or len(cp_str) <=0:
        return ('',0)
    try:
        cp_map = json.loads(cp_str)
        item_id = cp_map['item_id']
        # language = cp_map['language']
        update_time = cp_map['update_time']
        tp = cp_map.get('type','')
        category = cp_map['categories']
        keywords = cp_map['entities']
        wt_name = 'weight'
        category = [(x['name'],x[wt_name]) for x in category if 'name' in x and wt_name in x]
        wt_name = 'L1_weight'
        keywords = [(x['name'],x[wt_name]) for x in keywords if 'name' in x and wt_name in x]
        return (item_id,update_time,tp,category,keywords)
    except:
        print "error parsing cp."
        print cp_str
        return ('',0,'',[],[])

@outputSchema('data:(id:chararray,cat:{T:(cat_id:chararray,weight:double)},kw:{T:(keyword:chararray,weight:double)})')
def gen_up_cat_kw(up_str):
    if up_str is None or len(up_str) <=0:
        return ('',0)
    try:
        up_map = json.loads(up_str)
        item_id = up_map['uid']
        category = up_map['categories']
        keywords = up_map['keywords']
        wt_name = 'weight'
        category = [(x['name'],x[wt_name]) for x in category if 'name' in x and wt_name in x]
        keywords = [(x['name'],x[wt_name]) for x in keywords if 'name' in x and wt_name in x]
        return (item_id,category,keywords)
    except:
        print "error parsing up."
        print up_str
        return ('',[],[])

@outputSchema('data:{T:(id:chararray,u_cat_len:int,u_kw_len:int)}')
def random_sample(input_bag,count):
    if input_bag is None or len(input_bag) <=0:
        return []
    try:
        result = random.sample(input_bag, count)
        return result
    except:
        print "error sampling cp."
        return ('',0,0)

@outputSchema('data:{T:(id:chararray,u_cat_len:int,u_kw_len:int)}')
def gen_json(uid, content_id,rid ,label,dwelltime,up,cp,gmp,ts,product_id):
    return '{"uid":"' + unicode(uid) + '","item_id":"'+unicode(content_id) +'","rid":"' + unicode(rid) + '","label":' + unicode(label) + ',"dwelltime":' + unicode(dwelltime) + ',"up":' + up + ',"cp":' + cp + ',"gmp":' + unicode(gmp) + ',"ts":' + unicode(ts) + ',"pid":"' + unicode(product_id) + '"}'

if __name__ == '__main__':
    print gen_up_cat_kw('{"aid:" : "7defce83f4a7", "uid" : "7defce83f4a7", "version" : "1", "join_flag" : 0, "gender" : "2", "age" : "2", "new_user" : "", "u_cat_len" : 0, "u_kw_len" : 0, "categories" : [], "keywords" : [], "u_cat_len_hindi" : 0, "u_kw_len_hindi" : 0, "categories_hindi" : ["aa"], "keywords_hindi" : [],"lasttime": ""}')
    print gen_cp_cat_kw('{"content_length": 3334,"type": "article", "image_md5": "adb62750", "videos": [], "word_count": 2960, "thirdads": "0", "is_servable": true, "regist_time": null, "has_copyright": 0, "images": ["http://img1.store.ksmobile.net/cminstanews/20160325/12/6442_adb62750_145890920327_200_140.jpg"], "head_image": "http://img1.store.ksmobile.net/cminstanews/20160325/12/17094_3421735e_145890920378_400_250_s.jpg", "editorial_item_level": 0, "cp_id": "", "fetch_time": 1458909203, "copyright": "", "author": "", "discovery_time": 1458909203, "entities": [{"L1_weight": 0.3039881116398135, "name": "111", "weight": 0.8096102202715608}, {"L1_weight": 0.14912200886653934, "name": "333", "weight": 0.3971559999320855}], "update_time": 1459250486, "publish_time": 1458908821, "large_image": "", "body_images": [], "channel_ids": [31, 43], "cp_version": 13, "editor_level": null, "source_type": 2, "link": "http://dhunt.in/12qB5", "display_type": 0, "item_id": "10328966", "source_feed": ["http://api-news.dailyhunt.in/api/v1/news/articles/latest/group/sanjetod/home"], "categories": [{"name": "1000111", "weight": 0.6588442659712693}], "publisher": "Sanjeevni Today", "feature_scores": {"newsy_score": 0.0, "adult_score": 0.0}, "language": "hi", "photos": [], "editorial_importance_level": 0, "region": "in", "title": "\u091a\u0947\u0939\u0930\u093e \u0927\u094b\u0924\u0947 \u0938\u092e\u092f \u0930\u0916\u0947\u0902 \u0907\u0928 \u0916\u093e\u0938 \u092c\u093e\u0924\u094b\u0902 \u0915\u093e \u0916\u094d\u092f\u093e\u0932", "last_modified_time": 1458909203, "summary": "", "cp_disable": true, "newsy_score": 0, "group_id": "823837", "image_count": 1}')

