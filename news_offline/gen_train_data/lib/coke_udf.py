import re
import datetime
import sys
import math

if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper
    # import json
# else:
#     import com.xhaus.jyson.JysonCodec as json

@outputSchema("y:chararray")
def timestamp2date(timestamp):
    date = datetime.date.fromtimestamp(timestamp)
    return  "%s"%date

@outputSchema("y:chararray")
def dump_feat_json(feats):
    ostr = None
    try:

        ostr = "["
        k = 0
        for type, name, val in feats:
            if k>0:
                ostr += ", "
            ostr += '{"type":"%s","name":"%s","value":%s}'%(type,name,val)
            k += 1
        ostr += "]"
        return ostr
    except:
        return None


@outputSchema("y:bag{t:tuple(type:chararray, name:chararray, val:double)}")
def aggregate_bags_of_feat_val(bags_of_feats):
    acc = {} # accumulated feat val
    for feats in bags_of_feats:
        if feats==None or feats[0]==None:
            continue
        try:
            for type, name, val in feats[0]:
                if val == None:
                    continue
                if acc.has_key((type,name)):
                    acc[(type,name)] += val
                else:
                    acc[(type,name)] = val
        except:
            #sys.stderr.write("%s\n"%feats)
            continue
    out = []
    for (type,name),val in acc.items():
        out.append((type,name,val))
    return out


@outputSchema("y:double")
def score_profile(profile, doc_feats, type_use):
    # both a and b should be bags of type,name,val tuples
    q_dict = {}
    if profile == None or len(profile)==0:
        return 0.0
    for type, name, val in profile:
        if val != None and re.match(type_use, type):
            q_dict[(type,name)] = val

    res = 0.0
    if doc_feats==None or len(doc_feats)==0:
        return 0.0
    for type, name, val in doc_feats:
        key = (type, name)
        if q_dict.has_key(key) and val  != None:
            res += val*q_dict[key]

    return res

@outputSchema("y:chararray")
def extract_les_uid(uf_str):
    if uf_str==None:
        return None
    for tkn in uf_str.split(','):
        if tkn.find('y:')==0:
            return tkn
    return None


@outputSchema("y:bag{t:tuple(pos:int, pkgt:int, item:chararray)}")
def parse_les_context(context_str):
    out = []
    try:
        if context_str==None or len(context_str)==0:
            return None
        parts = context_str.split('\001')
        for part in parts:
            tkns = part.split('\002')
            pos, item = tkns[0], tkns[1]
            m = re.search('\d+', pos)
            if m!=None:
                pos = int(m.group(0))
                pkgt = None
                if len(tkns)>2:
                    for att in tkns[2].split('\003'):
                         k,v = att.split('\004')
                         if k=="pkgt":
                             pkgt = int(v)

                out.append((pos, pkgt, item))
        return out
    except:
        return []


@outputSchema("y:chararray")
def get_att_gcontext(context_str, att):
    try:
        if context_str==None or len(context_str)==0:
            return None
        parts = context_str.split('\001')
        for part in parts:
            tkns = part.split('\002')
            k, v = tkns[0], tkns[1]
            if k==att:
                return v
        return None
    except:
        return None




@outputSchema("y:bag{t:tuple(time:long, pos:int, pkgt:int, rcode:chararray, item:chararray, action:chararray)}")
def match_clicks(events):
    if events==None:
        return []
    events_sorted = sorted(events, key = lambda event: event[0])
    strm_table = {}
    skips_above = {}
    skips_below = {}
    views = {} # map item views to view context
    clicks = {}
    out = []
    for time, pos, pkgt, item, action, rcode in events_sorted:
        ctx = (pos, pkgt, rcode, time)
        if action=="v":
            strm_table[pos] = (item, "v", pkgt, rcode, time)
            if views.has_key(item):
                if views[item].has_key(ctx):
                    views[item][ctx] += 1
                else:
                    views[item][ctx] = 1
            else:
                views[item] = {ctx: 1}

        elif action=="c":
            if strm_table.has_key(pos):
                item2, action2, pkgt2, rcode2, time2 = strm_table[pos]
            else:
                item2, action2, pkgt2, rcode2, time2 = None, None, None, None, None

            strm_table[pos] = (item, "c", pkgt, rcode, time)
            clicks[item] = ctx
            if skips_above.has_key(item):
                del skips_above[item]

            if skips_below.has_key(item):
                del skips_below[item]

            if views.has_key(item) and views[item].has_key(ctx):
                views[item][ctx] -= 1

            if item2!=item: # mismatched click
                out.append((time,pos,pkgt,rcode,item,action))
    return out

# bag of key value pairs to tuple, with specificed key order
@outputSchema("t:tuple()")
def kv_bag2tuple(kvs, *key_order):
    kvs_map = {}
    for k,v in kvs:
        kvs_map[k] = v
    out = []
    for k in key_order:
        if kvs_map.has_key(k):
            out.append(float(kvs_map[k]))
        else:
            out.append(0.0)
    return tuple(out)

@outputSchema("y:bag{t:tuple(time:long, pos:int, pkgt:chararray, rcode:chararray, item:chararray, action:chararray)}")
def interpret_stream_action(events):
    if events==None:
        return []
    events_sorted = sorted(events, key = lambda event: event[0])
    strm_table = {}
    skips_above = {}
    skips_below = {}
    views = {} # map item views to view context
    clicks = {}

    for time, pos, pkgt, item, action, rcode in events_sorted:
        pos = str(pos)
        m = re.search('\d+', pos)
        if m!=None:
            pos = int(m.group(0))
        else:
            continue

        ctx = (pos, pkgt, rcode, time)

        if action=="v":
            strm_table[pos] = (item, "v", pkgt, rcode, time)
            if views.has_key(item):
                if views[item].has_key(ctx):
                    views[item][ctx] += 1
                else:
                    views[item][ctx] = 1
            else:
                views[item] = {ctx: 1}

        elif action=="c":
            if strm_table.has_key(pos):
                item2, action2, pkgt2, rcode2, time2 = strm_table[pos]
            else:
                item2, action2, pkgt2, rcode2, time2 = None, None, None, None, None

            strm_table[pos] = (item, "c", pkgt, rcode, time)
            clicks[item] = ctx
            if skips_above.has_key(item):
                del skips_above[item]

            if skips_below.has_key(item):
                del skips_below[item]

            if views.has_key(item) and views[item].has_key(ctx):
                views[item][ctx] -= 1

            if item2!=item: # mismatched click
                continue

            # skips above
            i = pos - 1
            while i > 0:
                if strm_table.has_key(i):
                    item, state, pkgt, rcode, time = strm_table[i]
                    if state == "v": # detect a skip
                        state = "s"
                        strm_table[i] = (item, state, pkgt, rcode, time)
                        ctx = (i, pkgt, rcode, time)
                        d = pos - i
                        skips_above[item] = ctx, d
                i -= 1

            # skips below
            i = pos + 1
            while i < pos+4:
                if strm_table.has_key(i):
                    item, state, pkgt, rcode, time = strm_table[i]
                    if state == "v": # detect a skip
                        state = "s"
                        strm_table[i] = (item, state, pkgt, rcode, time)
                        ctx = (i, pkgt, rcode, time)
                        d = i - pos
                        skips_below[item] = ctx, d
                i += 1

    out = []
    for item, (ctx, d) in skips_above.items():
        pos, pkgt, rcode, time = ctx
        '''
        if d <= 3:
            d = d
        elif d <= 10:
            d = 10
        elif d <= 20:
            d = 20
        else:
            d = 100
        '''
        out.append((time, pos, pkgt, rcode, item, "s-%d"%d))

    for item, (ctx, d) in skips_below.items():
        pos, pkgt, rcode, time = ctx
        out.append((time, pos, pkgt, rcode, item, "s+%d"%d))

    for item, ctx in clicks.items():
        pos, pkgt, rcode, time = ctx
        out.append((time, pos, pkgt, rcode, item, "c"))

#    for item, ctx_cnts in views.items():
#        for ctx, cnt in ctx_cnts.items():
#            pos, pkgt, rcode, time = ctx
#            for i in xrange(cnt):
#                out.append((time, pos, pkgt, rcode, item, "v"))
    return out


@outputSchema("a:bag{t:tuple(date:chararray,pos:int, pkgt:int, item:chararray, action:chararray)}")
def resolve_actions(item_action_bag):
    resolved_actions = {}
    for date, pos, pkgt, item, action in item_action_bag:
        if not resolved_actions.has_key(item) or action == 'c':
            resolved_actions[item] = (date, action, pos, pkgt)
        elif resolved_actions[item] == 'v':
            resolved_ations[item] = (date, action, pos, pkgt)
    out = []
    for item, (date, action, pos, pkgt) in resolved_actions.items():
        out.append((date, pos, pkgt, item, action))
    return out

@outputSchema("pos:int, neg:int")
def pos_neg(act_score_bag):
    k0 = 0
    k1 = 0
    for act, val in act_score_bag:
        if act=='c':
            k1 += 1
        else:
            k0 += 1
    return k0,k1


@outputSchema("y:double")
def core_compute(gmp, agg, facebook, inferred, positive, negative, ed_promote, ed_demote):
    return negative * ed_demote * ( math.pow(math.pow(gmp,4)+math.pow(agg,4)+math.pow(ed_promote,4),0.25) + math.pow(math.pow(inferred,4)+math.pow(facebook,4)+math.pow(positive,4),0.25) )


@outputSchema("y:double")
def metric_mrr(act_score_bag):
    act_score_sorted = sorted(act_score_bag, key=lambda pair:pair[1], reverse=True)
    mrr = 0.0
    i = 1
    k = 0
    for act, val in act_score_sorted:
        if act=='c':
            mrr += 1.0/i
            k += 1
        i += 1
    if k>0:
        mrr /= k
    return mrr


@outputSchema("y:double")
def metric_wmrr(act_score_bag):
    act_score_sorted = sorted(act_score_bag, key=lambda pair:pair[1], reverse=True)
    mrr = 0.0
    i = 1
    k = 0
    for act, val, weight in act_score_sorted:
        if act=='c':
            mrr += weight/i
            k += weight
        i += 1
    if k>0:
        mrr /= k
    return mrr


@outputSchema("y:double")
def metric_map(act_score_bag):
    act_score_sorted = sorted(act_score_bag, key=lambda pair:pair[1], reverse=True)
    map = 0.0
    k0 = 0
    k1 = 0
    for act, val in act_score_sorted:
        if act=='c':
            k1 += 1
            map += float(k1)/float(k0+k1)
        else:
            k0 += 1
    if k1>0:
        map /= k1
    return map

@outputSchema("y:double")
def metric_wmap(act_score_bag):
    act_score_sorted = sorted(act_score_bag, key=lambda pair:pair[1], reverse=True)
    map = 0.0
    k0 = 0
    k1 = 0
    for act, val, weight in act_score_sorted:
        if act=='c':
            k1 += weight
            map += float(k1)/float(k0+k1)
        else:
            k0 += weight
    if k1>0:
        map /= k1
    return map


@outputSchema("y:double")
def metric_recency(age_score_bag):
    age_score_sorted = sorted(age_score_bag, key=lambda pair:pair[1], reverse=True)
    avg_age = 0.0
    cnt = 0
    for age, val in age_score_sorted:
        if cnt==3:
            break
        cnt += 1
        avg_age += float(age)
    avg_age /= cnt
    recency = 1.0/avg_age
    return recency

@outputSchema("y:double")
def f_standard_dcg(label_score):
  val = 0.0
  i = 0
  #print "one sort:"
  for label, score in label_score:
  #  print label, score
    i += 1
    y = float(label) + 0.0
    if y <= 0:
      continue
    else:
      val += y/math.log(i+1)
  return val

@outputSchema("y:double")
def metric_standard_ndcg(act_score_bag):
  act_score_sorted_by_score = sorted(act_score_bag, key=lambda pair:pair[1], reverse=True)
  act_score_sorted_by_act = sorted(act_score_bag, key=lambda pair:pair[0], reverse=True)
  idcg = f_standard_dcg(act_score_sorted_by_act) # ideal dcg
  dcg = f_standard_dcg(act_score_sorted_by_score) # achieved dcg
  if idcg>0:
    return dcg/idcg
  else:
    return 0.0


@outputSchema("y:double")
def metric_standard_ndcgk(act_score_bag, K):
  act_score_sorted_by_score = sorted(act_score_bag, key=lambda pair:pair[1], reverse=True)
  act_score_sorted_by_act = sorted(act_score_bag, key=lambda pair:pair[0], reverse=True)
  idcg = f_standard_dcg(act_score_sorted_by_act[:K]) # ideal dcg
  dcg = f_standard_dcg(act_score_sorted_by_score[:K]) # achieved dcg
  if idcg>0:
    return dcg/idcg
  else:
    return 0.0


@outputSchema("y:double")
def metric_mean_label(label_score):
  val = 0.0
  i = 0
  for label, score in label_score:
    i += 1
    val += float(label)
  if i == 0:
    return 0.0
  val /= float(i)
  return val

@outputSchema("y:double")
def metric_mean_labelk(act_score_bag, K):
  act_score_sorted = sorted(act_score_bag, key=lambda pair:pair[1], reverse=True)
  val = metric_mean_label(act_score_sorted[:K])
  return val


@outputSchema("y:double")
def metric_auc(act_score_bag):
    act_score_sorted = sorted(act_score_bag, key=lambda pair:pair[1], reverse=True)
    auc = 0.0
    k0 = 0
    k1 = 0
    tp0 = 0.0
    fp0 = 0.0
    tp1 = 1.0
    fp1 = 1.0
    P = 0
    N = 0
    for act, val in act_score_sorted:
        if act=='c':
            P += 1.0
        else:
            N += 1.0
    if P==0:
        return 0.0
    if N==0:
        return 1.0

    for act, val in act_score_sorted:
        if act=='c':
            k1 += 1
            tp1 = float(k1)/P
            fp1 = float(k0)/N
            auc += (fp1-fp0)*(tp1+tp0)/2
            tp0 = tp1
            fp0 = fp1
        else:
            k0 += 1
    print fp1
    print k0,k1,N,P
    auc += 1.0 - fp1
    return auc


@outputSchema("y:double")
def metric_wauc(act_score_bag):
  act_score_sorted = sorted(act_score_bag, key=lambda pair:pair[1], reverse=True)
  auc = 0.0
  k0 = 0
  k1 = 0
  tp0 = 0.0
  fp0 = 0.0
  tp1 = 1.0
  fp1 = 1.0
  P = 0
  N = 0
  for act, val, weight in act_score_sorted:
    if(act=='c' ):
      P += weight
    else:
      N += weight
  if P == 0:
    return 0.0
  if N == 0:
    return 1.0

  for act, val, weight in act_score_sorted:
    if (act=='c'):
      k1 += weight
      tp1 = float(k1)/P
      fp1 = float(k0)/N
      auc += (fp1-fp0)*(tp1+tp0)/2
      tp0 = tp1
      fp0 = fp1
    else:
      k0 += weight
  auc += 1.0 - fp1
  return auc

@outputSchema("t:tuple(entropy:double, gini:double, herfindahl:double)")
def diversity_metric(rec_cnt, user_cnt, item_cnt):

    prob = float(rec_cnt)/float(user_cnt)
    entropy = - prob * math.log(prob)
    gini = 0.0
    herfindahl = prob**2

    return (entropy, gini, herfindahl)

if __name__ == '__main__':
    data = ((0.5951433821234429,0.0,1.0), (0.40593956599836234,0.0,1.0), (0.4641271054858744,0.0,1.0), (0.37371728142479027,0.0,1.0), (0.478557449761314,0.0,1.0), (0.26856617083375517,0.0,1.0), (0.40724287021254063,0.0,1.0), (0.5183867552227289,0.0,1.0), (0.6461673018720813,0.0,1.0), (0.22966912171058015,0.0,1.0), (0.5048613455459867,0.0,1.0), (0.44997741402775976,1.0,1.0), (0.3226713350922976,0.0,1.0), (0.2345328507262902,0.0,1.0), (0.3543991229015133,0.0,1.0), (0.2544677587390481,0.0,1.0), (0.40808420372746773,0.0,1.0), (0.21732178021906348,0.0,1.0), (0.561552563804558,1.0,1.0), (0.22530832585568752,0.0,1.0), (0.16530707421254254,0.0,1.0), (0.24895923061030625,0.0,1.0), (0.20371385299557537,0.0,1.0), (0.349144212092727,1.0,1.0), (0.6798418272552947,1.0,1.0), (0.43402410082404075,0.0,1.0), (0.2554007553583082,0.0,1.0), (0.50284723915609,0.0,1.0), (0.24222643931365068,0.0,1.0), (0.4247695481636642,0.0,1.0), (0.4772373312621194,1.0,1.0), (0.5054689709385701,0.0,1.0), (0.3782080160218219,0.0,1.0), (0.453347238936561,0.0,1.0), (0.3960859956891152,0.0,1.0), (0.5738176400153179,0.0,1.0), (0.6270249051744763,0.0,1.0), (0.37938194720996854,0.0,1.0), (0.6856236979975623,0.0,1.0), (0.289233502097774,0.0,1.0), (0.38639497098444253,0.0,1.0), (0.35161266301264477,0.0,1.0), (0.4513526067395909,0.0,1.0), (0.502917540113657,0.0,1.0), (0.39943581763822067,0.0,1.0), (0.2389705581482398,0.0,1.0), (0.3019721165111694,0.0,1.0), (0.4250223885359039,0.0,1.0), (0.22380749418274307,0.0,1.0), (0.3391428400344623,0.0,1.0), (0.42454851802792265,0.0,1.0), (0.5346797438468472,0.0,1.0), (0.15581035229297402,0.0,1.0), (0.467218684692011,0.0,1.0), (0.4222925233024303,0.0,1.0), (0.3265329318147416,0.0,1.0), (0.3342987302339189,0.0,1.0), (0.38578461513846973,0.0,1.0), (0.631968876874954,0.0,1.0), (0.42338493448451386,0.0,1.0), (0.2229461261517307,0.0,1.0), (0.3438945617994457,0.0,1.0), (0.5656470411413813,0.0,1.0), (0.389300195324054,0.0,1.0), (0.3151709422595057,0.0,1.0), (0.47817252540107014,0.0,1.0), (0.4932880673818451,0.0,1.0), (0.3764736240263899,0.0,1.0), (0.24154244879886683,0.0,1.0), (0.3889746239434854,0.0,1.0), (0.5528495210444093,0.0,1.0), (0.5652114405431293,0.0,1.0), (0.4668463350351674,0.0,1.0), (0.3309306181308396,0.0,1.0), (0.716004390524754,0.0,1.0), (0.5328419899283864,0.0,1.0), (0.37375488568618126,0.0,1.0), (0.24956367776278263,0.0,1.0), (0.33772533456265064,0.0,1.0), (0.3931552142622769,0.0,1.0), (0.5807304443341219,0.0,1.0), (0.5087644658770508,0.0,1.0), (0.42221110921957283,0.0,1.0), (0.3511183433050847,0.0,1.0), (0.45269725491598023,0.0,1.0), (0.4284111024122022,0.0,1.0), (0.2498475630056127,0.0,1.0), (0.3973511598240747,0.0,1.0), (0.4237363285321091,0.0,1.0), (0.2263688589651272,0.0,1.0), (0.6382855573871606,0.0,1.0), (0.5645454889702061,0.0,1.0), (0.23740129817126546,0.0,1.0), (0.4343828237323799,0.0,1.0), (0.5512638128684384,0.0,1.0), (0.5471129724342222,0.0,1.0), (0.6124700352891466,0.0,1.0), (0.2734648768282414,0.0,1.0), (0.36608755985122177,0.0,1.0), (0.3354330742157416,0.0,1.0), (0.3913881683904404,0.0,1.0), (0.51614288296401,0.0,1.0), (0.36566243351531436,0.0,1.0), (0.4330848043032984,0.0,1.0), (0.3658959002272598,0.0,1.0), (0.2645269492397296,0.0,1.0), (0.3116006192095115,0.0,1.0), (0.413552947460907,0.0,1.0), (0.3929324115674919,0.0,1.0), (0.34413850907755433,0.0,1.0), (0.5195928162271104,0.0,1.0), (0.3423984416807153,0.0,1.0), (0.40430931853558105,0.0,1.0), (0.21388732071538757,0.0,1.0), (0.3267506414702086,0.0,1.0), (0.1089715206327554,0.0,1.0), (0.20852602885861513,0.0,1.0), (0.5298601231131689,0.0,1.0), (0.3537667576829878,0.0,1.0), (0.27241265680402205,0.0,1.0), (0.6011312670127943,1.0,1.0), (0.444226572208048,0.0,1.0), (0.4271581552304093,0.0,1.0), (0.4814938049859579,0.0,1.0), (0.2576405822766119,0.0,1.0))
    def change (x):
        if x > 0:
            return 'c'
        else:
            return 'v'
    data = [(change(x[1]),x[0]) for x in data]
    print data
    print metric_auc(data)
