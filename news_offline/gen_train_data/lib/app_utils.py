app_factor = {}

def outputSchema(dont_care):
    def wrapper(func):
        def inner(*args, **kwargs):
            return func(*args, **kwargs)
        return inner
    return wrapper

def get_app_factor():
    global app_factor
    if len(app_factor) == 0:
        f = open('app_factor.param')
        for line in f:
            fid,fname,weight = line.split('\t')
            if fname.find('-') < 0:
                continue
            fields = fname.rstrip().split('-')
            if len(fields) == 3:
                prefix,app_cat,dcat = fname.rstrip().split('-')

                if dcat not in app_factor:
                    app_factor[dcat] = {app_cat:float(weight)}
                else:
                    if app_cat not in app_factor[dcat]:
                        app_factor[dcat][app_cat] = float(weight)
            else:
                continue

    return app_factor

@outputSchema("b:bag{t:tuple(value:float)}")
def tuple2bag(data):
    return list(data)

@outputSchema("b:bag{t:tuple(category:chararray, weight:float)}")
def transform(data):
    result = []
    app_factor = get_app_factor()
    if app_factor == None or len(app_factor) == 0:
        return None

    #print app_factor

    for dcat in app_factor:
        sum_score = 0
        factor = app_factor[dcat]
        for app_cat, weight in data:
            sum_score += weight*factor.get(app_cat,0.0)

        if sum_score > 0:
            result.append((dcat,sum_score))

    return result


if __name__ == '__main__':
    #data = [('entertainment',0.21428571428571427),('transportation',0.14285714285714285),('lifestyle',0.14285714285714285),('tools',0.07142857142857142),('productivity',0.07142857142857142)]
    data = [('tools',0.38461538461538464),('game_sports',0.07692307692307693),('photography',0.07692307692307693),('communication',0.07692307692307693)]

    print data
    print transform(data)





