def get_feature_lists():
    #load feature map file
    if get_feature_lists._global_feature_map is None:
        feature_map_file = 'feature.map'
        f3 = open(feature_map_file, 'r')
        feature_map = {}
        feature_list = []
        for line in f3:
            fid, fname, ftype = line.strip('\n').split('\t')
            feature_map[fname] = fid
            feature_list.append(fname)
        get_feature_lists._global_feature_map = feature_map
        get_feature_lists._global_feature_list = feature_list
        f3.close()
    return get_feature_lists._global_feature_list, get_feature_lists._global_feature_map

get_feature_lists._global_feature_map = None
get_feature_lists._global_feature_list = None

import sys
if __name__ == '__main__':
    get_feature_list, feature_map = get_feature_lists()
    print '\t'.join(['label'] + get_feature_list)

    for line in sys.stdin:
	print line
