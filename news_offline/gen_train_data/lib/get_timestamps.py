
import sys
from datetime import datetime, timedelta

if __name__ == '__main__':
    if len(sys.argv) < 3:
	exit(-1);

    start_date = sys.argv[1]
    end_date = sys.argv[2]
  
    sdt = datetime.strptime(start_date, '%Y%m%d')
    edt = datetime.strptime(end_date, '%Y%m%d')
    edt = edt + timedelta(days=1)

    start_ts = (sdt - datetime(1970, 1, 1)).days*86400
    end_ts = (edt - datetime(1970, 1, 1)).days*86400

    print start_ts, end_ts

    print ','.join(str(x) for x in range(start_ts,end_ts,600))

	

