#!/usr/bin/env python
#coding=utf-8

import sys
import math
import datetime
import time


if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper
    import json
else:
    import com.xhaus.jyson.JysonCodec as json


# #load feature list file
# feature_list_file = 'cfb_feature.list'
# f1 = open(feature_list_file, 'r')
# feature_str = f1.readline()
# features = feature_str.strip('\n').split('\t')
# f1.close()

# #load noncfb feature list
# noncfb_feature_list_file = 'noncfb_feature.list'
# f2 = open(noncfb_feature_list_file, 'r')
# noncfb_feature_str = f2.readline()
# noncfb_features = noncfb_feature_str.strip('\n').split('\t')
# f2.close()

def get_config_json():
    if get_config_json.config_json is None:
        try:
            f = open('config.json')
            data = f.read()
            get_config_json.config_json = json.loads(data)
            f.close()
        except:
            print "Error loading config file config.json"
            get_config_json.config_json = {}
    return get_config_json.config_json

get_config_json.config_json = None

def get_exclude_feature_type():
    config_json = get_config_json()
    return set(config_json.get('exclude_feature_type',[]))

@outputSchema("rel:float")
def inner_product_bag(bag1, bag2, idx_name, idx_score):
    if bag1 is None or len(bag1) <= 0 or bag2 is None or len(bag2) <= 0:
        return 0

    bag1_dict = {}
    for entity in bag1:
        feature_name = entity[idx_name]
        feature_score = float(entity[idx_score])
        if feature_name not in bag1_dict:
            bag1_dict[feature_name] = feature_score

    final_score = 0.0
    for entity in bag2:
        feature_name = entity[idx_name]
        feature_score = float(entity[idx_score])
        if feature_name in bag1_dict:
            final_score += bag1_dict[feature_name] * feature_score

    return final_score

def get_feature_lists():
    #load feature map file
    if get_feature_lists._global_feature_map is None:
        feature_map_file = 'feature.map'
        f3 = open(feature_map_file, 'r')
        feature_map = {}
        feature_list = []
        for line in f3:
            line = line.rstrip('\n')
            if line == '':
                continue
            fid, fname, ftype = line.split('\t')
            if fid.startswith('#'):
                fid = fid.replace('#','').strip()
            feature_map[fname] = fid
            feature_list.append(fname)
        get_feature_lists._global_feature_map = feature_map
        get_feature_lists._global_feature_list = feature_list
        f3.close()
    return get_feature_lists._global_feature_list, get_feature_lists._global_feature_map

get_feature_lists._global_feature_map = None
get_feature_lists._global_feature_list = None

def get_feature_map_inverse():
    if get_feature_map_inverse._global_feature_map_inverse is None:
        feature_list ,feature_map = get_feature_lists()
        inv_map = {}
        for k,v in feature_map.items():
            inv_map[v] = k
        # inv_map = {v:k for k,v in feature_map.items()}
        get_feature_map_inverse._global_feature_map_inverse = inv_map
    return get_feature_map_inverse._global_feature_map_inverse

get_feature_map_inverse._global_feature_map_inverse=None


@outputSchema("record:chararray")
def parse_id_feature(feature_name):
    fid = feature_map[str(feature_name)]
    return str(fid) + ':1'

@outputSchema("record:chararray")
def gen_fid_fvalue(fvalue, fname):
    fid = feature_map[str(fname)]
    if fid is None:
        #print >> sys.stderr, 'unkown fname: '+fname
        sys.exit(-1)

    if fvalue is None:
        return str(fid) + ':missing'
    return str(fid) + ':' + str(fvalue)

@outputSchema("record:[]")
def parse_and_get_gmp(cfb_json_input,gmp_thres=100):
    feature_dict = {}
    if cfb_json_input is None:
        return feature_dict
    cfb_features = json.loads(cfb_json_input)
    for key in cfb_features.keys():
        if 'd_conid' in key.lower():
            values = cfb_features[key]
            clk = values[0]
            pv = values[1]
            gmp = None
            if clk is not None and pv is not None and pv >= gmp_thres:
                gmp = float(clk)/float(pv)
                fname = key + '_' + 'COEC'
                feature_dict[fname] = gmp
            break;

    return feature_dict

@outputSchema("record:[]")
def parse_and_calc_cfb_feature(cfb_json_input):
    feature_dict = {}
    if cfb_json_input is None:
        return feature_dict

    try:
        cfb_features = json.loads(cfb_json_input)
    except:
        #print cfb_json_input
        return feature_dict
    for key in cfb_features.keys():
        if "keyword" in key.lower() or "category" in key.lower():
            kw_cat_key_values = cfb_features[key]
            if (len(kw_cat_key_values) == 0) or (kw_cat_key_values is None):
                continue

            mean_pv = 0
            mean_clk = 0
            max_pv = 0
            max_clk = 0
            macro_mean_coec = 0
            micro_max_coec = 0
            macro_max_coec = 0
            micro_mean_coec = 0

            for kw_cat_key in kw_cat_key_values.keys():
                kw_cat_values = kw_cat_key_values[kw_cat_key]
                pv = None
                clk = None
                weight = None
                if  len(kw_cat_values) < 3:
                    continue
                if kw_cat_values[0] is not None:
                    clk = float(kw_cat_values[0])
                if kw_cat_values[1] is not None:
                    pv = float(kw_cat_values[1])
                if kw_cat_values[2] is not None:
                    weight = float(kw_cat_values[2])
                if pv is None or clk is None or weight is None:
                    continue

                weighted_pv = weight * pv
                weighted_clk = weight * clk
                coec = 0.0
                if pv > 1e-6:
                    coec = clk / pv

                weighted_coec = weight * coec

                mean_pv += weighted_pv
                mean_clk += weighted_clk
                max_pv = max(weighted_pv, max_pv)
                max_clk = max(weighted_clk, max_clk)
                macro_mean_coec += weighted_coec
                micro_max_coec = max(coec, micro_max_coec)
                macro_max_coec = max(weighted_coec, macro_max_coec)

            if mean_pv > 1e-6:
                micro_mean_coec = mean_clk / mean_pv

            feature_dict[key+'_MEAN_EC'] = mean_pv
            feature_dict[key+'_MEAN_C'] = mean_clk
            feature_dict[key+'_MAX_EC'] = max_pv
            feature_dict[key+'_MAX_C'] = max_clk
            feature_dict[key+'_MICRO_MEAN_COEC'] = micro_mean_coec
            feature_dict[key+'_MICRO_MAX_COEC'] = micro_max_coec
            feature_dict[key+'_MACRO_MEAN_COEC'] = macro_mean_coec
            feature_dict[key+'_MACRO_MAX_COEC'] = macro_max_coec

        else:
            values = cfb_features[key]
            clk = values[0]
            pv = values[1]
            fname = key + '_' + 'EC'
            feature_dict[fname] = pv
            fname = key + '_' + 'C'
            feature_dict[fname] = clk
            coec = None
            if clk is not None and pv is not None and pv > 1e-6:
                coec = float(clk)/float(pv)
            fname = key + '_' + 'COEC'
            feature_dict[fname] = coec

    return feature_dict

@outputSchema("features:chararray")
def format_train_data_tsv(label, static_features, cfb_features):
    out_put = []
    #out_put.append(str(label))
    #print static_features, cfb_features
    if cfb_features is None:
        cfb_features = {}
    if static_features is None:
        static_features = {}

    feature_list, feature_map = get_feature_lists()
    for feature_name in feature_list:
        if feature_name in cfb_features:
            fvalue = cfb_features[feature_name]
        elif feature_name in static_features:
            fvalue = static_features[feature_name]
        else:
            #out_put.append('0')
        #continue
            fvalue = None

        if fvalue is None or ( type(fvalue) == type("") and fvalue==""):
            out_put.append('0')
    else:
            fid = feature_map[feature_name]
            #out_put.append(feature_name + ':' + str(fvalue))
            out_put.append(str(fvalue))

    return '\t'.join(out_put)


@outputSchema("features:chararray")
def format_train_data(label, static_features, cfb_features):
    out_put = []
    out_put.append(str(label))
    # print static_features, cfb_features
    if cfb_features is None:
        cfb_features = {}
    if static_features is None:
        static_features = {}

    feature_list, feature_map = get_feature_lists()
    for feature_name in feature_list:
        if feature_name in cfb_features:
            fvalue = cfb_features[feature_name]
        elif feature_name in static_features:
            fvalue = static_features[feature_name]
        else:
            continue

        if fvalue is None or ( type(fvalue) == type("") and fvalue==""):
            continue
        fid = feature_map[feature_name]
        out_put.append(fid + (":%.10f" % float(fvalue)).rstrip('0').rstrip('.'))

    return ' '.join(out_put)

def modifiy_fields(fdata):
    if fdata.find('e'):
        kv = fdata.split(':')
        if len(kv) < 2:
            # return fdata
            ''
        else:
            try:
                result= kv[0]+ (":%.10f" % float(kv[1])).rstrip('0').rstrip('.')
                return ''
            except:
                return fdata

@outputSchema("features:chararray")
def remove_libsvm_scientific_float(data):
    fields = [ modifiy_fields(_k) for _k in data.split(' ') ]
    return ' '.join(fields)

@outputSchema("features:map[]")
def parse_libsvm(data):
    if data is None:
        return {}
    feature_map_inv = get_feature_map_inverse()

    result = {}
    fields = data.split(' ')
    for idx in range(1,len(fields)):
        item = fields[idx]
        k,v = item.split(':',1)
        v = float(v)
        result[feature_map_inv[k]] = v

    return result

@outputSchema("fea_names:{(fname:chararray)}")
def gen_train_featmap(static_features, cfb_features):
    output=[]
    if cfb_features is not None:
        for k in cfb_features.keys():
            output.append((k,))
    if static_features is not None:
        for k in static_features.keys():
            output.append((k,))
    return output

@outputSchema("featmap:{(fid:int, fname:chararray, ftype:chararray)}")
def format_featmap(features):
    fid = 1
    (feature_list, feature_map ) = get_feature_lists()
    # print feature_list
    feature_map_local = {}
    max_id = 0
    result=[]
    if len(feature_map) > 0:
        for k in feature_map:
            idx = int(feature_map[k])
            feature_map_local[idx] = k
            if idx > max_id:
                max_id = idx
        result = [(_k,feature_map_local[_k],'q') for _k in sorted(feature_map_local.keys())]
        cur_idx = max_id + 1
    else:
        cur_idx = 0
    for item in features:
        # print item
        fname = item[0]
        if fname in feature_map:
            continue
        if fname is None or fname.strip() == '':
            continue
        result.append((cur_idx,fname,'q'))
        cur_idx += 1

    return result

@outputSchema("data_map:[]")
def to_map(data_tuple):
    # print data_tuple
    result = {}
    if data_tuple is None:
        return result
    i=0
    while i+1 < len(data_tuple):
        d=data_tuple[i+1]
        if d is not None:
            result[data_tuple[i]] = str(d)
        i += 2
    # print result
    return result

def add_field(data_dict,name,value,ignore_zero=False):
    if value is None or value == '':
        return
    if ignore_zero and abs(float(value)) < 1e-6:
        return

    exclude_feature_sets = get_exclude_feature_type()
    if name in exclude_feature_sets:
        return

    data_dict[name] = value

def add_field_single_bag(data_dict, prefix_name, bag1, fname1='weight'):
    if bag1 is not None and len(bag1) > 0:
        bag_dict = {}
        for entity in bag1:
            try:
                feature_name = entity[0]['name']
                feature_score = float(entity[0][fname1])
                if feature_name is not None and feature_name != '' and feature_name not in bag_dict:
                    bag_dict[feature_name] = feature_score
                    name = prefix_name + feature_name
                    add_field(data_dict,name,feature_score,ignore_zero=False)
            except:
                continue



def add_field_dnn(data_dict, prefix_name, u_cat_bag, c_cat_bag, u_kw_bag, c_kw_bag, t_kw_bag, fname1='weight', truncate=(50,50,50,50,50)):
    u_cat_bag = u_cat_bag[:truncate[0]]
    c_cat_bag = c_cat_bag[:truncate[1]]
    u_kw_bag = u_kw_bag[:truncate[2]]
    c_kw_bag = c_kw_bag[:truncate[3]]
    t_kw_bag = t_kw_bag[:truncate[4]]
    fea_name = prefix_name + "U_CAT_"
    add_field_single_bag(data_dict,fea_name,u_cat_bag,fname1)
    fea_name = prefix_name + "C_CAT_"
    add_field_single_bag(data_dict,fea_name,c_cat_bag,fname1)
    fea_name = prefix_name + "U_KW_"
    add_field_single_bag(data_dict,fea_name,u_kw_bag,fname1)
    fea_name = prefix_name + "C_KW_"
    add_field_single_bag(data_dict,fea_name,c_kw_bag,fname1)
    fea_name = prefix_name + "T_KW_"
    add_field_single_bag(data_dict,fea_name,t_kw_bag,fname1)



def inner_product(bag1, bag2, fname1='weight', fname2='weight'):
    idx_name=0
    idx_score=1
    # print bag1
    # print bag2

    if bag1 is None or len(bag1) <= 0 or bag2 is None or len(bag2) <= 0:
        return 0

    bag1_dict = {}
    for entity in bag1:
        try:
            feature_name = entity[0]['name']
        except:
            continue
        feature_score = float(entity[0][fname1])
        if feature_name not in bag1_dict:
            bag1_dict[feature_name] = feature_score

    final_score = 0.0
    for entity in bag2:
        try:
            feature_name = entity[0]['name']
        except:
            continue
        feature_score = float(entity[0][fname2])
        if feature_name in bag1_dict:
            final_score += bag1_dict[feature_name] * feature_score

    return final_score


@outputSchema("data:[]")
def gen_phase1_features(u_raw, d_raw, static_features, truncate=None):
    if static_features is None:
        result = {}
    else:
        result = static_features

    try:
        up_map = json.loads(u_raw)
    except:
        up_map = {}

    try:
        cp_map = json.loads(d_raw)
    except:
        cp_map = {}

    del_list=[]
    for k in result:
        if result.get(k,'') is None or result.get(k,'') == '':
            del_list.append(k)

    for k in del_list:
        if k in result:
            del result[k]

    # calculate len of categories and keywords
    u_categories = up_map.get('categories',[])
    u_keywords = up_map.get('keywords',[])
    c_categories = cp_map.get('categories',[])
    c_keywords = cp_map.get('entities',[])
    t_keywords = cp_map.get('title_entities',[])
    add_field(result,'U_CAT_LEN', up_map.get('u_cat_len',0),ignore_zero=True)
    add_field(result,'U_KW_LEN', up_map.get('u_kw_len',0),ignore_zero=True)

    # image_count

    #word count

    # truncate cp, up categories and keywords
    if truncate is not None:
        u_categories = u_categories[:truncate[0]]
        u_keywords = u_keywords[:truncate[1]]
        c_categories = c_categories[:truncate[2]]
        c_keywords = c_keywords[:truncate[3]]


    try:
        add_field(result,'CAT_REL',inner_product(u_categories,c_categories,fname2='weight'),ignore_zero=True)
        add_field(result,'KW_REL',inner_product(u_keywords,c_keywords,fname2='L1_weight'),ignore_zero=True)
        add_field(result,'TITLE_KW_REL',inner_product(u_keywords,t_keywords,fname2='L1_weight'),ignore_zero=True)
    except:
        pass

    return result

def normalize_category(category):
    sum_norm=0
    for item in category:
        try:
            sum_norm += float(item[0]['weight'])
        except:
            print item

    if sum_norm == 0:
        return category
    for item in category:
        try:
            item[0]['weight'] = float(item[0]['weight'])/sum_norm
        except:
            print item
    return category

@outputSchema("data:[]")
def gen_app_rel(app_inferred_profile,d_raw):
    result = {}

    try:
        cp_map = json.loads(d_raw)
    except:
        cp_map = {}
        add_field(result,'APP_CAT_REL',0.0)
        return result

    if app_inferred_profile == None:
        add_field(result,'APP_CAT_REL',0.0)
        return result

    c_categories = cp_map.get('categories',[])
    c_categories = normalize_category(c_categories)

    u_categories = []
    for name,weight in app_inferred_profile:
        u_categories.append([{'name':name, 'weight':weight}])

    add_field(result,'APP_CAT_REL',inner_product(u_categories,c_categories,fname2='weight'))

    return result

@outputSchema("data:[]")
def gen_static_features_dnn(u_raw, d_raw, static_features, truncate=(200,200,200,200)):
    if static_features is None:
        result = {}
    else:
        result = static_features

    try:
        up_map = json.loads(u_raw)
    except:
        up_map = {}

    try:
        cp_map = json.loads(d_raw)
    except:
        cp_map = {}

    del_list=[]
    for k in result:
        if result.get(k,'') is None or result.get(k,'') == '':
            del_list.append(k)

    for k in del_list:
        if k in result:
            del result[k]

    add_field(result,'U_GENDER', up_map.get('gender'))
    add_field(result,'U_AGE_1',1 if up_map.get('age','')=='1' else 0)
    add_field(result,'U_AGE_2',1 if up_map.get('age','')=='2' else 0)

    # calculate len of categories and keywords
    u_categories = up_map.get('categories',[])
    u_keywords = up_map.get('keywords',[])
    c_categories = cp_map.get('categories',[])
    c_keywords = cp_map.get('entities',[])
    t_keywords = cp_map.get('title_entities',[])
    # add_field(result,'U_CAT_LEN', (1 if up_map.get('u_cat_len',0) > 0 else 0))
    add_field(result,'U_CAT_LEN', up_map.get('u_cat_len',0))
    add_field(result,'D_CAT_LEN', len(c_categories))
    # add_field(result,'U_KW_LEN', (1 if up_map.get('u_kw_len',0) > 0 else 0))
    add_field(result,'U_KW_LEN', up_map.get('u_kw_len',0) )
    add_field(result,'D_KW_LEN', len(c_keywords))
    add_field(result,'D_TITLE_KW_LEN', len(t_keywords))

    # image_count
    add_field(result,'IMAGE_COUNT', len(cp_map.get('images',[])))
    #add_field(result,'BODY_IMAGE_COUNT', len(cp_map.get('body_images',[])))
    add_field(result,'HEAD_IMAGE', 0 if cp_map.get('head_image','')=='' else 1)
    add_field(result,'LARGE_IMAGE', 0 if cp_map.get('large_image','')=='' else 1)

    #word count
    add_field(result,'WORD_COUNT', cp_map.get('word_count',0))

    c_categories = normalize_category(c_categories)
    #print c_categories
    # truncate cp, up categories and keywords
    if truncate is not None:
        u_categories = u_categories[:truncate[0]]
        u_keywords = u_keywords[:truncate[1]]
        c_categories = c_categories[:truncate[2]]
        c_keywords = c_keywords[:truncate[3]]

    try:
        add_field(result,'CAT_REL',inner_product(u_categories,c_categories,fname2='weight'))
    except:
        print "calculating cat rel \t" + u_raw + '\t' + d_raw
    try:
        add_field(result,'KW_REL',inner_product(u_keywords,c_keywords,fname2='L1_weight'))
    except:
        print "calculating kw rel \t" + u_raw + '\t' + d_raw
    try:
        add_field(result,'TITLE_KW_REL',inner_product(u_keywords,t_keywords,fname2='L1_weight'))
    except:
        print up_map.get('uid')
        print cp_map.get('item_id')
    try:
        add_field_dnn(result,'BIN_',u_categories,c_categories,u_keywords,c_keywords,t_keywords,fname2='weight')
    except:
        print "calculating BIN_ features \t" + u_raw + '\t' + d_raw

    # add_field(result,'CAT_REL',inner_product(u_categories,c_categories,fname2='weight'))
    # add_field(result,'KW_REL',inner_product(u_keywords,c_keywords,fname2='L1_weight'))
    # add_field(result,'TITLE_KW_REL',inner_product(u_keywords,t_keywords,fname2='L1_weight'))

    return result

@outputSchema("data:[]")
def gen_static_features(u_raw, d_raw, static_features, truncate=(200,200,200,200)):
    if static_features is None:
        result = {}
    else:
        result = static_features

    try:
        up_map = json.loads(u_raw)
    except:
        up_map = {}

    try:
        cp_map = json.loads(d_raw)
    except:
        cp_map = {}

    del_list=[]
    for k in result:
        if result.get(k,'') is None or result.get(k,'') == '':
            del_list.append(k)

    for k in del_list:
        if k in result:
            del result[k]

    add_field(result,'U_GENDER', up_map.get('gender'))
    add_field(result,'U_AGE_1',1 if up_map.get('age','')=='1' else 0)
    add_field(result,'U_AGE_2',1 if up_map.get('age','')=='2' else 0)

    # calculate len of categories and keywords
    u_categories = up_map.get('categories',[])
    u_keywords = up_map.get('keywords',[])
    c_categories = cp_map.get('categories',[])
    c_keywords = cp_map.get('entities',[])
    t_keywords = cp_map.get('title_entities',[])
    # add_field(result,'U_CAT_LEN', (1 if up_map.get('u_cat_len',0) > 0 else 0))
    add_field(result,'U_CAT_LEN', up_map.get('u_cat_len',0))
    add_field(result,'D_CAT_LEN', len(c_categories))
    # add_field(result,'U_KW_LEN', (1 if up_map.get('u_kw_len',0) > 0 else 0))
    add_field(result,'U_KW_LEN', up_map.get('u_kw_len',0) )
    add_field(result,'D_KW_LEN', len(c_keywords))
    add_field(result,'D_TITLE_KW_LEN', len(t_keywords))

    # image_count
    add_field(result,'IMAGE_COUNT', len(cp_map.get('images',[])))
    #add_field(result,'BODY_IMAGE_COUNT', len(cp_map.get('body_images',[])))
    add_field(result,'HEAD_IMAGE', 0 if cp_map.get('head_image','')=='' else 1)
    add_field(result,'LARGE_IMAGE', 0 if cp_map.get('large_image','')=='' else 1)

    #word count
    add_field(result,'WORD_COUNT', cp_map.get('word_count',0))

    c_categories = normalize_category(c_categories)
    #print c_categories
    # truncate cp, up categories and keywords
    if truncate is not None:
        u_categories = u_categories[:truncate[0]]
        u_keywords = u_keywords[:truncate[1]]
        c_categories = c_categories[:truncate[2]]
        c_keywords = c_keywords[:truncate[3]]

    try:
        add_field(result,'CAT_REL',inner_product(u_categories,c_categories,fname2='weight'))
    except:
        print "calculating cat rel \t" + u_raw + '\t' + d_raw
    try:
        add_field(result,'KW_REL',inner_product(u_keywords,c_keywords,fname2='L1_weight'))
    except:
        print "calculating kw rel \t" + u_raw + '\t' + d_raw
    try:
        add_field(result,'TITLE_KW_REL',inner_product(u_keywords,t_keywords,fname2='L1_weight'))
    except:
        print up_map.get('uid')
        print cp_map.get('item_id')

    # add_field(result,'CAT_REL',inner_product(u_categories,c_categories,fname2='weight'))
    # add_field(result,'KW_REL',inner_product(u_keywords,c_keywords,fname2='L1_weight'))
    # add_field(result,'TITLE_KW_REL',inner_product(u_keywords,t_keywords,fname2='L1_weight'))

    return result

    # ('U_GENDER',u_gender,'U_AGE',u_age,'WORD_COUNT',word_count, 'CAT_REL', cat_relevence, 'KEYWD_REL', keyword_relevence)

@outputSchema("doc_age:chararray")
def gen_doc_age(ts, publish_time):
    # print ts,publish_time
    try:
        if long(ts) > long(publish_time):
            return str(float(long(ts) - long(publish_time))/3600.0)
        else:
            return '0'
    except:
        return ''

@outputSchema("time_of_day:int")
def gen_time_of_day(event_time):
    if event_time is None:
        return -1
    try:
        event_time = long(event_time)
    except:
        return None

    time_of_day  = time.localtime(event_time).tm_hour
    return time_of_day

@outputSchema("data:{T:(fname:chararray,fvalue:float)}")
def merge_maps(map1, map2):
    result = []
    for k in map1:
        result.append(([k],float(map1[k])))
    for k in map2:
        result.append(([k],float(map2[k])))
    return result

@outputSchema("count:map[]")
def to_map(input_bag):
    result={}
    if input_bag is None:
        return result
    for item in input_bag:
        if len(item)==2:
            (key,value)=item
            result[key]=value
    return result

@outputSchema("time_of_week:int")
def gen_day_of_week(event_time):
    if event_time is None:
        return -1

    try:
        event_time = long(event_time)
    except:
        return None


    day_of_week = time.strftime("%w", time.localtime(event_time));
    return day_of_week

@outputSchema("feature:chararray")
def add_libsvm_weight(train_data,weight,label=None):
    fields = train_data.split(' ',1)
    if(weight is None):
        weight = 0
    wtstr = str(weight).lower()
    if wtstr.find('nan') >= 0 or wtstr.find("inf") >= 0:
        weight = 0
    if label is None:
        label = fields[0]

    fields[0] = str(label) + (":%.6f" % weight).rstrip('0').rstrip('.')

    return ' '.join(fields)

@outputSchema("feature:chararray")
def check_featurevalue(train_data):
    fields = train_data.split(' ')
    for idx in range(1,len(fields)):
        item = fields[idx]
        k,v = item.split(':',1)
        v = float(v)
        if v >= 0:
            fields[idx] = k + (":%.10f" % v).rstrip('0').rstrip('.')
        else:
            fields[idx] = k + ':0'
            print "check_featurevalue error\t" + item

    return ' '.join(fields)


@outputSchema("line:chararray")
def adjust_feature_value(line):
    if line is None or line == '':
        return line
    tmpline=""
    term = line.strip('\r\n').split(' ')
    try:
        tmpline = term[0]
        for i in xrange(len(term)-1):
            key,value = term[i+1].split(':')
            v = float(value)
            v = 10000 * math.log(v + 1) + 1
            tmpline = "%s %s:%.4f" % (tmpline, key, v)
    except:
        return ""
    return tmpline

import re
catrel_regex = re.compile(' 1:([0-9]+) ')

def get_feature_value(data_str, regex):
    if data_str is None or len(data_str) <=0:
        return None

    res = regex.search(data_str)
    if res is None:
        return None
    res_grp = res.groups()
    try:
        return float(res_grp[0])
    except:
        return None

@outputSchema("cat_rel:float")
def get_cat_rel(data_str):
    cat_rel = get_feature_value(data_str,catrel_regex)
    if cat_rel is None:
        return 0.0
    return cat_rel


# @outputSchema("day_of_week:chararray")
# def gen_day_of_week(ts):
#     try:
#         return str((( ts / 86400 ) -4 ) % 7)
#     except:
#         return ''

if __name__ == '__main__':
    # print format_featmap([('test1',),('test2',),('CAT_REL',)])
    # sys.exit()

    #main()
    #u_raw = """{"keywords_hindi":[],"aid:":"50ef2386660ae8dc","categories_hindi":[],"gender":"2","uid":"50ef2386660ae8dc","version":"1","u_cat_len":"7","lasttime":"1455732162","u_cat_len_hindi":"0","doclist":[["8486291"],["8473379"],["8297562"],["8475044"],["8475044"],["8481153"],["8093343"],["8308103"],["8469900"],["8482719"],["8179978"],["8492213"],["8370766"],["8487524"],["8473169"],["8483618"],["8316764"],["8413873"],["8448943"],["8165787"],["8402745"],["8294072"],["8442491"],["8487097"],["8308092"],["8451300"],["8161210"]],"categories":[[{"weight":"0.332116","name":"1000780"}],[{"weight":"0.247995","name":"1000031"}],[{"weight":"0.158842","name":"1000123"}],[{"weight":"0.082777","name":"1000931"}],[{"weight":"0.078227","name":"1000395"}],[{"weight":"0.076079","name":"1000288"}],[{"weight":"0.023964","name":"1000661"}]],"u_kw_len_hindi":"0","keywords":[[{"weight":"0.027446","name":"dosa"}],[{"weight":"0.026051","name":"roja"}],[{"weight":"0.025051","name":"you"}],[{"weight":"0.023459","name":"her"}],[{"weight":"0.020193","name":"jnu"}],[{"weight":"0.018363","name":"your"}],[{"weight":"0.018107","name":"sex"}],[{"weight":"0.015956","name":"court"}],[{"weight":"0.013365","name":"shehla"}],[{"weight":"0.013242","name":"roja&apos"}],[{"weight":"0.012383","name":"gauri"}],[{"weight":"0.011791","name":"lovemaking"}],[{"weight":"0.010174","name":"router"}],[{"weight":"0.009988","name":"kanhaiya"}],[{"weight":"0.009701","name":"steamy"}],[{"weight":"0.009506","name":"kareena"}],[{"weight":"0.008783","name":"coupons"}],[{"weight":"0.008655","name":"emoji"}],[{"weight":"0.008536","name":"suspension"}],[{"weight":"0.008281","name":"lawyers"}],[{"weight":"0.008174","name":"jayalalitha"}],[{"weight":"0.007881","name":"jhanvi"}],[{"weight":"0.007801","name":"sridevi"}],[{"weight":"0.007624","name":"hanuman"}],[{"weight":"0.007553","name":"chirmuley"}],[{"weight":"0.007525","name":"breasts"}],[{"weight":"0.007491","name":"patent"}],[{"weight":"0.007378","name":"suspended"}],[{"weight":"0.007281","name":"shahrukh"}],[{"weight":"0.006944","name":"younce"}],[{"weight":"0.00685","name":"bhagat"}],[{"weight":"0.006827","name":"muscles"}],[{"weight":"0.006798","name":"barks"}],[{"weight":"0.006626","name":"arnab"}],[{"weight":"0.006488","name":"rajan"}],[{"weight":"0.006458","name":"kapoor"}],[{"weight":"0.006415","name":"lord"}],[{"weight":"0.006285","name":"lip"}],[{"weight":"0.006259","name":"kissing"}],[{"weight":"0.006159","name":"students"}],[{"weight":"0.006151","name":"house"}],[{"weight":"0.006026","name":"position"}],[{"weight":"0.005953","name":"salary"}],[{"weight":"0.005793","name":"our"}],[{"weight":"0.005726","name":"bar"}],[{"weight":"0.0055","name":"phaansi"}],[{"weight":"0.005439","name":"trolled"}],[{"weight":"0.005433","name":"locks"}],[{"weight":"0.005428","name":"80%"}],[{"weight":"0.005407","name":"bollywood"}],[{"weight":"0.005341","name":"sale"}],[{"weight":"0.005306","name":"rohtas"}],[{"weight":"0.005267","name":"deepika"}],[{"weight":"0.005245","name":"tawa"}],[{"weight":"0.005218","name":"goswami"}],[{"weight":"0.005113","name":"apkmirror"}],[{"weight":"0.005083","name":"anitha"}],[{"weight":"0.00507","name":"partner"}],[{"weight":"0.005018","name":"scenes"}],[{"weight":"0.004877","name":"raguram"}],[{"weight":"0.00482","name":"journalism"}],[{"weight":"0.004765","name":"comrade"}],[{"weight":"0.004741","name":"chetan"}],[{"weight":"0.004644","name":"shopperstop"}],[{"weight":"0.004574","name":"maaro"}],[{"weight":"0.004574","name":"advocate"}],[{"weight":"0.004494","name":"advocates"}],[{"weight":"0.004491","name":"constitution"}],[{"weight":"0.004426","name":"bajrang"}],[{"weight":"0.00442","name":"draw"}],[{"weight":"0.004393","name":"snapchats"}],[{"weight":"0.004361","name":"ysr"}],[{"weight":"0.004299","name":"she"}],[{"weight":"0.004282","name":"crazy"}],[{"weight":"0.004217","name":"profession"}],[{"weight":"0.004187","name":"school"}],[{"weight":"0.004177","name":"nipples"}],[{"weight":"0.004162","name":"traitor"}],[{"weight":"0.00415","name":"scene"}],[{"weight":"0.004138","name":"foreplay"}],[{"weight":"0.004135","name":"debut"}],[{"weight":"0.004117","name":"smoking"}],[{"weight":"0.004052","name":"generic"}],[{"weight":"0.003998","name":"price"}],[{"weight":"0.003994","name":"signal"}],[{"weight":"0.003988","name":"teacher"}],[{"weight":"0.003969","name":"dog"}],[{"weight":"0.003941","name":"channels"}],[{"weight":"0.003878","name":"temple"}],[{"weight":"0.003855","name":"jabong"}],[{"weight":"0.003843","name":"khan"}],[{"weight":"0.00377","name":"network"}],[{"weight":"0.003769","name":"cable"}],[{"weight":"0.003768","name":"routers"}],[{"weight":"0.003712","name":"wireless"}],[{"weight":"0.00369","name":"debuting"}],[{"weight":"0.003686","name":"fi"}],[{"weight":"0.003683","name":"changes"}],[{"weight":"0.003681","name":"lumps"}],[{"weight":"0.003669","name":"screen"}],[{"weight":"0.003647","name":"breast"}],[{"weight":"0.003626","name":"muscle"}],[{"weight":"0.003595","name":"district"}],[{"weight":"0.003588","name":"missionary"}],[{"weight":"0.003576","name":"cherubic"}],[{"weight":"0.003572","name":"goli"}],[{"weight":"0.003556","name":"wi"}],[{"weight":"0.003555","name":"jnusu"}],[{"weight":"0.003525","name":"kumar"}],[{"weight":"0.003514","name":"practicing"}],[{"weight":"0.003506","name":"bajirao"}],[{"weight":"0.003501","name":"trace"}],[{"weight":"0.003493","name":"tweets"}],[{"weight":"0.003483","name":"flipkart"}],[{"weight":"0.00347","name":"supreme"}],[{"weight":"0.003463","name":"core"}],[{"weight":"0.003462","name":"&apos"}],[{"weight":"0.003386","name":"overshadowed"}],[{"weight":"0.003375","name":"professor"}],[{"weight":"0.003367","name":"mastani"}],[{"weight":"0.003286","name":"compliments"}],[{"weight":"0.003279","name":"desirable"}],[{"weight":"0.003216","name":"boasted"}],[{"weight":"0.003176","name":"441"}],[{"weight":"0.003162","name":"get"}],[{"weight":"0.00315","name":"patiala"}],[{"weight":"0.003132","name":"excitement"}],[{"weight":"0.00311","name":"encroachment"}],[{"weight":"0.003108","name":"specualtions"}],[{"weight":"0.003108","name":"romurs"}],[{"weight":"0.003108","name":"naevli"}],[{"weight":"0.003108","name":"jhanv"}],[{"weight":"0.003075","name":"shop"}],[{"weight":"0.003043","name":"panty"}],[{"weight":"0.003024","name":"xander"}],[{"weight":"0.003014","name":"varies"}],[{"weight":"0.003012","name":"version"}],[{"weight":"0.003002","name":"hurry"}],[{"weight":"0.002991","name":"sashikala"}],[{"weight":"0.002985","name":"leg"}],[{"weight":"0.002975","name":"kisses"}],[{"weight":"0.002961","name":"emraan"}],[{"weight":"0.002937","name":"film"}],[{"weight":"0.002929","name":"drunkenly"}],[{"weight":"0.002928","name":"xxx"}],[{"weight":"0.002923","name":"groan"}],[{"weight":"0.002917","name":"nuanced"}],[{"weight":"0.002912","name":"comfort&apos"}],[{"weight":"0.002877","name":"moans"}],[{"weight":"0.002874","name":"prices"}],[{"weight":"0.002866","name":"courtesy"}],[{"weight":"0.002812","name":"70%"}],[{"weight":"0.002811","name":"mar"}],[{"weight":"0.002804","name":"upwards"}],[{"weight":"0.002795","name":"tabs"}],[{"weight":"0.002771","name":"oppressed"}],[{"weight":"0.00276","name":"shoot"}],[{"weight":"0.002735","name":"pregnancy"}],[{"weight":"0.002729","name":"twitterati"}],[{"weight":"0.002722","name":"vivacious"}],[{"weight":"0.002714","name":"roadside"}],[{"weight":"0.002675","name":"imprisonment"}],[{"weight":"0.002667","name":"glutes"}],[{"weight":"0.002659","name":"wanting"}],[{"weight":"0.002633","name":"judgement"}],[{"weight":"0.002621","name":"app"}],[{"weight":"0.002619","name":"night"}],[{"weight":"0.002617","name":"technology"}],[{"weight":"0.002617","name":"kiss"}],[{"weight":"0.002612","name":"panchmukhi"}],[{"weight":"0.002601","name":"bipasha"}],[{"weight":"0.002572","name":"mean"}],[{"weight":"0.002565","name":"ki"}],[{"weight":"0.002555","name":"inflation"}],[{"weight":"0.00255","name":"romcoms"}],[{"weight":"0.002524","name":"truth"}],[{"weight":"0.002523","name":"gynaecologist"}],[{"weight":"0.002499","name":"moods"}],[{"weight":"0.002484","name":"whetting"}],[{"weight":"0.002456","name":"might"}],[{"weight":"0.002445","name":"arrested"}],[{"weight":"0.002445","name":"tweeting"}],[{"weight":"0.002419","name":"personality"}],[{"weight":"0.002381","name":"improving"}],[{"weight":"0.002329","name":"voice"}],[{"weight":"0.002325","name":"blames"}],[{"weight":"0.002303","name":"shell"}],[{"weight":"0.002291","name":"update"}],[{"weight":"0.002288","name":"drugs"}],[{"weight":"0.002281","name":"hormonal"}],[{"weight":"0.002261","name":"act"}],[{"weight":"0.002259","name":"sectors"}],[{"weight":"0.002222","name":"hepatitis"}],[{"weight":"0.00221","name":"kochi"}],[{"weight":"0.002123","name":"jmu"}],[{"weight":"0.002112","name":"wages"}],[{"weight":"0.002109","name":"explains"}],[{"weight":"0.002103","name":"agitation"}],[{"weight":"0.002102","name":"store"}],[{"weight":"0.002081","name":"shora"}]],"new_user":"0","age":"2","u_kw_len":"225","join_flag":"1"}"""
    d_raw = """{"editor_level":null,"discovery_time":"1456131618","last_modified_time":"1456131618","summary":"","title_md5":"3f3c9e76","source_feed":[["http://api-news.dailyhunt.in/api/v1/news/articles/latest/group/boldsky/Insync"]],"update_time":"1456308444","body_images":[["http://img1.store.ksmobile.net/cminstanews/20160222/09/23052_bbba5f3a_145613161637_400_300.jpg"],["http://img1.store.ksmobile.net/cminstanews/20160222/09/19922_89246d43_145613161593_400_300.jpg"],["http://img1.store.ksmobile.net/cminstanews/20160222/09/23103_9de28834_145613161833_400_300.jpg"],["http://img1.store.ksmobile.net/cminstanews/20160222/09/20537_d91aae7e_145613161480_400_300.jpg"],["http://img1.store.ksmobile.net/cminstanews/20160222/09/38079_f271f86c_145613161417_400_300.jpg"],["http://img1.store.ksmobile.net/cminstanews/20160222/09/14435_302ffd4c_145613161735_400_300.jpg"],["http://img1.store.ksmobile.net/cminstanews/20160222/09/12439_171835d0_145613161646_400_300.jpg"]],"has_copyright":"0","channel_ids":[],"feature_scores":{"newsy_score":"0.0","adult_score":"0.0"},"image_count":"3","author":"","newsy_score":"0.0","large_image":"","is_servable":"true","fetch_time":"1456131618","image_md5":"1906a737","editorial_importance_level":"0","content_md5":"090399a0","cp_version":"13","copyright":"","title_entities":[[{"L1_weight":"0.3663366336633663","weight":"0.6322246667104031","name":"visit"}],[{"L1_weight":"0.33663366336633666","weight":"0.5809632072473975","name":"places"}],[{"L1_weight":"0.297029702970297","weight":"0.5126145946300565","name":"scary"}]],"head_image":"http://img1.store.ksmobile.net/cminstanews/20160222/09/34822_0cb34881_14561316182_400_267.jpg,http://img1.store.ksmobile.net/cminstanews/20160222/09/19596_2e0259af_145613161874_400_267.jpg,http://img1.store.ksmobile.net/cminstanews/20160222/09/17727_f03128fa_145613161894_400_267.jpg","group_id":"335425","source":"Boldsky","images":[["http://img1.store.ksmobile.net/cminstanews/20160222/09/11074_1906a737_145613161864_200_140.jpg"],["http://img1.store.ksmobile.net/cminstanews/20160222/09/6981_af5d3ac5_145613161831_200_140.jpg"],["http://img1.store.ksmobile.net/cminstanews/20160222/09/6700_59df9dae_145613161882_200_140.jpg"]],"md5":"3c22f9df","type":"article","language":"en","entities":[[{"L1_weight":"0.1452993795361382","weight":"0.5764168959501342","name":"island"}],[{"L1_weight":"0.07974040892994448","weight":"0.3163380266586828","name":"scary"}],[{"L1_weight":"0.07199366229995983","weight":"0.285605922637156","name":"comrade"}],[{"L1_weight":"0.06861938331913191","weight":"0.27221982682306567","name":"scariest"}],[{"L1_weight":"0.05469275898709523","weight":"0.21697154156428644","name":"dolls"}],[{"L1_weight":"0.04995537563130075","weight":"0.19817787694169448","name":"earth"}],[{"L1_weight":"0.04745554914011221","weight":"0.188260819958623","name":"alcatraz"}],[{"L1_weight":"0.04699348217911745","weight":"0.18642775498458114","name":"image"}],[{"L1_weight":"0.04593719883909153","weight":"0.18223737532814116","name":"islands"}],[{"L1_weight":"0.04504434666340047","weight":"0.1786953431371079","name":"believed"}],[{"L1_weight":"0.04129938354895698","weight":"0.16383870699202113","name":"creepiest"}],[{"L1_weight":"0.04007748023435956","weight":"0.15899129664519138","name":"place"}],[{"L1_weight":"0.03961573247485764","weight":"0.15715949797479287","name":"located"}],[{"L1_weight":"0.03950962077859445","weight":"0.15673854246363172","name":"places"}],[{"L1_weight":"0.0345412039423026","weight":"0.13702834535401884","name":"sounds"}],[{"L1_weight":"0.030880190820476428","weight":"0.12250474706713906","name":"haunted"}],[{"L1_weight":"0.03054696726205649","weight":"0.12118281651371844","name":"suicide"}],[{"L1_weight":"0.0296401386210003","weight":"0.11758533831315751","name":"munecas"}],[{"L1_weight":"0.0296401386210003","weight":"0.11758533831315751","name":"malinta"}],[{"L1_weight":"0.028517598171103123","weight":"0.1131321102004575","name":"tunnel"}]],"adult_score":"0.0","content_length":"4245","word_count":"3300","title":"Would You Visit These Scary Places?","source_type":"2","item_id":"8643273","categories":[[{"weight":"0.8667934236203926","name":"1000780"}]],"publisher":"Boldsky","editorial_item_level":"0","region":"in","link":"http://newshunt.com/share/50008290","channel_names":[],"publish_time":"1456131029"}"""
    #static_features = {"DOCAGE":2}
    #print gen_static_features(u_raw, d_raw, static_features)

    for line in sys.stdin:
        print adjust_feature_value(line)
