#! /usr/bin/env python

import math
import sys,traceback

class Metric:
    score_index = 1
    view_index = 2
    click_index = 3

    def __init__(self, model):
        self.model = model
        self.current_click = 0
        self.current_view = 0
        self.view_click = 0
        # area initial
        self.view_click_roc_area = 0
        self.view_click_pr_area = 0
        # view->click
        self.view_click_pr_pre_y = 0
        self.prev_click = 0

        # log loss
        # viecw->click
        self.view_click_log_loss_factor1 = 0
        self.view_click_log_loss_factor2 = 0
        self.view_click_log_loss_total_neg = 0

    def next_line(self,click,view,score,weight):
        # current line values: click, view
        tk_click = float(click)*weight
        tk_view = float(view)*weight
        tk_score = float(score)

        # cumulative (view - click)
        self.view_click += (tk_view - tk_click)

        self.current_click += float(tk_click)
        self.current_view += float(tk_view)

        #print  self.current_click, self.view_click

        # generate view-> click roc
        #print self.current_click, self.prev_click
        view_click_roc_sub_area = (tk_view - tk_click)*(self.current_click + self.prev_click)/2
        self.view_click_roc_area += view_click_roc_sub_area
        #print self.view_click_roc_area
        self.prev_click = self.current_click

        # generate view->click pr
        if self.current_click + self.view_click > 0:
            view_click_r = float(self.current_click)/(self.current_click + self.view_click)
            view_click_pr_sub_area = (tk_click)*(view_click_r + self.view_click_pr_pre_y)/2
            self.view_click_pr_pre_y = view_click_r
            self.view_click_pr_area += view_click_pr_sub_area

        # geneate view->click logloss
        if tk_score < 10e-6 or tk_score >= 1:
            tk_score = 0.5
            return

        tmp_tk_click = tk_click
        for i in xrange(0, int(tk_view)):
            if tmp_tk_click > 0:
                self.view_click_log_loss_factor1 += math.log(tk_score)
                tmp_tk_click -= 1
            else:
                self.view_click_log_loss_factor2 += math.log(1-tk_score)
        self.view_click_log_loss_total_neg += tk_view


    def get_result(self):
    # last pkg
        if self.current_click * self.view_click > 0:
            self.view_click_roc_area = self.view_click_roc_area/(self.current_click * self.view_click)
        else:
            self.view_click_roc_area = 0

        # view-click pr
        if self.current_click > 0:
            self.view_click_pr_area = self.view_click_pr_area/self.current_click
        else:
            self.view_click_pr_area = 0

        # logloss
        # view->clic
        if self.view_click_log_loss_total_neg >0:
            view_click_total_logLoss = -1 * (self.view_click_log_loss_factor1 + self.view_click_log_loss_factor2)/self.view_click_log_loss_total_neg
        else:
            view_click_total_logLoss = 0

        current_bin = [str(self.current_view), str(self.current_click), str(self.view_click_roc_area), str(self.view_click_pr_area), str(view_click_total_logLoss)]
        return current_bin


def generate_roc_pr():
    metrics = {}
    for line in sys.stdin:
        f = line.rstrip().split('\t')
        if len(f) == 5:
            model, view, click, score, weight = f
        else:
            model, view, click, score = f
            weight = 1.0

        if model not in metrics:
            metrics[model] = Metric(model)
            metrics[model].next_line(click, view, float(score), float(weight))
        else:
            metrics[model].next_line(click, view, float(score), float(weight))

    for model in metrics:
        res = metrics[model].get_result()
        print '\t'.join([model] + res)

if __name__ == '__main__':
    generate_roc_pr()
