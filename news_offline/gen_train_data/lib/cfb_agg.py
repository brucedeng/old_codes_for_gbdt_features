# -*- coding:utf-8 -*-
import os
import datetime
import time
import math
import hashlib

if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper
    import json
else:
    import com.xhaus.jyson.JysonCodec as json

@outputSchema("b:{t:(ts:long, total_click_count:double, total_view_count:double, prev_ts:long)}")
def gen_cfb_new(input_bag, start_timestamp, end_timestamp, decay_factor, freq ='1d'):
    # print "gen_cfb_new"
    if input_bag is None or len(input_bag) <= 0:
        return None
    if start_timestamp is None or start_timestamp <= 0:
        return None
    if end_timestamp is None or end_timestamp < start_timestamp:
        return None

    # aggregate every 600 seconds, hense, 10 minutes.
    agg_slice=600

    start_timestamp = start_timestamp/agg_slice * agg_slice
    end_timestamp = end_timestamp / agg_slice * agg_slice

    period = freq[-1]
    num = freq[:-1]
    interval = 0
    if period == 'h':
        interval = 6
    elif period == 'd':
        interval = 144

    interval = int(num) * interval
    interval = interval * agg_slice
    interval = interval - agg_slice

    #pre process bag
    record_dict = {}
    for record in input_bag:
        ts = long(record[0])
        try:
            click_count = float(str(record[1]))
            view_count = float(record[2])
            prev_ts = long(record[3])
            record_dict[ts] = [click_count, view_count, prev_ts]
        except:
            print record

    out_bag = []
    need_ts = set()
    for record in input_bag:
        ts = long(record[0])
        try:
            click_count = float(str(record[1]))
            view_count = float(record[2])
            prev_ts = long(record[3])
            record_dict[ts] = [click_count, view_count, prev_ts]
            need_ts.add(prev_ts - agg_slice)
        except:
            print record

    out_bag = []
    timestamp_interval = range(start_timestamp, end_timestamp, agg_slice)
    # print "start parsing"

    sum_timestamp = range(start_timestamp-interval, end_timestamp+1, agg_slice)
    sum_view_table = [0]
    sum_click_table = [0]
    for timestamp in sum_timestamp:
        if timestamp in record_dict:
            sum_view_table.append(decay_factor*sum_view_table[-1] +
                                  record_dict[timestamp][1])
            sum_click_table.append(decay_factor*sum_click_table[-1] +
                                   record_dict[timestamp][0])
        else:
            sum_view_table.append(decay_factor*sum_view_table[-1])
            sum_click_table.append(decay_factor*sum_click_table[-1])

    advance_index = len(range(start_timestamp - interval, start_timestamp,
                              agg_slice)) + 1
    decay_factor_interval = decay_factor ** advance_index
    for i, cur_ts in enumerate(timestamp_interval):
        if cur_ts not in need_ts:
            continue
        total_click = sum_click_table[i + advance_index] - sum_click_table[i]*decay_factor_interval
        total_view = sum_view_table[i + advance_index] - sum_view_table[i]*decay_factor_interval
        out_bag.append((cur_ts, total_click, total_view, cur_ts))
    return out_bag


@outputSchema("timestamp:long")
def date2timestamp(date_str, wnd, format_str='%Y%m%d%H%M'):
    if date_str is None  or format_str is None:
        return None
    try:
        date_str = str(date_str)
        timestamp = int(time.mktime(datetime.datetime.strptime(date_str, format_str).timetuple()))
        timestamp = timestamp/long(wnd) * long(wnd)
        return timestamp
    except:
        return None

if __name__ == '__main__':
    import sys
    import re
    print date2timestamp('201512212350',600)
    raw_data = [(1448235000,0,0,1448235000),(1448236200,1,1,1448236200),(1448239800,0,1,1448239800)]
    test_data= '{(1448123400,2.0,23.0,1448123400),(1448124000,2.0,42.0,1448124000),(1448124600,0.0,36.0,1448124600),(1448127000,2.0,30.0,1448127000),(1448132400,1.0,5.0,1448132400),(1448134200,1.0,47.0,1448134200),(1448135400,1.0,66.0,1448135400),(1448136600,1.0,19.0,1448136600),(1448137200,2.0,53.0,1448137200),(1448154600,4.0,24.0,1448154600),(1448155200,0.0,36.0,1448155200),(1448157600,1.0,64.0,1448157600),(1448161200,7.0,66.0,1448161200),(1448161800,2.0,10.0,1448161800),(1448167800,1.0,5.0,1448167800),(1448168400,2.0,67.0,1448168400),(1448174400,1.0,31.0,1448174400),(1448176200,0.0,5.0,1448176200),(1448178000,1.0,1.0,1448178000),(1448178600,4.0,160.0,1448178600),(1448179800,1.0,1.0,1448179800),(1448187000,4.0,33.0,1448187000),(1448194800,2.0,82.0,1448194800),(1448199000,0.0,46.0,1448199000),(1448199600,1.0,21.0,1448199600),(1448200200,5.0,32.0,1448200200),(1448200800,9.0,42.0,1448200800),(1448201400,5.0,61.0,1448201400),(1448202600,3.0,62.0,1448202600),(1448203200,5.0,39.0,1448203200),(1448203800,2.0,25.0,1448203800),(1448204400,3.0,60.0,1448204400),(1448205000,1.0,150.0,1448205000),(1448205600,1.0,128.0,1448205600),(1448208000,0.0,14.0,1448208000),(1448208600,1.0,14.0,1448208600),(1448209800,0.0,17.0,1448209800),(1448210400,11.0,195.0,1448210400),(1448211000,4.0,120.0,1448211000),(1448211600,8.0,137.0,1448211600),(1448212200,6.0,95.0,1448212200),(1448212800,2.0,73.0,1448212800),(1448220000,0.0,50.0,1448220000),(1448220600,4.0,232.0,1448220600),(1448245200,5.0,76.0,1448245200),(1448248800,2.0,110.0,1448248800),(1448252400,1.0,5.0,1448252400),(1448257200,2.0,56.0,1448257200),(1448258400,1.0,27.0,1448258400),(1448259000,3.0,81.0,1448259000),(1448260200,3.0,38.0,1448260200),(1448260800,4.0,74.0,1448260800),(1448262000,2.0,65.0,1448262000),(1448262600,1.0,37.0,1448262600),(1448266200,0.0,4.0,1448266200),(1448268600,1.0,59.0,1448268600),(1448269200,4.0,135.0,1448269200),(1448270400,0.0,1.0,1448270400),(1448271000,1.0,16.0,1448271000),(1448277000,1.0,3.0,1448277000),(1448277600,2.0,152.0,1448277600),(1448278200,0.0,11.0,1448278200),(1448278800,1.0,0.0,1448278800),(1448281200,1.0,90.0,1448281200),(1448281800,1.0,218.0,1448281800),(1448282400,2.0,20.0,1448282400),(1448284800,1.0,65.0,1448284800),(1448286000,1.0,3.0,1448286000),(1448287200,1.0,53.0,1448287200),(1448287800,1.0,83.0,1448287800),(1448289000,0.0,85.0,1448289000),(1448289600,3.0,14.0,1448289600),(1448290200,0.0,25.0,1448290200),(1448290800,4.0,111.0,1448290800),(1448291400,4.0,97.0,1448291400)}'

    test_data = '{(1448236800,0.0,3.0,1448236800),(1448237400,0.0,4.0,1448237400),(1448238000,0.0,1.0,1448238000),(1448238600,0.0,3.0,1448238600),(1448239200,0.0,3.0,1448239200),(1448239800,1.0,4.0,1448239800),(1448240400,0.0,4.0,1448240400),(1448241000,1.0,5.0,1448241000),(1448241600,2.0,6.0,1448241600),(1448242200,0.0,3.0,1448242200),(1448242800,0.0,3.0,1448242800),(1448243400,0.0,3.0,1448243400),(1448244000,0.0,2.0,1448244000),(1448244600,0.0,2.0,1448244600),(1448245200,0.0,1.0,1448245200),(1448245800,0.0,1.0,1448245800),(1448246400,0.0,3.0,1448246400),(1448247000,0.0,4.0,1448247000),(1448248800,0.0,3.0,1448248800),(1448249400,0.0,1.0,1448249400),(1448271600,0.0,1.0,1448271600),(1448272200,0.0,1.0,1448272200),(1448284200,0.0,1.0,1448284200),(1448290200,0.0,1.0,1448290200),(1448309400,0.0,1.0,1448309400),(1448425200,0.0,1.0,1448425200)}'
    bg_splitor=re.compile('\\(([^,()\\[\\]]*),([^,()\\[\\]]*),([^,()\\[\\]]*),([^,()\\[\\]]*)\\)')
    #print gen_cfb_new(bg_splitor.findall(test_data), 1448409600, 1448410200, 0.99, '2d')
    print gen_cfb_new(raw_data, 1448235000, 1448241000, 0.99, '2d')
    # print date2timestamp('2015112300',600, '%Y%m%d%H')

