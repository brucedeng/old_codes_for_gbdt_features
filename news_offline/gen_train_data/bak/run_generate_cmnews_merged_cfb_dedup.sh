
while getopts "s:e:l:" arg
do
    case $arg in
        s) #start date of events
        event_start=$OPTARG
        ;;
        e) #end date  of event
        event_end=$OPTARG
        ;;
        l) #run data
        app_lan=$OPTARG
        ;;
        ?)  #?
        echo "unkonw argument"
        exit 1
        ;;
    esac
done
                                                                                                                                                                                    
PIG_PARAM="-conf ./default-hadoop.xml -useHCatalog ";
PIG_PARAM=" -useHCatalog -Dmapred.job.queue.name=offline ";

echo $event_start
echo $event_end
cur_date=$event_start

rm -f pig*.log

config_file="hdfs://mycluster/projects/news/model/configs/config.json"
hadoop fs -copyFromLocal -f config.json $config_file
category_data="hdfs://mycluster/projects/news/model/configs/cat_tree.txt"
hadoop fs -copyFromLocal -f lib/cat_tree.txt $category_data
city_data="hdfs://mycluster/projects/news/model/configs/ip_city.data"
hadoop fs -copyFromLocal -f lib/ip_city.data $city_data

while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
do
    # TOP_PARAM="-p U_NCAT=5 -p U_NKEY=15 -p D_NCAT=10 -p D_NKEY=25 -p WND=600"
    TOP_PARAM="-p U_NCAT=15 -p U_NKEY=20 -p D_NCAT=5 -p D_NKEY=10 -p WND=600"

    # input_event="s3://iwarehouse/cmnow/news_rcv_log/$cur_date/*/*.rcv" 
    input_event="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click,listpagedwelltime,readtime}/$cur_date/*/*"
    prev_date=`date -d "$cur_date -1 days" +%Y%m%d`
    if [[ "$app_lan" == "hi" ]]; then
        input_user="/projects/news/model/hindi_up/$prev_date"
    else
        input_user="s3://com.cmcm.instanews.usw2.prod/user_profile/V1/up_json/$prev_date"
    fi

    ftr_start=`date -d "+4 day ago $cur_date" +%Y%m%d`;
    ftr_end=$cur_date;
    agg_dates=$ftr_start
    for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done

    input_content_7d="s3://com.cmcm.instanews.usw2.prod/cpp/feeder/{$agg_dates}/*/*/*.data"
    input_content_7d="/projects/news/cpp/feeder/in_cp_dump/{$agg_dates}/*/*/*.data"
    input_interest="s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall/$cur_date"
    
    # output="/projects/news/model/experiments/features/${app_lan}_raw_features_joined_with_cfb/$cur_date"
    # output="/projects/news/model/experiments/features/${app_lan}_raw_features_joined_with_nrtcfb_dedup/$cur_date"
    output="/projects/news/model/experiments/features/${app_lan}_raw_features_joined_with_offline_dedup_cfb/$cur_date"

    ftr_start=`date -d "+2 day ago $cur_date" +%Y%m%d`;
    ftr_end=$cur_date;
    agg_dates_cfb=$ftr_start
    for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates_cfb="$agg_dates_cfb,$agg_day"; done
    # input_cfb="hdfs://mycluster/projects/news/model/${app_lan}_offline_cfb_raw/{$agg_dates_cfb}"
    input_cfb="hdfs://mycluster/projects/news/model/${app_lan}_offline_cfb_batchdedup_raw/{$agg_dates_cfb}"
    # if [[ $app_lan == "en" ]]; then
    #     # without inner batch dedup
    #     input_cfb="s3://com.cmcm.instanews.usw2.prod/nrt/cfb/{$agg_dates_cfb}"
    #     # with inner batch dedup
    #     #input_cfb="/tmp/content/nrt/cfb/{$agg_dates_cfb}"
    # else
    #     input_cfb="s3://com.cmcm.instanews.usw2.prod/nrt/cfb_multi_lan/hi/{$agg_dates_cfb}"
    # fi

    # data_start_time="${cur_date}08"
    data_end_time=`date -d "$cur_date +1 days" +%Y%m%d`
    data_end_time="${data_end_time}00"

    data_start_time=${cur_date}00
    decay_factor=0.99
    n_decaywnd=2d

    cmd="pig $PIG_PARAM $TOP_PARAM -p CONFIG=$config_file -p CAT_TREE=$category_data -p CITY_DATA=$city_data -p app_lan=$app_lan -p INPUT_EVENT='$input_event' -p INPUT_CONTENT='$input_content_7d' -p INPUT_USER='$input_user' -p INPUT_CFB='$input_cfb' -p OUTPUT='$output' -p START_DATE_TIME=$data_start_time -p END_DATE_TIME=$data_end_time -p DECAY_FACTOR=$decay_factor -p N_DECAYWND=$n_decaywnd -p INTEREST=$input_interest generate_cmnews_merged_cfb.pig"
    echo $cmd
    eval $cmd;

    if [[ $cur_date -eq 20160206 ]]; then
        echo sleep 1 hour
        sleep 3600
    fi

    if [ $? != 0 ]; then echo "ERROR: Failed to run pipeline!"; exit -1; fi
    cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
done



