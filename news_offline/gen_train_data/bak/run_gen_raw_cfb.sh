
while getopts "s:e:l:" arg
do
    case $arg in
        s) #start date of events
        event_start=$OPTARG
        ;;
        e) #end date  of event
        event_end=$OPTARG
        ;;
        l) #run data
        app_lan=$OPTARG
        ;;
        ?)  #?
        echo "unkonw argument"
        exit 1
        ;;
    esac
done
                                                                                                                                                                                    
#PIG_PARAM="-conf ./default-hadoop.xml -useHCatalog ";
PIG_PARAM=" -Dmapred.job.queue.name=offline";
PIG_PARAM=" -Dmapred.job.queue.name=default";

echo $event_start
echo $event_end
cur_date=$event_start

rm -f pig*.log
set -e

# if [ "$run_raw_cfb" = "true" ]; then
    config_file="hdfs://mycluster/projects/news/model/configs/config_nrt.json"
    city_file="hdfs://mycluster/projects/news/model/configs/ip_city.data"
    hadoop fs -copyFromLocal -f config.json $config_file
    hadoop fs -copyFromLocal -f lib/ip_city.data $city_file

    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do
        TOP_PARAM="-p U_NCAT=15 -p U_NKEY=20 -p D_NCAT=5 -p D_NKEY=10 -p WND=600"
        
        
          # input_event="s3://iwarehouse/cmnow/news_rcv_log/$cur_date/*/*.rcv" 
        input_event="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click}/$cur_date/*/*"
        up_date=`date -d "+1 day ago $cur_date" +%Y%m%d`;
        if [[ "$app_lan" == "hi" ]]; then
            input_user="/projects/news/model/hindi_up/$up_date"
        else
            input_user="s3://com.cmcm.instanews.usw2.prod/user_profile/V1/up_json/$up_date"
        fi

        ftr_start=`date -d "+6 day ago $cur_date" +%Y%m%d`;
        ftr_end=$cur_date;
        agg_dates=$ftr_start
           for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done

        # input_content_7d="s3://com.cmcm.instanews.usw2.prod/cpp/feeder/{$agg_dates}/*/*/*.data"
        input_content_7d="/projects/news/cpp/feeder/in_cp_dump/{$agg_dates}/*/*/*.data"

        #output="s3://com.cmcm.instanews.usw2.prod/offline_cfb_raw/$cur_date"
        output="/projects/news/model/${app_lan}_offline_cfb_raw/$cur_date"

        cmd="pig $PIG_PARAM $TOP_PARAM -p CITY_DATA=$city_file -p CONFIG=$config_file -p event='$input_event' -p cp_dump='$input_content_7d' -p up_dump='$input_user' -p output='$output' -p app_lan=$app_lan generate_cmnews_cfb_raw_data_1h.pig"
        echo $cmd
        eval $cmd;

        if [ $? != 0 ]; then echo "ERROR: Failed to run pipeline!"; exit -1; fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done
# fi
