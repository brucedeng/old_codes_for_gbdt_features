REGISTER 'lib/*.jar';
REGISTER 'interest_utils.py' USING jython AS udf;

-- set mapred.create.symlink yes;
-- set mapred.cache.files $CAT_CONF#cat_conf.txt

-- raw_cfb = load '$raw_cfb' as (ts:long, feature_type:chararray, feature_name:chararray, click:float, view:float);

-- raw_cfb = filter raw_cfb by feature_type is not null and feature_type != '';
-- --and feature_name=='3ee33a470877ffc';

-- -- category_cfb = filter raw_cfb by feature_type == 'C_D_U_CATEGORY_CATEGORY';
-- category_cfb = filter raw_cfb by feature_type == 'C_D_U_CONID_CATEGORY';

-- events = FOREACH ( GROUP category_cfb by (feature_type, feature_name)) GENERATE 
--     FLATTEN($0) as (feature_type,feature_name), SUM($1.click) as click, SUM($1.view) as view;

-- events = FOREACH events GENERATE feature_type, FLATTEN(udf.split_chararray_ctrlb(feature_name)) as (conid,category), click, view;

-- events = FOREACH events GENERATE feature_type, (chararray)conid as conid, (chararray)category as category, click, view;
-- -- store events into '$output';

-- raw_content = LOAD '$cp_dump' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

-- raw_content = foreach raw_content generate  
--   json#'item_id' as content_id:chararray,
--   -- josn#'channel_names' as channel_names:chararray,
--   json#'title_md5' as title_md5:chararray,
--   json#'group_id' as groupid:chararray,
--   REPLACE(json#'publisher','(\r\n|\n|\t|\r)','') as publisher:chararray,
--   json#'type' as type:chararray,
--   (int)(json#'source_type') as source_type:int,
--   (long)json#'publish_time' as publish_time:long,
--   (long)json#'update_time' as update_time:long,
--   (int)json#'image_count' as image_count:int,
--   (int)json#'word_count' as word_count:int,
--   json#'categories' as categories:{t1:(y:map[])},
--   json#'entities' as keywords:{t1:(y:map[])};

-- raw_content_filter = FILTER raw_content by (type=='article' or type=='slideshow' or type=='photostory');
-- raw_content = FOREACH raw_content_filter GENERATE content_id,title_md5, groupid, publisher, type, publish_time, update_time, image_count, word_count,
--     categories as categories:{t1:(y:map[])},
--     keywords as keywords;

-- raw_content_distinct = foreach (group raw_content by content_id) {
--     r = order raw_content by update_time DESC;
--     l = limit r 1;
--     generate flatten(l) as (content_id,title_md5, groupid, publisher, type, publish_time,update_time, image_count, word_count,categories,keywords);
-- };

-- raw_content_distinct = FOREACH raw_content_distinct {
--     cats = FOREACH categories GENERATE (chararray)(y#'name') as name:chararray;
--     GENERATE content_id, update_time, cats as categories:{T:(name:chararray)};
-- }

-- events_jnd = FOREACH ( JOIN events by conid, raw_content_distinct by content_id ) GENERATE 
--     events::feature_type as feature_type,
--     events::conid as d_conid,
--     events::category as u_category,
--     events::click as click,
--     events::view as view,
--     udf.get_category_names(raw_content_distinct::categories) as categories;

--     -- raw_content_distinct::categories;
-- events_cat = FOREACH events_jnd GENERATE d_conid,udf.get_category_names({(u_category)}) as u_category, click, view, click/view as ctr, categories;

-- d_ctr_grp = FOREACH ( GROUP events_cat by d_conid ) GENERATE 
--     SUM($1.click) as d_click, SUM($1.view) as d_view, FLATTEN($1);


-- d_ctr_data = FOREACH d_ctr_grp GENERATE d_conid,u_category, categories, ctr, view, d_click/d_view as d_ctr, d_view;


-- rmf $output/raw_result
-- store d_ctr_data into '$output/raw_result';
d_ctr_data = LOAD '$output/raw_result' as (d_conid:chararray,u_category:chararray, categories:chararray, ctr:float, view:float, d_ctr:float, d_view:float);

events_filter = FILTER d_ctr_data by view > 100;

top_content_event = FOREACH ( GROUP events_filter by u_category ){
    data_norm = FOREACH $1 GENERATE d_conid,u_category, categories, ctr, view, d_ctr, d_view, ctr/d_ctr as ctr_norm;
    data_ordered = ORDER data_norm BY ctr_norm desc;
    data_limit = LIMIT data_ordered 10;
    GENERATE FLATTEN(data_limit);
}

u_result = foreach top_content_event GENERATE  u_category as u_category, d_conid, categories, ctr, view, d_ctr, d_view;
rmf $output/user_level_top_content_norm
store u_result into '$output/user_level_top_content_norm';


-- top_user_event = FILTER events_filter by d_view > 200;
-- d_result = ORDER top_user_event BY d_view desc,d_conid, ctr desc;
-- store d_result into '$output/conid_level_result';
