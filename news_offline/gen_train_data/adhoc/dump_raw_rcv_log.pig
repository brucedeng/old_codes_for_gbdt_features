REGISTER './*.jar';
REGISTER './utils.py' USING jython AS myfunc;

%DECLARE ORI_COL "eventtime, act, aid, contentid, appv,brand, ccode, ip, net, model, pf, pid, v"

%DECLARE TYPE_COL "eventtime:long, act:chararray, aid:chararray, contentid:chararray, appv:chararray , brand:chararray, ccode:chararray,ip:chararray, net:chararray, model:chararray, pf:chararray, pid:chararray, v:chararray"

set mapred.create.symlink yes;
set mapred.cache.files $CONFIG#config.json,$CITY_DATA#ip_city.data

DEFINE rcvLoader com.ijinshan.cmcm.rcv.log.loader.rcvLogLoader('$ORI_COL','&', '=');
raw_rcv_log = LOAD '$event' USING rcvLoader AS ($TYPE_COL);

raw_rcv_log = filter raw_rcv_log by 
		v is not null and v!= '' and (int)v>=3 
		and act is not null and act!='' 
		and pid == '11'
		and aid is not null 
		and contentid is not null;

raw_rcv_log = filter raw_rcv_log by act == '1' or act == '2';
--test_log = distinct raw_rcv_log;

-- view_stream = filter raw_rcv_log by act == '1';
view_event_projector = FOREACH  raw_rcv_log {
  GENERATE
    contentid AS content_id,
    aid AS uid,
    myfunc.get_city_name(ip) as city,
    (long)(eventtime/$WND) * $WND as ts,
    (act=='1'? 'view' : 'click') AS event_type;
}

raw_log_dist = foreach (group view_event_projector by (uid,content_id,city,event_type)) {

  times = foreach $1 generate ts;
  r = order times by ts DESC;
  l = limit r 1;
  generate flatten(group) as (uid,content_id,city,event_type), flatten(l) as ts;

};

store raw_log_dist into '$output';

-- pig -Dmapred.job.queue.name=offline -p U_NCAT=15 -p U_NKEY=20 -p D_NCAT=5 -p D_NKEY=10 -p WND=600 -p CITY_DATA=hdfs://mycluster/user/mengchong/tmp/ip_city.data -p CONFIG=hdfs://mycluster/user/mengchong/tmp/config_less.json -p event='s3://iwarehouse/cmnow/news_rcv_log/20151207/*/*.rcv' -p output='/tmp/mengchong/rcv_event_dump/20151207' dump_raw_rcv_log.pig
