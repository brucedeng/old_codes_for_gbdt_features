#!/usr/bin/env python

import sys
import json

if __name__=='__main__':
	overall_cnt = 0.0
	cat_cnt = {}
	for line in sys.stdin:
		data = json.loads(line)
		interest = data['interest']
		overall_cnt += 1
		if interest == '':
			continue
		int_list = interest.split(',')
		for cur_cat in int_list:
			cur_cat = cur_cat.strip()
			if cur_cat not in cat_cnt:
				cat_cnt[cur_cat] = 0.0
			cat_cnt[cur_cat] += 1
	print "overall\t{}".format(overall_cnt)
	for item in cat_cnt:
		print '{}\t{}\t{}'.format(item,cat_cnt[item]/overall_cnt, cat_cnt[item])