REGISTER 'lib/*.jar';
-- REGISTER 'utils.py' USING jython AS myfunc;
-- REGISTER 'interest_utils.py' USING jython AS udf;

-- %default CAT_TREE hdfs://mycluster/tmp/mengchong/cat_tree.txt
-- set mapred.create.symlink yes;
-- set mapred.cache.files $CAT_TREE#cat_tree.txt

raw_rcv_log = LOAD '$INPUT_EVENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate  
    json#'contentid' as content_id,
    json#'aid' as uid,
    json#'ip' as ip,
    (long)(json#'ext'#'eventtime') as eventtime:long,
    json#'pid' as pid,
    json#'scenario'#'level1_type' as level1_type,
    json#'scenario'#'level1' as level1,
    json#'scenario'#'level2' as channelid,
    json#'appv' as appv,
    json#'act' as act;

hot_events = FILTER raw_rcv_log by pid=='11' and level1_type=='1' and level1=='1' and act=='1';
hot_events = FOREACH hot_events GENERATE content_id, uid, ip, (eventtime>1893456000? eventtime/1000 : eventtime ) as eventtime, pid, channelid, act;

events_dist = FOREACH ( group hot_events by (uid, content_id, pid, channelid ) ) {
    clicks = FILTER $1 by act == '2';
    click = COUNT(clicks);
    data = order $1 by eventtime desc;
    r = limit data 1;
    GENERATE FLATTEN(r) as (content_id, uid, ip, eventtime, pid, channelid, act),
        1 as pv, ( COUNT(clicks) > 0? 1.0:0.0 ) as click;
}

raw_content = LOAD '$INPUT_CONTENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_content = foreach raw_content generate  
  json#'item_id' as content_id:chararray,
  json#'related_items' as rel:chararray;

jnd = FOREACH ( join events_dist by content_id, raw_content by content_id ) GENERATE 
    events_dist::uid as uid,
    events_dist::content_id as content_id,
    raw_content::rel as rel;

rel_data = FOREACH jnd GENERATE uid, content_id, rel, ( ( rel is null or rel == '[]' or rel == '') ? 0: 1 ) as covered;

result = FOREACH ( GROUP rel_data all ) GENERATE SUM($1.covered) as covered_cnt, COUNT($1) as all_cnt;
store result into '/tmp/mengchong/rel_article_covg/pv_covered';

uniq_result = FOREACH ( GROUP rel_data by content_id ) {
    data = limit $1 1;
    data_out = FOREACH data GENERATE content_id, covered;
    GENERATE FLATTEN(data_out) as (content_id, covered);
}

result2 = FOREACH ( GROUP uniq_result all ) GENERATE SUM($1.covered) as covered_cnt, COUNT($1) as all_cnt;
store uniq_result into '/tmp/mengchong/rel_article_covg/uniq_contentid';
