-- REGISTER '../*.jar';
-- REGISTER 'cfb_agg.py' USING jython AS cfb;

-- %DECLARE ORI_COL "eventtime, act, aid, contentid, appv,brand, ccode,ip, net, model, pf, pid, v"
-- %DECLARE TYPE_COL "eventtime:long, act:chararray, aid:chararray, contentid:chararray, appv:chararray , brand:chararray, ccode:chararray, ip:chararray, net:chararray, model:chararray, pf:chararray, pid:chararray, v:chararray"

-- set mapred.create.symlink yes;
-- set mapred.cache.files $CONFIG#config.json

raw_cfb = LOAD '$INPUT_CFB' AS (
    ts : long,
    feature_type : chararray,
    feature_name : chararray,
    click_sum : double,
    view_sum : double
);
raw_cfb = FILTER raw_cfb BY ts > 0;
raw_cfb_group = GROUP raw_cfb BY (ts, feature_type, feature_name);
raw_cfb = FOREACH raw_cfb_group GENERATE
    FLATTEN(group) AS (ts, feature_type, feature_name),
    SUM($1.click_sum) AS click_sum,
    SUM($1.view_sum) AS view_sum;

-- raw_cfb_grp = FOREACH ( GROUP raw_cfb by (feature_type, feature_name) ) {
--     tmp_data = order $1 by ts;
--     data = FOREACH tmp_data GENERATE ts, click_sum, view_sum;
--     GENERATE FLATTEN($0) as (feature_type,feature_name), data as data;
-- }

rmf /tmp/mengchong/cfb_join_compare/raw_cfb
store raw_cfb into '/tmp/mengchong/cfb_join_compare/raw_cfb';

nrt_cfb = load '$DUMP_CFB' as (
    features : chararray,
    ts : long,
    view_sum : double,
    click_sum : double
);
nrt_cfb = FILTER nrt_cfb BY ts > 0;
nrt_cfb_format = FOREACH nrt_cfb GENERATE (ts/600*600)-600 as ts, FLATTEN(STRSPLIT(features,'\u0001',2)) as (feature_type:chararray, feature_name:chararray), click_sum, view_sum;

nrt_cfb_group = GROUP nrt_cfb_format BY (ts, feature_type, feature_name);
nrt_cfb = FOREACH nrt_cfb_group GENERATE
    FLATTEN(group) AS (ts, feature_type, feature_name),
    SUM($1.click_sum) AS click_sum,
    SUM($1.view_sum) AS view_sum;

-- nrt_cfb_grp = FOREACH ( GROUP nrt_cfb by (feature_type, feature_name) ) {
--     tmp_data = order $1 by ts;
--     data = FOREACH tmp_data GENERATE ts, click_sum, view_sum;
--     GENERATE FLATTEN($0) as (feature_type,feature_name), data as data;
-- }

rmf /tmp/mengchong/cfb_join_compare/nrt_cfb
store raw_cfb into '/tmp/mengchong/cfb_join_compare/nrt_cfb';

jnd = join raw_cfb by (ts, feature_type, feature_name), nrt_cfb by (ts, feature_type, feature_name);

result = FOREACH jnd GENERATE
    (raw_cfb::ts is null? nrt_cfb::ts : raw_cfb::ts ) as ts,
    (raw_cfb::feature_type is null? nrt_cfb::feature_type : raw_cfb::feature_type ) as feature_type,
    (raw_cfb::feature_name is null? nrt_cfb::feature_name : raw_cfb::feature_name ) as feature_name,
    raw_cfb::click_sum as offline_click,
    raw_cfb::view_sum as offline_view,
    nrt_cfb::click_sum as nrt_click,
    nrt_cfb::view_sum as nrt_view;
    -- raw_cfb::data as offline_data,
    -- nrt_cfb::data as nrt_data;

result = order result by feature_type, feature_name, ts;

rmf /tmp/mengchong/cfb_join_compare/full_jnd
store result into '/tmp/mengchong/cfb_join_compare/full_jnd';
