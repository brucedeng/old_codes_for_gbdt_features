REGISTER 'lib/*.jar';
-- REGISTER 'utils.py' USING jython AS myfunc;
REGISTER 'interest_utils.py' USING jython AS udf;

%default cp_data '/projects/news/cpp/feeder/in_cp_dump/2015121{3,4,5,6,7}/*/*'

raw_content = LOAD '$cp_data' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_content = foreach raw_content generate  
  json#'item_id' as content_id:chararray,
  COUNT(json#'categories') as cat_cnt:int,
  json#'type' as type:chararray,
  (long)json#'publish_time' as publish_time:long,
  (long)json#'update_time' as update_time:long,
  (long)json#'last_modified_time' as edit_date:long,
  json#'categories' as categories:{t1:(y:map[])};

content_filter = FILTER raw_content by edit_date >= 1450224000;

content_id = FOREACH content_filter GENERATE content_id;
result = DISTINCT content_id;
store result into '/tmp/mengchong/distinct_content_id_by_editdate';
-- raw_content = FOREACH raw_content {
--   cat_data = FOREACH categories GENERATE udf.get_l1_category(y#'name') as category;
--   cat_normed = FILTER cat_data by category is not null;
--   GENERATE content_id, cat_cnt, type, publish_time, update_time, cat_normed as categories;
-- }

-- raw_content = FOREACH raw_content GENERATE content_id, cat_cnt, type, publish_time, update_time, 
--     (COUNT(categories) > 0 ? categories:null) as categories;

-- raw_content_distinct = foreach (group raw_content by content_id PARALLEL 30) {
    
--   r = order raw_content by update_time DESC;
--   l = limit r 1;
--   generate flatten(l) as (content_id, cat_cnt, type, publish_time, update_time, categories);
-- };

-- jnd = JOIN events_dist by content_id, raw_content_distinct by content_id PARALLEL 50;

-- events_info = FOREACH jnd GENERATE
--     events_dist::uid as uid,
--     events_dist::content_id as content_id,
--     events_dist::eventtime as eventtime,
--     events_dist::pv as pv,
--     events_dist::click as click,
--     raw_content_distinct::cat_cnt as cat_cnt,
--     raw_content_distinct::categories as categories;

-- declared_interest = load '$INTEREST' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

-- raw_interest = FOREACH declared_interest GENERATE json#'aid' as aid, json#'interest' as interest, json#'time' as time;

-- uniq_interest = FOREACH ( GROUP raw_interest by aid ) {
--     r = ORDER $1 BY  time;
--     l = LIMIT r 1;
--     GENERATE FLATTEN(l) as (aid, interest, time);
-- }

-- user_jnd = JOIN events_info by uid left, uniq_interest by aid; 

-- interest_data = FOREACH user_jnd GENERATE
--     events_info::uid as uid,
--     events_info::content_id as content_id,
--     events_info::eventtime as eventtime,
--     events_info::cat_cnt as cat_cnt,
--     events_info::categories as categories,
--     uniq_interest::interest as interest,
--     uniq_interest::time as interest_ts,
--     events_info::pv as pv,
--     events_info::click as click;

-- -- interest_data = FOREACH interest_data GENERATE uid, content_id, eventtime, cat_cnt, categories, interest, interest_ts, (interest_ts is null? 0 : (interest_ts < eventtime ? 1:0) ) as declared, pv, click;
-- interest_data = FOREACH interest_data GENERATE uid, content_id, eventtime, cat_cnt, categories, interest, interest_ts, (interest is null? 0:1) as declared, pv, click;

-- rmf $OUTPUT
-- store interest_data into '$OUTPUT/raw_result';

-- interest_data2 = FOREACH interest_data GENERATE uid, content_id, eventtime, cat_cnt, categories, 
--     ( declared == 1 ? interest : '' ) as interest:chararray,
--     interest_ts, declared, pv, click;

-- channel_name_dist = FOREACH interest_data2 GENERATE uid, content_id, eventtime, FLATTEN(categories) as channel, interest as interest, declared as declared, pv, click as click;

-- channel_name_agg = FOREACH ( GROUP channel_name_dist by (channel,declared)) GENERATE FLATTEN($0) as (channel,declared), SUM($1.pv) as pv, SUM($1.click) as click;

-- channel_name_agg = FOREACH channel_name_agg GENERATE declared, channel, pv, click/pv as ctr;
-- channel_name_agg = ORDER channel_name_agg BY declared, channel;
-- store channel_name_agg into '$OUTPUT/overall_channel_distribute';

-- channel_declare_agg = FOREACH ( GROUP channel_name_dist by (channel, interest, declared )) GENERATE 
--     FLATTEN($0) as (channel, interest, declared ), SUM($1.pv) as pv, SUM($1.click) as click;
-- channel_declare_agg_output = FOREACH channel_declare_agg GENERATE channel, interest, declared, pv, click/pv as ctr;
-- store channel_declare_agg_output into '$OUTPUT/declare_channel_distribute';

-- -- channel_declare_agg = LOAD '$OUTPUT/declare_channel_distribute' as (channel:chararray, interest:chararray, declared:chararray ,pv:int);
-- -- channel_declare_agg = FILTER channel_declare_agg by declared=='1';

-- result = FOREACH channel_declare_agg GENERATE FLATTEN(udf.cube_interest_str(interest)) as interest, channel, pv, click;
-- grp_data = GROUP result by (interest, channel);

-- result_grp = FOREACH grp_data GENERATE FLATTEN($0) as (interest, channel), SUM($1.pv) as pv, SUM($1.click) as click;
-- result_ctr = FOREACH result_grp GENERATE interest, channel, pv, click/pv as ctr;
-- result_ctr = order result_ctr by interest, channel;

-- -- rmf $OUTPUT/channel_distribute_by_interest
-- store result_grp into '$OUTPUT/channel_distribute_by_interest';

