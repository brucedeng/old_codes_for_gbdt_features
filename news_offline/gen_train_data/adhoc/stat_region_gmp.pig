REGISTER '../lib/*.jar';

%default INPUT_CONTENT 'hdfs://mycluster/projects/news/cpp/feeder/in_cp_dump/{20160624,20160623,20160622,20160621}/*/*/*'
%default gmp_input /projects/news/nrt/india_group_gmp/dump/20160624/2350
%default output /tmp/mengchong/stat_gmp_output3

raw_content = LOAD '$INPUT_CONTENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_content = foreach raw_content generate  
  json#'item_id' as content_id:chararray,
  json#'title_md5' as title_md5:chararray,
  json#'group_id' as groupid:chararray,
  (long)json#'update_time' as update_time:long,
  (1466812800-(long)json#'publish_time')/3600 as doc_age:long,
  json#'language' as language:chararray,
  json#'region' as region:chararray;

raw_content_distinct = foreach (group raw_content by content_id PARALLEL 30) {
	r = order raw_content by update_time DESC;
	l = limit r 1;
	generate flatten(l);

};

gmp_content = load '$gmp_input' as (content_id:chararray,bin_num:int,pv:float,clk:float,agg_pv:float,agg_clk:float,lan:chararray,region:chararray,ts:long);

data_jnd = FOREACH ( JOIN gmp_content by content_id, raw_content_distinct by content_id ) GENERATE 
    gmp_content::content_id as content_id,
    gmp_content::pv as pv,
    gmp_content::clk as clk,
    gmp_content::lan as gmp_lan,
    gmp_content::region as gmp_region,
    raw_content_distinct::l::language as language,
    raw_content_distinct::l::region as region,
    raw_content_distinct::l::doc_age as doc_age;

store data_jnd into '$output/raw_data_joined';

data_jnd_filter = FILTER data_jnd by doc_age <=96;

stat = FOREACH ( GROUP data_jnd_filter by (gmp_lan,gmp_region,language,region)) GENERATE 
    FLATTEN($0) as (gmp_lan,gmp_region,language,region),
    COUNT($1) as cnt;

store stat into '$output/stat_result';