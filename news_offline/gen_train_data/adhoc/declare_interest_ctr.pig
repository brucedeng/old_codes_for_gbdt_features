REGISTER 'lib/*.jar';
-- REGISTER 'utils.py' USING jython AS myfunc;
REGISTER 'interest_utils.py' USING jython AS udf;
REGISTER '../utils.py' USING jython as myfunc;

%default OUTPUT /tmp/mengchong/declared_interest_coverage
%default INPUT_EVENT 's3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click,readtime}/20160225/*/*'
%default INPUT_CONTENT /projects/news/model/experiments/merged_cp_multiple_days/20160220-20160226
%default INTEREST s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall/20160226


%default CAT_TREE hdfs://mycluster/projects/news/model/configs/cat_tree.txt
set mapred.create.symlink yes;
set mapred.cache.files $CAT_TREE#cat_tree.txt

raw_rcv_log = LOAD '$INPUT_EVENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate  
    json#'contentid' as content_id,
    json#'aid' as uid,
    json#'ip' as ip,
    (long)(json#'ext'#'eventtime') as eventtime:long,
    json#'pid' as pid,
    json#'scenario'#'level1_type' as level1_type,
    json#'scenario'#'level1' as level1,
    json#'scenario'#'level2' as channelid,
    json#'appv' as appv,
    (int)json#'ext'#'dwelltime' AS dwelltime:int,
    json#'act' as act;

hot_events = FILTER raw_rcv_log by pid=='11' and level1_type=='1' and level1=='1' and ( act=='1' or act=='2' or act=='3' or act=='4' );
hot_events = FOREACH hot_events GENERATE content_id, uid, ip, (eventtime>1893456000? eventtime/1000 : eventtime ) as eventtime, pid, channelid, act, appv, ((dwelltime is null or dwelltime<0) ? 0 : dwelltime ) as dwelltime;

events_info = FOREACH ( group hot_events by (uid, content_id, pid ) ) {
    clicks = FILTER $1 by act == '2';
    click = COUNT(clicks);
    dwelltime = SUM($1.dwelltime);
    data = order $1 by eventtime desc;
    r = limit data 1;
    GENERATE FLATTEN(r) as (content_id, uid, ip, eventtime, pid, channelid, act, appv, old_dwell),
        1 as pv, ( COUNT(clicks) > 0? 1.0:0.0 ) as click, (dwelltime > 600 ? 600:dwelltime) as dwelltime;
}

-- load cp info
raw_content = LOAD '$INPUT_CONTENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_content = foreach raw_content generate  
  json#'item_id' as content_id:chararray,
  json#'publisher' as publisher:chararray,
  json#'type' as type:chararray,
  (long)json#'publish_time' as publish_time:long,
  (long)json#'update_time' as update_time:long,
  (int)json#'source_type' as source_type:int,
  myfunc.gen_json_str(json) as d_raw:chararray,
  json#'categories' as categories:{t1:(y:map[])},
  json#'entities' as keywords:{t1:(y:map[])};

raw_content_filter = FILTER raw_content by type=='article';
raw_content = FOREACH raw_content_filter GENERATE content_id, publisher, type, publish_time, update_time, d_raw,
    categories as categories:{t1:(y:map[])},
    keywords as keywords;

raw_content_distinct = foreach (group raw_content by content_id PARALLEL 30) {
    
    r = order raw_content by update_time DESC;
    l = limit r 1;
    generate flatten(l);

};

content_info = FOREACH raw_content_distinct {
  categories = FOREACH categories GENERATE
          (chararray)y#'name' AS name:chararray,
          (double)y#'weight' AS weight:double;
  keywords = FOREACH keywords GENERATE
          (chararray)y#'name' AS name:chararray,
          (double)y#'L1_weight' AS weight:double;
  
  categories = filter categories by name is not null and weight is not null;
  keywords = filter keywords by name is not null and weight is not null;

  GENERATE
        content_id AS content_id,
        publisher AS publisher,
        update_time AS update_time,
        categories AS categories,
        keywords AS keywords,
        d_raw AS d_raw,
        type as type,
        publish_time as publish_time;
}

events_with_cp = FOREACH ( JOIN events_info by content_id, content_info by content_id ) GENERATE 
    events_info::content_id as content_id,
    events_info::uid as uid,
    events_info::ip as ip,
    events_info::eventtime as eventtime,
    events_info::pid as pid,
    events_info::channelid as channelid,
    events_info::act as act,
    events_info::appv as appv,
    events_info::pv as pv,
    events_info::click as click,
    events_info::dwelltime as dwelltime,
    content_info::d_raw as d_raw;

declared_interest = load '$INTEREST' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);
raw_interest = FOREACH declared_interest GENERATE json#'aid' as aid, json#'interest' as interest, json#'time' as time;
uniq_interest = FOREACH ( GROUP raw_interest by aid ) {
    r = ORDER $1 BY  time;
    l = LIMIT r 1;
    GENERATE FLATTEN(l) as (aid, interest, time);
}

user_jnd = JOIN events_with_cp by uid left, uniq_interest by aid; 

interest_data = FOREACH user_jnd GENERATE
    events_with_cp::uid as uid,
    events_with_cp::content_id as content_id,
    events_with_cp::eventtime as eventtime,
    events_with_cp::appv as appv,
    uniq_interest::interest as interest,
    uniq_interest::time as interest_ts,
    events_with_cp::pv as pv,
    events_with_cp::click as click,
    events_with_cp::dwelltime as dwelltime,
    events_with_cp::d_raw as d_raw;

interest_data = FOREACH interest_data GENERATE uid, content_id, FLATTEN(udf.get_category_matched_detail(d_raw,interest)) as (category, declare_match, declare_cnt), pv, click, dwelltime;

rmf $OUTPUT/raw_result
store interest_data into '$OUTPUT/raw_result';

interest_dist = FOREACH ( GROUP interest_data by (category, declare_match,declare_cnt) ) GENERATE
    FLATTEN($0) as (category, declare_match,declare_cnt),
    SUM($1.pv) as pv,
    SUM($1.click) as click,
    SUM($1.dwelltime) as dwelltime;

result = FOREACH interest_dist GENERATE category, declare_match,declare_cnt, pv, click,dwelltime, click/pv as ctr, ((float)dwelltime)/pv as dwell_per_pv;

rmf $OUTPUT/format_result
store result into '$OUTPUT/format_result';

interest_data = LOAD '$OUTPUT/raw_result' as (uid:chararray, content_id:chararray, category:chararray, declare_match:int, declare_cnt:int , pv:int, click:float, dwelltime:float);

-- interest_data = FILTER interest_data by declare_cnt>0;
interest_dist_2 = FOREACH ( GROUP interest_data by (category, declare_match) ) GENERATE
    FLATTEN($0) as (category,declare_match),
    SUM($1.pv) as pv,
    SUM($1.click) as click,
    SUM($1.dwelltime) as dwelltime;

result = FOREACH interest_dist_2 GENERATE category, declare_match, pv, click, dwelltime, click/pv as ctr, ((float)dwelltime)/pv as dwell_per_pv;

rmf $OUTPUT/format_result_2
store result into '$OUTPUT/format_result_2';
