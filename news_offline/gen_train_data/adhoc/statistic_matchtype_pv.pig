REGISTER '../lib/*.jar';
REGISTER '../lib/utils.py' USING jython AS myfunc;
REGISTER '../lib/cfb_agg.py' USING jython AS cfb;

%default INPUT_EVENT 's3://com.cmcm.instanews.usw2.prod/data/raw_data/impression/201606{10,11,12}/*/*'

raw_rcv_log = LOAD '$INPUT_EVENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate
    json,
    (json#'app_lan' is null ? 'en' : ( json#'app_lan' MATCHES 'hi.*' ? 'hi' : 'en' )) as app_lan,
    json#'contentid' as content_id:chararray,
    json#'aid' as uid:chararray,
    json#'ip' as ip:chararray,
    json#'servertime_sec' as eventtime,
    json#'pid' as pid:chararray,
    json#'ctype' as ctype,
    (int)json#'ext'#'dwelltime' AS dwelltime:int,
    json#'act' as act,
    json#'upack'#'exp' as exp_id:chararray,
    myfunc.parse_src(json#'cpack') as src:chararray,
    myfunc.parse_rid(json#'cpack') as rid:chararray;

raw_rcv_log = filter raw_rcv_log by pid == '11' and  ( ctype is not null and (ctype=='1' or ctype=='0x01' ) ) and act is not null and ( act == '1' or act == '2' or act=='3' or act=='4' ) and json#'scenario'#'level1_type' =='1' and json#'scenario'#'level1' == '1' and exp_id=='insta_hindi_064';

data_grp = GROUP raw_rcv_log by src;

data_cnt = FOREACH data_grp GENERATE $0 as src, COUNT($1) as cnt;
store data_cnt into '/tmp/mengchong/match_type_statistic2';