#!/bin/bash

dt=`date -d '1 day ago' +%Y%m%d`
lan_list="en_us,en_uk,en_in,hi_in,ta_in,fr_fr,es_es,de_de,it_it,ru_ru,pt_br,en_ca"
lan_list="en_us,en_uk,en_in,hi_in,ta_in,fr_fr,es_es,de_de,it_it,ru_ru,pt_br,en_ca"
output_root=/projects/news/model/up_data/sample_uid
output_file=random_sample_uid.txt
set -x

IFS=,
for lan in $lan_list; do
    pig -p up_dump=/projects/news/user_profile/data/V4/up_json/$lan/$dt -p SAMPLE_RATE=0.004 -p LAN=$lan -p output=$output_root/$dt/$lan random_sample_up.pig
done

for lan in $lan_list; do
    hadoop fs -cat $output_root/$dt/$lan/* 2>/dev/null | head -n 20 > $output_file
done
