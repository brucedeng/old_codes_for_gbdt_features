--%DEFAULT SAMPLE_RATIO 0.02
%DEFAULT WEIGHT_INDEX 3
%DEFAULT SAMPLE_RATIO 1
%DEFAULT LABEL_INDEX 5

set mapred.create.symlink yes;
set mapred.cache.files $CAT_TREE#cat_tree.txt,$model_file#model.dump;

%DECLARE PIG_COL_TYPE "id:chararray,uid:chararray,content_id:chararray,ts:long,label:chararray,city:chararray,publisher:chararray,cp_update_time:chararray,d_raw:chararray,u_raw:chararray,cp_type:chararray,publish_time:chararray,u_age:chararray,u_gender:chararray,cfb_json:chararray,dwelltime:float,listpagedwell:float,declare_match:int,declare_cnt:int"
%DECLARE PIG_COL "id,uid,content_id,ts,label,city,publisher,cp_update_time,d_raw,u_raw,cp_type,publish_time,u_age,u_gender,cfb_json,dwelltime,listpagedwell,declare_match,declare_cnt"

REGISTER *.jar;
REGISTER 'feature_udf.py' USING jython AS myudf;
REGISTER 'interest_utils.py' USING jython AS interest;
define EVAL com.ijinshan.cmcm.gbdt.runtime.udf.GbdtRuntimeMap('model.dump');
--DEFINE HEADER `python append_header.py` ship('append_header.py');

rmf $output
raw = LOAD '$input' AS (
  ${PIG_COL_TYPE}
);

declare_interest = load '$INTEREST' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);
raw_interest = FOREACH declare_interest GENERATE json#'aid' as aid, json#'interest' as interest, json#'time' as time;

uniq_interest = FOREACH ( GROUP raw_interest by aid ) {
    r = ORDER $1 BY  time;
    l = LIMIT r 1;
    GENERATE FLATTEN(l) as (aid, interests, time);
}

data_with_interest = FOREACH ( JOIN raw by uid left outer, uniq_interest by aid ) GENERATE
    $PIG_COL,
    FLATTEN(interest.get_category_matched(d_raw,interests)) as (declare_match, declare_cnt);

-- data_sample = STREAM raw THROUGH MY_SAMPLE as (label:int, nsample, sample_weight, ${PIG_COL_TYPE}); 

data_sample_train = FOREACH data_with_interest {
    doc_age = myudf.gen_doc_age(ts, publish_time);
    time_of_day = myudf.gen_time_of_day(ts);
    day_of_week = myudf.gen_day_of_week(ts);
    static_features = myudf.gen_static_features(u_raw, d_raw, TOMAP('DOC_AGE',doc_age,'TIME_OF_DAY',time_of_day,'DAY_OF_WEEK',day_of_week,'DECLARE_REL',declare_match,'U_DECLARE_CNT',declare_cnt), null);

    cfb_features = myudf.parse_and_calc_cfb_feature(cfb_json);
    GENERATE
        uid, content_id, label, dwelltime, static_features as static_features, cfb_features as cfb_features;
}

score_result = FOREACH data_sample_train GENERATE uid, content_id, label, dwelltime,
    EVAL(myudf.merge_maps(static_features, cfb_features))  as score;

store score_result INTO '$output';

