dt=20160109
out_root=hdfs://mycluster/projects/news/model/tmp/hi_cfb_join_compare_$dt
pig -Dmapred.job.queue.name=offline -p date=$dt -p INPUT_CFB=hdfs://mycluster/projects/news/model/hi_offline_cfb_raw/$dt -p DUMP_CFB="s3://com.cmcm.instanews.usw2.prod/nrt/cfb_multi_lan/cfb/hi/$dt/*" -p output=$out_root compare_raw_cfb.pig
