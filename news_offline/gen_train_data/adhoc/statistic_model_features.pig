
DEFINE STAT `python stat_features.py featmap.txt` ship('lib/stat_features.py','featmap.txt');

raw_training_data = load '$input' as (uid:chararray, content_id:chararray, label:int, dwelltime:float, train_data:chararray);

training_data_format = FOREACH raw_training_data GENERATE train_data;

training_stat = STREAM training_data_format THROUGH STAT as (fid:int, fname:chararray,cnt:float,sum_v:float,sum_square:float,sum_cross:float, sum_label:float,sum_label_square:float);

training_stat_grp = FOREACH ( GROUP training_stat by (fid,fname) ) GENERATE FLATTEN($0) as (fid,fname),
    SUM($1.cnt) as cnt, SUM($1.sum_v) as sum_v, SUM($1.sum_square) as sum_square, SUM($1.sum_cross) as sum_cross, SUM($1.sum_label) as sum_label, SUM($1.sum_label_square) as sum_label_square;

training_stat_grp = FOREACH training_stat_grp GENERATE fid,fname, (cnt <=0? 1.0 : cnt) as cnt, sum_v, sum_square, sum_cross, sum_label, sum_label_square;

training_stat_result = FOREACH training_stat_grp GENERATE fid,fname, 
    FLATTEN({('count', cnt),('mean', sum_v/cnt),('std',(sum_square-sum_v*sum_v/cnt)/cnt),
	    ('correlation',(cnt*sum_cross-sum_label*sum_v)/SQRT((cnt*sum_square-sum_v*sum_v)*(cnt*sum_label_square-sum_label_square*sum_label_square)))}) as (metric,value);

training_stat_result = ORDER training_stat_result BY fid,metric;

rmf $output
store training_stat_result into '$output';
