REGISTER '*.jar';
REGISTER 'utils.py' USING jython AS myfunc;
REGISTER 'cfb_agg.py' USING jython AS cfb;

%DECLARE ORI_COL "eventtime, act, aid, contentid, appv,brand, ccode,ip, net, model, pf, pid, v"
%DECLARE TYPE_COL "eventtime:long, act:chararray, aid:chararray, contentid:chararray, appv:chararray , brand:chararray, ccode:chararray, ip:chararray, net:chararray, model:chararray, pf:chararray, pid:chararray, v:chararray"

-- set mapred.create.symlink yes;
-- set mapred.cache.files $CONFIG#config.json

DEFINE rcvLoader com.ijinshan.cmcm.rcv.log.loader.rcvLogLoader('$ORI_COL','&', '=');
raw_rcv_log = LOAD 's3://iwarehouse/cmnow/news_rcv_log/20151129/*/*.rcv' USING rcvLoader AS ($TYPE_COL);

raw_rcv_log = filter raw_rcv_log by 
		v is not null and v!= '' and (int)v>=3 
		and act is not null and act=='1'
		and pid == '11'
		and aid is not null 
		and contentid is not null;

raw_rcv_log = FOREACH raw_rcv_log GENERATE aid, contentid, myfunc.timestamp2dateformat(eventtime,'%Y%m%d%H') as ts;
raw_rcv_log_filter = FILTER raw_rcv_log by ts is not null;
-- rmf /tmp/mengchong/test_rcv_log_cnt/raw_rcv_log_filter
-- store raw_rcv_log_filter into '/tmp/mengchong/test_rcv_log_cnt/raw_rcv_log_filter';

raw_log = FOREACH ( GROUP raw_rcv_log_filter by (aid, contentid) ) {
    event_ts = ORDER $1 BY ts;
    event_limit = LIMIT event_ts 1;
    GENERATE FLATTEN($1) as (aid, contentid, ts);
}

rmf /tmp/mengchong/test_rcv_log_cnt/ts_stat
store raw_log into '/tmp/mengchong/test_rcv_log_cnt/ts_stat';

ts_grp = FOREACH ( GROUP raw_log by ts ) GENERATE
    $0 as ts,
    COUNT($1) as cnt;

ts_ordered = order ts_grp by ts;

rmf /tmp/mengchong/test_rcv_log_cnt/ts_ordered
store ts_ordered into '/tmp/mengchong/test_rcv_log_cnt/ts_ordered';
