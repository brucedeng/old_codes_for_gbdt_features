REGISTER '../lib/*.jar';
REGISTER '../lib/utils.py' USING jython AS myfunc;


--set mapred.create.symlink yes;
--set mapred.cache.files $CONFIG#config.json,$CITY_DATA#ip_city.data

set default_parallel 200;
raw_rcv_log = LOAD '$event' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate
    json,
    --(json#'app_lan' is null ? 'en' : ( json#'app_lan' MATCHES 'hi.*' ? 'hi' : json#'app_lan' )) as app_lan,
    json#'contentid' as content_id,
    json#'aid' as uid,
    json#'servertime_sec' as eventtime,
    json#'pid' as pid,
    json#'ctype' as ctype,
    json#'country' as country,
    json#'upack'#'exp' as exp,
    myfunc.parse_src(json#'cpack') as src,
    json#'scenario'#'level1_type' as pos_type,
    json#'scenario'#'level1' as pos,
    json#'act' as act;

raw_rcv_log = filter raw_rcv_log by act is not null and pid=='1' 
            and ( ctype is null or ctype=='' or ctype=='1' or ctype=='0x01' ) 
            and ( act == '1' or act == '2' ) 
            --and ( pos_type =='1' or pos_type =='10' ) 
            --and pos == '1' 
            and country == 'RU' 
            and src/16 % 2 == 1; --Explore flag

click_view_data = FOREACH raw_rcv_log GENERATE
    content_id, uid, 
    eventtime,
    (act=='1'? 'view':'click') as event_type,
    pos, pos_type, exp;

log_click_view = FOREACH click_view_data GENERATE
      uid AS uid,
      content_id AS content_id,
      eventtime AS ts,
      event_type AS event_type,
      pos as pos,
      pos_type as pos_type,
      exp as exp;

log_click_view = distinct log_click_view;

-- pre process content
raw_content = LOAD '$cp_dump' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_content = foreach raw_content generate  
  json#'item_id' as content_id:chararray,
  json#'group_id' as groupid:chararray,
  REPLACE(json#'publisher','(\r\n|\n|\t|\r)','') as publisher:chararray,
  (long)json#'publish_time' as publish_time:long,
  (long)json#'update_time' as update_time:long,
  json#'categories' as categories:{t1:(y:map[])};

raw_content_filter = FILTER raw_content by (type=='article');
raw_content = FOREACH raw_content_filter GENERATE content_id, groupid, publisher, publish_time, update_time, categories as categories:{t1:(y:map[])};

raw_content_distinct = foreach (group raw_content by content_id) {
    
    r = order raw_content by update_time DESC;
    l = limit r 1;
    generate flatten(l);

};

content_info = FOREACH raw_content_distinct {
  categories = FOREACH categories GENERATE
          (chararray)y#'name' AS name:chararray,
          (double)y#'weight' AS weight:double;
  
  categories = filter categories by name is not null and weight is not null;

  GENERATE
      content_id AS content_id,
      groupid AS groupid,
      publisher AS publisher,
      update_time AS update_time,
      publish_time as publish_time,
      categories AS categories;
}


log_content_info_join = JOIN log_click_view BY (content_id),
                             content_info BY (content_id);

log_content_info_for = FOREACH  log_content_info_join GENERATE
      log_click_view::content_id AS content_id,
      log_click_view::ts AS ts,
      log_click_view::uid AS uid,
      log_click_view::event_type AS event_type,
      log_click_view::pos AS pos,
      log_click_view::pos_type AS pos_type,
      log_click_view::exp AS exp,
      content_info::publisher as publisher,
      content_info::update_time as update_time,
      content_info::publish_time as publish_time,
      content_info::categories as categories;


res = foreach (group log_content_info_for by (exp, pos_type, pos)) {
    c = filter log_content_info_for by event_type == 'click';
    v = filter log_content_info_for by event_type == 'view';
    
    generate exp, pos_type, pos, COUNT(c) as click, COUNT(v) as view;
}

rmf /tmp/chenkehan/explore_metric
store res into '/tmp/chenkehan/explore_metric/pv_dist';
