REGISTER '../lib/*.jar';
-- REGISTER 'ts_util.py' USING jython AS timefunc;

%default event '/projects/news/data/raw_data/impression/201606{26,25,24,23}/*/*,/projects/news/data/raw_data/click/201606{26,25,24,23}/*/*'
%default gmp /projects/news/nrt/india_group_gmp/dump/20160627/0000
%default output /tmp/mengchong/test_join_gmp_event

-- %DECLARE ORI_COL "eventtime, act, aid, contentid, appv,brand, ccode,ip, net, model, pf, pid, v"
-- %DECLARE TYPE_COL "eventtime:long, act:chararray, aid:chararray, contentid:chararray, appv:chararray , brand:chararray, ccode:chararray, ip:chararray, net:chararray, model:chararray, pf:chararray, pid:chararray, v:chararray"

-- set mapred.create.symlink yes;
-- set mapred.cache.files $CONFIG#config.json
-- set default_parallel 400;

-- raw_cfb = LOAD 's3://com.cmcm.instanews.usw2.prod/nrt/cfb_multi_lan/hi/20160114/{00,01,02,03,04,05,06}*' AS (
--     ts : long,
--     feature_type : chararray,
--     feature_name : chararray,
--     click_sum : double,
--     view_sum : double
-- );
-- raw_cfb = FILTER raw_cfb BY ts >= timefunc.date2timestamp($date,'%Y%m%d') and ts < timefunc.date2timestamp($date,'%Y%m%d')+86400;
-- raw_cfb_group = GROUP raw_cfb BY (feature_type, feature_name);
-- raw_cfb = FOREACH raw_cfb_group GENERATE
--     FLATTEN(group) AS (feature_type, feature_name),
--     SUM($1.click_sum) AS click_sum,
--     SUM($1.view_sum) AS view_sum;

-- conid_cfb = FILTER raw_cfb by feature_type=='D_CONID';

raw_rcv_log = LOAD '$event' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate
    json,
    (json#'app_lan' is null ? 'en' : ( json#'app_lan' MATCHES 'hi.*' ? 'hi' : 'en' )) as app_lan,
    json#'contentid' as content_id,
    json#'aid' as uid,
    json#'ip' as ip,
    json#'servertime_sec' as eventtime,
    json#'pid' as pid,
    json#'ctype' as ctype,
    json#'act' as act;

raw_rcv_log = filter raw_rcv_log by act is not null and pid=='1' and ( ctype is null or ctype=='' or ctype=='1' or ctype=='0x01' ) and ( act == '1' or act == '2' ) and json#'scenario'#'level1_type' =='1' and json#'scenario'#'level1' == '1'; -- and app_lan=='hi';

click_view_data = FOREACH raw_rcv_log GENERATE
    content_id, uid, 
    (act=='1'? 1:0) as pv,
    (act=='2'? 1:0) as click,
    app_lan;

conid_cfb = FOREACH ( group click_view_data by content_id ) GENERATE 
    $0 as content_id, SUM($1.pv) as view_sum, SUM($1.click) as click_sum;

gmp = LOAD '$gmp' as (
    content_id:chararray,
    batch_id:chararray,
    raw_pv:float,
    raw_click:float,
    decay_pv:float,
    decay_click:float,
    language:chararray,
    region:chararray,
    ts:long);

gmp = FILTER gmp by region=='US';

data_jnd =  JOIN conid_cfb by content_id, gmp by content_id ;
result = FOREACH data_jnd GENERATE 
    conid_cfb::content_id as contentid,
    conid_cfb::view_sum as cfb_view,
    conid_cfb::click_sum as cfb_click,
    gmp::raw_pv as gmp_view,
    gmp::raw_click as gmp_click;

store result into '$output';

