#!/usr/bin/env python

import sys
if __name__=='__main__':
	result = {}
	for line in sys.stdin:
		line = line.rstrip('\n')
		fields = line.split('\t')
		(interest, cat, pv, click) = fields
		pv = float(pv)
		click = float(click)
		pure_interest = interest.replace('!', '')
		if interest not in result:
			result[interest] = [0.0,0.0,0.0,0.0]
		if pure_interest == cat:
			result[interest][0] += pv
			result[interest][1] += click
		result[interest][2] += pv
		result[interest][3] += click

	for i in result:
		if i.startswith('!'):
			continue
		baseline = '!' + i
		cur_list = result[i]
		cur_pv = cur_list[0]
		cur_pct = cur_pv / cur_list[2]
		cur_ctr = cur_list[1] / cur_pv
		overall_d_ctr = cur_list[3]/cur_list[2]
		if baseline in result:
			base_list = result[baseline]
			base_pv = base_list[0]
			base_pct = base_pv / base_list[2]
			base_ctr = base_list[1] / base_pv
			base_all_ctr = base_list[3] / base_list[2]
		print '\t'.join([str(k) for k in (i,cur_pv, cur_pct, cur_ctr, overall_d_ctr, base_pv, base_pct, base_ctr, base_all_ctr)])
