REGISTER 'lib/*.jar';
-- REGISTER 'utils.py' USING jython AS myfunc;
-- REGISTER 'data_utils.py' USING jython AS udf;

%default OUTPUT /tmp/mengchong/completeness
%default INPUT_EVENT 's3://com.cmcm.instanews.usw2.prod/data/raw_data/completeness/201603{06,07}/*/*'

raw_rcv_log = LOAD '$INPUT_EVENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate  
    json#'contentid' as content_id,
    json#'aid' as uid,
    json#'ip' as ip,
    (int)(json#'ext'#'completeness') as completeness:int;
    
data_grp = FOREACH ( GROUP raw_rcv_log by completeness ) GENERATE $0 as completeness, COUNT($1) as cnt;

store data_grp into '$OUTPUT';

-- raw_rcv_log = foreach raw_rcv_log generate  
--     json#'contentid' as content_id,
--     json#'aid' as uid,
--     json#'ip' as ip,
--     (int)(json#'ext'#'completeness') as completeness:int,
--     json#'pid' as pid,
--     json#'scenario'#'level1_type' as level1_type,
--     json#'scenario'#'level1' as level1,
--     json#'scenario'#'level2' as channelid,
--     json#'appv' as appv,
--     json#'act' as act;

-- hot_events = FILTER raw_rcv_log by pid=='11' and level1_type=='1' and level1=='1' and channelid is not null and channelid == '29' and ( act=='1' or act=='2' );
-- hot_events = FOREACH hot_events GENERATE content_id, uid, ip, (eventtime>1893456000? eventtime/1000 : eventtime ) as eventtime, pid, channelid, act, appv;
