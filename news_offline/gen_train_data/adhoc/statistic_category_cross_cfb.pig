REGISTER 'lib/*.jar';
REGISTER 'interest_utils.py' USING jython AS udf;

set mapred.create.symlink yes;
set mapred.cache.files $CAT_CONF#cat_conf.txt

raw_cfb = load '$raw_cfb' as (ts:long, feature_type:chararray, feature_name:chararray, click:float, view:float);

raw_cfb = filter raw_cfb by feature_type is not null and feature_type != '';
--and feature_name=='3ee33a470877ffc';

category_cfb = filter raw_cfb by feature_type == 'C_D_U_CATEGORY_CATEGORY';
-- category_cfb = filter raw_cfb by feature_type == 'C_D_U_CONID_CATEGORY';

events = FOREACH ( GROUP category_cfb by (feature_type, feature_name)) GENERATE 
    FLATTEN($0) as (feature_type,feature_name), SUM($1.click) as click, SUM($1.view) as view;

events = FOREACH events GENERATE feature_type, FLATTEN(udf.split_chararray_ctrlb(feature_name)) as (d_category,u_category), click, view;

events = FOREACH events GENERATE feature_type, (chararray)d_category as d_category, (chararray)u_category as u_category, click, view;

result = FOREACH events GENERATE udf.get_l1_category_name(d_category) as d_category,  udf.get_l1_category_name(u_category) as u_category, click, view;

result_sum = FOREACH ( GROUP result by (d_category,u_category) ) GENERATE FLATTEN($0) as (d_category,u_category), SUM($1.click) as click, SUM($1.view) as view;

res_filter = FILTER result_sum by view > 100;

result = FOREACH res_filter GENERATE d_category, u_category, click/view as ctr, view;

rmf $output
store result into '$output';

-- raw_content = LOAD '$cp_dump' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

-- raw_content = foreach raw_content generate  
--   json#'item_id' as content_id:chararray,
--   -- josn#'channel_names' as channel_names:chararray,
--   json#'title_md5' as title_md5:chararray,
--   json#'group_id' as groupid:chararray,
--   REPLACE(json#'publisher','(\r\n|\n|\t|\r)','') as publisher:chararray,
--   json#'type' as type:chararray,
--   (int)(json#'source_type') as source_type:int,
--   (long)json#'publish_time' as publish_time:long,
--   (long)json#'update_time' as update_time:long,
--   (int)json#'image_count' as image_count:int,
--   (int)json#'word_count' as word_count:int,
--   json#'categories' as categories:{t1:(y:map[])},
--   json#'entities' as keywords:{t1:(y:map[])};

-- raw_content_filter = FILTER raw_content by (type=='article' or type=='slideshow' or type=='photostory');
-- raw_content = FOREACH raw_content_filter GENERATE content_id,title_md5, groupid, publisher, type, publish_time, update_time, image_count, word_count,
--     categories as categories:{t1:(y:map[])},
--     keywords as keywords;

-- raw_content_distinct = foreach (group raw_content by content_id) {
--     r = order raw_content by update_time DESC;
--     l = limit r 1;
--     generate flatten(l) as (content_id,title_md5, groupid, publisher, type, publish_time,update_time, image_count, word_count,categories,keywords);
-- };

-- raw_content_distinct = FOREACH raw_content_distinct {
--     cats = FOREACH categories GENERATE (chararray)(y#'name') as name:chararray;
--     GENERATE content_id, update_time, cats as categories:{T:(name:chararray)};
-- }

-- events_jnd = FOREACH ( JOIN events by conid, raw_content_distinct by content_id ) GENERATE 
--     events::feature_type as feature_type,
--     events::conid as d_conid,
--     events::category as u_category,
--     events::click as click,
--     events::view as view,
--     udf.get_category_names(raw_content_distinct::categories) as categories;

--     -- raw_content_distinct::categories;


-- events_filter = FILTER events_jnd by view > 100;
-- events_filter = FOREACH events_filter GENERATE d_conid,u_category, click, view, click/view as ctr, categories;

-- top_content_event = FOREACH ( GROUP events_filter by u_category ){
--     data_ordered = ORDER $1 BY ctr desc;
--     data_limit = LIMIT data_ordered 10;
--     GENERATE FLATTEN(data_limit);
-- }

-- result = foreach top_content_event GENERATE  udf.get_category_names({(u_category)}) as u_category, d_conid, categories, ctr, click, view;

-- rmf $output
-- store result into '$output';

