
-- set default_parallel 400;

-- raw_cfb = LOAD '/tmp/cfb_dump_2016011{1,2}/*/*' as (
raw_cfb = LOAD 's3://com.cmcm.instanews.usw2.prod/nrt/cfb/2016011{1,2}/*/*' AS (
    ts : long,
    feature_type : chararray,
    feature_name : chararray,
    click_sum : double,
    view_sum : double
);
raw_cfb = FILTER raw_cfb BY feature_name=='7291282' or feature_name=='7281690';
store raw_cfb into '/tmp/news_model/dump_raw_cfb3';

