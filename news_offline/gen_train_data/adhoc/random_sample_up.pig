REGISTER '../lib/*.jar';
REGISTER '../lib/hindi_up.py' USING jython AS myfunc;

raw_user_profile = LOAD '$up_dump' as (up_str:chararray);

hindi_user_profile = FOREACH raw_user_profile GENERATE FLATTEN(myfunc.gen_up_id_catlen(up_str)) as (id,u_cat_len,u_kw_len);

hindi_user_profile_bin = foreach hindi_user_profile GENERATE id, u_cat_len, u_kw_len, u_cat_len/10 as bin;
hindi_user_profile_bin = filter hindi_user_profile_bin by  u_kw_len>0;

data_sample = SAMPLE hindi_user_profile_bin $SAMPLE_RATE;

data_output = FOREACH data_sample GENERATE RANDOM() as rnd, '$LAN' as lan, id;
data_order = ORDER data_output BY rnd;
data_output = FOREACH data_output GENERATE lan,id;
rmf $output
store data_output into '$output';

-- up_grp = GROUP hindi_user_profile_bin by bin;

-- sampled_data = FOREACH up_grp {
--     data = FOREACH $1 GENERATE id,u_cat_len, u_kw_len;
--     GENERATE FLATTEN(myfunc.random_sample(data,100)) as (id,u_cat_len, u_kw_len);
-- }

-- rmf $output
-- store sampled_data into '$output';
