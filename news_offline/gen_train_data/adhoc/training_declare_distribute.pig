REGISTER 'lib/*.jar';
REGISTER 'interest_utils.py' USING jython AS udf;

%default input_hi_train 's3://com.cmcm.instanews.usw2.prod/model/experiment/hi_cfb_training_data/2016010{4,5,6,7,8, 9}'
%default output /tmp/news_model/train_declare_result

data = load '$input_hi_train' as (aid:chararray, content_id:chararray, label:int, dwell:float, train_data:chararray, feature1:chararray, feature2:chararray);

data_declared = FOREACH data GENERATE aid, content_id, label, dwell,
    FLATTEN(udf.parse_declare_feature(train_data)) as (declare_rel, declare_cnt);

store data_declared into '$output/raw_aid_declare';

delcare_user_dist = FOREACH data_declared GENERATE aid, declare_cnt;

user_all = FOREACH ( GROUP delcare_user_dist by aid ) {
    uniq_data = LIMIT $1 1;
    GENERATE FLATTEN(uniq_data) as (aid, declare_cnt);
}

store user_all into '$output/aid_uniq_declare_cnt';