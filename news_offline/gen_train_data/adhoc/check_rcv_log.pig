SET job.priority VERY_HIGH;
SET DEFAULT_PARALLEL 50;

%DECLARE ORI_COL "date, act, aid, naid, appv,brand, ccode, net, model, pf, pid, v, dwelltime"

%DECLARE TYPE_COL "date:chararray, act:chararray,aid:chararray, naid:chararray, appv:chararray , brand:chararray, ccode:chararray, net:chararray, model:chararray, pf:chararray, pid:chararray, v:chararray, dwelltime:chararray"

--REGISTER  rcv_log_loader.jar;
REGISTER rcv_log_loader_2.jar;
--REGISTER 'seq_parser.py' USING jython AS myudf;
ship('config.json')

DEFINE rcvLoader com.ijinshan.cmcm.rcv.log.loader.rcvLogLoader('$ORI_COL','&', '=');
raw_rcv_log = LOAD 's3://iwarehouse/cmnow/news_rcv_log/20151121/*/*' USING rcvLoader AS ($TYPE_COL);

raw_rcv_log = filter raw_rcv_log by ( v is not null and v!= '' and (int)v>=3 and act is not null and act!='' and pid == '11');

rmf news_rcv_log;

impression = FILTER raw_rcv_log BY act=='1';
aid_impression = FOREACH impression GENERATE aid;
aid_impression = DISTINCT aid_impression;
aid_impression = FOREACH (GROUP aid_impression ALL) GENERATE COUNT($1);
store impression into 'news_rcv_log/impression';
store aid_impression into 'news_rcv_log/aid_impression';

click = FILTER raw_rcv_log BY act=='2';
aid_click = FOREACH click GENERATE aid;
aid_click = DISTINCT aid_click;
aid_click = FOREACH (GROUP aid_click ALL) GENERATE COUNT($1);
store click into 'news_rcv_log/click';
store aid_click into 'news_rcv_log/aid_click';

readtime = FILTER raw_rcv_log BY act=='4';
aid_readtime = FOREACH readtime GENERATE aid;
aid_readtime = DISTINCT aid_readtime;
aid_readtime = FOREACH (GROUP aid_readtime ALL) GENERATE COUNT($1);
store readtime into 'news_rcv_log/readtime';
store aid_readtime into 'news_rcv_log/aid_readtime';
