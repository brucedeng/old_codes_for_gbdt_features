-- REGISTER '../*.jar';
REGISTER 'ts_util.py' USING jython AS timefunc;

-- %DECLARE ORI_COL "eventtime, act, aid, contentid, appv,brand, ccode,ip, net, model, pf, pid, v"
-- %DECLARE TYPE_COL "eventtime:long, act:chararray, aid:chararray, contentid:chararray, appv:chararray , brand:chararray, ccode:chararray, ip:chararray, net:chararray, model:chararray, pf:chararray, pid:chararray, v:chararray"

-- set mapred.create.symlink yes;
-- set mapred.cache.files $CONFIG#config.json
set default_parallel 400;

raw_cfb = LOAD '$INPUT_CFB' AS (
    ts : long,
    feature_type : chararray,
    feature_name : chararray,
    click_sum : double,
    view_sum : double
);
raw_cfb = FILTER raw_cfb BY ts >= timefunc.date2timestamp($date,'%Y%m%d') and ts < timefunc.date2timestamp($date,'%Y%m%d')+86400;
raw_cfb_group = GROUP raw_cfb BY (feature_type, feature_name);
raw_cfb = FOREACH raw_cfb_group GENERATE
    FLATTEN(group) AS (feature_type, feature_name),
    SUM($1.click_sum) AS click_sum,
    SUM($1.view_sum) AS view_sum;

-- rmf /tmp/mengchong/cfb_join_compare1/raw_cfb
-- store raw_cfb into '/tmp/mengchong/cfb_join_compare1/raw_cfb';

-- nrt_cfb = load '$DUMP_CFB' as (
--     features : chararray,
--     ts : long,
--     view_sum : double,
--     click_sum : double
-- );
-- nrt_cfb = FILTER nrt_cfb BY ts >= timefunc.date2timestamp($date) and ts < timefunc.date2timestamp($date)+86400;
-- nrt_cfb_format = FOREACH nrt_cfb GENERATE (ts/600*600)-600 as ts, FLATTEN(STRSPLIT(features,'\u0001',2)) as (feature_type:chararray, feature_name:chararray), click_sum, view_sum;

nrt_cfb = load '$DUMP_CFB' as (
    ts : long,
    feature_type : chararray,
    feature_name : chararray,
    click_sum : double,
    view_sum : double
);
nrt_cfb_format = FILTER nrt_cfb BY ts >= timefunc.date2timestamp($date,'%Y%m%d') and ts < timefunc.date2timestamp($date,'%Y%m%d')+86400;

nrt_cfb_group = GROUP nrt_cfb_format BY (feature_type, feature_name);
nrt_cfb = FOREACH nrt_cfb_group GENERATE
    FLATTEN(group) AS (feature_type, feature_name),
    SUM($1.click_sum) AS click_sum,
    SUM($1.view_sum) AS view_sum;

-- rmf hdfs://mycluster/projects/news/model/tmp/cfb_join_compare/nrt_cfb
-- store nrt_cfb into 'hdfs://mycluster/projects/news/model/tmp/cfb_join_compare/nrt_cfb';

jnd = join raw_cfb by (feature_type, feature_name) full outer, nrt_cfb by (feature_type, feature_name);

result = FOREACH jnd GENERATE
    (raw_cfb::feature_type is null? nrt_cfb::feature_type : raw_cfb::feature_type ) as feature_type,
    (raw_cfb::feature_name is null? nrt_cfb::feature_name : raw_cfb::feature_name ) as feature_name,
    raw_cfb::click_sum as offline_click,
    raw_cfb::view_sum as offline_view,
    nrt_cfb::click_sum as nrt_click,
    nrt_cfb::view_sum as nrt_view;
    -- raw_cfb::data as offline_data,
    -- nrt_cfb::data as nrt_data;

result = order result by feature_type, feature_name;

rmf $output/full_jnd
store result into '$output/full_jnd';

-- result = load 'hdfs://mycluster/projects/news/model/tmp/cfb_join_compare_$date/full_jnd' as (feature_type:chararray, feature_name:chararray, offline_click:float, offline_view:float, nrt_click:float, nrt_view:float);
conid = filter result by feature_type=='D_CONID';
zero_values = FOREACH conid GENERATE feature_type, feature_name, 
    (offline_click is null? 0 : offline_click) as offline_click,
    (offline_view is null? 0 : offline_view) as offline_view,
    (nrt_click is null? 0 : nrt_click) as nrt_click,
    (nrt_view is null? 0 : nrt_view) as nrt_view;

diff = FOREACH zero_values GENERATE (offline_view-nrt_view) as view_diff;
diff_grp = FOREACH ( GROUP diff by view_diff ) GENERATE $0 as diff, COUNT($1) as cnt;
diff_order = ORDER diff_grp BY cnt desc;

rmf $output/pv_diff
store diff_order into '$output/pv_diff';

conid = filter result by feature_type=='D_CONID' and offline_view is not null and nrt_view is not null;
zero_values = FOREACH conid GENERATE feature_type, feature_name, 
    (offline_click is null? 0 : offline_click) as offline_click,
    (offline_view is null? 0 : offline_view) as offline_view,
    (nrt_click is null? 0 : nrt_click) as nrt_click,
    (nrt_view is null? 0 : nrt_view) as nrt_view;

diff = FOREACH zero_values GENERATE (offline_view-nrt_view) as view_diff;
diff_grp = FOREACH ( GROUP diff by view_diff ) GENERATE $0 as diff, COUNT($1) as cnt;
diff_order = ORDER diff_grp BY cnt desc;

rmf $output/pv_diff_nonempty
store diff_order into '$output/pv_diff_nonempty';
