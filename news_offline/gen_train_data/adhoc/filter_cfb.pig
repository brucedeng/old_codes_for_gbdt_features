REGISTER lib/*.jar

%default DUMP_CFB s3://com.cmcm.instanews.usw2.prod/content/nrt/cfb/20151207/*,s3://com.cmcm.instanews.usw2.prod/content/nrt/cfb/20151208/*
-- %default output /tmp/mengchong/diff_uid_profile

nrt_cfb = load '$DUMP_CFB' as (
    features : chararray,
    ts : long,
    view_sum : double,
    click_sum : double
);
nrt_cfb = FILTER nrt_cfb BY ts > 0;
nrt_cfb_format = FOREACH nrt_cfb GENERATE (ts/600*600) as ts, FLATTEN(STRSPLIT(features,'\u0001',2)) as (feature_type:chararray, feature_name:chararray), click_sum, view_sum;

-- nrt_cfb_group = GROUP nrt_cfb_format BY (ts, feature_type, feature_name);
-- nrt_cfb = FOREACH nrt_cfb_group GENERATE
--     FLATTEN(group) AS (ts, feature_type, feature_name),
--     SUM($1.click_sum) AS click_sum,
--     SUM($1.view_sum) AS view_sum;

nrt_cfb_filter = FILTER nrt_cfb_format by feature_type=='D_CONID' and feature_name=='4940298' and ts >= 1449495600 and ts <= 1449532200;

nrt_order = order nrt_cfb_filter by ts;

store nrt_order into '/tmp/mengcong/nrt_cfb_dump';