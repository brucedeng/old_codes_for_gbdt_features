#!/bin/bash


while getopts "s:e:l:" arg
do
    case $arg in
        s) #start date of events
        event_start=$OPTARG
        ;;
        e) #end date  of event
        event_end=$OPTARG
        ;;
        l) #run data
        app_lan=$OPTARG
        ;;
        ?)  #?
        echo "unkonw argument"
        exit 1
        ;;
    esac
done

if [[ $app_lan == "en" ]]; then
    input_cfb="s3://com.cmcm.instanews.usw2.prod/nrt/cfb"
else
    input_cfb="s3://com.cmcm.instanews.usw2.prod/nrt/cfb_multi_lan/hi"
fi

cur_date=$event_start
while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
do
    cmd="hadoop distcp ${input_cfb}/$cur_date /projects/news/model/training/raw_cfb/${app_lan}_nrt_cfb/$cur_date"
    echo "$cmd"
    eval $cmd
    cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
done
