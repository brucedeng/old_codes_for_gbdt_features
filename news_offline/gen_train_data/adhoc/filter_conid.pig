REGISTER 'lib/*.jar';

%default OUTPUT /tmp/news_model/dump_contentid2
%default INPUT_EVENT 's3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click}/20160107/*/*'

raw_rcv_log = LOAD '$INPUT_EVENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate  
    json#'contentid' as content_id,
    json#'aid' as uid,
    json#'ip' as ip,
    (long)(json#'ext'#'eventtime') as eventtime:long,
    json#'pid' as pid,
    json#'scenario'#'level1_type' as level1_type,
    json#'scenario'#'level1' as level1,
    json#'scenario'#'level2' as channelid,
    json#'appv' as appv,
    json#'act' as act,
    json#'ctype' as ctype,
    json#'app_lan' as app_lan,
    json;

raw_rcv_log = filter raw_rcv_log by act is not null and pid=='11' and ( ctype is null or ctype=='' or ctype=='1' or ctype=='0x01' ) and ( act == '1' or act == '2' ) and json#'scenario'#'level1_type' =='1' and json#'scenario'#'level1' == '1' and app_lan=='en';

id_events = FILTER raw_rcv_log by content_id=='6539984' or
content_id=='6540447' or
content_id=='6540979' or
content_id=='6541326' or
content_id=='6541942' or
content_id=='6543182' or
content_id=='6543596';

events_sort = ORDER id_events BY content_id, eventtime;
rmf $OUTPUT
store events_sort into '$OUTPUT';
