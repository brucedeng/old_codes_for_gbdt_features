-- REGISTER '../*.jar';
REGISTER 'ts_util.py' USING jython AS timefunc;

-- %DECLARE ORI_COL "eventtime, act, aid, contentid, appv,brand, ccode,ip, net, model, pf, pid, v"
-- %DECLARE TYPE_COL "eventtime:long, act:chararray, aid:chararray, contentid:chararray, appv:chararray , brand:chararray, ccode:chararray, ip:chararray, net:chararray, model:chararray, pf:chararray, pid:chararray, v:chararray"

-- set mapred.create.symlink yes;
-- set mapred.cache.files $CONFIG#config.json
set default_parallel 400;

raw_cfb = LOAD 's3://com.cmcm.instanews.usw2.prod/nrt/cfb_multi_lan/hi/20160114/{00,01,02,03,04,05,06}*' AS (
    ts : long,
    feature_type : chararray,
    feature_name : chararray,
    click_sum : double,
    view_sum : double
);
raw_cfb = FILTER raw_cfb BY ts >= timefunc.date2timestamp($date,'%Y%m%d') and ts < timefunc.date2timestamp($date,'%Y%m%d')+86400;
raw_cfb_group = GROUP raw_cfb BY (feature_type, feature_name);
raw_cfb = FOREACH raw_cfb_group GENERATE
    FLATTEN(group) AS (feature_type, feature_name),
    SUM($1.click_sum) AS click_sum,
    SUM($1.view_sum) AS view_sum;

conid_cfb = FILTER raw_cfb by feature_type=='D_CONID';

gmp = LOAD 's3://com.cmcm.instanews.usw2.prod/nrt/gmp_hindi/20160114/0700' as (
    content_id:chararray,
    batch_id:chararray,
    raw_pv:float,
    raw_click:float,
    decay_pv:float,
    decay_click:float,
    ts:long);

jnd = FOREACH ( JOIN conid_cfb by feature_name, gmp by content_id ) GENERATE 
    conid_cfb::feature_name as contentid,
    conid_cfb::view_sum as cfb_view,
    conid_cfb::click_sum as cfb_click,
    gmp::raw_pv as gmp_view,
    gmp::raw_click as gmp_click;

store jnd into '/tmp/test_join_gmp_cfb';

