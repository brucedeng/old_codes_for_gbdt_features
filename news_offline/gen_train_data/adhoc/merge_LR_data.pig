
REGISTER 'lib/*.jar';
train_data = LOAD '$input' USING parquet.pig.ParquetLoader;
merged_data = STORE train_data into '$output' USING parquet.pig.ParquetStorer;
