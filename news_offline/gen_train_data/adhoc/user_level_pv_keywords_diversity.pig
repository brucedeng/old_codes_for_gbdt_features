REGISTER 'lib/*.jar';
-- REGISTER 'utils.py' USING jython AS myfunc;
REGISTER 'interest_utils.py' USING jython AS udf;

%default INPUT_EVENT 's3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click}/201512{16,17}/*/*' 
%default INPUT_CONTENT '/projects/news/cpp/feeder/in_cp_dump/201512{09,10,11,12,13,14,15,16,17}/*/*'
%default OUTPUT '/tmp/mengchong/dump_content_channel_kwd_diversity'
%default CAT_TREE hdfs://mycluster/tmp/mengchong/cat_tree.txt
set mapred.create.symlink yes;
set mapred.cache.files $CAT_TREE#cat_tree.txt

raw_rcv_log = LOAD '$INPUT_EVENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate  
    json#'contentid' as content_id,
    json#'aid' as uid,
    json#'ip' as ip,
    (long)(json#'ext'#'eventtime') as eventtime:long,
    json#'pid' as pid,
    json#'scenario'#'level1_type' as level1_type,
    json#'scenario'#'level1' as level1,
    json#'scenario'#'level2' as channelid,
    (long)(json#'ext'#'requesttime') as requesttime:long,
    (long)(json#'servertime_sec') as servertime:long,
    (chararray)(json#'upack') as upack:chararray,
    json#'appv' as appv,
    json#'act' as act;

hot_events = FILTER raw_rcv_log by pid=='11' and level1_type=='1' and level1=='1' and channelid is not null  and ( act=='1' or act=='2' );
-- and channelid == '29'
hot_events = FOREACH hot_events GENERATE content_id, uid, ip, (eventtime>1893456000? eventtime/1000 : eventtime ) as eventtime, pid, channelid, act, requesttime, servertime, udf.get_exp_id(upack) as expid;

hot_pvs = FILTER hot_events by act == '1';

events_dist = FOREACH ( group hot_pvs by (uid, content_id, pid, channelid, expid ) ) {
    clicks = FILTER $1 by act == '2';
    click = COUNT(clicks);
    data = order $1 by eventtime desc;
    r = limit data 1;
    GENERATE FLATTEN(r) as (content_id, uid, ip, eventtime, pid, channelid, act, requesttime, servertime, expid );
}

raw_content = LOAD '$INPUT_CONTENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_content = foreach raw_content generate  
  json#'item_id' as content_id:chararray,
  COUNT(json#'categories') as cat_cnt:int,
  json#'type' as type:chararray,
  json#'publisher' as publisher:chararray,
  (long)json#'publish_time' as publish_time:long,
  (long)json#'update_time' as update_time:long,
  json#'categories' as categories:{t1:(y:map[])},
  json#'entities' as keywords:{t1:(y:map[])};

raw_content = FOREACH raw_content {
  -- cat_data = FOREACH categories GENERATE udf.get_l1_category(y#'name') as category;
  cat_data = FOREACH categories GENERATE y#'name' as category;
  cat_normed = FILTER cat_data by category is not null;
  kwd_data = FOREACH keywords GENERATE y#'name' as keyword, y#'L1_weight' as weight;
  kwd_data = LIMIT kwd_data 10;
  GENERATE content_id, cat_cnt, type, publish_time, update_time, (COUNT(cat_normed) ==0 ? null : cat_normed ) as categories, publisher, kwd_data as keywords;
}

raw_content = FOREACH raw_content GENERATE content_id, cat_cnt, type, publish_time, update_time, keywords;

raw_content_distinct = foreach (group raw_content by content_id PARALLEL 30) {
  r = order raw_content by update_time DESC;
  l = limit r 1;
  generate flatten(l) as (content_id, cat_cnt, type, publish_time, update_time, keywords);
};

jnd = JOIN events_dist by content_id, raw_content_distinct by content_id PARALLEL 50;

events_info = FOREACH jnd GENERATE
    events_dist::uid as uid,
    events_dist::content_id as content_id,
    events_dist::requesttime as requesttime,
    events_dist::servertime as servertime,
    events_dist::expid as expid,
    events_dist::channelid as channelid,
    raw_content_distinct::cat_cnt as cat_cnt,
    raw_content_distinct::keywords as keywords;

user_grp = GROUP events_info by (uid, channelid, expid);
rmf $OUTPUT
store user_grp into '$OUTPUT/user_grouped';

user_data = FOREACH user_grp {
    cats = FOREACH $1 GENERATE keywords;
    GENERATE FLATTEN($0) as (uid,channelid,expid), FLATTEN(udf.calc_kwd_cnt(cats)) as (uniq_kwd:int, kwd_cnt:int, article_cnt:int);
}

user_data = FOREACH user_data GENERATE uid, channelid, expid, uniq_kwd, kwd_cnt, article_cnt, ((float)(uniq_kwd))/kwd_cnt as uniq_ratio;
-- rmf $OUTPUT/channel_distribute_by_interest
store user_data into '$OUTPUT/user_level_diversity';

result = FOREACH ( GROUP user_data by (channelid,expid) ) GENERATE FLATTEN($0) as (channelid,expid), COUNT($1) as cnt, SUM($1.uniq_kwd) as uniq_kwd, SUM($1.kwd_cnt) as kwd_cnt, SUM($1.article_cnt) as article_cnt, SUM($1.uniq_ratio) as uniq_ratio;

result = FOREACH result generate channelid,expid, cnt, ((float)uniq_kwd)/cnt as uniq_kwd, ((float)kwd_cnt)/cnt as kwd_cnt, ((float)article_cnt)/cnt as article_cnt, ((double)uniq_ratio)/cnt as uniq_ratio;
store result into '$OUTPUT/avg_diversity';
