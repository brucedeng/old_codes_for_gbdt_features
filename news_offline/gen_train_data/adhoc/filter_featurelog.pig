REGISTER '../lib/*.jar';
-- REGISTER 'feature_log_util.py' USING jython AS myfunc;

-- set mapred.create.symlink yes;
-- set mapred.cache.files $FEATMAP_FILE#feature.map
-- set default_parallel 400;
set fs.s3a.connection.maximum 1000;

DEFINE ADD_IDX `python gen_feature_idx.py` ship ('gen_feature_idx.py');

%default INPUT_FEATURE_LOG 's3://com.cmcm.instanews.usw2.prod/model/feature_log/20160623/{03,04,05}/*/*';
%default output /tmp/mengchong/sample_featurelog_event

feature_log = LOAD '$INPUT_FEATURE_LOG' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

feature_log = FOREACH feature_log GENERATE 
    json#'req_id' as rid:chararray,
    json#'prod_id' as pid:chararray,
    json#'predictor_id' as predictor_id:chararray,
    json#'now' as ts:long,
    json#'doc_size' as doc_size:long,
    json#'uid' as uid:chararray,
    -- json#'up' as up:map[],
    FLATTEN(json#'docs') as features;


featurelog_idx = STREAM feature_log THROUGH ADD_IDX as (idx:chararray,rid:chararray,pid:chararray, predictor_id:chararray,ts:long,doc_size:chararray,uid:chararray,features:map[chararray]);
store feature_log into '$output/test_raw';
-- store featurelog_idx into '$output';
target_log = FILTER featurelog_idx by (chararray)(features#'id') == '18466747' and pid=='11';

target_log = ORDER target_log BY ts;

store target_log into '$output/sample';

