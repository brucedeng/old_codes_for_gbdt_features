

input_event="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click}/20151215/*/*"

cur_date=20151212

ftr_start=`date -d "+7 day ago $cur_date" +%Y%m%d`;
ftr_end=$cur_date;
agg_dates=$ftr_start
for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done

input_content_7d="s3://com.cmcm.instanews.usw2.prod/cpp/feeder/{$agg_dates}/*/*/*.data"
input_content_7d="s3://com.cmcm.instanews.usw2.prod/cpp/feeder/related_items-201512070335-201512160335.data"

cmd="pig -Dmapred.job.queue.name=default -p INPUT_EVENT='$input_event' -p INPUT_CONTENT='$input_content_7d' related_item_coverage.pig"
echo $cmd
eval $cmd;

if [ $? != 0 ]; then echo "ERROR: Failed to run pipeline!"; exit -1; fi
cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
