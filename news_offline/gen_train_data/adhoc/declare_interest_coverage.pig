REGISTER 'lib/*.jar';
-- REGISTER 'utils.py' USING jython AS myfunc;
REGISTER 'interest_utils.py' USING jython AS udf;

%default OUTPUT /tmp/mengchong/declared_interest_coverage
%default INPUT_EVENT 's3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click}/201512{11,12,13,14}/*/*'
%default INTEREST s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall/20151217
-- %default CAT_TREE hdfs://mycluster/tmp/mengchong/cat_tree.txt
-- set mapred.create.symlink yes;
-- set mapred.cache.files $CAT_TREE#cat_tree.txt

raw_rcv_log = LOAD '$INPUT_EVENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate  
    json#'contentid' as content_id,
    json#'aid' as uid,
    json#'ip' as ip,
    (long)(json#'ext'#'eventtime') as eventtime:long,
    json#'pid' as pid,
    json#'scenario'#'level1_type' as level1_type,
    json#'scenario'#'level1' as level1,
    json#'scenario'#'level2' as channelid,
    json#'appv' as appv,
    json#'act' as act;

hot_events = FILTER raw_rcv_log by pid=='11' and level1_type=='1' and level1=='1' and channelid is not null and channelid == '29' and ( act=='1' or act=='2' );
hot_events = FOREACH hot_events GENERATE content_id, uid, ip, (eventtime>1893456000? eventtime/1000 : eventtime ) as eventtime, pid, channelid, act, appv;

events_info = FOREACH ( group hot_events by (uid, content_id, pid, channelid ) ) {
    clicks = FILTER $1 by act == '2';
    click = COUNT(clicks);
    data = order $1 by eventtime desc;
    r = limit data 1;
    GENERATE FLATTEN(r) as (content_id, uid, ip, eventtime, pid, channelid, act, appv),
        1 as pv, ( COUNT(clicks) > 0? 1.0:0.0 ) as click;
}

declared_interest = load '$INTEREST' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_interest = FOREACH declared_interest GENERATE json#'aid' as aid, json#'interest' as interest, json#'time' as time;

uniq_interest = FOREACH ( GROUP raw_interest by aid ) {
    r = ORDER $1 BY  time;
    l = LIMIT r 1;
    GENERATE FLATTEN(l) as (aid, interest, time);
}

user_jnd = JOIN events_info by uid left, uniq_interest by aid; 

interest_data = FOREACH user_jnd GENERATE
    events_info::uid as uid,
    events_info::content_id as content_id,
    events_info::eventtime as eventtime,
    events_info::appv as appv,
    uniq_interest::interest as interest,
    uniq_interest::time as interest_ts,
    events_info::pv as pv,
    events_info::click as click;

interest_data = FOREACH interest_data GENERATE uid, content_id, eventtime,appv, interest, interest_ts, (interest_ts is null? 0 : (interest_ts < eventtime ? 1:0) ) as declared, pv, click;
-- interest_data = FOREACH interest_data GENERATE uid, content_id, eventtime, cat_cnt, categories, interest, interest_ts, (interest is null? 0:1) as declared, pv, click;

rmf $OUTPUT
store interest_data into '$OUTPUT/raw_result';

coverage_with_ctr = FOREACH ( GROUP interest_data by (appv, declared)) GENERATE 
    FLATTEN($0) as (appv, declared) , SUM($1.pv) as pv, SUM($1.click) as click;
store coverage_with_ctr into '$OUTPUT/coverage_with_ctr';

coverage = FOREACH ( group coverage_with_ctr by appv ) GENERATE $0 as appv, SUM($1.declared) as declared, COUNT($1) as total_cnt;
store coverage into '$OUTPUT/coverage';

user_interest_data = FOREACH interest_data GENERATE uid, content_id, eventtime,appv, interest, interest_ts, (interest is null? 0:1) as declared, pv, click;
user_level_covg = FOREACH ( GROUP user_interest_data by (appv,uid) ) GENERATE FLATTEN($0) as (appv,uid), ( SUM($1.declared) > 0 ? 1 : 0 ) as declared;

user_covg = FOREACH ( GROUP user_level_covg by appv ) GENERATE $0 as appv, SUM($1.declared) as declared, COUNT($1) as total_cnt;

store user_covg into '$OUTPUT/user_coverage';
