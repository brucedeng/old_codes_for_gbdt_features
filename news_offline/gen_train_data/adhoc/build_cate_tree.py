#!/usr/bin/python
# -*- coding:utf8-*-
# find parent id for each category by the category label
import sys

def Usage(self):
  print "%s cate_list" % (self)
  return 0

def OutputTree(cate_info):
  id_tbl = cate_info["id_tbl"]
  for id in sorted(id_tbl.keys()):
    print "%s\t%s\t%s" % (id, id_tbl[id]["parent"], id_tbl[id]["label"])
  return 0

#去掉yct标注体系前面的字母，标点，0，只保留一个数字格式作为ID
def FormatID(raw_id):
  num_id = raw_id
  pos = raw_id.rfind(":")
  if pos >= 0:
    i = pos + 1
    while i < len(raw_id):
      if raw_id[i] != '0':
        break
      i += 1
    if i < len(raw_id):
      num_id = raw_id[i:]
    else:
      print >>sys.stderr, "invalid category_id format: %s" % (raw_id)
  return num_id 

def ReadCateList(cate_list_file):
  cate_info = {"id_tbl":{}, "label_tbl":{}} #two index, key is cate_id and cate_label
  IN = open(cate_list_file, 'r')
  line = IN.readline()
  while line:
    line = line.rstrip()
    arr = line.split("\t")
    if len(arr) == 2:
      id = FormatID(arr[0].strip())
      label = arr[1].strip().lower()
      if label[-1:] != "/":
        label += "/"
      cate_info["id_tbl"][id] = {"label":label}
      cate_info["label_tbl"][label] = id
    line = IN.readline()
  IN.close()
  return cate_info 

def GetParentLabel(label):
  end = len(label) - 2
  pos = label.rfind("/", 0, end)
  if pos < 0:
    return "-"
  else:
    return label[0:pos+1]

def BuildTree(cate_info):
  id_tbl = cate_info["id_tbl"]
  label_tbl = cate_info["label_tbl"]
  for id in id_tbl:
    label = id_tbl[id]["label"]
    if label == "/":
      id_tbl[id]["parent"] = "root"
      print >>sys.stderr, "Basic category '%s' is useless, please delete it from input_file" % (id)
    else:
      parent_label = GetParentLabel(label)
      while parent_label != "-":
        if parent_label == "/":  #在分类体系中，此分类没有父节点或者间接父节点，所以此分类就作为顶级类目
          id_tbl[id]["parent"] = "root"
          break
        elif parent_label in label_tbl: #如果找到了父节点
          id_tbl[id]["parent"] = label_tbl[parent_label]
          break
        else: #继续查找间接父节点
          #print >>sys.stderr, "Unknown parent_label:%s for %s,%s" % (parent_label, id, label)
          parent_label = GetParentLabel(parent_label)
    if len(id_tbl[id]["parent"]) == 0:
      print id
  return cate_info

def main(argv):
  if len(argv) < 2:
    Usage(argv[0])
    return 0;
  infile = argv[1]
  cate_info = ReadCateList(infile)
  BuildTree(cate_info)   #fill parent_cate_id for each cate 
  OutputTree(cate_info)
  return 0

if __name__ == "__main__":
  main(sys.argv)

