REGISTER '*.jar';
REGISTER 'utils.py' USING jython AS myfunc;
REGISTER 'cfb_agg.py' USING jython AS cfb;
-- REGISTER 'interest_utils.py' USING jython AS interest;

-- ,$CAT_TREE#cat_tree.txt
set default_parallel 400;

%default INPUT_EVENT 's3://com.cmcm.instanews.usw2.prod/data/raw_data/impression/2016010{6,7}/*/*/*,s3://com.cmcm.instanews.usw2.prod/data/raw_data/click/2016010{6,7}/*/*/*';
-- DEFINE rcvLoader com.ijinshan.cmcm.rcv.log.loader.rcvLogLoader('$ORI_COL','&', '=');
-- raw_rcv_log = LOAD '$INPUT_EVENT' USING rcvLoader AS ($TYPE_COL);
-- set mapred.create.symlink yes;
-- set mapred.cache.files hdfs://mycluster/user/news_model/ip_city.data#ip_city.data

-- raw_rcv_log = LOAD '$INPUT_EVENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

-- raw_rcv_log = foreach raw_rcv_log generate
--     json,
--     (json#'app_lan' is null ? 'en' : ( json#'app_lan' MATCHES 'hi.*' ? 'hi' : 'en' )) as app_lan,
--     json#'contentid' as content_id:chararray,
--     json#'aid' as uid:chararray,
--     json#'ip' as ip:chararray,
--     json#'servertime_sec' as eventtime,
--     json#'pid' as pid:chararray,
--     json#'ctype' as ctype,
--     (int)json#'ext'#'dwelltime' AS dwelltime:int,
--     json#'act' as act;

-- raw_rcv_log = filter raw_rcv_log by pid == '11' and  ( ctype is not null and (ctype=='1' or ctype=='0x01' ) ) and act is not null and ( act == '1' or act == '2' ) and json#'scenario'#'level1_type' =='1' and json#'scenario'#'level1' == '1';

-- click_view_data = FOREACH raw_rcv_log GENERATE
--     content_id, uid, 
--     myfunc.get_city_name(ip) as city_name,
--     (long)(eventtime) as original_time,
--     pid as productid,
--     ((dwelltime is null or dwelltime<0) ? 0 : dwelltime ) as dwelltime,
--     (act=='1'? 'view':( act=='2'? 'click':( act=='4'? 'read': (act=='3'? 'listpagedwell':'unknown')))) as event_type;

-- log_click_view = FOREACH click_view_data GENERATE
--       uid AS uid,
--       city_name AS city,
--       (event_type == 'click' ? 1 : 0)  AS click,
--       (event_type == 'view' ? 1 : 0 )  AS view;

-- log_click_view = FOREACH log_click_view GENERATE uid, city, click, ( view < click ? click:view) as view;

-- uid_event_type = FOREACH ( group log_click_view by (uid, city)) GENERATE 
--     FLATTEN($0) as (uid, city),
--     SUM($1.view) as view,
--     SUM($1.click) as click;

-- store uid_event_type into '/tmp/mengchong/uid_city_distribute';
-- city_level_impression = FOREACH ( GROUP uid_event_type by city ) GENERATE 
--     $0 as city,
--     SUM($1.view) as view,
--     SUM($1.click) as click;
-- store city_level_impression into '/tmp/mengchong/city_level_impression';

user_distribute = load '/tmp/mengchong/uid_city_distribute' as (uid:chararray, city:chararray, view:int, click:int);
user_grp = FOREACH ( GROUP user_distribute by uid ) {
  data_filter = filter $1 by city!='unknown';
  data_order = ORDER data_filter BY view desc;
  data_uniq = limit data_order 1;
  pv = SUM($1.view);
  GENERATE FLATTEN(data_uniq) as (uid, city, view, click ), pv as total_pv;
}

city_2 = FOREACH  ( group user_grp by city ) GENERATE 
    $0 as city,
    COUNT($1) as user;

store city_2 into '/tmp/mengchong/city_level_user2';
-- raw_log_dist = foreach (group raw_log by (uid,content_id,productid,city,event_type)){

-- 	times = foreach raw_log generate ts;
-- 	r = order times by ts DESC;
-- 	l = limit r 1;
-- 	generate flatten(group) as (uid,content_id,productid,city,event_type), flatten(l) as ts;

-- };

-- log_click_view = FOREACH raw_log GENERATE
--   ts AS ts,
--   original_ts as original_ts,
--   content_id AS content_id,
--   productid AS productid,
--   uid AS uid,
--   event_type AS event_type,
--   city AS city,
--   1.0 AS expression;

