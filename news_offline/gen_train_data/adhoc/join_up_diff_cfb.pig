REGISTER lib/*.jar

%default diff_data /tmp/mengchong/diff_uid.txt
%default up_dump s3://com.cmcm.instanews.usw2.prod/user_profile/V1/up_json/20151206
%default output /tmp/mengchong/diff_uid_profile

raw_user_profile = LOAD '$up_dump' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_user_profile = FOREACH raw_user_profile GENERATE
     json#'uid' AS uid:chararray,
     (chararray)json#'age' AS age:chararray,
     (chararray)json#'gender' AS gender:chararray,
     json#'categories' AS categories:{t1:(y:map[])},
     json#'keywords' AS keywords:{t1:(y:map[])};

input_diff = LOAD '$diff_data' as (feature_type:chararray, feature_name:chararray, offline_click:float, offline_pv:float, nrt_click:float, nrt_pv:float);

data_jnd = JOIN input_diff by feature_name left, raw_user_profile by uid;

result = FOREACH data_jnd GENERATE
    input_diff::feature_name as uid,
    raw_user_profile::age as age,
    raw_user_profile::gender as gender,
    input_diff::offline_click as offline_click,
    input_diff::offline_pv as offline_pv,
    input_diff::nrt_click as nrt_click,
    input_diff::nrt_pv as nrt_pv;

store result into '$output';