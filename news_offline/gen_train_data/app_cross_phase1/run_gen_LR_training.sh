
train_config_file=$1
source $train_config_file

echo $event_start
echo $event_end

set -e
# test if all needed variables are assigned.
test_var="${PIG_PARAM?} ${TOP_PARAM?}"

if [[ "$run_merge_events" == "true" ]]; then
    # test if all needed variables are assigned.
    test_var="${app_install_user_root?} ${input_cp_template?} ${joined_events_root?} ${input_gmp_template?}"

    cur_date=$event_start

    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do
        if [[ "a$cfb_ignore_date" != "a" ]]; then
            if [[ $cfb_ignore_date -gt $cur_date ]]; then
                cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
                continue
            fi
        fi
        input_event="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click,listpagedwelltime,readtime}/$cur_date/*/*"
        
        #up_path=`hadoop fs -ls $app_install_user_root | tail -n 1`
        #up_date=`date -d 'last wednesday' +%Y%m%d`
        up_date="20160427"
        user_app_install="hdfs://mycluster/projects/news/model/training/app_install_profile/$up_date"

        
        prev_date=`date -d "+1 day ago $cur_date" +%Y%m%d`;
        input_gmp=`printf "$input_gmp_template" "{$prev_date,$cur_date}"`

        ftr_start=`date -d "+4 day ago $cur_date" +%Y%m%d`;
        ftr_end=$cur_date;
        if [[ "a$cp_first_date" != "a" ]]; then
            if [[ $cp_first_date -gt $cur_date ]]; then
                ftr_end=$cp_first_date;
            fi
        fi
        agg_dates=$ftr_start
        for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done

        input_contents=`printf "$input_cp_template" "{$agg_dates}"`

        output="$joined_events_root/$cur_date"

        cmd="pig $PIG_PARAM -p app_lan=$app_lan -p INPUT_EVENT='$input_event' -p INPUT_CONTENT='$input_contents' -p APP_PROFILE='$user_app_install' -p input_gmp='$input_gmp' -p OUTPUT='$output' gen_app_lr_train.pig"
        echo "$cmd"
        eval $cmd;

        if [ $? != 0 ]; then echo "ERROR: Failed to run pipeline!"; exit -1; fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done
fi
