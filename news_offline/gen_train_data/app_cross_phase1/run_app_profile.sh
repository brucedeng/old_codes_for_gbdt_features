while getopts "s:e:" arg
do
    case $arg in
        s) #start date of events
        event_start=$OPTARG
        ;;
        e) #end date  of event
        event_end=$OPTARG
        ;;
        ?)  #?
        echo "unkonw argument"
        exit 1
        ;;
    esac
done


echo $event_start
echo $event_end

PIG_PARAM='-Dmapred.job.queue.name=offline'
run_up="true"

app_fator_param="/home/chenkehan/news_model/home/india_news_ranking/spark/models/hi_20160427-20160503.param"

if [[ "$run_up" == "true" ]]; then
    # test if all needed variables are assigned.
    cur_date=$event_start

    hadoop fs -rm -skipTrash hdfs://mycluster/projects/news/model/app_factor_model/app_factor.param
    hadoop fs -copyFromLocal $app_fator_param hdfs://mycluster/projects/news/model/app_factor_model/app_factor.param 

    while [ $(expr $cur_date + 0) -le $(expr $event_end + 0) ]
    do

        input_app_install="s3://com.cmcm.instanews.usw2.prod/model/infoc_demo/run_install/install_status/$cur_date"
        output="hdfs://mycluster/projects/news/model/training/app_install_profile/$cur_date/inferred"

        cmd="pig $PIG_PARAM -p INPUT_APP_INSTALL=$input_app_install -p APP_FACTOR='hdfs://mycluster/projects/news/model/app_factor_model/app_factor.param' -p OUTPUT='$output' app_install_profile.pig"
        echo "$cmd"
        eval $cmd;

        if [ $? != 0 ]; then echo "ERROR: Failed to run pipeline!"; exit -1; fi
        cur_date=`date -d "+1 day $cur_date" +%Y%m%d`;
    done
fi
