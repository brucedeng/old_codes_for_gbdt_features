REGISTER '../lib/*.jar';
REGISTER '../lib/app_utils.py' USING jython AS app_func;

set default_parallel 400;
set fs.s3a.connection.maximum 1000;

define Quantile datafu.pig.stats.StreamingQuantile('101');
/*app install feature*/
raw_app_install = LOAD 'hdfs://mycluster/projects/news/model/training/app_install_profile/20160427/inferred/*' as (aid:chararray, app_cat:bag{t:tuple(catid:chararray, weight:double)}, inferred:bag{t:tuple(catid:chararray, weight:double)}); 

flat = foreach raw_app_install generate aid, flatten(app_cat) as (catid, weight);
flat = filter flat by weight is not null;

quantile = foreach (group flat by catid) {
    weights = foreach flat generate weight;
    generate group as catid, app_func.tuple2bag(Quantile(weights)) as percentiles:bag{t:(point:float)};
}

rmf /tmp/chenkehan/quantile;
STORE quantile into '/tmp/chenkehan/quantile-text';
STORE quantile into '/tmp/chenkehan/quantile/'USING parquet.pig.ParquetStorer;




