REGISTER '../lib/*.jar';
REGISTER '../lib/utils.py' USING jython AS myfunc;
REGISTER '../lib/hindi_up.py' USING jython AS up_utils;
-- REGISTER 'interest_utils.py' USING jython AS interest;
%default WND 600

set default_parallel 400;
set fs.s3a.connection.maximum 1000;

raw_rcv_log = LOAD '$INPUT_EVENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate
    json,
    (json#'app_lan' is null ? 'en' : ( json#'app_lan' MATCHES 'hi.*' ? 'hi' : 'en' )) as app_lan,
    json#'contentid' as content_id:chararray,
    json#'aid' as uid:chararray,
    json#'ip' as ip:chararray,
    json#'servertime_sec' as eventtime,
    json#'pid' as pid:chararray,
    json#'ctype' as ctype,
    (int)json#'ext'#'dwelltime' AS dwelltime:int,
    json#'act' as act,
    myfunc.parse_rid(json#'cpack') as rid:chararray;

raw_rcv_log = filter raw_rcv_log by pid == '11' and  ( ctype is not null and (ctype=='1' or ctype=='0x01' ) ) and act is not null and ( act == '1' or act == '2' or act=='3' or act=='4' ) and json#'scenario'#'level1_type' =='1' and json#'scenario'#'level1' == '1' and app_lan=='$app_lan';
-- eventtime >= myfunc.date2timestamp('$START_DATE_TIME',$WND,'%Y%m%d%H') and eventtime <= myfunc.date2timestamp('$END_DATE_TIME', $WND,'%Y%m%d%H') and 
click_view_data = FOREACH raw_rcv_log GENERATE
    content_id, uid, rid,
    -- myfunc.get_city_name(ip) as city_name,
    (long)(eventtime) as original_time,
    (long)(eventtime)/$WND * $WND as updatetime,
    pid as productid,
    ((dwelltime is null or dwelltime<0) ? 0 : dwelltime ) as dwelltime,
    (act=='1'? 'view':( act=='2'? 'click':( act=='4'? 'read': (act=='3'? 'listpagedwell':'unknown')))) as event_type;

log_click_view = FOREACH click_view_data GENERATE
      uid AS uid,
      content_id AS content_id,
      rid AS rid,
      original_time as original_ts,
      updatetime AS ts,
      -- city_name AS city,
      event_type AS event_type,
      productid as productid,
      (event_type=='read' ? dwelltime:0) as dwelltime:int,
      (event_type=='listpagedwell' ? dwelltime:0 ) as listpagedwell:int,
      1.0 AS expression;

log_click_view = FILTER log_click_view BY (content_id matches '^\\d+$') and (ts != -1);

raw_event_group = GROUP log_click_view BY (uid, content_id, productid, rid); 

raw_event_out = FOREACH raw_event_group {
    ts = MIN( $1.ts );
    join_ts = ts/$WND * $WND - $WND;
    click_event = FILTER $1 BY (event_type == 'click');
    click_count = COUNT(click_event);
    label = (click_count > 0 ? 1: 0);
    dwelltime = SUM($1.dwelltime);
    listpagedwell = SUM($1.listpagedwell);
    -- city = MIN($1.city);
GENERATE
    $0.uid AS uid,
    $0.content_id AS content_id,
    $0.productid as productid,
    $0.rid AS rid,
    ts AS ts,
    join_ts AS join_ts,
    -- city AS city,
    (dwelltime > 600 ? 600:dwelltime) as dwelltime,
    (listpagedwell > 600 ? 600:listpagedwell) as listpagedwell,
    label AS label;
}

gmp_data = LOAD '$input_gmp' as (content_id:chararray,batch_id:long, orig_pv:float, orig_clk:float, decay_pv:float, decay_clk:float, updatetime:long);
gmp_filter = FILTER gmp_data by orig_pv > 100;
gmp_with_ts = FOREACH gmp_filter GENERATE content_id, updatetime/1000 as ts , decay_clk/decay_pv as ctr;

event_with_gmp = FOREACH ( JOIN raw_event_out by (content_id,join_ts) left outer, gmp_with_ts by (content_id,ts)) GENERATE 
    raw_event_out::uid as uid,
    raw_event_out::content_id AS content_id,
    raw_event_out::productid as productid,
    raw_event_out::rid AS rid,
    raw_event_out::ts AS ts,
    raw_event_out::join_ts AS join_ts,
    raw_event_out::dwelltime as dwelltime,
    raw_event_out::label AS label,
    (gmp_with_ts::ctr is null? -1.0 : gmp_with_ts::ctr) as gmp:float;

raw_event_out_withid = FOREACH event_with_gmp {
    id_tuple = TOTUPLE(uid,content_id,ts,productid,label,rid);
    id = myfunc.get_md5_id(id_tuple);
    GENERATE id as id, uid, content_id, productid, rid, ts, join_ts, dwelltime, label, gmp;
}

-- pre process content
-- raw_content = LOAD '$INPUT_CONTENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_content = LOAD '$INPUT_CONTENT' as (cp_str:chararray);

cp_data = FOREACH raw_content GENERATE FLATTEN(up_utils.gen_cp_cat_kw(cp_str)) as (content_id,update_time,type, categories, keywords);

raw_content_filter = FILTER cp_data by (type=='article' or type=='slideshow' or type=='photostory');

raw_content_distinct = foreach (group raw_content_filter by content_id PARALLEL 30) {
    
	r = order $1 by update_time DESC;
	l = limit r 1;
	generate flatten(l) as (content_id,update_time,type,categories, keywords);
};

/*app install feature*/
app_install_profile = LOAD '$APP_PROFILE' as (aid:chararray,install_profile:bag{t:tuple(gpcat:chararray,weight:double)});

log_content_info_join = JOIN raw_event_out_withid BY (uid) LEFT,
                             app_install_profile BY (aid);

log_content_info_for = FOREACH  log_content_info_join GENERATE
      raw_event_out_withid::uid AS uid,
      raw_event_out_withid::content_id AS content_id,
      raw_event_out_withid::rid AS rid,
      raw_event_out_withid::label AS label,
      raw_event_out_withid::dwelltime as dwelltime,
      app_install_profile::install_profile as u_app_intall_cats,
      raw_event_out_withid::gmp as gmp,
      raw_event_out_withid::ts AS ts,
      raw_event_out_withid::join_ts AS join_ts,
      raw_event_out_withid::productid AS productid;
      -- raw_event_out_withid::city AS city;
      
log_content_user_info_join = JOIN log_content_info_for BY (content_id),
                                  raw_content_distinct BY (content_id);

log_content_user_info = FOREACH log_content_user_info_join GENERATE
        log_content_info_for::uid AS uid,
        log_content_info_for::content_id AS content_id,
        log_content_info_for::rid AS rid,
        log_content_info_for::label AS label,
        log_content_info_for::dwelltime as dwelltime,
        log_content_info_for::u_app_intall_cats as u_app_intall_cats,
        raw_content_distinct::categories as d_cats,
        raw_content_distinct::keywords as d_keywords,
        log_content_info_for::gmp as gmp,
        log_content_info_for::ts as ts,
        log_content_info_for::productid as productid;

rmf $OUTPUT-text
rmf $OUTPUT
STORE log_content_user_info INTO '$OUTPUT-text';
STORE log_content_user_info INTO '$OUTPUT' USING parquet.pig.ParquetStorer;


