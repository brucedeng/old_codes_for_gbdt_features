REGISTER '../lib/*.jar';
REGISTER '../lib/app_utils.py' USING jython AS app_func;
REGISTER '../lib/utils.py' USING jython AS myfunc;

set default_parallel 400;
set fs.s3a.connection.maximum 1000;
set mapred.create.symlink yes;
--set mapred.cache.files hdfs://mycluster/projects/news/model/app_factor_model/hi_20160427-20160503.param#app_factor.param
set mapred.cache.files $APP_FACTOR#app_factor.param


/*app install feature*/
raw_app_install = LOAD '$INPUT_APP_INSTALL' USING AvroStorage (); 
describe raw_app_install;
--(aid:chararray,pkgname:chararray,uptime:int,type:int,country:chararray,gp:chararray,dt:chararray);

raw_app_install = filter raw_app_install by aid is not null and aid != '' and pkgname is not null and pkgname!='' and gp!='' and gp!='game_' and gp is not null;

raw_app_install = foreach raw_app_install generate aid, pkgname, gp, 1 as num;

raw_app_install = distinct raw_app_install PARALLEL 400;

app_install_grp = GROUP raw_app_install by (aid, gp) PARALLEL 400;
data_gp = FOREACH app_install_grp {
    GENERATE FLATTEN($0) as (aid,gp), SUM($1.num) as cnt;
}

app_install_profile = FOREACH ( GROUP data_gp by aid PARALLEL 400){
    data = FOREACH $1 GENERATE gp,cnt;
    GENERATE $0 as aid, 
            myfunc.normalize_with_smooth(data,0,1,50,'L1',5) AS app_cat;
}

--rmf hdfs://mycluster/projects/news/model/training/app_install_profile/$date;
--store app_install_profile into 'hdfs://mycluster/projects/news/model/training/app_install_profile/$date/raw';

inferred = foreach app_install_profile generate aid, app_cat, app_func.transform(app_cat);
--store inferred into 'hdfs://mycluster/projects/news/model/training/app_install_profile/$date/inferred';
rmf $OUTPUT;
STORE inferred into '$OUTPUT';



