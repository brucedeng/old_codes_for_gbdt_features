REGISTER '../lib/*.jar';
REGISTER '../lib/utils.py' USING jython AS myfunc;
REGISTER '../lib/hindi_up.py' USING jython AS up_utils;

set default_parallel 400;
set fs.s3a.connection.maximum 1000;

/*app install feature*/
raw_app_install = LOAD '$INPUT_APP_INSTALL' USING AvroStorage (); 
describe raw_app_install;
--(aid:chararray,pkgname:chararray,uptime:int,type:int,country:chararray,gp:chararray,dt:chararray);

raw_app_install = filter raw_app_install by aid is not null and aid != '' and pkgname is not null and pkgname!='' and gp!='' and gp!='game_' and gp is not null;

raw_app_install = foreach raw_app_install generate aid, pkgname, gp, 1 as num;

raw_app_install = distinct raw_app_install PARALLEL 400;

app_install_grp = GROUP raw_app_install by (aid, gp) PARALLEL 400;
data_gp = FOREACH app_install_grp {
    GENERATE FLATTEN($0) as (aid,gp), SUM($1.num) as cnt;
}

app_install_profile = FOREACH ( GROUP data_gp by aid PARALLEL 400){
    data = FOREACH $1 GENERATE gp,cnt;
    GENERATE $0 as aid, 
            myfunc.normalize_with_smooth(data,0,1,50,'L1',5) AS app_cat;
}

rmf hdfs://mycluster/projects/news/model/training/app_install_profile/$date;
store app_install_profile into 'hdfs://mycluster/projects/news/model/training/app_install_profile/$date';

/*
user_cat = foreach raw_app_install generate aid, gp;
user_cat = distinct user_cat;

cat_count = foreach (group user_cat by gp) generate group, COUNT($1);
store cat_count into 'hdfs://mycluster/projects/news/model/tmp/app_install_cat_count';
*/


