export PATH="/data1/bin/anaconda2/bin:/home/mengchong/news_model/home/bin/:/home/ec2/bin:/home/ec2/bin:/sbin:/bin:/usr/sbin:/usr/bin:$PATH"
export HADOOP_HOME="/usr/lib/hadoop"
now=`date +%Y%m%d_%H%M`
mkdir -p logs
bash -x add_index.sh >> logs/$now.log 2>&1
find logs/ -mtime +2 -print0 | xargs -0 rm -rf
