export PATH="/data1/bin/anaconda2/bin:/home/mengchong/news_model/home/bin/:/home/ec2/bin:/home/ec2/bin:/sbin:/bin:/usr/sbin:/usr/bin:$PATH"
export HADOOP_HOME="/usr/lib/hadoop"
now=`date +%Y%m%d_%H%M`
mkdir -p logs
start_time="$1"
end_time="$2"
start_ts=`date -d "$start_time" +%s`
end_ts=`date -d "$end_time" +%s`

for (( i=start_ts; i<=end_ts; i+=300)); do
	bash -x add_index.sh "`date -d@$i '+%Y-%m-%d %H:%M'`" >> logs/rerun.log 2>&1
done
# find logs/ -mtime +2 -print0 | xargs -0 rm -rf
