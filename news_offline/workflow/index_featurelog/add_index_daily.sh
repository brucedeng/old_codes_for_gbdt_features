
look_forward_batch_number=6
running_tag_folder=tags
hdfs_root_path="s3://com.cmcm.instanews.usw2.prod/model/feature_log_v2"
folder_number=0

export PS4='+${BASH_SOURCE}:${LINENO}:${FUNCNAME[0]}: '

function wait_process() {
    base_file=$1
    for ((i=0;i<40;i++)); do
        if [[ -e $base_file.done ]]; then
            return 0
        fi
        if [[ -e $base_file.running ]]; then
            sleep 30
            continue
        fi
        return 0
    done
    echo "previous process did not stop within 20 minutes"
}

function build_index() {
    subfolder=$1
    shift
    while [[ $# -gt 0 ]]; do
        cur_ts=$1
        shift
        day=`date "-d@$cur_ts" +%Y%m%d`
        hour=`date "-d@$cur_ts" +%H`
        minu=`date "-d@$cur_ts" +%M`
        mkdir -p $running_tag_folder/$folder
        flag_file="$running_tag_folder/$folder/$day$hour$minu"
        if [[ -e $flag_file.done ]]; then
            continue
        fi
        wait_process $flag_file
        touch $flag_file.running
        cmd="hadoop jar /usr/lib/hadoop/lib/hadoop-lzo.jar com.hadoop.compression.lzo.DistributedLzoIndexer -Dmapreduce.job.queuename=offline $hdfs_root_path/$subfolder/$day/$hour/$minu"
        echo "$cmd"
        header="$subfolder-$cur_ts"
        eval $cmd 2>&1 | awk '{print "'$header'",$0}'
        if [[ $? -eq 0 ]]; then
            folder_number=$((folder_number + 1))
        fi
        touch $flag_file.done
        rm -f $flag_file.running
    done
}

today=`date +%s`
now=$((today / 300 * 300 - 1200))
echo "running for $today"

start_ts=`date +%s`

split_name=""
for name in `hadoop fs -ls $hdfs_root_path | awk '{print $6}' | grep "$hdfs_root_path"`; do
    subfolder=`basename $name`
    split_name="$split_name $subfolder"
done

for folder in $split_name; do
    build_index $folder $now $((now - 300)) $((now - 600)) $((now - 900)) &
done

wait

find ${running_tag_folder?}/ -mtime +2 -print0 | xargs -0 rm -rf

end_ts=`date +%s`

interval=$((end_ts - start_ts))
interval=`echo $interval | awk '{print $0/60.0}'`

local_ip=`hostname -i`
cur_ts=`date +%s`
curl -XPOST '127.0.0.1:1988/v1/push' -d '[{"endpoint": "'$local_ip'","metric":"oversea.featurelog.index","timestamp":'$cur_ts',"step":300,"value":'$folder_number',"counterType": "GAUGE","tags": ""}]'
curl -XPOST '127.0.0.1:1988/v1/push' -d '[{"endpoint": "'$local_ip'","metric":"oversea.featurelog.index_time","timestamp":'$cur_ts',"step":300,"value":'$interval',"counterType": "GAUGE","tags": ""}]'
