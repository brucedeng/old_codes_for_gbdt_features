
look_forward_batch_number=6
running_tag_folder=tags
hdfs_root_path="s3://com.cmcm.instanews.usw2.prod/model/feature_log_v2"
folder_number=0

export PS4='+${BASH_SOURCE}:${LINENO}:${FUNCNAME[0]}: '
set -e

function wait_process() {
    base_file=$1
    for ((i=0;i<40;i++)); do
        if [[ -e $base_file.done ]]; then
            return 0
        fi
        if [[ -e $base_file.running ]]; then
            sleep 30
            continue
        fi
        return 0
    done
    echo "previous process did not stop within 20 minutes"
}

function build_index() {
    subfolder=$1

    cur_ts=$2
    day=`date "-d@$cur_ts" +%Y%m%d`
    hour=`date "-d@$cur_ts" +%H`
    minu=`date "-d@$cur_ts" +%M`
    mkdir -p $running_tag_folder/$folder
    flag_file="$running_tag_folder/$folder/$day$hour"
    if [[ -e $flag_file.done ]]; then
        continue
    fi
    # wait_process $flag_file
    touch $flag_file.running
    cmd="./hadoop jar /usr/lib/hadoop/lib/hadoop-lzo.jar com.hadoop.compression.lzo.DistributedLzoIndexer -Dmapreduce.job.queuename=offline $hdfs_root_path/$subfolder/$day/$hour/"
    echo "$cmd"
    header="$subfolder-$cur_ts"
    eval $cmd 2>&1 | awk '{print "'$header'",$0}'
    # sleep 20
    touch $flag_file.done
    rm -f $flag_file.running
}

function get_split_name() {
    split_name=""
    for name in `hadoop fs -ls $hdfs_root_path | awk '{print $6}' | grep "$hdfs_root_path"`; do
        subfolder=`basename $name`
        split_name="$split_name $subfolder"
    done
    echo "$split_name"
}

function write_monitor() {
    monitor_now=$1
    running_ts=$1
    local_ip=`hostname -i`
    monitor_cur_ts=`date +%s`
    interval=$((cur_ts - running_ts))
    interval=$((interval / 60))

    curl -XPOST '127.0.0.1:1988/v1/push' -d '[{"endpoint": "'$local_ip'","metric":"oversea.featurelog.index.delay","timestamp":'$monitor_cur_ts',"step":3600,"value":'$interval',"counterType": "GAUGE","tags": ""}]'
}

start_ts=`date -d "$1" +%s`
cur_ts=$((start_ts / 3600 * 3600))
today=`date +%s`
now=$((today / 3600 * 3600 - 3600))
cur_splits="`get_split_name`"
prev_ts=$now

while [[ 1 -eq 1 ]]; do
    today=`date +%s`
    # today=$((today - 1800))
    now=$((today / 3600 * 3600 - 3600))
    echo "running for `date -d@$cur_ts '+%Y%m%d %H:%M'`"
    if [[ $cur_ts -ge $now ]]; then
        cur_splits=`get_split_name`
        if [[ $now -gt $prev_ts ]]; then
            prev_ts=$now
            write_monitor $now $prev_ts
        fi
        sleep 30
        find ${running_tag_folder?}/ -mtime +2 -print0 | xargs -0 rm -rf
        continue
    fi

    for folder in $cur_splits; do
        while [[ 1 -eq 1 ]]; do
            process_cnt=`find $running_tag_folder -name '*.running' | wc -l`
            if [[ $process_cnt -lt 4 ]]; then
                build_index $folder $cur_ts &
                break
            fi
            sleep 30
            today=`date +%s`
            # today=$((today - 1800))
            now=$((today / 3600 * 3600 - 3600))
            if [[ $now -gt $prev_ts ]]; then
                prev_ts=$now
                write_monitor $now $prev_ts
            fi
        done
    done
    cur_ts=$((cur_ts + 3600))
done



local_ip=`hostname -i`
cur_ts=`date +%s`
curl -XPOST '127.0.0.1:1988/v1/push' -d '[{"endpoint": "'$local_ip'","metric":"oversea.featurelog.index","timestamp":'$cur_ts',"step":300,"value":'$folder_number',"counterType": "GAUGE","tags": ""}]'
curl -XPOST '127.0.0.1:1988/v1/push' -d '[{"endpoint": "'$local_ip'","metric":"oversea.featurelog.index_time","timestamp":'$cur_ts',"step":300,"value":'$interval',"counterType": "GAUGE","tags": ""}]'
