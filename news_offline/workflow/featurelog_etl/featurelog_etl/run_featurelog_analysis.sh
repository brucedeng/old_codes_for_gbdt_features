cd /home/chenkehan/news_model/home/india_news_ranking/news_offline/workflow/featurelog_etl/featurelog_etl 

PIG_PARAM="-Dmapred.job.queue.name=offline"

run_date=`date -d"-2hours now" "+%Y%m%d"`
run_hour=`date -d "-2hours now" "+%H"`
#run_date=`date -d "-1days $cur_date" "+%Y%m%d"`
#retention_date=`date -d "-7days $cur_date" "+%Y%m%d"`


feature_log_path="s3://com.cmcm.instanews.usw2.prod/model/feature_log_v2/{en_in,hi_in,en_us}"
feature_log_result="/projects/news/model/featurelog_analysis"



hadoop fs -rmr -skipTrash $feature_log_result/$run_date/$run_hour

pig_path="/home/chenkehan/news_model/home/india_news_ranking/news_offline/workflow/featurelog_etl/featurelog_etl/pig"

cmd="pig  $PIG_PARAM -p INPUT_FEATURE_LOG='$feature_log_path/$run_date/$run_hour/*' -p OUTPUT='$feature_log_result/$run_date/$run_hour' $pig_path/featurelog_analysis.pig"
echo $cmd
eval $cmd

#cmd2="pig $PIG_PARAM -p INPUT_EVENT='$rcv_log/$run_date/*/*' -p FEATURE_LOG_INPUT='$feature_log_fomatted/$run_date' -p OUTPUT='$feature_log_joined/$run_date'  $pig_path/join_featurelog_event.pig"
#eval $cmd2



