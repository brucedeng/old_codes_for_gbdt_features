set mapred.job.queue.name=default;
set fs.s3.canned.acl=BucketOwnerFullControl;
 
use ${OUTPUT_DATABASE};
ALTER TABLE ${OUTPUT_TABLE} ADD IF NOT EXISTS PARTITION (dt='${DAY}') LOCATION '${DAY}.bz2';
