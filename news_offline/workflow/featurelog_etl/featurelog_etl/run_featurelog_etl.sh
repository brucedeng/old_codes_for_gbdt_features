cd /home/chenkehan/news_model/home/india_news_ranking/news_offline/workflow/featurelog_etl/featurelog_etl 

PIG_PARAM="-Dmapred.job.queue.name=offline"

cur_date=`date -d"now" +%Y%m%d`
run_date=`date -d "-1days $cur_date" "+%Y%m%d"`
retention_date=`date -d "-7days $cur_date" "+%Y%m%d"`


feature_log_path="s3://com.cmcm.instanews.usw2.prod/model/feature_log"
feature_log_fomatted="/projects/news/model/cfb/training/featurelog_etl/formatted"

rcv_log="/projects/news/data/raw_data/{impression,click,listpagedwelltime,readtime}"
feature_log_joined="/projects/news/model/cfb/training/featurelog_etl/joined_with_rcv"


hadoop fs -rmr -skipTrash $feature_log_fomatted/$retention_date
hadoop fs -rmr -skipTrash $feature_log_fomatted/$run_date
#hadoop fs -rmr -skipTrash $feature_log_joined/$retention_date
#hadoop fs -rmr -skipTrash $feature_log_joined/$run_date

pig_path="/home/chenkehan/news_model/home/india_news_ranking/news_offline/workflow/featurelog_etl/featurelog_etl/pig"

cmd="pig  $PIG_PARAM -p INPUT_FEATURE_LOG='$feature_log_path/$run_date/*/*' -p OUTPUT='$feature_log_fomatted/$run_date' $pig_path/featurelog_process.pig"
echo $cmd
eval $cmd

#cmd2="pig $PIG_PARAM -p INPUT_EVENT='$rcv_log/$run_date/*/*' -p FEATURE_LOG_INPUT='$feature_log_fomatted/$run_date' -p OUTPUT='$feature_log_joined/$run_date'  $pig_path/join_featurelog_event.pig"
#eval $cmd2



