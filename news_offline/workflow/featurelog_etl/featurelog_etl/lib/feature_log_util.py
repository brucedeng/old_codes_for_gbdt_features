# -*- coding:utf-8 -*-
import os
import datetime
import time
import math,sys

if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper
    import json
else:
    import com.xhaus.jyson.JysonCodec as json

#// non cfb feature start from 501

feature_logger_map = {"f1":"U_UID_EC","f2":"U_UID_C","f3":"U_UID_COEC","f4":"U_CITY_EC","f5":"U_CITY_C","f6":"U_CITY_COEC","f7":"U_CATEGORY_MEAN_EC","f8":"U_CATEGORY_MEAN_C","f9":"U_CATEGORY_MAX_EC","f10":"U_CATEGORY_MAX_C","f11":"U_CATEGORY_MICRO_MEAN_COEC","f12":"U_CATEGORY_MICRO_MAX_COEC","f13":"U_CATEGORY_MACRO_MEAN_COEC","f14":"U_CATEGORY_MACRO_MAX_COEC","f15":"U_KEYWORD_MEAN_EC","f16":"U_KEYWORD_MEAN_C","f17":"U_KEYWORD_MAX_EC","f18":"U_KEYWORD_MAX_C","f19":"U_KEYWORD_MICRO_MEAN_COEC","f20":"U_KEYWORD_MICRO_MAX_COEC","f21":"U_KEYWORD_MACRO_MEAN_COEC","f22":"U_KEYWORD_MACRO_MAX_COEC","f23":"D_CONID_EC","f24":"D_CONID_C","f25":"D_CONID_COEC","f26":"D_PUBLISHER_EC","f27":"D_PUBLISHER_C","f28":"D_PUBLISHER_COEC","f29":"D_CATEGORY_MEAN_EC","f30":"D_CATEGORY_MEAN_C","f31":"D_CATEGORY_MAX_EC","f32":"D_CATEGORY_MAX_C","f33":"D_CATEGORY_MICRO_MEAN_COEC","f34":"D_CATEGORY_MICRO_MAX_COEC","f35":"D_CATEGORY_MACRO_MEAN_COEC","f36":"D_CATEGORY_MACRO_MAX_COEC","f37":"C_D_U_CONID_CITY_EC","f38":"C_D_U_CONID_CITY_C","f39":"C_D_U_CONID_CITY_COEC","f40":"C_D_U_CONID_CATEGORY_MEAN_EC","f41":"C_D_U_CONID_CATEGORY_MEAN_C","f42":"C_D_U_CONID_CATEGORY_MAX_EC","f43":"C_D_U_CONID_CATEGORY_MAX_C","f44":"C_D_U_CONID_CATEGORY_MICRO_MEAN_COEC","f45":"C_D_U_CONID_CATEGORY_MICRO_MAX_COEC","f46":"C_D_U_CONID_CATEGORY_MACRO_MEAN_COEC","f47":"C_D_U_CONID_CATEGORY_MACRO_MAX_COEC","f48":"C_D_U_PUBLISHER_CITY_EC","f49":"C_D_U_PUBLISHER_CITY_C","f50":"C_D_U_PUBLISHER_CITY_COEC","f51":"C_D_U_PUBLISHER_AGE_EC","f52":"C_D_U_PUBLISHER_AGE_C","f53":"C_D_U_PUBLISHER_AGE_COEC","f54":"C_D_U_PUBLISHER_GENDER_EC","f55":"C_D_U_PUBLISHER_GENDER_C","f56":"C_D_U_PUBLISHER_GENDER_COEC","f57":"C_D_U_CATEGORY_CITY_MEAN_EC","f58":"C_D_U_CATEGORY_CITY_MEAN_C","f59":"C_D_U_CATEGORY_CITY_MAX_EC","f60":"C_D_U_CATEGORY_CITY_MAX_C","f61":"C_D_U_CATEGORY_CITY_MICRO_MEAN_COEC","f62":"C_D_U_CATEGORY_CITY_MICRO_MAX_COEC","f63":"C_D_U_CATEGORY_CITY_MACRO_MEAN_COEC","f64":"C_D_U_CATEGORY_CITY_MACRO_MAX_COEC","f65":"C_D_U_CATEGORY_AGE_MEAN_EC","f66":"C_D_U_CATEGORY_AGE_MEAN_C","f67":"C_D_U_CATEGORY_AGE_MAX_EC","f68":"C_D_U_CATEGORY_AGE_MAX_C","f69":"C_D_U_CATEGORY_AGE_MICRO_MEAN_COEC","f70":"C_D_U_CATEGORY_AGE_MICRO_MAX_COEC","f71":"C_D_U_CATEGORY_AGE_MACRO_MEAN_COEC","f72":"C_D_U_CATEGORY_AGE_MACRO_MAX_COEC","f73":"C_D_U_CATEGORY_GENDER_MEAN_EC","f74":"C_D_U_CATEGORY_GENDER_MEAN_C","f75":"C_D_U_CATEGORY_GENDER_MAX_EC","f76":"C_D_U_CATEGORY_GENDER_MAX_C","f77":"C_D_U_CATEGORY_GENDER_MICRO_MEAN_COEC","f78":"C_D_U_CATEGORY_GENDER_MICRO_MAX_COEC","f79":"C_D_U_CATEGORY_GENDER_MACRO_MEAN_COEC","f80":"C_D_U_CATEGORY_GENDER_MACRO_MAX_COEC","f81":"D_KEYWORD_MEAN_EC","f82":"D_KEYWORD_MEAN_C","f83":"D_KEYWORD_MAX_EC","f84":"D_KEYWORD_MAX_C","f85":"D_KEYWORD_MICRO_MEAN_COEC","f86":"D_KEYWORD_MICRO_MAX_COEC","f87":"D_KEYWORD_MACRO_MEAN_COEC","f88":"D_KEYWORD_MACRO_MAX_COEC","f89":"C_D_U_KEYWORD_CATEGORY_MEAN_EC","f90":"C_D_U_KEYWORD_CATEGORY_MEAN_C","f91":"C_D_U_KEYWORD_CATEGORY_MAX_EC","f92":"C_D_U_KEYWORD_CATEGORY_MAX_C","f93":"C_D_U_KEYWORD_CATEGORY_MICRO_MEAN_COEC","f94":"C_D_U_KEYWORD_CATEGORY_MICRO_MAX_COEC","f95":"C_D_U_KEYWORD_CATEGORY_MACRO_MEAN_COEC","f96":"C_D_U_KEYWORD_CATEGORY_MACRO_MAX_COEC","f97":"M_D_U_KEYWORD_KEYWORD_MEAN_EC","f98":"M_D_U_KEYWORD_KEYWORD_MEAN_C","f99":"M_D_U_KEYWORD_KEYWORD_MAX_EC","f100":"M_D_U_KEYWORD_KEYWORD_MAX_C","f101":"M_D_U_KEYWORD_KEYWORD_MICRO_MEAN_COEC","f102":"M_D_U_KEYWORD_KEYWORD_MICRO_MAX_COEC","f103":"M_D_U_KEYWORD_KEYWORD_MACRO_MEAN_COEC","f104":"M_D_U_KEYWORD_KEYWORD_MACRO_MAX_COEC","f501":"U_CAT_LEN","f502":"U_KW_LEN","f503":"U_AGE_1","f504":"U_AGE_2","f505":"U_GENDER","f506":"TIME_OF_DAY","f507":"DAY_OF_WEEK","f508":"PROD_ID_3","f509":"PROD_ID_5","f510":"WORD_COUNT","f511":"DOC_AGE","f512":"IMAGE_COUNT","f513":"NEWSY_SCORE","f514":"D_CAT_LEN","f515":"D_KW_LEN","f516":"CAT_REL","f517":"KW_REL","f105":"D_TITLEMD5_EC","f106":"D_TITLEMD5_C","f107":"D_TITLEMD5_COEC","f108":"C_D_U_TITLEMD5_CITY_EC","f109":"C_D_U_TITLEMD5_CITY_C","f110":"C_D_U_TITLEMD5_CITY_COEC","f111":"C_D_U_TITLEMD5_AGE_EC","f112":"C_D_U_TITLEMD5_AGE_C","f113":"C_D_U_TITLEMD5_AGE_COEC","f114":"C_D_U_TITLEMD5_GENDER_EC","f115":"C_D_U_TITLEMD5_GENDER_C","f116":"C_D_U_TITLEMD5_GENDER_COEC","f117":"C_D_U_TITLEMD5_CATEGORY_MEAN_EC","f118":"C_D_U_TITLEMD5_CATEGORY_MEAN_C","f119":"C_D_U_TITLEMD5_CATEGORY_MAX_EC","f120":"C_D_U_TITLEMD5_CATEGORY_MAX_C","f121":"C_D_U_TITLEMD5_CATEGORY_MICRO_MEAN_COEC","f122":"C_D_U_TITLEMD5_CATEGORY_MICRO_MAX_COEC","f123":"C_D_U_TITLEMD5_CATEGORY_MACRO_MEAN_COEC","f124":"C_D_U_TITLEMD5_CATEGORY_MACRO_MAX_COEC","f125":"D_GROUPID_EC","f126":"D_GROUPID_C","f127":"D_GROUPID_COEC","f128":"C_D_U_GROUIDTITLEMD5_CITY_EC","f129":"C_D_U_GROUPID_CITY_C","f130":"C_D_U_GROUPID_CITY_COEC","f131":"C_D_U_GROUPID_AGE_EC","f132":"C_D_U_GROUPID_AGE_C","f133":"C_D_U_GROUPID_AGE_COEC","f134":"C_D_U_GROUPID_GENDER_EC","f135":"C_D_U_GROUPID_GENDER_C","f136":"C_D_U_GROUPID_GENDER_COEC","f137":"C_D_U_GROUPID_CATEGORY_MEAN_EC","f138":"C_D_U_GROUPID_CATEGORY_MEAN_C","f139":"C_D_U_GROUPID_CATEGORY_MAX_EC","f140":"C_D_U_GROUPID_CATEGORY_MAX_C","f141":"C_D_U_GROUPID_CATEGORY_MICRO_MEAN_COEC","f142":"C_D_U_GROUPID_CATEGORY_MICRO_MAX_COEC","f143":"C_D_U_GROUPID_CATEGORY_MACRO_MEAN_COEC","f144":"C_D_U_GROUPID_CATEGORY_MACRO_MAX_COEC","f219":"C_D_U_PUBLISHER_UID_C","f220":"C_D_U_PUBLISHER_UID_COEC","f221":"C_D_U_PUBLISHER_UID_EC","f222":"C_D_U_CATEGORY_UID_MACRO_MAX_COEC","f223":"C_D_U_CATEGORY_UID_MACRO_MEAN_COEC","f224":"C_D_U_CATEGORY_UID_MAX_C","f225":"C_D_U_CATEGORY_UID_MAX_EC","f226":"C_D_U_CATEGORY_UID_MEAN_C","f227":"C_D_U_CATEGORY_UID_MEAN_EC","f228":"C_D_U_CATEGORY_UID_MICRO_MAX_COEC","f229":"C_D_U_CATEGORY_UID_MICRO_MEAN_COEC","f230":"C_D_U_KEYWORD_UID_MACRO_MAX_COEC","f231":"C_D_U_KEYWORD_UID_MACRO_MEAN_COEC","f232":"C_D_U_KEYWORD_UID_MAX_C","f233":"C_D_U_KEYWORD_UID_MAX_EC","f234":"C_D_U_KEYWORD_UID_MEAN_C","f235":"C_D_U_KEYWORD_UID_MEAN_EC","f236":"C_D_U_KEYWORD_UID_MICRO_MAX_COEC","f237":"C_D_U_KEYWORD_UID_MICRO_MEAN_COEC"}

@outputSchema("rid:chararray")
def parse_rid(cpack):
    if cpack is None:
        return ''
    if 'des' not in cpack:
        return ''
    str_data = cpack['des']
    data = str_data.split('|')
    data_map = {}
    for item in data:
        kv_list = item.split('=',1)
        if len(kv_list) == 2:
            data_map[kv_list[0]] = kv_list[1]
    if 'rid' in data_map:
        return data_map['rid']
    return ''

reserved_keys = set(["id","score","gbdt_score"])

@outputSchema("features:map[]")
def format_feature_log(features):
    if features is None:
        return None
    result = {}
    for k in features:
        if k in reserved_keys:
            continue
        if k not in feature_logger_map:
            continue
        result[feature_logger_map[k]] = float(features[k])

    return result

no_zero_ftrs = ["CAT_REL","KW_REL"]
@outputSchema("features:bag{t:(fname:chararray,fvalue:float)}")
def format_feature_log_bag(features,up):
    if features is None:
        return None
    result = []
    for k in features:
        if k in reserved_keys:
            continue
        if k not in feature_logger_map:
            continue
        if feature_logger_map[k] in no_zero_ftrs and float(features[k]) <= 0:
            #print >> sys.stderr, feature_logger_map[k], features[k]
            continue

        result.append((feature_logger_map[k],float(features[k])))

    if len(up.get('kw',[])) > 0:
        result.append(('has_kw',len(up.get('kw',[]))))

    if len(up.get('cg',[])) > 0:
        result.append(('has_cat',len(up.get('cg',[]))))

    return result


@outputSchema("fstat:{T:(fname:chararray,cnt:int,v:float,v_square:float)}")
def stat_feature_log(features,up):
    if features is None:
        return []
    result = []
    for k in features:
        if k in reserved_keys:
            continue
        if k not in feature_logger_map:
            continue
        v = float(features[k])
        if v > 0:
            cnt = 1
        else:
            cnt = 0
        result.append((feature_logger_map[k],cnt,v,v*v))
        # result[feature_logger_map[k]] = features[k]
    return result

@outputSchema("feature_coverage:bag{t:tuple(fname:chararray,fvalue:float)}")
def stat_feature_coverage(pv_cnt,ftr_cnt):
    result = []
    pv = pv_cnt[0][0]
    for ftr in ftr_cnt:
        result.append((ftr[0],float(ftr[1])/pv))

    return result


def get_feature_lists():
    #load feature map file
    if get_feature_lists._global_feature_map is None:
        feature_map_file = 'feature.map'
        f3 = open(feature_map_file, 'r')
        feature_map = {}
        feature_list = []
        for line in f3:
            line = line.rstrip('\n')
            if line == '':
                continue
            fid, fname, ftype = line.split('\t')
            if fid.startswith('#'):
                fid = fid.replace('#','').strip()
            feature_map[fname] = fid
            feature_list.append(fname)
        get_feature_lists._global_feature_map = feature_map
        get_feature_lists._global_feature_list = feature_list
        f3.close()
    return get_feature_lists._global_feature_list, get_feature_lists._global_feature_map

get_feature_lists._global_feature_map = None
get_feature_lists._global_feature_list = None

@outputSchema("features:map[]")
def merge_features(feature1, feature2):
#user feauter1 in there is collision
    #print >> sys.stderr, str(feature1)

    if feature1 == None:
        feature1 = {}

    for k in feature2:
        if k not in feature1:
            feature1[k] = feature2[k]

    return feature1


@outputSchema("features:chararray")
def format_train_data(label, log_features, gen_features):
#use gen_features to override log_features
    out_put = []
    out_put.append(str(label))

    features = merge_features(gen_features, log_features)

    print >> sys.stderr, str(get_feature_lists())

    feature_list, feature_map = get_feature_lists()
    for feature_name in feature_list:
        if feature_name in features:
            fvalue = features[feature_name]

            if fvalue is None or ( type(fvalue) == type("") and fvalue==""):
                continue

            fid = feature_map[feature_name]
            out_put.append(fid + ":" + str(fvalue))

    return ' '.join(out_put)

@outputSchema("b:{t:(rid:chararray,pid:chararray,predictor_id:chararray,ts:long,doc_size:long,uid:chararray,up:map[],features:[])}")
def parse_json_featurelog(line):
    try:
        data_map = json.loads(line)
    except:
        print "error parsing json data."
        print line
        return [(None,None,None,None,None,None,None,None)]

    docs = data_map.get('docs')

    result = [(None,None,None,None,None,None,None,None)]
    if docs is not None:
        for item in docs:
            result.append((data_map.get('req_id',''),data_map.get('prod_id'),data_map.get('predictor_id'),data_map.get('now'),data_map.get('doc_size'),data_map.get('uid'),data_map.get('up'),item))

    return result
    # return (data_map.get('req_id',''),data_map.get('prod_id'),data_map.get('predictor_id'),data_map.get('now'),data_map.get('doc_size'),data_map.get('uid'),data_map.get('up'),docs)

@outputSchema("f:[]")
def json_to_map(data):
    try:
        result = json.loads(data)
        return result
    except:
        return {}

@outputSchema("flag:chararray")
def filter_expired(current_ts, prev_ts, expired_ts):
    if prev_ts is None or current_ts is None or expired_ts is None:
        return '0'
    if long(current_ts) - long(prev_ts) <= long(expired_ts):
        return '1'
    else:
        return '0'

@outputSchema("flag:chararray")
def filter_date(current_ts, prev_ts):
    if prev_ts is None or current_ts is None :
        return '0'
    if long(current_ts) - long(prev_ts) >= 0:
        return '1'
    else:
        return '0'

@outputSchema("flag:chararray")
def filter_flag(filter_field, flag):
    if filter_field is None:
        return '0'
    try:
        filter_field = str(filter_field)
        if filter_field == flag:
            return '1'
        else:
            return '0'
    except:
        return '0'

# preprocess log udf
@outputSchema("interval:int")
def get_interval(current_ts, prev_ts, wnd):
    if prev_ts is None or current_ts is None:
        return 0
    return (current_ts - prev_ts)/(wnd)

@outputSchema("power:double")
def power(base, n):
    if base is None or n is None:
        return None
    return float(base) ** float(n)

@outputSchema("date:chararray")
def substring(date_string, start, end):
    if date_string is None or date_string == '':
        return None
    if start >= end:
        return None
    return date_string[start:end]

@outputSchema("timestamp:long")
def date2timestamp(date_str, wnd, format_str='%Y%m%d%H%M'):
    if date_str is None  or format_str is None:
        return None
    try:
        date_str = str(date_str)
        timestamp = int(time.mktime(datetime.datetime.strptime(date_str, format_str).timetuple()))
        timestamp = timestamp/long(wnd) * long(wnd)
        return timestamp
    except:
        return None

@outputSchema("interval:int")
def diff_date(current_date, prev_date):
    if current_date is None or len(current_date) < 10 or prev_date is None or len(prev_date) < 10:
        return 0
    try:
        current_timestamp = int(time.mktime(datetime.datetime.strptime(current_date, '%Y%m%d%H%M').timetuple()))
        prev_timestamp = int(time.mktime(datetime.datetime.strptime(prev_date, '%Y%m%d%H%M').timetuple()))
        diff_timestamp = current_timestamp - prev_timestamp

        return diff_timestamp/600
    except:
        return 0


@outputSchema("date:chararray")
def date2date10min(date_string):
    if date_string is None or len(date_string) < 10:
        return None
    try:
        timestamp = int(time.mktime(datetime.datetime.strptime(date_string, '%Y-%m-%d %H:%M:%S').timetuple()))
        timestamp = (timestamp/600) * 600
        return datetime.datetime.fromtimestamp(timestamp).strftime('%Y%m%d%H%M')
    except:
        return None

@outputSchema("date:chararray")
def timestamp2date(timestamp):
    if timestamp is None or len(timestamp) < 10:
        return None
    try:
        timestamp = long(timestamp)
        return datetime.datetime.fromtimestamp(timestamp).strftime('%Y%m%d%H%M')
    except:
        return None

@outputSchema("date:chararray")
def timestamp2dateformat(timestamp,format_str='%Y%m%d%H'):
    try:
        timestamp = long(timestamp)
        return datetime.datetime.fromtimestamp(timestamp).strftime(format_str)
    except:
        return None

@outputSchema("hot:int")
def get_hot(rank_type):
    if rank_type is None or rank_type == '':
        return None
    try:
        rank_type_int = int(rank_type)
        hot_flag = rank_type_int &0x1
        return hot_flag
    except:
        return None

@outputSchema("channel:chararray")
def get_channel(rank_type):
    if rank_type is None or rank_type == '':
        return -1
    try:
        rank_type_int = int(rank_type)
        channel_flag = (rank_type_int >>1) & 0xff
        return global_channel_dict[channel_flag]
    except:
        return 'B102'

@outputSchema("b:{t:(content_id:chararray,cid:chararray,eventtime:chararray,requesttime:chararray,ranktypes:chararray)}")
def extract_view_fields(input_list):
    if input_list is None or len(input_list) <= 0 :
        return None
    item_length = len(input_list[0])

    out_bag = []
    try:
        for index in xrange(0,item_length):
            inner_list = []
            for item in input_list:
                if len(item) <= index:
                    inner_list.append(item[len(item) - 1][0].strip())
                else:
                    inner_list.append(item[index][0].strip())
            out_bag.append(tuple(inner_list))
    except:
        pass
    return out_bag

#static feature udf
@outputSchema("time_of_day:int")
def time_of_day(event_time):
    if event_time is None:
        return -1

    time_of_day  = time.localtime(event_time).tm_hour
    return time_of_day

@outputSchema("time_of_week:int")
def day_of_week(event_time):
    if event_time is None:
        return -1

    day_of_week = time.strftime("%w", time.localtime(event_time));
    return day_of_week

@outputSchema("b:bag{t:tuple(feature_name:chararray, score:double)}")
def top(list,NReserved):
    if list is None:
        return None
    NReserved = int(NReserved)
    return list[:NReserved]

@outputSchema("b:bag{t:tuple(feature_name:chararray, score:double)}")
def normalize(list, fea_idx, score_idx, NReserved, style):
    if list is None:
        return None

    NReserved = int(NReserved)
    positive_list = filter(lambda elem: elem[score_idx] > 0.0, list)
    notnone_list = filter(lambda elem: elem[score_idx] is not None, positive_list)
    list = sorted(notnone_list, key=lambda elem:elem[score_idx], reverse=True)
    list = list[:NReserved]

    sum = 0;
    for item in list:
        if style == 'L0':
            sum = 1
        if style == 'L1':
            sum += abs(item[score_idx])
        elif style == 'L2':
            sum += item[score_idx] * item[score_idx]
        else:
            sum = 1

    if style == 'L2':
        sum = math.sqrt(sum)
    factor = 1.0
    if sum > 0:
        factor = 1.0 / sum

    outBag = []
    for item in list:
        tup = (item[fea_idx], item[score_idx]*factor)
        outBag.append(tup)
    return outBag


@outputSchema("data:chararray")
def gen_json_str(data):
    if data is None:
        return ""
    else:
        return json.dumps(data)

@outputSchema("categories:{t1:(y:map[])}")
def gen_empty_category_bag(source_type):
    return []

@outputSchema("data:{(itemid:chararray,score:float,gbdt_score:float,meta:chararray)}")
def flatten_doc_data(data):
    data_list = json.loads(data)
    result = []
    for item in data_list:
        itemid = item['id']
        score = item['score']
        gbdt_score = item['gbdt_score']
        result.append((itemid,score,gbdt_score,json.dumps(item)))
    return result
    # except:
    #     return []

import math
@outputSchema("data:{(cnt:int,ratio:double)}")
def eval_agedecay(data, decay_factor, window=12):
    if data is None:
        return []
    data = sorted(data, key=lambda x:x[0], reverse=True)
    idx = [i+2 for i in xrange(len(data))]
    decay_data = []
    for idx in xrange(len(data)):
        score = data[idx][0]
        age = data[idx][1]
        decay_age = int(age) / int(window)
        decayed_score = score * math.exp(decay_factor * decay_age)
        decay_data.append((idx+2,score,age,decayed_score))
    data_order = sorted(decay_data, key=lambda _x: _x[3], reverse=True)

    diff = 0.0
    overall_diff = 0.0
    for idx in xrange(len(data_order)-1):
        cur_idx = idx + 2
        for idx2 in xrange(idx,len(data_order)):
            if data_order[idx] > data_order[idx2]:
                diff += 1/float(min(cur_idx,data_order[idx2]))
            overall_diff += 1/float(cur_idx)
    if overall_diff ==0:
        overall_diff = 1
    diff_ratio = diff/overall_diff
    return [(len(decay_data),diff)]

@outputSchema("data:float")
def eval_age_score_diff(data):
    if data is None:
        return []
    data = sorted(data, key=lambda x:x[0], reverse=True)
    idx = [i+2 for i in xrange(len(data))]
    decay_data = []
    for idx in xrange(len(data)):
        score = data[idx][0]
        age = data[idx][1]
        decay_data.append((idx+2,score,age))
    data_order = sorted(decay_data, key=lambda _x: _x[2])

    diff = 0.0
    overall_diff = 0.0
    for idx in xrange(len(data_order)-1):
        cur_idx = idx + 2
        for idx2 in xrange(idx,len(data_order)):
            if data_order[idx] > data_order[idx2]:
                diff += 1/float(min(cur_idx,data_order[idx2]))
            overall_diff += 1/float(cur_idx)
    if overall_diff ==0:
        overall_diff = 1
    diff_ratio = diff/overall_diff
    # return [(len(decay_data),diff)]
    return diff

@outputSchema("flag:chararray")
def is_new_user(up):
    if up == None or len(up) == 0:
        return 'True'

    try:
        if len(up['kw']) ==0 and len(up['cg']) == 0:
            return 'True'
        else:
            return 'False'
    except:
        return 'True'

if __name__ == '__main__':
    # cpack={"des":"rid=4764d473|src=0|ord=21","ishot":1,"md5":"ce35a256"}
    # # print get_city_name(ip_address)
    # print parse_rid(cpack)

    print get_feature_lists()
