REGISTER 'lib/*.jar';
REGISTER 'lib/feature_log_util.py' USING jython AS myfunc;

set mapred.create.symlink yes;
set mapred.cache.files s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/20160612-20160613.txt#feature.map;
set default_parallel 200;
set fs.s3a.connection.maximum 1000;

raw_rcv_log = LOAD '$INPUT_EVENT' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate
    json,
    (json#'app_lan' is null ? 'en' : ( json#'app_lan' MATCHES 'hi.*' ? 'hi' : 'en' )) as app_lan,
    json#'contentid' as content_id:chararray,
    json#'aid' as uid:chararray,
    json#'ip' as ip:chararray,
    json#'servertime_sec' as eventtime,
    json#'pid' as pid:chararray,
    json#'ctype' as ctype,
    (int)json#'ext'#'dwelltime' AS dwelltime:int,
    json#'act' as act,
    json#'cpack' as cpack,
    json#'upack' as upack,
    myfunc.parse_rid(json#'cpack') as rid:chararray;

raw_rcv_log = filter raw_rcv_log by (pid == '1' or pid == '11') 
                and  ( ctype is not null and (ctype=='1' or ctype=='0x01' ) ) 
                and act is not null and ( act == '1' or act == '2' or act=='3' or act=='4' ) 
                and (json#'scenario'#'level1_type' == '1' or json#'scenario'#'level1_type' == '10')and json#'scenario'#'level1' == '1';

click_view_data = FOREACH raw_rcv_log GENERATE
    content_id, uid, rid,
    cpack, upack,
    (long)(eventtime) as original_time,
    pid as productid,
    ((dwelltime is null or dwelltime<0) ? 0 : dwelltime ) as dwelltime,
    (act=='1'? 'view':( act=='2'? 'click':( act=='4'? 'read': (act=='3'? 'listpagedwell':'unknown')))) as event_type;

log_click_view = FOREACH click_view_data GENERATE
      uid AS uid,
      content_id AS content_id,
      rid as rid,
      cpack, upack,
      original_time as ts,
      event_type AS event_type,
      productid as productid,
      (event_type=='read' ? dwelltime:0) as dwelltime:int,
      (event_type=='listpagedwell' ? dwelltime:0 ) as listpagedwell:int;

feature_log_format = load '$FEATURE_LOG_INPUT' using PigStorage('\t','-schema'); 

data_jnd = join log_click_view by (uid, content_id, productid,rid), feature_log_format by (uid, content_id, pid, rid);

events_with_feature = foreach data_jnd generate      
        log_click_view::uid, 
        log_click_view::content_id,
        log_click_view::productid as pid, 
        log_click_view::rid, 
        feature_log_format::predictor_id, 
        log_click_view::ts, 
        feature_log_format::doc_size, 
        feature_log_format::up,
        feature_log_format::features,
        feature_log_format::features_format;

store events_with_feature into '$OUTPUT';

