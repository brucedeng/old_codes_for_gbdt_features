REGISTER 'lib/*.jar';
REGISTER 'lib/feature_log_util.py' USING jython AS myfunc;

set mapred.create.symlink yes;
set mapred.cache.files s3://com.cmcm.instanews.usw2.prod/model/experiments/en_cfb_training_featmap/20160612-20160613.txt#feature.map;
set default_parallel 200;
set fs.s3a.connection.maximum 1000;

set output.compression.enabled true;
set output.compression.codec org.apache.hadoop.io.compress.GzipCodec;

set mapred.max.map.failures.percent 5;

feature_log = LOAD '$INPUT_FEATURE_LOG' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

feature_log = FOREACH feature_log GENERATE 
    json#'req_id' as rid:chararray,
    json#'prod_id' as pid:chararray,
    json#'predictor_id' as predictor_id:chararray,
    json#'now' as ts:long,
    json#'doc_size' as doc_size:long,
    json#'uid' as uid:chararray,
    json#'up' as up:map[],
    FLATTEN(json#'docs') as features;

feature_log_flat = FOREACH feature_log GENERATE
    uid, features#'id' as content_id, pid, rid, predictor_id, ts, doc_size, up, features;

feature_log_dist = FOREACH ( GROUP feature_log_flat by (uid,content_id,pid,rid) ) {
    data_sorted = ORDER $1 BY ts;
    data_limit = LIMIT data_sorted 1;
    GENERATE FLATTEN(data_limit) as (uid, content_id, pid, rid, predictor_id, ts, doc_size, up, features);
}

feature_log_format = FOREACH feature_log_dist GENERATE 
    uid, content_id, pid, rid, predictor_id, ts, doc_size, up,
    features,
    myfunc.format_feature_log(features) as features_format;

store feature_log_format into '$OUTPUT' using PigStorage('\t','-schema');


