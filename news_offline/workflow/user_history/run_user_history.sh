#!/bin/bash

set -x
# environment variables
declare -x EC2_HOME="/home/ec2"
declare -x HOSTNAME="launcher-news-cp.hadoop.busdev.usw2.cmcm.com"
declare -x JAVA_HOME="/etc/alternatives/jre"
declare -x PATH="/data1/bin/anaconda2/bin:/home/ec2/bin:/sbin:/bin:/usr/sbin:/usr/bin"
declare -x PKG_CONFIG_PATH="/usr/lib/pkgconfig:/usr/local/lib/pkgconfig"
declare -x PWD="/home/news_cp"
declare -x QTDIR="/usr/lib64/qt-3.3"
declare -x SHELL="/bin/bash"
declare -x SSH_ASKPASS="/usr/libexec/openssh/gnome-ssh-askpass"

# set -e 

if [[ $# -gt 0 ]]; then
    cur_ts=$1
else
    cur_ts=`date +%s`
    cur_ts=$((cur_ts-3600))
fi
cur_dt=`date -d@$cur_ts +%Y%m%d`
hour=`date -d@$cur_ts +%h`

input_event_template="+s3://com.cmcm.instanews.usw2.prod/data/raw_data/impression/%Y%m%d/%H/*"
history_template="+hdfs://mycluster/projects/news/user_profile/user_history/overall/%Y%m%d%H00"
output_txt_template="+hdfs://mycluster/projects/news/user_profile/user_history/hourly/%Y%m%d%H00.gz"
local_txt_template="+/data1/user_history/uh_rcv_hour_%Y%m%d%H00.dat"

prev_ts=$((cur_ts-3600))
input_event1=`date -d@$prev_ts "$input_event_template"`
input_event2=`date -d@$cur_ts "$input_event_template"`
input_event="$input_event1,$input_event2"
input_history=`date -d@$prev_ts "$history_template"`
output_history=`date -d@$cur_ts "$history_template"`
output_txt=`date -d@$cur_ts "$output_txt_template"`
output_local=`date -d@$cur_ts "$local_txt_template"`
# 7 days
filter_ts=$((cur_ts - 604800))

found=no
for ((i=0;i<6;i++)); do
    if hadoop fs -test -e $input_history/_SUCCESS; then
        found=yes
        break
    else
        echo $input_history/_SUCCESS does not exist
        found=no
        for ((j=0;j<10;j++)); do
            if hadoop fs -test -e $input_history/_SUCCESS; then
                found=yes
                break
            fi
            sleep 60
        done
        if [[ $found == "yes" ]]; then
            break
        fi
        prev_ts=$((prev_ts-3600))
        input_history=`date -d@$prev_ts "$history_template"`
    fi
done
if [[ $found == "no" ]]; then
    echo "can not find input history. exiting..."
    exit
fi

cmd="pig -Dmapred.job.queue.name=offline -p event='$input_event' -p history='$input_history' -p output_history='$output_history' -p output_txt='$output_txt' -p ts=$filter_ts read_history_hourly.pig"

echo "$cmd"
eval $cmd

if [[ $? -eq 0 ]]; then
    echo "fetching file $output_txt"
    set -x
    hadoop fs -getmerge $output_txt $output_local.gz
    for ((i=0;i<5;i++)); do
        rsync $output_local.gz rsync://10.5.1.26:9999/remote_cmnews_rcv
        if [[ $? -eq 0 ]]; then
            echo "successfully export file $output_local"
            done_file=$output_local.done
            touch $done_file
            rsync $done_file rsync://10.5.1.26:9999/remote_cmnews_rcv
            local_ip=`hostname -i`
            result_size=`zcat $output_local.gz | wc -l `
            curl -XPOST '127.0.0.1:1988/v1/push' -d '[{"endpoint": "'$local_ip'","metric":"indian.user_history.list","timestamp":'$cur_ts',"step":3600,"value": '$result_size',"counterType": "GAUGE","tags": ""}]'
            rm -f $output_local.gz $done_file
            break
        fi
    done

    dep_ts=$((cur_ts - 32400))
    dep_history=`date -d@$dep_ts "$history_template"`
    hadoop fs -rm -r -skipTrash $dep_history
    dep_ts=$((cur_ts - 259200))
    dep_txt=`date -d@$dep_ts "$output_txt_template"`
    hadoop fs -rm -r -skipTrash $dep_txt
fi
