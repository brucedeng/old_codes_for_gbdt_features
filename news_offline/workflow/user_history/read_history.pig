
REGISTER 'lib/*.jar';
-- REGISTER 'utils.py' USING jython AS myfunc;

raw_rcv_log = LOAD '$event' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate
    json,
    (json#'app_lan' is null ? 'en' : ( json#'app_lan' MATCHES 'hi.*' ? 'hi' : 'en' )) as app_lan,
    (chararray)json#'contentid' as content_id:chararray,
    (chararray)json#'aid' as uid:chararray,
    json#'ip' as ip,
    (long)json#'servertime_sec' as eventtime:long,
    json#'pid' as pid,
    json#'ctype' as ctype,
    json#'act' as act;

raw_rcv_log = filter raw_rcv_log by act is not null;

rcv_log_bag = FOREACH raw_rcv_log GENERATE uid, content_id, eventtime;

rcv_log_distinct = FOREACH ( GROUP rcv_log_bag by (uid,content_id) ) {
    data = LIMIT $1 1;
    GENERATE FLATTEN(data) as (uid,content_id,eventtime);
}

rcv_log_grp = FOREACH ( GROUP rcv_log_distinct by uid ) {
    data = FOREACH $1 GENERATE content_id,eventtime;
    GENERATE $0 as uid, data as data;
}

store rcv_log_grp into '$output' USING parquet.pig.ParquetStorer;
