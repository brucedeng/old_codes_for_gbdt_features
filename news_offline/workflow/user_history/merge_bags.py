
if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper
    import json
else:
    import com.xhaus.jyson.JysonCodec as json
    pass

@outputSchema("data:{T:(id:chararray,ts:long)}")
def mergeHistory(data1,data2,original_ts=0):
    if data1 is None:
        return data2
    if data2 is None:
        return data1
    d1map = dict([x for x in data1 if x[1] > original_ts])
    d2map = dict([x for x in data2 if x[1] > original_ts])
    d1map.update(d2map)
    return d1map.items()

@outputSchema("list:chararray")
def tojson(data):
    if data is None or len(data) == 0:
        return "[]"
    try:
        return '["%s"]' % '","'.join([str(x[0]) for x in data])
    except:
        return "[]"

if __name__=="__main__":
    print mergeHistory([('id1',123),('id2',234)],[('id2',124)],123)
    print tojson([('id1',123),('id2',234)])
    print tojson([('id1',123)])
    print tojson([])