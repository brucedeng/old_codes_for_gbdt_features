#!/bin/bash

# environment variables
declare -x EC2_HOME="/home/ec2"
declare -x HOSTNAME="launcher-news-cp.hadoop.busdev.usw2.cmcm.com"
declare -x JAVA_HOME="/etc/alternatives/jre"
declare -x PATH="/data1/bin/anaconda2/bin:/home/ec2/bin:/sbin:/bin:/usr/sbin:/usr/bin"
declare -x PKG_CONFIG_PATH="/usr/lib/pkgconfig:/usr/local/lib/pkgconfig"
declare -x PWD="/home/news_cp"
declare -x QTDIR="/usr/lib64/qt-3.3"
declare -x SHELL="/bin/bash"
declare -x SSH_ASKPASS="/usr/libexec/openssh/gnome-ssh-askpass"

# set -e 

if [[ $# -gt 0 ]]; then
    cur_ts=$1
else
    cur_ts=`date +%s`
    cur_ts=$((cur_ts-3600))
fi
cur_dt=`date -d@$cur_ts +%Y%m%d`
hour=`date -d@$cur_ts +%h`

ftr_start=`date -d "-7 days $cur_dt" +%Y%m%d`
agg_dates=$ftr_start
ftr_end=$cur_dt
for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do agg_dates="$agg_dates,$agg_day"; done

input_event_template="s3://com.cmcm.instanews.usw2.prod/data/raw_data/impression/{%s}/*/*"
history_template="+hdfs://mycluster/projects/news/user_profile/user_history/overall/%Y%m%d%H00"
output_txt_template="+hdfs://mycluster/projects/news/user_profile/user_history/hourly/%Y%m%d%H00.gz"
local_txt_template="+/data1/user_history/uh_rcv_hour_%Y%m%d%H00.dat"

prev_ts=$((cur_ts-3600))
input_event=`printf "$input_event_template" "$agg_dates"`
output_history=`date -d@$prev_ts "$history_template"`

cmd="pig -Dmapred.job.queue.name=offline -p event='$input_event' -p output='$output_history' read_history.pig"

echo "$cmd"
eval $cmd
