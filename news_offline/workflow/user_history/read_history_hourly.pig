
REGISTER 'lib/*.jar';
REGISTER 'merge_bags.py' USING jython AS udf;

raw_rcv_log = LOAD '$event' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate
    json,
    (json#'app_lan' is null ? 'en' : json#'app_lan') as app_lan:chararray,
    (chararray)json#'contentid' as content_id:chararray,
    (chararray)json#'aid' as uid:chararray,
    json#'ip' as ip,
    (long)json#'servertime_sec' as eventtime:long,
    json#'pid' as pid,
    json#'ctype' as ctype,
    json#'act' as act;

raw_rcv_log = filter raw_rcv_log by act is not null and SIZE(uid) >= 8;

rcv_log_bag = FOREACH raw_rcv_log GENERATE uid, content_id, eventtime;

rcv_log_grp = FOREACH ( GROUP rcv_log_bag by uid ) {
    data = FOREACH $1 GENERATE content_id,eventtime;
    GENERATE $0 as uid, data as data;
}

history_data = LOAD '$history' USING parquet.pig.ParquetLoader;
history_data = FILTER history_data by SIZE(uid) >=8 ;
events_join = FOREACH (JOIN rcv_log_grp by uid full outer, history_data by uid) GENERATE 
    rcv_log_grp::uid as uid1,
    rcv_log_grp::data as data1,
    history_data::uid as uid2,
    history_data::data as data2;

events_distinct = FOREACH events_join GENERATE uid1,uid2, udf.mergeHistory(data1,data2,$ts) as data;

history_data_out = FOREACH events_distinct GENERATE (uid1 is not null? uid1 : uid2) as uid, data as data;
store history_data_out into '$output_history' USING parquet.pig.ParquetStorer;

cur_events = FILTER events_distinct by uid1 is not null;

json_data = FOREACH cur_events GENERATE uid1 as uid, udf.tojson(data) as data;
json_data = FILTER json_data by data != '[]';
store json_data into '$output_txt';
