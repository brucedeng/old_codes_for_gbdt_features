#!/bin/bash

export logdir=/data/app/cmnews_prediction/logs
export aws_dir=s3://com.cmcm.instanews.usw2.prod/model/feature_log

### aws configs
export AWS_ACCESS_KEY_ID='AKIAIOL4WNL35F2X7H5Q'
export AWS_SECRET_ACCESS_KEY='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'
export AWS_DEFAULT_REGION='us-west-2'
export PATH=/usr/lib64/qt-3.3/bin:/home/ec2/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin

### ip whitelist. only machines in this whitelist execute the scripts below.
ip_whitelist=' 10.2.2.137 10.2.2.141 10.5.1.227 10.5.1.225 52.34.245.29 52.88.108.89 52.76.229.145 52.76.250.235 '

user_name=`whoami`
if [[ $user_name == 'root' ]]; then
    done_dir=$logdir
    log_path=$logdir
else
    done_dir=/home/$user_name/done
    log_path=/home/$user_name/logs
    mkdir -p $done_dir
    mkdir -p $log_path
fi

log_file="$log_path/indian_log_uploader.log"

function my_ip {
    hostname -i
}

function log() {
    echo "`date +'%Y%m%d %H:%M:%S'` => $*" >> $log_file
}

### first check if the current ip in ip whitelist. 
local_ip=`my_ip`
if [[ ! "$ip_whitelist" =~ " $local_ip " ]]; then 
    exit
fi

if [[ -e $done_dir/upload_featurelog.pid ]]; then
    prev_pid=`cat $done_dir/upload_featurelog.pid`
    process_cnt=`ps aux | awk '$2==$prev_pid' | wc -l`
    if [[ process_cnt -gt 0 ]]; then
        log "exiting because a previous process is running."
        exit
    fi
fi

echo $$ > $done_dir/upload_featurelog.pid

### ip in whitelist, then start creating dir


cur_ts=`date +%s`

cd $logdir

result_size=0
result_cnt=0
log "starting upload"

### upload all file to s3
for f in features.log.*.gz; do
    f_date=`basename $f .gz | sed 's/features\.log\.//g'`
    f_date=${f_date:0:12}
    # echo $f_date
    if [[ ${#f_date} -ne 12 || ! "$f_date" =~ [[:digit:]]+ ]]; then
        log "ignored $f, invalid date format."
        continue
    fi
    if grep -q "$f" $done_dir/uploaded_featurelog.log; then
        continue
    fi
    cur_aws_path="$aws_dir/${f_date:0:8}/${f_date:8:2}/${f_date:10:2}/${local_ip}.$f"
    for ((i=0;i<5;i++)); do
        aws s3 cp $f $cur_aws_path
        success=$?
        if [[ $success -eq 0 ]]; then
            echo $f >> $done_dir/uploaded_featurelog.log
            cur_file_size=`du $f | cut -f1`
            result_size=$((result_size + cur_file_size))
            result_cnt=$((result_cnt + 1))
            log "uploaded $f to $cur_aws_path"
            break;
        fi
        log "failed for the $i th time. retry needed ..."
    done

done

log "upload finished"
result_size=`echo $result_size | awk '{print $1 / 1024}'`
curl -XPOST '127.0.0.1:1988/v1/push' -d '[{"endpoint": "'$local_ip'","metric":"indian.featurelog.uploadsize","timestamp":'$cur_ts',"step":600,"value": '$result_size',"counterType": "GAUGE","tags": ""}]'
curl -XPOST '127.0.0.1:1988/v1/push' -d '[{"endpoint": "'$local_ip'","metric":"indian.featurelog.uploadcnt","timestamp":'$cur_ts',"step":600,"value": '$result_cnt',"counterType": "GAUGE","tags": ""}]'

### clean up done file logs, only save the most recent 3 days
tmp_file=`mktemp -t uploaded_featurelog.XXXXX`
tail -n 432 $done_dir/uploaded_featurelog.log > $tmp_file
mv $tmp_file $done_dir/uploaded_featurelog.log
chmod 644 $done_dir/uploaded_featurelog.log

### truncate log file to 5000 lines
tail -n 5000 $log_file > $tmp_file
mv $tmp_file $log_file
chmod 644 $log_file

### remove pid file
rm -f $done_dir/upload_featurelog.pid

