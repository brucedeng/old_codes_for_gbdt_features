#!/bin/bash

# environment variables
declare -x EC2_HOME="/home/ec2"
declare -x HOSTNAME="launcher-news-cp.hadoop.busdev.usw2.cmcm.com"
declare -x JAVA_HOME="/etc/alternatives/jre"
declare -x PATH="/data1/bin/anaconda2/bin:/home/ec2/bin:/sbin:/bin:/usr/sbin:/usr/bin"
declare -x PKG_CONFIG_PATH="/usr/lib/pkgconfig:/usr/local/lib/pkgconfig"
declare -x PWD="/home/news_cp"
declare -x QTDIR="/usr/lib64/qt-3.3"
declare -x SHELL="/bin/bash"
declare -x SSH_ASKPASS="/usr/libexec/openssh/gnome-ssh-askpass"

set -e 

cur_date=$1

input_table="cmnews_dwh.content_activity"
output="hdfs://mycluster/projects/news/cpp/opencms_id_whitelist/$cur_date"

ftr_start=`date -d "+9 day ago $cur_date" +%Y%m%d`;
ftr_end=$cur_date;
agg_dates_cfb=$ftr_start
for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates_cfb="$agg_dates_cfb,$agg_day"; done
input_cp="hdfs://mycluster/projects/news/cpp/feeder/in_cp_dump/{$agg_dates_cfb}/*/*/*.data"

event_start=`date -d "+6 day ago $cur_date" +%Y%m%d00`;
event_end=`date -d "+1 day $cur_date" +%Y%m%d00`;

cmd="pig -useHCatalog -p input_table='$input_table' -p cp_dump='$input_cp' -p start_time='$event_start' -p end_time='$event_end' -p output='$output' gen_opencmsid_whitelist.pig"

echo "$cmd"
eval $cmd

if [[ $? -eq 0 ]]; then
    hadoop fs -cat $output/* > data/$cur_date.txt
    echo "cat data/$cur_date.txt | python write_opencmsid_list.py"
    cat data/$cur_date.txt | python write_opencmsid_list.py
    local_ip=`hostname -i`
    cur_ts=`date +%s`
    result_size=`cat data/$cur_date.txt | wc -l `
    # result_size=1
    if [[ $? -eq 0 ]]; then
        curl -XPOST '127.0.0.1:1988/v1/push' -d '[{"endpoint": "'$local_ip'","metric":"indian.opencms_id.whitelist","timestamp":'$cur_ts',"step":86400,"value": '$result_size',"counterType": "GAUGE","tags": ""}]'
    fi
fi
