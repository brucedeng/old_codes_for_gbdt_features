REGISTER 'lib/*.jar';
-- REGISTER 'hindi_up.py' USING jython AS myfunc;

raw_events = LOAD '$input_table' USING org.apache.hive.hcatalog.pig.HCatLoader();

events_filter = FILTER raw_events by date_time > '$start_time' and date_time <= '$end_time';

events_filter = FILTER events_filter by pos_type==1 and pid==11;

content_grp = FOREACH ( GROUP events_filter by (contentid,lan) ) GENERATE 
    FLATTEN($0) as (contentid,lan),
    SUM($1.article_view_count) as view_count,
    SUM($1.article_click_count) as click_count;

overall_ctr = FOREACH ( GROUP content_grp by lan ) GENERATE 
    $0 as lan,
    SUM($1.view_count) as view_count,
    SUM($1.click_count) as click_count;
overall_ctr = FOREACH overall_ctr GENERATE lan, click_count/(view_count+1.0) as ctr, view_count;

raw_content = LOAD '$cp_dump' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_content = foreach raw_content generate  
  json#'item_id' as content_id:chararray,
  json#'type' as type:chararray,
  json#'opencms_id' as opencms_id:chararray,
  (int)(json#'source_type') as source_type:int,
  (long)json#'publish_time' as publish_time:long,
  (long)json#'update_time' as update_time:long;

raw_content_filter = FILTER raw_content by (type=='article' or type=='slideshow' or type=='photostory') and opencms_id is not null and opencms_id != '' and opencms_id!='0';
raw_content = FOREACH raw_content_filter GENERATE content_id,opencms_id, update_time;

raw_content_distinct = foreach (group raw_content by content_id) {
    
    r = order raw_content by update_time DESC;
    l = limit r 1;
    generate flatten(l);

};

-- rmf $output
-- store raw_content_distinct into '$output/raw_content_distinct';

content_jnd_with_opencmsid = FOREACH ( JOIN content_grp by contentid, raw_content_distinct by content_id ) GENERATE 
    content_grp::contentid as contentid,
    content_grp::lan as lan,
    content_grp::view_count as view_count, 
    content_grp::click_count as click_count,
    raw_content_distinct::l::opencms_id as opencms_id;

-- store content_jnd_with_opencmsid into '$output/content_jnd_with_opencmsid';

cmsid_ctr = FOREACH ( GROUP content_jnd_with_opencmsid by (opencms_id,lan) ) GENERATE 
    FLATTEN($0) as (opencms_id,lan),
    SUM($1.view_count) as view_count,
    SUM($1.click_count) as click_count;

cmsid_ctr_compare = FOREACH ( JOIN cmsid_ctr by lan left outer, overall_ctr by lan USING 'replicated' ) GENERATE
    cmsid_ctr::opencms_id as opencms_id,
    cmsid_ctr::lan as lan,
    cmsid_ctr::click_count/(cmsid_ctr::view_count+0.1) as ctr,
    cmsid_ctr::view_count as view_count,
    overall_ctr::ctr as overall_ctr,
    overall_ctr::view_count as overall_pv;

rmf $output
store cmsid_ctr_compare into '$output';
