#!/usr/bin/env python
# encoding=utf-8

import sys
import os
import traceback
import json
import datetime
from redis import Redis

redis_confs = [{"host":"featureserver.mkjqd2.ng.0001.apse1.cache.amazonaws.com","port":6379,"db":3},
{"host" : "featureserverprd.mkjqd2.0001.apse1.cache.amazonaws.com","port" : 6379,"db" : 3},
{"host" : "featureserver.bgkplr.ng.0001.usw2.cache.amazonaws.com","port" : 6379,"db" : 3},
{"host" : "feature-server.mkjqd2.ng.0001.apse1.cache.amazonaws.com","port" : 6379, "db" : 3},
{"host" : "feature-server.6gvwka.ng.0001.euw1.cache.amazonaws.com", "port" : 6379, "db" :3},
{"host" : "feature-server.bgkplr.ng.0001.usw2.cache.amazonaws.com", "port" : 6379, "db" : 3}]
global key
key="news_opencmsid_whitelist"
def write_redis (conf,data):
    try:
        r = Redis(**conf)
        r.set(key,data)
    except:
        print traceback.format_exc()

if __name__ == "__main__":
    result = set(['72'])
    for line in sys.stdin:
        line = line.rstrip('\n')
        fields = line.split('\t')
        opencms_id = fields[0]
        try:
            pv = float(fields[3])
            if pv < 30:
                continue

            #overall_ctr = float(fields[4])
            overall_ctr = 0.05
            cms_ctr = float(fields[2])
            if cms_ctr > overall_ctr:
                result.add(opencms_id)
        except:
            print traceback.format_exc()
    result = ','.join(result)
    print "result is " + result
    for r_conf in redis_confs:
        print "writing redis " + str(r_conf)
        write_redis(r_conf,result)
