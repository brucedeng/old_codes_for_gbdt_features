#!/bin/bash

# start_time="$1"

# start_ts=`date -d "$start_time" +%s`
# end_ts=`date -d "$end_time" +%s`

check_interval=30
prev_ts=10

while true
do

	cur_time=`date +%s`
	cur_ts=$((cur_time/86400*86400))
	if [[ $cur_ts -eq $prev_ts ]]; then
		sleep $check_interval
		continue
	fi
	prev_ts=$cur_ts
	nominal_time=$((cur_ts-86400))
	day=`date -d@$nominal_time +%Y%m%d`
	log_file=logs/whitelist_work.log.$day
	echo running for $day >> $log_file
	bash run_cmsid_list.sh $day >> $log_file 2>&1 &

	find logs/ -mtime +14 -print0 | xargs -0 rm -f
done
