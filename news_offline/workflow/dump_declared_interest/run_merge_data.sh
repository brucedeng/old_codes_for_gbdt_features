#!/bin/bash

bin_file=`readlink -f $0`
bin_dir=`dirname $bin_file`
set -e

daily_declare=s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/daily
all_declare=s3://com.cmcm.instanews.usw2.prod/user_profile/declared_interest/overall

# nominal_time=$1
# nominal_time=`date -d "$nominal_time" +%Y%m%d`
nominal_time=`date +%Y%m%d`
check_interval=30
wait_time="+1 days"
start_ts=`date +%s`
echo "started pipeline at `date`"
end_ts=`date -d "$nominal_time $wait_time" +%s`

has_data=0
while :
do
    cur_ts=`date +%s`
    if ! hadoop fs -test -e $daily_declare/$nominal_time/_SUCCESS; then
        echo "$daily_declare/$nominal_time/_SUCCESS not exists"
        if [[ $cur_ts -lt $end_ts ]]; then
            sleep $check_interval
            continue
        else
            break
        fi
    fi
    has_data=1
    break
done

input_all=none
for ((i=1;i<5;i++)); do
    dt=`date -d "$nominal_time -$i days" +%Y%m%d`
    if hadoop fs -test -e $all_declare/$dt/_SUCCESS; then
        echo "$all_declare/$dt/_SUCCESS exists"
        input_all=$all_declare/$dt
        break;
    fi
done

if [[ $input_all != 'none' && has_data -eq 1 ]]; then
    cmd="pig -p daily_declare=$daily_declare/$nominal_time -p all_declare=$input_all -p output=$all_declare/$nominal_time merge_declare.pig"
    echo $cmd
    eval $cmd
else
    echo "overall data not exists"
fi
