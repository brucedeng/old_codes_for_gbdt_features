#!/usr/bin/env python

from redis import Redis
import StringIO
# from s3_uploader import S3_Uploader
import time
import os
import sys
import argparse
import json
import boto3
import traceback

declare_interest_key = "mq_work:interestDataInQuene"

news_declare_interest_redis = [{
    'host' : "newsapistatic-001.mkjqd2.0001.apse1.cache.amazonaws.com",
    'port' : 6379,
    'db' : 0
},
{
    'host' : "instanewsapistatic.bgkplr.0001.usw2.cache.amazonaws.com",
    'port' : 6379,
    'db' : 0
}]

s3_bucket_name = 'com.cmcm.instanews.usw2.prod'

def save_s3(content, key_name):
    s3 = boto3.resource('s3')
    try:
        s3.Object(s3_bucket_name , key_name + '/data').put(Body=content)
        s3.Object(s3_bucket_name , key_name + '/_SUCCESS').put(Body='')
        return True
    except:
        traceback.print_exc()
        return False

def save_file(content, path):
    if not os.path.exists(path):
        try:
            os.makedirs(path)
            f = open(path + '/data','w+')
            f.write(content)
            f.close()
            f = open(path + '/_SUCCESS', 'w+')
            f.write('')
            f.close()
            return True
        except:
            traceback.print_exc()
            return False
            # raise OSError("Can't create destination directory (%s)!" % (path))

if __name__=='__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-s', '--s3key', action='store', dest='s3key', help='s3 key path')
    arg_parser.add_argument('-d', '--dir', action='store', dest='local_path', help='local path.')
    arg_parser.add_argument('-r', '--remove', action='store', dest='rm_interval', help='remove interval')

    args = arg_parser.parse_args()
    interval = args.rm_interval
    out_s3 = args.s3key
    out_local = args.local_path

    input_redis = []
    for conf in news_declare_interest_redis:
        input_redis.append([Redis(**conf),0])

    start_ts = 0
    rm_idx = 0
    if interval is not None:
        current = time.time()
        start_ts = current - int(interval) * 60

    output = StringIO.StringIO()

    overall_cnt = 0
    # load data
    for tmp_redis_data in input_redis:
        redis_obj = tmp_redis_data[0]
        data_list = redis_obj.lrange(declare_interest_key, 0, -1)
        count_continue = True
        cnt = 0
        total_cnt = 0
        for line in data_list:
            data = json.loads(line)
            ts = data.get('time',0)
            total_cnt += 1
            if count_continue:
                if ts >= start_ts:
                    count_continue = False
                else:
                    cnt += 1
            output.write(line)
            output.write('\n')
        tmp_redis_data[1] = cnt
        overall_cnt += total_cnt
        print "loaded {} items from redis. ".format(total_cnt)

    print "totally loaded {} items.".format(overall_cnt)

    content_str = output.getvalue()
    success = True
    if out_s3 is not None:
        if not save_s3(content_str, out_s3):
            success = False
        else:
            print "successfully output to " + s3_bucket_name + '/' + out_s3

    if out_local is not None:
        if not save_file(content_str, out_local):
            success = False
        else:
            print "successfully output to " + out_local

    if success:
        print "successfully ported data"
    else:
        print "failed port data"

    if success and interval is not None:
        for tmp_redis_data in input_redis:
            redis_obj = tmp_redis_data[0]
            cnt = tmp_redis_data[1]
            print cnt
            for i in range(cnt):
                rm_data = redis_obj.pop(declare_interest_key)
                # print rm_data
            print "successfully removed {} items." .format(cnt)
        sys.exit(0)
    else:
        sys.exit(1)
