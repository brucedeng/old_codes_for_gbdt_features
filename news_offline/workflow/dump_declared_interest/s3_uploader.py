#!/usr/bin/env python
# encoding=utf-8

# File Name: s3_uploader.py
# Author: Baochang Ma
# mail: mabaochang@conew.com
# Created Time: 2015-11-13

import sys
import boto3
import config
import traceback

class S3_Uploader(object):
    def __init__(self , logger = None):
        self.s3 = boto3.resource('s3')
        self.logger = logger

    def put(self , key_name , contents):
        try:
            self.s3.Object(config.bucket_name , key_name).put(Body=contents)
            return True
        except:
            self.logger.info(traceback.format_exc())
            return False
        return False

if __name__ == "__main__":
    test = S3_Uploader()
    test.put_contents("test" ,"test")