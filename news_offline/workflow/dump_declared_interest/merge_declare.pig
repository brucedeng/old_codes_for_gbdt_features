REGISTER 'lib/*.jar';
-- REGISTER 'utils.py' USING jython AS myfunc;
REGISTER 'declare_util.py' USING jython AS udf;

daily_interest = load '$daily_declare' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);
daily_raw_interest = FOREACH daily_interest GENERATE json#'aid' as aid, json#'interest' as interest, json#'time' as time, udf.dump_json_str(json) as json_str;

declared_interest = load '$all_declare' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_interest = FOREACH declared_interest GENERATE json#'aid' as aid, json#'interest' as interest, json#'time' as time, udf.dump_json_str(json) as json_str;

all_data = UNION daily_raw_interest, raw_interest;

uniq_interest = FOREACH ( GROUP all_data by aid PARALLEL 1 ) {
    r = ORDER $1 BY  time DESC;
    l = LIMIT r 1;
    GENERATE FLATTEN(l) as (aid, interest, time, json_str);
}

result = FOREACH uniq_interest GENERATE json_str;

store result into '$output';