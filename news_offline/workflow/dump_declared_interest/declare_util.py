# -*- coding:utf-8 -*-
import com.xhaus.jyson.JysonCodec as json
#import json

if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper

@outputSchema('data:chararray')
def dump_json_str(data):
    if data is None:
        return ''
    else:
        return json.dumps(data)

# if __name__ == '__main__':
#     ip_address = '49.15.86.0'
#     print get_city_name(ip_address)

