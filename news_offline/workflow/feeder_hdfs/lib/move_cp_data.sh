#!/bin/bash

needed_data=`echo "$1" | sed 's/\/$//g'`
# output_data=`echo "$2" | sed 's/\/$//g'`

# set -e

interval=30
copied=no

# output_data_root=`dirname $output_data`
# hadoop fs -mkdir -p $output_data_root

for ((i=0; i<40; i++)); do
    echo "copying $needed_data to $output_data, ${i}th try..." 1>&2
    if hadoop fs -test -e $needed_data/_SUCCESS; then
    	echo "successfully copied" 1>&2
    	copied=yes
    	break
        # hadoop fs -rm -r -f $output_data
        # hadoop fs -cp $needed_data $output_data
        # if [[ $? -eq 0 ]]; then
        #     
        #     copied=yes
        #     break
        # else
        #     echo "copy failed, try again" 1>&2
        #     sleep ${interval}
        #     continue
        # fi
    else
        echo "data is not ready. wait for ${interval} seconds" 1>&2
        sleep ${interval}
        continue
    fi
done

if [[ $copied != yes ]]; then
    echo "data not ready after 40 tries, put success flag."
    hadoop fs -mkdir -p $needed_data
    hadoop fs -touchz $needed_data/_SUCCESS
fi
