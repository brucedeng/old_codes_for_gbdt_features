#!/usr/bin/env bash
source ./metric.conf

input_event="s3://com.cmcm.instanews.usw2.prod/data/raw_data/{impression,click}/${target_date_day}/${target_date_hour}/*"
input_contents="/projects/news/cpp/feeder/in_cp_dump/{$agg_dates}/*/*/*.data"
cmd="pig $PIG_PARAM -p event='$input_event' -p cp_dump='$input_contents' -p app_lan='${app_lan}' -p country='${country}' -p pid='${pid}' explore_metric.pig > ./logs/explore_metric_${app_lan}_${country}.log 2>&1 &"
echo ${cmd}
eval ${cmd}
