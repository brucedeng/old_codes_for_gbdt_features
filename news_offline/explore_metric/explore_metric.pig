REGISTER '../gen_train_data/lib/*.jar';
REGISTER './explore_utils.py' USING jython AS myfunc;

--set mapred.create.symlink yes;
--set mapred.cache.files $CONFIG#config.json,$CITY_DATA#ip_city.data

set default_parallel 200;

-- pre process content
raw_content = LOAD '$cp_dump' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_content = FOREACH raw_content GENERATE
  json#'item_id' as content_id:chararray,
  json#'type' as type:chararray,
  (long)json#'publish_time' as publish_time:long,
  (long)json#'update_time' as update_time:long,
  (json#'has_copyright' is null ? 0 : (json#'has_copyright')) as has_copyright:int;

raw_content_filter = FILTER raw_content by (type=='article');

raw_content_distinct = foreach (group raw_content_filter by content_id) {
    r = order raw_content_filter by update_time DESC;
    l = limit r 1;
    GENERATE flatten(l);


};

content_info = FOREACH raw_content_distinct {
  GENERATE
      content_id AS content_id,
      update_time AS update_time,
      publish_time as publish_time,
      has_copyright as has_copyright;
}

-- process raw_rcv_log
raw_rcv_log = LOAD '$event' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad') as (json:map[]);

raw_rcv_log = foreach raw_rcv_log generate
    json,
    (json#'app_lan' is null ? 'en' : ( json#'app_lan' MATCHES 'hi.*' ? 'hi' : json#'app_lan'   )) as app_lan,
    json#'contentid' as content_id,
    json#'aid' as uid,
    json#'servertime_sec' as eventtime,
    json#'pid' as pid,
    json#'ctype' as ctype,
    json#'country' as country,
    json#'upack'#'exp' as exp,
    myfunc.parse_src(json#'cpack') as src,
    json#'scenario'#'level1_type' as pos_type,
    json#'scenario'#'level1' as pos,
    json#'appv' as appv,
    json#'act' as act;

raw_rcv_log = filter raw_rcv_log by act is not null
            -- and pid=='$pid'
            and ( ctype is null or ctype=='' or ctype=='1' or ctype=='0x01'   )
            and ( act == '1' or act == '2'   )
            and ( pos_type == '1'  )
            and ( pos == '1'  )
            and country == '$country'
            and app_lan == '$app_lan';

click_view_data = FOREACH raw_rcv_log GENERATE
    content_id, uid, pid, appv,
    eventtime AS ts,
    (act=='1'? 'view':'click') AS event_type,
    pos, pos_type, exp, src;

log_click_view = distinct click_view_data;

log_content_info_join = JOIN log_click_view BY (content_id),
                             content_info BY (content_id);

log_content_info = FOREACH  log_content_info_join GENERATE
      log_click_view::content_id AS content_id,
      log_click_view::ts AS ts,
      log_click_view::uid AS uid,
      log_click_view::pid AS pid,
      log_click_view::event_type AS event_type,
      log_click_view::pos AS pos,
      log_click_view::pos_type AS pos_type,
      log_click_view::exp AS exp,
      log_click_view::src AS src,
      log_click_view::appv AS appv,
      content_info::update_time AS update_time,
      content_info::publish_time AS publish_time,
      log_click_view::ts - content_info::publish_time AS docage,
      content_info::has_copyright AS has_copyright;

--rmf /tmp/guozhenyuan/explore_metric/log_content_info;
--store log_content_info into '/tmp/guozhenyuan/explore_metric/log_content_info';

all_statistic = foreach (group log_content_info by (pid, exp, appv)) {
    c = filter log_content_info by event_type == 'click';
    v = filter log_content_info by event_type == 'view';
  GENERATE
    FLATTEN(group) AS (pid, exp, appv), COUNT(c) as click, COUNT(v) as view, AVG($1.docage) as avg_docage, SUM($1.has_copyright) as cnt_has_copyright;
}

rmf /tmp/guozhenyuan/explore_metric/all_statistic
store all_statistic into '/tmp/guozhenyuan/explore_metric/all_statistic';
fs -getmerge /tmp/guozhenyuan/explore_metric/all_statistic ./data/${country}_${app_lan}_all_statistic;

log_content_info_explore = filter log_content_info by ((INT)src)/16 % 2 == 1;

explore_statistic = foreach (group log_content_info_explore by (pid, exp, appv)) {
    c = filter log_content_info_explore by event_type == 'click';
    v = filter log_content_info_explore by event_type == 'view';
  GENERATE
    FLATTEN(group) AS (pid, exp, appv), COUNT(c) as click, COUNT(v) as view, AVG($1.docage) as avg_docage, SUM($1.has_copyright) as cnt_has_copyright;
}

rmf /tmp/guozhenyuan/explore_metric/explore_statistic
store explore_statistic into '/tmp/guozhenyuan/explore_metric/explore_statistic';
fs -getmerge /tmp/guozhenyuan/explore_metric/explore_statistic ./data/${country}_${app_lan}_explore_statistic;

explore_all_statistic = JOIN explore_statistic BY (pid, exp, appv), all_statistic BY (pid, exp, appv);
explore_metric = foreach explore_all_statistic GENERATE
    explore_statistic::pid as pid,
    explore_statistic::exp as exp,
    explore_statistic::appv as appv,--common
    explore_statistic::click as explore_click,
    explore_statistic::view as explore_view,
    explore_statistic::cnt_has_copyright as explore_cnt_has_copyright,
    explore_statistic::avg_docage as explore_avg_docage,
    all_statistic::click as all_click,
    all_statistic::view as all_view,
    all_statistic::cnt_has_copyright as all_cnt_has_copyright,
    all_statistic::avg_docage as all_avg_docage,
    (explore_statistic::click + explore_statistic::view)/(double)(all_statistic::click + all_statistic::view) as explore_traffic_ratio;

rmf /tmp/guozhenyuan/explore_metric/explore_metric
store explore_metric into '/tmp/guozhenyuan/explore_metric/explore_metric';
fs -getmerge /tmp/guozhenyuan/explore_metric/explore_metric ./data/${country}_${app_lan}_explore_metric;

