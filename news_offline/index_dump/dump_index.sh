#!/bin/sh
bin_file=`readlink -f $0`
bin_dir=`dirname $bin_file`


export PS4='+${BASH_SOURCE}:${LINENO}:${FUNCNAME[0]}: '
# set -e

lan=$1
if [ $lan = 'en' ]
then
input_gmp_path=/data/news_india_fallback_pipeline/gmp
output_gmp_path=/data2/tmp/index_dump_
elif [ $lan = 'hi' ]
then
input_gmp_path=/data/hindi_news_india_fallback_pipeline/gmp
output_gmp_path=/data2/tmp/index_dump_
else
exit -1
fi

input_cp=/data/news_india_fallback_pipeline/data/
output_cp=/data2/tmp/index_dump_

mkdir -p $output_cp
mkdir -p $output_gmp_path
rm -rf $output_gmp_path/*
rm -rf $output_cp/*

cmd_merge_cp="./merge_cp.sh $input_cp $output_cp"
eval $cmd_merge_cp
cmd_merge_gmp="./merge_gmp.sh $input_gmp_path $output_gmp_path"
eval $cmd_merge_gmp

cur_day=`date +%Y%m%d`
cur_hour=`date +%H`
cur_min=`date +%M`
cur_minute=`echo "$cur_min / 10 * 10" | bc | sed ':a;s/\(..\)/\1/;tb;s/^/0/;ta;:b'`

ts=$cur_day$cur_hour$cur_minute

if [ $lan = 'en' ]
then
index_output=/data2/index_dump/en/${cur_day}/${cur_hour}/${cur_minute}
elif [ $lan = 'hi' ]
then
index_output=/data2/index_dump/hi/${cur_day}/${cur_hour}/${cur_minute}
else
exit -1
fi

mkdir -p $index_output
python dump_index.py -c ${output_cp}"/cp.data" -g ${output_gmp_path}"/gmp.data" -t $ts -l $lan -o ${index_output}/${cur_minute}
