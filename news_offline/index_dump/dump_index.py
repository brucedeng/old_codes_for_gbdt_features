import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import traceback
import argparse
import logging
import json
import ujson,datetime,time
import urllib2
import time


console_handler = logging.StreamHandler()
console_handler.setFormatter(logging.Formatter('%(asctime)s\t%(levelname)s\t%(message)s'))
logger = logging.getLogger(__name__)
logger.addHandler(console_handler)
logger.setLevel(logging.DEBUG)

def load_gmp(path, logger, threshold=20.0):
    res = {}
    file = open(path)
    total_ec = 0
    total_c = 0

    for line in file:
        f = line.split('\t')
        item_id = str(f[0])
        original_ec = float(f[2])
        ec = float(f[4])
        c = float(f[5])

        if c >0 and ec >0 and original_ec > threshold and c <= ec:
            coec = c/ec
            res[item_id] = (c,ec,coec)

        total_ec += ec
        total_c += c

    res['total'] = (total_c,total_ec,0)
    file.close()

    logger.info('[PARSER] Valid GMP count: %s over original ec threshold %f, average GMP: %s' % (str(len(res)), threshold, str(res['total'])))
    return res

def load_dedup_content(path,logger):
    doc_num = 0
    result = []
    item_id_map = {}
    idx = 0
    f = open(path)
    for line in f:
        try:
            obj = ujson.loads(line)
            publisher=obj.get('publisher','')
            if publisher=='Glamsham':
                continue
            item_id = obj['item_id']
            if item_id not in item_id_map:
                item_id_map[item_id] = idx
                result.append(obj)
                idx += 1
            else:
                tmp_idx = item_id_map[item_id]
                tmp_item = result[tmp_idx]
                tmp_publish_time = tmp_item.get('update_time')
                publish_time = obj.get('update_time')
                if publish_time >= tmp_publish_time:
                    result[tmp_idx] = obj
        except:
            continue
        doc_num += 1

    logger.info('[PARSER] Total doc num: %s' % str(doc_num))
    logger.info('[PARSER] Total distinct doc num: %d \n' % len(result))

    return result

def filter_article(input_content, config_json, logger):
    non_servable = 0
    cp_disable = 0
    language_region_invalid = 0
    short_content = 0
    low_quality = 0

    new_cnt = add_cnt = merge_cnt = 0

    article_list = []

    for obj in input_content:
        try:
            if obj['is_servable'] == False:
                non_servable += 1
                continue

            if obj.get('cp_disable',False) == True:
                cp_disable += 1
                continue
            #region&lang incorrect
            now = datetime.datetime.now()
            fourdaybefore = now - datetime.timedelta(days=4)
            fourdaybefore_time = time.mktime(fourdaybefore.timetuple())
            if obj['publish_time'] < fourdaybefore_time:
                continue
            if str(obj['language']) != config_json['content_lan'] or str(obj['region']) != config_json['content_region']:
                language_region_invalid += 1
                continue

            type = str(obj['type'])
            if not type in ['article', 'photostory', 'slideshow']:
                    continue
            if type == 'article' and int(obj['word_count']) < 400:
                    short_content += 1
                    continue

            if len(str(obj['title'])) <= 6:
                short_content += 1
                continue

            if str(obj.get('editor_level',0)) == '-5':
                low_quality += 1
                continue

            article_list.append(obj)


        except:
            traceback.print_exc()

    logger.info("[PARSER] Filter info: %d non servable docs, %d invalid_region&language, %d short content, %d low quality_content." %(non_servable,language_region_invalid,short_content, low_quality))
    logger.info('non servable, cp_disable: {},{}'.format(non_servable,cp_disable))
    return article_list

def parse_content_dump(input_content,config_json,logger,gmp,ts):
    tmp_list = []
    content_list = []
    gmp_info = []

    article_list = filter_article(input_content,config_json,logger)

    for obj in article_list:
        try :
            item_id = str(obj['item_id'])
            publish_time = int(obj['publish_time'])
            categories = obj['categories']

            keywords = obj['entities']
            #tmp_list.append([item_id,publish_time,categories,keywords,ts]);
            tmp_list.append([item_id,publish_time,ts])
        except:
                traceback.print_exc()

    joined_gmp_cnt = 0
    for t in tmp_list:
        if t[0] in gmp:
            joined_gmp_cnt += 1
            c,ec,pop_score =  gmp.get(t[0])
            t.append(pop_score)
        else:
            t.append(0)

        content_list.append(t)


    logger.info('[PARSER] Valid doc num deduped by checksum: %d, %d with non-zero GMP \n' % (len(content_list),joined_gmp_cnt))

    #schema = {'item_id':0,'publish_time':1,'category':2,'keywords':3,'ts':4,'gmp':5}
    schema = {'item_id':0,'publish_time':1,'ts':2,'gmp':3}
    return content_list, schema





if __name__ == '__main__':


    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-c','--cp',action='store',dest='cp',help='cp content path')
    arg_parser.add_argument('-g','--gmp',action='store',dest='gmp',help='gmp content path')
    arg_parser.add_argument('-t','--timestamp',action='store',dest='timestamp',help='timestamp')
    arg_parser.add_argument('-l','--lan',action='store',dest='lan',help='language:en or hi')
    arg_parser.add_argument('-o','--output_file',action='store',dest='output_file',help='output file path')
    args = arg_parser.parse_args()

    cp = args.cp
    gmp = args.gmp
    ts = args.timestamp
    lan = args.lan
    output_file = args.output_file

    cp_content = load_dedup_content(cp,logger)
    logger.info("cp_content:" + str(len(cp_content)))
    gmp_data = load_gmp(gmp,logger,threshold=100.0)
    logger.info("gmp_data:" + str(len(gmp_data)))

    if lan == 'hi':
        config_json = {"content_lan":"hi","content_region":"in"}
    if lan == 'en':
        config_json = {"content_lan":"en","content_region":"in"}
    content_list, schema = parse_content_dump(cp_content,config_json,logger,gmp_data,ts);
    logger.info("valid content: "+ str(len(content_list)))


    with open(output_file+'.data','w') as f:
        json.dump(content_list,f)

    with open(output_file+'.schema','w') as f:
        json.dump(schema,f)

    logger.info("successfully dumped")

