export PS4='+${BASH_SOURCE}:${LINENO}:${FUNCNAME[0]}: '
gmp_file=$1
file_check_times=20
local_path=$2
for((i=0;i<$file_check_times;i++))
do
    cur_day=`date -d "$i minutes ago" +%Y%m%d`
    cur_hour=`date -d "$i minutes ago" +%H`
    cur_min=`date -d "$i minutes ago" +%M`
    cur_minute=`echo "$cur_min / 10 * 10" | bc | sed ':a;s/\(..\)/\1/;tb;s/^/0/;ta;:b'`
    if [ -e "$gmp_file/$cur_day/$cur_hour/$cur_minute".data ]
    then
        echo "download the gmp"${cur_day}/${cur_hour}/${cur_minute}
        cat "$gmp_file/$cur_day/$cur_hour/$cur_minute"".data" >> ${local_path}"/gmp.data"
        break
    else
        echo "gmp.data not ready, check $i minutes ago"
        sleep 1
    fi

done
