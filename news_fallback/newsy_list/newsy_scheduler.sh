#!/bin/sh

bin_file=`readlink -f $0`
bin_dir=`dirname $bin_file`

py_script=$bin_dir/write_newsy_content.py

export PS4='+${BASH_SOURCE}:${LINENO}:${FUNCNAME[0]}: '
# set -e

export AWS_CONFIG_FILE="/home/mengchong/.aws/config_content_india"
log_dir=../log
newsy_dir=../data
hist_data_dir=../hist_data
#config_file=./hi_news.cfg
config_file=$1
#online_dump_dir=../online_dump
log_filename=pipeline.log
interval_minutes="10"
check_interval_second=30
load_data_days=4

newsy_path_root="s3://com.cmcm.instanews.usw2.prod/cpp/feeder_signal"

tmp_dir=/tmp
#`mktemp -d -t content_feeder.XXXXXX`

function log() {
    echo "`date +'%Y%m%d %H:%M:%S'` => $*" >> $log_dir/$log_filename
}

function add_log_ts {
    while read line; do
        echo "$line" | awk '{print "'"`date +'%Y%m%d %H:%M:%S'` => $*"'",$0}'
    done
}

function get_data_no_wait() {
    remote=`echo "$1" | sed 's/\/$//g'`
    local_dir=$2
    if [[ -e $local_dir/_SUCCESS ]]; then
        return 1
    fi

    cnt=`aws s3 ls $remote/_SUCCESS | wc -l`
    if [[ $cnt -ne 0 ]]
    then
        echo "data exists $1"
        mkdir -p $local_dir
        aws s3 cp $remote/ $local_dir/ --recursive
        return 0
    fi
    return 2
}

function get_file() {
    remote=`echo "$1" | sed 's/\/$//g'`
    local_file=$2
    if [[ -e $local_file ]]; then
        return 1
    fi

    cnt=`aws s3 ls $remote/_SUCCESS | wc -l`
    tmp_cnt=`aws s3 ls $remote/_temporary | wc -l`
    tmp_dir=`mktemp -d -t newsy_tmp_file.XXXXXX`
    # echo $tmp_dir
    if [[ $cnt -ne 0 && $tmp_cnt -eq 0 ]]
    then
        echo "data exists $1"
        aws s3 cp $remote/ $tmp_dir/newsy --recursive
        cat $tmp_dir/newsy/* > $local_file
        rm -rf ${tmp_dir?}
        return 0
    fi
    rm -rf $tmp_dir
    return 2
}

mkdir -p $log_dir
mkdir -p $hist_data_dir

load_data_seconds=$((load_data_days*24*60*60))
interval_seconds=$((interval_minutes*60))
time_shift='120'
prev_key=xxxxx
prev_day=`date -d "$time_shift minutes ago" +%Y%m%d`

while :
do
    cur_ts=`date +%s`
    key=`echo "$cur_ts / $interval_seconds * $interval_seconds" | bc`
    if [[ $key == $prev_key ]]; then
        sleep $check_interval_second
        continue
    fi
    prev_key=$key

    prev_ts=`date -d "$time_shift minutes ago" +%s`
    updated_newsy=0
    ts=$cur_ts
    cur_newsy_data=""
    while [[ $ts -ge $prev_ts ]]; do
        minute=`date -d@$ts +%M`
        hour=`date -d@$ts +%H`
        day=`date -d@$ts +%Y%m%d`
        path_minute=`echo "$minute / $interval_minutes * $interval_minutes" | bc | sed ':a;s/\(..\)/\1/;tb;s/^/0/;ta;:b'`
        newsy_path="$newsy_path_root/$day/$hour/$path_minute"
        mkdir -p $newsy_dir/$day/$hour/
        cur_newsy_data="$newsy_dir/$day/$hour/$path_minute.data"
        get_file $newsy_path $cur_newsy_data

        success=$?
        if [[ $success -eq 0 || $success -eq 1 ]]; then
            log "got newsy $data_dir/$day/$hour/$path_minute"
            updated_newsy=1
            break
        else
            log "error fetching data"
        fi
        # set -e
        ts=$((ts-interval_seconds))
    done

    if [[ $updated_newsy -eq 0 ]]; then
        sleep $check_interval_second
        continue
    fi

    cur_date=`date +%Y%m%d`
    python $py_script $cur_newsy_data hot_news_multilan.json prod >> $log_dir/newsy_data.$cur_date.log 2>&1
    find ${newsy_dir?}/ -mtime +4 -print0 | xargs -0 rm -rf
    find ${log_dir?}/ -mtime +9 -print0 | xargs -0 rm -rf
    # log "writing hindi newsy list"
    # python $py_script $cur_newsy_data hi_news.cfg prod >> $log_dir/$log_filename 2>&1
    # log "writing english newsy list"
    # python $py_script $cur_newsy_data en_news.cfg prod >> $log_dir/$log_filename 2>&1
    # log "update done"

    sleep $check_interval_second

done
