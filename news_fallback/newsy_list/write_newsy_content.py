#######################

import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import traceback
import redis
import time
import local_utils
global config
import json
import traceback
import monitor
import socket

global loginfo
loginfo=[]

def int_value(val):
    try:
        return int(val)
    except:
        return -1

def load_newsy_content(path, loginfo, threshold=0.7):
    f = open(path)
    result = []
    for line in f:
        # try:
            obj = json.loads(line)
            item_id = obj['item_id']
            newsy_score = obj['newsy_score']
            if newsy_score < threshold:
                continue
            result.append(obj)
        # except:
        #     continue
    return result

def filter_conditions(obj,config_json):
    pid_flag = 1 << int(config_json["pid"])
    try:
        if obj['is_servable'] == False or obj.get("cp_disable",False):
            return False

        index_flag = obj.get("index_flag",0)
        if index_flag & pid_flag ==0:
            return False

        #region&lang incorrect
        if str(obj['language']) != config_json['content_lan'] or str(obj['region']) != config_json['content_region']:
            return False

        type = str(obj['type'])
        if not type in ['article', 'photostory', 'slideshow']:
            return False

        if str(obj.get('editor_level',0)) == '-5':
            return False
    except:
        return False

    return True

def remove_empty_str(data):
    if data is None:
        return None
    else:
        data = data.strip()
        if data == '':
            return None
        return data

def get_md5_tuple(obj):
    # check_sum = remove_empty_str(obj.get('md5',None))
    title_md5 = remove_empty_str(obj.get('title_md5',None))
    content_md5 = remove_empty_str(obj.get('content_md5',None))
    image_md5 = remove_empty_str(obj.get('image_md5',None))
    group_id = remove_empty_str(obj.get('group_id',None))
    cur_md5 = (title_md5, content_md5, image_md5, group_id)
    return cur_md5

def filter_and_distinct_by_md5(input_content, config_json, loginfo):
    non_servable = 0
    language_region_invalid = 0
    short_content = 0
    low_quality = 0

    new_cnt = add_cnt = merge_cnt = 0

    ### filter out invalid docs
    md5_maps = []
    for i in xrange(len(get_md5_tuple({}))):
        md5_maps.append({})
    article_list = []

    for obj in input_content:
        try:
            if not filter_conditions(obj,config_json):
                continue

            cur_md5 = get_md5_tuple(obj)
            cur_idxs = [None] * len(cur_md5)
            for i in xrange(len(cur_md5)):
                if cur_md5[i] is None or cur_md5[i] not in md5_maps[i]:
                    continue
                else:
                    cur_idxs[i] = md5_maps[i][cur_md5[i]]

            idx_list = sorted(set(filter(lambda x:x is not None,cur_idxs)))
            cur_idx = None
            if len(idx_list) ==0:
                article_list.append([obj])
                cur_idx = len(article_list) - 1
                for j in xrange(len(cur_md5)):
                    if cur_md5[j] is None:
                        continue
                    md5_maps[j][cur_md5[j]] = cur_idx
                new_cnt += 1
            elif len(idx_list) == 1:
                article_list[idx_list[0]].append(obj)
                cur_idx = idx_list[0]
                for j in xrange(len(cur_md5)):
                    if cur_md5[j] is None:
                        continue
                    md5_maps[j][cur_md5[j]] = cur_idx
                add_cnt += 1
            else:
                cur_idx = idx_list[0]
                for i in idx_list[1:]:
                    article_list[cur_idx] += article_list[i]
                    for obj2 in article_list[i]:
                        tmp_md5 = get_md5_tuple(obj2)
                        for j in xrange(len(tmp_md5)):
                            if tmp_md5[j] is None:
                                continue
                            md5_maps[j][tmp_md5[j]] = cur_idx

                    article_list[i] = None
                article_list[cur_idx].append(obj)

                merge_cnt += 1
        except:
            traceback.print_exc()

    loginfo.append("[PARSER] Filter info: %d non servable docs, %d invalid_region&language, %d short content, %d low quality_content." %(non_servable,language_region_invalid,short_content, low_quality))
    return article_list

def get_sort_keys(obj):
    display_type = 0 if obj.get('display_type',0) != 4 else 4
    return (-obj.get('has_copyright',0), display_type, int(obj.get('discovery_time',0)) ,int(obj.get('publish_time',0)))

if __name__ == '__main__':

    if len(sys.argv) < 3:
        print "usage: content_file config_file"
        exit(-1)

    config_map = local_utils.parse_config(sys.argv[2])
    redis_config = config_map["redis_config"]
    redis_server = config_map["redis_server"]
    # print config_json

    path = sys.argv[1]
    newsy_content = load_newsy_content(path,loginfo,threshold=0.7)
    # print newsy_content

    if len(sys.argv) < 4:
        mode = "test"
    else:
        mode = sys.argv[3]

    for config_tmp in config_map["run_configs"]:
        config_json = dict((k,v) for k,v in redis_config.items())
        config_json['redis_db_content_key_json'] = config_json['redis_db_content_key_json'] + config_tmp['pid'] + "_" + config_tmp['content_lan'] + '_' + config_tmp['content_region']
        config_json.update(config_tmp)
        dedup_content = filter_and_distinct_by_md5(newsy_content,config_json,loginfo)

        result=[]
        num_checksum = len(get_md5_tuple({}))

        for obj_list in dedup_content:
            if obj_list is None:
                continue
            obj_sorted = sorted(obj_list,key=get_sort_keys)
            grp_check_sum = []
            for idx in xrange(num_checksum):
                grp_check_sum.append(set())

            cur_cnt = 0
            for idx in xrange(len(obj_sorted)):
                cur_tuple=get_md5_tuple(obj_sorted[idx])
                dupe = False
                for tmp_idx in xrange(len(cur_tuple)):
                    if cur_tuple[tmp_idx] in grp_check_sum[tmp_idx]:
                        dupe = True
                    else:
                        grp_check_sum[tmp_idx].add(cur_tuple[tmp_idx])

                if dupe:
                    continue

                obj = obj_sorted[idx]
                cur_cnt += 1
                result.append({"id":obj['item_id'],"score":obj['newsy_score'],"md5":obj.get("content_md5",obj.get("title_md5","")),"group_id":obj.get("group_id")})

        result = sorted(result, key=lambda o: o.get('score',0), reverse=True )
        needed_count = int(config_json.get('needed_count',50))
        #if needed_count < len(dedup_content):
        #    needed_count = len(dedup_content)
        # print type(result)
        # print needed_count
        result = result[:needed_count]
        print result

        data = json.dumps({"version":"1","data":result,"timestamp":int(time.time())})

        # print config_json
        redis_port = config_json['redis_port']
        redis_password = config_json['redis_password']
        key = config_json['redis_db_content_key_json']
        if mode == "prod" or mode == "production":
            db_writer = 1
        else:
            db_writer = 4
        tag="lan="+config_json['content_lan'] + ",region=" + config_json["content_region"]

        metric = "hotnews.redis.success"
        list_size = len(result)
        monitor_data = [("hotnews.redis.size",list_size,tag)]

        for server in redis_server:
            if server.get("enable",True):
                # write_list2redis(list_map, json_map, timestamp, config_json, server["host"], config_json['redis_port'],config_json['redis_password'],mode)
                for i in xrange(3):
                    try:
                        redis_writer = redis.Redis(host=server['host'],port=redis_port, password=redis_password, db=db_writer)
                        redis_writer.set(key,data)
                        monitor_data.append((metric,1,"key="+key+",host="+server['host']))
                        print "writing to %s %s" % (server['host'],key)
                        break
                    except:
                        traceback.print_exc()

        if mode == "prod" or mode == "production":
            host=socket.gethostname()
            print host, monitor_data
            monitor_obj = monitor.monitor_server(endpoint=host,step=600)
            monitor_obj.write_falcon_with_tag(monitor_data)
        else:
            print data
