import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import traceback
import ujson

def load_gmp(path, loginfo, threshold=20.0):
    res = {}
    file = open(path)
    total_ec = 0
    total_c = 0

    for line in file:
        f = line.split('\t')
        item_id = str(f[0])
        original_ec = float(f[2])
        ec = float(f[4])
        c = float(f[5])

        if c >0 and ec >0 and original_ec > threshold and c <= ec:
            coec = c/ec
            res[item_id] = coec

        total_ec += ec
        total_c += c

    res['total'] = total_c/total_ec
    file.close()

    loginfo.append('[PARSER] Valid GMP count: %s over original ec threshold %f, average GMP: %f' % (str(len(res)), threshold, res['total']))
    return res

def load_dedup_content(path,loginfo):
    doc_num = 0
    result = []
    item_id_map = {}
    idx = 0
    f = open(path)
    for line in f:
        try:
            obj = ujson.loads(line)
            publisher=obj.get('publisher','')
            if publisher=='Glamsham' or publisher=='Cricketnmore':
                continue
            item_id = obj['item_id']
            if item_id not in item_id_map:
                item_id_map[item_id] = idx
                result.append(obj)
                idx += 1
            else:
                tmp_idx = item_id_map[item_id]
                tmp_item = result[tmp_idx]
                tmp_publish_time = tmp_item.get('update_time')
                publish_time = obj.get('update_time')
                if publish_time >= tmp_publish_time:
                    result[tmp_idx] = obj
        except:
            continue
        doc_num += 1

    loginfo.append('[PARSER] Total doc num: %s' % str(doc_num))
    loginfo.append('[PARSER] Total distinct doc num: %d \n' % len(result))

    return result


def parse_content_dump(input_content,loginfo,gmp=None):

    check_sum_set = set()
    title_md5_set = set()
    content_md5_set = set()
    image_md5_set = set()

    groupid_dict = {}
    tmp_list = []
    content_list = []


    non_servable = 0
    language_region_invalid = 0
    short_content = 0

    for obj in input_content:

        #not servable
        try:
            if obj['is_servable'] == False:
                non_servable += 1
                continue
            #region&lang incorrect
            if str(obj['language']) != 'en' or str(obj['region']) != 'in':
                language_region_invalid += 1
                continue
            if int(obj['word_count']) < 400:
                short_content += 1
                continue
            if len(str(obj['title'])) <= 6:
                short_content += 1
                continue
            type = str(obj['type'])
            if type != 'article':
                continue

            item_id = str(obj['item_id'])

            publish_time = str(obj['publish_time'])

            #if len(obj['editorial_categories']) >= 1:
                #category =  obj['editorial_categories']
            if len(obj['categories']) >=1:
                category = [t['id'] for t in obj['categories'] if 'id' in t]
                category += [t['name'] for t in obj['categories'] if 'name' in t]
            else:
                category = []

            title = str(obj['title'])

            check_sum = obj.get('md5',None)
            title_md5 = obj.get('title_md5',None)
            content_md5 = obj.get('content_md5',None)
            image_md5 = obj.get('image_md5',None)

            group_id = str(obj['group_id'])

            if len(obj['channel_names']) >= 1:
                channels = obj['channel_names']
            else:
                channels = []

            images_cnt = len(obj['images'])
            head_image = 1 if obj['head_image'] != "" else 0

            word_count = obj['word_count']
            publisher = str(obj['publisher'])
            keywords_count = len(obj['entities'])
            #has_large_image = obj['has_large_image']

            src_type = str(obj.get('source_type'))
            #features = (images_cnt, keywords_count, word_count, head_images, type, str(obj['is_servable']))
            features = {'images_cnt':images_cnt, 'keywords_count':keywords_count, 'word_count':word_count, 'head_image':head_image,'type':type, 'is_servable':str(obj['is_servable']), 'source_type':str(src_type)}

            if (check_sum and check_sum in check_sum_set) or (title_md5 and title_md5 in title_md5_set) or (content_md5 and content_md5 in content_md5_set) or (image_md5 and image_md5 in image_md5_set):
                continue
            else:

                if int(group_id) > 0 and group_id in groupid_dict:
                    groupid_dict[group_id] += 1
                else:
                    groupid_dict[group_id] = 1
                    if check_sum:
                        check_sum_set.add(check_sum)
                    if title_md5:
                        title_md5_set.add(title_md5)
                    if content_md5:
                        content_md5_set.add(content_md5)
                    if image_md5:
                        image_md5_set.add(image_md5)

                    check_sum = ','.join(str(s) for s in [check_sum,title_md5,content_md5,image_md5])
                    tmp_list.append([item_id,publisher,publish_time,channels,category,title,check_sum,group_id,features]);

        except:
            traceback.print_exc()

    joined_gmp_cnt = 0
    for t in tmp_list:
        groupid_score =  groupid_dict.get(t[7],1)
        if gmp:
            if t[0] in gmp:
                joined_gmp_cnt += 1
                pop_score =  gmp.get(t[0],0)
            else:
                pop_score =  gmp.get('total')
            t.append(pop_score)
        else:
            pop_score =  groupid_score
            t.append(pop_score)

        t[8]['groupid_score'] = groupid_score
        content_list.append(t)

    loginfo.append("[PARSER] Filter info: %d non servable docs, %d invalid_region&language, %d short content." %(non_servable,language_region_invalid,short_content))
    loginfo.append('[PARSER] Valid doc num deduped by checksum: %d, %d with non-zero GMP \n' % (len(content_list),joined_gmp_cnt))

    schema = {'item_id':0,'publisher':1,'publish_time':2,'channel_name':3,'category':4,'title':5,'check_sum':6,'group_id':7,'features':8,'pop_score':9}
    return content_list, schema

