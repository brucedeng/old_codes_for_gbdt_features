import sys,os
reload(sys)
sys.setdefaultencoding('utf-8')
from datetime import datetime
from collections import Counter
import ujson as json;

def analysis_doc_age(content_list, schema):
    doc_age_index = schema['doc_age']
    doc_age = [doc_age_bin(item[doc_age_index]) for item in content_list]
    freqs = Counter(doc_age)
    avg_doc_age = sum(item[doc_age_index] for item in content_list)/float(len(content_list))
    doc_age_dist = sorted([(k,freqs[k]) for k in freqs], key = lambda x:x[0])

    return avg_doc_age,doc_age_dist

def list2dict(output_list):
    output_dict = {}
    for i in range(len(output_list)):
        id = output_list[i][0]
        score = 1.0 - 0.0001*i
        output_dict[id] = score

    return output_dict

def list2json(output_list,schema):
    id_index = schema['item_id']
    score_index = schema['rank_score']
    feature_index = schema['features']

    output = []
    for i in range(len(output_list)):
        id = output_list[i][id_index]
        score = output_list[i][score_index]
        hot_flag = output_list[i][feature_index]['hot_news']
        output.append({'id':str(id), 'score':float(score), 'hot':hot_flag})

    out_map = {'version':'5','data':output}
    out_str = json.dumps(out_map)
    return out_str



def write_log(timestamp,channel_id,config,valid_list,expired_list,schema,loginfo,filename_postfix='',mode='verbose'):
    local_output_path = config['stat_path'] + '/' + timestamp_to_str(timestamp)
    latest_output_path = config['stat_path'] + '/' + '0_latest'

    if filename_postfix  != '':
        tmp_file_name = '.'.join([str(channel_id),'valid',filename_postfix,'txt'])
        expired_file_name = '.'.join([str(channel_id),'expired',filename_postfix,'txt'])
    else:
        tmp_file_name = '.'.join([str(channel_id),'valid','txt'])
        expired_file_name = '.'.join([str(channel_id),'expired','txt'])

    write_local_file(local_output_path, tmp_file_name,expired_file_name,valid_list,expired_list)
    write_local_file(latest_output_path, tmp_file_name,expired_file_name,valid_list,expired_list)

    if mode != 'quiet':
        print "write fallback content list into local path: %s" % local_output_path
        avg_doc_age, doc_age_dist = analysis_doc_age(valid_list,schema)
        loginfo.append('[UTIL] Average doc age for channel %s: %f' % (channel_id,avg_doc_age));
        loginfo.append('[UTIL] Doc age distribution for channel %s: %s\n' % (channel_id,str(doc_age_dist)));

def write_log_file(path, out_file, output):
    if not os.path.exists(path):
        os.mkdir(path)

    out_file = open(path + '/' + out_file,'w')
    for line in output:
        out_file.write(line+'\n')

    out_file.close()

def write_local_file(path, out_file,expire_file,output,expire):

    if not os.path.exists(path):
        os.mkdir(path)

    out_file = open(path + '/' + out_file,'w')
    #expire_file = open(path + '/' + expire_file,'w')

    valid_list = output
    #expire_list = expire

    for item in valid_list:
        s = '\t'.join(str(t) for t in item)
        out_file.write(s+'\n')

    #for item in expire_list:
        #s = '\t'.join(str(t) for t in item)
        #expire_file.write(s+'\n')

    out_file.close()
    #expire_file.close()

def timestamp_to_str(ts):
    dt = datetime.fromtimestamp(ts)
    return dt.strftime('%Y%m%d_%H%M')

def doc_age_bin(hour):
    if hour <= 1:
        return "1#<1h"
    elif hour <= 4:
        return "2#1-4h"
    elif hour <= 24:
        return "3#4-24h"
    elif hour <= 48:
        return "4#24-48h"
    else:
        return "5#>2d"
