#######################
#
# Update: 1. write a json version fallback list
#         2. update fine-tuned gmp and other doc features
#
#
#######################
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import traceback
import time
import traceback
import argparse
import datetime
import monitor
from data_parser import *

global loginfo
loginfo=[]

global host
import socket
prod_mode = False
try:
    host=socket.gethostname()
except:
    host='can_not_detect'

def load_gmp(path, loginfo, threshold=20.0):
    res = {}
    file = open(path)
    total_ec = 0
    total_c = 0

    for line in file:
        f = line.split('\t')
        item_id = str(f[0])
        original_ec = float(f[2])
        ec = float(f[4])
        c = float(f[5])

        language = f[6]
        if len(f) >= 9:
            country=f[7]
        else:
            country = ""

        if c >0 and ec >0 and original_ec > threshold and c <= ec:
            region_key = (language,country)
            country = country.lower()
            if region_key not in res:
                res[region_key] = {}
            coec = c/ec
            res[region_key][item_id] = (c,ec,coec,original_ec)

        total_ec += ec
        total_c += c

    # res['total'] = (total_c,total_ec,total_c/total_ec)
    file.close()

    # loginfo.append('[PARSER] Valid GMP count: %s over original ec threshold %f, average GMP: %s' % (str(len(res)), threshold, str(res['total'])))
    return res

if __name__ == '__main__':

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('prev_gmp', metavar='prev_gmp', help='previous data')
    arg_parser.add_argument('current_gmp',metavar='current_gmp', help='current data')
    arg_parser.add_argument('-t', '--timestamp', action='store', dest='timestamp', help='timestamp in yyyymmddHHMM format')
    arg_parser.add_argument('-i', '--interval', action='store', dest='interval', default='10m', help='interval in seconds')
    arg_parser.add_argument('-l', '--label', action='store', dest='label', default='en', help='language label, en or hi')
    arg_parser.add_argument('-H','--threshold', action='store', dest='threshold',type=int,default=100, help='gmp threshold')

    args = arg_parser.parse_args()

    threshold = args.threshold
    label = args.label
    interval = None
    if args.interval.endswith('m'):
        interval_m=60
        value=int(args.interval.replace('m',''))
        interval=value * interval_m
    elif args.interval.endswith('d'):
        interval_m=86400
        value=int(args.interval.replace('d',''))
        interval=value * interval_m

    if args.timestamp is not None:
        timestamp = datetime.datetime.strptime(args.timestamp,'%Y%m%d%H%M')
    else:
        timestamp = datetime.datetime.now()

    ts = int(timestamp.strftime('%s'))

    prev_gmp = load_gmp(args.prev_gmp,loginfo, -1)
    cur_gmp = load_gmp(args.current_gmp, loginfo, -1)
    # print cur_gmp
    prev_all_gmp = {}

    for gmp_map in prev_gmp.values():
        # print gmp_map
        prev_all_gmp.update(filter(lambda x : x[1][3]>=threshold, gmp_map.items()))
    if len(prev_all_gmp) < 5:
        sys.exit()

    result = []
    lan_region_whitelist = set(["hi_in","en_in","ta_in","en_us","en_gb","fr_fr","es_es","de_de","it_it","ru_ru","pt_br","en_ca","zh_tw"])
    country_whitelist = set(["in","us"])
    for (key,gmp_map) in cur_gmp.items():
        (lan,country) = key
        if lan.lower() + '_' + country.lower() not in lan_region_whitelist:
            continue
        cnt = 0
        valid_cnt = 0
        for item_id in gmp_map:
            if gmp_map[item_id][3] >= threshold:
                valid_cnt += 1
                if item_id not in prev_all_gmp:
                    cnt +=1
        result.append((key,(cnt,valid_cnt,len(gmp_map))))
        print "new gmp count for %s is %d" % (str(key),cnt)

    monitor_obj = monitor.monitor_server(endpoint=host,step=600,ts=ts)
    monitor_values = []
    for item in result:
        (key,cnt_tuple) = item
        (language,country) = key
        (cnt,valid_cnt,all_cnt) = cnt_tuple
        monitor_values.append(("gmp.exploration.count",valid_cnt,'threshold=%d,lan=%s,region=%s'%(threshold,language,country)) )
        monitor_values.append(("gmp.exploration.invalid",all_cnt-valid_cnt,'threshold=%d,lan=%s,region=%s'%(threshold,language,country)) )
        if country.lower() == "in":
            monitor_values.append(("gmp.exploration.test."+ args.interval,cnt,'threshold=%d,lan=%s'%(threshold,language)) )
        else:
            monitor_values.append(("gmp.exploration.test."+ args.interval,cnt,'threshold=%d,lan=%s,region=%s'%(threshold,language,country)) )
    monitor_obj.write_falcon_with_tag(monitor_values)
    print 'write success'
    # import time
    # time.sleep(605)
