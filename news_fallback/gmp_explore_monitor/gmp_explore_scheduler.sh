#!/bin/sh

bin_file=`readlink -f $0`
bin_dir=`dirname $bin_file`

py_script=$bin_dir/monitor_gmp_explore.py

rerun_period_sec=$1
if [[ a"$rerun_period_sec" == "a" ]]; then
    rerun_period_sec=0
fi

export PS4='+${BASH_SOURCE}:${LINENO}:${FUNCNAME[0]}: '
# set -e

export AWS_CONFIG_FILE="/home/mengchong/.aws/config_content_india"
log_dir=../log
data_dir=../data
gmp_dir=../gmp
hist_data_dir=../hist_data
online_dump_dir=../online_dump
log_filename=pipeline.log
interval_minutes="10"
check_interval_second=30
load_data_days=4

#content_path_root="s3://com.cmcm.instanews.usw2.prod/contents"
content_path_root="s3://com.cmcm.instanews.usw2.prod/cpp/feeder"
gmp_path_root="s3://com.cmcm.instanews.usw2.prod/nrt/gmp"
gmp_path_root="hdfs://mycluster/projects/news/nrt/india_group_gmp/dump"

tmp_dir=/tmp
#`mktemp -d -t content_feeder.XXXXXX`

function log() {
    echo "`date +'%Y%m%d %H:%M:%S'` => $*" >> $log_dir/$log_filename
}

function add_log_ts {
    while read line; do
        echo "$line" | awk '{print "'"`date +'%Y%m%d %H:%M:%S'` => $*"'",$0}'
    done
}

function get_file_wait() {
    remote=`echo "$1" | sed 's/\/$//g'`
    local_file=$2
    if [[ -e $local_file ]]; then
        return 1
    fi

    tmp_dir=`mktemp -d -t gmp_tmp_file.XXXXXX`
    # echo $tmp_dir
    for (( i=0;i<80;i++ )); do
        cnt=`aws s3 ls $remote/_SUCCESS | wc -l`
        tmp_cnt=`aws s3 ls $remote/_temporary | wc -l`
        if [[ $cnt -ne 0 ]]
        then
            if [[ $tmp_cnt -ne 0 ]]; then sleep 60; fi
            echo "data exists $1"
            for file in `aws s3 ls $remote/ | grep -v "Found" | awk '{print $4}'`
            do
                if [[ "$file" == "_temporary" ]]; then
                    continue
                fi
                aws s3 cp $remote/$file $tmp_dir/
                cat $tmp_dir/$file >> $local_file
            done
            # aws s3 cp $remote/ $tmp_dir/gmp --recursive
            # cat $tmp_dir/gmp/* > $local_file
            rm -rf ${tmp_dir?}
            return 0
        else
            sleep 30
        fi
    done
    rm -rf ${tmp_dir?}
    return 2
}

function get_hdfs_file_wait() {
    remote=`echo "$1" | sed 's/\/$//g'`
    local_file=$2
    if [[ -e $local_file ]]; then
        return 1
    fi

    for (( i=0;i<80;i++ )); do
        cnt=`hadoop fs -ls $remote/_SUCCESS | wc -l`
        if [[ $cnt -ne 0 ]]
        then
            if [[ $tmp_cnt -ne 0 ]]; then sleep 60; fi
            echo "data exists $1"
            for ((j=0;j<5;j++)); do
                hadoop fs -cat $remote/* > $local_file
                if [[ $? -eq 0 ]]; then
                    return 0
                fi
            done
            return 2
        else
            sleep 30
        fi
    done
    return 2
}

mkdir -p $log_dir
mkdir -p $data_dir
mkdir -p $hist_data_dir

load_data_seconds=$((load_data_days*24*60*60))
interval_seconds=$((interval_minutes*60))
time_shift='120'
prev_key=xxxxx
prev_day=`date -d "$time_shift minutes ago" +%Y%m%d`

cur_ts=`date +%s`
run_time_ts=`echo "$cur_ts / $interval_seconds * $interval_seconds - $interval_seconds - $rerun_period_sec" | bc`
while :
do
    cur_ts=`date +%s`
    if [[ $cur_ts -lt $run_time_ts ]]; then
        sleep $check_interval_second
        continue
    fi
    run_time_ts=$((run_time_ts+interval_seconds))
    if [[ $run_time_ts == $prev_key ]]; then
        sleep $check_interval_second
        continue
    fi
    prev_key=$run_time_ts

    updated_gmp=0
    ts=$run_time_ts

    minute=`date -d@$ts +%M`
    hour=`date -d@$ts +%H`
    day=`date -d@$ts +%Y%m%d`
    path_minute=`echo "$minute / $interval_minutes * $interval_minutes" | bc | sed ':a;s/\(..\)/\1/;tb;s/^/0/;ta;:b'`
    gmp_path="$gmp_path_root/$day/$hour$path_minute"
    mkdir -p $gmp_dir/$day/$hour/
    cur_gmp_data="$gmp_dir/$day/$hour/$path_minute.data"
    get_hdfs_file_wait $gmp_path $cur_gmp_data
    success=$?

    timestamp=$day$hour$path_minute

    prev_ts=$((ts-interval_seconds))
    minute=`date -d@$prev_ts +%M`
    hour=`date -d@$prev_ts +%H`
    day=`date -d@$prev_ts +%Y%m%d`
    path_minute=`echo "$minute / $interval_minutes * $interval_minutes" | bc | sed ':a;s/\(..\)/\1/;tb;s/^/0/;ta;:b'`
    gmp_path="$gmp_path_root/$day/$hour$path_minute"
    prev_gmp_data="$gmp_dir/$day/$hour/$path_minute.data"
    mkdir -p $gmp_dir/$day/$hour/
    get_hdfs_file_wait $gmp_path $prev_gmp_data

    if [[ $success -eq 0 || $success -eq 1 ]]; then
        log "got gmp $data_dir/$day/$hour/$path_minute"
        python $py_script -t "$timestamp" -H 20 $prev_gmp_data $cur_gmp_data
        python $py_script -t "$timestamp" -H 50 $prev_gmp_data $cur_gmp_data
        python $py_script -t "$timestamp" -H 100 $prev_gmp_data $cur_gmp_data
    else
        log "error fetching data"
    fi

    prev_day_ts=$((run_time_ts-86400))
    minute=`date -d@$prev_day_ts +%M`
    hour=`date -d@$prev_day_ts +%H`
    day=`date -d@$prev_day_ts +%Y%m%d`
    path_minute=`echo "$minute / $interval_minutes * $interval_minutes" | bc | sed ':a;s/\(..\)/\1/;tb;s/^/0/;ta;:b'`
    gmp_path="$gmp_path_root/$day/$hour$path_minute"
    prev_gmp_data="$gmp_dir/$day/$hour/$path_minute.data"
    mkdir -p $gmp_dir/$day/$hour/
    get_hdfs_file_wait $gmp_path $prev_gmp_data
    if [[ $success -eq 0 || $success -eq 1 ]]; then
        log "got gmp $data_dir/$day/$hour/$path_minute"
        python $py_script -t "$timestamp" -H 20 -i 1d $prev_gmp_data $cur_gmp_data
        python $py_script -t "$timestamp" -H 50 -i 1d $prev_gmp_data $cur_gmp_data
        python $py_script -t "$timestamp" -H 100 -i 1d $prev_gmp_data $cur_gmp_data
    else
        log "error fetching data"
    fi
    find $gmp_dir -mtime 9 -exec rm -rf {} \;

done
