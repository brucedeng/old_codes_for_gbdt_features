#!/bin/sh

log_file=$1

function rotate {
    target=$1
    size=`du $target | awk '{print $1}'`
    if [[ $size -gt 102400 ]]; then
        mv $target $target.2
    fi
}

cnt=0

while read line; do
    cnt=$((cnt + 1))
    if [[ $cnt -gt 10000 ]]; then
        cnt=0
        rotate $log_file
    fi
    echo "$line" >> $log_file
done
