#!/bin/sh

bin_file=`readlink -f $0`
bin_dir=`dirname $bin_file`

export PS4='+${BASH_SOURCE}:${LINENO}:${FUNCNAME[0]}: '
# set -e

spark_path="/data/news_workflow/spark-1.6.0-runtime"
export PATH=$spark_path/bin:/data/guozhenyuan/anaconda/bin:$PATH
export AWS_CONFIG_FILE="/home/dulimei/.aws/config_content_india"
log_dir=../logs
dump_dir=../online_dump

log_filename=pipeline.log
interval_minutes="10"
check_interval_second=30
load_data_days=7

content_path_root="hdfs://mycluster/projects/news/cpp/feeder/in_cp_dump"
gmp_path_root="hdfs://mycluster/projects/news/nrt/india_group_gmp/dump"
output_data_root="hdfs:///projects/news/model/offline/fallback_nopid_new"


function log() {
    echo "`date +'%Y%m%d %H:%M:%S'` => $*" >> $log_dir/$log_filename
}

function add_log_ts {
    while read line; do
        echo "$line" | awk '{print "'"`date +'%Y%m%d %H:%M:%S'` => $*"'",$0}'
    done
}

function check_hdfs_file() {
    remote=`echo "$1" | sed 's/\/$//g'`

    cnt=`hadoop fs -ls $remote/_SUCCESS | wc -l`
    if [[ $cnt -ne 0 && $tmp_cnt -eq 0 ]]
    then
        echo "data exists $1"
        # hadoop fs -cat $remote/* > $local_file
        return 0
    fi
    return 2
}

mkdir -p $log_dir

load_data_seconds=$((load_data_days*24*60*60))
interval_seconds=$((interval_minutes*60))
time_shift='1200'
prev_key=xxxxx
prev_day=`date -d "$time_shift minutes ago" +%Y%m%d`

while :
do
    
    cur_ts=`date +%s`
    key=`echo "$cur_ts / $interval_seconds * $interval_seconds" | bc`
    if [[ $key == $prev_key ]]; then
        sleep $check_interval_second
        continue
    fi
    prev_key=$key

    prev_ts=`date -d "$time_shift minutes ago" +%s`
    updated_gmp=0
    ts=$cur_ts
    while [[ $ts -ge $prev_ts ]]; do
        minute=`date -d@$ts +%M`
        hour=`date -d@$ts +%H`
        day=`date -d@$ts +%Y%m%d`
        path_minute=`echo "$minute / $interval_minutes * $interval_minutes" | bc | sed ':a;s/\(..\)/\1/;tb;s/^/0/;ta;:b'`
        gmp_path="$gmp_path_root/$day/$hour$path_minute"
        check_hdfs_file $gmp_path

        success=$?
        if [[ $success -eq 0 ]]; then
            log "got gmp $gmp_path"
            updated_gmp=1
            break
        else
            log "error fetching data"
        fi
        # set -e
        ts=$((ts-interval_seconds))
    done

    if [[ $updated_gmp -eq 0 ]]; then
        sleep $check_interval_second
        continue
    fi

    minute=`date -d@$ts +%M`
    hour=`date -d@$ts +%H`
    day=`date -d@$ts +%Y%m%d`
    path_minute=`echo "$minute / $interval_minutes * $interval_minutes" | bc | sed ':a;s/\(..\)/\1/;tb;s/^/0/;ta;:b'`
    log "pulling data for $day $hour $path_minute"
    content_path="$content_path_root/$day/$hour/$path_minute"

    for ((idx=0;idx<100; idx++)); do
        check_hdfs_file "$content_path"
        success=$?
        if [[ $success -eq 0 ]]; then
            log "got gmp $gmp_path"
            updated_gmp=1
            break
        else
            log "data $content_path does not exist"
            sleep $check_interval_second
        fi
    done

    ftr_start=`date -d "+$load_data_days day ago $day" +%Y%m%d`
    ftr_end=$day
    agg_dates=$ftr_start
    for ((agg_day="`date -d \"+1 day $ftr_start\" +%Y%m%d`"; $agg_day <= $ftr_end; agg_day="`date -d \"+1 day $agg_day\" +%Y%m%d`")); do  agg_dates="$agg_dates,$agg_day"; done

    input_contents="$content_path_root/{$agg_dates}/*/*"
    output_data="$output_data_root/$day/$hour/$path_minute"
    input_gmp=$gmp_path
    output_dump="$dump_dir/${day}_$hour$path_minute.txt"

    cmd="spark-submit --master yarn-client  --executor-cores 4 --executor-memory 2g --num-executors 30 --queue offline --class com.cmcm.ranking.fallback.FallbackApp fallback_pipeline-0.1-SNAPSHOT-jar-with-dependencies.jar 'fallback.conf' input_cp='$input_contents' input_gmp='$input_gmp' output='$output_data'"
    log "$cmd"
    eval $cmd >> $log_dir/$log_filename 2> $log_dir/spark_${day}_$hour$path_minute.log

    target=$log_dir/$log_filename
    size=`du $target | awk '{print $1}'`
    if [[ $size -gt 102400 ]]; then
        mv $target $target.2
    fi

    find ${log_dir?}/ -mtime +7 -print0 | xargs -0 rm -rf
    find ${log_dir?}/ -mtime +1 -name 'spark_*' -print0 | xargs -0 rm -rf

    expire_day=`date -d "+9 day ago $day" +%Y%m%d`
    cmd2="hadoop fs -rm -r -skipTrash $output_data_root/$expire_day"
    log "$cmd2"

    sleep $check_interval_second

done
