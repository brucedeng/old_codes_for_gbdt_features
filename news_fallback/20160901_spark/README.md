https://docs.google.com/document/d/1arAMR5BGjn8sfUlrcNGEMiRmsAFH8FPNNgpERzqTsu0/edit#

spark生成的fallback列表

监控 ：http://falconus.liebaopay.com:8042/screen/1345

机器: 训练机 10.2.99.7(launcher-news-model-offline.hadoop.usw2.contents.cmcm.com)

git:
http://git.liebaopay.com/cmnews/india_news_ranking/tree/master/news_fallback/20160901_spark

http://git.liebaopay.com/cmnews/india_news_ranking/tree/master/spark/fallback_pipeline

路径: /data/news_workflow/article_fallback_pipeline_spark/scripts

运行方法: nohup bash -x fallback_content_scheduler_v3.sh 2>&1  | ./log_events.sh run.log  & #( 需要用news_model 用户起job )

脚本: gmp_explore_scheduler.sh

配置文件: prodiction.conf

whitelist 配置方法: 多组配置以逗号”,”隔开, 每组内部, 以冒号隔开三列, 依次为language, region, pid, 另外, 这三个字段每个都可以配置多个值, 以”/” 隔开. 例如: “en/hi/ta:in:11/14,en/es:us:1” 这组配置, 对应的语言国家的组合为: