#!/usr/bin/env bash
REDIS_HOSTS="feature-server-test.mkjqd2.0001.apse1.cache.amazonaws.com,feature-server.bgkplr.ng.0001.usw2.cache.amazonaws.com,feature-server.6gvwka.ng.0001.euw1.cache.amazonaws.com,feature-server.mkjqd2.ng.0001.apse1.cache.amazonaws.com,instanews.bgkplr.ng.0001.usw2.cache.amazonaws.com,instanews.6gvwka.ng.0001.euw1.cache.amazonaws.com,instanews.mkjqd2.ng.0001.apse1.cache.amazonaws.com"
#REDIS_HOSTS="localhost"
REDIS_HOSTS=${REDIS_HOSTS//,/ }
for REDIS_HOST in $REDIS_HOSTS
do
    for DB in {0..5}
    do
        redis-cli -h $REDIS_HOST -n $DB < delete_redis.txt
    done
done
