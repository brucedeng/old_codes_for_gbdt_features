import sys,os
reload(sys)
sys.setdefaultencoding('utf-8')
from datetime import datetime
from collections import Counter
import ujson as json;
import socket
import re
import monitor
# import ConfigParser
import json

def parse_config(config_file):
    f = open(config_file)
    data = f.read()
    return json.loads(data)

def analysis_doc_age(content_list, schema):
    if len(content_list)==0:
        return 0, []
    doc_age_index = schema['doc_age']
    doc_age = [doc_age_bin(item[doc_age_index]) for item in content_list]
    freqs = Counter(doc_age)
    avg_doc_age = sum(item[doc_age_index] for item in content_list)/float(len(content_list))
    doc_age_dist = sorted([(k,freqs[k]) for k in freqs], key = lambda x:x[0])

    return avg_doc_age,doc_age_dist

def list2dict(output_list):
    output_dict = {}
    for i in range(len(output_list)):
        id = output_list[i][0]
        score = 1.0 - 0.0001*i
        output_dict[id] = score

    return output_dict

def list2json(output_list,schema):
    id_index = schema['item_id']
    score_index = schema['rank_score']
    feature_index = schema['features']

    output = []
    for i in range(len(output_list)):
        id = output_list[i][id_index]
        score = output_list[i][score_index]
        hot_flag = output_list[i][feature_index]['hot_news']
        output.append({'id':str(id), 'score':float(score), 'hot':hot_flag})

    out_map = {'version':'5','data':output}
    out_str = json.dumps(out_map)
    return out_str



def write_log(timestamp,channel_id,config,valid_list,expired_list,schema,loginfo,filename_postfix='',mode='verbose',filename_prefix=""):
    local_output_path = config['stat_path'] + '/' + timestamp_to_str(timestamp)
    latest_output_path = config['stat_path'] + '/' + '0_latest'

    if filename_postfix  != '':
        tmp_file_name = '.'.join([str(channel_id),'valid',filename_postfix,'txt'])
        expired_file_name = '.'.join([str(channel_id),'expired',filename_postfix,'txt'])
    else:
        tmp_file_name = '.'.join([str(channel_id),'valid','txt'])
        expired_file_name = '.'.join([str(channel_id),'expired','txt'])

    write_local_file(local_output_path, filename_prefix+tmp_file_name,filename_prefix+expired_file_name,valid_list,expired_list)
    write_local_file(latest_output_path, filename_prefix+tmp_file_name,filename_prefix+expired_file_name,valid_list,expired_list)

    if mode != 'quiet':
        # print "write fallback content list into local path: %s" % local_output_path
        avg_doc_age, doc_age_dist = analysis_doc_age(valid_list,schema)
        loginfo.append('[UTIL] Average doc age for channel %s: %f' % (channel_id,avg_doc_age));
        loginfo.append('[UTIL] Doc age distribution for channel %s: %s\n' % (channel_id,str(doc_age_dist)));

global regex
regex=lambda x:x+1
regex.total_doc_num=re.compile('\\[PARSER\\] Total distinct doc num:\\s*(\\d+)\\s*')
regex.valid_dedup_num=re.compile('\\[PARSER\\] Valid doc num deduped by checksum:\\s*(\\d+)\\s*,\\s*(\\d+)\\s*with non-zero GMP')
regex.channel_num=re.compile('\\[RANKING\\] Channel\\s*([^ :]+)\\s*:\\s*(\\d+)\\s*valid docs,\\s*(\\d+)\\s*expired docs.')
regex.avg_doc_age=re.compile('\\[UTIL\\] Average doc age for channel\\s*([^ :]+)\\s*:\\s*([0-9.]+)\\s*')
regex.redis=re.compile("\\[Redis\\] Fallback id list redis status:\\s*(\\d+)\\s*db:\\s*(\\d+)\\s*host:\\s*([^ ]+)\\s*")
def write_monitor(output,lan='en_in',pid="11"):
    host=socket.gethostname()
    result=[]
    lan_tag = ',lan=' + lan + ",pid=" + pid

    for line in output:
        matched = regex.total_doc_num.match(line)
        if matched is not None:
            data=matched.groups()
            cnt=int(data[0])
            result.append(('fallback.multilan.overall', cnt,'channel=all' + lan_tag))
            continue
        matched = regex.valid_dedup_num.match(line)
        if matched is not None:
            data = matched.groups()
            total_num=int(data[0])
            non_zero=int(data[1])
            result.append(('fallback.multilan.deduped', total_num, 'channel=all' + lan_tag))
            result.append(('fallback.multilan.non_zero_gmp', non_zero, 'channel=all' + lan_tag))

        matched = regex.channel_num.match(line)
        if matched is not None:
            data = matched.groups()
            channel = data[0]
            valid = int(data[1])
            expire = int(data[2])
            total=valid+expire
            if total==0:
                total = 1
            result.append(('fallback.multilan.valid',valid, 'channel='+channel+lan_tag))
            result.append(('fallback.multilan.valid_ratio',float(valid)/total, 'channel=' + channel + lan_tag))

        matched = regex.avg_doc_age.match(line)
        if matched is not None:
            data = matched.groups()
            channel = data[0]
            avg_age = float(data[1])
            result.append(('fallback.multilan.doc_age', avg_age, 'channel=' + channel + lan_tag))
        matched = regex.redis.match(line)
        if matched is not None:
            data = matched.groups()
            (value,db,redis_host) = data
            value = float(value)
            result.append(("fallback.multilan.redis",value, "channel=all,host="+redis_host + lan_tag))

    print result
    monitor_obj = monitor.monitor_server(endpoint=host,step=600)
    monitor_obj.write_falcon_with_tag(result)

def write_log_file(path, out_file, output):
    if not os.path.exists(path):
        os.makedirs(path)

    out_file = open(path + '/' + out_file,'w')
    for line in output:
        out_file.write(line+'\n')

    out_file.close()

def write_gmp_file(path, out_file, gmp_info):
    if not os.path.exists(path):
        os.mkdir(path)

    out_file = open(path + '/' + out_file,'w')

    gmp_info = sorted(gmp_info, key = lambda x:x[1], reverse=True)

    for item in gmp_info:
        line = "%s\t\t%.4f\t%.0f\t%.0f\t%.0f\t%s\t%s" % item
        out_file.write(line+'\n')

    out_file.close()


def write_local_file(path, out_file,expire_file,output,expire):

    if not os.path.exists(path):
        os.mkdir(path)

    out_file = open(path + '/' + out_file,'w')
    #expire_file = open(path + '/' + expire_file,'w')

    valid_list = output
    #expire_list = expire

    for item in valid_list:
        s = '\t'.join(str(t) for t in item)
        out_file.write(s+'\n')

    #for item in expire_list:
        #s = '\t'.join(str(t) for t in item)
        #expire_file.write(s+'\n')

    out_file.close()
    #expire_file.close()

def timestamp_to_str(ts):
    dt = datetime.fromtimestamp(ts)
    return dt.strftime('%Y%m%d_%H%M')

def doc_age_bin(hour):
    if hour <= 1:
        return "1#<1h"
    elif hour <= 4:
        return "2#1-4h"
    elif hour <= 24:
        return "3#4-24h"
    elif hour <= 48:
        return "4#24-48h"
    else:
        return "5#>2d"

def check_servable_gmp(gmp_info,list_map):
    servable_id = []
    for key in list_map:
        servable_id += [t[0] for t in list_map[key][1]]

    servable_gmp_info = filter(lambda x:x[0] in servable_id, gmp_info)

    return servable_gmp_info

