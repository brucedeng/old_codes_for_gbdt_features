#######################
#
# Update: 1. write a json version fallback list
#         2. update fine-tuned gmp and other doc features
#
#
#######################
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import traceback
import redis
import time
import local_utils
import data_parser
import ranking_utils
#import traceback

global loginfo
loginfo=[]


def write_list2redis(list_map,json_map,timestamp,config_json,redis_host,redis_port,redis_password,mode='test'):
    try:
    #prepare redis client
        if mode == 'prod':
            redis_meta = redis.Redis(host=redis_host,port=redis_port, password=redis_password, db=0)
            db_mark = redis_meta.get(config_json['redis_db_mark'])
            print "db_mark is ",db_mark
            db_writer = 2 if db_mark == '1' else 1
            if db_writer is None:
                db_writer = 1
        elif mode == "test":
            db_writer = 4
        else:
            print "Unkonw mode [prod|test], exit.."
            return

        redis_writer = redis.Redis(host=redis_host,port=redis_port, password=redis_password, db=db_writer)
        dummpy_list = []
        redis_pipe = redis_writer.pipeline()

        # process channel content list
        for channel_id in sorted(list_map.keys()):
            channel_key = list_map[channel_id][0]
            channel_list = list_map[channel_id][1]
            print "writing to key ",channel_key
            channel_out_1d = local_utils.list2dict(channel_list)

            redis_pipe.delete(channel_key)
            redis_pipe.zadd(channel_key, *dummpy_list,**channel_out_1d)

        for channel_id in sorted(json_map.keys()):
            channel_key = json_map[channel_id][0]
            channel_json = json_map[channel_id][1]
            redis_pipe.delete(channel_key)
            redis_pipe.set(channel_key,channel_json)


        redis_pipe.execute()

        if mode == 'prod':
            redis_meta.set(config_json['redis_db_mark'], db_writer)
            redis_meta.set(config_json['redis_db_last_modify_time'], timestamp)

        # print redis_writer.zrangebyscore(redis_content_channel_key_1d,0.999,1)
        print "[Time:%s,Mode:%s] Fallback content id list has been generated and written to redis: %s @db:%s" % (str(timestamp), mode, redis_host, db_writer)
        loginfo.append("[Redis] Fallback id list redis status:1 db:%s host:%s" % (db_writer,redis_host))

    except:
        loginfo.append("[Redis] Fallback id list redis status:0 db:%s host:%s" % (db_writer,redis_host))
        traceback.print_exc()
        pass

def int_value(val):
    try:
        return int(val)
    except:
        return -1

def gen_redis_data(content_list,schema,channels,timestamp,config_json,filename_prefix=""):
    try:

        key_prefix = config_json['redis_db_content_key']
        key_prefix_json = config_json['redis_db_content_key_json']
        #key_prefix_json_v2 = config_json['redis_db_content_key_json_v2']

        redis_content_value_1d_threshold = 1000

        list_map = {}
        json_map = {}

        # process channel content list
        for channel in sorted(channels.items(),key=lambda t:int_value(t[0])):
            channel_id = channel[0]
            channel_out = ranking_utils.get_channel_by_conf(content_list,schema,channel,timestamp,config_json,loginfo,rank_method='linear',explore_budget=-1)

            schema = channel_out[2]

            #diversified list for raw list
            redis_content_diversified, schema = ranking_utils.diversify(channel_out[0],schema)
            redis_content_diversified = redis_content_diversified[:redis_content_value_1d_threshold]

            #un-diversified list for json list
            redis_content_origin = channel_out[0][:redis_content_value_1d_threshold]
            channel_out_1d_json = local_utils.list2json(redis_content_origin,schema)

            if len(redis_content_diversified) and len(redis_content_origin) >= 20:
                #format redis key
                list_map[channel_id] = (key_prefix + str(channel_id), redis_content_diversified)
                json_map[channel_id] = (key_prefix_json + str(channel_id), channel_out_1d_json)
            else:
                print "channel list too short for " + channel_id

            local_utils.write_log(timestamp,channel_id,config_json,redis_content_diversified,channel_out[1][:redis_content_value_1d_threshold],schema,loginfo,filename_postfix='div', mode='quiet',filename_prefix=filename_prefix)
            local_utils.write_log(timestamp,channel_id,config_json,redis_content_origin,channel_out[1][:redis_content_value_1d_threshold],schema,loginfo,filename_postfix='origin',filename_prefix=filename_prefix)

        return list_map, json_map


    except:
        traceback.print_exc()
        pass

if __name__ == '__main__':

    if len(sys.argv) < 4:
        print "usage: content_file gmp_file config_file"
        exit(-1)

    config_map = local_utils.parse_config(sys.argv[3])
    channel_configs = config_map["channel_configs"]
    redis_config = config_map["redis_config"]
    redis_server = config_map["redis_server"]

    path = sys.argv[1]
    dedup_content = data_parser.load_dedup_content(path,loginfo)
    print "deduped content: " + str(len(dedup_content))

    gmp_path = sys.argv[2]
    gmp_data = data_parser.load_gmp(gmp_path,loginfo,threshold=20.0)
    print "valid gmp: " + str(len(gmp_data))

    if len(sys.argv) >= 5:
        mode = sys.argv[4]
    else:
        mode = 'test'

    loginfo_old = [x for x in loginfo]

    start_ts = int(time.time())

    for config_json in config_map["run_configs"]:
        loginfo = [x for x in loginfo_old]
        channel_config = channel_configs[config_json["channel"]]
        config_json = dict((k,v) for k,v in config_json.items()+redis_config.items())
        lan_region_prefix = "%s_%s_%s_" % (config_json["content_lan"], config_json["content_region"],config_json["pid"])
        config_json["redis_db_content_key"] = lan_region_prefix + redis_config["redis_db_content_key"]
        config_json["redis_db_content_key_json"] = lan_region_prefix + redis_config["redis_db_content_key_json"]
        config_json["redis_db_last_modify_time"] = lan_region_prefix + redis_config["redis_db_last_modify_time"]
        config_json["redis_db_mark"] = lan_region_prefix + redis_config["redis_db_mark"]
        config_json["redis_db_last_modify_time"] = lan_region_prefix + redis_config["redis_db_last_modify_time"]

        local_output_path = config_json['stat_path'] + '/' + local_utils.timestamp_to_str(start_ts)
        latest_output_path = config_json['stat_path'] + '/' + '0_latest'
        print local_output_path

        content_list,schema,gmp_info = data_parser.parse_content_dump(dedup_content,config_json,loginfo,gmp=gmp_data);
        print "valid content: "+ str(len(content_list))

        timestamp = int(time.time())
        #list_map, json_map =  gen_redis_data(content_list,schema,channel_config,timestamp,config_json,filename_prefix=lan_region_prefix)
        list_map, json_map =  gen_redis_data(content_list,schema,channel_config,start_ts,config_json,filename_prefix=lan_region_prefix)

        for server in redis_server:
            if server.get("enable",True):
                write_list2redis(list_map, json_map, timestamp, config_json, server["host"], config_json['redis_port'],config_json['redis_password'],mode)

        loginfo.append('[MAIN] Last update time: %s' % local_utils.timestamp_to_str(timestamp))

        tmp_file_name = lan_region_prefix + 'result_info.txt'

        local_utils.write_log_file(local_output_path, tmp_file_name, loginfo)
        local_utils.write_log_file(latest_output_path, tmp_file_name, loginfo)
        try:
            if mode == 'test':
                pass
            else:
                local_utils.write_monitor(loginfo,config_json['log_lan'],config_json['pid'])
        except:
            traceback.print_exc()

        gmp_file_name=lan_region_prefix + 'gmp_info.txt'
        gmp_info = local_utils.check_servable_gmp(gmp_info,list_map)
        local_utils.write_gmp_file(local_output_path, gmp_file_name, gmp_info)
        local_utils.write_gmp_file(latest_output_path, gmp_file_name, gmp_info)

        print schema
