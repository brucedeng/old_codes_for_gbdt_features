import sys
#import json


files = (sys.argv[1:])

for fname in files:
    fs = open(fname)

    line_n = 0
    for line in fs:
        line_n += 1

        if line_n > 1000:
            break;

        f = line.split('\t')

        item_id = f[0]
        publisher = f[1]

        feature = f[8]
        gmp = float(f[9])
        doc_age = float(f[10])
        score = float(f[11])

        obj = eval(feature)
        #print '\t'.join([str(publisher), str(obj['groupid_factor']), str(gmp), str(doc_age), str(score), str(line_n)])
        print '\t'.join([item_id, str(obj['explore']), str(gmp), str(score), str(line_n)])

    fs.close()
