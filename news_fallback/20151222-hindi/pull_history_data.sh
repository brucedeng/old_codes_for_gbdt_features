#!/bin/sh

bin_file=`readlink -f $0`
bin_dir=`dirname $bin_file`

py_script=$bin_dir/write_fallback_content.py

export AWS_CONFIG_FILE="/home/mengchong/.aws/config_content_india"
log_dir=log/
data_dir=data/
log_filename=pipeline.log
interval="10"
check_interval_second=30

content_path_root="s3://com.cmcm.instanews.usw2.prod/contents"
#content_path_root="s3://com.cmcm.instanews.usw2.prod/mengchong"

tmp_dir=`mktemp -d -t content_feeder.XXXXXX`

function log() {
    echo "`date +'%Y%m%d %H:%M'` => $*" >> $log_dir/$log_filename
}

file_check_times=$((interval*2))
function get_data() {
    remote=`echo "$1" | sed 's/\/$//g'`
    rm -f $2
    for((i=0;i<$file_check_times;i++))
    do
        cnt=`aws s3 ls $remote/_SUCCESS | wc -l`
        if [[ $cnt -ne 0 ]]
        then
            echo "data exists $1"
            for f in `aws s3 ls $remote/ | awk '{print $4}'`
            do
                # ignore hidden files such as .pig_schema
                echo $f | grep '^\.'
                if [[ $? -eq 0 ]];
                then
                    continue
                fi
                echo "copying $remote/$f"
                aws s3 cp $remote/$f $tmp_dir/$f
                cat $tmp_dir/$f >> $2
                rm -f $tmp_dir/$f
            done
            return
        fi
        log "data $1 is not ready, waiting for 30 seconds"
        sleep 30
    done
    return 2
}

function get_file() {
    for((i=0;i<$file_check_times;i++))
    do
        # hadoop fs -test -e $1
        cnt=`aws s3 ls $1 | wc -l`
        if [[ $cnt -ne 0 ]]
        then
            echo "data exists $1"
            #hadoop fs -cat $1 > $2
            aws s3 cp $1 $2
            return
        fi
        log "data $1 is not ready, waiting for 30 seconds"
        sleep 30
    done
    return 2
}

mkdir -p $log_dir
mkdir -p $data_dir

prev_key=xxxxx
prev_day=`date +%Y%m%d`
while :
do
    minute=`date +%M | sed 's/^0*//g'`
    hour=`date +%H`
    day=`date +%Y%m%d`
    path_minute=`echo "$minute / $interval * $interval" | bc | sed ':a;s/\(..\)/\1/;tb;s/^/0/;ta;:b'`
    log $minute $path_minute
    content_path="$content_path_root/$day/$hour/$path_minute"
    key="$day/$hour/$path_minute"
    if [[ "$key" == "$prev_key" ]]; then
        log "minute has not updated yet. waiting..."
        sleep $check_interval_second
        continue
    fi

    mkdir -p $data_dir/$day/$hour/
    get_data $content_path $data_dir/$day/$hour/$path_minute.json
    success=$?
    if [[ $success -eq 0 ]]; then
        cat $data_dir/*/*/*.json > $tmp_dir/overall_data
        python $py_script $tmp_dir/overall_data online_dump/
    else
        # send alert
        # sendmail
        # sendtxt
        log "error parsing data"
    fi
    prev_key=$key
    if [[ $prev_day != $day ]];then
        if [[ -e $log_dir/$log_filename ]]; then
            log "rotating log file"
            mv $log_dir/$log_filename $log_dir/$log_filename.$prev_day
        fi
        log "remove data older than 7 days"
        find $data_dir -mtime 7 -exec delete {} \;
    fi

    sleep $check_interval_second

done
