import sys,os,math
reload(sys)
sys.setdefaultencoding('utf-8')
import traceback
import copy
import redis
import time,json
from datetime import datetime

global config
config={"stat_path":"/home/chenkehan/news_data"}
def list2dict(output_list):
    output_dict = {}
    for i in range(len(output_list)):
        id = output_list[i][0]
        score = 1.0 - 0.0001*i
        output_dict[id] = score

    return output_dict


def write_local_file(path, out_file,expire_file,output,expire):

    if not os.path.exists(path):
        os.mkdir(path)

    out_file = open(path + '/' + out_file,'w')
    expire_file = open(path + '/' + expire_file,'w')

    valid_list = output
    expire_list = expire

    for item in valid_list:
        s = '\t'.join(str(t) for t in item)
        out_file.write(s+'\n')

    for item in expire_list:
        s = '\t'.join(str(t) for t in item)
        expire_file.write(s+'\n')

    out_file.close()
    expire_file.close()

def timestamp_to_str(ts):
    dt = datetime.fromtimestamp(ts)
    return dt.strftime('%Y%m%d_%H%M')

def write_redis(content_list,channel_list,timestamp,config_json):
    try:
    #prepare redis client
        redis_host = config_json['redis_host']
        redis_port = config_json['redis_port']
        redis_password = config_json['redis_password']
        redis_meta = redis.Redis(host=redis_host,port=redis_port, password=redis_password, db=0)
        db_mark = redis_meta.get(config_json['redis_db_mark'])

        db_writer = 2 if db_mark == '1' else 1
        if db_writer is None:
            db_writer = 1

        redis_writer = redis.Redis(host=redis_host,port=redis_port, password=redis_password, db=db_writer)
        #dummpy_list = ['0',-1]
        dummpy_list = []
        redis_pipe = redis_writer.pipeline()

        redis_content_key_prefix_1d = config_json['redis_db_content_key_1d']
        redis_content_value_1d_threshold = 1000
        # process channel content list
        for channel in channel_list:
            channel_out = get_channel_list(content_list,channel,timestamp)


            redis_content_channel_key_1d = redis_content_key_prefix_1d + channel
            redis_pipe.delete(redis_content_channel_key_1d)

            #redis_content_channel_value_1d = channel_out[0][:redis_content_value_1d_threshold]
            redis_content_channel_value_1d = diversify(channel_out[0])[:redis_content_value_1d_threshold]

            channel_out_1d = list2dict(redis_content_channel_value_1d)

            #print channel_out_1d
            if len(channel_out_1d) >= 20:
                redis_pipe.zadd(redis_content_channel_key_1d, *dummpy_list,**channel_out_1d)
            else:
                print "channel list too short for " + channel

            local_output_path = config['stat_path'] + '/' + timestamp_to_str(timestamp)
            tmp_file_name = str(channel) + '.' + 'valid'
            expired_file_name = str(channel) + '.' + 'expired'
            write_local_file(local_output_path, tmp_file_name,expired_file_name,redis_content_channel_value_1d,channel_out[1])

            print "write fallback content list into local path :%s" % local_output_path


        redis_pipe.execute()
        redis_meta.set('db_mark', db_writer)
        redis_meta.set(config_json['redis_db_last_modify_time'], timestamp)

        #print redis_writer.zrangebyscore(redis_content_channel_key_1d,0.99,1)
        print "%s fallback content id list has been generated and written to redis"% str(timestamp)


    except:
        traceback.print_exc()
        pass

def diversify(sorted_content,wnd_size=3,publisher_index=1,group_id_index=7,category_index=4):

    result = []
    while len(sorted_content) > 0:
        #print result[:10]

        if len(result) < wnd_size:
            context = result
        elif wnd_size >0:
            context = result[-wnd_size:]
        else:
            context = []

        wnd_set = set()
        for item in context:
            wnd_set.add('p-' + str(item[publisher_index]))
            #wnd_set.add('g-' + str(item[group_id_index]))
            wnd_set.update(set(item[category_index]))

        for i in range(len(sorted_content)):
            item = sorted_content[i]
            if ('p-' + item[publisher_index]) in wnd_set or ('g-' + item[group_id_index]) in wnd_set or len(set(item[category_index]) & wnd_set) > 0:
                continue
            else:
                item = sorted_content.pop(i)
                result.append(item)
                #print wnd_set, len(result), item
                #sys.stdin.readline()
                break

        if i == len(sorted_content)-1:
            #print i, wnd_set, item
            #wnd_size -= 1
            return result + sorted_content


    return result

def load_dedup_content(path, mode):
    if mode == 'local':
        f = open(path)

        result = []
        item_id_map = {}
        idx = 0

        for line in f:
            obj = json.loads(line)
            item_id = obj['item_id']
            if item_id not in item_id_map:
                item_id_map[item_id] = idx
                result.append(obj)
                idx += 1
            else:
                tmp_idx = item_id_map[item_id]
                tmp_item = result[tmp_idx]
                tmp_publish_time = tmp_item.get('update_time')
                publish_time = obj.get('update_time')
                if publish_time >= tmp_publish_time:
                    result[tmp_idx] = obj

        return result


def parse_content_dump(input_content):
    # if mode == 'local':
    #     f = open(path)

    check_sum_dict = {}
    tmp_list = []
    content_list = []

    for obj in input_content:

        #not servable
        src_type = str(obj['source_type'])
        if src_type == '0' and str(obj['is_servable'])  ==  'false':
            continue
        #region&lang incorrect
        if str(obj['language']) != 'en' or str(obj['region']) != 'in':
            continue
        if int(obj['image_count']) == 0 and int(obj['word_count']) < 400:
            continue

        item_id = str(obj['item_id'])

        type = str(obj['type'])
        publish_time = str(obj['publish_time'])

        #if len(obj['editorial_categories']) >= 1:
            #category =  obj['editorial_categories']
        if len(obj['categories']) >=1:
            category = [t['id'] for t in obj['categories']]
        else:
            category = []

        title = str(obj['title'])
        check_sum = str(obj['md5'])
        group_id = str(obj['group_id'])

        if len(obj['channel_names']) >= 1:
            channels = obj['channel_names']
        else:
            channels = []

        images_cnt = obj['image_count']

        word_count = obj['word_count']
        publisher = str(obj['publisher'])
        keywords_count = len(obj['entities'])
        #has_large_image = obj['has_large_image']

        features = (images_cnt, keywords_count, word_count, type)

        if check_sum in check_sum_dict:
            check_sum_dict[check_sum] += 1
        else:
            check_sum_dict[check_sum] = 1
            tmp_list.append([item_id,publisher,publish_time,channels,category,title,check_sum,group_id,features]);

    for t in tmp_list:
        pop_score =  check_sum_dict.get(t[6],1)
        t.append(pop_score * 1000)

        content_list.append(t)


    # f.close()

    #print '\n'.join(str(c) for c in content_list)

    return content_list

def get_channel_list(content_list,channel_id,timestamp,channel_index=3,ts_index=2,feature_index=8,pop_index=9):

    id2channel = {'29':'hot','3':'entertainment','4':'sports','30':'business','31':'lifestyle'}
    channel = id2channel.get(channel_id,None)

    channel_list = []
    expired_list = []

    if channel == 'hot':
        for c in content_list:
            if int(timestamp) - int(c[ts_index]) <= 86400*7:
                channel_list.append(copy.copy(c))
            else:
                expired_list.append(copy.copy(c))
    else:
        for c in content_list:
            if channel in c[channel_index]:
                if int(timestamp) - int(c[ts_index]) <= 86400*7:
                    channel_list.append(copy.copy(c))
                else:
                    expired_list.append(copy.copy(c))

    #print '\n'.join(str(c) for c in channel_list[:3])

    for item in channel_list:
        age_hour = float(int(timestamp) - int(item[ts_index]))/3600.0

        score = item[pop_index] * math.exp(-0.2*age_hour)

        if score < 1:
            pop_score = '1'
        elif int(score) >= 100000:
            pop_score = '99999'
        else:
            pop_score = "%05d" % int(score)
            #pop_score = "%02d" % int(item[pop_index])


        if int(item[feature_index][0]) >= 100:
            image_cnt = "99"
        else:
            image_cnt = "%02d" % int(item[feature_index][0])

        if int(item[feature_index][1]) >= 100:
            keywords_cnt = "99"
        else:
            keywords_cnt = "%02d" % int(item[feature_index][1])


        if int(item[feature_index][2]) >= 10000:
            word_cnt = "9999"
        else:
            word_cnt = "%04d" % int(item[feature_index][2])

        rank_score = int(pop_score+image_cnt+keywords_cnt+word_cnt)
        item.append(age_hour)
        item.append(rank_score)


    #output_list = sorted(channel_list, key = lambda x:x[pop_index], reverse=True)
    output_list = sorted(channel_list, key = lambda x:x[-1], reverse=True)


    return (output_list,expired_list)

if __name__ == '__main__':

    config_json = {}
    config_json['redis_host'] = 'featureserver.mkjqd2.0001.apse1.cache.amazonaws.com'
    config_json['redis_port'] = '6379'
    config_json['redis_password'] = ''
    config_json['redis_db_mark'] = 'db_mark'
    config_json['redis_db_content_key_1d'] = 'news_class_1_'
    config_json['redis_db_last_modify_time'] = 'last_modify_time'

    channel_list = ['29','3','4','30','31']
    # channel_list = ['29']
    config['stat_path'] = sys.argv[2]

    path = sys.argv[1]
    dedup_content = load_dedup_content(path,'local')
    print "deduped content: " + str(len(dedup_content))

    content_list = parse_content_dump(dedup_content);
    print "valid content: "+ str(len(content_list))

    timestamp = int(time.time())
    write_redis(content_list,channel_list,timestamp,config_json)

