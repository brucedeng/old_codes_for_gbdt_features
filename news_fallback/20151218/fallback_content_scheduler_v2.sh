#!/bin/sh

bin_file=`readlink -f $0`
bin_dir=`dirname $bin_file`

py_script=$bin_dir/write_fallback_content_v5.py

export PS4='+${BASH_SOURCE}:${LINENO}:${FUNCNAME[0]}: '
# set -e

export AWS_CONFIG_FILE="/home/mengchong/.aws/config_content_india"
log_dir=../log
data_dir=../data
gmp_dir=../gmp
hist_data_dir=../hist_data
online_dump_dir=../online_dump
log_filename=pipeline.log
interval_minutes="10"
check_interval_second=30
load_data_days=4

#content_path_root="s3://com.cmcm.instanews.usw2.prod/contents"
content_path_root="s3://com.cmcm.instanews.usw2.prod/cpp/feeder"
gmp_path_root="s3://com.cmcm.instanews.usw2.prod/nrt/gmp"

tmp_dir=/tmp
#`mktemp -d -t content_feeder.XXXXXX`

function log() {
    echo "`date +'%Y%m%d %H:%M:%S'` => $*" >> $log_dir/$log_filename
}

function add_log_ts {
    while read line; do
        echo "$line" | awk '{print "'"`date +'%Y%m%d %H:%M:%S'` => $*"'",$0}'
    done
}

function get_data_no_wait() {
    remote=`echo "$1" | sed 's/\/$//g'`
    local_dir=$2
    if [[ -e $local_dir/_SUCCESS ]]; then
        return 1
    fi

    cnt=`aws s3 ls $remote/_SUCCESS | wc -l`
    if [[ $cnt -ne 0 ]]
    then
        echo "data exists $1"
        mkdir -p $local_dir
        aws s3 cp $remote/ $local_dir/ --recursive
        return 0
    fi
    return 2
}

function get_file() {
    remote=`echo "$1" | sed 's/\/$//g'`
    local_file=$2
    if [[ -e $local_file ]]; then
        return 1
    fi

    cnt=`aws s3 ls $remote/_SUCCESS | wc -l`
    tmp_dir=`mktemp -d -t gmp_tmp_file.XXXXXX`
    # echo $tmp_dir
    if [[ $cnt -ne 0 ]]
    then
        echo "data exists $1"
        aws s3 cp $remote/ $tmp_dir/gmp --recursive
        cat $tmp_dir/gmp/* > $local_file
        rm -rf ${tmp_dir?}
        return 0
    fi
    return 2
}

mkdir -p $log_dir
mkdir -p $data_dir
mkdir -p $hist_data_dir

load_data_seconds=$((load_data_days*24*60*60))
interval_seconds=$((interval_minutes*60))
time_shift='120'
prev_key=xxxxx
prev_day=`date -d "$time_shift minutes ago" +%Y%m%d`

while :
do
    
    cur_ts=`date +%s`
    key=`echo "$cur_ts / $interval_seconds * $interval_seconds" | bc`
    if [[ $key == $prev_key ]]; then
        sleep $check_interval_second
        continue
    fi
    prev_key=$key

    prev_ts=`date -d "$time_shift minutes ago" +%s`
    updated_gmp=0
    ts=$cur_ts
    cur_gmp_data=""
    while [[ $ts -ge $prev_ts ]]; do
        minute=`date -d@$ts +%M`
        hour=`date -d@$ts +%H`
        day=`date -d@$ts +%Y%m%d`
        path_minute=`echo "$minute / $interval_minutes * $interval_minutes" | bc | sed ':a;s/\(..\)/\1/;tb;s/^/0/;ta;:b'`
        gmp_path="$gmp_path_root/$day/$hour$path_minute"
        mkdir -p $gmp_dir/$day/$hour/
        cur_gmp_data="$gmp_dir/$day/$hour/$path_minute.data"
        get_file $gmp_path $cur_gmp_data

        success=$?
        if [[ $success -eq 0 || $success -eq 1 ]]; then
            log "got gmp $data_dir/$day/$hour/$path_minute"
            updated_gmp=1
            break
        else
            log "error fetching data"
        fi
        # set -e
        ts=$((ts-interval_seconds))
    done

    if [[ $updated_gmp -eq 0 ]]; then
        sleep $check_interval_second
        continue
    fi

    ts=`date -d "$time_shift minutes ago" +%s`
    updated=0
    while [[ $ts -le $cur_ts ]]; do
        minute=`date -d@$ts +%M`
        hour=`date -d@$ts +%H`
        day=`date -d@$ts +%Y%m%d`
        path_minute=`echo "$minute / $interval_minutes * $interval_minutes" | bc | sed ':a;s/\(..\)/\1/;tb;s/^/0/;ta;:b'`
        log "pulling data for $day $hour $path_minute"
        content_path="$content_path_root/$day/$hour/$path_minute"
        
        mkdir -p $data_dir/$day/$hour/
        # set +e
        get_data_no_wait $content_path $data_dir/$day/$hour/$path_minute

        success=$?
        if [[ $success -eq 0 ]]; then
            log "fetched files $data_dir/$day/$hour/$path_minute"
            updated=$((updated+1))
        elif [[ $success -eq 1 ]]; then
            log "$data_dir/$day/$hour/$path_minute already exists. ignored..."
        else
            log "error fetching data"
        fi
        # set -e
        ts=$((ts+interval_seconds))
    done

    # if [[ $updated -eq 0 ]]; then
    #     sleep $check_interval_second
    #     continue
    # fi

    log "merging data to $data_dir/overall_data"
    hist_data_file=$hist_data_dir/"`date +%Y%m%d-%H%M`"
    rm -f $hist_data_file
    start_ts=$((cur_ts-load_data_seconds))
    for ((ts=start_ts; ts<=cur_ts; ts+=interval_seconds)); do
        minute=`date -d@$ts +%M`
        hour=`date -d@$ts +%H`
        day=`date -d@$ts +%Y%m%d`
        path_minute=`echo "$minute / $interval_minutes * $interval_minutes" | bc | sed ':a;s/\(..\)/\1/;tb;s/^/0/;ta;:b'`
        local_path="$data_dir/$day/$hour/$path_minute"
        if [[ -e $local_path/_SUCCESS ]]; then
            log "merging $local_path"
            cat $local_path/*.data >> $hist_data_file
        fi
    done

    log "writing redies"
    python $py_script $hist_data_file $cur_gmp_data $online_dump_dir prod >> $log_dir/$log_filename
    log "removing merged file $hist_data_file"
    rm -f $hist_data_file
    
    if [[ $prev_day != $day ]];then
        prev_day=$day
        if [[ -e $log_dir/$log_filename ]]; then
            log "rotating log file"
            mv $log_dir/$log_filename $log_dir/$log_filename.$prev_day
        fi
        log "remove data older than 7 days"
        find $data_dir -mtime 9 -exec rm -f {} \;
        #log "remove merged data older than 2 days"
        #find $data_dir -mtime 2 -exec rm -f {} \;
    fi

    sleep $check_interval_second

done
