import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import traceback
import redis
import time
import local_utils
import data_parser
import ranking_utils
global config
config={"stat_path":"/home/chenkehan/news_data"}

global loginfo
loginfo=[]

def write_redis_test(content_list,schema,channels,timestamp,config_json,redis_host,redis_port,redis_password,write_flag=False):
    try:
        redis_content_value_1d_threshold = 1000
        # process channel content list
        for channel in sorted(channels.items(),key=lambda t:int(t[0])):
            channel_id = channel[0]

            channel_out = ranking_utils.get_channel_by_conf(content_list,schema,channel,timestamp,loginfo,write_flag=write_flag)
            #channel_out = ranking_utils.get_channel_list(content_list,schema,channel,timestamp,loginfo,write_flag=write_flag)
            schema = channel_out[2]

            redis_content_channel_value_1d, schema = ranking_utils.diversify(channel_out[0],schema)
            redis_content_channel_value_1d = redis_content_channel_value_1d[:redis_content_value_1d_threshold]

            channel_out_1d = local_utils.list2dict(redis_content_channel_value_1d)
            #print channel_out_1d
            if len(channel_out_1d) >= 20:
                pass
            else:
                print "channel list too short for " + channel_id

            if write_flag:
                local_utils.write_log(timestamp,channel_id,config,redis_content_channel_value_1d,channel_out[1][:redis_content_value_1d_threshold],schema,loginfo)
    except:
        traceback.print_exc()
        pass

def write_redis(content_list,schema,channels,timestamp,config_json,redis_host,redis_port,redis_password,write_flag=False):
    try:
    #prepare redis client
        redis_meta = redis.Redis(host=redis_host,port=redis_port, password=redis_password, db=0)
        db_mark = redis_meta.get(config_json['redis_db_mark'])

        db_writer = 2 if db_mark == '1' else 1
        if db_writer is None:
            db_writer = 1

        redis_writer = redis.Redis(host=redis_host,port=redis_port, password=redis_password, db=db_writer)
        dummpy_list = []
        redis_pipe = redis_writer.pipeline()

        redis_content_key_prefix_1d = config_json['redis_db_content_key_1d']
        redis_content_value_1d_threshold = 1000
        # process channel content list
        for channel in sorted(channels.items(),key=lambda t:int(t[0])):
            channel_id = channel[0]
            channel_out = ranking_utils.get_channel_by_conf(content_list,schema,channel,timestamp,loginfo,write_flag=write_flag)

            #channel_out = ranking_utils.get_channel_list(content_list,schema,channel,timestamp,loginfo,write_flag=write_flag)
            schema = channel_out[2]

            redis_content_channel_key_1d = redis_content_key_prefix_1d + channel_id
            redis_pipe.delete(redis_content_channel_key_1d)

            #redis_content_channel_value_1d = channel_out[0][:redis_content_value_1d_threshold]
            redis_content_channel_value_1d, schema = ranking_utils.diversify(channel_out[0],schema)
            redis_content_channel_value_1d = redis_content_channel_value_1d[:redis_content_value_1d_threshold]

            channel_out_1d = local_utils.list2dict(redis_content_channel_value_1d)

            #print channel_out_1d
            if len(channel_out_1d) >= 20:
                redis_pipe.zadd(redis_content_channel_key_1d, *dummpy_list,**channel_out_1d)
            else:
                print "channel list too short for " + channel_id

            if write_flag:
                local_utils.write_log(timestamp,channel_id,config,redis_content_channel_value_1d,channel_out[1][:redis_content_value_1d_threshold],schema,loginfo)

        redis_pipe.execute()
        redis_meta.set('db_mark', db_writer)
        redis_meta.set(config_json['redis_db_last_modify_time'], timestamp)

        #print redis_writer.zrangebyscore(redis_content_channel_key_1d,0.999,1)
        print "[Time:%s] Fallback content id list has been generated and written to redis: %s" % (str(timestamp), redis_host)


    except:
        traceback.print_exc()
        pass

if __name__ == '__main__':

    config_json = {}
    config_json['redis_host_sig1'] = 'featureserver.mkjqd2.0001.apse1.cache.amazonaws.com'
    config_json['redis_host_sig2'] = 'featureserverprd.mkjqd2.0001.apse1.cache.amazonaws.com'
    config_json['redis_host_uswest'] = 'featureserver.bgkplr.0001.usw2.cache.amazonaws.com'
    config_json['redis_port'] = '6379'
    config_json['redis_password'] = ''
    config_json['redis_db_mark'] = 'db_mark'
    config_json['redis_db_content_key_1d'] = 'news_class_1_'
    config_json['redis_db_last_modify_time'] = 'last_modify_time'

    #channel_list = ['29','3','4','30','31','1','99']
    # channel_list = ['29']

    channel_config = {}
    channel_config['29'] = []
    channel_config['3'] = ['entertainment<-channel_name']
    channel_config['4'] = ['sports<-channel_name']
    channel_config['30'] = ['business<-channel_name']
    channel_config['31'] = ['lifestyle<-channel_name']
    channel_config['1'] = ['politics<-channel_name']
    channel_config['32'] = ['1000849<-category']

    config['stat_path'] = sys.argv[2]

    path = sys.argv[1]
    dedup_content = data_parser.load_dedup_content(path,loginfo)
    print "deduped content: " + str(len(dedup_content))

    content_list,schema = data_parser.parse_content_dump(dedup_content,loginfo);
    print "valid content: "+ str(len(content_list))
    #print content_list

    timestamp = int(time.time())
    write_redis(content_list,schema,channel_config,timestamp,config_json,config_json['redis_host_sig1'],config_json['redis_port'],config_json['redis_password'],True)
    write_redis(content_list,schema,channel_config,timestamp,config_json,config_json['redis_host_sig2'],config_json['redis_port'],config_json['redis_password'],False)
    write_redis(content_list,schema,channel_config,timestamp,config_json,config_json['redis_host_uswest'],config_json['redis_port'],config_json['redis_password'],False)

    loginfo.append('[MAIN] Last update time: %s' % local_utils.timestamp_to_str(timestamp))

    local_output_path = config['stat_path'] + '/' + local_utils.timestamp_to_str(timestamp)
    latest_output_path = config['stat_path'] + '/' + '0_latest'
    tmp_file_name = 'result_info.txt'
    local_utils.write_log_file(local_output_path, tmp_file_name, loginfo)
    local_utils.write_log_file(latest_output_path, tmp_file_name, loginfo)

    print schema
