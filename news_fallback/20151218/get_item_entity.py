import sys
import json

for line in sys.stdin:
    obj = json.loads(line)
    print ','.join("(%s,%f)" % (item['name'],item['L1_weight']) for item in obj['entities'])

