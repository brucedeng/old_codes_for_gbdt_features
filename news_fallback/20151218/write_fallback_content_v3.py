import sys,os,math
reload(sys)
sys.setdefaultencoding('utf-8')
import traceback
import copy
import redis
import time
import ujson
from datetime import datetime

global config
config={"stat_path":"/home/chenkehan/news_data"}

global loginfo
loginfo=[]

def list2dict(output_list):
    output_dict = {}
    for i in range(len(output_list)):
        id = output_list[i][0]
        score = 1.0 - 0.0001*i
        output_dict[id] = score

    return output_dict

def write_log_file(path, out_file, output):
    if not os.path.exists(path):
        os.mkdir(path)

    out_file = open(path + '/' + out_file,'w')
    for line in output:
        out_file.write(line+'\n')

    out_file.close()

def write_local_file(path, out_file,expire_file,output,expire):

    if not os.path.exists(path):
        os.mkdir(path)

    out_file = open(path + '/' + out_file,'w')
    expire_file = open(path + '/' + expire_file,'w')

    valid_list = output
    expire_list = expire

    for item in valid_list:
        s = '\t'.join(str(t) for t in item)
        out_file.write(s+'\n')

    for item in expire_list:
        s = '\t'.join(str(t) for t in item)
        expire_file.write(s+'\n')

    out_file.close()
    expire_file.close()

def timestamp_to_str(ts):
    dt = datetime.fromtimestamp(ts)
    return dt.strftime('%Y%m%d_%H%M')

def write_redis(content_list,channel_list,timestamp,config_json,redis_host,redis_port,redis_password,write_flag=False):
    try:
    #prepare redis client
        redis_meta = redis.Redis(host=redis_host,port=redis_port, password=redis_password, db=0)
        db_mark = redis_meta.get(config_json['redis_db_mark'])

        db_writer = 2 if db_mark == '1' else 1
        if db_writer is None:
            db_writer = 1

        redis_writer = redis.Redis(host=redis_host,port=redis_port, password=redis_password, db=db_writer)
        #dummpy_list = ['0',-1]
        dummpy_list = []
        redis_pipe = redis_writer.pipeline()

        redis_content_key_prefix_1d = config_json['redis_db_content_key_1d']
        redis_content_value_1d_threshold = 1000
        # process channel content list
        for channel in channel_list:
            channel_out = get_channel_list(content_list,channel,timestamp,write_flag=write_flag)


            redis_content_channel_key_1d = redis_content_key_prefix_1d + channel
            redis_pipe.delete(redis_content_channel_key_1d)

            #redis_content_channel_value_1d = channel_out[0][:redis_content_value_1d_threshold]
            redis_content_channel_value_1d = diversify(channel_out[0])[:redis_content_value_1d_threshold]

            channel_out_1d = list2dict(redis_content_channel_value_1d)

            #print channel_out_1d
            if len(channel_out_1d) >= 20:
                redis_pipe.zadd(redis_content_channel_key_1d, *dummpy_list,**channel_out_1d)
            else:
                print "channel list too short for " + channel

            if write_flag:

                local_output_path = config['stat_path'] + '/' + timestamp_to_str(timestamp)
                latest_output_path = config['stat_path'] + '/' + '0_latest'
                tmp_file_name = str(channel) + '.' + 'valid.txt'
                expired_file_name = str(channel) + '.' + 'expired.txt'
                write_local_file(local_output_path, tmp_file_name,expired_file_name,redis_content_channel_value_1d,channel_out[1])
                write_local_file(latest_output_path, tmp_file_name,expired_file_name,redis_content_channel_value_1d,channel_out[1])

                print "write fallback content list into local path :%s" % local_output_path

                def doc_age_bin(hour):
                    if hour <= 1:
                        return "1#<1h"
                    elif hour <= 4:
                        return "2#1-4h"
                    elif hour <= 24:
                        return "3#4-24h"
                    elif hour <= 48:
                        return "4#24-48h"
                    else:
                        return "5#>2d"

                from collections import Counter
                doc_age_index = 10
                doc_age = [doc_age_bin(item[doc_age_index]) for item in redis_content_channel_value_1d]
                freqs = Counter(doc_age)
                avg_doc_age = sum(item[doc_age_index] for item in redis_content_channel_value_1d)/float(len(redis_content_channel_value_1d))
                doc_age_dist = sorted([(k,freqs[k]) for k in freqs], key = lambda x:x[0])

                loginfo.append('Average doc age for channel %s: %f' % (channel,avg_doc_age));
                loginfo.append('Doc age distribution for channel %s: %s\n' % (channel,str(doc_age_dist)));


        redis_pipe.execute()
        redis_meta.set('db_mark', db_writer)
        redis_meta.set(config_json['redis_db_last_modify_time'], timestamp)

        #print redis_writer.zrangebyscore(redis_content_channel_key_1d,0.999,1)
        print "%s fallback content id list has been generated and written to redis: %s" % (str(timestamp), redis_host)


    except:
        traceback.print_exc()
        pass

def diversify(sorted_content,wnd_size=4,candidate_wnd=20,publisher_index=1,group_id_index=7,category_index=4,score_index=11):

    result = []
    while len(sorted_content) > 0:
        #print result[:10]

        if len(result) < wnd_size:
            context = result
        elif wnd_size >0:
            context = result[-wnd_size:]
        else:
            context = []

        wnd_set = set()
        for item in context:
            wnd_set.add('p-' + str(item[publisher_index]))
            wnd_set.update(set(item[category_index]))
            if int(item[group_id_index]) > 0:
                wnd_set.add('g-' + str(item[group_id_index]))

        max_score = 0
        max_item_index = -1
        max_penalty = 1

        for i in range(min(candidate_wnd, len(sorted_content))):
            item = sorted_content[i]
            diversity_factor = 1.0
            if ('p-' + item[publisher_index]) in wnd_set:
                diversity_factor *= 0.5
            if ('g-' + item[group_id_index]) in wnd_set:
                diversity_factor *= 0.1
            if len(set(item[category_index]) & wnd_set) > 0:
                diversity_factor *= 0.8

            score = float(item[score_index]) * float(diversity_factor)
            #print wnd_set, diversity_factor, item

            if score > max_score:
                max_score = score
                max_penalty = diversity_factor
                max_item_index = i

        if max_item_index >= 0:
            item = sorted_content.pop(max_item_index)
            item.append((max_score,max_penalty))
            #print item
            #sys.stdin.readline()
            result.append(item)

    return result

def load_dedup_content(path, mode):
    doc_num = 0
    if mode == 'local':
        f = open(path)

        result = []
        item_id_map = {}
        idx = 0

        for line in f:
            try:
                obj = ujson.loads(line)
                publisher=obj.get('publisher','')
                if publisher=='Glamsham':
                    continue
                item_id = obj['item_id']
                if item_id not in item_id_map:
                    item_id_map[item_id] = idx
                    result.append(obj)
                    idx += 1
                else:
                    tmp_idx = item_id_map[item_id]
                    tmp_item = result[tmp_idx]
                    tmp_publish_time = tmp_item.get('update_time')
                    publish_time = obj.get('update_time')
                    if publish_time >= tmp_publish_time:
                        result[tmp_idx] = obj
            except:
                continue
            doc_num += 1

        loginfo.append('Total doc num: %s' % str(doc_num))
        loginfo.append('Total distinct doc num: %d \n' % len(result))
        return result
    else:
        return []


def parse_content_dump(input_content):
    # if mode == 'local':
    #     f = open(path)

    check_sum_dict = {}
    groupid_dict = {}
    tmp_list = []
    content_list = []

    non_servable = 0
    language_region_invalid = 0
    short_content = 0

    for obj in input_content:

        #not servable
        try:
            #src_type = str(obj.get('source_type'))
            if obj['is_servable'] == False:
                non_servable += 1
                continue
            #region&lang incorrect
            if str(obj['language']) != 'en' or str(obj['region']) != 'in':
                language_region_invalid += 1
                continue
            if int(obj['image_count']) == 0 and int(obj['word_count']) < 400:
                short_content += 1
                continue

            item_id = str(obj['item_id'])

            type = str(obj['type'])
            publish_time = str(obj['publish_time'])

            #if len(obj['editorial_categories']) >= 1:
                #category =  obj['editorial_categories']
            if len(obj['categories']) >=1:
                category = [t['id'] for t in obj['categories'] if 'id' in t]
                category += [t['name'] for t in obj['categories'] if 'name' in t]
            else:
                category = []

            title = str(obj['title'])
            check_sum = str(obj['md5'])
            group_id = str(obj['group_id'])

            if len(obj['channel_names']) >= 1:
                channels = obj['channel_names']
            else:
                channels = []

            images_cnt = obj['image_count']

            word_count = obj['word_count']
            publisher = str(obj['publisher'])
            keywords_count = len(obj['entities'])
            #has_large_image = obj['has_large_image']

            features = (images_cnt, keywords_count, word_count, type, str(obj['is_servable']))

            if check_sum in check_sum_dict:
                check_sum_dict[check_sum] += 1
            else:
                check_sum_dict[check_sum] = 1
                if int(group_id) > 0 and group_id in groupid_dict:
                    groupid_dict[group_id] += 1
                else:
                    groupid_dict[group_id] = 1
                    tmp_list.append([item_id,publisher,publish_time,channels,category,title,check_sum,group_id,features]);

        except:
            traceback.print_exc()

    for t in tmp_list:
        #pop_score =  check_sum_dict.get(t[6],1)
        pop_score =  groupid_dict.get(t[7],1)
        t.append(pop_score * 1000)

        content_list.append(t)


    # f.close()

    #print '\n'.join(str(c) for c in content_list)

    loginfo.append("Filter info: %d non servable docs, %d invalid_region&language, %d short content." %(non_servable,language_region_invalid,short_content))
    loginfo.append('Valid doc num deduped by checksum: %d\n' % len(content_list))

    return content_list

def get_channel_list(content_list,channel_id,timestamp,category_index=4,channel_index=3,ts_index=2,publisher_index=1,feature_index=8,pop_index=9,write_flag=False):

    id2channel = {'29':'hot','3':'entertainment','4':'sports','30':'business','31':'lifestyle','1':'politics','99':'cricket'}
    channel = id2channel.get(channel_id,None)
    pro_publisher = ['ndtv','toi','the economic times','india times','indiatimes','the hindu','india.com','cricbuzz','hindustantimes','the indian express','one india','sify','firstpost.com','engadget.com','digit.in','indiatoday']

    channel_list = []
    expired_list = []

    if channel == 'hot':
        for c in content_list:
            if int(timestamp) - int(c[ts_index]) <= 86400*7:
                channel_list.append(copy.copy(c))
            else:
                expired_list.append(copy.copy(c))
    elif channel == 'cricket':
        for c in content_list:
            if '1000849' in c[category_index]:
                if int(timestamp) - int(c[ts_index]) <= 86400*7:
                    channel_list.append(copy.copy(c))
                else:
                    expired_list.append(copy.copy(c))
    else:
        for c in content_list:
            if channel in c[channel_index]:
                if int(timestamp) - int(c[ts_index]) <= 86400*7:
                    channel_list.append(copy.copy(c))
                else:
                    expired_list.append(copy.copy(c))

    #print '\n'.join(str(c) for c in channel_list[:3])

    for item in channel_list:
        age_hour = float(int(timestamp) - int(item[ts_index]))/3600.0

        #!boost head publisher
        publisher = item[publisher_index].lower()
        if publisher in pro_publisher:
            pro_factor = 1.5
        else:
            pro_factor = 1.0

        #!age decay
        score = pro_factor * item[pop_index] * math.exp(-0.2*age_hour)

        #ad-hoc top id
        #if str(item[0]) == '4551993':
            #score *= 100

        if score < 1:
            pop_score = '1'
        elif int(score) >= 100000:
            pop_score = '99999'
        else:
            pop_score = "%05d" % int(score)
            #pop_score = "%02d" % int(item[pop_index])


        if int(item[feature_index][0]) >= 100:
            image_cnt = "99"
        else:
            image_cnt = "%02d" % int(item[feature_index][0])

        if int(item[feature_index][1]) >= 100:
            keywords_cnt = "99"
        else:
            keywords_cnt = "%02d" % int(item[feature_index][1])


        if int(item[feature_index][2]) >= 10000:
            word_cnt = "9999"
        else:
            word_cnt = "%04d" % int(item[feature_index][2])

        rank_score = int(pop_score+image_cnt+keywords_cnt+word_cnt)
        item.append(age_hour)
        item.append(rank_score)


    #output_list = sorted(channel_list, key = lambda x:x[pop_index], reverse=True)
    output_list = sorted(channel_list, key = lambda x:x[-1], reverse=True)


    if write_flag:
        loginfo.append('Channel %s: %d valid docs, %d expired docs.' % (str(channel_id), len(output_list), len(expired_list)))
    return (output_list,expired_list)

if __name__ == '__main__':

    config_json = {}
    config_json['redis_host_sig1'] = 'featureserver.mkjqd2.0001.apse1.cache.amazonaws.com'
    config_json['redis_host_sig2'] = 'featureserverprd.mkjqd2.0001.apse1.cache.amazonaws.com'
    config_json['redis_host_uswest'] = 'featureserver.bgkplr.0001.usw2.cache.amazonaws.com'
    config_json['redis_port'] = '6379'
    config_json['redis_password'] = ''
    config_json['redis_db_mark'] = 'db_mark'
    config_json['redis_db_content_key_1d'] = 'news_class_1_'
    config_json['redis_db_last_modify_time'] = 'last_modify_time'

    channel_list = ['29','3','4','30','31','1','99']
    # channel_list = ['29']
    config['stat_path'] = sys.argv[2]

    path = sys.argv[1]
    dedup_content = load_dedup_content(path,'local')
    print "deduped content: " + str(len(dedup_content))

    content_list = parse_content_dump(dedup_content);
    print "valid content: "+ str(len(content_list))
    #print content_list

    timestamp = int(time.time())
    write_redis(content_list,channel_list,timestamp,config_json,config_json['redis_host_sig1'],config_json['redis_port'],config_json['redis_password'],True)
    write_redis(content_list,channel_list,timestamp,config_json,config_json['redis_host_sig2'],config_json['redis_port'],config_json['redis_password'],False)
    write_redis(content_list,channel_list,timestamp,config_json,config_json['redis_host_uswest'],config_json['redis_port'],config_json['redis_password'],False)

    loginfo.append('Last update time: %s' % timestamp_to_str(timestamp))

    local_output_path = config['stat_path'] + '/' + timestamp_to_str(timestamp)
    latest_output_path = config['stat_path'] + '/' + '0_latest'
    tmp_file_name = 'result_info.txt'
    write_log_file(local_output_path, tmp_file_name, loginfo)
    write_log_file(latest_output_path, tmp_file_name, loginfo)

