#######################
#
# Update: 1. write a json version fallback list
#         2. update fine-tuned gmp and other doc features
#
#
#######################
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import traceback
import redis
import time
import local_utils
import data_parser
import ranking_utils
global config
config={"stat_path":"/home/chenkehan/news_data"}

global loginfo
loginfo=[]


def write_list2redis(list_map,json_map,timestamp,config_json,redis_host,redis_port,redis_password,mode='test'):
    try:
    #prepare redis client
        if mode == 'prod':
            redis_meta = redis.Redis(host=redis_host,port=redis_port, password=redis_password, db=0)
            db_mark = redis_meta.get(config_json['redis_db_mark'])

            db_writer = 2 if db_mark == '1' else 1
            if db_writer is None:
                db_writer = 1
        elif mode == "test":
            db_writer = 4
        else:
            print "Unkonw mode [prod|test], exit.."
            return

        redis_writer = redis.Redis(host=redis_host,port=redis_port, password=redis_password, db=db_writer)
        dummpy_list = []
        redis_pipe = redis_writer.pipeline()

        # process channel content list
        for channel_id in sorted(list_map.keys()):
            channel_key = list_map[channel_id][0]
            channel_list = list_map[channel_id][1]
            channel_out_1d = local_utils.list2dict(channel_list)

            redis_pipe.delete(channel_key)
            redis_pipe.zadd(channel_key, *dummpy_list,**channel_out_1d)

        for channel_id in sorted(json_map.keys()):
            channel_key = json_map[channel_id][0]
            channel_json = json_map[channel_id][1]
            redis_pipe.delete(channel_key)
            redis_pipe.set(channel_key,channel_json)

            #print channel_id, channel_json

        redis_pipe.execute()

        if mode == 'prod':
            redis_meta.set(config_json['redis_db_mark'], db_writer)
            redis_meta.set(config_json['redis_db_last_modify_time'], timestamp)

        #print redis_writer.zrangebyscore(redis_content_channel_key_1d,0.999,1)
        print "[Time:%s,Mode:%s] Fallback content id list has been generated and written to redis: %s @db:%s" % (str(timestamp), mode, redis_host, db_writer)
        loginfo.append("[Redis] Fallback id list redis status:1 db:%s host:%s" % (db_writer,redis_host))

    except:
        loginfo.append("[Redis] Fallback id list redis status:1 db:%s host:%s" % (db_writer,redis_host))
        traceback.print_exc()
        pass


def gen_redis_data(content_list,schema,channels,timestamp,config_json):
    try:

        key_prefix = config_json['redis_db_content_key_1d']
        key_prefix_json = config_json['redis_db_content_key_json']
        #key_prefix_json_v2 = config_json['redis_db_content_key_json_v2']

        redis_content_value_1d_threshold = 1000

        list_map = {}
        json_map = {}

        # process channel content list
        for channel in sorted(channels.items(),key=lambda t:int(t[0])):
            channel_id = channel[0]
            channel_out = ranking_utils.get_channel_by_conf(content_list,schema,channel,timestamp,loginfo,rank_method='linear',explore_budget=-1)

            schema = channel_out[2]

            #diversified list for raw list
            redis_content_diversified, schema = ranking_utils.diversify(channel_out[0],schema)
            redis_content_diversified = redis_content_diversified[:redis_content_value_1d_threshold]

            #un-diversified list for json list
            redis_content_origin = channel_out[0][:redis_content_value_1d_threshold]
            channel_out_1d_json = local_utils.list2json(redis_content_origin,schema)

            if len(redis_content_diversified) and len(redis_content_origin) >= 20:
                #format redis key
                list_map[channel_id] = (key_prefix + str(channel_id), redis_content_diversified)
                json_map[channel_id] = (key_prefix_json + str(channel_id), channel_out_1d_json)
            else:
                print "channel list too short for " + channel_id

            local_utils.write_log(timestamp,channel_id,config,redis_content_diversified,channel_out[1][:redis_content_value_1d_threshold],schema,loginfo,filename_postfix='div', mode='quiet')
            local_utils.write_log(timestamp,channel_id,config,redis_content_origin,channel_out[1][:redis_content_value_1d_threshold],schema,loginfo,filename_postfix='origin')

        return list_map, json_map


    except:
        traceback.print_exc()
        pass

if __name__ == '__main__':

    config_json = {}
    config_json['redis_host_sig_stg'] = 'featureserver.mkjqd2.0001.apse1.cache.amazonaws.com'
    config_json['redis_host_sig_serving'] = 'featureserverprd.mkjqd2.0001.apse1.cache.amazonaws.com'
    config_json['redis_host_sig_api'] = 'featureserver.mkjqd2.ng.0001.apse1.cache.amazonaws.com'
    config_json['redis_host_uswest_serving'] = 'featureserver.bgkplr.0001.usw2.cache.amazonaws.com'
    config_json['redis_host_uswest_api'] = 'featureserver.bgkplr.ng.0001.usw2.cache.amazonaws.com'

    config_json['redis_port'] = '6379'
    config_json['redis_password'] = ''
    config_json['redis_db_mark'] = 'hi_db_mark'
    config_json['redis_db_content_key_1d'] = 'hi_news_class_1_'
    config_json['redis_db_content_key_json'] = 'hi_news_class_json_'
    config_json['redis_db_last_modify_time'] = 'last_modify_time'

    #channel_list = ['29','3','4','30','31','1','99']
    # channel_list = ['29']

    channel_config = {}
    channel_config['29'] = []
    channel_config['1'] = ['politics<-channel_name']
    channel_config['2'] = ['society<-channel_name']
    channel_config['3'] = ['entertainment<-channel_name']
    channel_config['4'] = ['sports<-channel_name']
    channel_config['6'] = ['technology<-channel_name']
    channel_config['7'] = ['finance<-channel_name']
    channel_config['8'] = ['auto<-channel_name']
    channel_config['9'] = ['real estate<-channel_name']
    channel_config['11'] = ['relationship<-channel_name']
    channel_config['12'] = ['health<-channel_name']
    channel_config['13'] = ['education<-channel_name']
    channel_config['14'] = ['travel<-channel_name']
    channel_config['15'] = ['international<-channel_name']
    channel_config['24'] = ['food<-channel_name']
    channel_config['30'] = ['biz&tech|business<-channel_name']
    channel_config['31'] = ['lifestyle<-channel_name']
    channel_config['32'] = ['cricket<-channel_name']
    channel_config['33'] = ['science<-channel_name']
    channel_config['42'] = ['business<-channel_name']
    channel_config['43'] = ['beauty<-channel_name']

    config['stat_path'] = sys.argv[3]

    gmp_path = sys.argv[2]
    gmp_data = data_parser.load_gmp(gmp_path,loginfo,threshold=20.0)
    print "valid gmp: " + str(len(gmp_data))

    path = sys.argv[1]
    dedup_content = data_parser.load_dedup_content(path,loginfo)
    print "deduped content: " + str(len(dedup_content))

    content_list,schema,gmp_info = data_parser.parse_content_dump(dedup_content,loginfo,gmp=gmp_data);
    print "valid content: "+ str(len(content_list))

    if len(sys.argv) >= 5:
        mode = sys.argv[4]
    else:
        mode = 'test'

    timestamp = int(time.time())
    list_map, json_map =  gen_redis_data(content_list,schema,channel_config,timestamp,config_json)

    write_list2redis(list_map, json_map, timestamp, config_json,config_json['redis_host_sig_stg'],config_json['redis_port'],config_json['redis_password'],mode)
    write_list2redis(list_map, json_map, timestamp, config_json,config_json['redis_host_sig_serving'],config_json['redis_port'],config_json['redis_password'],mode)
    write_list2redis(list_map, json_map, timestamp, config_json,config_json['redis_host_sig_api'],config_json['redis_port'],config_json['redis_password'],mode)
    write_list2redis(list_map, json_map, timestamp, config_json,config_json['redis_host_uswest_serving'],config_json['redis_port'],config_json['redis_password'],mode)
    write_list2redis(list_map, json_map, timestamp, config_json,config_json['redis_host_uswest_api'],config_json['redis_port'],config_json['redis_password'],mode)

    loginfo.append('[MAIN] Last update time: %s' % local_utils.timestamp_to_str(timestamp))

    local_output_path = config['stat_path'] + '/' + local_utils.timestamp_to_str(timestamp)
    latest_output_path = config['stat_path'] + '/' + '0_latest'
    tmp_file_name = 'result_info.txt'
    local_utils.write_log_file(local_output_path, tmp_file_name, loginfo)
    local_utils.write_log_file(latest_output_path, tmp_file_name, loginfo)

    try:
        if mode == 'test':
            pass
        else:
            local_utils.write_monitor(loginfo,'hi_in')
    except:
        traceback.print_exc()

    gmp_file_name='gmp_info.txt'
    gmp_info = local_utils.check_servable_gmp(gmp_info,list_map)
    local_utils.write_gmp_file(local_output_path, gmp_file_name, gmp_info)
    local_utils.write_gmp_file(latest_output_path, gmp_file_name, gmp_info)

    print schema
