import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import traceback
import ujson,datetime,time

def load_gmp(path, loginfo, threshold=20.0):
    res = {}
    file = open(path)
    total_ec = 0
    total_c = 0

    for line in file:
        f = line.split('\t')
        item_id = str(f[0])
        original_ec = float(f[2])
        ec = float(f[4])
        c = float(f[5])

        if c >0 and ec >0 and original_ec > threshold and c <= ec:
            coec = c/ec
            res[item_id] = (c,ec,coec)

        total_ec += ec
        total_c += c

    res['total'] = (total_c,total_ec,total_c/total_ec)
    file.close()

    loginfo.append('[PARSER] Valid GMP count: %s over original ec threshold %f, average GMP: %s' % (str(len(res)), threshold, str(res['total'])))
    return res

def load_dedup_content(path,loginfo):
    doc_num = 0
    result = []
    item_id_map = {}
    idx = 0
    f = open(path)
    for line in f:
        try:
            obj = ujson.loads(line)
            publisher=obj.get('publisher','')
            if publisher=='Glamsham':
                continue
            item_id = obj['item_id']
            if item_id not in item_id_map:
                item_id_map[item_id] = idx
                result.append(obj)
                idx += 1
            else:
                tmp_idx = item_id_map[item_id]
                tmp_item = result[tmp_idx]
                tmp_publish_time = tmp_item.get('update_time')
                publish_time = obj.get('update_time')
                if publish_time >= tmp_publish_time:
                    result[tmp_idx] = obj
        except:
            continue
        doc_num += 1

    loginfo.append('[PARSER] Total doc num: %s' % str(doc_num))
    loginfo.append('[PARSER] Total distinct doc num: %d \n' % len(result))

    return result

def remove_empty_str(data):
    if data is None:
        return None
    else:
        data = data.strip()
        if data == '':
            return None
        return data

def get_md5_tuple(obj):
    # check_sum = remove_empty_str(obj.get('md5',None))
    title_md5 = remove_empty_str(obj.get('title_md5',None))
    content_md5 = remove_empty_str(obj.get('content_md5',None))
    image_md5 = remove_empty_str(obj.get('image_md5',None))
    group_id = remove_empty_str(obj.get('group_id',None))
    cur_md5 = (title_md5, content_md5, image_md5, group_id)
    return cur_md5

def filter_and_distinct_by_md5(input_content, loginfo):
    non_servable = 0
    language_region_invalid = 0
    short_content = 0
    low_quality = 0

    new_cnt = add_cnt = merge_cnt = 0

    ### filter out invalid docs
    md5_maps = []
    for i in xrange(len(get_md5_tuple({}))):
        md5_maps.append({})
    article_list = []

    for obj in input_content:
        try:
            if obj['is_servable'] == False:
                non_servable += 1
                continue
            #region&lang incorrect
            if str(obj['language']) != 'hi' or str(obj['region']) != 'in':
                language_region_invalid += 1
                continue

            type = str(obj['type'])
            if not type in ['article', 'photostory', 'slideshow']:
                    continue
            if type == 'article' and int(obj['word_count']) < 400:
                    short_content += 1
                    continue

            if len(str(obj['title'])) <= 6:
                short_content += 1
                continue

            if str(obj.get('editor_level',0)) == '-5':
                low_quality += 1
                continue

            cur_md5 = get_md5_tuple(obj)
            cur_idxs = [None] * len(cur_md5)
            for i in xrange(len(cur_md5)):
                if cur_md5[i] is None or cur_md5[i] not in md5_maps[i]:
                    continue
                else:
                    cur_idxs[i] = md5_maps[i][cur_md5[i]]

            idx_list = sorted(set(filter(lambda x:x is not None,cur_idxs)))
            cur_idx = None
            if len(idx_list) ==0:
                article_list.append([obj])
                cur_idx = len(article_list) - 1
                for j in xrange(len(cur_md5)):
                    if cur_md5[j] is None:
                        continue
                    md5_maps[j][cur_md5[j]] = cur_idx
                new_cnt += 1
            elif len(idx_list) == 1:
                article_list[idx_list[0]].append(obj)
                cur_idx = idx_list[0]
                for j in xrange(len(cur_md5)):
                    if cur_md5[j] is None:
                        continue
                    md5_maps[j][cur_md5[j]] = cur_idx
                add_cnt += 1
            else:
                cur_idx = idx_list[0]
                for i in idx_list[1:]:
                    article_list[cur_idx] += article_list[i]
                    for obj2 in article_list[i]:
                        tmp_md5 = get_md5_tuple(obj2)
                        for j in xrange(len(tmp_md5)):
                            if tmp_md5[j] is None:
                                continue
                            md5_maps[j][tmp_md5[j]] = cur_idx

                    article_list[i] = None
                article_list[cur_idx].append(obj)

                merge_cnt += 1
        except:
            traceback.print_exc()

    loginfo.append("[PARSER] Filter info: %d non servable docs, %d invalid_region&language, %d short content, %d low quality_content." %(non_servable,language_region_invalid,short_content, low_quality))
    # cnt_map = {}
    # for a in article_list:
    #     if a is None:
    #         continue
    #     length = len(a)
    #     if length not in cnt_map:
    #         cnt_map[length] = 0
    #     cnt_map[length] += 1
    # for l in sorted(cnt_map):
    #     print l,cnt_map[l]
    # print len(article_list)
    return article_list

def get_sort_keys(obj):
    return (-obj.get('has_copyright',0),int(obj.get('discovery_time',0)) ,int(obj.get('publish_time',0)))

def parse_content_dump(input_content,loginfo,gmp=None):
    tmp_list = []
    content_list = []
    gmp_info = []

    article_list = filter_and_distinct_by_md5(input_content,loginfo)
    num_checksum = len(get_md5_tuple({}))

    for obj_list in article_list:
        if obj_list is None:
            continue
        obj_sorted = sorted(obj_list,key=get_sort_keys)
        grp_check_sum = []
        for idx in xrange(num_checksum):
            grp_check_sum.append(set())

        cur_cnt = 0
        for idx in xrange(len(obj_sorted)):
            cur_tuple=get_md5_tuple(obj_sorted[idx])
            dupe = False
            for tmp_idx in xrange(len(cur_tuple)):
                if cur_tuple[tmp_idx] in grp_check_sum[tmp_idx]:
                    dupe = True
                else:
                    grp_check_sum[tmp_idx].add(cur_tuple[tmp_idx])

            if dupe:
                continue

            obj = obj_sorted[idx]
            cur_cnt += 1

            try:
                item_id = str(obj['item_id'])
                publish_time = int(obj['publish_time'])
                discovery_time = int(obj['discovery_time'])

                publish_time = str(min(publish_time,discovery_time))

                #if len(obj['editorial_categories']) >= 1:
                    #category =  obj['editorial_categories']
                if len(obj['categories']) >=1:
                    category = [t['id'] for t in obj['categories'] if 'id' in t]
                    category += [t['name'] for t in obj['categories'] if 'name' in t]
                else:
                    category = []

                title = str(obj['title'])

                check_sum = obj.get('md5',None)
                title_md5 = obj.get('title_md5',None)
                content_md5 = obj.get('content_md5',None)
                image_md5 = obj.get('image_md5',None)
                has_copyright = obj.get('has_copyright')

                group_id = str(obj['group_id'])
                newsy_score = obj.get('newsy_score',0.0)

                if len(obj['channel_names']) >= 1:
                    channels = obj['channel_names']
                else:
                    channels = []

                images_cnt = len(obj['images'])
                head_image = 1 if obj['head_image'] != "" else 0

                word_count = obj['word_count']
                publisher = str(obj['publisher'])
                keywords_count = len(obj['entities'])
                #has_large_image = obj['has_large_image']
                tp = obj.get('type')

                src_type = str(obj.get('source_type'))
                #features = (images_cnt, keywords_count, word_count, head_images, type, str(obj['is_servable']))
                features = {'images_cnt':images_cnt, 'keywords_count':keywords_count, 'word_count':word_count, 'head_image':head_image,'type':tp, 'is_servable':str(obj['is_servable']), 'source_type':str(src_type),'newsy_score':newsy_score}
                if has_copyright is not None:
                    features['has_copyright'] = has_copyright
                else:
                    features['has_copyright'] = 0
                check_sum = ','.join(str(s) for s in [check_sum,title_md5,content_md5,image_md5])
                tmp_list.append([item_id,publisher,publish_time,channels,category,title,check_sum,group_id,features]);

            except:
                traceback.print_exc()
    joined_gmp_cnt = 0
    for t in tmp_list:
        if gmp:
            if t[0] in gmp:
                joined_gmp_cnt += 1
                c,ec,pop_score =  gmp.get(t[0])
                t[8]['explore'] = 'False'
                t.append(pop_score)
                dt = datetime.datetime.fromtimestamp(float(t[2])).strftime('%Y%m%d-%H:%M')
                doc_age = int(((time.time()-float(t[2]))/3600))
                gmp_info.append((t[0],pop_score,c,ec,doc_age,dt,t[5]))
            else:
                c,ec,pop_score =  gmp.get('total')
                t[8]['explore'] = 'True'
                t.append(pop_score)
        else:
            pop_score =  newsy_score
            t.append(pop_score)

        content_list.append(t)


    loginfo.append('[PARSER] Valid doc num deduped by checksum: %d, %d with non-zero GMP \n' % (len(content_list),joined_gmp_cnt))

    schema = {'item_id':0,'publisher':1,'publish_time':2,'channel_name':3,'category':4,'title':5,'check_sum':6,'group_id':7,'features':8,'pop_score':9}
    return content_list, schema, gmp_info

if __name__=='__main__':
    item1 = {'md5':'1', 'title_md5':'t1', 'image_md5':'i1', 'content_md5':'c1', 'groupid':'1', 'is_servable':True, 'language':'en','word_count':1000,'title':'1234567','type':'article','editor_level':1,'region':'in'}
    item2 = {'md5':'2', 'title_md5':'t2', 'image_md5':'i2', 'content_md5':'c2', 'groupid':'2', 'is_servable':True, 'language':'en','word_count':1000,'title':'1234567','type':'article','editor_level':1,'region':'in'}
    item3 = {'md5':'3', 'title_md5':'t3', 'image_md5':'i3', 'content_md5':'c3', 'groupid':'3', 'is_servable':True, 'language':'en','word_count':1000,'title':'1234567','type':'article','editor_level':1,'region':'in'}
    item4 = {'md5':'4', 'title_md5':'t3', 'image_md5':'i2', 'content_md5':'c1', 'groupid':'0', 'is_servable':True, 'language':'en','word_count':1000,'title':'1234567','type':'article','editor_level':1,'region':'in'}
    item5 = {'md5':'5', 'title_md5':'t5', 'image_md5':'i5', 'content_md5':'c5', 'groupid':'5', 'is_servable':True, 'language':'en','word_count':1000,'title':'1234567','type':'article','editor_level':1,'region':'in'}
    item6 = {'md5':'1', 'title_md5':'t1', 'image_md5':'i1', 'content_md5':'c1', 'groupid':'1', 'is_servable':True, 'language':'en','word_count':1000,'title':'1234567','type':'article','editor_level':1,'region':'in'}
    item7 = {'md5':'1', 'title_md5':'t1', 'image_md5':'i1', 'content_md5':'c1', 'groupid':'1', 'is_servable':True, 'language':'en','word_count':1000,'title':'1234567','type':'article','editor_level':1,'region':'in'}
    item8 = {'md5':'1', 'title_md5':'t1', 'image_md5':'i1', 'content_md5':'c1', 'groupid':'1', 'is_servable':True, 'language':'en','word_count':1000,'title':'1234567','type':'article','editor_level':1,'region':'in'}
    item9 = {'md5':'1', 'title_md5':'t1', 'image_md5':'i1', 'content_md5':'c1', 'groupid':'1', 'is_servable':True, 'language':'en','word_count':1000,'title':'1234567','type':'article','editor_level':1,'region':'in'}

    print filter_and_distinct_by_md5([item1,item2,item3,item4,item5],[])







