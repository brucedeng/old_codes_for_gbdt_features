DEFINE PROC_ALL_METRIC `get_all_metric.py` SHIP('./lib/get_all_metric.py');
REGISTER './lib/coke_udf.py' using jython as coke_udf;

-- DEFINE SCORING `python simulate_phase1.py $params` SHIP('./lib/simulate_phase1.py');

data = load '$input' as (uid:chararray, iid:chararray, action:int, score:float);

-- result = Stream data THROUGH SCORING as (uid:chararray, iid:chararray, action:int, score:float);
event_score = foreach data generate uid, iid, (action == 1?'c':'v') as action, score;

G = group event_score by uid parallel 40;

user_group = foreach G {
    c = filter event_score by action == 'c';
    o = order event_score by score DESC;
    l = limit o 3;
    top_item = foreach l generate uid, iid;
    generate group, event_score, COUNT(c) as pos_num, COUNT(event_score) as total_cnt, top_item;
};


/*per user*/
G = filter user_group by pos_num >0;

UserScores = foreach G generate group as uid, 
        (double)coke_udf.metric_mrr(event_score.(action, score)) as score_mrr,
        (double)coke_udf.metric_map(event_score.(action, score)) as score_map,
        (double)coke_udf.metric_auc(event_score.(action, score)) as score_auc;

Gall  = group UserScores all;

O_peruser  = foreach Gall generate 'all' as key, AVG(UserScores.score_mrr) as score_mrr, AVG(UserScores.score_map) as score_map, AVG(UserScores.score_auc)    as score_auc;

/*overall*/
event_score = foreach event_score generate
    score,
    1 as view:int,
    (action=='c'?1:0) as click;

G = group event_score ALL;

sorted_score_action = foreach G {
    D = order $1 by score desc;
    generate
    flatten(D);
};

O = stream sorted_score_action through PROC_ALL_METRIC as (current_view, current_click, view_click_mrr, view_click_map, view_click_roc_area,           view_click_pr_area, view_click_total_logLoss);

O_overall = foreach O generate 'all' as key,current_view, current_click, view_click_roc_area, view_click_pr_area, view_click_total_logLoss;


/*Diversity metric*/
user_group = filter user_group by total_cnt >= 10;
rec_items = foreach user_group generate flatten(top_item) as (uid, iid);

uids = foreach rec_items generate uid;
d_uids = distinct uids;
total_user = foreach (group d_uids all) generate COUNT(d_uids) as user_cnt;

iids = foreach rec_items generate iid;
d_iids = distinct iids;
total_item = foreach (group d_iids all) generate COUNT(d_iids) as item_cnt;

rec_count = foreach (group rec_items by iid PARALLEL 100) {
    rec_uid = foreach rec_items generate uid;
    d = distinct rec_uid;
    generate group as iid, COUNT(d) as rec_cnt, total_user.user_cnt as cnt;
};

rec_metric = foreach rec_count generate flatten(coke_udf.diversity_metric(rec_cnt,cnt,total_item.item_cnt)) as (entropy_div, gini_div, herfindahl_div);

O_diversity = foreach (group rec_metric all) generate 'all' as key, SUM($1.entropy_div) as entropy_div, 1 - SUM($1.herfindahl_div) as herfindahl_div;

result = FOREACH ( JOIN O_peruser by key, O_overall by key , O_diversity by key) GENERATE
    O_overall::current_view as current_view,
    O_overall::current_click as current_click,
    O_overall::view_click_roc_area as view_click_roc_area,
    O_overall::view_click_pr_area as view_click_pr_area,
    O_overall::view_click_total_logLoss as view_click_total_logLoss,
    O_peruser::score_mrr as score_mrr,
    O_peruser::score_map as score_map,
    O_peruser::score_auc as score_auc,
    O_diversity::entropy_div as entropy_div,
    O_diversity::herfindahl_div as herfindahl_div;

rmf $output_metric;
store result into '$output_metric';



