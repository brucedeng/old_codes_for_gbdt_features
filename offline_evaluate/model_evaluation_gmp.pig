DEFINE PROC_ALL_METRIC `get_all_metric.py` SHIP('./lib/get_all_metric.py');
REGISTER './lib/coke_udf.py' using jython as coke_udf;

--event_sample = load '$sample' as (uid, iid, action, score);
event_sample = load '$sample' as (uid:chararray, iid:chararray, action:chararray, f1:chararray, f2:map[], f3:map[]);
event_score = foreach event_sample generate uid, (action == '1'?'c':'v') as action, RANDOM() as score;


/*per user metric*/
G = group event_score by uid parallel 40;

UserScores = foreach G generate group as uid, (double)coke_udf.metric_mrr(event_score.(action, score)) as score_mrr, 
																						 (double)coke_udf.metric_map(event_score.(action, score)) as score_map,
																						 (double)coke_udf.metric_auc(event_score.(action, score)) as score_auc;

Gall  = group UserScores all;

O  = foreach Gall generate AVG(UserScores.score_mrr) as score_mrr, AVG(UserScores.score_map) as score_map, AVG(UserScores.score_auc) as score_auc;
rmf $peruser_metric;
store O into '$peruser_metric';


/*overall metric*/
event_score = foreach event_score generate
    score,
    1 as view:int,
    (action=='c'?1:0) as click;

G = group event_score ALL;

sorted_score_action = foreach G {
    D = order $1 by score desc;
    generate 
    flatten(D);
}

O = stream sorted_score_action through PROC_ALL_METRIC as (current_view, current_click, view_click_mrr, view_click_map, view_click_roc_area, view_click_pr_area, view_click_total_logLoss);

O = foreach O generate  current_view, current_click, view_click_roc_area, view_click_pr_area, view_click_total_logLoss;

rmf $overall_metric;
store O into '$overall_metric';


