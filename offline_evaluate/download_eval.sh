#!/bin/bash

printf "experiment\tpv\tclick\toverall auc\toverall pr\toverall log loss\tperuser mrr\tperuser map\tperuser auc\tentropy_div\therfindahl_div\n"
for data in $*; do
	name=`basename $data`
	printf "$name\t"
	hadoop fs -cat $data/*
done
