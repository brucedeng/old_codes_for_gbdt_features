
queue=default
while getopts "d:q:" arg
do
    case $arg in
        d) #sample data path
        run_eval="true"
        sample_data=$OPTARG
        ;;
        q) #queue
        queue=$OPTARG
        ;;
        ?)  #?
        echo "unkonw argument"
        exit 1
        ;;
    esac
done
                                                                                                                                                                                    
#PIG_PARAM="-conf ./default-hadoop.xml -useHCatalog ";
PIG_PARAM=" -Dmapred.job.queue.name=$queue";

echo "evaluate scored result: "$sample_data
basedir=`dirname $sample_data`
name=`basename $sample_data`

rm -f pig*.log

if [ "$run_eval" = "true" ]; then
	

	output_metric="$basedir/$name.eval/";
        
	cmd="pig $PIG_PARAM -p input='$sample_data' -p output_metric='$output_metric' model_evaluation.pig"
        echo $cmd
        eval $cmd;
        if [ $? != 0 ]; then echo "ERROR: Failed to run pipeline!"; exit -1; fi
fi


