
import sys

if __name__ == '__main__':
    coef = {'cat_rel':1.0707,'gmp':2.0256,'kw_rel':0}

    if len(sys.argv) >= 2:
        params = sys.argv[1].split('_')
        coef['gmp'] = float(params[0])
        coef['cat_rel'] = float(params[1])
        coef['kw_rel'] = float(params[2])

    gmp_cnt = 0
    gmp_sum = 0
    cat_cnt = 0
    cat_sum = 0
    kw_cnt = 0
    kw_sum = 0
    total_cnt = 0

    for line in sys.stdin:
        fmap = {}
        fields = line.rstrip().split('\t')

        for f in fields[-1].split(' ')[1:]:
            fid, value = f.split(':')
            fmap[str(fid)] = float(value)

        U_CAT_LEN = fmap.get('62',0)
        U_KW_LEN = fmap.get('64',0)

        if U_CAT_LEN == 0 and U_KW_LEN == 0:
            continue

        gmp = fmap.get('47',0.0)
        cat_rel = fmap.get('1',0.0)
        kw_rel = fmap.get('56',0.0)

        gmp_cnt += 1 if gmp >0 else 0
        gmp_sum += gmp
        cat_cnt += 1 if cat_rel>0 else 0
        cat_sum += cat_rel
        kw_cnt += 1 if kw_rel >0 else 0
        kw_sum += kw_rel
        total_cnt += 1

        score = cat_rel*coef['cat_rel'] + gmp*coef['gmp'] + kw_rel*coef['kw_rel']

        print '\t'.join(fields[0:3]+[str(score)])

    #summary = 'gmp_cover:%.4f, cat_cover:%.4f, kw_cover:%.4f, gmp_avg:%.4f, cat_avg:%.4f, kw_avg:%.4f' % (float(gmp_cnt)/total_cnt, float(cat_cnt)/total_cnt, float(kw_cnt)/total_cnt, gmp_sum/gmp_cnt, cat_sum/cat_cnt, kw_sum/kw_cnt)
    #print '\t'.join(['SUMMARY',summary])




