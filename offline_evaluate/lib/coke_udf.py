import re
import datetime
import sys
import math

@outputSchema("y:chararray")
def timestamp2date(timestamp):
    date = datetime.date.fromtimestamp(timestamp)
    return  "%s"%date

@outputSchema("y:chararray")
def dump_feat_json(feats):
    ostr = None
    try:

        ostr = "["
        k = 0
        for type, name, val in feats:
            if k>0:
                ostr += ", "
            ostr += '{"type":"%s","name":"%s","value":%s}'%(type,name,val)
            k += 1
        ostr += "]"
        return ostr
    except:
        return None


@outputSchema("y:bag{t:tuple(type:chararray, name:chararray, val:double)}")
def aggregate_bags_of_feat_val(bags_of_feats):
    acc = {} # accumulated feat val
    for feats in bags_of_feats:
        if feats==None or feats[0]==None:
            continue
        try:
            for type, name, val in feats[0]:
                if val == None:
                    continue
                if acc.has_key((type,name)):
                    acc[(type,name)] += val
                else:
                    acc[(type,name)] = val
        except:
            #sys.stderr.write("%s\n"%feats)
            continue
    out = []
    for (type,name),val in acc.items():
        out.append((type,name,val))
    return out


@outputSchema("y:double")
def score_profile(profile, doc_feats, type_use):
    # both a and b should be bags of type,name,val tuples
    q_dict = {}
    if profile == None or len(profile)==0:
        return 0.0
    for type, name, val in profile:
        if val != None and re.match(type_use, type):
            q_dict[(type,name)] = val

    res = 0.0
    if doc_feats==None or len(doc_feats)==0:
        return 0.0
    for type, name, val in doc_feats:
        key = (type, name)
        if q_dict.has_key(key) and val  != None:
            res += val*q_dict[key]

    return res

@outputSchema("y:chararray")
def extract_les_uid(uf_str):
    if uf_str==None:
        return None
    for tkn in uf_str.split(','):
        if tkn.find('y:')==0:
            return tkn
    return None


@outputSchema("y:bag{t:tuple(pos:int, pkgt:int, item:chararray)}")
def parse_les_context(context_str):
    out = []
    try:
        if context_str==None or len(context_str)==0:
            return None
        parts = context_str.split('\001')
        for part in parts:
            tkns = part.split('\002')
            pos, item = tkns[0], tkns[1]
            m = re.search('\d+', pos)
            if m!=None:
                pos = int(m.group(0))
                pkgt = None
                if len(tkns)>2:
                    for att in tkns[2].split('\003'):
                         k,v = att.split('\004')
                         if k=="pkgt":
                             pkgt = int(v)

                out.append((pos, pkgt, item))
        return out
    except:
        return []


@outputSchema("y:chararray")
def get_att_gcontext(context_str, att):
    try:
        if context_str==None or len(context_str)==0:
            return None
        parts = context_str.split('\001')
        for part in parts:
            tkns = part.split('\002')
            k, v = tkns[0], tkns[1]
            if k==att:
                return v
        return None
    except:
        return None




@outputSchema("y:bag{t:tuple(time:long, pos:int, pkgt:int, rcode:chararray, item:chararray, action:chararray)}")
def match_clicks(events):
    if events==None:
        return []
    events_sorted = sorted(events, key = lambda event: event[0])
    strm_table = {}
    skips_above = {}
    skips_below = {}
    views = {} # map item views to view context
    clicks = {}
    out = []
    for time, pos, pkgt, item, action, rcode in events_sorted:
        ctx = (pos, pkgt, rcode, time)
        if action=="v":
            strm_table[pos] = (item, "v", pkgt, rcode, time)
            if views.has_key(item):
                if views[item].has_key(ctx):
                    views[item][ctx] += 1
                else:
                    views[item][ctx] = 1
            else:
                views[item] = {ctx: 1}

        elif action=="c":
            if strm_table.has_key(pos):
                item2, action2, pkgt2, rcode2, time2 = strm_table[pos]
            else:
                item2, action2, pkgt2, rcode2, time2 = None, None, None, None, None

            strm_table[pos] = (item, "c", pkgt, rcode, time)
            clicks[item] = ctx
            if skips_above.has_key(item):
                del skips_above[item]

            if skips_below.has_key(item):
                del skips_below[item]

            if views.has_key(item) and views[item].has_key(ctx):
                views[item][ctx] -= 1

            if item2!=item: # mismatched click
                out.append((time,pos,pkgt,rcode,item,action))
    return out

# bag of key value pairs to tuple, with specificed key order
@outputSchema("t:tuple()")
def kv_bag2tuple(kvs, *key_order):
    kvs_map = {}
    for k,v in kvs:
        kvs_map[k] = v
    out = []
    for k in key_order:
        if kvs_map.has_key(k):
            out.append(float(kvs_map[k]))
        else:
            out.append(0.0)
    return tuple(out)

@outputSchema("y:bag{t:tuple(time:long, pos:int, pkgt:chararray, rcode:chararray, item:chararray, action:chararray)}")
def interpret_stream_action(events):
    if events==None:
        return []
    events_sorted = sorted(events, key = lambda event: event[0])
    strm_table = {}
    skips_above = {}
    skips_below = {}
    views = {} # map item views to view context
    clicks = {}

    for time, pos, pkgt, item, action, rcode in events_sorted:
        pos = str(pos)
        m = re.search('\d+', pos)
        if m!=None:
            pos = int(m.group(0))
        else:
            continue

        ctx = (pos, pkgt, rcode, time)

        if action=="v":
            strm_table[pos] = (item, "v", pkgt, rcode, time)
            if views.has_key(item):
                if views[item].has_key(ctx):
                    views[item][ctx] += 1
                else:
                    views[item][ctx] = 1
            else:
                views[item] = {ctx: 1}

        elif action=="c":
            if strm_table.has_key(pos):
                item2, action2, pkgt2, rcode2, time2 = strm_table[pos]
            else:
                item2, action2, pkgt2, rcode2, time2 = None, None, None, None, None

            strm_table[pos] = (item, "c", pkgt, rcode, time)
            clicks[item] = ctx
            if skips_above.has_key(item):
                del skips_above[item]

            if skips_below.has_key(item):
                del skips_below[item]

            if views.has_key(item) and views[item].has_key(ctx):
                views[item][ctx] -= 1

            if item2!=item: # mismatched click
                continue

            # skips above
            i = pos - 1
            while i > 0:
                if strm_table.has_key(i):
                    item, state, pkgt, rcode, time = strm_table[i]
                    if state == "v": # detect a skip
                        state = "s"
                        strm_table[i] = (item, state, pkgt, rcode, time)
                        ctx = (i, pkgt, rcode, time)
                        d = pos - i
                        skips_above[item] = ctx, d
                i -= 1

            # skips below
            i = pos + 1
            while i < pos+4:
                if strm_table.has_key(i):
                    item, state, pkgt, rcode, time = strm_table[i]
                    if state == "v": # detect a skip
                        state = "s"
                        strm_table[i] = (item, state, pkgt, rcode, time)
                        ctx = (i, pkgt, rcode, time)
                        d = i - pos
                        skips_below[item] = ctx, d
                i += 1

    out = []
    for item, (ctx, d) in skips_above.items():
        pos, pkgt, rcode, time = ctx
        '''
        if d <= 3:
            d = d
        elif d <= 10:
            d = 10
        elif d <= 20:
            d = 20
        else:
            d = 100
        '''
        out.append((time, pos, pkgt, rcode, item, "s-%d"%d))

    for item, (ctx, d) in skips_below.items():
        pos, pkgt, rcode, time = ctx
        out.append((time, pos, pkgt, rcode, item, "s+%d"%d))

    for item, ctx in clicks.items():
        pos, pkgt, rcode, time = ctx
        out.append((time, pos, pkgt, rcode, item, "c"))

#    for item, ctx_cnts in views.items():
#        for ctx, cnt in ctx_cnts.items():
#            pos, pkgt, rcode, time = ctx
#            for i in xrange(cnt):
#                out.append((time, pos, pkgt, rcode, item, "v"))
    return out


@outputSchema("a:bag{t:tuple(date:chararray,pos:int, pkgt:int, item:chararray, action:chararray)}")
def resolve_actions(item_action_bag):
    resolved_actions = {}
    for date, pos, pkgt, item, action in item_action_bag:
        if not resolved_actions.has_key(item) or action == 'c':
            resolved_actions[item] = (date, action, pos, pkgt)
        elif resolved_actions[item] == 'v':
            resolved_ations[item] = (date, action, pos, pkgt)
    out = []
    for item, (date, action, pos, pkgt) in resolved_actions.items():
        out.append((date, pos, pkgt, item, action))
    return out

@outputSchema("pos:int, neg:int")
def pos_neg(act_score_bag):
    k0 = 0
    k1 = 0
    for act, val in act_score_bag:
        if act=='c':
            k1 += 1
        else:
            k0 += 1
    return k0,k1


@outputSchema("y:double")
def core_compute(gmp, agg, facebook, inferred, positive, negative, ed_promote, ed_demote):
    return negative * ed_demote * ( math.pow(math.pow(gmp,4)+math.pow(agg,4)+math.pow(ed_promote,4),0.25) + math.pow(math.pow(inferred,4)+math.pow(facebook,4)+math.pow(positive,4),0.25) )


@outputSchema("y:double")
def metric_mrr(act_score_bag):
    act_score_sorted = sorted(act_score_bag, key=lambda pair:pair[1], reverse=True)
    mrr = 0.0
    i = 1
    k = 0
    for act, val in act_score_sorted:
        if act=='c':
            mrr += 1.0/i
            k += 1
        i += 1
    if k>0:
        mrr /= k
    return mrr


@outputSchema("y:double")
def metric_map(act_score_bag):
    act_score_sorted = sorted(act_score_bag, key=lambda pair:pair[1], reverse=True)
    map = 0.0
    k0 = 0
    k1 = 0
    for act, val in act_score_sorted:
        if act=='c':
            k1 += 1
            map += float(k1)/float(k0+k1)
        else:
            k0 += 1
    if k1>0:
        map /= k1
    return map


@outputSchema("y:double")
def metric_auc(act_score_bag):
    act_score_sorted = sorted(act_score_bag, key=lambda pair:pair[1], reverse=True)
    auc = 0.0
    k0 = 0
    k1 = 0
    tp0 = 0.0
    fp0 = 0.0
    tp1 = 1.0
    fp1 = 1.0
    P = 0
    N = 0
    for act, val in act_score_sorted:
        if act=='c':
            P += 1.0
        else:
            N += 1.0
    if P==0:
        return 0.0
    if N==0:
        return 1.0

    for act, val in act_score_sorted:
        if act=='c':
            k1 += 1
            tp1 = float(k1)/P
            fp1 = float(k0)/N
            auc += (fp1-fp0)*(tp1+tp0)/2
            tp0 = tp1
            fp0 = fp1
        else:
            k0 += 1
    auc += 1.0 - fp1
    return auc

@outputSchema("y:double")
def metric_recency(age_score_bag):
    age_score_sorted = sorted(age_score_bag, key=lambda pair:pair[1], reverse=True)
    avg_age = 0.0
    cnt = 0
    for age, val in age_score_sorted:
        if cnt==3:
            break
        cnt += 1
        avg_age += float(age)
    avg_age /= cnt
    recency = 1.0/avg_age
    return recency

@outputSchema("y:double")
def f_standard_dcg(label_score):
  val = 0.0
  i = 0
  #print "one sort:"
  for label, score in label_score:
  #  print label, score
    i += 1
    y = float(label) + 0.0
    if y <= 0:
      continue
    else:
      val += y/math.log(i+1)
  return val

@outputSchema("y:double")
def metric_standard_ndcg(act_score_bag):
  act_score_sorted_by_score = sorted(act_score_bag, key=lambda pair:pair[1], reverse=True)
  act_score_sorted_by_act = sorted(act_score_bag, key=lambda pair:pair[0], reverse=True)
  idcg = f_standard_dcg(act_score_sorted_by_act) # ideal dcg
  dcg = f_standard_dcg(act_score_sorted_by_score) # achieved dcg
  if idcg>0:
    return dcg/idcg
  else:
    return 0.0


@outputSchema("y:double")
def metric_standard_ndcgk(act_score_bag, K):
  act_score_sorted_by_score = sorted(act_score_bag, key=lambda pair:pair[1], reverse=True)
  act_score_sorted_by_act = sorted(act_score_bag, key=lambda pair:pair[0], reverse=True)
  idcg = f_standard_dcg(act_score_sorted_by_act[:K]) # ideal dcg
  dcg = f_standard_dcg(act_score_sorted_by_score[:K]) # achieved dcg
  if idcg>0:
    return dcg/idcg
  else:
    return 0.0


@outputSchema("y:double")
def metric_mean_label(label_score):
  val = 0.0
  i = 0
  for label, score in label_score:
    i += 1
    val += float(label)
  if i == 0:
    return 0.0
  val /= float(i)
  return val

@outputSchema("y:double")
def metric_mean_labelk(act_score_bag, K):
  act_score_sorted = sorted(act_score_bag, key=lambda pair:pair[1], reverse=True)
  val = metric_mean_label(act_score_sorted[:K])
  return val


@outputSchema("y:double")
def metric_wauc(act_score_bag):
  act_score_sorted = sorted(act_score_bag, key=lambda pair:pair[1], reverse=True)
  auc = 0.0
  k0 = 0
  k1 = 0
  tp0 = 0.0
  fp0 = 0.0
  tp1 = 1.0
  fp1 = 1.0
  P = 0
  N = 0
  for act, val in act_score_sorted:
    label = float(act)
    if(label > 0 ):
      P = P + label
    else:
      N += 1.0
  if P == 0:
    return 0.0
  if N == 0:
    return 1.0

  for act, val in act_score_sorted:
    label = float(act)
    if (label > 0 ):
      k1 = k1 + label
      tp1 = float(k1)/P
      fp1 = float(k0)/N
      auc += (fp1-fp0)*(tp1+tp0)/2
      tp0 = tp1
      fp0 = fp1
    else:
      k0 += 1
  auc += 1.0 - fp1
  return auc

@outputSchema("t:tuple(entropy:double, gini:double, herfindahl:double)")
def diversity_metric(rec_cnt, user_cnt, item_cnt):

    prob = float(rec_cnt)/float(user_cnt)
    entropy = - prob * math.log(prob)
    gini = 0.0
    herfindahl = prob**2

    return (entropy, gini, herfindahl)

