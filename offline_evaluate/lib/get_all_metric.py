#! /usr/bin/env python

import math
import sys,traceback


def generate_roc_pr():

    score_index = 1
    view_index = 2
    click_index = 3
    current_click = 0
    current_view = 0
    view_click = 0
    # sub area initial
    view_click_roc_sub_area = 0
    view_click_pr_sub_area = 0
    prev_keyname = 'all'
    # area initial
    view_click_roc_area = 0
    view_click_pr_area = 0
    # view->click
    view_click_pr_pre_y = 0

    prev_click = 0

    # view -> click mrr, map
    view_click_mrr = 0.0
    view_click_map = 0.0

    # log loss
    # view->click
    view_click_log_loss_factor1 = 0
    view_click_log_loss_factor2 = 0
    view_click_log_loss_total_neg = 0

    for line in sys.stdin:
        try:
            fields = line.strip().split('\t')
            field_len = len(fields)
            if field_len < click_index or field_len < view_index:
                continue

            # current line values: click, view
            tk_click = float(fields[click_index - 1])
            tk_view = float(fields[view_index - 1])
            tk_score = float(fields[score_index - 1])

            # cumulative (view - click)
            view_click += (tk_view - tk_click)

            current_click += float(fields[click_index - 1])
            current_view += float(fields[view_index - 1])

            # generate view-> click roc
            view_click_roc_sub_area = (tk_view - tk_click)*(current_click + prev_click)/2
            view_click_roc_area += view_click_roc_sub_area

            # generate view->click pr
            if current_click + view_click > 0:
                view_click_r = float(current_click)/(current_click + view_click)
                view_click_pr_sub_area = (tk_click)*(view_click_r + view_click_pr_pre_y)/2
                view_click_pr_pre_y = view_click_r
                view_click_pr_area += view_click_pr_sub_area

            # generate view->click mrr
            view_click_mrr += float(tk_click)/current_view

            # genenrate view->click map
            if tk_click > 0:
                view_click_map += float(current_click)/current_view

            # geneate view->click logloss
            if tk_score < 10e-6 or tk_score >= 1:
                continue
            tmp_tk_click = tk_click
            for i in xrange(0, int(tk_view)):
                if tmp_tk_click > 0:
                    view_click_log_loss_factor1 += math.log(float(fields[score_index - 1]))
                    tmp_tk_click -= 1
                else:
                    view_click_log_loss_factor2 += math.log(1-float(fields[score_index - 1]))
            view_click_log_loss_total_neg += tk_view

            prev_click = current_click
        except:
            traceback.print_exc()
            continue

    # last pkg
    try:
        if current_click * view_click > 0:
            view_click_roc_area = view_click_roc_area/(current_click * view_click)
        else:
            view_click_roc_area = 0

        # view-click pr
        if current_click > 0:
            view_click_pr_area = view_click_pr_area/current_click
        else:
            view_click_pr_area = 0

        # view -> click mrr, map
        view_click_mrr = float(view_click_mrr)/current_click
        view_click_map = float(view_click_map)/current_click

        # logloss
        # view->click
	if view_click_log_loss_total_neg >0:
	    view_click_total_logLoss = -1 * (view_click_log_loss_factor1 + view_click_log_loss_factor2)/view_click_log_loss_total_neg
	else:
	    view_click_total_logLoss = 0


        current_bin = [str(current_view), str(current_click), str(view_click_mrr), str(view_click_map), str(view_click_roc_area), str(view_click_pr_area), str(view_click_total_logLoss)]
        print '\t'.join(current_bin)
    except:
        traceback.print_exc()
        pass

if __name__ == '__main__':
    generate_roc_pr()
