
while getopts "d:" arg
do
    case $arg in
        d) #sample data path
        run_eval="true"
        sample_data=$OPTARG
        ;;
        ?)  #?
        echo "unkonw argument"
        exit 1
        ;;
    esac
done
                                                                                                                                                                                    
#PIG_PARAM="-conf ./default-hadoop.xml -useHCatalog ";
PIG_PARAM=" -Dmapred.job.queue.name=experiment";

echo "evaluate scored result: "$sample_data
basedir=`dirname $sample_data`
echo "basedir is $basedir"
name=`basename $sample_data`
echo "name is $name"
rm -f pig*.log

if [ "$run_eval" = "true" ]; then
    gmp_weight=1.0
    cat_weight=0.15
    kw_weight=0.5

    step=5
    max_weight=0.15
    
    rm -f grid_search_result.txt
    touch grid_search_result.txt

    while [ $(echo "$cat_weight <= $max_weight" | bc) -eq 1 ]
    do
	    # peruser_metric="$basedir/$name.eval/peruser";
        params="${gmp_weight}_${cat_weight}_${kw_weight}"
	    output_metric="$basedir/$name.eval/$params";

	    cmd="pig $PIG_PARAM -p params=$params -p input='$sample_data' -p output_metric='$output_metric' simulate_phase1_div_featurelog.pig"
	    #cmd="pig $PIG_PARAM -p params=$params -p input='$sample_data' -p output_metric='$output_metric' simulate_phase1_div_featurelog.pig"
        echo $cmd
        eval $cmd;
        if [ $? != 0 ]; then echo "ERROR: Failed to run pipeline!"; exit -1; fi
        cat_weight=$(echo "$cat_weight + $step" | bc | awk '{printf "%.2f", $0}')
        hadoop fs -cat $output_metric/* >> grid_search_result.txt

    done
fi


