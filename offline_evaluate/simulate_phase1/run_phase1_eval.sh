
while getopts "d:" arg
do
    case $arg in
        d) #sample data path
        run_eval="true"
        sample_data=$OPTARG
        ;;
        ?)  #?
        echo "unkonw argument"
        exit 1
        ;;
    esac
done
                                                                                                                                                                                    
#PIG_PARAM="-conf ./default-hadoop.xml -useHCatalog ";
PIG_PARAM=" -Dmapred.job.queue.name=experiment";

echo "evaluate scored result: "$sample_data
basedir=`dirname $sample_data`
name=`basename $sample_data`

rm -f pig*.log

if [ "$run_eval" = "true" ]; then
    gmp_weight=1.0
    cat_weight=0.10
    kw_weight=0.5

    step=0.5
    max_weight=5
    
    rm -f grid_search_result.txt
    touch grid_search_result.txt

    while [ $(echo "$kw_weight <= $max_weight" | bc) -eq 1 ]
    do
	    # peruser_metric="$basedir/$name.eval/peruser";
        params="${gmp_weight}_${cat_weight}_${kw_weight}"
	    output_metric="$basedir/$name.eval/$params";

	    cmd="pig $PIG_PARAM -p params=$params -p input='$sample_data' -p output_metric='$output_metric' simulate_phase1_div.pig"
	    #cmd="pig $PIG_PARAM -p params=$params -p input='$sample_data' -p output_metric='$output_metric' simulate_phase1.pig"
        echo $cmd
        eval $cmd;
        if [ $? != 0 ]; then echo "ERROR: Failed to run pipeline!"; exit -1; fi
        kw_weight=$(echo "$kw_weight + $step" | bc | awk '{printf "%.2f", $0}')
        hadoop fs -cat $output_metric/* >> grid_search_result.txt

    done
fi


