DEFINE PROC_ALL_METRIC `get_all_metric.py` SHIP('./lib/get_all_metric.py');
REGISTER './lib/coke_udf.py' using jython as coke_udf;

DEFINE SCORING `python simulate_phase1.py $params` SHIP('./lib/simulate_phase1.py');

data = load '$input' as (uid:chararray, iid:chararray, action:int, raw:chararray);

result = Stream data THROUGH SCORING as (uid:chararray, iid:chararray, action:int, score:float);

summary = filter result by uid == 'SUMMARY';
rmf /tmp/chenkehan/phase1_summary;
store summary into '/tmp/chenkehan/phase1_summary';

event_score = filter result by uid != 'SUMMARY';
event_score = foreach event_score generate uid, iid, (action == 1?'c':'v') as action, score;


--event_score = sample event_score 0.1;

G = group event_score by uid parallel 40;

G = foreach G {
    c = filter event_score by action == 'c';
    generate group, event_score, COUNT(c) as pos_num;
}

/*per user*/
G = filter G by pos_num >0;

UserScores = foreach G generate group as uid, 
        (double)coke_udf.metric_mrr(event_score.(action, score)) as score_mrr,
        (double)coke_udf.metric_map(event_score.(action, score)) as score_map,
        (double)coke_udf.metric_auc(event_score.(action, score)) as score_auc;

Gall  = group UserScores all;

O_peruser  = foreach Gall generate 'all' as key, AVG(UserScores.score_mrr) as score_mrr, AVG(UserScores.score_map) as score_map, AVG(UserScores.score_auc)    as score_auc;

/*overall*/
event_score = foreach event_score generate
    score,
    1 as view:int,
    (action=='c'?1:0) as click;

G = group event_score ALL;

sorted_score_action = foreach G {
    D = order $1 by score desc;
    generate
    flatten(D);
};

O = stream sorted_score_action through PROC_ALL_METRIC as (current_view, current_click, view_click_mrr, view_click_map, view_click_roc_area,           view_click_pr_area, view_click_total_logLoss);

O_overall = foreach O generate 'all' as key, view_click_roc_area, view_click_pr_area, view_click_total_logLoss;

result = FOREACH ( JOIN O_peruser by key full outer, O_overall by key ) GENERATE
    '$params' as params,
    O_overall::view_click_roc_area as view_click_roc_area,
    O_overall::view_click_pr_area as view_click_pr_area,
    O_peruser::score_mrr as score_mrr,
    O_peruser::score_map as score_map,
    O_peruser::score_auc as score_auc;

rmf $output_metric;
store result into '$output_metric';



