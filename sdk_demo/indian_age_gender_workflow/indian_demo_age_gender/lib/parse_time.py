#!/usr/bin/jython
import sys
import math
import re
import string
import datetime

if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper

@outputSchema("t:chararray")
def parse_timestamp(unix_ts):
    if unix_ts is None:
        return None
    else:
        return datetime.datetime.fromtimestamp(unix_ts) .strftime('%Y%m%d')

if __name__=='__main__':
    # import json
    # test_file=sys.argv[1]
    # f=open(test_file)
    # for line in f:
    #     fields = line.split('\t')
    #     data = fields[1]
    #     input_map=json.loads(data)
    #     input_list=[]
    #     for k in input_map:
    #         input_list.append((k['ftype'],k['fname'],k['fvalue']))

    #     print get_user_feature_ratio(input_list,'game_strategy')
    # f.close()
    pass

