#!/usr/bin/jython
import sys
import math
import re
import string
import datetime

if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper

gp_map={
    "10":"weather",
    "11":"entertainment",
    "12":"media_and_video",
    "14":"tools",
    "15":"photography",
    "16":"productivity",
    "17":"education",
    "18":"news_and_magazines",
    "19":"travel_and_local",
    "35":"game_casual",
    "36":"game_sports",
    "50":"game_puzzle",
    "37":"game_adventure",
    "51":"game_casino",
    "38":"game_action",
    "52":"game_music",
    "39":"game_family",
    "1":"personalization",
    "2":"transportation",
    "3":"sports",
    "4":"health_and_fitness",
    "6":"comics",
    "7":"medical",
    "8":"business",
    "9":"books_and_reference",
    "20":"lifestyle",
    "21":"social",
    "22":"finance",
    "23":"shopping",
    "24":"libraries_and_demo",
    "25":"communication",
    "26":"music_and_audio",
    "40":"game_educational",
    "41":"game_word",
    "42":"game_board",
    "43":"game_simulation",
    "44":"game_trivia",
    "45":"game_racing",
    "46":"game_strategy",
    "47":"game_card",
    "48":"game_arcade",
    "49":"game_role_playing"
}

@outputSchema('data:{T:(gpcategory:chararray)}')
def get_all_gp():
    v = gp_map.values()
    return [(_k,) for _k in v ]

@outputSchema('data:{T:(fid:int,fname:chararray,ftype:chararray)}')
def get_featmap(features):
    if features is None:
        return []
    result = []
    featnames=set()
    for k in features:
        featnames.add(k[0])
    idx=1
    for f in sorted(featnames):
        result.append((idx,f,'q'))
        idx += 1
    return result

@outputSchema("data:(train:chararray,ignore_features:{T:(fname)})")
def format_features(label,feature,needed_features):
    result=[label]
    fv={}
    ignore_list = []
    if feature is None:
        return str(label) + ' '
    feature = dict(feature)

    for item in needed_features:
        (fid,fname,ftype)=item
        if fname in feature:
            value=feature[fname]
            del(feature[fname])
            val_str = '%g'%value
            result.append(str(fid)+':'+val_str)
    for f in feature:
        ignore_list.append((f,))

    return ( ' '.join([str(_k) for _k in result]) , ignore_list )

@outputSchema("train:chararray")
def format_features_with_name(label,feature):
    result=[label]
    fv={}
    ignore_list = []
    if feature is None:
        return str(label) + ' '
    
    for item in feature:
        result.append(item[0]+ ":" + str(item[1]))

    return ( ' '.join([str(_k) for _k in result]) )

@outputSchema("data:chararray")
def map_gp_category(gp_id):
    if gp_id in gp_map:
        return gp_map[gp_id]
    try:
        sys.stderr.write("not found gpcategory id " + str(gp_id) + " in gpcategory mapping\n")
    except:
        sys.stderr.write("error encoding gpid name")
    return ''

@outputSchema("b:bag{t:(pkgname:chararray,type:int)}")
def pkgname_map_new(file1,file2):
    a = {}
    b = {}
    for k,v in file1:
        b[str(k)] = v
#    file1_set = set([_k[1] for _k in file1])
    file2_set = set([_k[0] for _k in file2])
    #print file2_set
    for m in b.values():
        if m in file2_set:
            a[str(m)]=1
        else:
            a[str(m)]=0
    #print b.keys()
    #print b.values()
    res = [(n,a[b[n]]) for n in b.keys()]
    return res


@outputSchema("count:map[]")
def to_map(input_bag):
    result={}
    if input_bag is None:
        return result
    for item in input_bag:
        if len(item)==2:
            (key,value)=item
            result[key]=value
    return result

def gen_status_or_run_feature(feature,name):
    result = []
    if feature is not None:
        sum_idf = 0.0
        sum_cnt = 0.0
        for f in feature:
            gp,tfidf,cnt = f
            try:
                sum_idf += tfidf
                sum_cnt += cnt
            except:
                pass
            if gp != '':
                #result.append(('F_' + name + '-CNT-' + gp.upper(),cnt))
                result.append(('F_' + name + '-IDF-' + gp.upper(),tfidf))
        for f in feature:
            gp,tfidf,cnt = f
            try:
                tfidf = float(tfidf)
            except:
                tfidf = None
            if gp != '':
                if sum_idf != 0:
                    pass
                    #result.append(('R_' + name + '-CNT-' + gp.upper(),float(cnt)/sum_cnt ))
                if sum_idf != 0 and tfidf is not None:
                    result.append(('R_' + name + '-IDF-' + gp.upper(),float(tfidf)/sum_idf ))
    return result

def parse_float(data):
    try:
        return float(data)
    except:
        print "error parsing " + data
        return None

def gen_pkg_feature(feature,name):
    result = []
    added=False
    if feature is not None:
        feature_limit = [parse_float(f[1]) for f in feature]
        feature_limit = filter(lambda x:x is not None, feature_limit)
        feature_limit.append(0.5)
        f_sorted = sorted(feature_limit)
        if len(f_sorted) > 0:
            added=True
            result.append((name + '_CNT',float(len(f_sorted))))
            result.append((name + '_MIN',f_sorted[0]))
            result.append((name + '_MAX',f_sorted[-1]))
            result.append((name + '_AVG',sum(f_sorted)/len(f_sorted)))
            length = len(f_sorted)
            mid = length/2
            if length%2 != 0:
                result.append((name + '_MID',f_sorted[mid]))
            else:
                result.append((name + '_MID',(f_sorted[mid]+f_sorted[mid-1]) / 2.0))
    if not added:
        result.append((name + '_MIN', 0.5))
        result.append((name + '_MAX', 0.5))
        result.append((name + '_AVG', 0.5))
        result.append((name + '_MID', 0.5))
    return result

map_of_map={}
def get_map_from_file(filename):
    if filename not in map_of_map:
        f=open(filename)
        print "opening file " + filename
        cur_map={}
        for line in f:
            line = line.rstrip('\n')
            fields = line.split('\t')
            cur_map[fields[0]] = float(fields[1])
        f.close()
        map_of_map[filename] = cur_map
    return map_of_map.get(filename)

import re
bg_splitor=re.compile('\\(([^,()\\[\\]]*),([^,()\\[\\]]*)\\)')

@outputSchema("feature:{T:(fname:chararray,fvalue:float)}")
def get_status_run_feature_ratio(status_gp,run_gp, status_pkg, run_pkg, user_features, model_file, brand_file):
    status = gen_status_or_run_feature(status_gp,'INS_GP')
    run = gen_status_or_run_feature(run_gp,"RUN_GP")
    if status_pkg is not None:
        status_pkg_list = bg_splitor.findall(status_pkg)
    else:
        status_pkg_list = None
    status_pkg_feature = gen_pkg_feature(status_pkg_list, "V_INS_PKG")
    # run_pkg_feature = gen_pkg_feature(run_pkg, "F_RUN_PKG")

    uf = []
    if user_features is not None:
        model_map = get_map_from_file(model_file)
        brand_map = get_map_from_file(brand_file)
        model = user_features.get('model', None)
        uf.append(('U_NUM_MODEL', model_map.get(model,0.5)))
        brand = user_features.get('brand', None)
        uf.append(('U_NUM_BRAND', brand_map.get(brand,0.5)))
    else:
        uf.append(('U_NUM_MODEL', 0.5))
        uf.append(('U_NUM_BRAND', 0.5))

    feature = status + run + status_pkg_feature + uf # run_pkg_feature + uf

    return feature

@outputSchema("num_features:bag{t:tuple(ftype:chararray,fname:chararray, fvalue:double)}")
def contact_bags(*arg):
    result=[]
    if arg is None or len(arg)==0:
        return result
    for i in arg:
        if i is not None:
            result+=i
    return result

@outputSchema("t:chararray")
def parse_timestamp(unix_ts):
    if unix_ts is None:
        return None
    else:
        return datetime.datetime.fromtimestamp(unix_ts) .strftime('%Y%m%d')


@outputSchema("b:bag{t:(pkgname:chararray,label:chararray,score:float)}")
def join_prob_info(data, mode):
    res = []
    if mode == 'binary':
        for d in data:
            try:
                res.append((d[0],'1',float(d[1])))
            except:
                print >> sys.stderr, d
    elif mode == 'multi':
        for d in data:
            for pair in d[1].split(','):
                label, score = pair.split('#')
                res.append((d[0],label,float(score)))

    return res

@outputSchema("f:bag{t:tuple(pkg:chararray,value:double)}")
def merge_pkg_features(pkg_bg):
    if pkg_bg is None:
        return []
    result = []
    for i in pkg_bg:
        result += i[0]
    return result


@outputSchema("b:bag{t:(catid:chararray,weight:float)}")
def l1_normalize(data):

    if len(data) > 0:
        try:
            total = sum(t[1] for t in data)
            res = [(t[0],float(t[1])/total) for t in data]
        except:
            print >> sys.stderr, data
            res = []
    else:
        res = []

    return res;ema("b:bag{t:tuple(pkgname:chararray,label:chararray, score:float)}")



@outputSchema("uf:bag{T:tuple(fname:chararray,label:chararray,score:float)}")
def gen_brand_model_map(model_score, brand_score, mode):
    res = []
    prior="0#0.1645,1#0.2232,2#0.2285,3#0.2358,4#0.1479"

    if mode == 'binary':
        res.append(('model_score', '1', float(model_score) if model_score else 0.5))
        res.append(('brand_score', '1', float(brand_score) if brand_score else 0.5))

    elif mode == 'multi':
        if model_score == None:
            model_score = prior
        if brand_score == None:
            brand_score = prior

        for pair in model_score.split(','):
            label, score = pair.split('#')
            res.append(('model_score',str(label),float(score)))
        for pair in brand_score.split(','):
            label, score = pair.split('#')
            res.append(('brand_score',str(label),float(score)))

    return res



@outputSchema("data:(feature:{T:(fname:chararray,fvalue:double)},featmap:{T2:(fname:chararray)})")
def get_status_run_feature_ratio_indian_new(status_gp, pkg_prob,run_gp,pkgname_prob, user_features):
    status = gen_status_or_run(status_gp,'INS_GP')
    run = gen_status_or_run(run_gp,"RUN_GP")
    status_pkg_feature = gen_pkg_feature(pkg_prob, "V_INS_PKG")
    pkgname = gen_pkgname_feature(pkgname_prob ,"PKG_LIST")

    uf = gen_uf_feature(user_features)
    feature = status + run + status_pkg_feature + pkgname + uf
    fname = []
    for k in feature:
        fname.append((k[0],))
    return (feature,fname)

def gen_uf_feature(user_features):
    uf = []

    for name,label,score in user_features:
        fname = '_'.join(['U', name.upper(), label])
        uf.append((fname,float(score)))

    return uf


def gen_pkgname_feature(feature,name):
    result =[]
    a = {}
    if not feature:
        return []
    try:
        for k,v in feature:
            a[str(k)]=float(v)
            result.append((name + '_NAME' + str(k),float(v)))
    except:
         print >>sys.stderr,'pkg_list_feature',feature
    return result




def gen_status_or_run(feature,name):
    result = []
    if feature is not None:
        try:
            for f in feature:
                gp = str(f[0])
                cnt = float(f[1])
                if gp != '':
                    result.append(('F_' + name + '-CNT-' + gp,float(cnt)))
        except:
            print >>sys.stderr, 'gen_feature', feature

    return result


if __name__=='__main__':
    status_gp=[('tools',2.350425601005554,2),('entertainment',1.506040334701538,1),('productivity',4.831389427185059,1)]
    run_gp=[('tools',1.350425601005554,2),('entertainment',0.506040334701538,1),('productivity',1.831389427185059,1)]
    status_pkg=[('com.google.android.gms',0.5587490200996399),('com.android.vending',0.5546133518218994),('com.google.android.play.games',0.5805855393409729)]
    run_pkg=[('com.google.android.gms',0.1),('com.android.vending',0.2),('com.google.android.play.games',0.3)]
    status_pkg='{(com.google.android.apps.maps,0.4414122402667999),(com.instagram.android,0.606022834777832),(com.askfm,0.8829794526100159),(com.google.android.youtube,0.48302993178367615)}'
    run_pkg="{(com.google.android.apps.maps,0.4414122402667999)}"
    # print get_status_run_feature_ratio(status_gp,run_gp,status_pkg, run_pkg, {'model':'MI 4W','brand':'asus'})
    # status_pkg = None
    # run_pkg = None
    print get_status_run_feature_ratio(status_gp,run_gp,status_pkg, run_pkg,  {'model':'MI 4W','brand':'asus'},'gender_model_prob.txt','gender_brand_prob.txt')
    # import json
    # test_file=sys.argv[1]
    # f=open(test_file)
    # for line in f:
    #     fields = line.split('\t')
    #     data = fields[1]
    #     input_map=json.loads(data)
    #     input_list=[]
    #     for k in input_map:
    #         input_list.append((k['ftype'],k['fname'],k['fvalue']))

    #     print get_user_feature_ratio(input_list,'game_strategy')
    # f.close()
    pass

