
if __name__ != '__lib__':
    def outputSchema(dont_care):
        def wrapper(func):
            def inner(*args, **kwargs):
                return func(*args, **kwargs)
            return inner
        return wrapper

import random
@outputSchema('age:int')
def random_age(original_age):
	if original_age is None:
		return None
	if original_age == 1:
		return 1
	elif original_age == 2:
		rand_int = random.randint(0,99)
		if rand_int < 50:
			return 2
		elif rand_int < 77:
			return 3
		else:
			return 4
	else:
		return original_age

if __name__=='__main__':
	for i in range(1000):
		print random_age(2)
