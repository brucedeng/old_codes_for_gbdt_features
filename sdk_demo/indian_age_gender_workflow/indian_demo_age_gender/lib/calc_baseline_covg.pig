register 'parse_time.py' using jython as myfuncs;
a = load 's3://com.cmcm.bigdata.dh/targeting_up/warehouse/demo_predict_data/scores/20150802_filter_brand' AS (aid:chararray,uptime:chararray, score:float);

b = load 'facemb.user' USING org.apache.hcatalog.pig.HCatLoader();
b = filter b by dt=='20150802';
b = FOREACH b GENERATE aid,uptime,gender;
DESCRIBE b;
-- b = FOREACH b generate aid, myfuncs.parse_timestamp(uptime*1000) as uptime, gender;
-- b = filter b by uptime=='20150802';

jnd = join a by aid, b by aid;
--describe jnd;
--result = FOREACH (join a by aid, b by aid) GENERATE a::aid as aid,a::uptime as uptime, b::gender as gender;
--describe result;
rmf /tmp/mengchong/gender_baseline_covg
store jnd into '/tmp/mengchong/gender_baseline_covg2';
