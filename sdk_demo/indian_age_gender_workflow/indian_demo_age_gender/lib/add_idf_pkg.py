#!/usr/bin/env python

import sys

if __name__=='__main__':
    pkg_field=sys.argv[1]
    idf_file=sys.argv[2]
    pkg_file = sys.argv[3]
    pkg_file2 = sys.argv[4]
    pkg_field = int(pkg_field)-1

    idf_input = open(idf_file)
    idf_data = {}
    max_idf = 0
    for line in idf_input:
        line=line.replace('\n','')
        fields = line.split('\t')
        pkgname=fields[0]
        idf = fields[2]
        idf = float(idf)
        if idf > max_idf:
            max_idf = idf
        idf_data[pkgname] = idf
    idf_input.close()

    pkg_input = open(pkg_file)
    pkg_map = {}
    for line in pkg_input:
        line = line.replace('\n','')
        fields = line.split('\t')
        pkgname=fields[0]
        pkg_map[pkgname]=fields[1]
    pkg_input.close()

    pkg_input = open(pkg_file2)
    pkg_map2 = {}
    for line in pkg_input:
        line = line.replace('\n','')
        fields = line.split('\t')
        pkgname=fields[0]
        pkg_map2[pkgname]=fields[1]
    pkg_input.close()

    for line in sys.stdin:
        line=line.replace('\n','')
        fields = line.split('\t')
        pkgname=fields[pkg_field]
        if pkgname in idf_data:
            current_idf = idf_data[pkgname]
        else:
            current_idf = max_idf

        if pkgname in pkg_map:
            current_pkg_tag = pkg_map[pkgname]
        else:
            current_pkg_tag = ''

        if pkgname in pkg_map2:
            current_pkg_tag2 = pkg_map2[pkgname]
        else:
            current_pkg_tag2 = ''

        print line + '\t' + str(current_idf) + '\t' + current_pkg_tag + '\t' + current_pkg_tag2

