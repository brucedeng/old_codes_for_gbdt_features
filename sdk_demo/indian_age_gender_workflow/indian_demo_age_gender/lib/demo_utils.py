import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import com.xhaus.jyson.JysonCodec as json
import math
import re

@outputSchema("b:bag{t:tuple(fname:chararray, fvalue:double)}")
def wtf(feature):
    res = [(str(t[0]),t[1]) for t in feature]

    return res;

@outputSchema("t:tuple(uid:chararray, nickname:chararray, birthday:chararray, gender:chararray)")
def get_fields(info):
    try:
        info_map = json.loads(info)

        uid = info_map.get('uid',None)
        nickname = info_map.get('nickname',None)
        birthday = info_map.get('birthday',None)
        gender = info_map.get('gender',None)

        return (uid,nickname,birthday,gender)

    except:
        print >> sys.stderr, info
        return (None,None,None,None)



@outputSchema("m:map[chararray]")
def get_map(data):

    result = {}

    f = data.replace('\n','').split('\t');

    for t in f:
        try:
            k,v = t.split('|')
            result[k]=str(v)

        except:
            pass

    return result

@outputSchema("mi:float")
def mutual_entropy(male_count, female_count, male_all, female_all):

    p_x = {}
    p_y = {}
    p_xy = {"m":{},"f":{}}

    p_x["m"]=float(male_all)/(male_all + female_all)
    p_x["f"]=float(female_all)/(male_all + female_all)

    p_y["1"]=float(male_count + female_count)/float(male_all + female_all)
    p_y["0"]=1-p_y["1"]

    p_xy["m"]["1"] = float(male_count)/(male_all + female_all)
    p_xy["m"]["0"] = float(male_all - male_count)/(male_all + female_all)
    p_xy["f"]["1"] = float(female_count)/(male_all + female_all)
    p_xy["f"]["0"] = float(female_all - female_count)/(male_all + female_all)

    mi = 0

    for x in ["m","f"]:
        for y in ["1","0"]:
            if p_xy[x][y] > 0:
                try:
                    mi += p_xy[x][y]*math.log(p_xy[x][y] / (p_x[x] * p_y[y]) , 2)
                except:
                    print >> sys.stderr, str([male_count, female_count, male_all, female_all])

    return mi


@outputSchema("bin_num:int")
def get_bin(run_num):

    segments = [0,5,10,20,100,500]

    idx = 0
    for thres in segments:
        if run_num <= thres:
            break
        idx += 1

    return idx


@outputSchema("mi:float")
def multi_mutual_entropy(male_dist, female_dist, male_all, female_all):

    p_x = {}
    p_y = {}
    count_y = {}
    p_xy = {"m":{},"f":{}}
    count_xy = {"m":{},"f":{}}

    user_all = male_all + female_all
    p_x["m"]=float(male_all)/(male_all + female_all)
    p_x["f"]=float(female_all)/(male_all + female_all)

    for item in male_dist:
        run_num = str(item[0])
        count = float(item[1])
        count_y[run_num] = count_y.get(run_num,0) + count
        count_xy["m"][run_num] = count_xy["m"].get(run_num,0) + count

    for item in female_dist:
        run_num = str(item[0])
        count = float(item[1])
        count_y[run_num] = count_y.get(run_num,0) + count
        count_xy["f"][run_num] = count_xy["f"].get(run_num,0) + count

    count_y["0"] = male_all + female_all - sum(count_y[k] for k in count_y if k!="0")
    count_xy["m"]["0"] = male_all - sum(count_xy["m"][k] for k in count_xy["m"] if k!="0")
    count_xy["f"]["0"] = female_all - sum(count_xy["f"][k] for k in count_xy["f"] if k!="0")

    mi = 0

    for x in ["m","f"]:
        for y in count_xy[x]:
            try:
                if count_xy[x][y] > 0:
                    mi += (count_xy[x][y]/user_all)*math.log(count_xy[x][y] / (p_x[x] * count_y[y]) , 2)

                    print >> sys.stderr, str([x, y, count_xy[x][y]/user_all, count_xy[x][y] / (p_x[x] * count_y[y])])
            except:
                continue
                #print >> sys.stderr, str([male_dist, female_dist, male_all, female_all])

    return mi


@outputSchema("result:double")
def normalize_feature(fname, fvalue, avg, std):

    if re.match('^(F_.*|P_RUN.*)', fname) or re.match('^U_NUM_(MODEL|BRAND)', fname):
        return (fvalue-avg)/std
    else:
        return fvalue

@outputSchema("result:double")
def cal_std_value(value, cnt, sum_value):
    result=0.0;
    avg = sum_value/cnt
    for item in value:
        fvalue,fcnt = item
        result += (fvalue-avg)*fcnt

    result = result / cnt
    return result


def gen_pkgname_feature(feature,name):
    result =[]
    a = {}
    if not feature:
        return []
    try:
    for k,v in feature:
            a[str(k)]=int(v)
            result.append((name + '_NAME' + str(k),int(v)))
    except:
        print >>sys.stderr,'pkg_list_feature',feature
    return result


