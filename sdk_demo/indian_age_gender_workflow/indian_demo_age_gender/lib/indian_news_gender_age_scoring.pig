register './demo_predict_features.py' using jython as myfuncs;

define EVAL_GENDER com.ijinshan.cmcm.gbdt.runtime.udf.GbdtRuntimeMap('$gender_dump_file');
define EVAL_AGE com.ijinshan.cmcm.gbdt.runtime.udf.GbdtRuntimeMap('$age_dump_file');
set fs.s3.canned.acl BucketOwnerFullControl;

status_feature = LOAD '$install_status_feature' as (aid:chararray, idf_feature:bag{T:(gpname:chararray,sum_idf:float,cnt:float)}, gender_pkg_data:chararray, age_pkg_data:chararray); -- USING org.apache.pig.piggybank.storage.PigStorageSchema();

run_feature = LOAD '$run_feature' as (aid:chararray, idf_feature:bag{T:(gpname:chararray,sum_idf:float,cnt:float)}, gender_pkg_data:chararray, age_pkg_data:chararray);
--  USING org.apache.pig.piggybank.storage.PigStorageSchema();

-- active_data = LOAD '$user_feature' using org.apache.hcatalog.pig.HCatLoader();
-- active_data = FILTER active_data by dt=='$end_date' and LOWER(country)=='in';
-- active_model_brand = FOREACH active_data GENERATE aid, myfuncs.parse_timestamp(uptime) as uptime, uptime as raw_uptime, brand, model;
-- active_filtered = FILTER active_model_brand by uptime >= '$start_date';
-- -- active_filtered = FILTER active_model_brand by uptime != '-1';

-- active_dist = FOREACH ( GROUP active_filtered by aid ) {
--     data_sorted = order $1 by raw_uptime;
--     data_limit = LIMIT data_sorted 1;
--     GENERATE FLATTEN(data_limit) as (aid,uptime,raw_uptime,brand,model);
-- }

uf_input = load '/tmp/mengchong/in_news_jnd2' as (aid:chararray, country:chararray, brand:chararray, model:chararray);
uf = FOREACH uf_input GENERATE
    aid,
    TOMAP('model',model,'brand',brand) as user_features;

jnd = join uf by aid left outer, status_feature by aid PARALLEL 400;

f = foreach jnd generate
        (uf::aid is not null? uf::aid : status_feature::aid) as aid,
        uf::user_features as user_features,
        status_feature::idf_feature as status_gp,
        status_feature::gender_pkg_data as gender_pkg_data,
        status_feature::age_pkg_data as age_pkg_data;

all_feature_jnd = JOIN f by aid left outer, run_feature by aid;
all_feature = FOREACH all_feature_jnd GENERATE
        (f::aid is not null? f::aid : run_feature::aid) as aid,
        f::status_gp as status_gp,
        f::gender_pkg_data as install_gender_pkg_data,
        f::age_pkg_data as install_age_pkg_data,
        run_feature::idf_feature as run_gp,
        run_feature::gender_pkg_data as run_gender_pkg_data,
        run_feature::age_pkg_data as run_age_pkg_data,
        f::user_features as user_features;

store all_feature into '/tmp/mengchong/in_news_all_features';
all_feature = FILTER all_feature by status_gp is not null or run_gp is not null or install_gender_pkg_data is not null or install_age_pkg_data is not null;

-- store all_feature into '/tmp/mengchong/eval_all_feature';
-- predict

-- raw_training_data = foreach all_feature {
--     generate
--         aid,
--         myfuncs.get_status_run_feature_ratio(status_gp,run_gp, install_gender_pkg_data, run_gender_pkg_data, user_features, '$gender_model_prob', '$gender_brand_prob') as gender_feature,
--         myfuncs.get_status_run_feature_ratio(status_gp,run_gp, install_age_pkg_data, run_age_pkg_data, user_features, '$age_model_prob', '$age_brand_prob') as age_feature;
-- }

-- pred_features = FOREACH raw_training_data GENERATE aid, 
--         myfuncs.to_map(gender_feature) as gender_feature,
--         myfuncs.to_map(age_feature) as age_feature;

-- pred_result = FOREACH pred_features GENERATE aid, '$end_date' as uptime,
--     EVAL_GENDER(gender_feature) as gender_score,
--     EVAL_AGE(age_feature) as age_score;

-- store pred_result into '/tmp/mengchong/in_news_gender_score' using org.apache.pig.piggybank.storage.PigStorageSchema();

