
while getopts "s:e:l:q:m:" arg
do
    case $arg in
        s) #start date of events
        event_start=$OPTARG
        ;;
        e) #end date  of event
        event_end=$OPTARG
        ;;
        l) #run data
        app_lan=$OPTARG
        ;;
        m) #run data
        model_file=$OPTARG
        ;;
        q) # queue name
        queue_name=$OPTARG
        ;;
        ?)  #?
        echo "unkonw argument"
        exit 1
        ;;
    esac
done


queue_name=experiment
PIG_PARAM="  -Dmapred.job.queue.name=$queue_name ";
TOP_PARAM="-p U_NCAT=5 -p U_NKEY=25 -p D_NCAT=5 -p D_NKEY=15 -p WND=600"
 dedup=" -p DEDUP=dedup_content "
 time="-p time=${event_start}"
cmd="pig $PIG_PARAM  $TOP_PARAM $dedup   $model_file"
echo $cmd
eval $cmd;
