
--set output.compression.enabled true;
--set output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
set fs.s3.canned.acl BucketOwnerFullControl;

--register '../lib/random_age.py' using jython as age_udf;
--%default input_score 's3://com.cmcm.bigdata.dh/targeting_up/warehouse/demo_predict_data/indian_age_gender/score';
--%default output_summary 's3://com.cmcm.bigdata.dh/targeting_up/warehouse/demo_predict_data/indian_age_gender/score_end/20160228';
%default input_score '/user/dulimei/tmp/indian_age_gender/score/20160513';
%default pre_input '/user/dulimei/tmp/indian_age_gender/score_end/20160512';

rmf /user/dulimei/tmp/test;

input_age_score = load '$input_score/age' as (aid:chararray, uptime:chararray,  age_score:float);



input_gender_score = load '$input_score/gender' as (aid:chararray, uptime:chararray, gender_score:float);
hist_summary = load '$pre_input' as (aid:chararray, uptime:chararray, gender:int, age:int);

age_end = foreach (JOIN  input_age_score by aid, hist_summary  by aid) generate 
    input_age_score::aid as aid,
    hist_summary::age as age,
    input_age_score::age_score as score;

age_end = filter age_end by age is not null and score is not null ;
store age_end into '/user/dulimei/tmp/test/age';


gender_end = foreach (JOIN  input_gender_score by aid, hist_summary  by aid) generate 
    input_gender_score::aid as aid,
    hist_summary::gender as gender,
    input_gender_score::gender_score as score;
gender_end = filter gender_end by gender is not null and score is not null ;

store gender_end into '/user/dulimei/tmp/test/gender';


