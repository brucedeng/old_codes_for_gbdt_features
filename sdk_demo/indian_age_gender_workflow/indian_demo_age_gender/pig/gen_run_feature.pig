--register '../lib/demo_predict_features.py' using jython as myfuncs;
--%default run_idf 'run_idf.txt';
--%default gender_pkg_data 'gender_pkg_prob.txt';
--%default age_pkg_data 'age_pkg_prob.txt';
--%default end_date '20160328';
--%default run_table 'facemb.sum_app_run_14d';
--%default output 's3://com.cmcm.bigdata.dh/targeting_up/warehouse/demo_predict_data/indian_age_gender/run_14d/';

--define add_status_idf `python add_idf_pkg.py 2 $run_idf $gender_pkg_data $age_pkg_data` ship('../lib/add_idf_pkg.py', '../lib/$run_idf', '../lib/$gender_pkg_data', '../lib/$age_pkg_data');

set output.compression.enabled true;
set output.compression.codec org.apache.hadoop.io.compress.GzipCodec;


set pig.temp.dir  /tmp/dulimei/demo_tmp;
set fs.s3.canned.acl BucketOwnerFullControl;
register './demo_predict_features.py' using jython as myfuncs;
define add_status_idf `python add_idf_pkg.py 2 $run_idf $gender_pkg_data $age_pkg_data`;


raw_features = LOAD '$run_table' USING  AvroStorage();;
raw_features_gender = FOREACH raw_features GENERATE aid,pkgname,( (gp is null or gp == '') ? '':gp) as gpcategory;

--age

status_jnd = STREAM raw_features_gender THROUGH add_status_idf as (aid:chararray, pkgname:chararray, gpcategory:chararray, idf:float,gender_pkg_tag:float, age_pkg_tag:float);
pkg_level_data = FOREACH status_jnd GENERATE aid,pkgname,gpcategory,1 as num, idf,  age_pkg_tag;

data_gp_grp = GROUP pkg_level_data by (aid, gpcategory);
data_gp = FOREACH data_gp_grp {
    age_pkg_filter = FILTER $1 by age_pkg_tag is not null;
    pkg_data2 = FOREACH age_pkg_filter GENERATE pkgname, age_pkg_tag;
    GENERATE FLATTEN($0) as (aid,gpcategory), SUM($1.idf) as tfidf, COUNT($1) as cnt, pkg_data2 as age_pkg_data;
}

aid_data = FOREACH ( GROUP data_gp by aid ) {
    data = FOREACH $1 GENERATE gpcategory,tfidf,cnt;
    GENERATE $0 as aid, data as idf_feature,
        myfuncs.merge_pkg_features($1.age_pkg_data) as age_pkg_data;
}

STORE aid_data into '$output/age'; -- using org.apache.pig.piggybank.storage.PigStorageSchema();


--gender
raw_features = foreach raw_features_gender generate aid, pkgname, gpcategory, 1 as num;
raw_features = filter raw_features by aid is not null and aid != '' and pkgname is not null and pkgname!='' and gpcategory !='' ;
raw_features = distinct raw_features ;

raw_features_ab = foreach raw_features generate aid,pkgname,gpcategory,num;
raw_features_ab = filter raw_features_ab by gpcategory is not null and gpcategory!='';
raw_features_ab = distinct raw_features_ab;

data_gp_grp = GROUP raw_features_ab by (aid, gpcategory) ;
data_gp = FOREACH data_gp_grp {
    GENERATE FLATTEN($0) as (aid,gpcategory), SUM($1.num) as cnt;
}

aid_data = FOREACH ( GROUP data_gp by aid ){
    data = FOREACH $1 GENERATE gpcategory,cnt;
    GENERATE $0 as aid, myfuncs.l1_normalize(data) as gp_cnt_feature;
}

rmf $output/gender;
STORE aid_data into '$output/gender' ;


