--register '../lib/demo_predict_features.py' using jython as myfuncs;
--%default age_dump_file 'hdfs:/user/dulimei/data/age_model.dump#age_model.dump';
--%default install_status_feature 's3://com.cmcm.bigdata.dh/targeting_up/warehouse/demo_predict_data/indian_age_gender/status_install/';
--%default run_feature 's3://com.cmcm.bigdata.dh/targeting_up/warehouse/demo_predict_data/indian_age_gender/run_14d';
--%default user_feature 'facemb.active';
--%default end_date '20160328';
--%default start_date '20160327';
--%default age_model_prob 'hdfs:/user/dulimei/data/age_model_prob.txt#age_model_prob.txt';
--%default age_brand_prob 'hdfs:/user/dulimei/data/age_brand_prob.txt#age_brand_prob.txt';
--set mapred.cache.files $age_model_prob,$age_brand_prob,$age_dump_file;
--%default output 's3://com.cmcm.bigdata.dh/targeting_up/warehouse/demo_predict_data/indian_age_gender/join';
--register '../lib/gbdt_runtime-0.5.0.jar';
--%default score 's3://com.cmcm.bigdata.dh/targeting_up/warehouse/demo_predict_data/indian_age_gender/score';
--define EVAL_AGE com.ijinshan.cmcm.gbdt.runtime.udf.GbdtRuntimeMap('age_model.dump');

register './demo_predict_features.py' using jython as myfuncs;
--register './gbdt_runtime-0.5.0.jar';
define EVAL_AGE com.ijinshan.cmcm.gbdt.runtime.udf.GbdtRuntimeMap('$age_dump_file');
set pig.temp.dir  /tmp/dulimei/demo_tmp;
set fs.s3.canned.acl BucketOwnerFullControl;
set output.compression.enabled true;
set output.compression.codec org.apache.hadoop.io.compress.GzipCodec;

status_feature = LOAD '$install_status_feature/age' as (aid:chararray, idf_feature:bag{T:(gpname:chararray,sum_idf:float,cnt:float)},  age_pkg_data:chararray); -- USING org.apache.pig.piggybank.storage.PigStorageSchema();

run_feature = LOAD '$run_feature/age' as (aid:chararray, idf_feature:bag{T:(gpname:chararray,sum_idf:float,cnt:float)},  age_pkg_data:chararray);
--  USING org.apache.pig.piggybank.storage.PigStorageSchema();

active_data = LOAD '$user_feature' using AvroStorage();
active_data = FILTER active_data by   LOWER(country)=='in';
active_model_brand = FOREACH active_data GENERATE aid, myfuncs.parse_timestamp(uptime) as uptime, uptime as raw_uptime, brand, model;
active_filtered = FILTER active_model_brand by uptime >= '$start_date';
-- active_filtered = FILTER active_model_brand by uptime != '-1';

active_dist = FOREACH ( GROUP active_filtered by aid ) {
    data_sorted = order $1 by raw_uptime;
    data_limit = LIMIT data_sorted 1;
    GENERATE FLATTEN(data_limit) as (aid,uptime,raw_uptime,brand,model);
}

uf = FOREACH active_dist GENERATE
    aid,
    TOMAP('model',model,'brand',brand) as user_features;

jnd = join uf by aid left outer, status_feature by aid PARALLEL 400;

f = foreach jnd generate
        (uf::aid is not null? uf::aid : status_feature::aid) as aid,
        uf::user_features as user_features,
        status_feature::idf_feature as status_gp,
        status_feature::age_pkg_data as age_pkg_data;

all_feature_jnd = JOIN f by aid left outer, run_feature by aid;
all_feature = FOREACH all_feature_jnd GENERATE
        (f::aid is not null? f::aid : run_feature::aid) as aid,
        f::status_gp as status_gp,
        f::age_pkg_data as install_age_pkg_data,
        run_feature::idf_feature as run_gp,
        run_feature::age_pkg_data as run_age_pkg_data,
        f::user_features as user_features;

all_feature = FILTER all_feature by status_gp is not null or run_gp is not null or  install_age_pkg_data is not null;


raw_training_data = foreach all_feature {
    generate
        aid,
        myfuncs.get_status_run_feature_ratio(status_gp,run_gp, install_age_pkg_data, run_age_pkg_data, user_features, 'age_model_prob.txt', 'age_brand_prob.txt') as age_feature;
}


--rmf $output/age_feature;
store raw_training_data into '$output/age_feature';

pred_features = FOREACH raw_training_data GENERATE aid, 
        myfuncs.to_map(age_feature) as age_feature;

pred_result = FOREACH pred_features GENERATE aid, '$end_date' as uptime,
    EVAL_AGE(age_feature) as age_score;


rmf $score/age;
store pred_result into '$score/age' ;

