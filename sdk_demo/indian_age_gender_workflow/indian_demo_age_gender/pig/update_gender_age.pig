
set output.compression.enabled true;
set output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
set fs.s3.canned.acl BucketOwnerFullControl;

--register '../lib/random_age.py' using jython as age_udf;
--%default input_score 's3://com.cmcm.bigdata.dh/targeting_up/warehouse/demo_predict_data/indian_age_gender/score';
--%default output_summary 's3://com.cmcm.bigdata.dh/targeting_up/warehouse/demo_predict_data/indian_age_gender/score_end/20160228';

set pig.temp.dir  /tmp/dulimei/demo_tmp;
input_age_score = load '$input_score/age' as (aid:chararray, uptime:chararray,  age_score:float);
input_gender_score = load '$input_score/gender' as (aid:chararray, uptime:chararray, gender_score:float);
gender_labeled_data = FOREACH input_gender_score   GENERATE aid, (chararray)uptime as uptime,
    (int)(gender_score<=0.5805 ? 2: (gender_score >=0.6109 ? 1:null)) as gender:int;
gender_labeled_data = filter gender_labeled_data by gender is not null;

age_labeled_data = FOREACH  input_age_score  GENERATE aid, (chararray)uptime as uptime,
    (int)(age_score<=0.2064? 2: (age_score >=0.4532 ? 1:null)) as age:int;
age_labeled_data = filter age_labeled_data by age is not null;


labeled_data = foreach (join gender_labeled_data by aid FULL, age_labeled_data by aid PARALLEL 100)
    generate gender_labeled_data::aid as aid, gender_labeled_data::uptime as uptime, age_labeled_data::aid as ageaid,age_labeled_data::uptime as ageuptime, gender_labeled_data::gender as gender, age_labeled_data::age as age;

labeled_data = foreach labeled_data generate ((aid is null or aid=='')?ageaid:aid) as aid,((uptime is null or uptime=='')?ageuptime:uptime) as uptime,gender,age;


labeled_data = filter labeled_data by gender is not null or age is not null;
labeled_data = foreach labeled_data generate  aid,uptime,gender,age;
--store labeled_data  into '$output_summary';




hist_summary = load '$pre_input' as (aid:chararray, uptime:chararray, gender:int, age:int);

data_union = UNION labeled_data, hist_summary;
data_uniq = FOREACH ( GROUP data_union by aid PARALLEL 100) {
    data_sorted = order data_union by uptime desc;
    data_limit = LIMIT data_sorted 1;
    GENERATE FLATTEN(data_limit) as (aid, uptime, gender, age);
}

data_uniq = filter data_uniq by aid is not null and aid != '';

store data_uniq into '$output_summary'; 

