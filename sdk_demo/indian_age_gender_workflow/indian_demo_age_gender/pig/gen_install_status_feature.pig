--register  '../lib/demo_predict_features.py' using jython as myfuncs;
--%default status_idf 'install_status_idf.txt';
--%default gender_pkg_data 'gender_pkg_prob.txt';
--%default age_pkg_data 'age_pkg_prob.txt';
--define add_status_idf `python add_idf_pkg.py 2 $status_idf $gender_pkg_data $age_pkg_data`  ship('../lib/add_idf_pkg.py', '../lib/$status_idf', '../lib/$gender_pkg_data', '../lib/$age_pkg_data');
set fs.s3.canned.acl BucketOwnerFullControl;
set output.compression.enabled true;
set output.compression.codec org.apache.hadoop.io.compress.GzipCodec;
--%default inputgender '/user/dulimei/model/indian/age_gender/gender/data/gender_pkg_indian.txt';
--%default pkg_prob_gender '/user/dulimei/model/indian/age_gender/gender/install/package_level_prob';
--
--%default install_status  'facemb.user_install_status';
--%default end_date '20160328';
--%default output 's3://com.cmcm.bigdata.dh/targeting_up/warehouse/demo_predict_data/indian_age_gender/status_install';
set  pig.temp.dir  /tmp/dulimei/demo_tmp;
register  './demo_predict_features.py' using jython as myfuncs;
define add_status_idf `python add_idf_pkg.py 2 $status_idf $gender_pkg_data $age_pkg_data` ;
set default_parallel 300;


raw_features = LOAD '$install_status' USING  AvroStorage(); 
raw_features = FOREACH raw_features GENERATE aid,pkgname,( (gp is null or gp == '') ? '':gp) as gpcategory;

--age

status_jnd = STREAM raw_features THROUGH add_status_idf as (aid:chararray, pkgname:chararray, gpcategory:chararray, idf:float,gender_pkg_tag:float, age_pkg_tag:float);
pkg_level_data = FOREACH status_jnd GENERATE aid,pkgname,gpcategory,1 as num, idf, age_pkg_tag;

data_gp_grp = GROUP pkg_level_data by (aid, gpcategory)  ;
data_gp = FOREACH data_gp_grp {
    age_pkg_filter = FILTER $1 by age_pkg_tag is not null;
    pkg_data2 = FOREACH age_pkg_filter GENERATE pkgname, age_pkg_tag;
    GENERATE FLATTEN($0) as (aid,gpcategory), SUM($1.idf) as tfidf, COUNT($1) as cnt, pkg_data2 as age_pkg_data;
}

aid_data = FOREACH ( GROUP data_gp by aid ) {
    data = FOREACH $1 GENERATE gpcategory,tfidf,cnt;
    GENERATE $0 as aid, data as idf_feature,
        myfuncs.merge_pkg_features($1.age_pkg_data) as age_pkg_data;
}


STORE aid_data into '$output/age';-- using org.apache.pig.piggybank.storage.PigStorageSchema();

--gender
raw_features_install = foreach raw_features generate aid,pkgname,gpcategory;
raw_features_install = filter raw_features_install by aid is not null and aid != '' and pkgname is not null and pkgname!='';
raw_features_install = DISTINCT raw_features_install ;
raw_features_end = group raw_features_install by aid;

label_data = LOAD '$inputgender' as (pkgno:chararray,pkgname:chararray);
label_data = group label_data all;
user_pkg_features_gender = foreach raw_features_end  generate group as aid,myfuncs.pkgname_map_new(label_data.$1,$1.pkgname) as type;

pkg_level_data = filter raw_features_install  by gpcategory is not null and gpcategory!='';
pkg_level_data = distinct pkg_level_data;
pkg_prob_gender = LOAD '$pkg_prob_gender' as (pkgname:chararray,prob:chararray,raw:chararray);

pkg_level_data_gender = foreach (join pkg_level_data by pkgname, pkg_prob_gender by pkgname using 'replicated' PARALLEL 200)
    generate pkg_level_data::aid as aid, pkg_level_data::pkgname as pkgname, pkg_level_data::gpcategory as gpcategory,
            pkg_prob_gender::prob as prob_info;


data_gp_grp_gender = GROUP pkg_level_data_gender by (aid, gpcategory) ;
data_gp_gender = FOREACH data_gp_grp_gender {
    data = foreach pkg_level_data_gender generate pkgname, prob_info;
    GENERATE FLATTEN(group) as (aid,gpcategory), COUNT(pkg_level_data_gender.pkgname) as cnt, pkg_level_data_gender, myfuncs.join_prob_info(data, 'binary') as prob_info;
}

aid_data_gender = FOREACH ( GROUP data_gp_gender by aid ){
    data = FOREACH data_gp_gender GENERATE gpcategory,cnt;
    GENERATE $0 as aid, myfuncs.l1_normalize(data) as gp_cnt_feature,
        myfuncs.merge_pkg_features($1.prob_info) as pkg_prob_data;
}

aid_data_end_gender = foreach (join aid_data_gender by aid,user_pkg_features_gender by aid PARALLEL 200)generate
    aid_data_gender::aid as aid,
    aid_data_gender::gp_cnt_feature as gp_cnt_feature,
    aid_data_gender::pkg_prob_data as pkg_prob_data,
    user_pkg_features_gender::type as type;

rmf $output/gender;
STORE aid_data_end_gender into '$output/gender';
