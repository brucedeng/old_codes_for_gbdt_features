
import sys
import numpy as np
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn import preprocessing

bin_num = 100
target = 'male'
#target = 'female'

if __name__ == '__main__':


    if len(sys.argv) < 2:
        print "usage:eval_pr.py sorted_label_file"

    label_file = open(sys.argv[1])

    try:
        neg_weight = float(sys.argv[2])
    except:
        neg_weight = 1

    labels = []
    scores = []
    for lines in label_file:
        fields = lines.rstrip('\n').split('\t')
        label = int(fields[1])
        if target == 'male':
            score = float(fields[2])
        else:
            score = 1-float(fields[2])

        labels.append(label)
        scores.append(score)

    accu_cnt = {}
    accu=0
    for i in sorted(scores):
        accu += 1
        accu_cnt[i] = accu

    weight = []
    for i in range(len(labels)):
        if labels[i] != 1:
            weight.append(neg_weight)
        else:
            weight.append(1.0)

    labels = np.array(labels)
    scores = np.array(scores)
    weight=np.array(weight)

    print labels
    print weight


    precision, recall, threshold = precision_recall_curve(labels, scores, pos_label=(1 if target == 'male' else 0)) #,sample_weight=neg_weight)

    step = len(precision)/bin_num
    acc = 0
    cnt=len(labels)
    print len(precision)

    while acc < len(precision):
        P=precision[acc]
        R=recall[acc]
        T = threshold[acc]
        accu_user = accu_cnt[T]
        f1=2*P*R/(P+R)
        print "%d\t%.4f\t%.4f\t%.4f\t%.4f" % (cnt-accu_user, precision[acc],recall[acc],threshold[acc],f1)
        acc += step

    P=precision[-1]
    R=recall[-1]
    f1=2*P*R/(P+R)
    print "%d\t%.4f\t%.4f\t%.4f" % (0,precision[-1],recall[-1],threshold[-1])



