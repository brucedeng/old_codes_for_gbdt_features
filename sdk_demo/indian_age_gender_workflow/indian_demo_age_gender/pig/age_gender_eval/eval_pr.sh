#!/bin/bash

#xgboost=~/xgboost_quick

#folder=$1
folder=/home/dulimei/news_model/home/indian_age_gender/indian_age_gender_workflow/cn_demo_age_gender/pig/age_gender_eval
#model=0080.model
raw_testing_data=gender.txt
pushd $folder

# $xgboost gender.conf task=dump model_in=models/0110.model name_dump=0110.dump fmap=featmap.txt dump_stats=1
#$xgboost gender.conf task=pred model_in=result/$model

#cat $raw_testing_data | cut -f1,2 | paste - pred.txt | awk -F$'\t' '{print $1"\t"(1-$2)"\t"(1-$3)}' > female_sample_test.label
#cat $raw_testing_data | cut -f1,2 | paste - pred.txt > male_sample_test.label

popd

echo =============male==============
python  eval_pr.py $folder/gender.txt
echo ============female=============
python  eval_pr.py $folder/gender_change.txt


