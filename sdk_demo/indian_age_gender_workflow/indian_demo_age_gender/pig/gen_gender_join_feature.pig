--register '../lib/jyson-1.0.2.jar';
--register '../lib/demo_predict_features.py' using jython as myfuncs;
--register '../lib/gbdt_runtime-0.5.0.jar';
--%default gender_dump_file 'hdfs:/user/dulimei/data/gender_model.dump#gender_model.dump';
--define EVAL_GENDER com.ijinshan.cmcm.gbdt.runtime.udf.GbdtRuntimeMap('gender_model.dump');
--%default gender_prob '/user/dulimei/model/indian/age_gender/gender/model_brand';
--%default install_status_feature 's3://com.cmcm.bigdata.dh/targeting_up/warehouse/demo_predict_data/indian_age_gender/status_install';
--%default end_date '20160328';
--%default start_date '20160327';
--%default user_feature 'facemb.active';
--%default  run_feature 's3://com.cmcm.bigdata.dh/targeting_up/warehouse/demo_predict_data/indian_age_gender/run_14d';
--%default output 's3://com.cmcm.bigdata.dh/targeting_up/warehouse/demo_predict_data/indian_age_gender/join';
--%default score 's3://com.cmcm.bigdata.dh/targeting_up/warehouse/demo_predict_data/indian_age_gender/score';
--%default date '20160228';
--set mapred.cache.files $gender_dump_file;
set output.compression.enabled true;
set output.compression.codec org.apache.hadoop.io.compress.GzipCodec;

set pig.temp.dir  /tmp/dulimei/demo_tmp;
register './demo_predict_features.py' using jython as myfuncs;
register '*.jar';
--register './gbdt_runtime-0.5.0.jar';
define EVAL_GENDER com.ijinshan.cmcm.gbdt.runtime.udf.GbdtRuntimeMap('$gender_dump_file');

gender_status_feature = LOAD '$install_status_feature/gender' as (aid:chararray, category_feature:bag{T:(gpname:chararray,weight:float)}, pkg_prob_data:bag{(pkgname:chararray,label:chararray,score:float)},label:bag{T:(pkgname:chararray,type:int)}); 
run_feature = LOAD '$run_feature/gender' as (aid:chararray, category_feature:bag{T:(gpname:chararray,weight:float)});


active_data = LOAD '$user_feature' using AvroStorage();
active_data = FILTER active_data by  LOWER(country)=='in';
active_model_brand = FOREACH active_data GENERATE aid, myfuncs.parse_timestamp(uptime) as uptime, uptime as raw_uptime, brand, model;
active_filtered = FILTER active_model_brand by uptime >= '$start_date';
iactive_filtered = foreach active_filtered generate aid,model,brand,uptime as dt;
uf= distinct active_filtered;


/*generate model score for binary classification prob*/


model_prob_binary = LOAD '$gender_model_brand/model_level_prob' as (model:chararray, score:chararray, weight:chararray);
brand_prob_binary = LOAD '$gender_model_brand/brand_level_prob' as (brand:chararray, score:chararray, weight:chararray);


uf_model = foreach (join uf by model LEFT, model_prob_binary by model using 'replicated' PARALLEL 200) 
    generate uf::aid as aid,  uf::brand as brand, uf::model as model, model_prob_binary::score as model_score; 
uf_model_brand = foreach (join uf_model by brand LEFT, brand_prob_binary by brand using 'replicated' PARALLEL 200) 
    generate uf_model::aid as aid,  uf_model::brand as brand, uf_model::model as model, uf_model::model_score as model_score,
            brand_prob_binary::score as brand_score; 

uf_score = foreach uf_model_brand generate
        aid,  myfuncs.gen_brand_model_map(model_score, brand_score, 'binary') as user_features;


jnd = join uf_score by aid left outer, gender_status_feature by aid PARALLEL 200;
f = foreach jnd generate
        uf_score::aid as aid,
        uf_score::user_features as user_features,
        gender_status_feature::category_feature as status_gp,
        gender_status_feature::pkg_prob_data as pkg_prob,
    gender_status_feature::label as label;


all_feature_jnd = JOIN f by aid left outer, run_feature by aid PARALLEL 200;

all_feature_a = FOREACH all_feature_jnd GENERATE
        f::aid as aid,
        f::status_gp as status_gp,
        f::pkg_prob as pkg_prob,
        f::label as pkgname_prob,
        run_feature::category_feature as run_gp,
        f::user_features as user_features;


raw_training_data = foreach all_feature_a {
    generate
        aid,
        FLATTEN(myfuncs.get_status_run_feature_ratio_indian_new(status_gp, run_gp, pkg_prob, pkgname_prob, user_features)) as (feature,featmap);
};
raw_training_data = foreach raw_training_data generate aid, feature;

rmf $output/gender_feature;
STORE raw_training_data into '$output/gender_feature';


--raw_training_data = load '$output/gender_feature' as (aid:chararray,feature:{T:(fname:chararray,fvalue:float)});
pred_features = FOREACH raw_training_data  GENERATE aid,
        myfuncs.to_map(feature) as gender_feature;

gender_pred_result = FOREACH pred_features GENERATE aid, '$end_date' as uptime,
    EVAL_GENDER(gender_feature) as gender_score;

rmf $score/gender;
store gender_pred_result into '$score/gender';



