
--set output.compression.enabled true;
--set output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
set fs.s3.canned.acl BucketOwnerFullControl;

--register '../lib/random_age.py' using jython as age_udf;
--%default input_score 's3://com.cmcm.bigdata.dh/targeting_up/warehouse/demo_predict_data/indian_age_gender/score';
--%default output_summary 's3://com.cmcm.bigdata.dh/targeting_up/warehouse/demo_predict_data/indian_age_gender/score_end/20160228';
%default input_score '/user/dulimei/tmp/indian_age_gender/score_end/20160520';
--%default pre_score '/user/dulimei/tmp/indian_age_gender/score_end/20160512';


hist_summary_pre = load '$input_score' as (aid:chararray, uptime:chararray, gender:int, age:int);

hist_summary_pre = filter hist_summary_pre by uptime=='20160519';
hist_summary_pre_gender = group hist_summary_pre by gender;
hist_summary_pre_gender = foreach   hist_summary_pre_gender generate group  as gender ,COUNT($1.aid);
dump hist_summary_pre_gender;


hist_summary_pre_age = group hist_summary_pre by age;
hist_summary_pre_age = foreach   hist_summary_pre_age generate group  as age ,COUNT($1.aid);
dump hist_summary_pre_age;

/*
hist_summary = load '$pre_score' as (aid:chararray, uptime:chararray, gender:int, age:int);

hist_summary_pre = filter hist_summary by uptime=='20160414';
hist_summary_pre_gender = group hist_summary_pre by gender;
hist_summary_pre_gender = foreach   hist_summary_pre_gender generate group  as gender ,COUNT($1.aid);

dump hist_summary_pre_gender;
hist_summary_pre_age = group hist_summary_pre by age;
hist_summary_pre_age = foreach   hist_summary_pre_age generate group  as age ,COUNT($1.aid);

dump hist_summary_pre_age;


*/
